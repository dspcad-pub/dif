# CMake generated Testfile for 
# Source directory: /Users/yaesoplee/dfw-pub/gitlab/difpub/import/clapack
# Build directory: /Users/yaesoplee/dfw-pub/gitlab/difpub/import/clapack
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("F2CLIBS")
subdirs("BLAS")
subdirs("SRC")
