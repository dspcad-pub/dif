Imported/redistributed packages from other sources (outside
the DSPCAD Group).

1. CLAPACK:
This package was downloaded from https://github.com/hunter-packages/clapack on
Sep 27 2021.

Set up IXCLAPACK variable `export IXCLAPACK=<path to here>/clapack

Run makeme to install the package.

*MODIFICATIONS DONE FROM THE SOURCE CODE*
Users should modify the source code once they reinstall the package.

- If you get "implicitly declaring library" error from xerbla.c, just comment
the line out and run makeme. It is likely to happen to Mac OS users.

- If you do not have sudo access, modify the destination in CMakeLists.txt files
. For example,
install(EXPORT CLAPACKTargets NAMESPACE CLAPACK:: DESTINATION
$ENV{LIDEUSER}/csmgen/CLAPACK)
install(FILES CLAPACKConfig.cmake DESTINATION $ENV{LIDEUSER}/csmgen/CLAPACK)

- If you are using cmake version >= 5 or if you meet compiler errors, you should
 modify the source code by following instructions below.
 1> remove or comment out line in 159-160 clapack/INCLUDE/f2c.h
 2> run clapack/edit_macro.sh to place the removed two lines into files that is
 needed to be used those macros.


2. MDPSolve:
This package was downloaded from https://github.com/PaulFackler/MDPSolve/ on Sep
 27 2021.

Set up IXCLAPACK variable `export IXMDPSOLVE=<path to here>/MDPSolve
You only need mdptools/ and mdputils/.
To update the source code to the latest version, run
`ixupdate MDPSolve

No need to build the source code.

3. jsonlab:
This package was downloaded from https://github.com/fangq/jsonlab/ on Sep
 27 2021.
Set up IXCLAPACK variable `export IXJSONLAB=<path to here>/IXJSONLAB
To update the source code to the latest version, run
`ixupdate jsonlab

No need to build the source code.
