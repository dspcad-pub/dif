# Install script for directory: /Users/yaesoplee/dfw-pub/gitlab/difpub/import/clapack

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/Library/Developer/CommandLineTools/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/Users/yaesoplee/dspcad_user/dif_user/difgen/CLAPACK/CLAPACKTargets.cmake")
    file(DIFFERENT EXPORT_FILE_CHANGED FILES
         "$ENV{DESTDIR}/Users/yaesoplee/dspcad_user/dif_user/difgen/CLAPACK/CLAPACKTargets.cmake"
         "/Users/yaesoplee/dfw-pub/gitlab/difpub/import/clapack/CMakeFiles/Export/_Users/yaesoplee/dspcad_user/dif_user/difgen/CLAPACK/CLAPACKTargets.cmake")
    if(EXPORT_FILE_CHANGED)
      file(GLOB OLD_CONFIG_FILES "$ENV{DESTDIR}/Users/yaesoplee/dspcad_user/dif_user/difgen/CLAPACK/CLAPACKTargets-*.cmake")
      if(OLD_CONFIG_FILES)
        message(STATUS "Old export file \"$ENV{DESTDIR}/Users/yaesoplee/dspcad_user/dif_user/difgen/CLAPACK/CLAPACKTargets.cmake\" will be replaced.  Removing files [${OLD_CONFIG_FILES}].")
        file(REMOVE ${OLD_CONFIG_FILES})
      endif()
    endif()
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/yaesoplee/dspcad_user/dif_user/difgen/CLAPACK/CLAPACKTargets.cmake")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/Users/yaesoplee/dspcad_user/dif_user/difgen/CLAPACK" TYPE FILE FILES "/Users/yaesoplee/dfw-pub/gitlab/difpub/import/clapack/CMakeFiles/Export/_Users/yaesoplee/dspcad_user/dif_user/difgen/CLAPACK/CLAPACKTargets.cmake")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^()$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "/Users/yaesoplee/dspcad_user/dif_user/difgen/CLAPACK/CLAPACKTargets-noconfig.cmake")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "/Users/yaesoplee/dspcad_user/dif_user/difgen/CLAPACK" TYPE FILE FILES "/Users/yaesoplee/dfw-pub/gitlab/difpub/import/clapack/CMakeFiles/Export/_Users/yaesoplee/dspcad_user/dif_user/difgen/CLAPACK/CLAPACKTargets-noconfig.cmake")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/yaesoplee/dspcad_user/dif_user/difgen/CLAPACK/CLAPACKConfig.cmake")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/Users/yaesoplee/dspcad_user/dif_user/difgen/CLAPACK" TYPE FILE FILES "/Users/yaesoplee/dfw-pub/gitlab/difpub/import/clapack/CLAPACKConfig.cmake")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/yaesoplee/dspcad_user/dif_user/difgen/CLAPACK/blaswrap.h;/Users/yaesoplee/dspcad_user/dif_user/difgen/CLAPACK/clapack.h;/Users/yaesoplee/dspcad_user/dif_user/difgen/CLAPACK/f2c.h")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/Users/yaesoplee/dspcad_user/dif_user/difgen/CLAPACK" TYPE FILE FILES
    "/Users/yaesoplee/dfw-pub/gitlab/difpub/import/clapack/INCLUDE/blaswrap.h"
    "/Users/yaesoplee/dfw-pub/gitlab/difpub/import/clapack/INCLUDE/clapack.h"
    "/Users/yaesoplee/dfw-pub/gitlab/difpub/import/clapack/INCLUDE/f2c.h"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/Users/yaesoplee/dfw-pub/gitlab/difpub/import/clapack/F2CLIBS/cmake_install.cmake")
  include("/Users/yaesoplee/dfw-pub/gitlab/difpub/import/clapack/BLAS/cmake_install.cmake")
  include("/Users/yaesoplee/dfw-pub/gitlab/difpub/import/clapack/SRC/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/Users/yaesoplee/dfw-pub/gitlab/difpub/import/clapack/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
