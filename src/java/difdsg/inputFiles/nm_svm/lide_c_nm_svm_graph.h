#ifndef _lide_c_nm_svm_graph_h
#define _lide_c_nm_svm_graph_h

#include <stdio.h>
#include <stdlib.h>
#include "lide_c_basic.h"
#include "lide_c_actor.h"
#include "lide_c_fifo.h"
#include "lide_c_fifo_basic.h"
#include "lide_c_graph.h"
#include "lide_c_util.h"
#include "lide_c_file_source_double.h"
#include "lide_c_svm.h"
#include "lide_c_class.h"
#include "lide_c_file_sink_double.h"

#define BUFFER_CAPACITY 1024

#define ACTOR_SVM	0
#define ACTOR_SINK	1
#define ACTOR_XSOURCE	2
#define ACTOR_CLASS	3
#define ACTOR_ALPHA	4
#define ACTOR_SV	5

#define FIFO_SRC_SVM	0
#define FIFO_SV	1
#define FIFO_SVM_CLASS	2
#define FIFO_CLASS_SINK	3
#define FIFO_ALPHA	4

#define ACTOR_COUNT	6
#define FIFO_COUNT	5

struct _lide_c_nm_svm_graph_context_struct;
typedef struct _lide_c_nm_svm_graph_context_struct 
    lide_c_nm_svm_graph_context_type;

lide_c_nm_svm_graph_context_type *lide_c_nm_svm_graph_new();

void lide_c_nm_svm_graph_terminate(lide_c_nm_svm_graph_context_type *graph);

void lide_c_nm_svm_graph_scheduler(lide_c_nm_svm_graph_context_type *graph);

#endif
