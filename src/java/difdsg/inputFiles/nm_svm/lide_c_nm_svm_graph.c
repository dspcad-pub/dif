#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lide_c_basic.h"
#include "lide_c_actor.h"
#include "lide_c_nm_svm_graph.h"


struct _lide_c_nm_svm_graph_context_struct {
#include "lide_c_graph_context_type_common.h"
};

lide_c_nm_svm_graph_context_type *lide_c_nm_svm_graph_new(char * x_file, char *
    out_file, char * sv_file, char * alpha_file, int feature_length, int
    sv_length, double gamma, double bias, int kernel_type){
    int token_size;
    lide_c_nm_svm_graph_context_type * context = NULL;
    context = (lide_c_nm_svm_graph_context_type *)lide_c_util_malloc(sizeof(
        lide_c_nm_svm_graph_context_type));
    context->actor_count = ACTOR_COUNT;
    context->fifo_count = FIFO_COUNT;


    context->actors = (lide_c_actor_context_type **)lide_c_util_malloc(
        context->actor_count * sizeof(lide_c_actor_context_type *));
    context->fifos = (lide_c_fifo_pointer *)lide_c_util_malloc(
        context->fifo_count * sizeof(lide_c_fifo_pointer));

    context->descriptors = (char **)lide_c_util_malloc(context->actor_count * 
        sizeof(char*));
    lide_c_graph_set_actor_desc((lide_c_graph_context_type *)context, 
        ACTOR_SVM, "svm");
    lide_c_graph_set_actor_desc((lide_c_graph_context_type *)context, 
        ACTOR_SINK, "sink");
    lide_c_graph_set_actor_desc((lide_c_graph_context_type *)context, 
        ACTOR_XSOURCE, "xsource");
    lide_c_graph_set_actor_desc((lide_c_graph_context_type *)context, 
        ACTOR_CLASS, "class");
    lide_c_graph_set_actor_desc((lide_c_graph_context_type *)context, 
        ACTOR_ALPHA, "alpha");
    lide_c_graph_set_actor_desc((lide_c_graph_context_type *)context, ACTOR_SV, 
        "sv");

    context->source_array = (lide_c_actor_context_type **)lide_c_util_malloc(
        context->fifo_count * sizeof(lide_c_actor_context_type *));

    context->sink_array = (lide_c_actor_context_type **)lide_c_util_malloc(
        context->fifo_count * sizeof(lide_c_actor_context_type *));

    token_size = sizeof(double);
    context->fifos[FIFO_SRC_SVM] = (lide_c_fifo_pointer)lide_c_fifo_basic_new(
        150000, token_size, FIFO_SRC_SVM);
    token_size = sizeof(double);
    context->fifos[FIFO_SV] = (lide_c_fifo_pointer)lide_c_fifo_basic_new(
        150000, token_size, FIFO_SV);
    token_size = sizeof(double);
    context->fifos[FIFO_SVM_CLASS] = (lide_c_fifo_pointer)lide_c_fifo_basic_new(
        150000, token_size, FIFO_SVM_CLASS);
    token_size = sizeof(double);
    context->fifos[FIFO_CLASS_SINK] = (lide_c_fifo_pointer)
        lide_c_fifo_basic_new(150000, token_size, FIFO_CLASS_SINK);
    token_size = sizeof(double);
    context->fifos[FIFO_ALPHA] = (lide_c_fifo_pointer)lide_c_fifo_basic_new(
        150000, token_size, FIFO_ALPHA);

    context->actors[ACTOR_SVM] = (lide_c_actor_context_type *)(lide_c_svm_new(
        context->fifos[FIFO_SRC_SVM], context->fifos[FIFO_SVM_CLASS],
        context->fifos[FIFO_SV], context->fifos[FIFO_ALPHA], feature_length,
        sv_length, gamma, bias, kernel_type, ACTOR_SVM));
    lide_c_svm_connect(context->actors[ACTOR_SVM], (lide_c_graph_context_type
        *)context);

    context->actors[ACTOR_SINK] = (lide_c_actor_context_type *)
        (lide_c_file_sink_double_new(out_file, context->fifos[FIFO_CLASS_SINK],
        ACTOR_SINK));
    lide_c_file_sink_double_connect(context->actors[ACTOR_SINK], 
        (lide_c_graph_context_type *)context);

    context->actors[ACTOR_XSOURCE] = (lide_c_actor_context_type *)
        (lide_c_file_source_double_new(x_file, context->fifos[FIFO_SRC_SVM],
        ACTOR_XSOURCE));
    lide_c_file_source_double_connect(context->actors[ACTOR_XSOURCE], 
        (lide_c_graph_context_type *)context);

    context->actors[ACTOR_CLASS] = (lide_c_actor_context_type *)
        (lide_c_class_new(context->fifos[FIFO_SVM_CLASS],
        context->fifos[FIFO_CLASS_SINK], 0, ACTOR_CLASS));
    lide_c_class_connect(context->actors[ACTOR_CLASS], 
        (lide_c_graph_context_type *)context);

    context->actors[ACTOR_ALPHA] = (lide_c_actor_context_type *)
        (lide_c_file_source_double_new(alpha_file, context->fifos[FIFO_ALPHA],
        ACTOR_ALPHA));
    lide_c_file_source_double_connect(context->actors[ACTOR_ALPHA], 
        (lide_c_graph_context_type *)context);

    context->actors[ACTOR_SV] = (lide_c_actor_context_type *)
        (lide_c_file_source_double_new(sv_file, context->fifos[FIFO_SV],
        ACTOR_SV));
    lide_c_file_source_double_connect(context->actors[ACTOR_SV], 
        (lide_c_graph_context_type *)context);

    context->scheduler = (lide_c_graph_scheduler_ftype)
        lide_c_nm_svm_graph_scheduler;
    return context;
}

void lide_c_nm_svm_graph_terminate(lide_c_nm_svm_graph_context_type *context){
    int i;
    for(i = 0; i < context->fifo_count; i++){
        lide_c_fifo_basic_free((lide_c_fifo_basic_pointer)context->fifos[i]);
    }

    lide_c_svm_terminate((lide_c_svm_context_type *)context->actors[ACTOR_SVM]);
    lide_c_file_sink_double_terminate((lide_c_file_sink_double_context_type *)
        context->actors[ACTOR_SINK]);
    lide_c_file_source_double_terminate((lide_c_file_source_double_context_type
        *)context->actors[ACTOR_XSOURCE]);
    lide_c_class_terminate((lide_c_class_context_type *)
        context->actors[ACTOR_CLASS]);
    lide_c_file_source_double_terminate((lide_c_file_source_double_context_type
        *)context->actors[ACTOR_ALPHA]);
    lide_c_file_source_double_terminate((lide_c_file_source_double_context_type
        *)context->actors[ACTOR_SV]);

    free(context->fifos);
    free(context->actors);
    free(context->descriptors);
    free(context);
}
void lide_c_nm_svm_graph_scheduler(lide_c_nm_svm_graph_context_type *context){
    lide_c_util_simple_scheduler(context->actors, context->actor_count, 
        context->descriptors);
    return;
}
