#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "welt_cpp_basic.h"
#include "welt_cpp_actor.h"
#include "welt_cpp_nm_svm_graph.h"


struct _welt_cpp_nm_svm_graph_context_struct {
#include "welt_cpp_graph_context_type_common.h"
};

welt_cpp_nm_svm_graph_context_type *welt_cpp_nm_svm_graph_new(char * x_file,
    char * out_file, char * sv_file, char * alpha_file, int feature_length, int
    sv_length, double gamma, double bias, int kernel_type){
    int token_size;
    welt_cpp_nm_svm_graph_context_type * context = NULL;
    context = (welt_cpp_nm_svm_graph_context_type *)welt_cpp_util_malloc(sizeof(
        welt_cpp_nm_svm_graph_context_type));
    context->actor_count = ACTOR_COUNT;
    context->fifo_count = FIFO_COUNT;


    context->actors = (welt_cpp_actor_context_type **)welt_cpp_util_malloc(
        context->actor_count * sizeof(welt_cpp_actor_context_type *));
    context->fifos = (welt_cpp_fifo_pointer *)welt_cpp_util_malloc(
        context->fifo_count * sizeof(welt_cpp_fifo_pointer));

    context->descriptors = (char **)welt_cpp_util_malloc(context->actor_count * 
        sizeof(char*));
    welt_cpp_graph_set_actor_desc((welt_cpp_graph_context_type *)context, 
        ACTOR_SVM, "svm");
    welt_cpp_graph_set_actor_desc((welt_cpp_graph_context_type *)context, 
        ACTOR_SINK, "sink");
    welt_cpp_graph_set_actor_desc((welt_cpp_graph_context_type *)context, 
        ACTOR_XSOURCE, "xsource");
    welt_cpp_graph_set_actor_desc((welt_cpp_graph_context_type *)context, 
        ACTOR_CLASS, "class");
    welt_cpp_graph_set_actor_desc((welt_cpp_graph_context_type *)context, 
        ACTOR_ALPHA, "alpha");
    welt_cpp_graph_set_actor_desc((welt_cpp_graph_context_type *)context, 
        ACTOR_SV, "sv");

    context->source_array = (welt_cpp_actor_context_type **)
        welt_cpp_util_malloc(context->fifo_count * 
        sizeof(welt_cpp_actor_context_type *));

    context->sink_array = (welt_cpp_actor_context_type **)welt_cpp_util_malloc(
        context->fifo_count * sizeof(welt_cpp_actor_context_type *));

    token_size = sizeof(double);
    context->fifos[FIFO_SRC_SVM] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_basic_new(150000, token_size, FIFO_SRC_SVM);
    token_size = sizeof(double);
    context->fifos[FIFO_SV] = (welt_cpp_fifo_pointer)welt_cpp_fifo_basic_new(
        150000, token_size, FIFO_SV);
    token_size = sizeof(double);
    context->fifos[FIFO_SVM_CLASS] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_basic_new(150000, token_size, FIFO_SVM_CLASS);
    token_size = sizeof(double);
    context->fifos[FIFO_CLASS_SINK] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_basic_new(150000, token_size, FIFO_CLASS_SINK);
    token_size = sizeof(double);
    context->fifos[FIFO_ALPHA] = (welt_cpp_fifo_pointer)welt_cpp_fifo_basic_new(
        150000, token_size, FIFO_ALPHA);

    context->actors[ACTOR_SVM] = (welt_cpp_actor_context_type *)
        (welt_cpp_svm_new(context->fifos[FIFO_SRC_SVM],
        context->fifos[FIFO_SVM_CLASS], context->fifos[FIFO_SV],
        context->fifos[FIFO_ALPHA], feature_length, sv_length, gamma, bias,
        kernel_type, ACTOR_SVM));
    welt_cpp_svm_connect(context->actors[ACTOR_SVM], 
        (welt_cpp_graph_context_type *)context);

    context->actors[ACTOR_SINK] = (welt_cpp_actor_context_type *)
        (welt_cpp_file_sink_double_new(out_file,
        context->fifos[FIFO_CLASS_SINK], ACTOR_SINK));
    welt_cpp_file_sink_double_connect(context->actors[ACTOR_SINK], 
        (welt_cpp_graph_context_type *)context);

    context->actors[ACTOR_XSOURCE] = (welt_cpp_actor_context_type *)
        (welt_cpp_file_source_double_new(x_file, context->fifos[FIFO_SRC_SVM],
        ACTOR_XSOURCE));
    welt_cpp_file_source_double_connect(context->actors[ACTOR_XSOURCE], 
        (welt_cpp_graph_context_type *)context);

    context->actors[ACTOR_CLASS] = (welt_cpp_actor_context_type *)
        (welt_cpp_class_new(context->fifos[FIFO_SVM_CLASS],
        context->fifos[FIFO_CLASS_SINK], 0, ACTOR_CLASS));
    welt_cpp_class_connect(context->actors[ACTOR_CLASS], 
        (welt_cpp_graph_context_type *)context);

    context->actors[ACTOR_ALPHA] = (welt_cpp_actor_context_type *)
        (welt_cpp_file_source_double_new(alpha_file,
        context->fifos[FIFO_ALPHA], ACTOR_ALPHA));
    welt_cpp_file_source_double_connect(context->actors[ACTOR_ALPHA], 
        (welt_cpp_graph_context_type *)context);

    context->actors[ACTOR_SV] = (welt_cpp_actor_context_type *)
        (welt_cpp_file_source_double_new(sv_file, context->fifos[FIFO_SV],
        ACTOR_SV));
    welt_cpp_file_source_double_connect(context->actors[ACTOR_SV], 
        (welt_cpp_graph_context_type *)context);

    context->scheduler = (welt_cpp_graph_scheduler_ftype)
        welt_cpp_nm_svm_graph_scheduler;
    return context;
}

void welt_cpp_nm_svm_graph_terminate(welt_cpp_nm_svm_graph_context_type
    *context){
    int i;
    for(i = 0; i < context->fifo_count; i++){
        welt_cpp_fifo_basic_free((welt_cpp_fifo_basic_pointer)
            context->fifos[i]);
    }

    welt_cpp_svm_terminate((welt_cpp_svm_context_type *)
        context->actors[ACTOR_SVM]);
    welt_cpp_file_sink_double_terminate((welt_cpp_file_sink_double_context_type
        *)context->actors[ACTOR_SINK]);
    welt_cpp_file_source_double_terminate(
        (welt_cpp_file_source_double_context_type *)
        context->actors[ACTOR_XSOURCE]);
    welt_cpp_class_terminate((welt_cpp_class_context_type *)
        context->actors[ACTOR_CLASS]);
    welt_cpp_file_source_double_terminate(
        (welt_cpp_file_source_double_context_type *)
        context->actors[ACTOR_ALPHA]);
    welt_cpp_file_source_double_terminate(
        (welt_cpp_file_source_double_context_type *)context->actors[ACTOR_SV]);

    free(context->fifos);
    free(context->actors);
    free(context->descriptors);
    free(context);
}
void welt_cpp_nm_svm_graph_scheduler(welt_cpp_nm_svm_graph_context_type
    *context){
    welt_cpp_util_simple_scheduler(context->actors, context->actor_count, 
        context->descriptors);
    return;
}
