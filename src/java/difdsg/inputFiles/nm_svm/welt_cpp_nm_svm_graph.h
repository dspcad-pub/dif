#ifndef _welt_cpp_nm_svm_graph_h
#define _welt_cpp_nm_svm_graph_h

#include <stdio.h>
#include <stdlib.h>
#include "welt_cpp_basic.h"
#include "welt_cpp_actor.h"
#include "welt_cpp_fifo.h"
#include "welt_cpp_fifo_basic.h"
#include "welt_cpp_graph.h"
#include "welt_cpp_util.h"
#include "welt_cpp_file_source_double.h"
#include "welt_cpp_svm.h"
#include "welt_cpp_class.h"
#include "welt_cpp_file_sink_double.h"

#define BUFFER_CAPACITY 1024

#define ACTOR_SVM	0
#define ACTOR_SINK	1
#define ACTOR_XSOURCE	2
#define ACTOR_CLASS	3
#define ACTOR_ALPHA	4
#define ACTOR_SV	5

#define FIFO_SRC_SVM	0
#define FIFO_SV	1
#define FIFO_SVM_CLASS	2
#define FIFO_CLASS_SINK	3
#define FIFO_ALPHA	4

#define ACTOR_COUNT	6
#define FIFO_COUNT	5

struct _welt_cpp_nm_svm_graph_context_struct;
typedef struct _welt_cpp_nm_svm_graph_context_struct 
    welt_cpp_nm_svm_graph_context_type;

welt_cpp_nm_svm_graph_context_type *welt_cpp_nm_svm_graph_new();

void welt_cpp_nm_svm_graph_terminate(welt_cpp_nm_svm_graph_context_type *graph);

void welt_cpp_nm_svm_graph_scheduler(welt_cpp_nm_svm_graph_context_type *graph);

#endif
