#ifndef _lide_c_nm_svm_sdsg_h
#define _lide_c_nm_svm_sdsg_h

#include <stdio.h>
#include <stdlib.h>
#include "lide_c_basic.h"
#include "lide_c_actor.h"
#include "lide_c_fifo.h"
#include "lide_c_fifo_basic.h"
#include "lide_c_fifo_unit_size.h"
#include "lide_c_graph.h"
#include "lide_c_util.h"

#include "lide_c_dsg_actor.h"
#include "lide_c_ra_util.h"
#include "lide_c_ref_pre_post_functions.h"

#include "lide_c_ref_actor.h"
#include "lide_c_sca_sch_loop.h"
#include "lide_c_sca_static_loop.h"

#include "lide_c_nm_svm_graph.h"

#define DSG_BUFFER_CAPACITY 1024

#define DSG_ACTOR_RSVM	0
#define DSG_ACTOR_RCLASS	1
#define DSG_ACTOR_LOOP1	2
#define DSG_ACTOR_RSRC	3
#define DSG_ACTOR_RSINK	4
#define DSG_ACTOR_LAE	5

#define DSG_FIFO_E4	0
#define DSG_FIFO_E0	1
#define DSG_FIFO_E5	2
#define DSG_FIFO_E3	3
#define DSG_FIFO_E1	4
#define DSG_FIFO_E6	5
#define DSG_FIFO_E2	6

#define DSG_ACTOR_COUNT	6
#define DSG_FIFO_COUNT	7

struct _lide_c_nm_svm_sdsg_context_struct;
typedef struct _lide_c_nm_svm_sdsg_context_struct 
    lide_c_nm_svm_sdsg_context_type;

lide_c_nm_svm_sdsg_context_type *lide_c_nm_svm_sdsg_new();

void lide_c_nm_svm_sdsg_terminate(lide_c_nm_svm_sdsg_context_type *graph);

void lide_c_nm_svm_sdsg_scheduler(lide_c_nm_svm_sdsg_context_type *graph);

#endif
