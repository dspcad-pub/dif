#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lide_c_basic.h"
#include "lide_c_actor.h"
#include "lide_c_nm_svm_sdsg.h"


struct _lide_c_nm_svm_sdsg_context_struct {
#include "lide_c_graph_context_type_common.h"
};

lide_c_nm_svm_sdsg_context_type *lide_c_nm_svm_sdsg_new(
    lide_c_graph_context_type * contextApp, int feature_length, int iter){
    int token_size;
    lide_c_nm_svm_sdsg_context_type * context = NULL;
    context = (lide_c_nm_svm_sdsg_context_type *)lide_c_util_malloc(sizeof(
        lide_c_nm_svm_sdsg_context_type));
    context->actor_count = DSG_ACTOR_COUNT;
    context->fifo_count = DSG_FIFO_COUNT;


    context->actors = (lide_c_actor_context_type **)lide_c_util_malloc(
        context->actor_count * sizeof(lide_c_actor_context_type *));
    context->fifos = (lide_c_fifo_pointer *)lide_c_util_malloc(
        context->fifo_count * sizeof(lide_c_fifo_pointer));

    context->descriptors = (char **)lide_c_util_malloc(context->actor_count * 
        sizeof(char*));
    lide_c_graph_set_actor_desc((lide_c_graph_context_type *)context, 
        DSG_ACTOR_RSVM, "rsvm");
    lide_c_graph_set_actor_desc((lide_c_graph_context_type *)context, 
        DSG_ACTOR_RCLASS, "rclass");
    lide_c_graph_set_actor_desc((lide_c_graph_context_type *)context, 
        DSG_ACTOR_LOOP1, "loop1");
    lide_c_graph_set_actor_desc((lide_c_graph_context_type *)context, 
        DSG_ACTOR_RSRC, "rsrc");
    lide_c_graph_set_actor_desc((lide_c_graph_context_type *)context, 
        DSG_ACTOR_RSINK, "rsink");
    lide_c_graph_set_actor_desc((lide_c_graph_context_type *)context, 
        DSG_ACTOR_LAE, "lae");

    context->source_array = (lide_c_actor_context_type **)lide_c_util_malloc(
        context->fifo_count * sizeof(lide_c_actor_context_type *));

    context->sink_array = (lide_c_actor_context_type **)lide_c_util_malloc(
        context->fifo_count * sizeof(lide_c_actor_context_type *));

    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E4] = (lide_c_fifo_pointer)
        lide_c_fifo_unit_size_new(token_size, DSG_FIFO_E4);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E0] = (lide_c_fifo_pointer)
        lide_c_fifo_unit_size_new(token_size, DSG_FIFO_E0);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E5] = (lide_c_fifo_pointer)
        lide_c_fifo_unit_size_new(token_size, DSG_FIFO_E5);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E3] = (lide_c_fifo_pointer)
        lide_c_fifo_unit_size_new(token_size, DSG_FIFO_E3);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E1] = (lide_c_fifo_pointer)
        lide_c_fifo_unit_size_new(token_size, DSG_FIFO_E1);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E6] = (lide_c_fifo_pointer)
        lide_c_fifo_unit_size_new(token_size, DSG_FIFO_E6);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E2] = (lide_c_fifo_pointer)
        lide_c_fifo_unit_size_new(token_size, DSG_FIFO_E2);

    context->actors[DSG_ACTOR_RSVM] = (lide_c_actor_context_type *)
        (lide_c_ref_actor_new(context->fifos[DSG_FIFO_E3],
        context->fifos[DSG_FIFO_E4], DSG_ACTOR_RSVM,
        contextApp->actors[ACTOR_SVM], 0, 0, NULL, NULL, NULL));
    lide_c_ref_actor_connect(context->actors[DSG_ACTOR_RSVM], 
        (lide_c_graph_context_type *)context);

    context->actors[DSG_ACTOR_RCLASS] = (lide_c_actor_context_type *)
        (lide_c_ref_actor_new(context->fifos[DSG_FIFO_E4],
        context->fifos[DSG_FIFO_E5], DSG_ACTOR_RCLASS,
        contextApp->actors[ACTOR_CLASS], 0, 0, NULL, NULL, NULL));
    lide_c_ref_actor_connect(context->actors[DSG_ACTOR_RCLASS], 
        (lide_c_graph_context_type *)context);

    context->actors[DSG_ACTOR_LOOP1] = (lide_c_actor_context_type *)
        (lide_c_sca_static_loop_new(context->fifos[DSG_FIFO_E0],
        context->fifos[DSG_FIFO_E2], context->fifos[DSG_FIFO_E1],
        context->fifos[DSG_FIFO_E3], feature_length, DSG_ACTOR_LOOP1));
    lide_c_sca_static_loop_connect(context->actors[DSG_ACTOR_LOOP1], 
        (lide_c_graph_context_type *)context);

    context->actors[DSG_ACTOR_RSRC] = (lide_c_actor_context_type *)
        (lide_c_ref_actor_new(context->fifos[DSG_FIFO_E1],
        context->fifos[DSG_FIFO_E2], DSG_ACTOR_RSRC,
        contextApp->actors[ACTOR_XSOURCE], 0, 0, NULL, NULL, NULL));
    lide_c_ref_actor_connect(context->actors[DSG_ACTOR_RSRC], 
        (lide_c_graph_context_type *)context);

    context->actors[DSG_ACTOR_RSINK] = (lide_c_actor_context_type *)
        (lide_c_ref_actor_new(context->fifos[DSG_FIFO_E5],
        context->fifos[DSG_FIFO_E6], DSG_ACTOR_RSINK,
        contextApp->actors[ACTOR_SINK], 0, 0, NULL, NULL, NULL));
    lide_c_ref_actor_connect(context->actors[DSG_ACTOR_RSINK], 
        (lide_c_graph_context_type *)context);

    context->actors[DSG_ACTOR_LAE] = (lide_c_actor_context_type *)
        (lide_c_sca_sch_loop_new(context->fifos[DSG_FIFO_E6],
        context->fifos[DSG_FIFO_E0], iter, DSG_ACTOR_LAE));
    lide_c_sca_sch_loop_connect(context->actors[DSG_ACTOR_LAE], 
        (lide_c_graph_context_type *)context);

    context->scheduler = (lide_c_graph_scheduler_ftype)
        lide_c_nm_svm_sdsg_scheduler;
    return context;
}

void lide_c_nm_svm_sdsg_terminate(lide_c_nm_svm_sdsg_context_type *context){
    int i;
    for(i = 0; i < context->fifo_count; i++){
        lide_c_fifo_basic_free((lide_c_fifo_basic_pointer)context->fifos[i]);
    }

    lide_c_ref_actor_terminate((lide_c_ref_actor_context_type *)
        context->actors[DSG_ACTOR_RSVM]);
    lide_c_ref_actor_terminate((lide_c_ref_actor_context_type *)
        context->actors[DSG_ACTOR_RCLASS]);
    lide_c_sca_static_loop_terminate((lide_c_sca_static_loop_context_type *)
        context->actors[DSG_ACTOR_LOOP1]);
    lide_c_ref_actor_terminate((lide_c_ref_actor_context_type *)
        context->actors[DSG_ACTOR_RSRC]);
    lide_c_ref_actor_terminate((lide_c_ref_actor_context_type *)
        context->actors[DSG_ACTOR_RSINK]);
    lide_c_sca_sch_loop_terminate((lide_c_sca_sch_loop_context_type *)
        context->actors[DSG_ACTOR_LAE]);

    free(context->fifos);
    free(context->actors);
    free(context->descriptors);
    free(context);
}
void lide_c_nm_svm_sdsg_scheduler(lide_c_nm_svm_sdsg_context_type *context){
    lide_c_util_simple_scheduler(context->actors, context->actor_count, 
        context->descriptors);
    return;
}
