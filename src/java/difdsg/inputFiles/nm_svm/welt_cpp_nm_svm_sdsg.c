#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "welt_cpp_basic.h"
#include "welt_cpp_actor.h"
#include "welt_cpp_nm_svm_sdsg.h"


struct _welt_cpp_nm_svm_sdsg_context_struct {
#include "welt_cpp_graph_context_type_common.h"
};

welt_cpp_nm_svm_sdsg_context_type *welt_cpp_nm_svm_sdsg_new(
    welt_cpp_graph_context_type * contextApp, int feature_length, int iter){
    int token_size;
    welt_cpp_nm_svm_sdsg_context_type * context = NULL;
    context = (welt_cpp_nm_svm_sdsg_context_type *)welt_cpp_util_malloc(sizeof(
        welt_cpp_nm_svm_sdsg_context_type));
    context->actor_count = DSG_ACTOR_COUNT;
    context->fifo_count = DSG_FIFO_COUNT;


    context->actors = (welt_cpp_actor_context_type **)welt_cpp_util_malloc(
        context->actor_count * sizeof(welt_cpp_actor_context_type *));
    context->fifos = (welt_cpp_fifo_pointer *)welt_cpp_util_malloc(
        context->fifo_count * sizeof(welt_cpp_fifo_pointer));

    context->descriptors = (char **)welt_cpp_util_malloc(context->actor_count * 
        sizeof(char*));
    welt_cpp_graph_set_actor_desc((welt_cpp_graph_context_type *)context, 
        DSG_ACTOR_LAE, "lae");
    welt_cpp_graph_set_actor_desc((welt_cpp_graph_context_type *)context, 
        DSG_ACTOR_RSRC, "rsrc");
    welt_cpp_graph_set_actor_desc((welt_cpp_graph_context_type *)context, 
        DSG_ACTOR_LOOP1, "loop1");
    welt_cpp_graph_set_actor_desc((welt_cpp_graph_context_type *)context, 
        DSG_ACTOR_RSVM, "rsvm");
    welt_cpp_graph_set_actor_desc((welt_cpp_graph_context_type *)context, 
        DSG_ACTOR_RCLASS, "rclass");
    welt_cpp_graph_set_actor_desc((welt_cpp_graph_context_type *)context, 
        DSG_ACTOR_RSINK, "rsink");

    context->source_array = (welt_cpp_actor_context_type **)
        welt_cpp_util_malloc(context->fifo_count * 
        sizeof(welt_cpp_actor_context_type *));

    context->sink_array = (welt_cpp_actor_context_type **)welt_cpp_util_malloc(
        context->fifo_count * sizeof(welt_cpp_actor_context_type *));

    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E1] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E1);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E4] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E4);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E5] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E5);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E0] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E0);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E2] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E2);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E3] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E3);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E6] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E6);

    context->actors[DSG_ACTOR_LAE] = (welt_cpp_actor_context_type *)
        (welt_cpp_sca_sch_loop_new(context->fifos[DSG_FIFO_E6],
        context->fifos[DSG_FIFO_E0], iter, DSG_ACTOR_LAE));
    welt_cpp_sca_sch_loop_connect(context->actors[DSG_ACTOR_LAE], 
        (welt_cpp_graph_context_type *)context);

    context->actors[DSG_ACTOR_RSRC] = (welt_cpp_actor_context_type *)
        (welt_cpp_ref_actor_new(context->fifos[DSG_FIFO_E1],
        context->fifos[DSG_FIFO_E2], DSG_ACTOR_RSRC,
        contextApp->actors[ACTOR_XSOURCE], 0, 0, NULL, NULL, NULL));
    welt_cpp_ref_actor_connect(context->actors[DSG_ACTOR_RSRC], 
        (welt_cpp_graph_context_type *)context);

    context->actors[DSG_ACTOR_LOOP1] = (welt_cpp_actor_context_type *)
        (welt_cpp_sca_static_loop_new(context->fifos[DSG_FIFO_E0],
        context->fifos[DSG_FIFO_E2], context->fifos[DSG_FIFO_E1],
        context->fifos[DSG_FIFO_E3], feature_length, DSG_ACTOR_LOOP1));
    welt_cpp_sca_static_loop_connect(context->actors[DSG_ACTOR_LOOP1], 
        (welt_cpp_graph_context_type *)context);

    context->actors[DSG_ACTOR_RSVM] = (welt_cpp_actor_context_type *)
        (welt_cpp_ref_actor_new(context->fifos[DSG_FIFO_E3],
        context->fifos[DSG_FIFO_E4], DSG_ACTOR_RSVM,
        contextApp->actors[ACTOR_SVM], 0, 0, NULL, NULL, NULL));
    welt_cpp_ref_actor_connect(context->actors[DSG_ACTOR_RSVM], 
        (welt_cpp_graph_context_type *)context);

    context->actors[DSG_ACTOR_RCLASS] = (welt_cpp_actor_context_type *)
        (welt_cpp_ref_actor_new(context->fifos[DSG_FIFO_E4],
        context->fifos[DSG_FIFO_E5], DSG_ACTOR_RCLASS,
        contextApp->actors[ACTOR_CLASS], 0, 0, NULL, NULL, NULL));
    welt_cpp_ref_actor_connect(context->actors[DSG_ACTOR_RCLASS], 
        (welt_cpp_graph_context_type *)context);

    context->actors[DSG_ACTOR_RSINK] = (welt_cpp_actor_context_type *)
        (welt_cpp_ref_actor_new(context->fifos[DSG_FIFO_E5],
        context->fifos[DSG_FIFO_E6], DSG_ACTOR_RSINK,
        contextApp->actors[ACTOR_SINK], 0, 0, NULL, NULL, NULL));
    welt_cpp_ref_actor_connect(context->actors[DSG_ACTOR_RSINK], 
        (welt_cpp_graph_context_type *)context);

    context->scheduler = (welt_cpp_graph_scheduler_ftype)
        welt_cpp_nm_svm_sdsg_scheduler;
    return context;
}

void welt_cpp_nm_svm_sdsg_terminate(welt_cpp_nm_svm_sdsg_context_type *context){
    int i;
    for(i = 0; i < context->fifo_count; i++){
        welt_cpp_fifo_basic_free((welt_cpp_fifo_basic_pointer)
            context->fifos[i]);
    }

    welt_cpp_sca_sch_loop_terminate((welt_cpp_sca_sch_loop_context_type *)
        context->actors[DSG_ACTOR_LAE]);
    welt_cpp_ref_actor_terminate((welt_cpp_ref_actor_context_type *)
        context->actors[DSG_ACTOR_RSRC]);
    welt_cpp_sca_static_loop_terminate((welt_cpp_sca_static_loop_context_type *)
        context->actors[DSG_ACTOR_LOOP1]);
    welt_cpp_ref_actor_terminate((welt_cpp_ref_actor_context_type *)
        context->actors[DSG_ACTOR_RSVM]);
    welt_cpp_ref_actor_terminate((welt_cpp_ref_actor_context_type *)
        context->actors[DSG_ACTOR_RCLASS]);
    welt_cpp_ref_actor_terminate((welt_cpp_ref_actor_context_type *)
        context->actors[DSG_ACTOR_RSINK]);

    free(context->fifos);
    free(context->actors);
    free(context->descriptors);
    free(context);
}
void welt_cpp_nm_svm_sdsg_scheduler(welt_cpp_nm_svm_sdsg_context_type *context){
    welt_cpp_util_simple_scheduler(context->actors, context->actor_count, 
        context->descriptors);
    return;
}
