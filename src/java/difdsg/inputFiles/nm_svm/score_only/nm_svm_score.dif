/*
Dataflow application graph DIF file for neuromodulation-svm.
@author Kyunghun Lee
Date: 03/06/2019
*/

sdf nm_svm_score {
  topology {
    nodes = xsource,svm,sv,alpha,sink;
    edges = SRC_SVM (xsource, svm),
            SVM_SINK (svm, sink),
            SV (sv, svm),
            ALPHA (alpha, svm);
  }

  attribute edgeType {
    SRC_SVM = "double"; SVM_SINK = "double"; SV = "double";
    ALPHA = "double";
  }

  attribute capacityValue {
    SRC_SVM = 150000; SVM_SINK = 150000; SV = 150000;
    ALPHA = 150000;
  }

  parameter {
    graphArguType1 = "char *"; graphArguType2 = "char *"; graphArguType3 = "char *"; graphArguType4 = "char *";
    graphArguType5 = "int"; graphArguType6 = "int"; graphArguType7 = "int"; graphArguType8 = "double";
    graphArguType9 = "int";
    graphArguVar1 = "x_file"; graphArguVar2 = "out_file"; graphArguVar3 = "sv_file"; graphArguVar4 = "alpha_file";
    graphArguVar5 = "feature_length"; graphArguVar6 = "sv_length"; graphArguVar7 = "gamma";
    graphArguVar8 = "bias"; graphArguVar9 = "kernel_type";
  }

  actortype source_double {
    output out1;
    param name;
    param argu1; // file name
    param argu2; // output FIFO
    param argu3; // actor index
    production out1:1;
  }

  actortype svm {
    output out1;
    input in1,in2,in3;
    param name;
    param argu1; // input FIFO1
    param argu2; // output FIFO
    param argu3; // SV
    param argu4; // ALPHA
    param argu5; // feature_length
    param argu6; // sv_length
    param argu7; // gamma
    param argu8; // bias
    param argu9; // kernel_type
    param argu10; // actor index
    consumption in1:1;
    consumption in2:1;
    consumption in3:1;
    production out1:1;
  }

  actortype class {
    input in1;
    output out1;
    param name;
    param argu1; // input fifo
    param argu2; // output fifo
    param argu3; // threshold
    param argu4; // actor index
    consumption in1:1;
    production out1:1;
  }

  actortype sink {
    input in1;
    param output_file;
    param name;
    param argu1; // file name
    param argu2; // input FIFO
    param argu3; // actor index
    consumption in1:1;
  }

  actor xsource {
    type : source_double;
    param name = "file_source_double", argu1 = "x_file", argu2 = "fifo_SRC_SVM", argu3 = "actor_index";
    interface out1->SRC_SVM;
  }

  actor svm {
    type : svm;
    param name = "svm", argu1 = "fifo_src_svm", argu2 = "fifo_svm_sink", argu3 = "fifo_sv",
    argu4 = "fifo_alpha" , argu5= "feature_length", argu6= "sv_length", argu7= "gamma",
    argu8= "bias", argu9= "kernel_type", argu10= "actor_index";
    interface SRC_SVM->in1;
    interface SV->in2;
    interface ALPHA->in3;
    interface out1->SVM_SINK;
  }

  actor sink {
    type : sink;
    param name = "file_sink_double", argu1 = "out_file", argu2 = "fifo_SVM_SINK", argu3 = "actor_index";
    interface SVM_SINK->in1;
  }

  actor sv {
    type : source_double;
    param name = "file_source_double", argu1 = "sv_file", argu2 = "fifo_SV", argu3 = "actor_index";
    interface out1->SV;
  }

  actor alpha {
    type : source_double;
    param name = "file_source_double", argu1 = "alpha_file", argu2 = "fifo_ALPHA", argu3 = "actor_index";
    interface out1->ALPHA;
  }
}
