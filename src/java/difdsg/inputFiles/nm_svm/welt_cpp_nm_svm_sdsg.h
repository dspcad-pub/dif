#ifndef _welt_cpp_nm_svm_sdsg_h
#define _welt_cpp_nm_svm_sdsg_h

#include <stdio.h>
#include <stdlib.h>
#include "welt_cpp_basic.h"
#include "welt_cpp_actor.h"
#include "welt_cpp_fifo.h"
#include "welt_cpp_fifo_basic.h"
#include "welt_cpp_fifo_unit_size.h"
#include "welt_cpp_graph.h"
#include "welt_cpp_util.h"

#include "welt_cpp_dsg_actor.h"
#include "welt_cpp_ra_util.h"
#include "welt_cpp_ref_pre_post_functions.h"

#include "welt_cpp_sca_sch_loop.h"
#include "welt_cpp_ref_actor.h"
#include "welt_cpp_sca_static_loop.h"

#include "welt_cpp_dpd_graph.h"

#define DSG_BUFFER_CAPACITY 1024

#define DSG_ACTOR_LAE	0
#define DSG_ACTOR_RSRC	1
#define DSG_ACTOR_LOOP1	2
#define DSG_ACTOR_RSVM	3
#define DSG_ACTOR_RCLASS	4
#define DSG_ACTOR_RSINK	5

#define DSG_FIFO_E1	0
#define DSG_FIFO_E4	1
#define DSG_FIFO_E5	2
#define DSG_FIFO_E0	3
#define DSG_FIFO_E2	4
#define DSG_FIFO_E3	5
#define DSG_FIFO_E6	6

#define DSG_ACTOR_COUNT	6
#define DSG_FIFO_COUNT	7

struct _welt_cpp_nm_svm_sdsg_context_struct;
typedef struct _welt_cpp_nm_svm_sdsg_context_struct 
    welt_cpp_nm_svm_sdsg_context_type;

welt_cpp_nm_svm_sdsg_context_type *welt_cpp_nm_svm_sdsg_new();

void welt_cpp_nm_svm_sdsg_terminate(welt_cpp_nm_svm_sdsg_context_type *graph);

void welt_cpp_nm_svm_sdsg_scheduler(welt_cpp_nm_svm_sdsg_context_type *graph);

#endif
