sdf dpts_sdsg {
  topology {
    nodes = Sch,Sl1,Rx,Sl2,Rfx,Rfy,Rconc,Sl3,Rsvm,Ralffs,Rclass,Rvoting,Rsnk;
    edges = e0 (Sch,Sl1),
            e1 (Sl1,Rx),
            e2 (Rx, Sl1),
            e3 (Sl1, Sl2),
            e4 (Sl2,Rfx),
            e5 (Rfx, Rfy),
            e6 (Rfy, Rconc),
            e7 (Rconc, Sl2),
            e8 (Sl2, Sl3),
            e9 (Sl3, Rsvm),
            e10 (Rsvm, Sl3),
            e11 (Sl3, Ralffs),
            e12 (Ralffs, Rclass),
            e13 (Rclass, Rvoting),
            e14 (Rvoting, Rsnk),
            e15 (Rsnk, Sch);
  }

  attribute edgeType {
     e0 = "int"; e1 = "int"; e2 = "int"; e3 = "int"; e4 = "int"; e5 = "int";
     e6 = "int"; e7 = "int"; e8 = "int"; e9 = "int"; e10 = "int";
     e11 = "int"; e12 = "int"; e13 = "int"; e14 = "int"; e15 = "int";
  }

  attribute capacityValue {
     e0 = 1; e1 = 1; e2 = 1; e3 = 1; e4 = 1; e5 = 1;
     e6 = 1; e7 = 1; e8 = 1; e9 = 1; e10 = 1;
     e11 = 1; e12 = 1; e13 = 1; e14 = 1; e15 = 1;
  }

  parameter {
      graphArguType1 = "int"; graphArguType2 = "int";
      graphArguType3 = "int"; graphArguType4 = "int";
      graphArguVar1 = "data_length"; graphArguVar2 = "feature_length";
      graphArguVar3 = "window_number"; graphArguVar4 = "iter";
    }


  actortype schloop {
    input in1;
    output out1;
    param name;
    param iterNum;
    param argu1; // input fifo in1
    param argu2; // output fifo out1
    param argu3; // iteration count
    param argu4; // actor index
    production out1:1;
    consumption in1:1;
  }

  actortype dynloop {
    input in1, in2;
    output out1, out2;
    param name;
    param argu1; // input fifo in1
    param argu2; // input fifo in2
    param argu3; // output fifo out1
    param argu4; // output fifo out2
    param argu5; // actor index
    production out1:1, out2:1;
    consumption in1:1, in2:1;
  }

  actortype staticloop {
    input in1, in2;
    output out1, out2;
    param name;
    param iterNum;
    param argu1; // input fifo in1
    param argu2; // input fifo in2
    param argu3; // output fifo out1
    param argu4; // output fifo out2
    param argu5; // iteration count
    param argu6; // actor index
    production out1:1, out2:1;
    consumption in1:1, in2:1;
  }


  actortype ra {
    input in1;
    output out1;
    param name;
    param refid;
    param ref_in_num;
    param ref_in_array;
    param ref_out_num;
    param ref_out_array;
    //param pre_ref;
    //param post_ref;
    //param exe_ref;
    param argu1; //input fifo
    param argu2; //output fifo
    param argu3; //actor index
    param argu4; //ref-ed actor pointer
    param argu5; //selected input count
    param argu6; //selected output count
    param argu7; //pre function
    param argu8; //post function
    param argu9; //exe function
    production out1:1;
    consumption in1:1;
  }


  actor Sch {
    type : schloop;
    param name = "sca_sch_loop", iterNum = "1", argu1 = "fifo_e15", argu2 = "fifo_e0", argu3 = "1", argu4 = "actor_index";
    interface e15->in1, out1->e0;
  }

  actor Sl1 {
    type : staticloop;
    param name = "sca_static_loop", iterNum = "data_length*2", argu1 = "fifo_e0", argu2 = "fifo_e2", argu3 = "fifo_e1", argu4 = "fifo_e3", argu5 = "data_length*2", argu6 = "actor_index";
    interface e0->in1, e2->in2, out1->e1, out2->e3;
  }

  actor Rx {
    type : ra;
    param name = "ref_actor", refid = "0", argu1 = "fifo_e1", argu2 = "fifo_e2", argu3 = "actor_index", argu4 = "ref_xsource", argu5 = "0", argu6 = "0", argu7 = "NULL", argu8 = "NULL", argu9 = "NULL";
    interface e1->in1, out1->e2;
  }

  actor Sl2 {
    type : staticloop;
    param name = "sca_static_loop", iterNum = "window_number", argu1 = "fifo_e3", argu2 = "fifo_e7", argu3 = "fifo_e4", argu4 = "fifo_e8", argu5 = "window_number", argu6 = "actor_index";
    interface e3->in1, e7->in2, out1->e4, out2->e8;
  }

  actor Rfx {
    type : ra;
    param name = "ref_actor", refid = "1", argu1 = "fifo_e4", argu2 = "fifo_e5", argu3 = "actor_index", argu4 = "ref_fetx", argu5 = "0", argu6 = "0", argu7 = "NULL", argu8 = "NULL", argu9 = "NULL";
    interface e4->in1, out1->e5;
  }

  actor Rfy {
    type : ra;
    param name = "ref_actor", refid = "2", argu1 = "fifo_e5", argu2 = "fifo_e6", argu3 = "actor_index", argu4 = "ref_fety", argu5 = "0", argu6 = "0", argu7 = "NULL", argu8 = "NULL", argu9 = "NULL";
    interface e5->in1, out1->e6;
  }

  actor Rconc {
    type : ra;
    param name = "ref_actor", refid = "3", argu1 = "fifo_e6", argu2 = "fifo_e7", argu3 = "actor_index", argu4 = "ref_fet_conc", argu5 = "0", argu6 = "0", argu7 = "NULL", argu8 = "NULL", argu9 = "NULL";
    interface e6->in1, out1->e7;
  }

  actor Sl3 {
    type : staticloop;
    param name = "sca_static_loop", iterNum = "window_number*2", argu1 = "fifo_e8", argu2 = "fifo_e10", argu3 = "fifo_e9", argu4 = "fifo_e11", argu5 = "window_number*2", argu6 = "actor_index";
    interface e8->in1, e10->in2, out1->e9, out2->e11;
  }

  actor Rsvm {
    type : ra;
    param name = "ref_actor", refid = "4", argu1 = "fifo_e9", argu2 = "fifo_e10", argu3 = "actor_index", argu4 = "ref_svm", argu5 = "0", argu6 = "0", argu7 = "NULL", argu8 = "NULL", argu9 = "NULL";
    interface e9->in1, out1->e10;
  }

  actor Ralffs {
    type : ra;
    param name = "ref_actor", refid = "5", argu1 = "fifo_e11", argu2 = "fifo_e12", argu3 = "actor_index", argu4 = "ref_alffs", argu5 = "0", argu6 = "0", argu7 = "NULL", argu8 = "NULL", argu9 = "NULL";
    interface e11->in1, out1->e12;
  }

  actor Rclass {
    type : ra;
    param name = "ref_actor", refid = "6", argu1 = "fifo_e12", argu2 = "fifo_e13", argu3 = "actor_index", argu4 = "ref_class", argu5 = "0", argu6 = "0", argu7 = "NULL", argu8 = "NULL", argu9 = "NULL";
    interface e12->in1, out1->e13;
  }

  actor Rvoting {
    type : ra;
    param name = "ref_actor", refid = "7", argu1 = "fifo_e13", argu2 = "fifo_e14", argu3 = "actor_index", argu4 = "ref_voting", argu5 = "0", argu6 = "0", argu7 = "NULL", argu8 = "NULL", argu9 = "NULL";
    interface e13->in1, out1->e14;
  }

  actor Rsnk {
    type : ra;
    param name = "ref_actor", refid = "8", argu1 = "fifo_e14", argu2 = "fifo_e15", argu3 = "actor_index", argu4 = "ref_sink", argu5 = "0", argu6 = "0", argu7 = "NULL", argu8 = "NULL", argu9 = "NULL";
    interface e14->in1, out1->e15;
  }

}
