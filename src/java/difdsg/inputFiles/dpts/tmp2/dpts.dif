/****************************************************************************
Copyright (c) 1999-2018
Maryland DSPCAD Research Group, The University of Maryland at College Park
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
******************************************************************************/
/*
@author Kyunghun Lee
*/

sdf dpts {
  topology {
    nodes = xsource,fetx,fety,fet_conc,svm,alffs,class,voting,sink,svm1,svm2,svm3,alpha1,alpha2,alpha3;
    edges = SRC_FETX (xsource, fetx),
            SRC_FETY (xsource, fety),
            FETX_CON (fetx, fet_conc),
            FETY_CON (fety, fet_conc),
            CON_SVM (fet_conc, svm),
            SVM_ALFFS (svm, alffs),
            ALFFS_CLASS (alffs, class),
            CLASS_VOTING (class, voting),
            VOTING_SNK (voting, sink),
            SVM1 (svm1, svm),
            SVM2 (svm2, svm),
            SVM3 (svm3, svm),
            ALPHA1 (alpha1, svm),
            ALPHA2 (alpha2, svm),
            ALPHA3 (alpha3, svm);
  }


  attribute edgeType {
    SRC_FETX = "double"; SRC_FETY = "double";
    FETX_CON = "double"; FETY_CON = "double";
    CON_SVM = "double"; SVM_ALFFS = "double";
    ALFFS_CLASS = "double"; CLASS_VOTING = "double";
    VOTING_SNK = "double"; SVM1 = "double"; SVM2 = "double"; SVM3 = "double"; ALPHA1 = "double";
    ALPHA2 = "double"; ALPHA3 = "double";
  }

  attribute capacityValue {
    SRC_FETX = 1024; SRC_FETY = 1024;
    FETX_CON = 1024; FETY_CON = 1024;
    CON_SVM = 1024; SVM_ALFFS = 1024;
    ALFFS_CLASS = 1024; CLASS_VOTING = 1024;
    VOTING_SNK = 1024; SVM1 = 1024; SVM2 = 1024; SVM3 = 1024;
    ALPHA1 = 1024; ALPHA2 = 1024; ALPHA3 = 1024;
  }

  parameter {
    graphArguType1 = "char *"; graphArguType2 = "char *"; graphArguType3 = "char *"; graphArguType4 = "char *";
    graphArguType5 = "char *"; graphArguType6 = "char *"; graphArguType7 = "char *"; graphArguType8 = "char *";
    graphArguType9 = "int"; graphArguType10 = "int"; graphArguType11 = "int"; graphArguType12 = "double";
    graphArguType13 = "int"; graphArguType14 = "int"; graphArguType15 = "int"; graphArguType16 = "int";
    graphArguType17 = "int"; graphArguType18 = "double"; graphArguType19 = "double"; graphArguType20 = "double";
    graphArguType21 = "int";
    graphArguVar1 = "x_file"; graphArguVar2 = "out_file"; graphArguVar3 = "svm1_file"; graphArguVar4 = "svm2_file";
    graphArguVar5 = "svm3_file"; graphArguVar6 = "alpha_file1"; graphArguVar7 = "alpha_file2";
    graphArguVar8 = "alpha_file3"; graphArguVar9 = "data_length"; graphArguVar10 = "feature_length";
    graphArguVar11 = "window_number"; graphArguVar12 = "window_overlap"; graphArguVar13 = "nf";
    graphArguVar14 = "sv_one_length"; graphArguVar15 = "sv_two_length"; graphArguVar16 = "sv_three_length";
    graphArguVar17 = "sigma"; graphArguVar18 = "bias1"; graphArguVar19 = "bias2";
    graphArguVar20 = "bias3"; graphArguVar21 = "svm_type";
  }

  actortype source_double {
    output out1;
    param name;
    param argu1; // file name
    param argu2; // output FIFO
    param argu3; // actor index
    production out1:1;
  }

  actortype source_two {
    output out1, out2;
    param name;
    param argu1; // file name
    param argu2; // output FIFO1
    param argu3; // output FIFO2
    param argu4; // actor index
    consumption in1:1;
    production out1:1, out2:1;
  }

  actortype feature {
    output out1;
    input in1;
    param name;
    param argu1; // input FIFO1
    param argu2; // output FIFO1
    param argu3; // data_length
    param argu4; // feature_length
    param argu5; // window_number
    param argu6; // window_overlap
    param argu7; // nf
    param argu8; // actor index
    consumption in1:1;
    production out1:1;
  }

  actortype fet_conc {
    output out1;
    input in1, in2;
    param name;
    param argu1; // input FIFO1
    param argu2; // input FIFO2
    param argu3; // output FIFO
    param argu4; // feature_length
    param argu5; // actor index
    consumption in1:1, in2:1;
    production out1:1;
  }

  actortype svm {
    output out1;
    input in1,in2,in3,in4,in5,in6,in7,in8;
    param name;
    param argu1; // input FIFO1
    param argu2; // output FIFO
    param argu3; // SVM1
    param argu4; // SVM2
    param argu5; // SVM3
    param argu6; // ALPHA1
    param argu7; // ALPHA2
    param argu8; // ALPHA3
    param argu9; // feature_length
    param argu10; // sv_one_length
    param argu11; // sv_two_length
    param argu12; // sv_three_length
    param argu13; // sigma
    param argu14; // bias1
    param argu15; // bias2
    param argu16; // bias3
    param argu17; // svm_type
    param argu18; // actor index
    consumption in1:1;
    consumption in2:1;
    consumption in3:1;
    consumption in4:1;
    consumption in5:1;
    consumption in6:1;
    consumption in7:1;
    production out1:1;
  }

  actortype alffs {
    input in1;
    output out1;
    param name;
    param argu1; // input fifo
    param argu2; // output fifo
    param argu3; // window_number
    param argu4; // actor index
    consumption in1:1;
    production out1:1;
  }

  actortype class {
    input in1;
    output out1;
    param name;
    param argu1; // input fifo
    param argu2; // output fifo
    param argu3; // threshold
    param argu4; // actor index
    consumption in1:1;
    production out1:1;
  }

  actortype voting {
    input in1;
    output out1;
    param name;
    param argu1; // input fifo
    param argu2; // output fifo
    param argu3; // block_length
    param argu4; // actor index
    consumption in1:1;
    production out1:1;
  }

  actortype sink {
    input in1;
    param output_file;
    param name;
    param argu1; // file name
    param argu2; // input FIFO
    param argu3; // actor index
    consumption in1:1;
  }

  actor xsource {
    type : source_two;
    param name = "file_source_two", argu1 = "x_file", argu2 = "fifo_SRC_FETX", argu3 = "fifo_SRC_FETY", argu4 = "actor_index";
    interface out1->SRC_FETX;
    interface out2->SRC_FETY;
  }

  actor fetx {
    type : feature;
    param name = "feature", argu1 = "fifo_SRC_FETX", argu2 = "fifo_FETX_CON", argu3 = "data_length",
    argu4 = "feature_length", argu5 = "window_number", argu6 = "window_overlap", argu7 = "nf",
    argu8 = "actor_index";
    interface SRC_FETX->in1;
    interface out1->FETX_CON;
  }

   actor fety {
      type : feature;
      param name = "feature", argu1 = "fifo_SRC_FETY", argu2 = "fifo_FETY_CON", argu3 = "data_length",
      argu4 = "feature_length", argu5 = "window_number", argu6 = "window_overlap", argu7 = "nf",
      argu8 = "actor_index";
      interface SRC_FETY->in1;
      interface out1->FETY_CON;
   }

  actor fet_conc {
    type : fet_conc;
    param name = "fet_conc", argu1 = "fifo_FETX_CON", argu2 = "fifo_FETY_CON", argu3 = "fifo_CON_SVM",
    argu4 = "feature_length", argu5 = "actor_index";
    interface FETX_CON->in1;
    interface FETY_CON->in2;
    interface out1->CON_SVM;
  }

  actor svm {
    type : svm;
    param name = "svm", argu1 = "fifo_con_svm", argu2 = "fifo_svm_alffs", argu3 = "fifo_svm1",
    argu4 = "fifo_svm2", argu5 = "fifo_svm3" , argu6 = "fifo_alpha1" , argu7 = "fifo_alpha2",
    argu8 = "fifo_alpha3", argu9= "feature_length*2", argu10= "sv_one_length", argu11= "sv_two_length",
    argu12= "sv_three_length", argu13= "sigma", argu14= "bias1", argu15= "bias2",
    argu16= "bias3", argu17= "svm_type", argu18= "actor_index";
    interface CON_SVM->in1;
    interface SVM1->in2;
    interface SVM2->in3;
    interface SVM3->in4;
    interface ALPHA1->in5;
    interface ALPHA2->in6;
    interface ALPHA3->in7;
    interface out1->SVM_ALFFS;
  }

  actor alffs {
    type : alffs;
    param name = "alffs", argu1 = "fifo_SVM_ALFFS", argu2 = "fifo_ALFFS_CLASS", argu3 = "window_number",
    argu4 = "actor_index";
    interface SVM_ALFFS->in1;
    interface out1->ALFFS_CLASS;
  }

  actor class {
    type : class;
    param name = "class", argu1 = "fifo_ALFFS_CLASS", argu2 = "fifo_CLASS_VOTING", argu3 = "0", argu4 = "actor_index";
    interface ALFFS_CLASS->in1;
    interface out1->CLASS_VOTING;
  }

  actor voting {
    type : voting;
    param name = "class", argu1 = "fifo_CLASS_VOTING", argu2 = "fifo_VOTING_SNK", argu3 = "3", argu4 = "actor_index";
    interface CLASS_VOTING->in1;
    interface out1->VOTING_SNK;
  }

  actortype sink {
    input in1;
    param output_file;
    param name;
    param argu1; // file name
    param argu2; // input FIFO
    param argu3; // actor index
    consumption in1:1;
  }

  actor sink {
    type : sink;
    param name = "file_sink_double", argu1 = "out_file", argu2 = "fifo_VOTING_SNK", argu3 = "actor_index";
    interface VOTING_SNK->in1;
  }

  actor svm1 {
    type : source_double;
    param name = "file_source_double", argu1 = "out_file", argu2 = "fifo_SVM1", argu3 = "actor_index";
    interface out1->SVM1;
  }

  actor svm2 {
    type : source_double;
    param name = "file_source_double", argu1 = "out_file", argu2 = "fifo_SVM2", argu3 = "actor_index";
    interface out1->SVM2;
  }

  actor svm3 {
    type : source_double;
    param name = "file_source_double", argu1 = "out_file", argu2 = "fifo_SVM3", argu3 = "actor_index";
    interface out1->SVM3;
  }

  actor alpha1 {
    type : source_double;
    param name = "file_source_double", argu1 = "out_file", argu2 = "fifo_ALPHA1", argu3 = "actor_index";
    interface out1->ALPHA1;
  }

  actor alpha2 {
    type : source_double;
    param name = "file_source_double", argu1 = "out_file", argu2 = "fifo_ALPHA2", argu3 = "actor_index";
    interface out1->ALPHA2;
  }

  actor alpha3 {
    type : source_double;
    param name = "file_source_double", argu1 = "out_file", argu2 = "fifo_ALPHA3", argu3 = "actor_index";
    interface out1->ALPHA3;
  }

}
