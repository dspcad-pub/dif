#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lide_c_basic.h"
#include "lide_c_actor.h"
#include "lide_c_add_graph.h"


struct _lide_c_add_graph_context_struct {
#include "lide_c_graph_context_type_common.h"
    char *x_file;
    char *y_file;
    char *out_file;
};

lide_c_add_graph_context_type *lide_c_add_graph_new(char * x, char * y, char *
    out){
    int token_size;
    lide_c_add_graph_context_type * context = NULL;
    context = (lide_c_add_graph_context_type *)lide_c_util_malloc(sizeof(
        lide_c_add_graph_context_type));
    context->actor_count = ACTOR_COUNT;
    context->fifo_count = FIFO_COUNT;

    context->x_file="x.txt";
    context->y_file="y.txt";
    context->out_file="out.txt";

    context->actors = (lide_c_actor_context_type **)lide_c_util_malloc(
        context->actor_count * sizeof(lide_c_actor_context_type *));
    context->fifos = (lide_c_fifo_pointer *)lide_c_util_malloc(
        context->fifo_count * sizeof(lide_c_fifo_pointer));

    context->descriptors = (char **)lide_c_util_malloc(context->actor_count * 
        sizeof(char*));
    lide_c_graph_set_actor_desc((lide_c_graph_context_type *)context, ACTOR_X, 
        "x");
    lide_c_graph_set_actor_desc((lide_c_graph_context_type *)context, ACTOR_Y, 
        "y");
    lide_c_graph_set_actor_desc((lide_c_graph_context_type *)context, ACTOR_A, 
        "a");
    lide_c_graph_set_actor_desc((lide_c_graph_context_type *)context, 
        ACTOR_OUT, "out");

    context->source_array = (lide_c_actor_context_type **)lide_c_util_malloc(
        context->fifo_count * sizeof(lide_c_actor_context_type *));

    context->sink_array = (lide_c_actor_context_type **)lide_c_util_malloc(
        context->fifo_count * sizeof(lide_c_actor_context_type *));

    token_size = sizeof(float);
    context->fifos[FIFO_E1] = (lide_c_fifo_pointer)lide_c_fifo_basic_new(1024, 
        token_size, FIFO_E1);
    token_size = sizeof(float);
    context->fifos[FIFO_E2] = (lide_c_fifo_pointer)lide_c_fifo_basic_new(1024, 
        token_size, FIFO_E2);
    token_size = sizeof(float);
    context->fifos[FIFO_E3] = (lide_c_fifo_pointer)lide_c_fifo_basic_new(1024, 
        token_size, FIFO_E3);

    context->actors[ACTOR_X] = (lide_c_actor_context_type *)
        (lide_c_file_source_new(x, context->fifos[FIFO_E1], ACTOR_X));
    lide_c_file_source_connect(context->actors[ACTOR_X], 
        (lide_c_graph_context_type *)context);

    context->actors[ACTOR_Y] = (lide_c_actor_context_type *)
        (lide_c_file_source_new(y, context->fifos[FIFO_E2], ACTOR_Y));
    lide_c_file_source_connect(context->actors[ACTOR_Y], 
        (lide_c_graph_context_type *)context);

    context->actors[ACTOR_A] = (lide_c_actor_context_type *)(lide_c_add_new(
        context->fifos[FIFO_E1], context->fifos[FIFO_E2],
        context->fifos[FIFO_E3], ACTOR_A));
    lide_c_add_connect(context->actors[ACTOR_A], (lide_c_graph_context_type
        *)context);

    context->actors[ACTOR_OUT] = (lide_c_actor_context_type *)
        (lide_c_file_sink_new(out, context->fifos[FIFO_E3], ACTOR_OUT));
    lide_c_file_sink_connect(context->actors[ACTOR_OUT], 
        (lide_c_graph_context_type *)context);

    context->scheduler = (lide_c_graph_scheduler_ftype)
        lide_c_add_graph_scheduler;
    return context;
}

void lide_c_add_graph_terminate(lide_c_add_graph_context_type *context){
    int i;
    for(i = 0; i < context->fifo_count; i++){
        lide_c_fifo_basic_free((lide_c_fifo_basic_pointer)context->fifos[i]);
    }

    lide_c_file_source_terminate((lide_c_file_source_context_type *)
        context->actors[ACTOR_X]);
    lide_c_file_source_terminate((lide_c_file_source_context_type *)
        context->actors[ACTOR_Y]);
    lide_c_add_terminate((lide_c_add_context_type *)context->actors[ACTOR_A]);
    lide_c_file_sink_terminate((lide_c_file_sink_context_type *)
        context->actors[ACTOR_OUT]);

    free(context->fifos);
    free(context->actors);
    free(context->descriptors);
    free(context);
}
void lide_c_add_graph_scheduler(lide_c_add_graph_context_type *context){
    lide_c_util_simple_scheduler(context->actors, context->actor_count, 
        context->descriptors);
    return;
}
