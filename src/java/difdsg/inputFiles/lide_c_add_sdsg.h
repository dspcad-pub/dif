#ifndef _lide_c_add_sdsg_h
#define _lide_c_add_sdsg_h

#include <stdio.h>
#include <stdlib.h>
#include "lide_c_basic.h"
#include "lide_c_actor.h"
#include "lide_c_fifo.h"
#include "lide_c_fifo_basic.h"
#include "lide_c_fifo_unit_size.h"
#include "lide_c_graph.h"
#include "lide_c_util.h"

#include "lide_c_dsg_actor.h"
#include "lide_c_ra_util.h"
#include "lide_c_ref_pre_post_functions.h"

#include "lide_c_ref_actor.h"
#include "lide_c_sca_sch_loop.h"

#include "lide_c_add_graph.h"

#define DSG_BUFFER_CAPACITY 1024

#define DSG_ACTOR_ROUT	0
#define DSG_ACTOR_RA	1
#define DSG_ACTOR_RX	2
#define DSG_ACTOR_RY	3
#define DSG_ACTOR_STARTLOOP	4

#define DSG_FIFO_E4	0
#define DSG_FIFO_E3	1
#define DSG_FIFO_E5	2
#define DSG_FIFO_E1	3
#define DSG_FIFO_E2	4

#define DSG_ACTOR_COUNT	5
#define DSG_FIFO_COUNT	5

struct _lide_c_add_sdsg_context_struct;
typedef struct _lide_c_add_sdsg_context_struct lide_c_add_sdsg_context_type;

lide_c_add_sdsg_context_type *lide_c_add_sdsg_new();

void lide_c_add_sdsg_terminate(lide_c_add_sdsg_context_type *graph);

void lide_c_add_sdsg_scheduler(lide_c_add_sdsg_context_type *graph);

#endif
