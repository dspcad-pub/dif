#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lide_c_basic.h"
#include "lide_c_actor.h"
#include "lide_c_add_sdsg.h"


struct _lide_c_add_sdsg_context_struct {
#include "lide_c_graph_context_type_common.h"
};

lide_c_add_sdsg_context_type *lide_c_add_sdsg_new(lide_c_graph_context_type *
    contextApp, int iter){
    int token_size;
    lide_c_add_sdsg_context_type * context = NULL;
    context = (lide_c_add_sdsg_context_type *)lide_c_util_malloc(sizeof(
        lide_c_add_sdsg_context_type));
    context->actor_count = DSG_ACTOR_COUNT;
    context->fifo_count = DSG_FIFO_COUNT;


    context->actors = (lide_c_actor_context_type **)lide_c_util_malloc(
        context->actor_count * sizeof(lide_c_actor_context_type *));
    context->fifos = (lide_c_fifo_pointer *)lide_c_util_malloc(
        context->fifo_count * sizeof(lide_c_fifo_pointer));

    context->descriptors = (char **)lide_c_util_malloc(context->actor_count * 
        sizeof(char*));
    lide_c_graph_set_actor_desc((lide_c_graph_context_type *)context, 
        DSG_ACTOR_ROUT, "rout");
    lide_c_graph_set_actor_desc((lide_c_graph_context_type *)context, 
        DSG_ACTOR_RA, "ra");
    lide_c_graph_set_actor_desc((lide_c_graph_context_type *)context, 
        DSG_ACTOR_RX, "rx");
    lide_c_graph_set_actor_desc((lide_c_graph_context_type *)context, 
        DSG_ACTOR_RY, "ry");
    lide_c_graph_set_actor_desc((lide_c_graph_context_type *)context, 
        DSG_ACTOR_STARTLOOP, "startloop");

    context->source_array = (lide_c_actor_context_type **)lide_c_util_malloc(
        context->fifo_count * sizeof(lide_c_actor_context_type *));

    context->sink_array = (lide_c_actor_context_type **)lide_c_util_malloc(
        context->fifo_count * sizeof(lide_c_actor_context_type *));

    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E4] = (lide_c_fifo_pointer)
        lide_c_fifo_unit_size_new(token_size, DSG_FIFO_E4);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E3] = (lide_c_fifo_pointer)
        lide_c_fifo_unit_size_new(token_size, DSG_FIFO_E3);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E5] = (lide_c_fifo_pointer)
        lide_c_fifo_unit_size_new(token_size, DSG_FIFO_E5);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E1] = (lide_c_fifo_pointer)
        lide_c_fifo_unit_size_new(token_size, DSG_FIFO_E1);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E2] = (lide_c_fifo_pointer)
        lide_c_fifo_unit_size_new(token_size, DSG_FIFO_E2);

    context->actors[DSG_ACTOR_ROUT] = (lide_c_actor_context_type *)
        (lide_c_ref_actor_new(context->fifos[DSG_FIFO_E4],
        context->fifos[DSG_FIFO_E5], DSG_ACTOR_ROUT,
        contextApp->actors[ACTOR_OUT], 0, 0, NULL, NULL, NULL));
    lide_c_ref_actor_connect(context->actors[DSG_ACTOR_ROUT], 
        (lide_c_graph_context_type *)context);

    context->actors[DSG_ACTOR_RA] = (lide_c_actor_context_type *)
        (lide_c_ref_actor_new(context->fifos[DSG_FIFO_E3],
        context->fifos[DSG_FIFO_E4], DSG_ACTOR_RA, contextApp->actors[ACTOR_A],
        0, 0, NULL, NULL, NULL));
    lide_c_ref_actor_connect(context->actors[DSG_ACTOR_RA], 
        (lide_c_graph_context_type *)context);

    context->actors[DSG_ACTOR_RX] = (lide_c_actor_context_type *)
        (lide_c_ref_actor_new(context->fifos[DSG_FIFO_E1],
        context->fifos[DSG_FIFO_E2], DSG_ACTOR_RX, contextApp->actors[ACTOR_X],
        0, 0, NULL, NULL, NULL));
    lide_c_ref_actor_connect(context->actors[DSG_ACTOR_RX], 
        (lide_c_graph_context_type *)context);

    context->actors[DSG_ACTOR_RY] = (lide_c_actor_context_type *)
        (lide_c_ref_actor_new(context->fifos[DSG_FIFO_E2],
        context->fifos[DSG_FIFO_E3], DSG_ACTOR_RY, contextApp->actors[ACTOR_Y],
        0, 0, NULL, NULL, NULL));
    lide_c_ref_actor_connect(context->actors[DSG_ACTOR_RY], 
        (lide_c_graph_context_type *)context);

    context->actors[DSG_ACTOR_STARTLOOP] = (lide_c_actor_context_type *)
        (lide_c_sca_sch_loop_new(context->fifos[DSG_FIFO_E5],
        context->fifos[DSG_FIFO_E1], 1, DSG_ACTOR_STARTLOOP));
    lide_c_sca_sch_loop_connect(context->actors[DSG_ACTOR_STARTLOOP], 
        (lide_c_graph_context_type *)context);

    context->scheduler = (lide_c_graph_scheduler_ftype)
        lide_c_add_sdsg_scheduler;
    return context;
}

void lide_c_add_sdsg_terminate(lide_c_add_sdsg_context_type *context){
    int i;
    for(i = 0; i < context->fifo_count; i++){
        lide_c_fifo_basic_free((lide_c_fifo_basic_pointer)context->fifos[i]);
    }

    lide_c_ref_actor_terminate((lide_c_ref_actor_context_type *)
        context->actors[DSG_ACTOR_ROUT]);
    lide_c_ref_actor_terminate((lide_c_ref_actor_context_type *)
        context->actors[DSG_ACTOR_RA]);
    lide_c_ref_actor_terminate((lide_c_ref_actor_context_type *)
        context->actors[DSG_ACTOR_RX]);
    lide_c_ref_actor_terminate((lide_c_ref_actor_context_type *)
        context->actors[DSG_ACTOR_RY]);
    lide_c_sca_sch_loop_terminate((lide_c_sca_sch_loop_context_type *)
        context->actors[DSG_ACTOR_STARTLOOP]);

    free(context->fifos);
    free(context->actors);
    free(context->descriptors);
    free(context);
}
void lide_c_add_sdsg_scheduler(lide_c_add_sdsg_context_type *context){
    lide_c_util_simple_scheduler(context->actors, context->actor_count, 
        context->descriptors);
    return;
}
