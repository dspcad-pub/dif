#ifndef _dpd_sdsg_h
#define _dpd_sdsg_h

#include <stdio.h>
#include <stdlib.h>
extern "C"{
#include "welt_c_util.h"
#include "welt_c_fifo.h"
#include "welt_c_fifo_unit_size.h"
}

#include "welt_cpp_graph.h"
#include "welt_cpp_dsg.h"

#include "welt_cpp_ref_actor.h"
#include "welt_cpp_sca_sch_loop.h"
#include "welt_cpp_sca_static_loop.h"

#include "dpd_graph.h"

#define DSG_BUFFER_CAPACITY 1024

#define DSG_ACTOR_SCONFIGS	16
#define DSG_ACTOR_RBNORM	12
#define DSG_ACTOR_RCONJ	10
#define DSG_ACTOR_SPFA	23
#define DSG_ACTOR_SCH	0
#define DSG_ACTOR_RACC	32
#define DSG_ACTOR_RFILT2	26
#define DSG_ACTOR_RFCD	20
#define DSG_ACTOR_SDISTRIB	18
#define DSG_ACTOR_SPAIN	21
#define DSG_ACTOR_RPOLY	24
#define DSG_ACTOR_RBCONJ	13
#define DSG_ACTOR_RFILT5	29
#define DSG_ACTOR_SSINKF	33
#define DSG_ACTOR_RSF	8
#define DSG_ACTOR_SSRC	1
#define DSG_ACTOR_RFILT4	28
#define DSG_ACTOR_RPAIN	22
#define DSG_ACTOR_RCONFIG	3
#define DSG_ACTOR_RPO	7
#define DSG_ACTOR_RPDF	19
#define DSG_ACTOR_RSRC	2
#define DSG_ACTOR_RFILT1	25
#define DSG_ACTOR_SPA	5
#define DSG_ACTOR_SES	14
#define DSG_ACTOR_RPI	6
#define DSG_ACTOR_RES	15
#define DSG_ACTOR_RFILT3	27
#define DSG_ACTOR_RPDE	4
#define DSG_ACTOR_RCONFIGS	17
#define DSG_ACTOR_RSINKF	34
#define DSG_ACTOR_SCONJ	9
#define DSG_ACTOR_RFILT7	31
#define DSG_ACTOR_RFILT6	30
#define DSG_ACTOR_SBASIS	11

#define DSG_FIFO_E1	1
#define DSG_FIFO_E37	37
#define DSG_FIFO_E7	7
#define DSG_FIFO_E17	17
#define DSG_FIFO_E31	31
#define DSG_FIFO_E0	0
#define DSG_FIFO_E30	30
#define DSG_FIFO_E28	28
#define DSG_FIFO_E32	32
#define DSG_FIFO_E44	44
#define DSG_FIFO_E43	43
#define DSG_FIFO_E42	42
#define DSG_FIFO_E6	6
#define DSG_FIFO_E9	9
#define DSG_FIFO_E29	29
#define DSG_FIFO_E23	23
#define DSG_FIFO_E24	24
#define DSG_FIFO_E35	35
#define DSG_FIFO_E38	38
#define DSG_FIFO_E14	14
#define DSG_FIFO_E18	18
#define DSG_FIFO_E19	19
#define DSG_FIFO_E2	2
#define DSG_FIFO_E8	8
#define DSG_FIFO_E13	13
#define DSG_FIFO_E20	20
#define DSG_FIFO_E22	22
#define DSG_FIFO_E41	41
#define DSG_FIFO_E39	39
#define DSG_FIFO_E34	34
#define DSG_FIFO_E21	21
#define DSG_FIFO_E5	5
#define DSG_FIFO_E25	25
#define DSG_FIFO_E36	36
#define DSG_FIFO_E26	26
#define DSG_FIFO_E16	16
#define DSG_FIFO_E33	33
#define DSG_FIFO_E3	3
#define DSG_FIFO_E12	12
#define DSG_FIFO_E15	15
#define DSG_FIFO_E4	4
#define DSG_FIFO_E10	10
#define DSG_FIFO_E40	40
#define DSG_FIFO_E11	11
#define DSG_FIFO_E27	27

#define DSG_ACTOR_COUNT	35
#define DSG_FIFO_COUNT	45
#define NUM_THREAD	(1)

class dpd_sdsg: public welt_cpp_dsg{
public:
dpd_sdsg(welt_cpp_graph*app_graph, int *target);

void dsg_scheduler(int thread_ind, int thread_num);
int thread_ind = 0;
void
    scheduler(); 



};
#endif
