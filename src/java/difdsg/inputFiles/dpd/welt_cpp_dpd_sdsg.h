#ifndef _welt_cpp_dpd_sdsg_h
#define _welt_cpp_dpd_sdsg_h

#include <stdio.h>
#include <stdlib.h>
extern "C"{
#include " welt_c_util.h"
#include " welt_c_fifo.h"
}

#include "welt_cpp_graph.h"
#include "welt_cpp_dsg.h"

#include "welt_cpp_ref_actor.h"
#include "welt_cpp_sca_sch_loop.h"
#include "welt_cpp_sca_static_loop.h"

#include "welt_cpp_dpd_graph.h"

#define DSG_BUFFER_CAPACITY 1024

#define DSG_ACTOR_SCONFIGS	0
#define DSG_ACTOR_RBNORM	1
#define DSG_ACTOR_RCONJ	2
#define DSG_ACTOR_SPFA	3
#define DSG_ACTOR_SCH	4
#define DSG_ACTOR_RACC	5
#define DSG_ACTOR_RFILT2	6
#define DSG_ACTOR_RFCD	7
#define DSG_ACTOR_SDISTRIB	8
#define DSG_ACTOR_SPAIN	9
#define DSG_ACTOR_RPOLY	10
#define DSG_ACTOR_RBCONJ	11
#define DSG_ACTOR_RFILT5	12
#define DSG_ACTOR_SSINKF	13
#define DSG_ACTOR_RSF	14
#define DSG_ACTOR_SSRC	15
#define DSG_ACTOR_RFILT4	16
#define DSG_ACTOR_RPAIN	17
#define DSG_ACTOR_RCONFIG	18
#define DSG_ACTOR_RPO	19
#define DSG_ACTOR_RPDF	20
#define DSG_ACTOR_RSRC	21
#define DSG_ACTOR_RFILT1	22
#define DSG_ACTOR_SPA	23
#define DSG_ACTOR_SES	24
#define DSG_ACTOR_RPI	25
#define DSG_ACTOR_RES	26
#define DSG_ACTOR_RFILT3	27
#define DSG_ACTOR_RPDE	28
#define DSG_ACTOR_RCONFIGS	29
#define DSG_ACTOR_RSINKF	30
#define DSG_ACTOR_SCONJ	31
#define DSG_ACTOR_RFILT7	32
#define DSG_ACTOR_RFILT6	33
#define DSG_ACTOR_SBASIS	34

#define DSG_FIFO_E1	0
#define DSG_FIFO_E37	1
#define DSG_FIFO_E7	2
#define DSG_FIFO_E17	3
#define DSG_FIFO_E31	4
#define DSG_FIFO_E0	5
#define DSG_FIFO_E30	6
#define DSG_FIFO_E28	7
#define DSG_FIFO_E32	8
#define DSG_FIFO_E44	9
#define DSG_FIFO_E43	10
#define DSG_FIFO_E42	11
#define DSG_FIFO_E6	12
#define DSG_FIFO_E9	13
#define DSG_FIFO_E29	14
#define DSG_FIFO_E23	15
#define DSG_FIFO_E24	16
#define DSG_FIFO_E35	17
#define DSG_FIFO_E38	18
#define DSG_FIFO_E14	19
#define DSG_FIFO_E18	20
#define DSG_FIFO_E19	21
#define DSG_FIFO_E2	22
#define DSG_FIFO_E8	23
#define DSG_FIFO_E13	24
#define DSG_FIFO_E20	25
#define DSG_FIFO_E22	26
#define DSG_FIFO_E41	27
#define DSG_FIFO_E39	28
#define DSG_FIFO_E34	29
#define DSG_FIFO_E21	30
#define DSG_FIFO_E5	31
#define DSG_FIFO_E25	32
#define DSG_FIFO_E36	33
#define DSG_FIFO_E26	34
#define DSG_FIFO_E16	35
#define DSG_FIFO_E33	36
#define DSG_FIFO_E3	37
#define DSG_FIFO_E12	38
#define DSG_FIFO_E15	39
#define DSG_FIFO_E4	40
#define DSG_FIFO_E10	41
#define DSG_FIFO_E40	42
#define DSG_FIFO_E11	43
#define DSG_FIFO_E27	44

#define DSG_ACTOR_COUNT	35
#define DSG_FIFO_COUNT	45
#define NUM_THREAD	(1)

class dpd_sdsg: public welt_cpp_dsg{
public:
dpd_sdsg(int thread_ind, int thread_num);

void dsg_scheduler(int thread_ind, int thread_num);
void dsg_scheduler(int
    thread_ind, int thread_num);
int thread_ind = 0;void scheduler();

private:

};#endif
