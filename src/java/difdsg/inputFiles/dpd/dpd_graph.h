#ifndef _dpd_graph_h
#define _dpd_graph_h

#include <iostream>
#include "welt_cpp_graph.h"
#include "welt_cpp_util.h"
#include "file_sink_float.h"
#include "param_distrib_flt.h"
#include "split_fifo.h"
#include "file_source_int.h"
#include "config_sink_float.h"
#include "dpd_filter.h"
#include "file_source_float.h"
#include "param_distrib_est.h"
#include "conjugate.h"
#include "flt_coeff_distrib.h"
#include "config_sink_int.h"
#include "polynomial.h"
#include "estimate.h"
#include "accumulator.h"
#include "basis_matrix.h"

#define BUFFER_CAPACITY 65536*4

#define ACTOR_CONFIG_SINK_FLOAT	10
#define ACTOR_ACC	15
#define ACTOR_FILTER_4	20
#define ACTOR_FILTER_3	19
#define ACTOR_FILE_SINK	23
#define ACTOR_ESTIMATE	9
#define ACTOR_FILE_SOURCE_DATA	13
#define ACTOR_SOURCE	0
#define ACTOR_PA_OUT_SRC	4
#define ACTOR_FILTER_1	17
#define ACTOR_POLY	14
#define ACTOR_PARAM_DISTRIB_EST	2
#define ACTOR_FILTER_2	18
#define ACTOR_PARAM_DISTRIB_FLT	11
#define ACTOR_FLT_COEFF_DISTRIB	12
#define ACTOR_FILTER_0	16
#define ACTOR_BASIS_MATRIX_NORM	6
#define ACTOR_FILTER_6	22
#define ACTOR_BASIS_MATRIX_CONJ	8
#define ACTOR_CONFIG_SINK_INIT_SUBINIT	1
#define ACTOR_SPLIT_FIFO	5
#define ACTOR_PA_IN_SRC	3
#define ACTOR_CONJUGATE	7
#define ACTOR_FILTER_5	21

#define FIFO_BODY_PARAM7	19
#define FIFO_DATA0	4
#define FIFO_BODY_PARAM3	15
#define FIFO_BODY_COEFF0	31
#define FIFO_BODY_DATA11	30
#define FIFO_BODY_DATA7	29
#define FIFO_DATA5	9
#define FIFO_BODY_PARAM1	13
#define FIFO_BODY_DATA0	22
#define FIFO_BODY_OUT1	40
#define FIFO_BODY_OUT3	42
#define FIFO_PARAM1	2
#define FIFO_BODY_PARAM8	20
#define FIFO_BODY_DATA1	23
#define FIFO_BODY_COEFF1	32
#define FIFO_DATA2	6
#define FIFO_BODY_COEFF3	34
#define FIFO_BODY_COEFF5	36
#define FIFO_BODY_COEFF4	35
#define FIFO_SRC_OUT	0
#define FIFO_BODY_PARAM2	14
#define FIFO_BODY_DATA5	27
#define FIFO_BODY_OUT4	43
#define FIFO_BODY_DATA4	26
#define FIFO_DATA1	5
#define FIFO_BODY_PARAM9	21
#define FIFO_BODY_COEFF2	33
#define FIFO_DATA4	8
#define FIFO_BODY_OUT5	44
#define FIFO_BODY_OUT0	39
#define FIFO_DATA6	10
#define FIFO_BODY_COEFF10	38
#define FIFO_BODY_PARAM4	16
#define FIFO_BODY_COEFF6	37
#define FIFO_BODY_OUT2	41
#define FIFO_BODY_DATA2	24
#define FIFO_BODY_DATA6	28
#define FIFO_BODY_PARAM6	18
#define FIFO_PARAM2	3
#define FIFO_PARAM0	1
#define FIFO_BODY_PARAM0	12
#define FIFO_DATA3	7
#define FIFO_FLT_COEFF_0	11
#define FIFO_BODY_DATA3	25
#define FIFO_BODY_PARAM5	17
#define FIFO_BODY_OUT6	45

#define ACTOR_COUNT	24
#define FIFO_COUNT	46

class dpd_graph: public welt_cpp_graph {
public: 
dpd_graph(char* data_filename, char* pa_in_filename, char* pa_out_filename, char* param_filename, char* output_filename, int* param, int param_length, float* coeffs, int coeffs_length);
void scheduler() override;
private: 
char* data_filename; 
char* pa_in_filename; 
char* pa_out_filename; 
char*
    param_filename; 
char* output_filename; 
int* param; 
int param_length;
    
float* coeffs; 
int coeffs_length;    ;}; 


#endif
