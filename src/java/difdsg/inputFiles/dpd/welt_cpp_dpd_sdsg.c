#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "welt_cpp_dsg_scheduler.h"
#include "welt_cpp_actor.h"
#include "welt_cpp_dpd_sdsg.h"


dpd_sdsg::dpd_sdsg (welt_cpp_graph* app_graph, int* target){
    this->dsg_count = DSG_ACTOR_COUNTthis->thread_count = NUM_THREAD
    this -> first_actors_index.reserve(this->thread_count);

    this->set_fifo_count(DSG_FIFO_COUNT);

    this->set_actor_count(DSG_ACTOR_COUNT);

    this->actors.reserve(DSG_ACtOR_COUNT);

    this->fifos.reserve(DSG_FIFO_COUNT);
    this->descriptors.reserve(this->actor_count);
    (this->source_array).reserve(this->fifo_count);
    (this->sink_array).reserve(this->fifo_count);
    for (int i =0; i<DSG_FIFO_COUNT; i++){
    (this->source_array).push_back(nullptr);
    (this->sink_array).push_back(nullptr);
    }


    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E19] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E19);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E39] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E39);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E7] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E7);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E35] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E35);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E30] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E30);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E22] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E22);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E1] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E1);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E3] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E3);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E17] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E17);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E21] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E21);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E41] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E41);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E5] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E5);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E8] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E8);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E9] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E9);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E12] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E12);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E40] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E40);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E16] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E16);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E4] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E4);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E24] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E24);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E26] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E26);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E36] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E36);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E20] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E20);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E23] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E23);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E33] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E33);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E6] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E6);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E0] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E0);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E27] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E27);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E14] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E14);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E18] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E18);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E29] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E29);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E31] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E31);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E43] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E43);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E11] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E11);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E44] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E44);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E15] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E15);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E34] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E34);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E10] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E10);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E38] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E38);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E32] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E32);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E28] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E28);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E2] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E2);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E13] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E13);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E42] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E42);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E25] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E25);
    token_size = sizeof(int);
    context->fifos[DSG_FIFO_E37] = (welt_cpp_fifo_pointer)
        welt_cpp_fifo_unit_size_new(token_size, DSG_FIFO_E37);

    this->actors[DSG_ACTOR_RFILT3] = (welt_cpp_actor *)(welt_cpp_ref_actor(
        context->fifos[DSG_FIFO_E34], context->fifos[DSG_FIFO_E35],
        DSG_ACTOR_RFILT3, contextApp->actors[ACTOR_FILT3], 0, 0, NULL, NULL,
        NULL));
    welt_cpp_ref_actor_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_RFILT3], DSG_ACTOR_RFILT3

    this->actors[DSG_ACTOR_RSRC] = (welt_cpp_actor *)(welt_cpp_ref_actor(
        context->fifos[DSG_FIFO_E1], context->fifos[DSG_FIFO_E2],
        DSG_ACTOR_RSRC, contextApp->actors[ACTOR_SRC], 0, 0, NULL, NULL, NULL));
    welt_cpp_ref_actor_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_RSRC], DSG_ACTOR_RSRC

    this->actors[DSG_ACTOR_SCONJ] = (welt_cpp_actor *)(welt_cpp_sca_static_loop(
        context->fifos[DSG_FIFO_E10], context->fifos[DSG_FIFO_E12],
        context->fifos[DSG_FIFO_E11], context->fifos[DSG_FIFO_E13],
        window_number*2, DSG_ACTOR_SCONJ));
    welt_cpp_sca_static_loop_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_SCONJ], DSG_ACTOR_SCONJ

    this->actors[DSG_ACTOR_SBASIS] = (welt_cpp_actor *)
        (welt_cpp_sca_static_loop(context->fifos[DSG_FIFO_E13],
        context->fifos[DSG_FIFO_E16], context->fifos[DSG_FIFO_E14],
        context->fifos[DSG_FIFO_E17], window_number*2, DSG_ACTOR_SBASIS));
    welt_cpp_sca_static_loop_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_SBASIS], DSG_ACTOR_SBASIS

    this->actors[DSG_ACTOR_RBNORM] = (welt_cpp_actor *)(welt_cpp_ref_actor(
        context->fifos[DSG_FIFO_E14], context->fifos[DSG_FIFO_E15],
        DSG_ACTOR_RBNORM, contextApp->actors[ACTOR_BNORM], 0, 0, NULL, NULL,
        NULL));
    welt_cpp_ref_actor_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_RBNORM], DSG_ACTOR_RBNORM

    this->actors[DSG_ACTOR_SCONFIGS] = (welt_cpp_actor *)
        (welt_cpp_sca_static_loop(context->fifos[DSG_FIFO_E20],
        context->fifos[DSG_FIFO_E22], context->fifos[DSG_FIFO_E21],
        context->fifos[DSG_FIFO_E23], window_number*2, DSG_ACTOR_SCONFIGS));
    welt_cpp_sca_static_loop_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_SCONFIGS], DSG_ACTOR_SCONFIGS

    this->actors[DSG_ACTOR_RACC] = (welt_cpp_actor *)(welt_cpp_ref_actor(
        context->fifos[DSG_FIFO_E39], context->fifos[DSG_FIFO_E40],
        DSG_ACTOR_RACC, contextApp->actors[ACTOR_ACC], 0, 0, NULL, NULL, NULL));
    welt_cpp_ref_actor_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_RACC], DSG_ACTOR_RACC

    this->actors[DSG_ACTOR_SSRC] = (welt_cpp_actor *)(welt_cpp_sca_static_loop(
        context->fifos[DSG_FIFO_E0], context->fifos[DSG_FIFO_E2],
        context->fifos[DSG_FIFO_E1], context->fifos[DSG_FIFO_E3], 100,
        DSG_ACTOR_SSRC));
    welt_cpp_sca_static_loop_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_SSRC], DSG_ACTOR_SSRC

    this->actors[DSG_ACTOR_RFILT7] = (welt_cpp_actor *)(welt_cpp_ref_actor(
        context->fifos[DSG_FIFO_E38], context->fifos[DSG_FIFO_E39],
        DSG_ACTOR_RFILT7, contextApp->actors[ACTOR_FILT7], 0, 0, NULL, NULL,
        NULL));
    welt_cpp_ref_actor_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_RFILT7], DSG_ACTOR_RFILT7

    this->actors[DSG_ACTOR_SSINKF] = (welt_cpp_actor *)
        (welt_cpp_sca_static_loop(context->fifos[DSG_FIFO_E41],
        context->fifos[DSG_FIFO_E43], context->fifos[DSG_FIFO_E42],
        context->fifos[DSG_FIFO_E44], window_number*2, DSG_ACTOR_SSINKF));
    welt_cpp_sca_static_loop_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_SSINKF], DSG_ACTOR_SSINKF

    this->actors[DSG_ACTOR_RFILT2] = (welt_cpp_actor *)(welt_cpp_ref_actor(
        context->fifos[DSG_FIFO_E33], context->fifos[DSG_FIFO_E34],
        DSG_ACTOR_RFILT2, contextApp->actors[ACTOR_FILT2], 0, 0, NULL, NULL,
        NULL));
    welt_cpp_ref_actor_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_RFILT2], DSG_ACTOR_RFILT2

    this->actors[DSG_ACTOR_RES] = (welt_cpp_actor *)(welt_cpp_ref_actor(
        context->fifos[DSG_FIFO_E18], context->fifos[DSG_FIFO_E19],
        DSG_ACTOR_RES, contextApp->actors[ACTOR_ES], 0, 0, NULL, NULL, NULL));
    welt_cpp_ref_actor_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_RES], DSG_ACTOR_RES

    this->actors[DSG_ACTOR_RSINKF] = (welt_cpp_actor *)(welt_cpp_ref_actor(
        context->fifos[DSG_FIFO_E42], context->fifos[DSG_FIFO_E43],
        DSG_ACTOR_RSINKF, contextApp->actors[ACTOR_SINKF], 0, 0, NULL, NULL,
        NULL));
    welt_cpp_ref_actor_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_RSINKF], DSG_ACTOR_RSINKF

    this->actors[DSG_ACTOR_SPFA] = (welt_cpp_actor *)(welt_cpp_sca_static_loop(
        context->fifos[DSG_FIFO_E30], context->fifos[DSG_FIFO_E40],
        context->fifos[DSG_FIFO_E31], context->fifos[DSG_FIFO_E41],
        window_number*2, DSG_ACTOR_SPFA));
    welt_cpp_sca_static_loop_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_SPFA], DSG_ACTOR_SPFA

    this->actors[DSG_ACTOR_RPDE] = (welt_cpp_actor *)(welt_cpp_ref_actor(
        context->fifos[DSG_FIFO_E4], context->fifos[DSG_FIFO_E5],
        DSG_ACTOR_RPDE, contextApp->actors[ACTOR_PDE], 0, 0, NULL, NULL, NULL));
    welt_cpp_ref_actor_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_RPDE], DSG_ACTOR_RPDE

    this->actors[DSG_ACTOR_RFILT5] = (welt_cpp_actor *)(welt_cpp_ref_actor(
        context->fifos[DSG_FIFO_E36], context->fifos[DSG_FIFO_E37],
        DSG_ACTOR_RFILT5, contextApp->actors[ACTOR_FILT5], 0, 0, NULL, NULL,
        NULL));
    welt_cpp_ref_actor_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_RFILT5], DSG_ACTOR_RFILT5

    this->actors[DSG_ACTOR_RBCONJ] = (welt_cpp_actor *)(welt_cpp_ref_actor(
        context->fifos[DSG_FIFO_E15], context->fifos[DSG_FIFO_E16],
        DSG_ACTOR_RBCONJ, contextApp->actors[ACTOR_BCONJ], 0, 0, NULL, NULL,
        NULL));
    welt_cpp_ref_actor_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_RBCONJ], DSG_ACTOR_RBCONJ

    this->actors[DSG_ACTOR_RSF] = (welt_cpp_actor *)(welt_cpp_ref_actor(
        context->fifos[DSG_FIFO_E8], context->fifos[DSG_FIFO_E9],
        DSG_ACTOR_RSF, contextApp->actors[ACTOR_SF], 0, 0, NULL, NULL, NULL));
    welt_cpp_ref_actor_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_RSF], DSG_ACTOR_RSF

    this->actors[DSG_ACTOR_SDISTRIB] = (welt_cpp_actor *)
        (welt_cpp_sca_static_loop(context->fifos[DSG_FIFO_E23],
        context->fifos[DSG_FIFO_E26], context->fifos[DSG_FIFO_E24],
        context->fifos[DSG_FIFO_E27], window_number*2, DSG_ACTOR_SDISTRIB));
    welt_cpp_sca_static_loop_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_SDISTRIB], DSG_ACTOR_SDISTRIB

    this->actors[DSG_ACTOR_SPA] = (welt_cpp_actor *)(welt_cpp_sca_static_loop(
        context->fifos[DSG_FIFO_E5], context->fifos[DSG_FIFO_9],
        context->fifos[DSG_FIFO_6], context->fifos[DSG_FIFO_10], window_number,
        DSG_ACTOR_SPA));
    welt_cpp_sca_static_loop_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_SPA], DSG_ACTOR_SPA

    this->actors[DSG_ACTOR_RCONFIGS] = (welt_cpp_actor *)(welt_cpp_ref_actor(
        context->fifos[DSG_FIFO_E21], context->fifos[DSG_FIFO_E22],
        DSG_ACTOR_RCONFIGS, contextApp->actors[ACTOR_CONFIGS], 0, 0, NULL,
        NULL, NULL));
    welt_cpp_ref_actor_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_RCONFIGS], DSG_ACTOR_RCONFIGS

    this->actors[DSG_ACTOR_RPDF] = (welt_cpp_actor *)(welt_cpp_ref_actor(
        context->fifos[DSG_FIFO_E24], context->fifos[DSG_FIFO_E25],
        DSG_ACTOR_RPDF, contextApp->actors[ACTOR_PDF], 0, 0, NULL, NULL, NULL));
    welt_cpp_ref_actor_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_RPDF], DSG_ACTOR_RPDF

    this->actors[DSG_ACTOR_RFILT1] = (welt_cpp_actor *)(welt_cpp_ref_actor(
        context->fifos[DSG_FIFO_E32], context->fifos[DSG_FIFO_E33],
        DSG_ACTOR_RFILT1, contextApp->actors[ACTOR_FILT1], 0, 0, NULL, NULL,
        NULL));
    welt_cpp_ref_actor_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_RFILT1], DSG_ACTOR_RFILT1

    this->actors[DSG_ACTOR_RPO] = (welt_cpp_actor *)(welt_cpp_ref_actor(
        context->fifos[DSG_FIFO_E7], context->fifos[DSG_FIFO_E8],
        DSG_ACTOR_RPO, contextApp->actors[ACTOR_PO], 0, 0, NULL, NULL, NULL));
    welt_cpp_ref_actor_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_RPO], DSG_ACTOR_RPO

    this->actors[DSG_ACTOR_RPI] = (welt_cpp_actor *)(welt_cpp_ref_actor(
        context->fifos[DSG_FIFO_E6], context->fifos[DSG_FIFO_E7],
        DSG_ACTOR_RPI, contextApp->actors[ACTOR_PI], 0, 0, NULL, NULL, NULL));
    welt_cpp_ref_actor_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_RPI], DSG_ACTOR_RPI

    this->actors[DSG_ACTOR_RCONJ] = (welt_cpp_actor *)(welt_cpp_ref_actor(
        context->fifos[DSG_FIFO_E11], context->fifos[DSG_FIFO_E12],
        DSG_ACTOR_RCONJ, contextApp->actors[ACTOR_CONJ], 0, 0, NULL, NULL, NULL
        ));
    welt_cpp_ref_actor_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_RCONJ], DSG_ACTOR_RCONJ

    this->actors[DSG_ACTOR_RPAIN] = (welt_cpp_actor *)(welt_cpp_ref_actor(
        context->fifos[DSG_FIFO_E28], context->fifos[DSG_FIFO_E29],
        DSG_ACTOR_RPAIN, contextApp->actors[ACTOR_PAIN], 0, 0, NULL, NULL, NULL
        ));
    welt_cpp_ref_actor_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_RPAIN], DSG_ACTOR_RPAIN

    this->actors[DSG_ACTOR_RPOLY] = (welt_cpp_actor *)(welt_cpp_ref_actor(
        context->fifos[DSG_FIFO_E31], context->fifos[DSG_FIFO_E32],
        DSG_ACTOR_RPOLY, contextApp->actors[ACTOR_POLY], 0, 0, NULL, NULL, NULL
        ));
    welt_cpp_ref_actor_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_RPOLY], DSG_ACTOR_RPOLY

    this->actors[DSG_ACTOR_SES] = (welt_cpp_actor *)(welt_cpp_sca_static_loop(
        context->fifos[DSG_FIFO_E17], context->fifos[DSG_FIFO_E19],
        context->fifos[DSG_FIFO_E18], context->fifos[DSG_FIFO_E20],
        window_number*2, DSG_ACTOR_SES));
    welt_cpp_sca_static_loop_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_SES], DSG_ACTOR_SES

    this->actors[DSG_ACTOR_SPAIN] = (welt_cpp_actor *)(welt_cpp_sca_static_loop(
        context->fifos[DSG_FIFO_E27], context->fifos[DSG_FIFO_E29],
        context->fifos[DSG_FIFO_E28], context->fifos[DSG_FIFO_E30],
        window_number*2, DSG_ACTOR_SPAIN));
    welt_cpp_sca_static_loop_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_SPAIN], DSG_ACTOR_SPAIN

    this->actors[DSG_ACTOR_RFILT4] = (welt_cpp_actor *)(welt_cpp_ref_actor(
        context->fifos[DSG_FIFO_E35], context->fifos[DSG_FIFO_E36],
        DSG_ACTOR_RFILT4, contextApp->actors[ACTOR_FILT4], 0, 0, NULL, NULL,
        NULL));
    welt_cpp_ref_actor_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_RFILT4], DSG_ACTOR_RFILT4

    this->actors[DSG_ACTOR_SCH] = (welt_cpp_actor *)(welt_cpp_sca_sch_loop(
        context->fifos[DSG_FIFO_E44], context->fifos[DSG_FIFO_E0], 1,
        DSG_ACTOR_SCH));
    welt_cpp_sca_sch_loop_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_SCH], DSG_ACTOR_SCH

    this->actors[DSG_ACTOR_RCONFIG] = (welt_cpp_actor *)(welt_cpp_ref_actor(
        context->fifos[DSG_FIFO_E3], context->fifos[DSG_FIFO_E4],
        DSG_ACTOR_RCONFIG, contextApp->actors[ACTOR_CONFIG], 0, 0, NULL, NULL,
        NULL));
    welt_cpp_ref_actor_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_RCONFIG], DSG_ACTOR_RCONFIG

    this->actors[DSG_ACTOR_RFCD] = (welt_cpp_actor *)(welt_cpp_ref_actor(
        context->fifos[DSG_FIFO_E25], context->fifos[DSG_FIFO_E26],
        DSG_ACTOR_RFCD, contextApp->actors[ACTOR_FCD], 0, 0, NULL, NULL, NULL));
    welt_cpp_ref_actor_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_RFCD], DSG_ACTOR_RFCD

    this->actors[DSG_ACTOR_RFILT6] = (welt_cpp_actor *)(welt_cpp_ref_actor(
        context->fifos[DSG_FIFO_E37], context->fifos[DSG_FIFO_E38],
        DSG_ACTOR_RFILT6, contextApp->actors[ACTOR_FILT6], 0, 0, NULL, NULL,
        NULL));
    welt_cpp_ref_actor_connect(this->dsg_add_actor(this,this->actors[
        DSG_ACTOR_RFILT6], DSG_ACTOR_RFILT6

}

void dpd_sdsg::dsg_scheduler(int thread_ind, int thread_num){
    int start_ind;
    int end_ind;
    start_ind = this->first_actors_index[thread_ind];
     if (thread_ind != thread_num -1){
         end_ind =this->first_actors_index[thread_ind+1] - 1;
      }else{
      end_ind = this->dsg_count -1;}
    welt_cpp_util_dsg_optimized_scheduler( this, , start_ind, end_ind-start_ind
        + 1, (this->sink_array));
    }
