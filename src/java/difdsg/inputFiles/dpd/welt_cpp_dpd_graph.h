#ifndef _welt_cpp_dpd_graph_h
#define _welt_cpp_dpd_graph_h

#include <iostream>
#include "welt_cpp_graph.h"
#include "welt_cpp_util.h"
#include "welt_cpp_file_sink_float.h"
#include "welt_cpp_param_distrib_flt.h"
#include "welt_cpp_split_fifo.h"
#include "welt_cpp_file_source_int.h"
#include "welt_cpp_config_sink_float.h"
#include "welt_cpp_file_source_float.h"
#include "welt_cpp_param_distrib_est.h"
#include "welt_cpp_conjugate.h"
#include "welt_cpp_flt_coeff_distrib.h"
#include "welt_cpp_config_sink_int.h"
#include "welt_cpp_polynomial.h"
#include "welt_cpp_estimate.h"
#include "welt_cpp_accumulator.h"
#include "welt_cpp_basis_matrix.h"

#define BUFFER_CAPACITY 65536*4

#define ACTOR_CONFIG_SINK_FLOAT	0
#define ACTOR_ACC	1
#define FILTER_4	2
#define FILTER_3	3
#define ACTOR_FILE_SINK	4
#define ESTIMATE	5
#define ACTOR_FILE_SOURCE_DATA	6
#define ACTOR_SOURCE	7
#define PA_OUT_SRC	8
#define FILTER_1	9
#define ACTOR_POLY	10
#define PARAM_DISTRIB_EST	11
#define FILTER_2	12
#define ACTOR_PARAM_DISTRIB_FLT	13
#define ACTOR_FLT_COEFF_DISTRIB	14
#define FILTER_0	15
#define BASIS_MATRIX_NORM	16
#define FILTER_6	17
#define BASIS_MATRIX_CONJ	18
#define ACTOR_CONFIG_SINK_INT_SUBINIT	19
#define SPLIT_FIFO	20
#define PA_IN_SRC	21
#define CONJUGATE	22
#define FILTER_5	23

#define FIFO_BODY_PARAM7	0
#define FIFO_DATA0	1
#define FIFO_BODY_PARAM3	2
#define FIFO_BODY_COEFF0	3
#define FIFO_BODY_DATA11	4
#define FIFO_BODY_DATA7	5
#define FIFO_DATA5	6
#define FIFO_BODY_PARAM1	7
#define FIFO_BODY_DATA0	8
#define FIFO_BODY_OUT1	9
#define FIFO_BODY_OUT3	10
#define FIFO_PARAM1	11
#define FIFO_BODY_PARAM8	12
#define FIFO_BODY_DATA1	13
#define FIFO_BODY_COEFF1	14
#define FIFO_DATA2	15
#define FIFO_BODY_COEFF3	16
#define FIFO_BODY_COEFF5	17
#define FIFO_BODY_COEFF4	18
#define FIFO_SRC_OUT	19
#define FIFO_BODY_PARAM2	20
#define FIFO_BODY_DATA5	21
#define FIFO_BODY_OUT4	22
#define FIFO_BODY_DATA4	23
#define FIFO_DATA1	24
#define FIFO_BODY_PARAM9	25
#define FIFO_BODY_COEFF2	26
#define FIFO_DATA4	27
#define FIFO_BODY_OUT5	28
#define FIFO_BODY_OUT0	29
#define FIFO_DATA6	30
#define FIFO_BODY_COEFF10	31
#define FIFO_BODY_PARAM4	32
#define FIFO_BODY_COEFF6	33
#define FIFO_BODY_OUT2	34
#define FIFO_BODY_DATA2	35
#define FIFO_BODY_DATA6	36
#define FIFO_BODY_PARAM6	37
#define FIFO_PARAM2	38
#define FIFO_PARAM0	39
#define FIFO_BODY_PARAM0	40
#define FIFO_DATA3	41
#define FIFO_FLT_COEFF_0	42
#define FIFO_BODY_DATA3	43
#define FIFO_BODY_PARAM5	44
#define FIFO_BODY_OUT6	45

#define ACTOR_COUNT	24
#define FIFO_COUNT	46

class dpd_graph: public welt_cpp_graph {
public: 
welt_cpp_dpd_graph(char* data_filename, char* pa_in_filename, char* pa_out_filename, char* param_filename, char* output_filename, int* param, int param_length, float* coeffs, int coeffs_length);
void scheduler() override;
private: 
char* data_filename; 
char* pa_in_filename; 
char* pa_out_filename; 
char*
    param_filename; 
char* output_filename; 
int* param; 
int param_length;
    
float* coeffs; 
int coeffs_length;    ;}; 


#endif
