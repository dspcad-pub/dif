#ifndef _lide_c_add_graph_h
#define _lide_c_add_graph_h

#include <stdio.h>
#include <stdlib.h>
#include "lide_c_basic.h"
#include "lide_c_actor.h"
#include "lide_c_fifo.h"
#include "lide_c_fifo_basic.h"
#include "lide_c_graph.h"
#include "lide_c_util.h"
#include "lide_c_add.h"
#include "lide_c_file_source.h"
#include "lide_c_file_sink.h"

#define BUFFER_CAPACITY 1024

#define ACTOR_X	0
#define ACTOR_Y	1
#define ACTOR_A	2
#define ACTOR_OUT	3

#define FIFO_E1	0
#define FIFO_E2	1
#define FIFO_E3	2

#define ACTOR_COUNT	4
#define FIFO_COUNT	3

struct _lide_c_add_graph_context_struct;
typedef struct _lide_c_add_graph_context_struct lide_c_add_graph_context_type;

lide_c_add_graph_context_type *lide_c_add_graph_new();

void lide_c_add_graph_terminate(lide_c_add_graph_context_type *graph);

void lide_c_add_graph_scheduler(lide_c_add_graph_context_type *graph);

#endif
