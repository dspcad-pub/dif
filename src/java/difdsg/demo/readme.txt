This folder contains demos for the DIF-GPU framework. 

To run the demo, do the following: 

(1) make sure you have set up dif environment variables by using the following command: 

1. UXDIF=/path/to/DIF/pub
2. source $UXDIF/setup/runme

(2) Then, in the current directory, use the following command: 
../../../scripts/difgpu config.xml



Inputs: 

This command takes a configuration file, config.xml, which contains parameters
for running the script. The parameters are as follows: 

<dif_name> -- The path and filename to the .dif file containing the graph. 

<scheduler> -- The scheduler for DIF-GPU to use. You have 3 choices: 
(1) FCFS (First Come First Serve), 
(2) EFT (Heterogeneous earliest finish time), 
(3) ALLGPU(put all actors with GPU version on GPU). 

<profile_dir> -- The directory containing the profiles of the actors. 

<h2d_file> -- Path to the profile of host-to-device data transfer. 

<d2h_file> -- Path to profile of device-to-host data transfer. 

<n_threads> -- Number of threads (processor cores) to use. 

<n_gpu> -- Number of GPUs. Currently, only 1 GPU is supported, so n_gpu = 1 is 
enforced. 

<repetitions> -- Number of graph iterations before the program terminates. 
Although dataflow DSP systems are usually assumed to be executed forever, on 
a PC you usually want the program to generate outputs and exit. 

<vec_degrees> -- A list of graph level vectorization degrees, each separated 
by a comma and no space in between (for example, 128,256,512,1024). 


Outputs: 

For each of the vectorization degrees specified in <vec_degrees>, the DIF-GPU 
framework vectorizes the graph, compute the schedule, and generate LIDE-CUDA 
code for execution. 

All generated files are stored in a directory called result/#graph/, 
at the directory the script is run. #graph is the name of the graph specified 
in the dif file. For each vectorization degree, a subdirectory is created 
with the vectorization degree being the folder name. For example, if in the 
configuration file, the line of <vect_degrees> is: 

<vect_degrees>128,256<\vect_degrees>

Then 2 folders are created as follows: 
result/#graph/128
result/#graph/256

Each folder contains several files. #graph.h is the header file of the graph. 
#graph.cu contains the LIDE-CUDA source code of the graph. thread_k contains 
the schedule for the kth thread in the program. Please keep all files together 
in the same folder. 

