/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
package difdsg;
import dif.csdf.sdf.SDFGraph;
import dif.util.graph.Edge;
import dif.util.graph.Node;

import java.util.*;
////import mocgraph.Node;

public class LIDECAppCodeWriter {

    LIDECAppCodeWriter(DIFtoHierarchy app_h, String outputDir) {
        LWDFActor actor;
        /* Parameter initialization*/
        _app_h = app_h;
        _graph = _app_h.graph;
        _lwdfGraph = app_h.lwdf_graph;
        _contextMap = _app_h.nodeMap;
        _edgeMap = _app_h.edgeMap;
        _outputDir = outputDir;
        _appGraph_Name = _graph.getName();
        _appGraphHeader_Name = _outputDir + _lide_c_prefix + _appGraph_Name + "_graph.h";
        _appGraphSource_Name = _outputDir + _lide_c_prefix + _appGraph_Name + "_graph.c";
        _appGraphName = _appGraph_Name + "_graph";
		/* Check all actor types*/
        _ActorType = new HashSet<String>();
        for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()) {
            actor = entry.getValue();
            _ActorType.add(actor.getName());
        }
		/* Create files*/
        CreateGraphADTFiles();
		/* Get argument list for New function and actor New extra argument list*/
        InitGraphNewArguList(_lwdfGraph);
        InitActorNewArguList(_lwdfGraph);
    }

    public void run() {

		/*Create Header */
        WriteGraphHeader();
		/* Create Source*/
        WriteGraphSource();

    }

    private void CreateGraphADTFiles() {
        try {
            _gcm_h = new Gcm(_appGraphHeader_Name, false);
            _gcm_s = new Gcm(_appGraphSource_Name, false);
            _gcm_h.close();
            _gcm_s.close();
        } catch (Exception e) {
            System.out.println("Error: Cannot find " + _appGraphHeader_Name + "or" + _appGraphSource_Name);
            e.printStackTrace();
        }
    }

    private void WriteGraphHeader() {
        int i;
        _gcm_h = new Gcm(_appGraphHeader_Name, true);
		/*start macro ifdef*/
        _gcm_h.writeLine("#ifndef _" + _lide_c_prefix + _appGraphName + "_h");
        _gcm_h.writeLine("#define _" + _lide_c_prefix + _appGraphName + "_h");
        _gcm_h.writeLine("");

		/*include header files*/
        //General header files
        _gcm_h.writeLine("#include <stdio.h>");
        _gcm_h.writeLine("#include <stdlib.h>");
        //LIDE related headers
        //todo: different type of fifos
        // common headers
        _gcm_h.writeLine("#include \"" + _lide_c_prefix + "basic.h\"");
        _gcm_h.writeLine("#include \"" + _lide_c_prefix + "actor.h\"");
        _gcm_h.writeLine("#include \"" + _lide_c_prefix + "fifo.h\"");
        _gcm_h.writeLine("#include \"" + _lide_c_prefix + "fifo_basic.h\"");
        _gcm_h.writeLine("#include \"" + _lide_c_prefix + "graph.h\"");
        _gcm_h.writeLine("#include \"" + _lide_c_prefix + "util.h\"");

        // actor headers
        for (String actortype : _ActorType) {
            _gcm_h.writeLine("#include \"" + _lide_c_prefix + actortype + ".h\"");
        }
        _gcm_h.nextLine();

		/*macro definitions*/
        // Buffer capacity
        _gcm_h.writeLine("#define BUFFER_CAPACITY 1024");
        _gcm_h.nextLine();
        // Actor Index
        i = 0;
        for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()) {

            LWDFActor actor = entry.getValue();
            _gcm_h.writeLine("#define ACTOR_" + actor.getInstanceName().toUpperCase() + "\t" + i);
            i++;
        }
        _gcm_h.nextLine();
        // FIFO Index
        i = 0;
        for (Map.Entry<Edge, LWDFEdge> entry : _edgeMap.entrySet()) {

            LWDFEdge edge = entry.getValue();
            _gcm_h.writeLine("#define FIFO_" + edge.getName().toUpperCase() + "\t" + i);
            i++;
        }
        _gcm_h.nextLine();
        // Count of FIFOs and actors
        _gcm_h.writeLine("#define ACTOR_COUNT\t" + _contextMap.size());
        _gcm_h.writeLine("#define FIFO_COUNT\t" + _edgeMap.size());
        _gcm_h.nextLine();

		/* Graph context definition*/
        _gcm_h.writeLine("struct _" + _lide_c_prefix + _appGraphName + "_context_struct;");
        _gcm_h.write("typedef struct");
        _gcm_h.write(" _" + _lide_c_prefix + _appGraphName + "_context_struct ");
        _gcm_h.write(_lide_c_prefix + _appGraphName + "_context_type;");
        _gcm_h.next();
        _gcm_h.nextLine();

		/* Function declaration*/
        //construct
        _gcm_h.write(_lide_c_prefix + _appGraphName + "_context_type *");
        _gcm_h.write(_lide_c_prefix + _appGraphName + "_new();");
        _gcm_h.next();
        _gcm_h.nextLine();
        //terminate
        _gcm_h.write("void ");
        _gcm_h.write(_lide_c_prefix + _appGraphName + "_terminate(");
        _gcm_h.write(_lide_c_prefix + _appGraphName + "_context_type *graph);");
        _gcm_h.next();
        _gcm_h.nextLine();
        //terminate
        _gcm_h.write("void ");
        _gcm_h.write(_lide_c_prefix + _appGraphName + "_scheduler(");
        _gcm_h.write(_lide_c_prefix + _appGraphName + "_context_type *graph);");
        _gcm_h.next();
        _gcm_h.nextLine();

		/*end macro endif*/
        _gcm_h.writeLine("#endif");
        _gcm_h.flush1();
        _gcm_h.close();
    }

    private void WriteGraphSource() {

        _gcm_s = new Gcm(_appGraphSource_Name, true);

        // Header files
        WriteGraphSourceHeaderFiles();
        // Context
        WriteContext();
        // Construct functions
        WriteConstructFunction();
        // Terminate functions
        WriteTerminateFunction();
        // Scheduler
        WriteSchedulerFunction();
        _gcm_s.flush1();
        _gcm_s.close();

    }

    private void WriteGraphSourceHeaderFiles() {
        _gcm_s.writeLine("#include <stdio.h>");
        _gcm_s.writeLine("#include <stdlib.h>");
        _gcm_s.writeLine("#include <string.h>");
        _gcm_s.writeLine("#include \"" + _lide_c_prefix + "basic.h\"");
        _gcm_s.writeLine("#include \"" + _lide_c_prefix + "actor.h\"");
        _gcm_s.writeLine("#include \"" + _lide_c_prefix + _appGraphName + ".h\"");
        _gcm_s.nextLine();
        //_gcm_s.writeLine("#define NAME_LENGTH 20");
        _gcm_s.nextLine();

    }

    private void WriteContext() {

        _gcm_s.write("struct _" + _lide_c_prefix + _appGraphName + "_context_struct {");
        _gcm_s.next();

        _gcm_s.write("#include \"" + _lide_c_prefix + "graph_context_type_common.h\"");
        _gcm_s.next();

        _gcm_s.incIndent();
        //todo:save these files info in an array separately
        for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()) {
            if (!entry.getValue().getFileName().equals("no_name")) {
                _gcm_s.write("char *" + entry.getValue().getInstanceName() + "_file;");
                _gcm_s.next();
            }
        }

        _gcm_s.decIndent();
        _gcm_s.writeLine("};");

        _gcm_s.nextLine();

    }

    private void WriteConstructFunction() {

        int i;

        _gcm_s.write(_lide_c_prefix + _appGraphName + "_context_type *" + _lide_c_prefix +
                _appGraphName + "_new(");
        _gcm_s.write(_graphNewArguList);
        _gcm_s.write("){");
        _gcm_s.next();

        _gcm_s.incIndent();

        // Variable definition
        _gcm_s.write("int token_size;");
        _gcm_s.next();

        _gcm_s.write(_lide_c_prefix + _appGraphName + "_context_type * context = NULL;");
        _gcm_s.next();
        // Assign elements in graph ADT
        _gcm_s.write("context = ");
        _gcm_s.write("(" + _lide_c_prefix + _appGraphName + "_context_type *)");
        _gcm_s.write(_lide_c_prefix + "util_malloc(");
        _gcm_s.write("sizeof(");
        _gcm_s.write(_lide_c_prefix + _appGraphName + "_context_type));");
        _gcm_s.next();

        _gcm_s.writeLine("context->actor_count = ACTOR_COUNT;");
        _gcm_s.writeLine("context->fifo_count = FIFO_COUNT;");
        _gcm_s.next();

        // file name assignment
        for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()) {
            if (!entry.getValue().getFileName().equals("no_name")) {
                String file = entry.getValue().getInstanceName();
                _gcm_s.write("context->" + file + "_file=" + "\"" + file + ".txt\";");
                _gcm_s.next();
            }
        }
        _gcm_s.nextLine();
        //actors
        _gcm_s.write("context->actors = (");
        _gcm_s.write(_lide_c_prefix+"actor_context_type **)");
        _gcm_s.write(_lide_c_prefix + "util_malloc(");
        _gcm_s.write("context->actor_count * ");
        _gcm_s.write("sizeof(");
        _gcm_s.write(_lide_c_prefix+"actor_context_type *)");
        _gcm_s.write(");");
        _gcm_s.next();

        //fifos
        _gcm_s.write("context->fifos = (");
        _gcm_s.write(_lide_c_prefix+"fifo_pointer *)");
        _gcm_s.write(_lide_c_prefix + "util_malloc(");
        _gcm_s.write("context->fifo_count * ");
        _gcm_s.write("sizeof(");
        _gcm_s.write(_lide_c_prefix+"fifo_pointer)");
        _gcm_s.write(");");
        _gcm_s.next();

        _gcm_s.nextLine();
        // 5 malloc and assignments
        // Descriptors
        _gcm_s.write("context->descriptors = (char **)");
        _gcm_s.write(_lide_c_prefix + "util_malloc(");
        _gcm_s.write("context->actor_count * ");
        _gcm_s.write("sizeof(char*));");
        _gcm_s.next();


        for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()) {

            String actor = entry.getValue().getInstanceName();
            _gcm_s.write(_lide_c_prefix + "graph_set_actor_desc((");
            _gcm_s.write(_lide_c_prefix + "graph_context_type *)context, ");
            _gcm_s.write("ACTOR_" + actor.toUpperCase() + ", ");
            _gcm_s.write("\"" + actor.toLowerCase() + "\");");
            _gcm_s.next();

        }
        _gcm_s.nextLine();

        // source array
        _gcm_s.write("context->source_array = ");
        _gcm_s.write("(" + _lide_c_prefix + "actor_context_type **)");
        _gcm_s.write(_lide_c_prefix + "util_malloc(");
        _gcm_s.write("context->fifo_count * ");
        _gcm_s.write("sizeof(" + _lide_c_prefix + "actor_context_type *));");
        _gcm_s.next();
        _gcm_s.nextLine();

        // sink array
        _gcm_s.write("context->sink_array = ");
        _gcm_s.write("(" + _lide_c_prefix + "actor_context_type **)");
        _gcm_s.write(_lide_c_prefix + "util_malloc(");
        _gcm_s.write("context->fifo_count * ");
        _gcm_s.write("sizeof(" + _lide_c_prefix + "actor_context_type *));");
        _gcm_s.next();
        _gcm_s.nextLine();

        // fifo array
        //_gcm_s.write("for (i = 0; i<context->fifo_count; i++){");
        //_gcm_s.next();
        // _gcm_s.incIndent();
        //i = 0;
        for (Map.Entry<Edge, LWDFEdge> entry : _edgeMap.entrySet()) {

            LWDFEdge edge = entry.getValue();
            String edgeType = edge.getEdgeType();
            int capacity = edge.getCapacity();
            _gcm_s.write("token_size = sizeof(");
            _gcm_s.write(edgeType + ");");
            _gcm_s.next();
            _gcm_s.write("context->fifos[" + "FIFO_" + edge.getName().toUpperCase() + "] = ");
            _gcm_s.write("(" + _lide_c_prefix + "fifo_pointer)");
            _gcm_s.write(_lide_c_prefix + "fifo_basic_new(");
            _gcm_s.write(capacity + ", ");
            _gcm_s.write("token_size, ");
            _gcm_s.write("FIFO_" + edge.getName().toUpperCase() + ");");
            _gcm_s.next();

        }
        _gcm_s.nextLine();
        //_gcm_s.decIndent();
        //_gcm_s.writeLine("}");

        // actor array
        for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()) {

            LWDFActor actor = entry.getValue();
            String actorName = actor.getInstanceName();
            String actorType = actor.getName();
            String fileName = actor.getFileName();


            // Get input port and output port
            ArrayList<Port> inputPorts = actor.getPorts(1);
            ArrayList<Port> outputPorts = actor.getPorts(0);
            ArrayList<String> inputPortNames = new ArrayList<String>();
            ArrayList<String> outputPortNames = new ArrayList<String>();
            for (Port inPort : inputPorts) {
                Edge inEdge = inPort.edge;
                inputPortNames.add(_graph.getName(inEdge));
            }

            for (Port outPort : outputPorts) {
                Edge outEdge = outPort.edge;
                outputPortNames.add(_graph.getName(outEdge));
            }

            // write new function for each actor
            // argument list: input fifos, output fifos and actor index
            _gcm_s.write("context->actors[" + "ACTOR_" + actorName.toUpperCase() + "] = ");
            _gcm_s.write("(" + _lide_c_prefix + "actor_context_type *)");
            _gcm_s.write("(" + _lide_c_prefix + actorType.toLowerCase() + "_new(");
            // input ports
            /*
            for (String fifoname : inputPortNames) {
                //_gcm_s.write("(" + _lide_c_prefix + "fifo_basic_pointer)");
                _gcm_s.write("context->fifos[" + "FIFO_" + fifoname.toUpperCase() + "], ");
            }
            */
            // output ports
            /*
            for (String fifoname : outputPortNames) {
                //_gcm_s.write("(" + _lide_c_prefix + "fifo_basic_pointer)");
                _gcm_s.write("context->fifos[" + "FIFO_" + fifoname.toUpperCase() + "], ");
            }
            */
            // Extra Argument List
            //System.out.println(actorName+"  "+actorType);
            String actor_argu_list = _actorNewExtraArguList.get(actor);
            if (actor_argu_list != null)
                _gcm_s.write(actor_argu_list);

            // actor index
            //_gcm_s.write("ACTOR_" + actorName.toUpperCase() + "));");
            _gcm_s.write("));");
            _gcm_s.next();

            // write connect function
            _gcm_s.write(_lide_c_prefix + actorType.toLowerCase() + "_connect(");
            _gcm_s.write("context->actors[" + "ACTOR_" + actorName.toUpperCase() + "], ");
            _gcm_s.write("(" + _lide_c_prefix + "graph_context_type *)context);");
            _gcm_s.next();
            _gcm_s.nextLine();

        }

        // assigen scheduler
        /* context->scheduler = (lide_c_graph_scheduler_ftype)
                        lide_c_add_graph_scheduler;*/
        _gcm_s.write("context->scheduler = ");
        _gcm_s.write("(" + _lide_c_prefix + "graph_scheduler_ftype)");
        _gcm_s.write(_lide_c_prefix + _appGraphName + "_scheduler;");
        _gcm_s.next();

        _gcm_s.writeLine("return context;");
        _gcm_s.decIndent();
        _gcm_s.writeLine("}");

        _gcm_s.nextLine();

    }

    private void WriteTerminateFunction() {

        _gcm_s.write("void " + _lide_c_prefix + _appGraphName + "_terminate(");
        _gcm_s.write(_lide_c_prefix + _appGraphName+"_context_type *context){");
        _gcm_s.next();

        _gcm_s.incIndent();
        //variable definition
        _gcm_s.writeLine("int i;");

        //terminate fifos
        _gcm_s.write("for(i = 0; i < context->fifo_count; i++){");
        _gcm_s.next();
        _gcm_s.incIndent();

        _gcm_s.write(_lide_c_prefix + "fifo_basic_free(");
        _gcm_s.write("(" + _lide_c_prefix + "fifo_basic_pointer)");
        _gcm_s.write("context->fifos[i]);");
        _gcm_s.next();

        _gcm_s.decIndent();
        _gcm_s.writeLine("}");

        _gcm_s.nextLine();
        // terminate actors
        for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()) {
            LWDFActor actor = entry.getValue();
            //String actor = entry.getValue().getInstanceName();
            String actorName = actor.getInstanceName();
            String actorType = actor.getName();
            _gcm_s.write(_lide_c_prefix +  actorType.toLowerCase() + "_terminate(");
            _gcm_s.write("(" + _lide_c_prefix +  actorType.toLowerCase() + "_context_type *)");
            _gcm_s.write("context->actors[" + "ACTOR_" + actorName.toUpperCase() + "]);");
            _gcm_s.next();

        }
        _gcm_s.nextLine();

        // de-allocation
        _gcm_s.writeLine("free(context->fifos);");
        _gcm_s.writeLine("free(context->actors);");
        _gcm_s.writeLine("free(context->descriptors);");
        _gcm_s.writeLine("free(context);");
        _gcm_s.decIndent();
        _gcm_s.writeLine("}");

    }

    private void WriteSchedulerFunction() {
        String sch = _lwdfGraph.getGraphSchedule();
        _gcm_s.write("void " + _lide_c_prefix + _appGraphName + "_scheduler(");
        _gcm_s.write(_lide_c_prefix + _appGraphName + "_context_type *context){");
        _gcm_s.next();

        _gcm_s.incIndent();

        if(sch.isEmpty()){
            _gcm_s.write(_lide_c_prefix + "util_simple_scheduler(");
            _gcm_s.write("context->actors, ");
            _gcm_s.write("context->actor_count, ");
            _gcm_s.write("context->descriptors);");
        }else {
            switch (sch) {
                case "simple":
                    _gcm_s.write(_lide_c_prefix + "util_simple_scheduler(");
                    _gcm_s.write("context->actors, ");
                    _gcm_s.write("context->actor_count, ");
                    _gcm_s.write("context->descriptors);");
                    break;
                default:
                    _gcm_s.write(sch);
                    break;
            }
        }


        _gcm_s.next();
        _gcm_s.writeLine("return;");
        _gcm_s.decIndent();
        _gcm_s.writeLine("}");

    }

    /*_graphNewArguList example: "int len, char * filename, double value" */
    private void InitGraphNewArguList(LWDFGraph lwdfGraph) {
        _graphNewArguList = new String("");
        LinkedHashMap<Integer, String> lwdfgraphArguType = _lwdfGraph.getGraphArguTypeList(_lwdfGraph);
        LinkedHashMap<Integer, String> lwdfgraphArguVar = _lwdfGraph.getGraphArguVarList(_lwdfGraph);
        if (lwdfgraphArguType == null || lwdfgraphArguVar == null){
            _graphNewArguList = "";
            return;
        }
        for (Map.Entry<Integer, String> entry : lwdfgraphArguType.entrySet()) {
            Integer Index = entry.getKey();
            String arguType = entry.getValue();
            String arguVar = lwdfgraphArguVar.get(Index);
            //System.out.print("index:"+Index+" arguType:"+arguType+"\n");
            //System.out.print("index:"+Index+" arguVar:"+arguVar+"\n");
            _graphNewArguList = _graphNewArguList.concat(arguType);
            _graphNewArguList = _graphNewArguList.concat(" ");
            _graphNewArguList = _graphNewArguList.concat(arguVar);
            _graphNewArguList = _graphNewArguList.concat(", ");
            //System.out.println(_graphNewArguList);
        }
        int len = _graphNewArguList.length();
        if(len < 2){
            _graphNewArguList = "";
            return;
        }
        len = len-2;
        //System.out.println(len);
        _graphNewArguList = _graphNewArguList.substring(0, len);
    }


    /*arguList example: "int len, char * filename, double value" */
    private void InitActorNewArguList(LWDFGraph lwdfGraph) {

        _actorNewExtraArguList = new HashMap<LWDFActor, String>();
        //iterate for each actor
        for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()) {
            LWDFActor actor = entry.getValue();
            String arguList = "";
            LinkedHashMap<Integer, String> arguListMap = actor.getActorArguVarList();
            //sort by key
            Map<Integer, String> sortedarguListMap = new TreeMap<Integer, String>(arguListMap);
            if(sortedarguListMap != null || sortedarguListMap.size() != 0) {
                //iterate for each argument
                for (Map.Entry<Integer, String> argu : sortedarguListMap.entrySet()) {
                    String arg_val = argu.getValue();
                    String argu_str;

                    if(arg_val.contains("fifo_")){
                        // fifo argument
                        String fifo_name =arg_val.substring(5);
                        argu_str = "context->fifos[FIFO_" + fifo_name.toUpperCase() + "]";

                    }else if(arg_val.contains("actor_index")) {
                        // index argument
                        argu_str = "ACTOR_"+actor.getInstanceName().toUpperCase();
                    }else {
                        //rest arguments
                        argu_str = arg_val;
                    }
                    // concatenate argument
                    arguList = arguList.concat(argu_str);
                    arguList = arguList.concat(", ");
                }
                //arguList = arguList.substring(0, arguList.length() - 2);
            }else{
                arguList = "";
            }

            int len = arguList.length();
            if(len < 2){
                arguList = "";
                return;
            }
            len = len-2;
            //System.out.println(len);
            arguList = arguList.substring(0, len);
            _actorNewExtraArguList.put(actor, arguList);
        }


        //System.out.println("actor argu extra list:"+_actorNewExtraArguList);
    }

    ///////////////////////////////////////////////////////////////////////////
    ////                           Members                                ////

    private SDFGraph _graph;
    private DIFtoHierarchy _app_h;
    private HashMap<Node, LWDFActor> _contextMap;
    //private HashMap<Node, dif2lide.LWDFActor> _contextMap;
    private HashMap<Edge, LWDFEdge> _edgeMap;
    //private HashMap<Edge, dif2lide.LWDFEdge> _edgeMap;
    private LWDFGraph _lwdfGraph;
    private String _outputDir;
    private String _appGraph_Name;
    private String _appGraphName;
    private String _appGraphHeader_Name;
    private String _appGraphSource_Name;
    private Gcm _gcm_h;
    private Gcm _gcm_s;
    private String _lide_c_prefix = "lide_c_";
    private String _graphNewArguList;
    private HashMap<LWDFActor, String> _actorNewExtraArguList;

    private Set<String> _ActorType;
    //private ApplGraphFileWriter _applGraphFileWriter;
    //private DsgGraphFileWriter _dsgGraphFileWriter;
    //private final SDFGraph _dsggraph;
    //private static final String _SCHEDULE_FILE_PREFIX = "thread_";
    //private PrintWriter makeMe;
    //private PrintWriter runMe;
}
