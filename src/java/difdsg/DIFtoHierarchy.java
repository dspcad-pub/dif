/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
package difdsg;
import dif.DIFHierarchy;
import dif.DIFLoader;
import dif.csdf.sdf.SDFGraph;
import dif.psdf.PSDFGraph;

import dif.util.graph.Edge;
import dif.util.graph.Node;

import java.util.Collection;
import java.util.HashMap;


public class DIFtoHierarchy {
    public DIFtoHierarchy(String DIFFileName, String outputDir){
        this(DIFFileName, outputDir, false);
    }
    /* Create Hierarchy for DIF FILE*/
    public DIFtoHierarchy(String DIFFileName, String outputDir, boolean diag){
        _diag = diag;

        outputDirectory = outputDir;
        /* Get Hierarchy for DIF file given */
        readDIFGraph(DIFFileName);
        initLWDFInfo();
    }

    private void readDIFGraph(String fileName) {

        topHierarchy = DIFLoader.loadDataflow(fileName);
        if(topHierarchy.getGraph() instanceof PSDFGraph){
            System.out.println("psdf graph");
            psdfgraph = (PSDFGraph)topHierarchy.getGraph();

        } else {
            graph = (SDFGraph) topHierarchy.getGraph();
        }
        System.out.println("graph");
        System.out.println(graph);
    }

    private void initLWDFInfo() {
        diagMessage("DIFtoLWDF: initializing LWDF Information...");
        nodeMap = new HashMap<Node, difdsg.LWDFActor>();
        edgeMap = new HashMap<Edge, difdsg.LWDFEdge>();
        /* Initialize Nodes */
        Collection<Node> nodeCollection = graph.nodes();
        int actorID = 0;
        for (Node node : nodeCollection) {
            /* LWDFActor class modified based on */
            difdsg.LWDFActor actor = new difdsg.LWDFActor(node, graph, actorID/*, 0, _directory*/);
            actorID++;
            nodeMap.put(node, actor);
        }
        /* Initialize Edges */
        Collection<Edge> edgeCollection = graph.edges();
        int edgeID = 0;
        for (Edge edge : edgeCollection) {
            difdsg.LWDFEdge fifo = new difdsg.LWDFEdge(edge, graph, edgeID, graph.getName(edge));
            edgeID++;
            edgeMap.put(edge, fifo);
        }

        /* Initialize Graph */
        String graphName = graph.getName();
        lwdf_graph = new LWDFGraph(graph, graphName);

    }

    void run() {
        //_vectorizer = new Vectorizer(_vecDegree, _bufferBound, _contextMap, _edgeMap);
        //_vectorizer.vectorize(_graph);
        //_actorVecDegrees = _vectorizer.getVectorizationDegrees();
        //create scheduler
        //createScheduler();
        //_scheduler.run();
        //_scheduleLists = _scheduler.getScheduleLists();
        // generate mapping information
        //mapActors();
        //mapEdges();
        //_scheduler.printSchedule();
        //System.out.println("Schedule Time: " + _scheduler.makeSpan());
        // insert memcpy nodes
        //insertMemcpyNodes();
        //writeGraph("converted");
        //GlobalSettings.debugMessage("Size: " + _edgeMap.size());


//COMMENT PARTS
        /*
        _codeWriter = new dif2lide.CodeWriter(_graph,_dsggraph, _contextMap, _dsgContextMap, _edgeMap, _dsgEdgeMap,
				 _outputDirectory);
        _codeWriter.run();

        */
    }
	
	/*private void mapActors(){
		for (int i = 0; i < _scheduleLists.size(); i++){
			LoopedScheduleList schedList = _scheduleLists.get(i); 
			for (int j = 0; j < schedList.getLength(); j++){
				Node node = schedList.getElement(j).getNode(); 
				_actorAssignments.put(node, i); 
			}
		}
	}*/
	
	/*private void mapEdges(){
		for (Map.Entry<Edge, LWDFEdge> entry : _edgeMap.entrySet()){
			Edge edge = entry.getKey(); 
			Node source = edge.source(); 
			Node sink = edge.sink(); 
			int sourceProcessorIndex = _actorAssignments.get(source); 
			int sinkProcessorIndex = _actorAssignments.get(sink);
			if (sourceProcessorIndex == sinkProcessorIndex && 
					_platform.getProcessorType(sourceProcessorIndex) == 1){
				_edgeMemspace.put(edge, 1); 
			} else {
				_edgeMemspace.put(edge, 0); 
			}
		}
	}*/

    /** Change the graph to make sure it gets synthesized correctly.
     *
     */
	/*
	private void insertMemcpyNodes(){
    	// For each edge whose source and sink are assigned to different processors, 
    	// insert a LWDF memcpy actor in between. 
    	Collection<Edge> allEdges = _graph.edges(); 
    	LinkedList<Edge> removedH2DEdges = new LinkedList<Edge>();
    	LinkedList<Edge> removedD2HEdges = new LinkedList<Edge>();
    	LinkedList<Edge> newH2DEdges = new LinkedList<Edge>(); 
    	LinkedList<Edge> newD2HEdges = new LinkedList<Edge>(); 
    	LinkedList<Node> newH2DNodes = new LinkedList<Node>();
    	LinkedList<Node> newD2HNodes = new LinkedList<Node>();

    	//record all edges and nodes to be modified. 
    	for (Edge e : allEdges){
    		Node srcNode = e.source();
    		Node snkNode = e.sink();
    		int srcProcessorType = _platform.getProcessorType(_actorAssignments.get(srcNode));  
    		int snkProcessorType = _platform.getProcessorType(_actorAssignments.get(snkNode));
    		if (srcProcessorType == 0 && snkProcessorType == 1){
    			// A H2D actor needs to be inserted.
				SDFEdgeWeight oldWeight = (SDFEdgeWeight)e.getWeight();
				SDFEdgeWeight inWeight = new SDFEdgeWeight(oldWeight.getSDFProductionRate(),
						oldWeight.getSDFConsumptionRate(),oldWeight.getIntDelay()); 
				SDFEdgeWeight outWeight = new SDFEdgeWeight(oldWeight.getSDFConsumptionRate(),
						oldWeight.getSDFConsumptionRate(),oldWeight.getIntDelay()); 
				CoreFunctionNode nodeWeight = new CoreFunctionNode();
				Node newNode = new Node(nodeWeight); 
				Edge inEdge = new Edge(srcNode,newNode,inWeight);
				Edge outEdge = new Edge(newNode, snkNode, outWeight);
				removedH2DEdges.add(e);
				newH2DEdges.add(inEdge);
				newH2DEdges.add(outEdge); 
				newH2DNodes.add(newNode); 
    		} else if (srcProcessorType == 1 && snkProcessorType == 0) {
    			// A D2H actor needs to be inserted. 
				SDFEdgeWeight oldWeight = (SDFEdgeWeight)e.getWeight();
				SDFEdgeWeight inWeight = new SDFEdgeWeight(oldWeight.getSDFProductionRate(),
						oldWeight.getSDFProductionRate(),0); //0 is delay
				SDFEdgeWeight outWeight = new SDFEdgeWeight(oldWeight.getSDFProductionRate(),
						oldWeight.getSDFConsumptionRate(),0); //0 is delay
				CoreFunctionNode nodeWeight = new CoreFunctionNode();
				Node newNode = new Node(nodeWeight); 
				Edge inEdge = new Edge(srcNode,newNode,inWeight);
				Edge outEdge = new Edge(newNode, snkNode, outWeight);
				removedD2HEdges.add(e);
				newD2HEdges.add(inEdge);
				newD2HEdges.add(outEdge); 
				newD2HNodes.add(newNode); 
    		} else {
    			// Do nothing
    		}
    	}
    	// sanity check
    	assert (newH2DEdges.size() == 2 * newH2DNodes.size() && 
    			newD2HEdges.size() == 2 * newD2HNodes.size());

    	// Add h2d and d2h nodes and edges    	
    	int h2dCount = 0; 
    	for (int i = 0; i < newH2DNodes.size(); i++) {
    		Node h2d = newH2DNodes.get(i);
    		Edge inEdge = newH2DEdges.get(2*i); 
    		Edge outEdge = newH2DEdges.get(2*i+1);
    		Edge oldEdge = removedH2DEdges.get(i); 
    		String instanceName = _MEMCPY_H2D_NAME.toUpperCase() + "_" + h2dCount;
    		String inEdgeName =  "edge_in_" + _MEMCPY_H2D_NAME + "_" + h2dCount; 
    		String outEdgeName =  "edge_out_" + _MEMCPY_H2D_NAME + "_" + h2dCount; 
    		addMemcpyNode(h2d, inEdge, outEdge, _MEMCPY_NAME, instanceName); 
    		addMemcpyEdgePair(inEdge, outEdge, oldEdge, inEdgeName, outEdgeName); 
    		// register to context map
    		int id = _contextMap.size();
    		LWDFEdge oldFifo = _edgeMap.get(oldEdge);
    		LWDFActor h2dActor = new LWDFActor(h2d, _graph, id, 1, oldFifo.getEdgeType()); 
    		_contextMap.put(h2d, h2dActor);
    		// register to assignment map
    		Node succ = successor(h2d); 
    		Integer processor = _actorAssignments.get(succ); 
    		_actorAssignments.put(h2d, processor); 
    		// change actor port information
    		Node source = oldEdge.source();
    		LWDFActor srcActor = _contextMap.get(source); 
    		srcActor.replacePort(oldEdge, inEdge); 
    		Node sink = oldEdge.sink(); 
    		LWDFActor snkActor = _contextMap.get(sink); 
    		snkActor.replacePort(oldEdge, outEdge); 
    		// register edge
    		int inEdgeID = _edgeMap.size(); 
    		LWDFEdge inFIFO = new LWDFEdge(inEdge, _graph, inEdgeID, inEdgeName); 
    		_edgeMap.put(inEdge, inFIFO); 
    		int outEdgeID = _edgeMap.size(); 
    		LWDFEdge outFIFO = new LWDFEdge(outEdge, _graph, outEdgeID, outEdgeName); 
    		_edgeMap.put(outEdge, outFIFO); 
    		// set edge memspace
    		_edgeMemspace.put(inEdge, 0); 
    		_edgeMemspace.put(outEdge,1); 
    		// change schedule nodes
    		insertMemcpyScheduleNode(h2d, 0); 
    		h2dCount++; 
    	}
    	
    	int d2hCount = 0; 
    	for (int i = 0; i < newD2HNodes.size(); i++) {
    		Node d2h = newD2HNodes.get(i);
    		Edge inEdge = newD2HEdges.get(2*i); 
    		Edge outEdge = newD2HEdges.get(2*i+1);
    		Edge oldEdge = removedD2HEdges.get(i); 
    		String instanceName = _MEMCPY_D2H_NAME.toUpperCase() + "_" + d2hCount; 
    		String inEdgeName =  "edge_in_" + _MEMCPY_D2H_NAME + "_" + d2hCount; 
    		String outEdgeName =  "edge_out_" + _MEMCPY_D2H_NAME + "_" + d2hCount; 
    		addMemcpyNode(d2h, inEdge, outEdge, _MEMCPY_NAME, instanceName); 
    		addMemcpyEdgePair(inEdge, outEdge, oldEdge, inEdgeName, outEdgeName); 
    		// register to context map
    		int id = _contextMap.size();
    		LWDFEdge oldFifo = _edgeMap.get(oldEdge);
    		LWDFActor h2dActor = new LWDFActor(d2h, _graph, id, 2, oldFifo.getEdgeType()); 
    		_contextMap.put(d2h, h2dActor);
    		// register to assignment map
    		Node pred = predecessor(d2h); 
    		Integer processor = _actorAssignments.get(pred); 
    		_actorAssignments.put(d2h, processor); 
    		// change actor port information
    		Node source = oldEdge.source();
    		LWDFActor srcActor = _contextMap.get(source); 
    		srcActor.replacePort(oldEdge, inEdge); 
    		Node sink = oldEdge.sink(); 
    		LWDFActor snkActor = _contextMap.get(sink); 
    		snkActor.replacePort(oldEdge, outEdge); 
    		// register edges	
    		int inEdgeID = _edgeMap.size(); 
    		LWDFEdge inFIFO = new LWDFEdge(inEdge, _graph, inEdgeID, inEdgeName); 
    		_edgeMap.put(inEdge, inFIFO); 
    		int outEdgeID = _edgeMap.size(); 
    		LWDFEdge outFIFO = new LWDFEdge(outEdge, _graph, outEdgeID, outEdgeName); 
    		_edgeMap.put(outEdge, outFIFO); 
    		// set edge memspace
    		_edgeMemspace.put(inEdge, 1); 
    		_edgeMemspace.put(outEdge,0); 
    		// change schedule nodes
    		insertMemcpyScheduleNode(d2h, 1); 
    		d2hCount++; 
    	}
    	
    	// remove edges
    	for (Edge e : removedH2DEdges){
    		_graph.removeEdge(e);
    		_edgeMap.remove(e); 
    		_edgeMemspace.remove(e); 
    	}
    	for (Edge e : removedD2HEdges){
    		_graph.removeEdge(e);
    		_edgeMap.remove(e); 
    		_edgeMemspace.remove(e); 
    	}
    	
    }
	
	// insert memcpy node into the schedule.
	// the memcpy node is inserted to the GPU actor that is either its successor (h2d transfer)
	// or predecessor (d2h transfer). 
	// 0: stands for h2d, 1 for d2h. 
	private void insertMemcpyScheduleNode(Node memcpyNode, int direction){
		Node node = null; 
		if (direction == 0) { // h2d transfer: 
			node = successor(memcpyNode); 
		} else {
			node = predecessor(memcpyNode); 
		}
		// find the node in the schedule
		int processorIndex = -1; 
		int size = _scheduleLists.size(); 
		for (int i = 0; i < size; i++){
			// a node should be mapped only to one of the processor. 
			LoopedScheduleList schedList = _scheduleLists.get(i);
			if (schedList.firstIndexOf(node) >= 0){
				processorIndex = i; 
				break; 
			}
		}
		if (processorIndex < 0){
			String instanceName = _contextMap.get(node).getInstanceName(); 
			GlobalSettings.debugMessage("MemcpyInsert: Unable to find the specified node: " + instanceName);
			return; 
		}
		LoopedScheduleList schedule = _scheduleLists.get(processorIndex); 
		//assert(schedule.firstIndexOf(node) > 0); 
		// one by one, insert the memory copy nodes before node (h2d) or after (d2h)
		if (direction == 0){
			// h2d. xxxAxxx becomes ... (q(A)h2d q(A)A) ...
			for (int i = 0; i < schedule.getLength(); i++){
				if (schedule.getElement(i).getNode().equals(node)){
					int q = _graph.getRepetitions(node); 
					schedule.insertNode(memcpyNode, q, i); 
					i++; 
				}
			}
		} else {
			// d2h. xxAxx becomes xx(q(A)A q(A)d2h) ...
			for (int i = 0; i < schedule.getLength(); i++){
				if (schedule.getElement(i).getNode().equals(node)){
					int q = _graph.getRepetitions(node); 
					if (i < schedule.getLength()){
						schedule.insertNode(memcpyNode, q, i+1);
					}
					else {
						LoopedScheduleElement element = new LoopedScheduleElement(memcpyNode, q); 
						schedule.addElement(element); 
					}
				}
			}
		} 
		// end function
	}
	
	
	/** Configure newly created memcpy node. 
     * 
     * @param node the memcpy node to be configured. 
     * @param inEdge the input edge of the node. 
     * @param outEdge the output edge of the node. 
     * @param actorName the name of the node. 
     /*
    private void addMemcpyNode(Node node, Edge inEdge, Edge outEdge, String actorName, 
    		String instanceName){
        // Should change: own context map, and all its neighbors's context. Especially ports. 
    	// set up the memcpy node
    	DIFAttribute nodeNameAttr = new DIFAttribute("name"); 
        nodeNameAttr.setValue(actorName);
        DIFAttribute cudaEnabledAttr = new DIFAttribute("CUDAEnabled"); 
        cudaEnabledAttr.setValue(1); 
        DIFAttribute inputPortAttr = new DIFAttribute("port_0"); 
        inputPortAttr.setType("INPUT"); 
        inputPortAttr.setValue(inEdge);
        DIFAttribute outputPortAttr = new DIFAttribute("port_1"); 
        outputPortAttr.setType("OUTPUT"); 
        outputPortAttr.setValue(outEdge);
		_graph.addNode(node);
		_graph.setName(node, instanceName);
		_graph.setAttribute(node, nodeNameAttr);
		_graph.setAttribute(node, cudaEnabledAttr); 
		_graph.setAttribute(node, inputPortAttr);
		_graph.setAttribute(node, outputPortAttr);
		
    }
    /** 
     * Configure an memcpy Edge. 
     * @param edge the edge to be configured. 
     * @param oldEdge the edge to be replaced by the new memcpy edge in the original graph.
     * @param name the name of the edge. 
     */
    /*
    private void addMemcpyEdgePair(Edge inEdge, Edge outEdge, Edge oldEdge, String inEdgeName, 
    		String outEdgeName){
    	// Add the new edges into graph.
		_graph.addEdge(inEdge);
		_graph.addEdge(outEdge);
        DIFAttribute edgeTypeAttr = _graph.getAttribute(oldEdge, "edgeType");
		_graph.setAttribute(inEdge, edgeTypeAttr);
		_graph.setAttribute(outEdge, edgeTypeAttr);
		_graph.setName(inEdge, inEdgeName);
		_graph.setName(outEdge, outEdgeName);
    }
    */

    /*
    private void initDIFLWDFInfo() {
        diagMessage("DIFtoLWDFApp: initializing LWDF Information...");
        _contextMap = new HashMap<Node, dif2lide.LWDFActor> ();
        _edgeMap = new HashMap<Edge, dif2lide.LWDFEdge>();
        // Initialize Nodes
        Collection<Node> nodeCollection = _graph.nodes();
        int actorID = 0;
        for (Node node : nodeCollection) {
            // 0 for computation actors
            dif2lide.LWDFActor actor = new dif2lide.LWDFActor(node, _graph, actorID);
            actorID++;
            _contextMap.put(node, actor);
        }
        // Initialize Edges
        Collection<Edge> edgeCollection = _graph.edges();
        int edgeID = 0;
        for (Edge edge : edgeCollection){
            dif2lide.LWDFEdge fifo = new dif2lide.LWDFEdge(edge, _graph, edgeID, _graph.getName(edge));
            edgeID++;
            _edgeMap.put(edge, fifo);
        }
    }
    */
    /*
    private void initDSGLWDFInfo() {
        diagMessage("DIFtoLWDFApp: initializing LWDF Information...");
        _dsgContextMap = new HashMap<Node, dif2lide.LWDFActor> ();
        _dsgEdgeMap = new HashMap<Edge, dif2lide.LWDFEdge>();

        // Initialize Nodes
        Collection<Node> nodeCollection = _dsggraph.nodes();
        int actorID = 0;
        for (Node node : nodeCollection) {
            // 0 for computation actors
            dif2lide.LWDFActor actor = new dif2lide.LWDFActor(node, _dsggraph, actorID);
            actorID++;
            _dsgContextMap.put(node, actor);
        }

        // Initialize Edges
        Collection<Edge> edgeCollection = _dsggraph.edges();
        int edgeID = 0;
        for (Edge edge : edgeCollection){
            dif2lide.LWDFEdge fifo = new dif2lide.LWDFEdge(edge, _dsggraph, edgeID, _dsggraph.getName(edge));
            edgeID++;
            _dsgEdgeMap.put(edge, fifo);
        }

    }
    */
    // assign all actors onto processor # 0-- a CPU core; assign all FIFOs on CPU -- 0
    /*
    private void initSchedulingInfo(){
        diagMessage("DIFtoLWDFApp: initializing scheduling information...");
        _actorAssignments = new HashMap<Node, Integer>();
        _edgeMemspace = new HashMap<Edge, Integer>();
        //_scheduleLists = new ArrayList<LoopedScheduleList>();

        for (Map.Entry<Node, dif2lide.LWDFActor> entry : _contextMap.entrySet()){
            Node node = entry.getKey();
            _actorAssignments.put(node, 0);
        }

        for (Map.Entry<Edge, dif2lide.LWDFEdge> entry : _edgeMap.entrySet()){
            Edge edge = entry.getKey();
            _edgeMemspace.put(edge, 0);
        }
    }

    // returns the first predecessor of a given node in the graph
    private Node predecessor(Node node){
        Collection<Node> predecessors = _graph.predecessors(node);
        Iterator<Node> iterator = predecessors.iterator();
        if (iterator.hasNext()){
            return iterator.next();
        } else {
            return null;
        }
    }
    // returns the first successor of a given node in the graph
    private Node successor(Node node){
        Collection<Node> successors = _graph.successors(node);
        Iterator<Node> iterator = successors.iterator();
        if (iterator.hasNext()){
            return iterator.next();
        } else {
            return null;
        }
    }
    */
    // return the consumption rate as integer of the first edge of a node in the graph. 
    /*
    private int consumptionRate(Node node){
        Collection<Edge> edgeCollection = _graph.inputEdges(node);
        Iterator<Edge> iterator = edgeCollection.iterator();
        Edge inEdge = null;
        if (iterator.hasNext()){
            inEdge = iterator.next();
            SDFEdgeWeight weight = (SDFEdgeWeight) inEdge.getWeight();
            int rate = weight.getSDFConsumptionRate();
            return rate;
        } else {
            dif2lide.GlobalSettings.debugMessage("consRate: requested node has no input edges");
            return 0;
        }
    }
    */

    // return the consumption rate as integer of the first edge of a node in the graph.
    // very useful for single input / output edges. 
    /*
    private int productionRate(Node node){
        Collection<Edge> edgeCollection = _graph.outputEdges(node);
        Iterator<Edge> iterator = edgeCollection.iterator();
        Edge outEdge = null;
        if (iterator.hasNext()){
            outEdge = iterator.next();
            SDFEdgeWeight weight = (SDFEdgeWeight) outEdge.getWeight();
            int rate = weight.getSDFProductionRate();
            return rate;
        } else {
            dif2lide.GlobalSettings.debugMessage("prodRate: requested node has no output edges");
            return 0;
        }
    }
    */
    /*
    private void readDSGGraph(String fileName) {
        //System.out.println(fileName);
        _dsgtopHierarchy = DIFLoader.loadDataflow(fileName);

        _dsggraph = (SDFGraph)_dsgtopHierarchy.getGraph();

//        try {
//            Reader reader = new Reader(fileName);
//            reader.compile();
//            _dsgtopHierarchy = reader.getTopHierarchy();
//            _dsggraph = (SDFGraph)_dsgtopHierarchy.getGraph();
//        } catch (IOException exp) {
//            throw new RuntimeException(exp.getMessage());
//        } catch (DIFLanguageException exp) {
//            throw new RuntimeException(exp.getMessage());
//        }

    }
    */

/*
    private void writeGraph(String name){
        DIFdoc testdoc = new DIFdoc(_topHierarchy);
        testdoc.generateNodePNG(_graph);
        String graphName = _graph.getName();
        String fileName = graphName + ".png";
        String fileName2 = graphName + _vecDegree + ".png";
        File oldPng = new File(fileName);
        File newPng = new File(fileName2);
        if (oldPng.exists()){
            oldPng.renameTo(newPng);
            dif2lide.GlobalSettings.debugMessage("Write to file " + name + ".png");
        } else {
            dif2lide.GlobalSettings.debugMessage("Cannot find " + fileName);
        }

    }
*/

    private void diagMessage(String s) {
        if (_diag){
            System.out.println(s);
        }
    }


    /******************** Members *****************/
    public DIFHierarchy topHierarchy;
    public SDFGraph graph;
    public PSDFGraph psdfgraph;
    private String outputDirectory;
    public HashMap<Node, difdsg.LWDFActor> nodeMap;
    //public HashMap<Node, dif2lide.LWDFActor> nodeMap;
    public HashMap<Edge, difdsg.LWDFEdge> edgeMap;
    //public HashMap<Edge, dif2lide.LWDFEdge> edgeMap;
    public LWDFGraph lwdf_graph;

    // components 
    //private Vectorizer _vectorizer; 
    //private dif2lide.CodeWriter _codeWriter;
    // private ListScheduler _scheduler;
    // settings
    private int _bufferBound;
    private int _vecDegree; // the desired graph-level vectorization degree
    private String _directory;
    private String _schedulerName;
    private String _outputDirectory;
    private String _h2dFileName;
    private String _d2hFileName;
    private int _fifoCapacity;   //TODO: fix fifo capacity to something
    //private final int _repetitions; 
    // graph related reference
    private DIFHierarchy _topHierarchy;
    private SDFGraph _graph;


    private DIFHierarchy _dsgtopHierarchy;
    private SDFGraph _dsggraph;
    private HashMap<Node, dif2lide.LWDFActor> _contextMap;
    private HashMap<Edge, dif2lide.LWDFEdge> _edgeMap;


    private HashMap<Node, dif2lide.LWDFActor> _dsgContextMap;
    private HashMap<Edge, dif2lide.LWDFEdge> _dsgEdgeMap;
    // vectorization related 
    private HashMap<Node, Integer> _actorVecDegrees;
    // scheduling related variables
    // maps an actor to some processor
    private HashMap<Node, Integer> _actorAssignments;
    private HashMap<Edge, Integer> _edgeMemspace;   // 0 for CPU, 1 for GPU 
    //private ArrayList<LoopedScheduleList> _scheduleLists; 

    private int _nThreads;
    //private PerformanceScheduler _scheduler; 
    //private ArrayList<LWDFLoopedSchedule> _loopedScheduleList; 
    // The string name used to repesent CPU-to-GPU memcpy actor
    private static final String _MEMCPY_H2D_NAME = "h2d";
    // The string name used to repesent CPU-to-GPU memcpy actor
    private static final String _MEMCPY_D2H_NAME = "d2h";
    // The string name used to represent the memcpy library actor
    private static final String _MEMCPY_NAME = "memcpy";

    private static final int _maxFIFOCapacityInBytes = (2 << 20);

    //  Generator flags
    private boolean _diag;  // diagnostic mode 

}
