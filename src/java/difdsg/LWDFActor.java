/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
package difdsg;
import dif.DIFAttribute;
import dif.csdf.sdf.SDFGraph;
import dif.util.graph.Edge;
import dif.util.graph.Node;
import difdsg.Port;

import java.util.*;

//TODO: for getting execution time, add something to support h2d and d2h actor execution 
// time queries. 

public class LWDFActor {
	/** Create an LWDFActor context
	 * @param node Node in DIF graph
	 * @param graph Graph
	 */
    public LWDFActor(Node node, SDFGraph graph, int id) {
    	/* Set names */
        _instanceName = graph.getName(node); 
        _actorName = getNodeName(node, graph);
        _node = node;
        _id = id;
		/* Parameters */
		_iterNum = getIterNum(node, graph);
		_fileName=getFileName(node,graph);
		_refName=getRefName(node,graph);
		_preRef=getPreRef(node,graph);
		_postRef=getPostRef(node,graph);
		_raExe=getRaExe(node,graph);

        /* load connections */
        initPortConnections(graph); 
        /* load parameters */
    	_parameterNames = new ArrayList<String>(); 
    	_parameterTypes = new ArrayList<String>();
    	getParameters(node, graph, _parameterNames, _parameterTypes);

    	/* load all construct function argument besides fifo list */
		initActorArgueVar(node, graph, _parameterNames, _parameterTypes);

		/* get RA auxiliary information */
		initRA(_parameterNames, _parameterTypes);
    }

    /********************* Set and get ******************/
    /* ID: the ID of the LWDF actor; also the index to refer to in actor array. */
    public void setID(int ID){
    	_id = ID; 
    }
    public int getID(){
    	return _id; 
    }
    public Node getNode() {
		return _node;
	}
	public void setNode(Node _node) {
		this._node = _node;
	}
    /* name: the name of LWDF actor that appears in output code. The macro is defined as 
    * #define LWDF_ACTOR_NAME [index]. */
    public void setName(String name){
    	_actorName = name; 
    }
    public String getName(){
    	return _actorName; 
    }
    public String getInstanceName(){
    	return _instanceName; 
    }
    public void setInstanceName(String name){
    	_instanceName = name; 
    }
    public String getFileName(){
    	return _fileName; 
    }
    public String setFileName(String name){
    	return _fileName=name; 
    }
    public String getRefName(){
    	return _refName; 
    }
    public String setPreRef(String name){
    	return _preRef=name; 
    }
    public String getPreRef(){
    	return _preRef; 
    }
    public String setPostRef(String name){
    	return _postRef=name; 
    }
    public String getPostRef(){
    	return _postRef; 
    }
    public String getRaExe(){
    	return _raExe; 
    }
    public String getIterNum(){
    	return _iterNum; 
    }
    public String setRefName(String name){
    	return _refName=name; 
    }
	public String getTokenSizeUnit(){
		return _tokenSizeUnit; 
	}
	public void setTokenSizeUnit(String tokenSizeUnit){
		_tokenSizeUnit = tokenSizeUnit; 
	}

	public ArrayList<String> getParameterNames(){
		ArrayList<String> params = (ArrayList<String>) _parameterNames.clone();
		return params; 
	}
	public ArrayList<String> getParameterTypes(){
		ArrayList<String> params = (ArrayList<String>) _parameterTypes.clone();
		return params; 
	}
	/* get input or output ports, input first */
	public ArrayList<Port> getPorts(int direction){
		ArrayList<Port> ports = new ArrayList<Port>();
		for (Port p : _ports){
			if (direction == p.direction) { // input ports
				ports.add(p); 
			}
		}
		return ports; 
	}
	public ArrayList<Port> getPorts(){
		ArrayList<Port> ports = new ArrayList<Port>();
		for (Port p : _ports){
			ports.add(p); 
		}
		return ports; 
	}
	
	/* Replace the port connection specified by oldEdge to newEdge */
	public void replacePort(Edge oldEdge, Edge newEdge){
		int idx = 0;
		int i = 0; 
		int size = _ports.size(); 
		for (i = 0; i < size; i++){
			Port p = _ports.get(i); 
			if (p.edge.equals(oldEdge)){
				idx = i; 
				break; 
			}
		}
		if (i == size){
			GlobalSettings.debugMessage("LWDFActor Warning: port not found");
		}
		Port port = _ports.get(idx); 
		port.edge = newEdge; 
	}
	
	private void initPortConnections(SDFGraph graph){
		/* parse the port_x specifications into two lists, input and output ports 
		* Create two maps for input ports (id --> edgeName),
		* then sort the keys */
		TreeMap<Integer, Edge> smap = new TreeMap<Integer, Edge>();
		HashMap<Integer, Integer> directionMap = new HashMap<Integer, Integer>(); 
		LinkedList<DIFAttribute> attrList = graph.getAttributes(_node);
		String EdgeName;
		Edge edge;
		Integer idx = 0;
		Integer outputSize = 0, inputSize = 0;
		for (DIFAttribute attr : attrList) {
			String attrName = attr.getName();
			String attrType = attr.getType();
			if ( (!isReservedWords(attrName)) && (attrType.equals("Edge"))) {
				if(attrName.toLowerCase().contains("out")){
					outputSize++;
				}else if(attrName.toLowerCase().contains("in")){
					inputSize++;
				}
			}
		}

    	for (DIFAttribute attr : attrList) {
    		String attrName = attr.getName(); 
    		String attrType = attr.getType();
			if ( (!isReservedWords(attrName)) && (attrType.equals("Edge"))) {

				if(attrName.toLowerCase().contains("out")){
					idx = inputSize + Integer.parseInt(attrName.substring(3))-1;
					directionMap.put(idx, 0);
				}else if(attrName.toLowerCase().contains("in")){
					idx = Integer.parseInt(attrName.substring(2))-1;
					directionMap.put(idx, 1);
				}else{
					GlobalSettings.debugMessage("LWDFActor Wrong format for port specification, skip this port");
					continue;
				}

				//find edge
				EdgeName = (String) attr.getValue();
				edge = graph.getEdge(EdgeName);
				Edge e = edge;
				smap.put(idx, e);

				//idx = idx + 1;
			}

		}
    	// initialize members, sorting port names by its numeric orders
    	_ports = new ArrayList<Port>();
    	for (Map.Entry<Integer, Edge> entry : smap.entrySet()) {
    		Integer index = entry.getKey();
    		Edge e = entry.getValue();
    		DIFAttribute typeAttr = graph.getAttribute(e, "edgeType"); 
            String typeName = (String) typeAttr.getValue();
    		Port p = new Port(index, e, directionMap.get(index), typeName); 
    		_ports.add(p); 
    	}
    }
	
	private String getNodeName(Node node, SDFGraph graph) {
        DIFAttribute attr = graph.getAttribute(node, "name");
        String nodeName = ""; 
        if (attr != null) {
        	nodeName += (String) attr.getValue();
        } else {
        	nodeName = "no_name"; 
        }
        return nodeName; 
    }
	
	private String getIterNum(Node node, SDFGraph graph) {
        DIFAttribute attr = graph.getAttribute(node, "iterNum");
        String nodeName = ""; 
        if (attr != null) {
        	nodeName += (String) attr.getValue();
        } else {
        	nodeName = "no_name"; 
        }
        return nodeName; 
    }
	
	private String getFileName(Node node, SDFGraph graph) {
        DIFAttribute attr;
        String nodeName;
		nodeName = "no_name";
        /*input file*/
		attr = graph.getAttribute(node, "input_file");
        if (attr != null) {
        	nodeName = (String) attr.getValue();
        }
        /*output file*/
		attr = graph.getAttribute(node, "output_file");
		if (attr != null) {
			nodeName = (String) attr.getValue();
		}
        return nodeName; 
    }
	
	private String getRefName(Node node, SDFGraph graph) {
        DIFAttribute attr = graph.getAttribute(node, "ref");
        String nodeName = ""; 
        if (attr != null) {
        	nodeName += (String) attr.getValue();
        } else {
        	nodeName = "no_name"; 
        }
        return nodeName;
	}
	
	private String getPreRef(Node node, SDFGraph graph) {
        DIFAttribute attr = graph.getAttribute(node, "pre_ref");
        String nodeName = ""; 
        if (attr != null) {
        	nodeName += (String) attr.getValue();
        } else {
        	nodeName = "NULL"; 
        }
        return nodeName;
	}
	
	private String getPostRef(Node node, SDFGraph graph) {
        DIFAttribute attr = graph.getAttribute(node, "post_ref");
        String nodeName = ""; 
        if (attr != null) {
        	nodeName += (String) attr.getValue();
        } else {
        	nodeName = "NULL"; 
        }
        return nodeName;
	}
	
	private String getRaExe(Node node, SDFGraph graph) {
        DIFAttribute attr = graph.getAttribute(node, "ra_exe");
        String nodeName = ""; 
        if (attr != null) {
        	nodeName += (String) attr.getValue();
        } else {
        	nodeName = _lide_c_prefix+"ra_exe_default";
        }
        return nodeName;
	}
	
    private boolean isReservedWords(String attrName){
    	if ((attrName.equals("name")) || attrName.equals("file") || attrName.equals("ref") ||
				attrName.equals("pre_ref")|| attrName.equals("post_ref")||attrName.equals("iterNum")||
				attrName.equals("ra_exe")) {
    		return true;
    	}
    	return false; 
    }
    
    private void getParameters(Node node, SDFGraph graph, 
    		ArrayList<String> paramNames, ArrayList<String> paramTypes){
    	paramNames.clear();
    	paramTypes.clear();
    	String nodeName = getNodeName(node, graph); 
    	LinkedList<DIFAttribute> attrList = graph.getAttributes(node); 
    	for (DIFAttribute attr : attrList) {
    		String attrName = attr.getName(); 
    		String attrType = attr.getType(); 
    		//if (!isReservedWords(attrName) && attrType.equals("PARAMETER")) {
    		if (!isReservedWords(attrName)) {
				paramNames.add(attrName);
				String paramType = attr.getValue().toString(); 
				paramTypes.add(paramType);
    		}
    	}
    }

	private void initActorArgueVar(Node node, SDFGraph graph,
								   ArrayList<String> paramNames, ArrayList<String> paramTypes){

		String _type = "argu";
		Integer startIndex = _type.length();
		HashMap<Integer, String> tmp = new HashMap();
		LinkedList<DIFAttribute> attrList = graph.getAttributes(node);

		_actorArguVar = new LinkedHashMap<Integer, String>();
		for (DIFAttribute attr : attrList) {
			String attrName = attr.getName();
			String attrType = attr.getType();
			if (isReservedWords(attrName)) {
				continue;
			}

			if (attrName.toLowerCase().contains(_type.toLowerCase())){
				String index_str = attrName.substring(startIndex);
				Integer para_index = Integer.parseInt(index_str);
				String argueType = (String)attr.getValue();
				argueType = argueType.replace("\"", "");
				tmp.put(para_index,argueType);
			}
		}
		/*Put ArgueVar List in order */
		for (int index = 1; index <= tmp.size(); index++){
			String val=(String)tmp.get(index);
			_actorArguVar.put(index,val);
		}

	}

	private void initRA(ArrayList<String> parameterNames, ArrayList<String> parameterTypes){
		if(parameterNames.isEmpty() || parameterTypes.isEmpty()) {
			_RArefID = Integer.toString(Integer.MIN_VALUE);
			_RArefInNum = 0;
			_RArefOutNum = 0;
			//RArefFIFOArray = "";
			return;
		}
		if (parameterNames.contains("refid")){
			int index = parameterNames.indexOf("refid");
			_RArefID = parameterTypes.get(index);
		}else{
			_RArefID = Integer.toString(Integer.MIN_VALUE);
			_RArefInNum = 0;
			_RArefOutNum = 0;
			return;
		}
		if (parameterNames.contains("ref_in_num")){
			int index = parameterNames.indexOf("ref_in_num");
			_RArefInNum = Integer.parseInt(parameterTypes.get(index));
		}else{
			_RArefInNum = 0;
		}
		if (parameterNames.contains("ref_out_num")){
			int index = parameterNames.indexOf("ref_out_num");
			_RArefOutNum = Integer.parseInt(parameterTypes.get(index));
		}else{
			_RArefOutNum = 0;
		}
		if (parameterNames.contains("ref_in_array")){
			int index = parameterNames.indexOf("ref_in_num");
			String paraValue = parameterTypes.get(index);
			String[] FIFOIndex = paraValue.split("\\s+");
			_RArefInArray = new ArrayList<String>(Arrays.asList(FIFOIndex));
		}
		if (parameterNames.contains("ref_out_array")){
			int index = parameterNames.indexOf("ref_out_array");
			String paraValue = parameterTypes.get(index);
			String[] FIFOIndex = paraValue.split("\\s+");
			_RArefOutArray = new ArrayList<String>(Arrays.asList(FIFOIndex));
		}
		return;

	}

	public LinkedHashMap<Integer, String> getActorArguVarList(){
		return _actorArguVar;
	}
	public String getActorArguVar(int index){
		return _actorArguVar.get(index);
	}
	public Integer getActorArguVarSize(){
		return _actorArguVar.size();
	}
	public Integer get_RArefID(){
		return Integer.parseInt(_RArefID);
	}

	public Integer get_RArefInNum(){
		return _RArefInNum;
	}

	public ArrayList<String> get_RArefInArray(){
		return _RArefInArray;
	}

	public Integer get_RArefOutNum(){
		return _RArefOutNum;
	}

	public ArrayList<String> get_RArefOutArray(){
		return _RArefOutArray;
	}


    /* Print info: print information about this LWDF actor */
    public void printContext(){
    	System.out.println("Name: " + _actorName);
    	System.out.println("Instance: " + _instanceName); 
    	System.out.println("ID: " + _id);
    	System.out.println("Parameters:"); 
    	for (int i = 0; i < _parameterNames.size(); i++){
    		String str = _parameterNames.get(i) + ":" + _parameterTypes.get(i); 
    		System.out.println(str); 
    	}
    	System.out.println("Connections:"); 
    	for (int i = 0; i < _ports.size(); i++){
    		String str = _ports.get(i).edge.toString();  
    		System.out.println(str); 
    	}
    }
    
	/************************** Members *****************************/

    private String _actorName;   /* name of the LWDF actor, but not the instance.*/
    private String _fileName; 
    private String _refName; 
    private String _iterNum; 
    private String _preRef; 
    private String _postRef; 
    private String _raExe; 
    private String _instanceName; //
    private String _refId;
    private int _id;
    private Node _node; 

    private ArrayList<String> _parameterNames;
    private ArrayList<String> _parameterTypes;
    private ArrayList<Port> _ports;

	private LinkedHashMap<Integer, String> _actorArguVar;
	private String _RArefID;
	private ArrayList<String> _RArefInArray;
	private ArrayList<String> _RArefOutArray;
	private int _RArefInNum;
	private int _RArefOutNum;

    // memcpy actor parameters
    private String _tokenSizeUnit;

    
	/************************** Members for code generation *****************************/

    private static String _lide_c_prefix = "lide_c_";
    private static String _lide_ocl_prefix = "lide_ocl_";
    private final static double _maxActorRuntime = 1000 * 3600 * 365; // maximum runtime of an actor: 1 year


}; 