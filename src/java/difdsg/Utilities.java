/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
package difdsg;
import java.io.File;


public class Utilities {
	public static void difdsg(String configFileName){

        int app_dif_file_length, dsg_dif_file_length;
        String app_dif_FileName, dsg_dif_FileName;
        boolean app_enable=false, dsg_enable=false;
        String lide_pkg = null;
		String app_graph_name = "";

	    /* Get information in config file*/
		DIFDSGtoLWDFXMLReader xmlReader = new DIFDSGtoLWDFXMLReader(configFileName);
		DIFDSGtoLWDFConfig config = xmlReader.getConfiguration();

        /* Create result directory*/
        String outputDir = config.resultDirectory+"/";
        File file = new File(outputDir);
        if (!file.exists()){
            file.mkdirs();
        }
        /* Get file name of app dif file (without .dif extension)*/
        if(!config.app_dif_File.equals("none")) {
            app_dif_file_length = config.app_dif_File.lastIndexOf("/");
            app_dif_FileName = config.app_dif_File.substring(app_dif_file_length + 1);
            app_dif_file_length = app_dif_FileName.lastIndexOf(".dif");
            app_dif_FileName = app_dif_FileName.substring(0, app_dif_file_length);
            app_enable = true;

        }
        /* Get file name of dsg dif file (without .dif extension)*/
        if(!config.dsg_dif_File.equals("none")) {
            dsg_dif_file_length = config.dsg_dif_File.lastIndexOf("/");
            dsg_dif_FileName = config.dsg_dif_File.substring(dsg_dif_file_length + 1);
            dsg_dif_file_length = dsg_dif_FileName.lastIndexOf(".dif");
            dsg_dif_FileName = dsg_dif_FileName.substring(0, dsg_dif_file_length);
            dsg_enable=true;
        }

		/*App graph ADT generation */
		DIFtoHierarchy app_h;
		if(app_enable) {
        	/* Get Hierarchy */
			/* App Graph Heirarchy and Get Graph Info*/
			app_h = new DIFtoHierarchy(config.app_dif_File,
					config.resultDirectory);
			System.out.println(config.resultDirectory);
			//System.out.println(app_h);

			/* Generate graph ADT*/
			switch (config.lide_pkg.toLowerCase()) {
				case "lide_c":
					LIDECAppCodeWriter lidec_codewriter_app = new LIDECAppCodeWriter(
																app_h, config.resultDirectory);
					lidec_codewriter_app.run();
					break;
				case "welt_cpp" :
					WELTCPPAppCodeWriter weltcpp_codewriter_app = new WELTCPPAppCodeWriter(
							app_h, config.resultDirectory);
					weltcpp_codewriter_app.run();
					break;
				case "welt_cpp_cdsg":
					WELTCPPAppCodeWriter weltcpp_cdsg_codewriter_app = new WELTCPPAppCodeWriter(
							app_h, config.resultDirectory);
					weltcpp_cdsg_codewriter_app.run();
				case "lide_ocl":
					System.out.println();
					break;
				default:
					System.out.println();
					break;
			}
			app_graph_name = app_h.graph.getName();
		}

        /* DSG graph ADT genearation and scheduler generation */
		DIFtoHierarchy dsg_h;
		if(app_enable && dsg_enable ) {

			/* Code generator */
			/* Get Hierarchy */
			/* App Graph Heirarchy and Get Graph Info*/
			dsg_h = new DIFtoHierarchy(config.dsg_dif_File,
					config.resultDirectory);

			/* Generate graph ADT*/
			switch (config.lide_pkg.toLowerCase()) {
				case "lide_c":
					LIDECDSGCodeWriter lidec_codewriter_dsg = new LIDECDSGCodeWriter(
							dsg_h, app_graph_name, config.resultDirectory);
					lidec_codewriter_dsg.run();
					break;
				case "welt_cpp":
					WELTCPPDSGCodeWriter weltcpp_codewriter_dsg = new WELTCPPDSGCodeWriter(
							dsg_h, app_graph_name, config.resultDirectory);
					weltcpp_codewriter_dsg.run();
					break;
				case "welt_cpp_cdsg":
					WELTCPPCDSGCodeWriter weltcpp_cdsg_codewriter_dsg = new WELTCPPCDSGCodeWriter(
							dsg_h, app_graph_name, config.resultDirectory);
					weltcpp_cdsg_codewriter_dsg.run();
					break;
				case "lide_ocl":
					System.out.println();
					break;
				case "lide_cdsg":
					LIDECCDSGCodeWriter lidec_cdsg_codewriter_dsg = new LIDECCDSGCodeWriter(
							dsg_h, app_graph_name, config.resultDirectory);
					lidec_cdsg_codewriter_dsg.run();
					break;
				default:
					System.out.println();
					break;
			}

		}
	}

}
