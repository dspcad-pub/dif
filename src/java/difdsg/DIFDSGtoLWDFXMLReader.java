/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
package difdsg;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.File;

public class DIFDSGtoLWDFXMLReader {
    /*
    public DIFDSGtoLWDFXMLReader(){
        _configuration = new DIFDSGtoLWDFConfig();
    }
    */
    public DIFDSGtoLWDFXMLReader(String xmlFileName) {
        _configuration = new DIFDSGtoLWDFConfig();
        readXML(xmlFileName);
    }

    public DIFDSGtoLWDFConfig getConfiguration() {
        return _configuration;
    }

    public boolean readXML(String xml) {
        Document dom;
        /* Make an  instance of the DocumentBuilderFactory */
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            /* use the factory to take an instance of the document builder */
            DocumentBuilder db = dbf.newDocumentBuilder();
            File file = new File(xml);
            InputStream inputStream = new FileInputStream(file);
            /* parse using the builder to get the DOM mapping of the
            XML file */
            dom = db.parse(inputStream);
            //dom = db.parse(xml);
            Element doc = dom.getDocumentElement();
            System.out.println("app_dif: "+getTextValue(doc, "app_dif_name"));
            System.out.println("dsg_dif_name: "+getTextValue(doc, "dsg_dif_name"));
            System.out.println("result_dir: "+getTextValue(doc, "result_dir"));

            if (getTextValue(doc, "app_dif_name").equals("")) {
                _configuration.app_dif_File = "none";
            } else {
                _configuration.app_dif_File = getTextValue(doc, "app_dif_name");
            }

            if (getTextValue(doc, "dsg_dif_name").equals("")) {
                _configuration.dsg_dif_File = "none";
            } else {
                _configuration.dsg_dif_File = getTextValue(doc, "dsg_dif_name");
            }

            _configuration.resultDirectory = getTextValue(doc, "result_dir");
            _configuration.lide_pkg = getTextValue(doc, "lide_pkg");

            _configuration.schedulerName = getTextValue(doc, "scheduler");
            _configuration.profileDirectory = getTextValue(doc, "profile_dir");

            _configuration.h2dProfile = getTextValue(doc, "h2d_file");
            _configuration.d2hProfile = getTextValue(doc, "d2h_file");

            String nThreadsStr = getTextValue(doc, "n_threads");
            nThreadsStr = nThreadsStr.replaceAll("[\\s]", "");
            _configuration.numberOfThreads = Integer.parseInt(nThreadsStr);

            String nGPUStr = getTextValue(doc, "n_gpu");
            nGPUStr = nGPUStr.replaceAll("[\\s]", "");
            _configuration.numberOfGPUs = Integer.parseInt(nGPUStr);

            String vecString = getTextValue(doc, "vec_degrees");
            String values[] = vecString.split(",");
            for (int i = 0; i < values.length; i++) {
                int vec = Integer.parseInt(values[i].replaceAll("[\\s]", ""));
                _configuration.vecDegrees.add(vec);
            }

            String repetitionStr = getTextValue(doc, "repetitions");
            repetitionStr = repetitionStr.replaceAll("[\\s]", "");
            _configuration.repetitions = Integer.parseInt(repetitionStr);

            return true;
        } catch (ParserConfigurationException pce) {
            System.out.println(pce.getMessage());
        } catch (SAXException se) {
            System.out.println(se.getMessage());
        } catch (IOException ioe) {
            System.err.println(ioe.getMessage());
        }
        return false;
    }

    private String getTextValue(Element doc, String tag) {
        String value = "";
        NodeList nl = doc.getElementsByTagName(tag);
        if (nl.getLength() > 0 && nl.item(0).hasChildNodes()) {
            value = nl.item(0).getFirstChild().getNodeValue();
        }
        return value;
    }

    private DIFDSGtoLWDFConfig _configuration;
}
