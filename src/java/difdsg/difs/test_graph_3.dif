/****************************************************************************
Copyright (c) 1999-2007 
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
******************************************************************************/
/* A test graph: 
   256      256  
src---->fir----->snk
      256       256
@author Shuoxin Lin
@version $Id$
*/

sdf fir_graph_1 {
    topology {
        nodes = src, fir, snk;
        edges = e1 (src,fir), e2 (fir,snk); 
    }
    production {
        e1 = 256;
        e2 = 256;
    }
    consumption {
        e1 = 256;
        e2 = 256;
    }

    attribute edgeType {
        e1 = "float"; 
        e2 = "float"; 
    }

    actor src {
        name = "src_1f";
        port_0 : OUTPUT = e1; 
    }
    actor fir {
        name = "fir";
        CUDAEnabled = 1; 
        port_0 : INPUT = e1; 
        port_1 : OUTPUT = e2;
	filter : PARAMETER = "float*";  
	ntaps : PARAMETER = "int"; 
    }
    actor snk {
        name = "snk_1f";
        port_0 : INPUT = e2; 
    } 
}

