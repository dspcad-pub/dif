/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
package difdsg;
import dif.DIFParameter;
import dif.csdf.sdf.SDFGraph;
import dif.util.graph.Edge;
import dif.util.graph.Node;

import java.util.*;

public class WELTCPPCDSGCodeWriter {

    WELTCPPCDSGCodeWriter(DIFtoHierarchy dsg_h, String app_graph, String outputDir) {
        LWDFActor actor;
        /* Parameter initialization*/
        _dsg_h = dsg_h;
        _graph = _dsg_h.graph;
        _lwdfGraph = dsg_h.lwdf_graph;
        _contextMap = _dsg_h.nodeMap;
        _edgeMap = _dsg_h.edgeMap;
        _outputDir = outputDir;
        _appGraph_Name = app_graph;
        _dsgGraph_Name = _graph.getName();
        _dsgGraphHeader_Name = _outputDir  + _dsgGraph_Name + ".h";
        _dsgGraphSource_Name = _outputDir  + _dsgGraph_Name + ".cpp";
        _dsgGraphName = _dsgGraph_Name;
        DIFParameter param = _graph.getParameter("numberOfThread");
        String strNum = param.getValue().toString();
        _numberOfThread = Integer.parseInt(strNum);
        /* Check all actor types*/
        _ActorType = new HashSet<String>();
        for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()) {
            actor = entry.getValue();
            _ActorType.add(actor.getName());
        }
        /* Create files*/
        CreateGraphADTFiles();
        /* Get argument list for New function and actor New extra argument list*/
        InitGraphNewArguList(_lwdfGraph);
        InitActorNewArguList(_lwdfGraph);

    }

    public void run() {

        /*Create Header */
        WriteGraphHeader();
        /* Create Source*/
        WriteGraphSource();

    }

    private void CreateGraphADTFiles() {
        try {
            _gcm_h = new Gcm(_dsgGraphHeader_Name, false);
            _gcm_s = new Gcm(_dsgGraphSource_Name, false);
            _gcm_h.close();
            _gcm_s.close();
        } catch (Exception e) {
            System.out.println("Error: Cannot find " + _dsgGraphHeader_Name + "or" + _dsgGraphSource_Name);
            e.printStackTrace();
        }
    }

    private void WriteGraphHeader() {
        int i;
        _gcm_h = new Gcm(_dsgGraphHeader_Name, true);
        /*start macro ifdef*/
        _gcm_h.writeLine("#ifndef _"  + _dsgGraphName + "_h");
        _gcm_h.writeLine("#define _"  + _dsgGraphName + "_h");
        _gcm_h.writeLine("");

        /*include header files*/
        //General header files
        _gcm_h.writeLine("#include <stdio.h>");
        _gcm_h.writeLine("#include <stdlib.h>");
        _gcm_s.writeLine("#include <pthread.h>");

        /* common headers */
        _gcm_h.writeLine("extern \"C\"{");
        _gcm_h.writeLine("#include \"welt_c_util.h\"");
        _gcm_h.writeLine("#include \"welt_c_fifo.h\"");
        _gcm_h.writeLine("#include \"welt_c_fifo_unit_size.h\"");

        _gcm_h.writeLine("}");

        _gcm_h.nextLine();
        _gcm_h.writeLine("#include \"" + _welt_cpp_prefix + "graph.h\"");

        _gcm_h.writeLine("#include \"" + _welt_cpp_prefix + "dsg.h\"");

        _gcm_h.nextLine();

        /* actor headers */
        for (String actortype : _ActorType) {
            _gcm_h.writeLine("#include \"" + _welt_cpp_prefix + actortype + ".h\"");
        }
        _gcm_h.nextLine();

        /* app graph header */
        _gcm_h.writeLine("#include \""  + _appGraph_Name + "_graph.h\"");
        _gcm_h.nextLine();

        /* macro definitions */
        /* Buffer capacity */
        _gcm_h.writeLine("#define DSG_BUFFER_CAPACITY 1024");
        _gcm_h.nextLine();
        /* Actor Index */
        i = 0;
        for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()) {
            LWDFActor actor = entry.getValue();
            _gcm_h.writeLine("#define DSG_ACTOR_" + actor.getInstanceName().toUpperCase() + "    " + actor.getID());
            i++;
        }
        _gcm_h.nextLine();
        i = 0;
        for (Map.Entry<Edge, LWDFEdge> entry : _edgeMap.entrySet()) {
            LWDFEdge edge = entry.getValue();
            _gcm_h.writeLine("#define DSG_FIFO_" + edge.getName().toUpperCase() + "    " + edge.getID());
            i++;
        }
        _gcm_h.nextLine();
        /* Count of FIFOs and actors */
        _gcm_h.writeLine("#define DSG_ACTOR_COUNT    " + _contextMap.size());
        _gcm_h.writeLine("#define DSG_FIFO_COUNT    " + _edgeMap.size());
        _gcm_h.writeLine("#define NUM_THREAD    " + _numberOfThread);

        _gcm_h.nextLine();

        /* Graph context definition*/
        _gcm_h.writeLine("class "  + _dsgGraphName + ": public welt_cpp_dsg{");
        _gcm_h.writeLine("public:");

        /* Function declaration*/
        _gcm_h.write( _dsgGraphName + "(");

        if(!_graphNewArguList.isEmpty()) {
            _gcm_h.write(_graphNewArguList);
        }
        _gcm_h.write(");");
        _gcm_h.next();
        _gcm_h.nextLine();
        _gcm_h.write("void dsg_scheduler(int thread_ind, int thread_num);\n");
        _gcm_h.write("int thread_ind = 0;\n");
        _gcm_h.write("void scheduler(); ");
        _gcm_h.next();
        _gcm_h.nextLine();

        _gcm_h.writeLine("};");
        /*end macro endif*/
        _gcm_h.writeLine("#endif");
        _gcm_h.flush1();
        _gcm_h.close();
    }

    private void WriteGraphSource() {

        _gcm_s = new Gcm(_dsgGraphSource_Name, true);

        /* Header files */
        WriteGraphSourceHeaderFiles();

        /* Construct functions */
        WriteConstructFunction();

        /* Scheduler */
        WriteSchedulerFunction();
        _gcm_s.flush1();
        _gcm_s.close();

    }

    private void WriteGraphSourceHeaderFiles() {
        _gcm_s.writeLine("#include <stdio.h>");
        _gcm_s.writeLine("#include <stdlib.h>");
        _gcm_s.writeLine("#include <string.h>");
        _gcm_s.writeLine("#include \"" + _welt_cpp_prefix + "dsg_scheduler.h\"");
        _gcm_s.writeLine("#include \"" + _welt_cpp_prefix + "actor.h\"");
        _gcm_s.writeLine("#include \""  + _dsgGraphName + ".h\"");
        _gcm_s.nextLine();
        _gcm_s.nextLine();

    }

    private void WriteConstructFunction() {
        int i;

        _gcm_s.write(  _dsgGraphName+"::"+ _dsgGraphName + " (");

        if(!_graphNewArguList.isEmpty()) {
            _gcm_s.write(_graphNewArguList);
        }

        _gcm_s.write("){");
        _gcm_s.next();

        _gcm_s.incIndent();

        /* Variable definition */
        _gcm_s.writeLine("this->dsg_count = DSG_ACTOR_COUNT;");
        _gcm_s.writeLine("this->thread_count = NUM_THREAD;");

        _gcm_s.next();

        _gcm_s.writeLine("this -> first_actors_index.reserve(this->thread_count);\n");
        _gcm_s.writeLine("this->set_fifo_count(DSG_FIFO_COUNT);\n");
        _gcm_s.writeLine("this->set_actor_count(DSG_ACTOR_COUNT);\n");
        _gcm_s.writeLine("this->actors.reserve(DSG_ACTOR_COUNT);\n");
        _gcm_s.writeLine("this->fifos.reserve(DSG_FIFO_COUNT);");
        _gcm_s.writeLine("this->descriptors.reserve(this->actor_count);");

        _gcm_s.writeLine("(this->source_array).reserve(this->fifo_count);");
        _gcm_s.writeLine("(this->sink_array).reserve(this->fifo_count);");

        _gcm_s.writeLine("for (int i =0; i<DSG_FIFO_COUNT; i++){");
        _gcm_s.incIndent();
        _gcm_s.writeLine("(this->source_array).push_back(nullptr);");
        _gcm_s.writeLine("(this->sink_array).push_back(nullptr);");
        _gcm_s.decIndent();
        _gcm_s.writeLine("}");

        _gcm_s.next();
        _gcm_s.writeLine("int token_size;");

        /* file name assignment */
        for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()) {
            if (!entry.getValue().getFileName().equals("no_name")) {
                String file = entry.getValue().getInstanceName();
                _gcm_s.write("this->" + file + "_file=" + "\"" + file + ".txt\";");
                _gcm_s.next();
            }
        }
        _gcm_s.nextLine();


        for (Map.Entry<Edge, LWDFEdge> entry : _edgeMap.entrySet()) {

            LWDFEdge edge = entry.getValue();
            String edgeType = edge.getEdgeType();
            int capacity = edge.getCapacity();
            _gcm_s.write("token_size = sizeof(");
            _gcm_s.write(edgeType + ");");
            _gcm_s.next();
            _gcm_s.write("this->fifos[" + "DSG_FIFO_" + edge.getName().toUpperCase() + "] = ");
            _gcm_s.write("(" + _welt_c_prefix + "fifo_pointer)");
            _gcm_s.write(_welt_c_prefix + "fifo_unit_size_new(");
            _gcm_s.write("token_size, ");
            _gcm_s.write("DSG_FIFO_" + edge.getName().toUpperCase() + ");");
            _gcm_s.next();

        }
        _gcm_s.nextLine();
        /* actor array */
        _firstActors = new TreeMap< Integer, LWDFActor>();

        for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()) {

            LWDFActor actor = entry.getValue();
            String actorName = actor.getInstanceName();
            String actorType = actor.getName();
            String fileName = actor.getFileName();
            int actorId = actor.getID();

            /* Get input port and output port */
            ArrayList<Port> inputPorts = actor.getPorts(1);
            ArrayList<Port> outputPorts = actor.getPorts(0);
            ArrayList<String> inputPortNames = new ArrayList<String>();
            ArrayList<String> outputPortNames = new ArrayList<String>();
            for (Port inPort : inputPorts) {
                Edge inEdge = inPort.edge;
                inputPortNames.add(_graph.getName(inEdge));
            }

            for (Port outPort : outputPorts) {
                Edge outEdge = outPort.edge;
                outputPortNames.add(_graph.getName(outEdge));
            }

            /* write new function for each actor */
            /* argument list: input fifos, output fifos and actor index */
            _gcm_s.write("this->actors[" + "DSG_ACTOR_" + actorName.toUpperCase() + "] = ");
            _gcm_s.write("(" + _welt_cpp_prefix + "actor *) ");
            _gcm_s.write("( new " + _welt_cpp_prefix + actorType.toLowerCase() + "(");

            /* Extra Argument List */
            String actor_argu_list = _actorNewExtraArguList.get(actor);
            if (actor_argu_list != null)
                _gcm_s.write(actor_argu_list);

            /* actor index */
            _gcm_s.write("));");
            _gcm_s.next();

            /* write connect function */
            _gcm_s.write("this->dsg_add_actor(this,this->actors[ " + "DSG_ACTOR_" + actorName.toUpperCase() + "], ");
            _gcm_s.write("DSG_ACTOR_" + actorName.toUpperCase()+ ");\n");

            _gcm_s.next();
            _gcm_s.nextLine();

            if (actorType.equals("sca_sch_loop")) {
                _gcm_s.write("welt_c_fifo_write_advance(( " + _welt_c_prefix + "fifo_pointer)this->fifos[DSG_FIFO_" + inputPortNames.get(0).toString().toUpperCase() + "]);\n");
            }

        }

        for (i=0;i<_numberOfThread; i++){
            _gcm_s.write("this->set_first_actor( this->actors[DSG_ACTOR_LAE"+(i+1)+"],DSG_ACTOR_LAE"+(i+1)+");\n");
        }

        for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()) {

            LWDFActor actor = entry.getValue();
            String actorName = actor.getInstanceName();
            String actorType = actor.getName();
            String fileName = actor.getFileName();
            if(!actorType.equals("ref_actor")){
                continue;
            }
            int in_num = actor.get_RArefInNum();
            int out_num = actor.get_RArefOutNum();
            ArrayList<String> in_fifo = actor.get_RArefInArray();
            ArrayList<String> out_fifo = actor.get_RArefOutArray();

            if(out_num > 0){
                _gcm_s.write(_welt_cpp_prefix + actorType.toLowerCase() + "_set_selected_output_count(");
                _gcm_s.write("("+_welt_cpp_prefix + actorType.toLowerCase() + " *)");
                _gcm_s.write("this->actors[" + "DSG_ACTOR_" + actorName.toUpperCase() + "], ");
                _gcm_s.write(Integer.toString(out_num) + ");");
                _gcm_s.next();
                _gcm_s.nextLine();
                for(int idx = 0; idx<out_num; idx++){
                    _gcm_s.write(_welt_cpp_prefix + actorType.toLowerCase() + "_set_selected_app_gr_outputs(");
                    _gcm_s.write("("+_welt_cpp_prefix + actorType.toLowerCase() + "_context_type *)");
                    _gcm_s.write("this->actors[" + "DSG_ACTOR_" + actorName.toUpperCase() + "], ");
                    _gcm_s.write(_app_context+ "->fifos[" + out_fifo.get(idx) + "], ");
                    _gcm_s.write(Integer.toString(idx) + ");");
                    _gcm_s.next();
                    _gcm_s.nextLine();
                }
            }
        }
        _gcm_s.decIndent();
        _gcm_s.writeLine("}");
        _gcm_s.nextLine();
    }


    private void WriteSchedulerFunction() {
        String sch = _lwdfGraph.getGraphSchedule();
        _gcm_s.write("void "  + _dsgGraphName + "::dsg_scheduler(int thread_ind, int thread_num){");
        _gcm_s.next();
        _gcm_s.incIndent();

        /* Decide which scheduler to use */
        if(sch.isEmpty()){
            _gcm_s.writeLine("int start_ind;");
            _gcm_s.writeLine("int end_ind;");
            _gcm_s.writeLine("start_ind = this->first_actors_index[thread_ind];");
            _gcm_s.writeLine(" if (thread_ind != thread_num -1){");
            _gcm_s.incIndent();
            _gcm_s.writeLine(" end_ind =this->first_actors_index[thread_ind+1] - 1;");
            _gcm_s.decIndent();
            _gcm_s.writeLine("  }else{");
            _gcm_s.writeLine("  end_ind = this->dsg_count -1;}");
        }else {
            switch (sch) {
                case "cdsg_sch":
                    _gcm_s.write(_welt_cpp_prefix+
                            "util_cdsg_scheduler( this->actors, start_ind, end_ind, this->descriptors" );
                    break;
                case "dsg_optimized":
                    _gcm_s.writeLine("int start_ind;");
                    _gcm_s.writeLine("int end_ind;");
                    _gcm_s.writeLine("start_ind = this->first_actors_index[thread_ind];");
                    _gcm_s.writeLine(" if (thread_ind != thread_num -1){");
                    _gcm_s.incIndent();
                    _gcm_s.writeLine(" end_ind =this->first_actors_index[thread_ind+1] - 1;");
                    _gcm_s.decIndent();
                    _gcm_s.writeLine("  }else{");
                    _gcm_s.writeLine("  end_ind = this->dsg_count -1;}");
                    _gcm_s.write(_welt_cpp_prefix +
                            "util_dsg_optimized_scheduler(this, start_ind, end_ind-start_ind+1, (this->sink_array));");
                default:
                    break;
            }
        }
        _gcm_s.next();
        _gcm_s.writeLine("return;");
        _gcm_s.decIndent();
        _gcm_s.writeLine("}");
        _gcm_s.writeLine("void "  + _dsgGraphName + "::scheduler(){");
        _gcm_s.incIndent();
        _gcm_s.writeLine("return;");
        _gcm_s.decIndent();
        _gcm_s.writeLine("}");

    }

    /* _graphNewArguList example: "int len, char * filename, double value" */
    private void InitGraphNewArguList(LWDFGraph lwdfGraph) {
        _graphNewArguList = new String("");
        LinkedHashMap<Integer, String> lwdfgraphArguType = _lwdfGraph.getGraphArguTypeList(_lwdfGraph);
        LinkedHashMap<Integer, String> lwdfgraphArguVar = _lwdfGraph.getGraphArguVarList(_lwdfGraph);
        if (lwdfgraphArguType == null || lwdfgraphArguVar == null){
            _graphNewArguList = "";
            return;
        }
        for (Map.Entry<Integer, String> entry : lwdfgraphArguType.entrySet()) {
            Integer Index = entry.getKey();
            String arguType = entry.getValue();
            String arguVar = lwdfgraphArguVar.get(Index);
            _graphNewArguList = _graphNewArguList.concat(arguType);
            _graphNewArguList = _graphNewArguList.concat(" ");
            _graphNewArguList = _graphNewArguList.concat(arguVar);
            _graphNewArguList = _graphNewArguList.concat(", ");
        }
        int len = _graphNewArguList.length();
        if(len < 2){
            _graphNewArguList = "";
            return;
        }
        len = len-2;
        _graphNewArguList = _graphNewArguList.substring(0, len);
    }


    /* arguList example: "int len, char * filename, double value" */
    private void InitActorNewArguList(LWDFGraph lwdfGraph) {

        _actorNewExtraArguList = new HashMap<LWDFActor, String>();
        /* iterate for each actor */
        for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()) {
            LWDFActor actor = entry.getValue();
            String arguList = "";
            LinkedHashMap<Integer, String> arguListMap = actor.getActorArguVarList();
            /* sort by key */
            Map<Integer, String> sortedarguListMap = new TreeMap<Integer, String>(arguListMap);
            if(sortedarguListMap != null || sortedarguListMap.size() != 0) {
                //iterate for each argument
                for (Map.Entry<Integer, String> argu : sortedarguListMap.entrySet()) {
                    String arg_val = argu.getValue();
                    String argu_str;
                    if(arg_val == null || arg_val == "NULL"){
                        arguList = arguList.concat("NULL");
                        arguList = arguList.concat(", ");
                        continue;
                    }
                    if(arg_val.contains("fifo_")){
                        // fifo argument
                        String fifo_name =arg_val.substring(5);
                        argu_str = "this->fifos[DSG_FIFO_" + fifo_name.toUpperCase() + "]";

                    }else if(arg_val.contains("actor_index")) {
                        // index argument
                        argu_str = "DSG_ACTOR_"+actor.getInstanceName().toUpperCase();
                    }else if(arg_val.contains("ref_")){
                        String ref_actor_name =arg_val.substring(4);
                        argu_str = _app_context+ "->actors[ACTOR_" + ref_actor_name.toUpperCase() + "]";
                    }
                    else{
                        /* rest arguments */
                        argu_str = arg_val;
                    }
                    /* concatenate argument */
                    arguList = arguList.concat(argu_str);
                    arguList = arguList.concat(", ");
                }
            }else{
                arguList = "";
            }

            int len = arguList.length();
            if(len < 2){
                arguList = "";
                return;
            }
            len = len-2;
            arguList = arguList.substring(0, len);
            _actorNewExtraArguList.put(actor, arguList);
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    ////                           Members                                ////
    private SDFGraph _graph;
    private DIFtoHierarchy _dsg_h;
    private HashMap<Node, LWDFActor> _contextMap;
    private HashMap<Edge, LWDFEdge> _edgeMap;
    private LWDFGraph _lwdfGraph;
    private String _outputDir;
    private String _appGraph_Name;
    private String _dsgGraph_Name;
    private String _dsgGraphName;
    private String _dsgGraphHeader_Name;
    private String _dsgGraphSource_Name;
    private int _numberOfThread;
    private Gcm _gcm_h;
    private Gcm _gcm_s;
    private String _welt_cpp_prefix = "welt_cpp_";
    private String _welt_c_prefix= "welt_c_";
    private String _graphNewArguList;
    private HashMap<LWDFActor, String> _actorNewExtraArguList;
    private TreeMap<Integer, LWDFActor> _firstActors;
    private String _app_context = "app_graph";
    private Set<String> _FirstActors;
    private Set<String> _FirstActorsInd;
    private Set<String> _ActorType;
}
