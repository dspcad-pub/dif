/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
package difdsg;
import dif.csdf.sdf.SDFGraph;
import dif.util.graph.Edge;
import dif.util.graph.Node;
import dif2lide.*;
import dif2lide.Gcm;
import dif2lide.LWDFActor;
import dif2lide.LWDFEdge;
import dif2lide.Port;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LIDECodeWriter {


	LIDECodeWriter(SDFGraph graph, SDFGraph dsggraph, HashMap<Node, LWDFActor> contextMap, HashMap<Node, LWDFActor> dsgContextMap
			, HashMap<Edge, LWDFEdge> fifoMap, HashMap<Edge, LWDFEdge> dsgFifoMap, String outputDir){
		_graph = graph;
		_dsggraph= dsggraph;
		String graphName = _graph.getName();
		String dsgGraphName = _dsggraph.getName();
		writeApplGraphDefines(outputDir,contextMap,fifoMap);
		writeDsgGraphDefines(outputDir,dsgContextMap,dsgFifoMap);
		writeSchduleDefines(outputDir);

		makeme(outputDir);
		runme(outputDir,graphName);
		if (!checkExists(outputDir, "dloclconfig")) {
			dloclconfig(outputDir,graphName);
		}

		writeSchdule(outputDir,dsgContextMap);

		String applGraphFileName = "lide_ocl_"+ graphName + "_driver.cpp";
		String dsgGraphFileName = "lide_ocl_"+ dsgGraphName + "_schedule.cpp";
		String applHeaderName = "lide_ocl_"+ graphName + "_graph.h";
		String dsgHeaderFileName = "lide_ocl_"+ dsgGraphName + "_graph.h";

		_applGraphFileWriter = new ApplGraphFileWriter(outputDir + applGraphFileName, applHeaderName,
				graph, contextMap, fifoMap);

		_dsgGraphFileWriter = new DsgGraphFileWriter(outputDir+dsgGraphFileName,dsgHeaderFileName,graph,
				dsggraph,contextMap,dsgContextMap,fifoMap,dsgFifoMap);
	}

	public boolean checkExists(String directory, String file) {
	    File dir = new File(directory);
	    File[] dir_contents = dir.listFiles();
	    String temp = file;
	    boolean check = new File(directory, temp).exists();
	    return check;
	}
	private void writeSchduleDefines(String Dir){
		int i=0;
		Gcm _gcm = new Gcm(Dir+"lide_ocl_"+_dsggraph.getName()+"_scheduler.h");

		_gcm.writeLine("#ifndef _lide_ocl_"+_graph.getName()+"_scheduler_");
		_gcm.writeLine("#define _lide_ocl_"+_graph.getName()+"_scheduler_");
		_gcm.writeLine("");

		_gcm.writeLine("#include <stdio.h>");
		_gcm.writeLine("#include <stdlib.h>");
		_gcm.writeLine("#include <string.h>");
		_gcm.writeLine("#include \"lide_ocl_basic.h\"");
		_gcm.writeLine("#include \"lide_ocl_actor.h\"");
		_gcm.writeLine("#include \"lide_ocl_"+_graph.getName()+"_graph.h\"");
		_gcm.writeLine("#include \"lide_ocl_"+_dsggraph.getName()+"_graph.h\"");
		_gcm.writeLine("#include \"lide_ocl_sca_sch_loop.h\"");
		_gcm.writeLine("");

		_gcm.write("boolean ");
		_gcm.write("lide_ocl_util_dsg_optimised_scheduler(");
		_gcm.write("lide_ocl_graph_context_type*");
		_gcm.write(" context);");
		_gcm.next();
		_gcm.writeLine("");

		_gcm.write("void ");
		_gcm.write("lide_ocl_utl_simple_dsc_scheduler(");
		_gcm.write("lide_ocl_graph_context_type*");
		_gcm.write(" context);");
		_gcm.next();
		_gcm.writeLine("");

		_gcm.write("boolean ");
		_gcm.write("lide_ocl_util_dsg_optimised_scheduler_with_loop_cluster(");
		_gcm.write("lide_ocl_graph_context_type*");
		_gcm.write(" context);");
		_gcm.next();
		_gcm.writeLine("");

		_gcm.write("boolean ");
		_gcm.write("lide_ocl_util_dsg_optimised_scheduler_with_cluster(");
		_gcm.write("lide_ocl_graph_context_type*");
		_gcm.write(" context);");
		_gcm.next();
		_gcm.writeLine("");

		_gcm.write("boolean ");
		_gcm.write("lide_ocl_util_dsg_optimised_scheduler_with_loop(");
		_gcm.write("lide_ocl_graph_context_type*");
		_gcm.write(" context);");
		_gcm.next();
		_gcm.writeLine("");

		_gcm.write("boolean ");
		_gcm.write("lide_ocl_utl_guarded_execution(");
		_gcm.write("lide_ocl_actor_context_type*");
		_gcm.write(" context);");
		_gcm.next();
		_gcm.writeLine("");

		_gcm.writeLine("#endif");

		_gcm.flush1();
		_gcm.close();

	}
	private void writeApplGraphDefines(String Dir, HashMap<Node,LWDFActor>contextMap, HashMap<Edge,LWDFEdge>fifoMap){

		int i=0;
		Gcm _gcm = new Gcm(Dir+"lide_ocl_"+_graph.getName()+"_graph.h");
		//_gcm.nextLine();
		_gcm.writeLine("#ifndef _lide_ocl_"+_graph.getName()+"_graph_");
		_gcm.writeLine("#define _lide_ocl_"+_graph.getName()+"_graph_");
		_gcm.nextLine();

		_gcm.writeLine("#include \"lide_ocl_basic.h\"");
		_gcm.writeLine("#include \"lide_ocl_actor.h\"");
		_gcm.writeLine("#include \"lide_ocl_fifo_basic.h\"");
		_gcm.writeLine("#include \"lide_ocl_fifo.h\"");
		_gcm.writeLine("#include \"lide_ocl_graph.h\"");
		_gcm.writeLine("#include \"lide_ocl_util.h\"");
		for (Map.Entry<Node, LWDFActor> entry : contextMap.entrySet()){

        	LWDFActor actor = entry.getValue();
        	_gcm.writeLine("#include \"lide_ocl_"+actor.getName()+".h\"" );
		}

		_gcm.nextLine();
		_gcm.writeLine("#define BUFFER_CAPACITY 1024");
		_gcm.nextLine();

		for (Map.Entry<Node, LWDFActor> entry : contextMap.entrySet()){

        	LWDFActor actor = entry.getValue();
        	_gcm.writeLine("#define ACTOR_"+actor.getInstanceName().toUpperCase()+" "+i);
        	i++;
		}
		_gcm.nextLine();

		i=0;
		for (Map.Entry<Edge, LWDFEdge > entry : fifoMap.entrySet()){

        	LWDFEdge edge = entry.getValue();
        	_gcm.writeLine("#define FIFO_"+edge.getName().toUpperCase()+" "+ i );
        	i++;
		}
		_gcm.nextLine();

		_gcm.writeLine("#define ACTOR_COUNT "+ contextMap.size());
		_gcm.writeLine("#define FIFO_COUNT "+ fifoMap.size());
		_gcm.nextLine();

		i=0;

		_gcm.writeLine("struct _lide_ocl_"+_graph.getName()+"_graph_context_struct;");
		_gcm.write("typedef ");
		_gcm.write("struct");
		_gcm.write(" _lide_ocl_"+_graph.getName()+"_graph_context_struct");
		_gcm.write("lide_ocl_"+_graph.getName()+"_graph_context_type;");
		_gcm.next();
		_gcm.nextLine();

		_gcm.write("lide_ocl_"+_graph.getName()+"_graph_context_type *");
		_gcm.write("lide_ocl_"+_graph.getName()+"_graph_new(");
		_gcm.write("lide_ocl_gpu_pointer gpu);");
		_gcm.next();
		_gcm.nextLine();

		_gcm.write("void");
		_gcm.write(" lide_ocl_"+_graph.getName()+"_graph_terminate(");
		_gcm.write("lide_ocl_"+_graph.getName()+"_graph_context_type *");
		_gcm.write("context);");
		_gcm.next();
		_gcm.writeLine("#endif");
		_gcm.flush1();
		_gcm.close();
	}

	private void writeSchdule(String outputDir, HashMap<Node, LWDFActor> dsgContextMap){
		Gcm _gcm = new Gcm(outputDir+"lide_ocl_"+_dsggraph.getName()+"_scheduler.cpp");
		HashMap<String,String> snkArray = new HashMap<String , String > ();
		ArrayList<dif2lide.Port> inputPorts;
		ArrayList<dif2lide.Port> outputPorts;
		ArrayList<String> inputPortNames;
		ArrayList<String> outputPortNames;
		for(Map.Entry<Node, LWDFActor> entry : dsgContextMap.entrySet()){

			LWDFActor actor = entry.getValue();
			inputPorts = actor.getPorts(1);
        	outputPorts = actor.getPorts(0);
        	inputPortNames = new ArrayList<String>();
        	outputPortNames = new ArrayList<String>();

        	for (dif2lide.Port inPort : inputPorts){
        		Edge inEdge = inPort.edge;
        		inputPortNames.add(_dsggraph.getName(inEdge));
        	}

        	for (Port outPort : outputPorts){
        		Edge outEdge = outPort.edge;
        		outputPortNames.add(_dsggraph.getName(outEdge));
        	}

    		for (String each : inputPortNames){
    			snkArray.put(each,actor.getInstanceName());
    		}
		}

		ActorGraph actorGraph = new ActorGraph(snkArray,dsgContextMap,_dsggraph);
		ArrayList<ArrayList<String>> Clusters=actorGraph.dfs();
		_gcm.writeLine("#include "+"<lide_ocl_"+_dsggraph.getName()+"_scheduler.h>");
		_gcm.writeLine("#include <time.h>");
		_gcm.write("boolean ");
		_gcm.write("lide_ocl_util_dsg_optimised_scheduler(");
		_gcm.write("lide_ocl_graph_context_type*");
		_gcm.write("context){");
		_gcm.next();
		_gcm.writeLine("");
		_gcm.next();
		_gcm.incIndent();
		_gcm.writeLine("lide_ocl_dsg_actor_context_type* nextDsg;");
		_gcm.write("lide_ocl_dsg_actor_context_type** array =");
		_gcm.write("lide_ocl_"+_dsggraph.getName()+"_graph_get_sink_array(");
		_gcm.write("context);");
		_gcm.next();
		_gcm.write("int * type =");
		_gcm.write("lide_ocl_"+_dsggraph.getName()+"_graph_get_type_array(");
		_gcm.write("context);");
		_gcm.next();
		_gcm.writeLine("lide_ocl_"+_dsggraph.getName()+"_graph_context_type *dsgContext;");
		_gcm.writeLine("int value = 1,i;");
		_gcm.write("nextDsg=");
		_gcm.write("(lide_ocl_dsg_actor_context_type *)");
		_gcm.write("(context->actors[DSG_ACTOR_STARTLOOP]);");
		_gcm.next();

		_gcm.writeLine("while(1){");
		_gcm.incIndent();

		_gcm.writeLine("nextDsg->invoke((lide_ocl_actor_context_type*)nextDsg);");
		_gcm.writeLine("if(((nextDsg)->last_fifo)==NULL){");
		_gcm.writeLine("	break;");
		_gcm.writeLine("}");

		_gcm.write("nextDsg=");
		_gcm.write("array[");
		_gcm.write("lide_ocl_fifo_unit_size_get_index(");
		_gcm.write("lide_ocl_dsg_actor_get_last_fifo(");
		_gcm.write("nextDsg))];");
		_gcm.next();

		_gcm.decIndent();
		_gcm.writeLine("}");
		_gcm.writeLine("return true;");
		_gcm.decIndent();
		_gcm.writeLine("}");
		_gcm.writeLine("");

		//-----------------loop_cluster----------------------------
		_gcm.write("boolean ");
		_gcm.write("lide_ocl_util_dsg_optimised_scheduler_with_loop_cluster(");
		_gcm.write("lide_ocl_graph_context_type*");
		_gcm.write("context){");
		_gcm.next();
		_gcm.writeLine("");
		_gcm.next();
		_gcm.incIndent();

		_gcm.writeLine("lide_ocl_dsg_actor_context_type* nextDsg;");
		_gcm.write("boolean prevDsgLoop;");

		_gcm.write("lide_ocl_dsg_actor_context_type** array =");
		_gcm.write("lide_ocl_"+_dsggraph.getName()+"_graph_get_sink_array(");
		_gcm.write("context);");
		_gcm.next();

		_gcm.write("int * type =");
		_gcm.write("lide_ocl_"+_dsggraph.getName()+"_graph_get_type_array(");
		_gcm.write("context);");
		_gcm.next();

		_gcm.writeLine("lide_ocl_"+_dsggraph.getName()+"_graph_context_type *dsgContext;");

		_gcm.writeLine("int value = 1,i;");
		_gcm.writeLine("int lastLoopIter;");

		_gcm.write("nextDsg=");
		_gcm.write("(lide_ocl_dsg_actor_context_type *)");
		_gcm.write("(context->actors[DSG_ACTOR_STARTLOOP]);");
		_gcm.next();

		_gcm.writeLine("prevDsgLoop=0;");

		_gcm.writeLine("while(1){");
		_gcm.incIndent();

		_gcm.write("if(prevDsgLoop){");
		_gcm.next();

		_gcm.incIndent();

		_gcm.write("i = lastLoopIter;");
		_gcm.next();

		_gcm.writeLine("while(i!=0){");
		_gcm.incIndent();
		int i=0,j=0;
		while(i<Clusters.size() ){
			if(i==0)_gcm.write("if");

			else
				_gcm.write("else if");


			_gcm.write(" (nextDsg->index == DSG_ACTOR_"+Clusters.get(i).get(0).toUpperCase()+"){");

			_gcm.next();
			_gcm.incIndent();
			j=0;
			while(j<Clusters.get(i).size()){
				_gcm.write("nextDsg->invoke(context->actors[DSG_ACTOR_"+Clusters.get(i).get(j).toUpperCase()+"]);");
				_gcm.next();
				j++;
			}

			_gcm.decIndent();
			_gcm.nextLine();
			_gcm.write("}");
			_gcm.nextLine();

			i++;
		}

		_gcm.write("else{");
		_gcm.next();
		_gcm.incIndent();

		_gcm.write("nextDsg->invoke((lide_ocl_actor_context_type *)nextDsg);");
		_gcm.next();
		_gcm.decIndent();
		_gcm.write("}");
		_gcm.next();

		_gcm.write("i--;");
		_gcm.next();
		_gcm.write("}");
		_gcm.next();
		_gcm.decIndent();

		i=0;
		j=0;

		_gcm.nextLine();

		_gcm.nextLine();
		while(i<Clusters.size() ){
			if(i==0)_gcm.write("if");

			else
				_gcm.write("else if");

			_gcm.write(" (nextDsg->index == DSG_ACTOR_"+Clusters.get(i).get(0).toUpperCase()+"){");

			_gcm.next();
			_gcm.incIndent();

			_gcm.write("nextDsg = array[");
			_gcm.write("lide_ocl_fifo_unit_size_get_index(");
			_gcm.write("lide_ocl_dsg_actor_get_last_fifo((");
			_gcm.write("lide_ocl_dsg_actor_context_type*)");

			_gcm.write("context->actors[DSG_ACTOR_"+Clusters.get(i).get(Clusters.get(i).size()-1).toUpperCase()+"]))];");
			_gcm.next();

			_gcm.decIndent();
			_gcm.nextLine();
			_gcm.write("}");
			_gcm.nextLine();
			i++;
		}

		_gcm.write("else{");
		_gcm.next();
		_gcm.incIndent();
		_gcm.write("nextDsg = array[");
		_gcm.write("lide_ocl_fifo_unit_size_get_index(");
		_gcm.write("lide_ocl_dsg_actor_get_last_fifo((");
		_gcm.write("nextDsg)))];");

		_gcm.next();
		_gcm.decIndent();
		_gcm.write("}");
		_gcm.next();
		_gcm.nextLine();

		_gcm.writeLine("if (type[nextDsg->index]==DSG_SCA_DYN_LOOP){");
		_gcm.incIndent();
		_gcm.write("lide_ocl_sca_dynamic_loop_set_current_iteration((");
		_gcm.write("lide_ocl_sca_dynamic_loop_context_type*)");
		_gcm.write("nextDsg,0);");

		_gcm.next();
		_gcm.decIndent();
		_gcm.write("}");
		_gcm.next();
		_gcm.nextLine();

		_gcm.writeLine("if (type[nextDsg->index]==DSG_SCA_STATIC_LOOP){");
		_gcm.incIndent();
		_gcm.write("lide_ocl_sca_static_loop_set_current_iteration((");
		_gcm.write("lide_ocl_sca_static_loop_context_type*)");
		_gcm.write("nextDsg,0);");

		_gcm.next();
		_gcm.decIndent();
		_gcm.write("}");
		_gcm.next();

		_gcm.writeLine("prevDsgLoop=0;");

		_gcm.writeLine("nextDsg->invoke((lide_ocl_actor_context_type*)nextDsg);");

		_gcm.write("nextDsg=");
		_gcm.write("array[");
		_gcm.write("lide_ocl_fifo_unit_size_get_index(");
		_gcm.write("lide_ocl_dsg_actor_get_last_fifo(");
		_gcm.write("nextDsg))];");
		_gcm.next();

		_gcm.writeLine("continue;");
		_gcm.decIndent();

		_gcm.writeLine("}");
		_gcm.nextLine();

		_gcm.writeLine("nextDsg->invoke((lide_ocl_actor_context_type*)nextDsg);");
		_gcm.nextLine();

		_gcm.writeLine("if(((nextDsg)->last_fifo)==NULL){");
		_gcm.writeLine("	break;");
		_gcm.writeLine("}");

		_gcm.writeLine("if (type[nextDsg->index]==DSG_SCA_DYN_LOOP){");
		_gcm.incIndent();
		_gcm.writeLine("prevDsgLoop=1;");
		_gcm.write("lastLoopIter=");
		_gcm.write("lide_ocl_sca_dynamic_loop_get_current_iteration((");
		_gcm.write("lide_ocl_sca_dynamic_loop_context_type*)");
		_gcm.write("nextDsg);");

		_gcm.next();
		_gcm.decIndent();
		_gcm.write("}");
		_gcm.next();
		_gcm.nextLine();

		_gcm.writeLine("else if (type[nextDsg->index]==DSG_SCA_STATIC_LOOP){");
		_gcm.incIndent();
		_gcm.writeLine("prevDsgLoop=1;");
		_gcm.write("lastLoopIter=");
		_gcm.write("lide_ocl_sca_static_loop_get_current_iteration((");
		_gcm.write("lide_ocl_sca_static_loop_context_type*)");
		_gcm.write("nextDsg);");

		_gcm.next();
		_gcm.decIndent();
		_gcm.write("}");
		_gcm.next();

		_gcm.writeLine("else{");
		_gcm.incIndent();
		_gcm.writeLine("prevDsgLoop=0;");
		_gcm.next();
		_gcm.decIndent();
		_gcm.write("}");
		_gcm.next();

		_gcm.write("nextDsg=");
		_gcm.write("array[");
		_gcm.write("lide_ocl_fifo_unit_size_get_index(");
		_gcm.write("lide_ocl_dsg_actor_get_last_fifo(");
		_gcm.write("nextDsg))];");
		_gcm.next();

		_gcm.decIndent();
		_gcm.write("}");
		_gcm.next();

		_gcm.writeLine("return true;");
		_gcm.decIndent();
		_gcm.writeLine("}");
		_gcm.writeLine("");

		//--------------- cluster ---------------------//
		_gcm.write("boolean ");
		_gcm.write("lide_ocl_util_dsg_optimised_scheduler_with_cluster(");
		_gcm.write("lide_ocl_graph_context_type*");
		_gcm.write("context){");
		_gcm.next();
		_gcm.writeLine("");
		_gcm.next();

		_gcm.incIndent();

		_gcm.writeLine("lide_ocl_dsg_actor_context_type* nextDsg;");
		_gcm.write("boolean prevDsgLoop;");

		_gcm.write("lide_ocl_dsg_actor_context_type** array =");
		_gcm.write("lide_ocl_"+_dsggraph.getName()+"_graph_get_sink_array(");
		_gcm.write("context);");
		_gcm.next();

		_gcm.write("int * type =");
		_gcm.write("lide_ocl_"+_dsggraph.getName()+"_graph_get_type_array(");
		_gcm.write("context);");
		_gcm.next();

		_gcm.writeLine("lide_ocl_"+_dsggraph.getName()+"_graph_context_type *dsgContext;");

		_gcm.writeLine("int value = 1,i;");
		_gcm.writeLine("int lastLoopIter;");

		_gcm.write("nextDsg=");
		_gcm.write("(lide_ocl_dsg_actor_context_type *)");
		_gcm.write("(context->actors[DSG_ACTOR_STARTLOOP]);");
		_gcm.next();

		_gcm.writeLine("prevDsgLoop=0;");

		_gcm.writeLine("while(1){");
		_gcm.incIndent();
		_gcm.nextLine();

		i=0;
		j=0;
		while(i<Clusters.size() ){
			_gcm.write("if");

			_gcm.write(" (nextDsg->index == DSG_ACTOR_"+Clusters.get(i).get(0).toUpperCase()+"){");

			_gcm.next();
			_gcm.incIndent();
			j=0;
			while(j<Clusters.get(i).size()){
				_gcm.write("nextDsg->invoke(context->actors[DSG_ACTOR_"+Clusters.get(i).get(j).toUpperCase()+"]);");
				_gcm.next();
				j++;
			}
			_gcm.write("nextDsg = array[");
			_gcm.write("lide_ocl_fifo_unit_size_get_index(");
			_gcm.write("lide_ocl_dsg_actor_get_last_fifo((");
			_gcm.write("lide_ocl_dsg_actor_context_type*)");
			_gcm.write("context->actors[DSG_ACTOR_"+Clusters.get(i).get(--j).toUpperCase()+"]))];");
			_gcm.next();

			_gcm.decIndent();
			_gcm.nextLine();
			_gcm.write("}");
			_gcm.nextLine();

			i++;
		}

		_gcm.writeLine("nextDsg->invoke((lide_ocl_actor_context_type*)nextDsg);");
		_gcm.nextLine();

		_gcm.writeLine("if(((nextDsg)->last_fifo)==NULL){");
		_gcm.writeLine("	break;");
		_gcm.writeLine("}");

		_gcm.write("nextDsg=");
		_gcm.write("array[");
		_gcm.write("lide_ocl_fifo_unit_size_get_index(");
		_gcm.write("lide_ocl_dsg_actor_get_last_fifo(");
		_gcm.write("nextDsg))];");
		_gcm.next();

		_gcm.decIndent();
		_gcm.write("}");
		_gcm.next();

		_gcm.writeLine("return true;");
		_gcm.decIndent();
		_gcm.writeLine("}");
		_gcm.writeLine("");

		_gcm.write("boolean ");
		_gcm.write("lide_ocl_utl_guarded_execution(");
		_gcm.write("lide_ocl_actor_context_type*");
		_gcm.write("context){");
		_gcm.next();

		_gcm.incIndent();
		_gcm.writeLine("if (context->enable(context)) {");
		_gcm.incIndent();

		_gcm.writeLine("context->invoke(context);");
		_gcm.writeLine("return TRUE;");
		_gcm.decIndent();

		_gcm.writeLine("} else {");
		_gcm.incIndent();

		_gcm.writeLine("return FALSE;");
		_gcm.decIndent();
		_gcm.writeLine("}");
		_gcm.decIndent();
		_gcm.writeLine("}");

		_gcm.write("void ");
		_gcm.write("lide_ocl_utl_simple_dsc_scheduler(");
		_gcm.write("lide_ocl_graph_context_type*");
		_gcm.write(" context){");
		_gcm.next();
		_gcm.incIndent();

		_gcm.writeLine("boolean progress = FALSE;");
		_gcm.writeLine("int i = 0;");
		_gcm.writeLine("int value = 1;");
		_gcm.writeLine("lide_ocl_actor_context_type* nextDsg;");
		_gcm.writeLine("nextDsg=context->actors[DSG_ACTOR_STARTLOOP];");

		_gcm.write("lide_ocl_fifo_unit_size_write(");
		_gcm.write("(lide_ocl_fifo_unit_size_pointer)");
		_gcm.write("context->fifos[DSG_FIFO_E1],");

		_gcm.write("&value);");
		_gcm.next();

		_gcm.writeLine("boolean holder;");
		_gcm.writeLine("do {");
		_gcm.incIndent();
		_gcm.writeLine("progress = 0;");
		_gcm.writeLine("for (i = 0; i < context->actor_count; i++) {");
		_gcm.incIndent();
		_gcm.writeLine("progress |=lide_ocl_utl_guarded_execution(context->actors[i]);");

		_gcm.decIndent();
		_gcm.writeLine("}");
		_gcm.decIndent();
		_gcm.writeLine("} while (progress);");
		_gcm.decIndent();
		_gcm.writeLine("}");

		_gcm.flush1();
		_gcm.close();

	}

	private void writeDsgGraphDefines(String Dir, HashMap<Node,LWDFActor>contextMap, HashMap<Edge,LWDFEdge>fifoMap){

		int i=0;
		Gcm _gcm = new Gcm(Dir+"lide_ocl_"+_dsggraph.getName()+"_graph.h");
		_gcm.writeLine("#ifndef _lide_ocl_"+_graph.getName()+"_sch1_graph_");
		_gcm.writeLine("#define _lide_ocl_"+_graph.getName()+"_sch1_graph_");
		_gcm.nextLine();
		_gcm.writeLine("#include \"lide_ocl_basic.h\"");
		_gcm.writeLine("#include \"lide_ocl_fifo_unit_size.h\"");
		_gcm.writeLine("#include \"lide_ocl_actor.h\"");
		_gcm.writeLine("#include \"lide_ocl_"+_graph.getName()+"_graph.h\"");
		_gcm.writeLine("#include \"lide_ocl_fifo.h\"");
		_gcm.writeLine("#include \"lide_ocl_dsg_actor.h\"");
		_gcm.writeLine("#include \"lide_ocl_ra_util.h\"");
		_gcm.writeLine("#include \"lide_ocl_graph.h\"");
		_gcm.writeLine("#include \"lide_ocl_ref_pre_post_functions.h\"");
		_gcm.writeLine("#include \"lide_ocl_util.h\"");

		for (Map.Entry<Node, LWDFActor> entry : contextMap.entrySet()){

        	LWDFActor actor = entry.getValue();
        	_gcm.writeLine("#include \"lide_ocl_"+actor.getName()+".h\"" );
		}

		_gcm.nextLine();
		_gcm.writeLine("#define BUFFER_CAPACITY 1024");
		_gcm.nextLine();

		for (Map.Entry<Node, LWDFActor> entry : contextMap.entrySet()){

        	LWDFActor actor = entry.getValue();
        	_gcm.writeLine("#define DSG_ACTOR_"+actor.getInstanceName().toUpperCase()+" "+i);
        	i++;
		}
		_gcm.nextLine();

		i=0;
		for (Map.Entry<Edge, LWDFEdge > entry : fifoMap.entrySet()){
        	
        	LWDFEdge edge = entry.getValue();
        	_gcm.writeLine("#define DSG_FIFO_"+edge.getName().toUpperCase()+" "+ i );
        	i++;
		}
		_gcm.nextLine();

		_gcm.writeLine("#define DSG_ACTOR_COUNT "+ contextMap.size());
		_gcm.writeLine("#define DSG_FIFO_COUNT "+ fifoMap.size());

		_gcm.nextLine();
		
		_gcm.writeLine("#define DSG_SCA_REF 0");
		_gcm.writeLine("#define DSG_SCA_DYN_LOOP 1");
		_gcm.writeLine("#define DSG_SCA_STATIC_LOOP 2");
		_gcm.writeLine("#define DSG_OTHER 3");
		
		_gcm.nextLine();
		
		i=0;
		
		_gcm.writeLine("struct _lide_ocl_"+_dsggraph.getName()+"_graph_context_struct;");
		_gcm.write("typedef ");
		_gcm.write("struct");
		_gcm.write(" _lide_ocl_"+_dsggraph.getName()+"_graph_context_struct");
		_gcm.write("lide_ocl_"+_dsggraph.getName()+"_graph_context_type;");
		_gcm.next();
		_gcm.nextLine();
		
		_gcm.write("lide_ocl_"+_dsggraph.getName()+"_graph_context_type *");
		_gcm.write("lide_ocl_"+_dsggraph.getName()+"_graph_new(");
		
		_gcm.write("lide_ocl_graph_context_type* contextApp);");
		_gcm.next();
		_gcm.nextLine();
		
		
		_gcm.write("void ");
		_gcm.write("lide_ocl_"+_dsggraph.getName()+"_graph_terminate(");
		_gcm.write("lide_ocl_"+_dsggraph.getName()+"_graph_context_type *");
		_gcm.write("context);");
		_gcm.next();
		//_gcm.writeLine("\n#endif");
		//_gcm.nextLine();	
		
		_gcm.write("lide_ocl_dsg_actor_context_type ** ");
		_gcm.write("lide_ocl_"+_dsggraph.getName()+"_graph_get_sink_array(");
		_gcm.write("lide_ocl_graph_context_type *");
		_gcm.write("context);");
		_gcm.next();
		
		_gcm.write("int * ");
		_gcm.write("lide_ocl_"+_dsggraph.getName()+"_graph_get_type_array(");
		_gcm.write("lide_ocl_graph_context_type *");
		_gcm.write("context);");
		_gcm.next();
		
		_gcm.writeLine("#endif");

		_gcm.flush1();
		_gcm.close();
	}
	
	public void dloclconfig(String outputDir,String graphName){
		String FileName = outputDir + "dloclconfig";
		try {
			makeMe = new PrintWriter(new BufferedWriter(new FileWriter(FileName)));
		} catch(IOException e){
			System.out.println("Error: Cannot find "); 
			e.printStackTrace(); 
		}
		makeMe.println("#!/usr/bin/env bash");
		makeMe.println("");
		
		makeMe.println("# In the variable assignments within this script, the programmer may need to modify one or more of the right hand sides");
		makeMe.println("# (RHSs) to properly configure the script for his or her specific environment.");
		makeMe.println("# Each variable performs a different function and the comment above it explains that.");
		makeMe.println("# Typically a general application would require us to modify the include path, compiled library path, specify the target filename,");
		makeMe.println("# the directory to install the new compiled file and name of the source file that needs to be compiled with a .o extension.");

		makeMe.println("");
		
		makeMe.println("# Write the paths to the header files that need to be included here. Just add another line of dloclincludepath and sepcify the path.");
		

		
		makeMe.println("dloclincludepath=\"-I. -I$UXLIDEOCL/src/gems/actors/basic\"");
		makeMe.println("dloclincludepath=\"$dloclincludepath -I$UXLIDEOCL/src/gems/actors/basic\"");
		makeMe.println("dloclincludepath=\"$dloclincludepath -I$UXLIDEOCL/src/gems/edges/basic -I$UXLIDEOCL/src/gems/actors/common \"");
		makeMe.println("dloclincludepath=\"$dloclincludepath -I$UXLIDEOCL/src/tools/runtime\"");
		makeMe.println("dloclmiscflags=\"\"");
		makeMe.println("dloclmiscldflags=\"\"");
		makeMe.println("dlocllibpath=\"\"");
		
		makeMe.println("# Write all the library files to be included for the compilation here.");
		makeMe.println("");
		
		makeMe.println("dlocllibs=\"$LIDEOCLGEN/lide_ocl_actors_basic.a\"");
		makeMe.println("dlocllibs=\"$dlocllibs $LIDEOCLGEN/lide_ocl_edges_basic.a $LIDEOCLGEN/lide_ocl_runtime.a\"");
		
		makeMe.println("# Write the file target file name here.");
		makeMe.println("dlocltargetfile=\""+"lide_ocl_"+graphName+"_driver.exe\"");
		
		makeMe.println("# Write the directory where you want to copy the current final target file here.");
		makeMe.println("dloclinstalldir=\"$LIDEOCLGEN\"");
		
		makeMe.println("# Write the names of the source files that you want to compile and replace the .cpp or .c extension by a .o");
		makeMe.println("dloclobjs=\""+"lide_ocl_"+graphName+"_driver.o\"");
		
		makeMe.println("dloclverbose=\"\"");
		
		makeMe.println("dloclcompiler=\"g++\"");
		makeMe.flush();
	}
	
	public void makeme(String outputDir){
		String makeMeFileName = outputDir + "makeme";
		try {
			makeMe = new PrintWriter(new BufferedWriter(new FileWriter(makeMeFileName)));
		} catch(IOException e){
			System.out.println("Error: Cannot find "); 
			e.printStackTrace(); 
		}
		makeMe.println("set -a # Export variable definitions apple to sub-processes");
		makeMe.println("source dloclmakeme &>diagnostics.txt");

		
		makeMe.flush();
	}
	
	public void runme(String outputDir,String graphName){
		String runMeFileName = outputDir + "runme";
		try {
			runMe = new PrintWriter(new BufferedWriter(new FileWriter(runMeFileName)));
		} catch(IOException e){
			System.out.println("Error: Cannot find "); 
			e.printStackTrace(); 
		}
		runMe.println("./"+ "lide_ocl_"+ graphName +"_driver.exe>>diagnostics.txt");
		runMe.println("cat ../out.txt");
		
		runMe.flush();
	}
	
	
	public void run(){
        //_headerWriter.run(); 
		_applGraphFileWriter.run(); 
		_dsgGraphFileWriter.run();
        //writeSchedules();
	}
	
	/*private void writeSchedules() {
    	try {
	    	for (int i = 0; i < this._loopedScheduleList.size(); i++){
	    		LoopedScheduleList ls = this._loopedScheduleList.get(i);
	    		String fileName = _outputDir + _SCHEDULE_FILE_PREFIX  + i + ".txt";
	    		PrintWriter scheduleFile = new PrintWriter(new BufferedWriter(new FileWriter( 
	                   fileName)));
	    		for (int j = 0; j < ls.getLength(); j++){
	    			String line = "";
	    			Node node = ls.getElement(j).getNode(); 
	    			LWDFActor actor = _contextMap.get(node);  
	    			line += actor.getID();
	    			line += " ";
	    			line += ls.getElement(j).getQ();
	    			scheduleFile.println(line);
	    		}
	    		scheduleFile.flush();
	    		scheduleFile.close(); 
	    	}
    	} catch (IOException exp){
    		throw new RuntimeException(exp.getMessage());
    	}
    }*/
    
	private ApplGraphFileWriter _applGraphFileWriter;
	private DsgGraphFileWriter _dsgGraphFileWriter;
	private final SDFGraph _graph; 
	private final SDFGraph _dsggraph; 
	private static final String _SCHEDULE_FILE_PREFIX = "thread_";
	private PrintWriter makeMe;
	private PrintWriter runMe;
}
