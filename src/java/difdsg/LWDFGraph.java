/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
package difdsg;
import dif.DIFParameter;
import dif.csdf.sdf.SDFGraph;

import java.util.*;


public class LWDFGraph {
	public LWDFGraph(SDFGraph graph, String name) {
		_name = name; 
		initGraph(graph);
	}
	
	private void initGraph(SDFGraph graph){

		LinkedList<DIFParameter> para_list = graph.getParameters();
		initGraphArgueType(para_list);
		initGraphArgueVar(para_list);
		initGraphSchedule(para_list);
	}

	private void initGraphArgueType(LinkedList<DIFParameter> para_list){
		_graphArguType = new LinkedHashMap<>();
		HashMap<Integer, String> tmp = new HashMap();
		String _type = "graphArguType";
		Integer startIndex = _type.length();

		for (DIFParameter para : para_list) {
			String para_name = para.getName();
			if (para_name.toLowerCase().contains(_type.toLowerCase())){
				String index_str = para_name.substring(startIndex);
				Integer para_index = Integer.parseInt(index_str);
				String argueType = (String)para.getValue();
				argueType = argueType.replace("\"", "");
				tmp.put(para_index,argueType);
			}
		}
		//Put ArgueType List in order
		for (int index = 1; index <= tmp.size(); index++){
			String val=(String)tmp.get(index);
			_graphArguType.put(index,val);
		}
	}

	private void initGraphArgueVar(LinkedList<DIFParameter> para_list){
		_graphArguVar = new LinkedHashMap<>();
		HashMap<Integer, String> tmp = new HashMap();
		String _type = "graphArguVar";
		Integer startIndex = _type.length();

		for (DIFParameter para : para_list) {
			String para_name = para.getName();
			if (para_name.toLowerCase().contains(_type.toLowerCase())){
				String index_str = para_name.substring(startIndex);
				Integer para_index = Integer.parseInt(index_str);
				String argueType = (String)para.getValue();
				argueType = argueType.replace("\"", "");
				tmp.put(para_index,argueType);
			}
		}
		//Put ArgueVar List in order
		for (int index = 1; index <= tmp.size(); index++){
			String val=(String)tmp.get(index);
			_graphArguVar.put(index,val);
		}
	}

	private void initGraphSchedule(LinkedList<DIFParameter> para_list){
		_graphSch = new String();
		HashMap<Integer, String> tmp = new HashMap();
		String _type = "graphSch";
		Integer startIndex = _type.length();

		for (DIFParameter para : para_list) {
			String para_name = para.getName();
			if (para_name.toLowerCase().equals(_type.toLowerCase())){
				_graphSch = ((String)para.getValue()).replace("\"", "");
			}
		}

	}


	public LinkedHashMap<Integer, String> getGraphArguTypeList(LWDFGraph lwdfGraph){
		return _graphArguType;
	}

	public String getGraphArguType(LWDFGraph lwdfGraph, int index){
		return _graphArguType.get(index);
	}
	public Integer getGraphArguTypeSize(LWDFGraph lwdfGraph){
		return _graphArguType.size();
	}


	public LinkedHashMap<Integer, String> getGraphArguVarList(LWDFGraph lwdfGraph){
		return _graphArguVar;
	}

	public String getGraphArguVar(LWDFGraph lwdfGraph, int index){
		return _graphArguVar.get(index);
	}

	public Integer getGraphArguVarSize(LWDFGraph lwdfGraph){
		return _graphArguVar.size();
	}

	public String getGraphSchedule(){
		return _graphSch;
	}

	/************************** Members *****************************/
	private String _name;
	private LinkedHashMap<Integer, String> _graphArguType;
	private LinkedHashMap<Integer, String> _graphArguVar;
	private String _graphSch;


	/************************** Constant Members *****************************/
	private final static double _maxActorRuntime = 1000 * 3600 * 365;


}
