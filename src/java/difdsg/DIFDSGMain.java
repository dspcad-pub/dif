/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
package difdsg;
public class DIFDSGMain {
	/* All tests */
	public static void main(String[] args){
		String configFile="";

		if (args.length == 1) {
			configFile = args[0];
		}else{
			configFile="./src/java/difdsg/config.xml";
		}
		testConfigXML(configFile);
		System.out.print("config_FileName:"+configFile + args.length+"\n");
		testDIFDSG(configFile);

	}

	public static void testDIFDSG(String configFile){
		Utilities.difdsg(configFile);
	}

	/* Test XML reader for config file*/
	public static void testConfigXML(String configFile){
		int app_dif_file_length, dsg_dif_file_length;
		String app_dif_FileName = null, dsg_dif_FileName = null;
		boolean app_enable=false, dsg_enable=false;

	    /* Get information in config file*/
		DIFDSGtoLWDFXMLReader xmlReader = new DIFDSGtoLWDFXMLReader(configFile);
		DIFDSGtoLWDFConfig config = xmlReader.getConfiguration();

        /* Get file name of app dif file (without .dif extension)*/
		if(!config.app_dif_File.equals("none")) {
			app_dif_file_length = config.app_dif_File.lastIndexOf("/");
			app_dif_FileName = config.app_dif_File.substring(app_dif_file_length + 1);
			app_dif_file_length = app_dif_FileName.lastIndexOf(".dif");
			app_dif_FileName = app_dif_FileName.substring(0, app_dif_file_length);
			app_enable = true;
		}
        /* Get file name of dsg dif file (without .dif extension)*/
		if(!config.dsg_dif_File.equals("none")) {
			dsg_dif_file_length = config.dsg_dif_File.lastIndexOf("/");
			dsg_dif_FileName = config.dsg_dif_File.substring(dsg_dif_file_length + 1);
			dsg_dif_file_length = dsg_dif_FileName.lastIndexOf(".dif");
			dsg_dif_FileName = dsg_dif_FileName.substring(0, dsg_dif_file_length);
			dsg_enable=true;
		}
		System.out.print("app_dif_FileName:"+app_dif_FileName+"\n");
		System.out.print("dsg_dif_FileName:"+dsg_dif_FileName+"\n");
		System.out.print("result directory:"+config.resultDirectory+"\n");
		System.out.print("lide package:"+config.lide_pkg+"\n");

	}
}
