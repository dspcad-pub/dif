/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
package difdsg;

import dif.csdf.sdf.SDFGraph;
import dif.util.graph.Edge;
import dif.util.graph.Node;

import java.util.*;

public class WELTCPPAppCodeWriter {

    WELTCPPAppCodeWriter(DIFtoHierarchy app_h, String outputDir) {
        LWDFActor actor;
        /* Parameter initialization*/
        _app_h = app_h;
        _graph = _app_h.graph;
        _lwdfGraph = app_h.lwdf_graph;
        _contextMap = _app_h.nodeMap;
        _edgeMap = _app_h.edgeMap;
        _outputDir = outputDir;
        _appGraph_Name = _graph.getName();
        _appGraphHeader_Name = _outputDir  + _appGraph_Name + "_graph.h";
        _appGraphSource_Name = _outputDir + _appGraph_Name + "_graph.cpp";
        _appGraphName = _appGraph_Name + "_graph";
		/* Check all actor types*/
        _ActorType = new HashSet<String>();
        for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()) {
            actor = entry.getValue();
            _ActorType.add(actor.getName());
        }
		/* Create files*/
        CreateGraphADTFiles();
		/* Get argument list for New function and actor New extra argument list*/
        InitGraphNewArguList(_lwdfGraph);
        InitGraphNewFieldListInit(_lwdfGraph);
        InitActorNewArguList(_lwdfGraph);
    }

    public void run() {
		/*Create Header */
        WriteGraphHeader();
		/* Create Source*/
        WriteGraphSource();
    }

    private void CreateGraphADTFiles() {
        try {
            _gcm_h = new Gcm(_appGraphHeader_Name, false);
            _gcm_s = new Gcm(_appGraphSource_Name, false);
            _gcm_h.close();
            _gcm_s.close();
        } catch (Exception e) {
            System.out.println("Error: Cannot find " + _appGraphHeader_Name + "or" + _appGraphSource_Name);
            e.printStackTrace();
        }
    }

    private void WriteGraphHeader() {
        int i;
        _gcm_h = new Gcm(_appGraphHeader_Name, true);
		/*start macro ifdef*/
        _gcm_h.writeLine("#ifndef _"  + _appGraphName + "_h");
        _gcm_h.writeLine("#define _"  + _appGraphName + "_h");
        _gcm_h.writeLine("");

        /* General header files */
        _gcm_h.writeLine("#include <iostream>");

        _gcm_h.writeLine("#include \"" + "welt_cpp_graph.h\"");
        _gcm_h.writeLine("#include \"" + "welt_cpp_util.h\"");

        /* actor headers */
        for (String actortype : _ActorType) {
            _gcm_h.writeLine("#include \""  + actortype + ".h\"");
        }
        _gcm_h.nextLine();

		/* macro definitions*/
        //_gcm_h.writeLine("#define BUFFER_CAPACITY 65536*4");
        //_gcm_h.nextLine();
        /* Actor Index */
        i = 0;
        for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()) {

            LWDFActor actor = entry.getValue();
            _gcm_h.writeLine("#define " + actor.getInstanceName().toUpperCase() + "    " + actor.getID());
            i++;
        }
        _gcm_h.nextLine();
        /* FIFO Index */
        i = 0;
        for (Map.Entry<Edge, LWDFEdge> entry : _edgeMap.entrySet()) {

            LWDFEdge edge = entry.getValue();
            _gcm_h.writeLine("#define " + edge.getName().toUpperCase() + "    " + edge.getID());
            i++;
        }
        _gcm_h.nextLine();
        /* Count of FIFOs and actors */
        _gcm_h.writeLine("#define ACTOR_COUNT    " + _contextMap.size());
        _gcm_h.writeLine("#define FIFO_COUNT    " + _edgeMap.size());
        _gcm_h.nextLine();

		/* Graph context definition*/
        _gcm_h.writeLine("class"+ " "+  _appGraphName + ": public welt_cpp_graph {");
        _gcm_h.writeLine("public: ");
        _gcm_s.incIndent();
        _gcm_h.writeLine( _appGraphName +"("+ _graphNewArguList +");");
        _gcm_h.writeLine( "void scheduler() override;");
        _gcm_s.decIndent();
        _gcm_h.writeLine("private: ");
        _gcm_h.write( _graphNewFieldList);
        _gcm_h.writeLine(";}; ");
        _gcm_h.next();
        _gcm_h.nextLine();

		/* end macro endif */
        _gcm_h.writeLine("#endif");
        _gcm_h.flush1();
        _gcm_h.close();
    }

    private void WriteGraphSource() {

        _gcm_s = new Gcm(_appGraphSource_Name, true);
        /* Header files */
        WriteGraphSourceHeaderFiles();
        /* Construct functions */
        WriteConstructFunction();
        /* Scheduler */
        WriteSchedulerFunction();
        _gcm_s.flush1();
        _gcm_s.close();

    }

    private void WriteGraphSourceHeaderFiles() {
        _gcm_s.writeLine("#include <stdlib.h>");
        _gcm_s.writeLine("#include <string.h>");
        _gcm_s.writeLine("#include \""  + _appGraphName + ".h\"");
        _gcm_s.nextLine();
    }

    private void WriteConstructFunction() {

        int i;
        _gcm_s.write(  _appGraphName + "::" + _appGraphName + "(");
        _gcm_s.write(_graphNewArguList);
        _gcm_s.write("){");
        _gcm_s.next();
        _gcm_s.incIndent();
        String[] graphArgList = _graphNewArguList.split(",");
        String[] temp;
        for (i = 0; i < graphArgList.length; i++){
            temp = graphArgList[i].split(" ");
            _gcm_s.write("this->"+ temp[temp.length-1] + " = "  + temp[temp.length-1]+ ";");
            _gcm_s.next();
        }
        _gcm_s.writeLine("this->actor_count = ACTOR_COUNT;");
        _gcm_s.writeLine("this->fifo_count = FIFO_COUNT;");
        _gcm_s.next();
        _gcm_s.writeLine("this->actors.reserve(this->actor_count);");
        _gcm_s.writeLine("this->descriptors.reserve(this->actor_count);");
        _gcm_s.writeLine("this->fifos.reserve(this->fifo_count);");
        _gcm_s.writeLine("this->source_array.reserve(this->fifo_count);");
        _gcm_s.writeLine("this->sink_array.reserve(this->fifo_count);");
        _gcm_s.writeLine("int token_size;");
        _gcm_s.next();

        for (Map.Entry<Edge, LWDFEdge> entry : _edgeMap.entrySet()) {

            LWDFEdge edge = entry.getValue();
            String edgeType = edge.getEdgeType();
            int capacity = edge.getCapacity();
            _gcm_s.write("token_size = sizeof(");
            _gcm_s.write(edgeType + ");");
            _gcm_s.next();
            _gcm_s.write("this->fifos["  + edge.getName().toUpperCase() + "] = ");
            _gcm_s.write("( "  + "welt_c_fifo_pointer)");
            _gcm_s.write(  _welt_c_prefix+"fifo_new("+ capacity+ ",");
            _gcm_s.write("token_size,"+ edge.getName().toUpperCase() +");");
            _gcm_s.next();

        }
        _gcm_s.nextLine();

        /* actor array */
        for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()) {

            LWDFActor actor = entry.getValue();
            String actorName = actor.getInstanceName();
            String actorType = actor.getName();
            String fileName = actor.getFileName();

            // Get input port and output port
            ArrayList<Port> inputPorts = actor.getPorts(1);
            ArrayList<Port> outputPorts = actor.getPorts(0);
            ArrayList<String> inputPortNames = new ArrayList<String>();
            ArrayList<String> outputPortNames = new ArrayList<String>();
            for (Port inPort : inputPorts) {
                Edge inEdge = inPort.edge;
                inputPortNames.add(_graph.getName(inEdge));
            }

            for (Port outPort : outputPorts) {
                Edge outEdge = outPort.edge;
                outputPortNames.add(_graph.getName(outEdge));
            }

            /* write new function for each actor */
            /* argument list: input fifos, output fifos and actor index */
            _gcm_s.write("this->actors["  + actorName.toUpperCase() + "] = ");
            _gcm_s.write("( new " +  actorType.toLowerCase() + "(");

            /* Extra Argument List */
            String actor_argu_list = _actorNewExtraArguList.get(actor);
            if (actor_argu_list != null)
                _gcm_s.write(actor_argu_list);

            /* actor index */
            _gcm_s.write("));");
            _gcm_s.next();

            /* write set_index function */
            _gcm_s.write("this->actors["  + actorName.toUpperCase() + "]");
            _gcm_s.write("->actor_set_index(" + actorName.toUpperCase()+");");
            _gcm_s.next();
            _gcm_s.write("this->descriptors["  + actorName.toUpperCase() + "]=");
            _gcm_s.write("(char*)\"" + actorName.toUpperCase()+"\";");
            _gcm_s.next();
            /* write connect function */
            _gcm_s.write("this->actors["  + actorName.toUpperCase() + "]");
            _gcm_s.write("->connect(("  + _welt_cpp_prefix +"graph *)this);");
            _gcm_s.next();
            _gcm_s.nextLine();

        }
        _gcm_s.nextLine();
        _gcm_s.decIndent();
        _gcm_s.writeLine("}");
    }

    private void WriteSchedulerFunction() {
        String sch = _lwdfGraph.getGraphSchedule();
        _gcm_s.write("void " + _appGraphName + "::scheduler(){");
        _gcm_s.next();
        _gcm_s.incIndent();
        if(sch.isEmpty()){
            _gcm_s.write("/* scheduler*/");
        }else {
            switch (sch) {
                case "simple":
                    _gcm_s.write("welt_cpp_util_simple_scheduler(");
                    _gcm_s.write("this->actors,0, ");
                    _gcm_s.write("this->actor_count, ");
                    _gcm_s.write("this->descriptors);");
                    break;
                default:
                    _gcm_s.write(sch);
                    break;
            }
        }
        _gcm_s.next();
        _gcm_s.writeLine("return;");
        _gcm_s.decIndent();
        _gcm_s.writeLine("}");
    }

    /* _graphNewArguList example: "int len, char * filename, double value" */
    private void InitGraphNewArguList(LWDFGraph lwdfGraph) {
        _graphNewArguList = new String("");
        _graphNewFieldList = new String("");
        LinkedHashMap<Integer, String> lwdfgraphArguType = _lwdfGraph.getGraphArguTypeList(_lwdfGraph);
        LinkedHashMap<Integer, String> lwdfgraphArguVar = _lwdfGraph.getGraphArguVarList(_lwdfGraph);
        if (lwdfgraphArguType == null || lwdfgraphArguVar == null){
            _graphNewArguList = "";
            return;
        }
        for (Map.Entry<Integer, String> entry : lwdfgraphArguType.entrySet()) {
            Integer Index = entry.getKey();
            String arguType = entry.getValue();
            String arguVar = lwdfgraphArguVar.get(Index);

                _graphNewArguList = _graphNewArguList.concat(arguType);
                _graphNewArguList = _graphNewArguList.concat(" ");
                _graphNewArguList = _graphNewArguList.concat(arguVar);
                _graphNewArguList = _graphNewArguList.concat(", ");

                _graphNewFieldList = _graphNewFieldList.concat(arguType);
                _graphNewFieldList = _graphNewFieldList.concat(" ");
                _graphNewFieldList = _graphNewFieldList.concat(arguVar);
                _graphNewFieldList = _graphNewFieldList.concat("; \n");
        }
        int len = _graphNewArguList.length();
        if(len < 2){
            _graphNewArguList = "";
            return;
        }
        len = len-2;
        _graphNewArguList = _graphNewArguList.substring(0, len);
        len = _graphNewFieldList.length();
        if(len < 2){
            _graphNewFieldList = "";
            return;
        }
        len = len-2;
        _graphNewFieldList = _graphNewFieldList.substring(0, len);
    }

    private void InitGraphNewFieldListInit(LWDFGraph lwdfGraph) {
        _graphNewFieldListInit = new String("");
        LinkedHashMap<Integer, String> lwdfgraphArguType = _lwdfGraph.getGraphArguTypeList(_lwdfGraph);
        LinkedHashMap<Integer, String> lwdfgraphArguVar = _lwdfGraph.getGraphArguVarList(_lwdfGraph);
        if (lwdfgraphArguType == null || lwdfgraphArguVar == null){
            _graphNewFieldListInit = "";
            return;
        }
        for (Map.Entry<Integer, String> entry : lwdfgraphArguType.entrySet()) {
            Integer Index = entry.getKey();
            String arguType = entry.getValue();
            String arguVar = lwdfgraphArguVar.get(Index);
                _graphNewFieldListInit = _graphNewFieldListInit.concat("this->");
                _graphNewFieldListInit = _graphNewFieldListInit.concat(arguVar);
                _graphNewFieldListInit = _graphNewFieldListInit.concat("=");
                _graphNewFieldListInit = _graphNewFieldListInit.concat(arguVar);
                _graphNewFieldListInit = _graphNewFieldListInit.concat(";\n");
        }
        int len = _graphNewFieldListInit.length();
        if(len < 2){
            _graphNewFieldListInit = "";
            return;
        }
        len = len-2;
        //System.out.println(len);
        _graphNewFieldListInit = _graphNewFieldListInit.substring(0, len);
    }

    /* arguList example: "int len, char * filename, double value" */
    private void InitActorNewArguList(LWDFGraph lwdfGraph) {

        _actorNewExtraArguList = new HashMap<LWDFActor, String>();
        /* iterate for each actor */
        for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()) {
            LWDFActor actor = entry.getValue();
            String arguList = "";
            LinkedHashMap<Integer, String> arguListMap = actor.getActorArguVarList();
            /* sort by key */
            Map<Integer, String> sortedarguListMap = new TreeMap<Integer, String>(arguListMap);
            boolean flag_=false;
            if(sortedarguListMap != null || sortedarguListMap.size() != 0) {
                /* iterate for each argument */
                for (Map.Entry<Integer, String> argu : sortedarguListMap.entrySet()) {
                    String arg_val = argu.getValue();
                    String argu_str;

                    if(arg_val.contains("fifo_") || arg_val.contains("FIFO_") && !arg_val.contains("&") ) {
                            if( !flag_ ){
                                /* fifo argument */
                                String fifo_name =arg_val.substring(5);
                                argu_str = "this->fifos[ FIFO_" + fifo_name.toUpperCase() + "]";
                            }else{
                                argu_str = arg_val;
                                flag_ = false;
                            }
                    }else if(arg_val.contains("fifo_") || arg_val.contains("FIFO_") && arg_val.contains("&") ){
                            if( !arg_val.contains("begin") ) {
                                String fifo_name = arg_val.substring(5);
                                argu_str = "this->fifos";
                                flag_= true;
                            }
                            else{
                                argu_str = arg_val;
                            }
                    }
                    else {
                        /* rest arguments */
                        argu_str = arg_val;
                    }
                    /* concatenate argument */
                    arguList = arguList.concat(argu_str);
                    arguList = arguList.concat(", ");
                }
            }else{
                arguList = "";
            }

            int len = arguList.length();
            if(len < 2){
                arguList = "";
                return;
            }
            len = len-2;
            arguList = arguList.substring(0, len);
            _actorNewExtraArguList.put(actor, arguList);
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    ////                           Members                                ////

    private SDFGraph _graph;
    private DIFtoHierarchy _app_h;
    private HashMap<Node, LWDFActor> _contextMap;
    private HashMap<Edge, LWDFEdge> _edgeMap;
    private LWDFGraph _lwdfGraph;
    private String _outputDir;
    private String _appGraph_Name;
    private String _appGraphName;
    private String _appGraphHeader_Name;
    private String _appGraphSource_Name;
    private Gcm _gcm_h;
    private Gcm _gcm_s;
    private String _welt_cpp_prefix = "welt_cpp_";
    private String _welt_c_prefix = "welt_c_";
    private String _graphNewArguList;
    private String _graphNewFieldList;
    private String _graphNewFieldListInit;
    private HashMap<LWDFActor, String> _actorNewExtraArguList;
    private Set<String> _ActorType;

}
