/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif2lide;

import dif.csdf.sdf.SDFGraph;
import dif.util.graph.Edge;
import dif.util.graph.Node;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
//import mocgraph.Node;

public class ApplGraphFileWriter {

    public ApplGraphFileWriter(String sourceFileName, String applGraphHeaderName, SDFGraph graph,
                               HashMap<Node, LWDFActor> contextMap, HashMap<Edge, LWDFEdge> fifoMap) {
        try {
            _gcm = new Gcm(sourceFileName);
        } catch (Exception e) {
            System.out.println("Error: Cannot find " + sourceFileName);
            e.printStackTrace();
        }

        _graph = graph;
        _graphName = _graph.getName();
        _nameSpace = _graphName + "::";
        _fifoMap = fifoMap;
        _contextMap = contextMap;
        _applGraphHeaderFileName = applGraphHeaderName;

        //_assignmentMap = assignmentMap;
        //_memspaceMap = memspaceMap;
        //_platform = platform;
        //_nThreads = nThreads;
        //_repetitions = repetitions;
        initIncludeHeaders(applGraphHeaderName);

    }

    public void run() {
        //GlobalSettings.debugMessage("Source code writer: Writing source code ...");
        //writeDefines();
        writeHeaders();

        //writeMethodDefinitions(); 
        // _sourcePrinter.flush();
        writeCreateFunction();
        writeTerminateFunction();

        //_sourcePrinter.close(); 
        _gcm.close();
    }

    private void initGPU() {
        //printCodeLine("");
        _gcm.nextLine();

        //printCodeLine("printf(\"Initialize GPU\\n\")");
        _gcm.writeLine("printf(\"Initialize GPU\\n\");");
        //printCodeLine("");
        _gcm.nextLine();

        _gcm.writeLine("gpu = lide_ocl_gpu_new();");
        _gcm.writeLine("if(gpu==NULL){");
        _gcm.incIndent();
        _gcm.writeLine("fprintf(stderr, \"fail to create gpu context\");");
        _gcm.writeLine("exit (-1);");
        _gcm.decIndent();
        _gcm.writeLine("}");
        _gcm.nextLine();

        _gcm.writeLine("err = lide_ocl__gpu_get_platformID(gpu, 1, NULL);");
        _gcm.writeLine("if(err != true){");
        _gcm.incIndent();
        _gcm.writeLine("fprintf(stderr, \"Failed to get GPU platform ID.\\n\");");
        _gcm.writeLine("exit (-1);");
        _gcm.decIndent();
        _gcm.writeLine("}");
        _gcm.nextLine();


        _gcm.writeLine("err = lide_ocl__gpu_get_deviceID(gpu, CL_DEVICE_TYPE_GPU, 1, NULL);");

        _gcm.writeLine("if(err != true){");
        _gcm.incIndent();
        _gcm.writeLine("fprintf(stderr, \"Failed to get GPU device ID.\\n\");");
        _gcm.writeLine("exit (-1);");
        _gcm.decIndent();
        _gcm.writeLine("}");
        _gcm.nextLine();


        _gcm.writeLine("err = lide_ocl_gpu_context_new(gpu, 0, 1, NULL, NULL);");
        _gcm.writeLine("if(err != true){");
        _gcm.incIndent();
        _gcm.writeLine("fprintf(stderr, \"Failed to create context.\\n\");");
        _gcm.writeLine("exit (-1);");
        _gcm.decIndent();
        _gcm.writeLine("}");
        _gcm.nextLine();


        _gcm.writeLine("err = lide_ocl_gpu_cmdq_new(gpu, 0);");
        _gcm.writeLine("if(err != true){");
        _gcm.incIndent();
        _gcm.writeLine("fprintf(stderr, \"Failed to create command queue.\\n\");");
        _gcm.writeLine("exit (-1);");
        _gcm.decIndent();
        _gcm.writeLine("}");
        _gcm.nextLine();


        _gcm.writeLine("printf(\"Initialize GPU finishes\\n\");");


        _gcm.nextLine();

    }

    // a simple method to write main functions.
    private void writeCreateFunction() {


        if (!hasParameters()) {
            _gcm.write("struct");
            _gcm.write(" _lide_ocl_");
            _gcm.write(_graphName);
            _gcm.write("_graph_context_struct {");

            _gcm.next();

            _gcm.write("#include ");

            _gcm.write("\"lide_ocl_graph_context_type_common.h\"");
            _gcm.next();
            _gcm.incIndent();
            for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()) {
                if (!entry.getValue().getFileName().equals("no_name")) {
                    _gcm.write("char *" + entry.getValue().getInstanceName() + "_file;");
                    _gcm.next();
                }
            }
            _gcm.decIndent();
            _gcm.writeLine("};");

            _gcm.nextLine();

            _gcm.write("lide_ocl_");
            _gcm.write(_graphName);
            _gcm.write("_graph_");
            _gcm.write("context_");
            _gcm.write("type");

            _gcm.write(" *lide_ocl_");
            _gcm.write(_graphName);
            _gcm.write("_graph");
            _gcm.write("_new(");

			/*for( Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet() ){
				if(!entry.getValue().getFileName().equals("no_name")){
				_gcm.write(" char *" + entry.getValue().getInstanceName()+"_file,");
				}
			}*/

            _gcm.write("lide_ocl_gpu_pointer gpu){");
            _gcm.next();
            _gcm.incIndent();

            _gcm.write("int token_size,");
            _gcm.write("i;");
            _gcm.next();

            _gcm.nextLine();

            _gcm.write("lide_ocl_");
            _gcm.write(_graphName);
            _gcm.write("_graph");
            _gcm.write("_context");
            _gcm.write("_type");
            _gcm.write(" *context =");
            _gcm.write(" NULL;");
            _gcm.next();

            _gcm.write("context =");
            _gcm.write(" (lide_ocl_");
            _gcm.write(_graphName);
            _gcm.write("_graph");
            _gcm.write("_context");
            _gcm.write("_type *)");
            _gcm.write(" lide_ocl_util_malloc(sizeof(");
            _gcm.write("lide_ocl_");
            _gcm.write(_graphName);
            _gcm.write("_graph_context_type");
            _gcm.write("));");
            _gcm.next();

            _gcm.writeLine("context->gpu = gpu;");
            _gcm.writeLine("context->actor_count = ACTOR_COUNT;");

            _gcm.write("context->actors =");
            _gcm.write(" (lide_ocl_");
            _gcm.write("actor_");
            _gcm.write("context_type **)");
            _gcm.write(" lide_ocl_util_malloc(");
            _gcm.write("context->actor_count");
            _gcm.write(" *sizeof(");
            _gcm.write("lide_ocl");
            _gcm.write("_actor_context");
            _gcm.write("_type *));");
            _gcm.next();

            _gcm.writeLine("context->fifo_count = FIFO_COUNT;");

            _gcm.write("context->fifos =");
            _gcm.write(" (lide_ocl_fifo_pointer *)");
            _gcm.write("lide_ocl_util_malloc(");
            _gcm.write("context->fifo_count *");
            _gcm.write("sizeof(");
            _gcm.write("lide_ocl_fifo_pointer));");
            _gcm.next();
			
			/*_gcm.write("context->descriptors =");
			_gcm.write("(char **)");
			_gcm.write("lide_ocl_util_malloc(");
			_gcm.write("context->actor_count *");
			_gcm.write("sizeof(");
			_gcm.write("char*));");
			_gcm.next();*/
			
	/*		_gcm.writeLine("for(i = 0; i < context->actor_count; i++){");
			_gcm.incIndent();
			_gcm.write("context->descriptors[i] = (char *)lide_ocl_util_malloc(");
			_gcm.write("context->actor_count");
			_gcm.write(" * sizeof(");
			_gcm.write("char *));");
			_gcm.next();
			_gcm.write("}");
			_gcm.next();
			_gcm.decIndent();*/
			
		/*	for( Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet() ){
			_gcm.writeLine("strcpy(context->descriptors[ACTOR_"+ entry.getValue().getInstanceName().toUpperCase()+"],\""+ entry.getValue().getInstanceName() +"\");");
			}*/

            _gcm.nextLine();

            for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()) {
                if (!entry.getValue().getFileName().equals("no_name")) {
                    _gcm.write("context->" + entry.getValue().getInstanceName() + "_file = \"" + entry.getValue().getFileName() + "\";");
                    _gcm.next();
                }
            }

            _gcm.nextLine();

            _gcm.writeLine("token_size = sizeof(int);");

            _gcm.nextLine();

            _gcm.writeLine("for(i = 0; i<context->fifo_count; i++){");
            _gcm.incIndent();
            _gcm.write("context->fifos[i] = ");
            _gcm.write("(lide_ocl_fifo_type *)");
            _gcm.write("lide_ocl_fifo_basic_new(BUFFER_CAPACITY, token_size, i);");
            _gcm.next();
            _gcm.write("}");
            _gcm.next();
            _gcm.decIndent();
            _gcm.nextLine();

            _gcm.writeLine("i = 0;");
            _gcm.nextLine();

            for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()) {

                LWDFActor actor = entry.getValue();
                String fileName = actor.getFileName();
                ArrayList<Port> inputPorts = actor.getPorts(1);
                ArrayList<Port> outputPorts = actor.getPorts(0);
                ArrayList<String> inputPortNames = new ArrayList<String>();
                ArrayList<String> outputPortNames = new ArrayList<String>();

                for (Port inPort : inputPorts) {
                    Edge inEdge = inPort.edge;

                    inputPortNames.add(_graph.getName(inEdge));
                }

                for (Port outPort : outputPorts) {
                    Edge outEdge = outPort.edge;

                    outputPortNames.add(_graph.getName(outEdge));
                }

                _gcm.write("context->actors");
                _gcm.write("[ACTOR_");
                _gcm.write(entry.getValue().getInstanceName().toUpperCase() + "]");

                _gcm.write("= (lide_ocl_actor_context_type *)");
                _gcm.write("(lide_ocl_" + entry.getValue().getName() + "_new(");


                int indexhld1 = 0;

                if (!entry.getValue().getFileName().equals("no_name")) {
                    _gcm.write("context->");
                    _gcm.write(entry.getValue().getInstanceName() + "_file,");
                }


                if (inputPortNames.size() > 0) {
                    for (String name : inputPortNames) {
                        indexhld1 += 1;
                        if (indexhld1 < inputPortNames.size())
                            _gcm.write("(lide_ocl_fifo_basic_pointer)context->fifos[FIFO_" + name.toUpperCase() + "],");
                        else if (outputPortNames.size() > 0)
                            _gcm.write("(lide_ocl_fifo_basic_pointer)context->fifos[FIFO_" + name.toUpperCase() + "],");
                        else
                            _gcm.write("(lide_ocl_fifo_basic_pointer)context->fifos[FIFO_" + name.toUpperCase() + "]");
                    }
                }

                indexhld1 = 0;
                if (outputPortNames.size() > 0) {
                    for (String name : outputPortNames) {
                        indexhld1 += 1;
                        if (indexhld1 < outputPortNames.size())
                            _gcm.write("(lide_ocl_fifo_basic_pointer)context->fifos[FIFO_" + name.toUpperCase() + "],");
                        else
                            _gcm.write("(lide_ocl_fifo_basic_pointer)context->fifos[FIFO_" + name.toUpperCase() + "]");
                    }
                }
                if (actor.isCUDAEnabled()) {
                    _gcm.write(",gpu,");
                    _gcm.write("1");
                    //_gcm.write("));");
                }


                _gcm.write(",ACTOR_" + entry.getValue().getInstanceName().toUpperCase() + "));");


                _gcm.next();
                _gcm.nextLine();
            }

            _gcm.writeLine("return context;");
            _gcm.decIndent();
            _gcm.write("}");

        }

			
			/*
			int i=0;
			_gcm.writeLine("int main(int argc, char* argv[]) {"); 
			_gcm.incIndent(); 
			_gcm.writeLine("lide_ocl_actor_context_type *actors[ACTOR_COUNT];");
			_gcm.writeLine("lide_ocl_gpu_pointer gpu;");
			_gcm.writeLine("bool err;");
			_gcm.nextLine();
			
			String fifoDec= "lide_ocl_fifo_pointer"+ " ";
			_gcm.write(fifoDec);

			for (Map.Entry<Edge, LWDFEdge> entry : _fifoMap.entrySet()){
				i+=1;
				if (i<_fifoMap.size()){
					fifoDec=entry.getValue().getName()+" = NULL, ";
					_gcm.write(fifoDec);
				}
				else{
					fifoDec=entry.getValue().getName()+" = NULL; ";
					_gcm.write(fifoDec);
				}
					
			}
			
			//fifoDec=fifoDec.substring(0,fifoDec.length()-2)+";";
			//printCodeLine(fifoDec);
			_gcm.next();
			_gcm.nextLine();
			_gcm.writeLine("int token_size = 0;");
			_gcm.writeLine("int i = 0;");
			_gcm.writeLine("int arg_count = 5;");
			_gcm.nextLine();
			
			
			initGPU();
			int count=0;
			_gcm.write("const char *descriptors[ACTOR_COUNT] = {");
			for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()){
				count+=1;
				LWDFActor actor1 = entry.getValue();
				if (count==_contextMap.entrySet().size())
					_gcm.write("\""+actor1.getInstanceName()+"\"");
				else
					_gcm.write("\""+actor1.getInstanceName()+"\""+",");
				
					
			}
			_gcm.write("};");
			
			_gcm.next();
			
			_gcm.writeLine("i = 1;");
			
			
			for (Map.Entry<Edge, LWDFEdge> entry : _fifoMap.entrySet()){
		        //_gcm.writeLine(entry.getValue().getName()+" = lide_ocl_fifo_new(BUFFER_CAPACITY, token_size);");
				_gcm.writeLine("token_size = sizeof("+ entry.getValue().getEdgeType()+");");
				_gcm.write(entry.getValue().getName());
				_gcm.write(" = lide_ocl_fifo_new(BUFFER_CAPACITY, token_size);");
				_gcm.next();
			}
			_gcm.nextLine();
			_gcm.writeLine("i = 0;");
			
			for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()){
	        	
	        	LWDFActor actor = entry.getValue();
	        	String fileName= actor.getFileName();
	        	ArrayList<Port> inputPorts = actor.getPorts(1); 
	        	ArrayList<Port> outputPorts = actor.getPorts(0); 
	        	ArrayList<String> inputPortNames = new ArrayList<String>();
	        	ArrayList<String> outputPortNames = new ArrayList<String>();
	        	
	        	for (Port inPort : inputPorts){
	        		Edge inEdge = inPort.edge; 
	        		
	        		inputPortNames.add(_graph.getName(inEdge)); 
	        	}
	        	
	        	for (Port outPort : outputPorts){
	        		Edge outEdge = outPort.edge;
		        		        		
	        		outputPortNames.add(_graph.getName(outEdge)); 
	        	}
	        	
	        	int indexhld=0;
	        	
	        	_gcm.write("actors[ACTOR_");
	        	_gcm.write(actor.getInstanceName().toUpperCase());
	        	_gcm.write("] = (lide_ocl_actor_context_type *)");
	        	_gcm.write("(lide_ocl_"+actor.getName()+"_new(");
	        	
	        	if(!fileName.equals("no_name"))
	        		_gcm.write("\""+fileName+"\",");
	        	if (inputPortNames.size()>0){
	        		for (String name : inputPortNames){
	        			indexhld+=1;
	        			if (indexhld<inputPortNames.size())
	        				_gcm.write(name+",");
	        			else
	        				if(outputPortNames.size()>0)
	        					_gcm.write(name+",");
	        				else
	        					_gcm.write(name);
	        		}
	        	}
	        	
	        	indexhld=0;
	        	if(outputPortNames.size()>0){
	        		for (String name : outputPortNames){
	        			indexhld+=1;
	        			if (indexhld<outputPortNames.size())
	        				_gcm.write(name+",");
	        			else
	        				_gcm.write(name);
	        		}
	        	}
	        	
	        /*	if (actor.isCUDAEnabled()){
	        		_gcm.write(",gpu,");
	        		_gcm.write("1");
	        		_gcm.write("));");
			}
	        		else
	*/        	
	/*        	_gcm.write("));");
	       	
	        	_gcm.next();
	        	_gcm.nextLine();
	        	
			}
			
			_gcm.nextLine();
			_gcm.writeLine("lide_ocl_dsg_scheduler(actors, ACTOR_COUNT, (char **)descriptors);");
			_gcm.nextLine();
			
			for (Map.Entry<Edge, LWDFEdge> entry : _fifoMap.entrySet()){
		       _gcm.writeLine("lide_ocl_fifo_free("+entry.getValue().getName()+");");
		       
				}
			
			_gcm.nextLine();

			for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()){
	        	LWDFActor actor = entry.getValue();
	        	String indexName = "lide_ocl_"+actor.getName()+"_terminate((lide_ocl_file_source_context_type *)(actors[ACTOR_"+actor.getInstanceName().toUpperCase()+"]));";
				_gcm.write("lide_ocl_");
				_gcm.write(actor.getName());
				_gcm.write("_terminate((lide_ocl_"+actor.getName()+"_context_type *)");
				_gcm.write("(actors[ACTOR_");
				_gcm.write(actor.getInstanceName().toUpperCase()+"]));");
				_gcm.next();
			}
			
			_gcm.nextLine();
			_gcm.writeLine("lide_ocl_gpu_cleanup(gpu);");
			_gcm.writeLine("return 0;");
			_gcm.decIndent(); 
			_gcm.writeLine("}"); 
			GlobalSettings.debugMessage("Code writer: Info: Graph has unspecified parameters, skip main()"); 
		} else {
			_gcm.flush1();
			}
	}*/

    }
    // if the graph has unspecified parameters, then the main function cannot be generated.

    private void writeTerminateFunction() {
        _gcm.nextLine();
        _gcm.nextLine();

        _gcm.write("void lide_ocl_");
        _gcm.write(_graphName);
        _gcm.write("_graph_terminate(");

        _gcm.write("lide_ocl_" + _graphName + "_graph_context_type");
        _gcm.write(" *context){");
        _gcm.next();
        _gcm.incIndent();
        _gcm.nextLine();

        _gcm.writeLine("int i;");
        _gcm.nextLine();

        _gcm.writeLine("for(i = 0; i<context->fifo_count; i++){");

        _gcm.incIndent();
        _gcm.write("lide_ocl_fifo_basic_free(");
        _gcm.write("(lide_ocl_fifo_basic_pointer)context->fifos[i]);");
        _gcm.next();
        _gcm.decIndent();
        _gcm.writeLine("}");

        for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()) {
            _gcm.write("lide_ocl_" + entry.getValue().getName() + "_terminate((");
            _gcm.write("lide_ocl_" + entry.getValue().getName() + "_context_type *)");

            _gcm.write("context->");

            _gcm.write("actors[ACTOR_" + entry.getValue().getInstanceName().toUpperCase() + "]");
            _gcm.write(");");
            _gcm.next();

        }

        _gcm.nextLine();
        _gcm.writeLine("lide_ocl_gpu_cleanup(context->gpu);");
        _gcm.writeLine("free(context->fifos);");
        _gcm.writeLine("free(context->actors);");
        //_gcm.writeLine("free(context->descriptors);");
        _gcm.writeLine("free(context);");
        _gcm.writeLine("return;");
        _gcm.decIndent();
        _gcm.writeLine("}");

    }

    private boolean hasParameters() {
        for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()) {
            LWDFActor actor = entry.getValue();
            String actorName = actor.getInstanceName();
            if (actor.getParameterNames().size() != 0) {
                return true;
            }
        }
        return false;
    }

    private void writeHeaders() {
        _gcm.writeLine("#include <stdio.h>");
        _gcm.writeLine("#include <stdlib.h>");
        _gcm.writeLine("#include <string.h>");
        _gcm.writeLine("#include \"lide_ocl_basic.h\"");
        _gcm.writeLine("#include \"lide_ocl_actor.h\"");
        _gcm.writeLine("#include \"" + _applGraphHeaderFileName + "\"");
        _gcm.nextLine();
        _gcm.writeLine("#define NAME_LENGTH 20");

        //_gcm.writeLine("#else");
        //_gcm.writeLine("#include <CL/cl.h>");
        //_gcm.writeLine("#endif");
        //_gcm.nextLine();
        //_gcm.writeLine("#include \"define.h\"");

        //_gcm.nextLine();
		
		/*for (String line : _headers){
			_gcm.writeLine(line);

		}*/

        _gcm.nextLine();
        _gcm.flush1();
    }

    private void writeDefines() {
		
		
		/*int i=0;
		_gcm.nextLine();	
		_gcm.writeLine("#define BUFFER_CAPACITY 1024");
		_gcm.nextLine();	

		for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()){
        	
        	LWDFActor actor = entry.getValue(); 
        	String indexName = "ACTOR_"+ actor.getInstanceName().toUpperCase()+" "+i;
        	_gcm.writeLine("#define "+ indexName);
        	i++;
		}
		
		_gcm.writeLine("#define ACTOR_COUNT "+ _contextMap.size());
		_gcm.nextLine();	
		_gcm.flush1();
		*/

    }

    private void initIncludeHeaders(String applGraphHeaderName) {
        _headers = new LinkedList<String>();
        //_headers.add("#include <stdio.h>");
        //_headers.add("#include <stdlib.h>");
        //_headers.add("#include \"lide_ocl_fifo.h\"");
        //_headers.add("#include \"lide_ocl_util.h\"");
        //_headers.add("#include \"lide_ocl_gpu.h\"");
        _headers.add("#include \"" + applGraphHeaderName + "\"");
        //	_headers.add("#ifdef __APPLE__");
        //	_headers.add("#include <OpenCL/cl.h>");
        //	_headers.add("#else");
        //	_headers.add("#include <CL/cl.h>");
        //	_headers.add("#endif");


        for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()) {
            LWDFActor actor = entry.getValue();
            // actors[INDEX] = lide_cuda_new(...
            String headerName = "#include \"" + _LIDE_OCL_PREFIX + actor.getName() + ".h\"";
            _headers.add(headerName);
        }


    }
	

    /*private void writeFullConstructor() {
    	diagMessage("Writing full constructor definition...");
    	_indentation = 0; 
        // construct actor context. Call this after vectorization.
    	printCodeLine(_nameSpace + constructorDeclaration()); 
        printCodeLine("{"); 
        _indentation += 4; 
        // constructor declaration here
        printCodeLine("/* Full constructor "); 
        // Construct edges.
        for (Map.Entry<Edge, LWDFEdge> entry : _fifoMap.entrySet()){
        	Edge edge = entry.getKey(); 
        	LWDFEdge fifo = entry.getValue(); 
        	String typeName = fifo.getEdgeType(); 
        	String capacity = "" + fifo.getCapacity(); 
        	Integer memLocation = _memspaceMap.get(edge); 
        	String memspace = memLocation == 1 ? "GPU" : "CPU";
        	String expression = fifo.getName() + " = " + _LIDE_CUDA_PREFIX + 
        			"fifo_new(" + capacity + ", "+"sizeof(" +  
                    typeName + "), " + memspace + "); " ; 
        	printCodeLine(expression); 
        }
        // Construct actors.
        // inputPortType inputEdgeName, outputPortType outputPortName, prodRate, consRate, 
        // ParameterType ParameterNames 
        for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()){
        	Node node = entry.getKey(); 
        	LWDFActor actor = entry.getValue(); 
        	// actors[INDEX] = lide_cuda_new(...
        	String newFunctionName = _LIDE_CUDA_PREFIX + actor.getName() + "_new"; 
        	String indexName = "actors[" + actor.getInstanceName().toUpperCase() + "]";
        	// prepare port informations
        	ArrayList<Port> inputPorts = actor.getPorts(1); 
        	ArrayList<Port> outputPorts = actor.getPorts(0); 
        	ArrayList<String> inputPortNames = new ArrayList<String>();
        	ArrayList<String> outputPortNames = new ArrayList<String>();
        	ArrayList<String> consRates = new ArrayList<String>(); 
        	ArrayList<String> prodRates = new ArrayList<String>(); 
        	for (Port inPort : inputPorts){
        		Edge inEdge = inPort.edge; 
        		inputPortNames.add(_graph.getName(inEdge)); 
        		SDFEdgeWeight weight = (SDFEdgeWeight) inEdge.getWeight();
        		int consRate = weight.getSDFConsumptionRate();
        		consRates.add("" + consRate); 
        	}
        	for (Port outPort : outputPorts){
        		Edge outEdge = outPort.edge; 
        		outputPortNames.add(_graph.getName(outEdge)); 
        		int prodRate = ((SDFEdgeWeight) outEdge.getWeight()).getSDFProductionRate(); 
        		prodRates.add("" + prodRate); 
        	}
        	// start writing
        	// parameters
        	ArrayList<String> paramNames = actor.getParameterNames(); 
        	// put everything together
        	ArrayList<String> argList = new ArrayList<String>(); 
        	argList.addAll(inputPortNames);
        	argList.addAll(outputPortNames); 
        	argList.addAll(consRates);
        	argList.addAll(prodRates); 
        	argList.addAll(paramNames); 
        	if (actor.getType() != 0){
        		argList.add(actor.getTokenSizeUnit()); 
        	}
        	String arguments = Utilities.commaSeparatedString(argList);
        	
        	Integer processor = _assignmentMap.get(node);
        	int processorType = _platform.getProcessorType(processor); 
        	String mappingStr = ""; 
        	if (processorType == 1){
        		mappingStr = "GPU"; 
        	} else { // other types, currently not available, map to CPU
        		mappingStr = "CPU"; 
        	}
        	String lideNewCall = newFunctionName + "(" + arguments + ", " + mappingStr + ");"; 
        	printCodeLine(indexName + " = " + "(lide_c_actor_context_type*) " + lideNewCall);
        }
        printCodeLine(""); 
        
        // Construct actors descriptors. 
        for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()){
        	LWDFActor actor = entry.getValue(); 
        	String descriptorName = "descriptors[" + actor.getInstanceName().toUpperCase() + "]";
        	printCodeLine(descriptorName + " = \"" + actor.getInstanceName() + "\"; "); 
        }
        printCodeLine(""); 
        
        // Construct thread schedule file list
        ArrayList<String> fileNameList = new ArrayList<String>(); 
        for (int i = 0; i < _nThreads; i++){
        	fileNameList.add("\"" + _SCHEDULE_FILE_PREFIX + i + ".txt\""); 
        }
        String fileNames = Utilities.commaSeparatedString(fileNameList);
        String fileNamesInitString = "char* file_names[" + _nThreads +
        		"] = {" + fileNames + "};"; 
        printCodeLine(fileNamesInitString); 
        
        // Construction thread initialization function
        // last parameter is the repetition count
       // String initParams = "NUMBER_OF_THREADS, file_names, actors, ACTOR_COUNT, descriptors, " + _repetitions; 
        // that 1 in the previous line means "do not duplicate the schedule" 
        //printCodeLine("thread_list = " + 
        //			_multithreadInitFunctionName + "(" + initParams + ");");
        _indentation -= 4; 
        printCodeLine("}"); 
    }
	/*
	/*
    private void writeFullDestructor() {
        // actor context.
        diagMessage("Writing full destructor definition...");
        printCodeLine(_nameSpace + "~" + _graphName + "()"); 
        printCodeLine("{"); 
        _indentation += 4; 
        // Destruct thread list. 
        printCodeLine(_multithreadTerminateFunctionName + "(thread_list);");
        printCodeLine(""); 
        // Destruct edge list.
        for (Map.Entry<Edge, LWDFEdge> entry : _fifoMap.entrySet()){
        	LWDFEdge fifo = entry.getValue(); 
        	String edgeName = fifo.getName(); 
        	printCodeLine(_LIDE_CUDA_PREFIX + "fifo_free(" + edgeName + ");"); 
        }
        printCodeLine(""); 
        // Destruct actor list. 
        for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()){
        	LWDFActor actor = entry.getValue(); 
        	String name = actor.getName(); 
        	String type = _LIDE_CUDA_PREFIX + name + "_context_type*"; 
        	String terminateFunction = _LIDE_CUDA_PREFIX + name + "_terminate" + "(" + 
            		"(" + type + ")" + "actors[" + actor.getInstanceName().toUpperCase() + "]);";
        	 printCodeLine(terminateFunction); 
        }
        printCodeLine(""); 
        _indentation -= 4; 
        printCodeLine("}"); 
    }
*/    

    /*private int memorySpace(Edge edge){
    	LWDFActor srcActor = _contextMap.get(edge.source()); 
    	LWDFActor snkActor = _contextMap.get(edge.sink()); 
    	int srcProcessorType = _platform.getProcessorType(_assignmentMap.get(srcActor)); 
    	int snkProcessorType = _platform.getProcessorType(_assignmentMap.get(snkActor)); 
    	if (srcProcessorType == 1 && snkProcessorType == 1){
    		return 1; 
    	} else {
    		return 0; 
    	}    	
    }
    
    private String constructorDeclaration() {
        String constructor = _graphName + "(";
    	ArrayList<String> argList = new ArrayList<String>(); 
    	for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()){
    		LWDFActor actor = entry.getValue();
    		ArrayList<String> paramNames = actor.getParameterNames(); 
    		ArrayList<String> paramTypes = actor.getParameterTypes(); 
    		for (int i = 0; i < paramNames.size(); i++) {
        		argList.add(paramTypes.get(i) + " " + paramNames.get(i));   
        	}
    	}
    	constructor += Utilities.commaSeparatedString(argList); 
    	constructor += ")"; 
    	return constructor; 
    }
    */
	
				
			
			
			
			
			/*
			spaces = new String(new char[_indentation]).replace('\0',' ');
			count=Math.ceil((code.length()/80.0));
			_sourcePrinter.println(spaces + code.substring(0,80));
			_indentation+=8;
			count-=1;
			spaces = new String(new char[_indentation]).replace('\0',' ');
			int start=80;
			
			while(count>1){
				_sourcePrinter.println(spaces + code.substring(start,start+80));
				start=start+80;
				count-=1;
			}
			
			_sourcePrinter.println(spaces + code.substring(start));
			_indentation-=8;*/


    private LinkedList<String> _headers;
    private PrintWriter _sourcePrinter;
    private Gcm _gcm;

    //    private final int _nThreads;
    //private final int _repetitions; // number of repetitions 
    private final SDFGraph _graph;
    private final String _applGraphHeaderFileName;
    private final String _graphName;
    private final String _nameSpace;
    private final HashMap<Node, LWDFActor> _contextMap;
    private final HashMap<Edge, LWDFEdge> _fifoMap;
//	private final HashMap<Node, Integer> _assignmentMap; 
//	private final HashMap<Edge, Integer> _memspaceMap; 

    /**************** Constant Strings ****************/
    private static final String _LIDE_C_PREFIX = "lide_c_";
    private static final String _LIDE_OCL_PREFIX = "lide_ocl_";

}
