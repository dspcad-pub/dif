/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif2lide;

import dif.DIFHierarchy;
import dif.DIFLoader;
import dif.csdf.sdf.SDFGraph;


class DIFtoLWDFDsg {
	public DIFtoLWDFDsg(String DsgFileName, String outputDir){
		
		_outputFileName=outputDir+"schedule.h";
		readDsgGraph(DsgFileName);
		
		
		
		
		
	}
	


	private void readDsgGraph(String fileName) {
        _topHierarchy = DIFLoader.loadDataflow(fileName);
        _graph = (SDFGraph)_topHierarchy.getGraph();

//        try {
//            Reader reader = new Reader(fileName);
//            reader.compile();
//            _topHierarchy = reader.getTopHierarchy();
//	        _graph = (SDFGraph)_topHierarchy.getGraph();
//        } catch (IOException exp) {
//            throw new RuntimeException(exp.getMessage());
//        } catch (DIFLanguageException exp) {
//            throw new RuntimeException(exp.getMessage());
//        }
    }

	//public void run(){
		//_dsgWriter = new DsgWriter(_graph, _contextMap, _edgeMap, 
			//	_actorAssignments, _edgeMemspace, _platform,  
				//_scheduleLists, _nThreads, _outputFileName/*, _repetitions*/);		
		
	//}

private DIFHierarchy _topHierarchy; 
private SDFGraph _graph;
private String _outputFileName;
private DsgWriter _dsgWriter;
}
