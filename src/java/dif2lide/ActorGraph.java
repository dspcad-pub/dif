/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif2lide;

import dif.csdf.sdf.SDFGraph;
import dif.util.graph.Edge;
import dif.util.graph.Node;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class ActorGraph {
	private HashMap<String,GraphNode> _nodeList = new HashMap<String,GraphNode>();
	public GraphNode startLoop;
	public ArrayList<ArrayList<String>> _clustered = new ArrayList<ArrayList<String>>();
	
	public class GraphNode{
		private String _name;
		private String _type;
		private boolean _mark=false;
		
		private ArrayList<GraphNode> _connected = new ArrayList<GraphNode>();
		
		public GraphNode(String name,String type){
			_name=name;
			_type=type;
		}
		
		public void addConnection(GraphNode name){
			_connected.add(name);
		}
		
		public void print(){
			System.out.print("Actor Name: ");
			System.out.println(_name);
			System.out.print("Connections :");
			for(GraphNode node : _connected)
				System.out.print(node._name+ " ");
			System.out.println(" ");
			System.out.println(" ");

			
		}
	}
	public void printClusters(){
		System.out.println();
		for (ArrayList<String> each : _clustered){
			
			for (String Nodes : each)
				System.out.print(Nodes+ " ");
			
		}
		System.out.println();
	}
	public ActorGraph(HashMap<String,String> snkArray, HashMap<Node,LWDFActor> actors, SDFGraph dsggraph ){
		
		ArrayList<Port> outputPorts;
        ArrayList<String> outputPortNames;		
		for(Map.Entry<Node,LWDFActor> entry : actors.entrySet() ){
			LWDFActor actor = entry.getValue();
			String actorName = actor.getInstanceName();
			GraphNode node = new GraphNode(actorName,actor.getName());
        	_nodeList.put(actorName , node);
    		
		}
		
		
		for(Map.Entry<Node,LWDFActor> entry : actors.entrySet() ){
			LWDFActor actor = entry.getValue();
			String actorName = actor.getInstanceName();
			outputPorts = actor.getPorts(0); 
        	outputPortNames = new ArrayList<String>();
			GraphNode node =_nodeList.get(actorName);
			
			for (Port outPort : outputPorts){
        		Edge outEdge = outPort.edge;
	        		        		
        		outputPortNames.add(dsggraph.getName(outEdge)); 
        	}

        	for (String portName : outputPortNames){	
        		node.addConnection(_nodeList.get(snkArray.get(portName)));
        	}
		
		}
		
		for (Map.Entry<String,GraphNode> entry : _nodeList.entrySet()){
			
			entry.getValue().print();
			if ( entry.getValue()._type.equals("sca_sch_loop") ){
				startLoop=entry.getValue();
			}
			
			
		}

	
	}

	public ArrayList<ArrayList<String>> dfs(){
		ArrayList<String> tempClusters=new ArrayList<String>(); 
		Stack<GraphNode> s = new Stack<GraphNode>();
		s.push(startLoop);
		while (!s.isEmpty()){
			GraphNode node = s.pop();
			System.out.println(node._name);

			node._mark=true;
			
			if (node._type.equals("ref_actor")){
				tempClusters.add(node._name);
				if (!node._connected.get(0)._type.equals("ref_actor")){
					if(tempClusters.size()>1)
					_clustered.add(tempClusters);
					tempClusters=new ArrayList<String>();
				}
			}
				
			for(GraphNode each : node._connected){
				if (each._mark!=true) {
					s.push(each);
				}
		}
		}
	
		return _clustered;
		
		
	}
}
