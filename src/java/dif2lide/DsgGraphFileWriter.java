/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif2lide;

import dif.csdf.sdf.SDFGraph;
import dif.util.graph.Edge;
import dif.util.graph.Node;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
//import mocgraph.Node;

public class DsgGraphFileWriter {
	//DsgGraphFileWriter(String fileName, String dsgHeaderFileName,SDFGraph graph,SDFGraph dsggraph, HashMap<Node, LWDFActor> contextMap, HashMap<Node, LWDFActor> dsgContextMap
		//	,HashMap<Edge, LWDFEdge> fifoMap,HashMap<Edge, LWDFEdge> dsgFifoMap){
	public DsgGraphFileWriter(String fileName, String dsgHeaderFileName, SDFGraph graph, SDFGraph dsggraph,
			HashMap<Node, LWDFActor> contextMap, HashMap<Node, LWDFActor> dsgContextMap,
			HashMap<Edge, LWDFEdge> fifoMap, HashMap<Edge, LWDFEdge> dsgFifoMap) {
		try {
			_gcm = new Gcm(fileName);
		} catch(Exception e){
			System.out.println("Error: Cannot find " + fileName); 
			e.printStackTrace(); 
		}
		
		for (Map.Entry<Node, LWDFActor> entry : dsgContextMap.entrySet()){
			if(entry.getValue().getName().equals("ref_actor")| entry.getValue().getName().equals("sca_actor")){
					_isDSG=true;
					break;
		}
		}
		
		_dsgGraph = dsggraph;
		_graph = graph;
		_graphName = graph.getName(); 
		_dsgGraphName = _dsgGraph.getName();
		_fifoMap = fifoMap;
		_contextMap = contextMap;
		_dsgFifoMap = dsgFifoMap; 
		_dsgContextMap = dsgContextMap;
		_dsgGraphHeaderFileName=dsgHeaderFileName;
		
		//_assignmentMap = assignmentMap; 
		//_memspaceMap = memspaceMap; 
		//_platform = platform; 
		//_nThreads = nThreads; 
		//_repetitions = repetitions; 
		
	}
	
		// TODO Auto-generated constructor stub
	

	public void run(){
			
		if(_isDSG){
			writeHeaders();
			
	        writeCreateFunction();
	        writeTerminateFunction();
	        writeSupportingAPIs();
			_gcm.nextLine();
			_gcm.close();
		
			
		}
		
		else
			System.out.println("not a dsg graph");
	
		
	}

	private void writeSupportingAPIs(){
		_gcm.writeLine("");
		
		_gcm.write("lide_ocl_dsg_actor_context_type ** ");

		_gcm.write("lide_ocl_"+_dsgGraphName+"_graph_get_sink_array(");

		_gcm.write("lide_ocl_graph_context_type * ");

		_gcm.write("context){");

		_gcm.next();
		_gcm.incIndent();
		_gcm.writeLine("");

		_gcm.write("return");
		

		_gcm.write("((lide_ocl_"+_dsgGraphName+"_graph_context_type");
		

		_gcm.write("*) context)->");
		
		_gcm.write("dsg_sink_array;");
		

		_gcm.next();
		
		_gcm.decIndent();

		
		_gcm.write("}");
		
		_gcm.writeLine("");

		_gcm.writeLine("  ");
		
		_gcm.write("int * ");

		_gcm.write("lide_ocl_"+_dsgGraphName+"_graph_get_type_array(");

		_gcm.write("lide_ocl_graph_context_type * ");

		_gcm.write("context){");

		_gcm.next();
		_gcm.incIndent();
		_gcm.writeLine("");

		_gcm.write("return");
		
		
		_gcm.write("((lide_ocl_"+_dsgGraphName+"_graph_context_type");
		

		_gcm.write("*) context)->");
		
		_gcm.write("dsg_type_array;");
		

		_gcm.next();
		
		_gcm.decIndent();

		
		_gcm.write("}");
		
	}	
	private void writeTerminateFunction() {
		_gcm.nextLine();
		_gcm.nextLine();

		_gcm.write("void lide_ocl_"+_dsgGraphName+"_graph_terminate(");
		
		_gcm.write("lide_ocl_"+_dsgGraphName+"_graph_context_type *context){");
		_gcm.next();
		
		_gcm.incIndent();
		_gcm.nextLine();

		_gcm.writeLine("int i;");
		_gcm.nextLine();
		
		_gcm.writeLine("for(i = 0; i<context->fifo_count; i++){");

		_gcm.incIndent();
		_gcm.write("lide_ocl_fifo_unit_size_free((lide_ocl_fifo_unit_size_pointer)");
		_gcm.write("(context->fifos[i]));");
		_gcm.next();
		_gcm.decIndent();
		_gcm.writeLine("}");

		_gcm.nextLine();
		for( Map.Entry<Node, LWDFActor> entry : _dsgContextMap.entrySet() ){
			_gcm.write("lide_ocl_"+entry.getValue().getName()+"_terminate((");
			_gcm.write("lide_ocl_"+entry.getValue().getName()+"_context_type *)");

			_gcm.write("context->");

			_gcm.write("actors[DSG_ACTOR_"+entry.getValue().getInstanceName().toUpperCase()+"]");
			_gcm.write(");");
			_gcm.next();

			_gcm.nextLine();

			}
		
		_gcm.nextLine();
//		_gcm.writeLine("lide_ocl_gpu_cleanup(context->gpu);");
		_gcm.writeLine("free(context->fifos);");
		_gcm.writeLine("free(context->actors);");
		//_gcm.writeLine("free(context->descriptors);");
		_gcm.writeLine("free(context);");
		_gcm.writeLine("return;");
		_gcm.decIndent();
		_gcm.writeLine("}");
		
	}

	private void writeCreateFunction() {
			 int ifficount=0;
			 HashMap<String,ArrayList<Integer>> holder = new HashMap<String,ArrayList<Integer>>();
			_gcm.write("struct");
			_gcm.write(" _lide_ocl_");
			_gcm.write(_dsgGraphName);
			_gcm.write("_graph_context_struct {");
            
			_gcm.next();
			
			_gcm.write("#include ");
            
			_gcm.write("\"lide_ocl_graph_context_type_common.h\"");
			_gcm.next();
			
			
			_gcm.write("lide_ocl_dsg_actor_context_type** ");
			_gcm.write("dsg_source_array;");
			_gcm.next();

			_gcm.write("lide_ocl_dsg_actor_context_type** ");
			_gcm.write("dsg_sink_array;");
			_gcm.next();
			
			_gcm.write("int * ");
			_gcm.write("dsg_type_array;");
			_gcm.next();

			
			_gcm.incIndent();
			for( Map.Entry<Node, LWDFActor> entry : _dsgContextMap.entrySet() ){
				if(!entry.getValue().getFileName().equals("no_name")){
				_gcm.write("char *" + entry.getValue().getInstanceName()+"_file;");
				_gcm.next();
				}
			}

			for(Map.Entry<Node, LWDFActor> entry :_contextMap.entrySet()){
				
				LWDFActor actor = entry.getValue();
	        	String fileName= actor.getFileName();
	        	ArrayList<Port> inputPorts = actor.getPorts(1); 
	        	ArrayList<Port> outputPorts = actor.getPorts(0); 
	        	ArrayList<String> inputPortNames = new ArrayList<String>();
	        	ArrayList<String> outputPortNames = new ArrayList<String>();
				
	        	for (Port inPort : inputPorts){
	        		Edge inEdge = inPort.edge; 
	        		
	        		inputPortNames.add(_graph.getName(inEdge)); 
	        	}
	        	
	        	for (Port outPort : outputPorts){
	        		Edge outEdge = outPort.edge;
		        		        		
	        		outputPortNames.add(_graph.getName(outEdge)); 
	        	}
	        	ArrayList<Integer> inOut= new ArrayList<Integer>(2);
	        	inOut.add(inputPorts.size());
	        	inOut.add(outputPorts.size());
	        	holder.put(actor.getInstanceName(),inOut);
}			
			
			
			
			_gcm.decIndent();
			_gcm.writeLine("};");
			
			_gcm.nextLine();
			
			_gcm.write("lide_ocl_");
			_gcm.write(_dsgGraphName);
			_gcm.write("_graph_");
			_gcm.write("context_");
			_gcm.write("type");
			
			_gcm.write(" *lide_ocl_");
			_gcm.write(_dsgGraphName);
			_gcm.write("_graph");
			_gcm.write("_new(");
			
			/*for( Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet() ){
				if(!entry.getValue().getFileName().equals("no_name")){
				_gcm.write(" char *" + entry.getValue().getInstanceName()+"_file,");
				}
			}*/
			
			_gcm.write("lide_ocl_graph_context_type* ");
			_gcm.write("contextApp){");
			_gcm.next();
			_gcm.incIndent();

			_gcm.write("int token_size,");
			_gcm.write("i;");
			_gcm.next();
			
			_gcm.nextLine();
			
			_gcm.write("lide_ocl_");
			_gcm.write(_dsgGraphName);
			_gcm.write("_graph");
			_gcm.write("_context");
			_gcm.write("_type");
			_gcm.write(" *context =");
			_gcm.write(" NULL;");
			_gcm.next();
			
			_gcm.write("context =");
			_gcm.write(" (lide_ocl_");
			_gcm.write(_dsgGraphName);
			_gcm.write("_graph");
			_gcm.write("_context");
			_gcm.write("_type *)");
			_gcm.write(" lide_ocl_util_malloc(sizeof(");
			_gcm.write("lide_ocl_"+_dsgGraphName+"_graph_context_type");
			_gcm.write("));");
			_gcm.next();
			
			_gcm.writeLine("context->actor_count = DSG_ACTOR_COUNT;");
			
			_gcm.write("context->actors =");
			_gcm.write(" (lide_ocl_actor_context_type **)");
			_gcm.write(" lide_ocl_util_malloc(");
			_gcm.write("context->actor_count");
			_gcm.write(" *sizeof(");
			_gcm.write("lide_ocl_actor_context_type *));");
			_gcm.next();
			
			_gcm.writeLine("context->fifo_count = DSG_FIFO_COUNT;");

			_gcm.write("context->fifos =");
			_gcm.write(" (lide_ocl_fifo_type **)");
			_gcm.write("lide_ocl_util_malloc(");
			_gcm.write("context->fifo_count *");
			_gcm.write("sizeof(");
			_gcm.write("lide_ocl_fifo_type*));");
			_gcm.next();
			
			/*_gcm.write("context->descriptors =");
			_gcm.write("(char **)");
			_gcm.write("lide_ocl_util_malloc(");
			_gcm.write("context->actor_count *");
			_gcm.write("sizeof(");
			_gcm.write("char*));");
			_gcm.next();*/
			
			/*_gcm.writeLine("for(i = 0; i < context->actor_count; i++){");
			_gcm.incIndent();
			_gcm.write("context->descriptors[i] = (char *)lide_ocl_util_malloc(");
			_gcm.write("context->actor_count");
			_gcm.write(" * sizeof(");
			_gcm.write("char));");
			_gcm.next();
			_gcm.write("}");
			_gcm.next();
			_gcm.decIndent();*/
			
			/*for( Map.Entry<Node, LWDFActor> entry : _dsgContextMap.entrySet() ){
			_gcm.writeLine("strcpy(context->descriptors[DSG_ACTOR_"+ entry.getValue().getInstanceName().toUpperCase()+"],\""+ entry.getValue().getInstanceName() +"\");");
			}*/
			_gcm.writeLine("  ");
			_gcm.write("context->dsg_source_array =");
			_gcm.write(" (lide_ocl_dsg_actor_context_type**)");
			_gcm.write(" lide_ocl_util_malloc(");
			_gcm.write("context->fifo_count");
			_gcm.write(" *sizeof(");
			_gcm.write("lide_ocl_dsg_actor_context_type *));");
			_gcm.next();
			_gcm.writeLine("  ");

			_gcm.write("context->dsg_sink_array =");
			_gcm.write(" (lide_ocl_dsg_actor_context_type**)");
			_gcm.write(" lide_ocl_util_malloc(");
			_gcm.write("context->fifo_count");
			_gcm.write(" *sizeof(");
			_gcm.write("lide_ocl_dsg_actor_context_type *));");
	
			_gcm.next();

			_gcm.writeLine("  ");
			
						
			_gcm.nextLine();
			
			_gcm.write("context->dsg_type_array =");
			_gcm.write(" (int *)");
			_gcm.write(" lide_ocl_util_malloc(");
			_gcm.write("context->actor_count");
			_gcm.write(" *sizeof(");
			_gcm.write("int *));");
	
			_gcm.next();

			_gcm.writeLine("  ");
			
			_gcm.nextLine();
			
			_gcm.writeLine("token_size = sizeof(int);");

        	_gcm.nextLine();

			_gcm.writeLine("for(i = 0; i<context->fifo_count; i++){");
			_gcm.incIndent();
			_gcm.write("context->fifos[i] = (lide_ocl_fifo_type *)");
			_gcm.write("lide_ocl_fifo_unit_size_new(token_size,i);");
			_gcm.next();
			_gcm.write("}");
			_gcm.next();
			_gcm.decIndent();
        	_gcm.nextLine();

			_gcm.writeLine("i = 0;");
        	_gcm.nextLine();
        	

			for(Map.Entry<Node, LWDFActor> entry : _dsgContextMap.entrySet()){
				boolean post=false;
				boolean pre=false;
				LWDFActor actor = entry.getValue();
	        	String fileName= actor.getFileName();
	        	ArrayList<Port> inputPorts = actor.getPorts(1); 
	        	ArrayList<Port> outputPorts = actor.getPorts(0); 
	        	ArrayList<String> inputPortNames = new ArrayList<String>();
	        	ArrayList<String> outputPortNames = new ArrayList<String>();
				
	        	for (Port inPort : inputPorts){
	        		Edge inEdge = inPort.edge; 
	        		
	        		inputPortNames.add(_dsgGraph.getName(inEdge)); 
	        	}
	        	
	        	for (Port outPort : outputPorts){
	        		Edge outEdge = outPort.edge;
		        		        		
	        		outputPortNames.add(_dsgGraph.getName(outEdge)); 
	        	}
	        	
	        	
				if(actor.getName().equals("ref_actor")){
					 int indexhld1 = 0;
					_gcm.write("context->actors");
					_gcm.write("[DSG_ACTOR_");
					_gcm.write(entry.getValue().getInstanceName().toUpperCase()+"]");
					_gcm.write("= (lide_ocl_actor_context_type *)");
					_gcm.write("(lide_ocl_"+entry.getValue().getName()+"_new(");
						
					if(!entry.getValue().getFileName().equals("no_name")){
						_gcm.write("context->");
						_gcm.write(entry.getValue().getInstanceName()+"_file,");
					}

				
					if (inputPortNames.size()>0){
						for (String name : inputPortNames){
							indexhld1+=1;
							if (indexhld1<inputPortNames.size())
								_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
							else
								if(outputPortNames.size()>0)
									_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
								else
									_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
						}	
					}
					else
						_gcm.write("NULL,");
	        	
					indexhld1=0;
					if(outputPortNames.size()>0){
						for (String name : outputPortNames){
							indexhld1+=1;
							if (indexhld1<outputPortNames.size())
								_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
							else
								_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
						}
					}
					else
						_gcm.write("NULL,");
	        	
					_gcm.write("DSG_ACTOR_"+entry.getValue().getInstanceName().toUpperCase()+",");
					_gcm.write("contextApp->actors[ACTOR_"+actor.getRefName().toUpperCase()+"],");
					_gcm.write("0," + "0,");
					
					if(!actor.getPostRef().equals("NULL") & !actor.getPreRef().equals("NULL") ){
					
						_gcm.write("&"+actor.getPreRef()+","); // Insert the Pre ra Parameter here
						_gcm.write("&"+actor.getPostRef()+",");
						post=true;
						pre=true;
					}
					
					else if(!actor.getPostRef().equals("NULL") & actor.getPreRef().equals("NULL")) {
					
						_gcm.write(actor.getPreRef()+","); // Insert the Pre ra Parameter here
						_gcm.write("&"+actor.getPostRef()+",");
						post=true;
					
					}
					
					else if(actor.getPostRef().equals("NULL") & !actor.getPreRef().equals("NULL")){
						_gcm.write("&"+actor.getPreRef()+","); // Insert the Pre ra Parameter here
						_gcm.write(actor.getPostRef()+",");
						pre=true;

						
						
						
					}
					
					else{
						_gcm.write(actor.getPreRef()+","); // Insert the Pre ra Parameter here
						_gcm.write(actor.getPostRef()+",");
						
					}
					// Insert the Post ra parameter here
					_gcm.write("&"+actor.getRaExe()+"));");
	    	    
					_gcm.next();
					_gcm.nextLine();
				}
				
				else if(actor.getName().equals("sca_dynamic_loop")){
					 int indexhld1 = 0;
					_gcm.write("context->actors");
					_gcm.write("[DSG_ACTOR_");
					_gcm.write(entry.getValue().getInstanceName().toUpperCase()+"]");
					_gcm.write("= (lide_ocl_actor_context_type *)");
					_gcm.write("(lide_ocl_"+entry.getValue().getName()+"_new(");
						
					if(!entry.getValue().getFileName().equals("no_name")){
						_gcm.write("context->");
						_gcm.write(entry.getValue().getInstanceName()+"_file,");
					}

				
					if (inputPortNames.size()>0){
						for (String name : inputPortNames){
							indexhld1+=1;
							if (indexhld1<inputPortNames.size())
								_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
							else
								if(outputPortNames.size()>0)
									_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
								else
									_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
						}	
					}
					else
						_gcm.write("NULL,");
	        	
					indexhld1=0;
					if(outputPortNames.size()>0){
						for (String name : outputPortNames){
							indexhld1+=1;
							if (indexhld1<outputPortNames.size())
								_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
							else
								_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
						}
					}
					else
						_gcm.write("NULL,");
	        	
					_gcm.write("DSG_ACTOR_"+entry.getValue().getInstanceName().toUpperCase());
					_gcm.write("));");
	    	    
					_gcm.next();
					_gcm.nextLine();
				}
				
				
				
				else if(actor.getName().equals("sca_static_loop")){
					 int indexhld1 = 0;
					_gcm.write("context->actors");
					_gcm.write("[DSG_ACTOR_");
					_gcm.write(entry.getValue().getInstanceName().toUpperCase()+"]");
					_gcm.write("= (lide_ocl_actor_context_type *)");
					_gcm.write("(lide_ocl_"+entry.getValue().getName()+"_new(");
						
					if(!entry.getValue().getFileName().equals("no_name")){
						_gcm.write("context->");
						_gcm.write(entry.getValue().getInstanceName()+"_file,");
					}

				
					if (inputPortNames.size()>0){
						for (String name : inputPortNames){
							indexhld1+=1;
							if (indexhld1<inputPortNames.size())
								_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
							else
								if(outputPortNames.size()>0)
									_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
								else
									_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
						}	
					}
					else
						_gcm.write("NULL,");
	        	
					indexhld1=0;
					if(outputPortNames.size()>0){
						for (String name : outputPortNames){
							indexhld1+=1;
							if (indexhld1<outputPortNames.size())
								_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
							else
								_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
						}
					}
					else
						_gcm.write("NULL,");
	        	
					_gcm.write(actor.getIterNum()+ ",DSG_ACTOR_"+ actor.getInstanceName().toUpperCase() +"));");
					_gcm.next();
					_gcm.nextLine();
				}
				
				else if(actor.getName().equals("sca_sch_loop")){
					_gcm.write("context->actors");
					_gcm.write("[DSG_ACTOR_");
					_gcm.write(entry.getValue().getInstanceName().toUpperCase()+"]");
					_gcm.write("= (lide_ocl_actor_context_type *)");
					_gcm.write("(lide_ocl_"+entry.getValue().getName()+"_new(");
					
					int indexhld1=0;
					
					if(!entry.getValue().getFileName().equals("no_name")){
						_gcm.write("context->");
						_gcm.write(entry.getValue().getInstanceName()+"_file,");
					}

			
					if (inputPortNames.size()>0){
						for (String name : inputPortNames){
							indexhld1+=1;
							if (indexhld1<inputPortNames.size())
								_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
							else
								if(outputPortNames.size()>0)
									_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
								else
									_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
					}	
					}
					else
						_gcm.write("NULL,");
        	
					indexhld1=0;
					if(outputPortNames.size()>0){
						for (String name : outputPortNames){
							indexhld1+=1;
							if (indexhld1<outputPortNames.size())
								_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
							else
								_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
						}
					}
					else
						_gcm.write("NULL,");

					_gcm.write(actor.getIterNum()+ ",DSG_ACTOR_"+ actor.getInstanceName().toUpperCase() +"));");	
				}
        	
				else if(actor.getName().equals("sca_if")){
					 ifficount+=1;
					 _gcm.write("lide_ocl_fifo_unit_size_pointer*");
					 _gcm.write("fifo"+ifficount+" = ");
					 _gcm.write("(lide_ocl_fifo_unit_size_pointer *)");
					 _gcm.write("lide_ocl_util_malloc(");
					 _gcm.write(outputPortNames.size()+"* sizeof(lide_ocl_fifo_unit_size_pointer));");
					 _gcm.next();
					 
					 int indexhld1 = 0;
					 int fifoindex=0;
					 
					 if(outputPortNames.size()>0){
							for (String name : outputPortNames){
								indexhld1+=1;
									_gcm.write("fifo"+ifficount+"["+fifoindex+"]=");
									_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"];");
									_gcm.next();
									fifoindex+=1;	
							}
						}
					 
					_gcm.write("context->actors");
					_gcm.write("[DSG_ACTOR_");
					_gcm.write(entry.getValue().getInstanceName().toUpperCase()+"]");
					_gcm.write("= (lide_ocl_actor_context_type *)");
					_gcm.write("(lide_ocl_"+entry.getValue().getName()+"_new(");
						
					if(!entry.getValue().getFileName().equals("no_name")){
						_gcm.write("context->");
						_gcm.write(entry.getValue().getInstanceName()+"_file,");
					}

				
					if (inputPortNames.size()>0){
						for (String name : inputPortNames){
							indexhld1+=1;
							if (indexhld1<inputPortNames.size())
								_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
							else
								if(outputPortNames.size()>0)
									_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
								else
									_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
						}	
					}
		
					_gcm.write("fifo"+ifficount+",");
					_gcm.write(""+outputPortNames.size());
					_gcm.write(",DSG_ACTOR_"+ actor.getInstanceName().toUpperCase());
					_gcm.write("));");
	    	    
					_gcm.next();
					_gcm.nextLine();
				}
				
				else if(actor.getName().equals("sca_fi")){
					 ifficount+=1;
					 
					 _gcm.write("lide_ocl_fifo_unit_size_pointer*");
					 _gcm.write("fifo"+ifficount+" = ");
					 _gcm.write("(lide_ocl_fifo_unit_size_pointer *)");
					 _gcm.write("lide_ocl_util_malloc(");
					 _gcm.write(inputPortNames.size()+"* sizeof(lide_ocl_fifo_unit_size_pointer));");
					 _gcm.next();
					 
					 int indexhld1 = 0;
					 int fifoindex=0;
					 
					 if(inputPortNames.size()>0){
							for (String name : inputPortNames){
								indexhld1+=1;
									_gcm.write("fifo"+ifficount+"["+fifoindex+"]=");
									_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"];");
									_gcm.next();
									fifoindex+=1;	
							}
						}
					 
					_gcm.write("context->actors");
					_gcm.write("[DSG_ACTOR_");
					_gcm.write(entry.getValue().getInstanceName().toUpperCase()+"]");
					_gcm.write("= (lide_ocl_actor_context_type *)");
					_gcm.write("(lide_ocl_"+entry.getValue().getName()+"_new(");
		
					

					_gcm.write("fifo"+ifficount+",");
				
					if (outputPortNames.size()>0){
						for (String name : outputPortNames){
							indexhld1+=1;
							if (indexhld1<outputPortNames.size())
								_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
							else
								if(inputPortNames.size()>0)
									_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
								else
									_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
						}	
					}
		
					_gcm.write(""+inputPortNames.size());
					_gcm.write(",DSG_ACTOR_"+ actor.getInstanceName().toUpperCase());
					_gcm.write("));");
	    	    
					_gcm.next();
					_gcm.nextLine();
				}
				
					
				
				else {
					_gcm.write("context->actors");
					_gcm.write("[DSG_ACTOR_");
					_gcm.write(entry.getValue().getInstanceName().toUpperCase()+"]");
					_gcm.write("= (lide_ocl_actor_context_type *)");
					_gcm.write("(lide_ocl_"+entry.getValue().getName()+"_new(");
					
					int indexhld1=0;
					
					if(!entry.getValue().getFileName().equals("no_name")){
						_gcm.write("context->");
						_gcm.write(entry.getValue().getInstanceName()+"_file,");
					}

			
					if (inputPortNames.size()>0){
						for (String name : inputPortNames){
							indexhld1+=1;
							if (indexhld1<inputPortNames.size())
								_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
							else
								if(outputPortNames.size()>0)
									_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
								else
									_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
					}	
					}
					else
						_gcm.write("NULL,");
        	
					indexhld1=0;
					if(outputPortNames.size()>0){
						for (String name : outputPortNames){
							indexhld1+=1;
							if (indexhld1<outputPortNames.size())
								_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
							else
								_gcm.write("(lide_ocl_fifo_unit_size_pointer)context->fifos[DSG_FIFO_"+name.toUpperCase()+"],");
						}
					}
					else
						_gcm.write("NULL,");

					_gcm.write(",DSG_ACTOR_"+ actor.getInstanceName().toUpperCase() +"));");	
				}
				
				_gcm.next();
				
				if (post==true){
					Integer index=0;  
					_gcm.write("lide_ocl_ref_actor_set_selected_output_count(");
					_gcm.write("(lide_ocl_ref_actor_context_type *)");
					_gcm.write("context->");
					_gcm.write("actors[");
					_gcm.write("DSG_ACTOR_"+actor.getInstanceName().toUpperCase()+"],");
					_gcm.write(holder.get(actor.getRefName()).get(1).toString());
					_gcm.write(");");
					_gcm.next();
					_gcm.nextLine();
					
					
					for(Map.Entry<Node, LWDFActor> entry1 :_contextMap.entrySet()){
						
						LWDFActor actor1 = entry1.getValue();
			        	ArrayList<Port> outputPorts1 = actor1.getPorts(0); 
			        	ArrayList<String> outputPortNames1 = new ArrayList<String>();
			        	
			        	for (Port outPort : outputPorts1){
			        		Edge outEdge = outPort.edge;
				        		        		
			        		outputPortNames1.add(_graph.getName(outEdge));
			        	}
			        	if (actor1.getInstanceName().equals(actor.getRefName())){
			        	for(String each : outputPortNames1){
							_gcm.write("lide_ocl_ref_actor_set_selected_app_gr_outputs(");
							_gcm.write("(lide_ocl_ref_actor_context_type *)");
							_gcm.write("context->");
							_gcm.write("actors[");
							_gcm.write("DSG_ACTOR_"+actor.getInstanceName().toUpperCase()+"],");
							_gcm.write("contextApp->fifos[FIFO_"+each.toUpperCase()+"],");
							_gcm.write(index.toString());
							_gcm.write(");");
							index+=1;
							_gcm.next();
							}
			        	break;
			        	}
			        	
					}
					
					
					
				}
				
				if (pre==true){
					Integer index=0;  
					_gcm.write("lide_ocl_ref_actor_set_selected_input_count(");
					_gcm.write("(lide_ocl_ref_actor_context_type *)");
					_gcm.write("context->");
					_gcm.write("actors[");
					_gcm.write("DSG_ACTOR_"+actor.getInstanceName().toUpperCase()+"],");
					_gcm.write(holder.get(actor.getRefName()).get(0).toString());
					_gcm.write(");");
					_gcm.next();
					_gcm.nextLine();
					
					
					for(Map.Entry<Node, LWDFActor> entry1 :_contextMap.entrySet()){
						
						LWDFActor actor1 = entry1.getValue();
			        	ArrayList<Port> inputPorts1 = actor1.getPorts(1); 
			        	ArrayList<String> inputPortNames1 = new ArrayList<String>();
			        	
			        	for (Port inPort : inputPorts1){
			        		Edge inEdge = inPort.edge;
				        		        		
			        		inputPortNames1.add(_graph.getName(inEdge));
			        	}
			        	if (actor1.getInstanceName().equals(actor.getRefName())){
			        	for(String each : inputPortNames1){
							_gcm.write("lide_ocl_ref_actor_set_selected_app_gr_inputs(");
							_gcm.write("(lide_ocl_ref_actor_context_type *)");
							_gcm.write("context->");
							_gcm.write("actors[");
							_gcm.write("DSG_ACTOR_"+actor.getInstanceName().toUpperCase()+"],");
							_gcm.write("contextApp->fifos[FIFO_"+each.toUpperCase()+"],");
							_gcm.write(index.toString());
							_gcm.write(");");
							index+=1;
							_gcm.next();
							}
			        	break;
			        	}
			        	
					}
					
					
					
				}
				

				_gcm.nextLine();

			
				
				
			}
			
			_gcm.nextLine();


			_gcm.writeLine("////////////////////Type Table////////////////");
			for(Map.Entry<Node, LWDFActor> entry : _dsgContextMap.entrySet()){
				int indexhld1=0;
				LWDFActor actor = entry.getValue();
	        	String typeName= actor.getName();
	        	
	        	if (typeName.equals("sca_dynamic_loop")){
	        		_gcm.write("context->dsg_type_array[DSG_ACTOR_"+actor.getInstanceName().toUpperCase()+"]=");
					_gcm.write("DSG_SCA_DYN_LOOP;");
	        		_gcm.next();
	        		
	        	}
	        	else if(typeName.equals("sca_static_loop")){
	        		
	        		_gcm.write("context->dsg_type_array[DSG_ACTOR_"+actor.getInstanceName().toUpperCase()+"]=");
					_gcm.write("DSG_SCA_STATIC_LOOP;");
	        		_gcm.next();
	        		
	        	}
	        	else if (typeName.equals("ref_actor")){

	        		_gcm.write("context->dsg_type_array[DSG_ACTOR_"+actor.getInstanceName().toUpperCase()+"]=");
					_gcm.write("DSG_SCA_REF;");
	        		_gcm.next();
	        	}
	        	else{
	        		_gcm.write("context->dsg_type_array[DSG_ACTOR_"+actor.getInstanceName().toUpperCase()+"]=");
					_gcm.write("DSG_OTHER;");
	        		_gcm.next();
	        		
	        	}
	        	
			}
			_gcm.writeLine("////////////////////////////////////////////////");

			_gcm.writeLine("  ");
			
			
			
			_gcm.writeLine("////////////////////Sink Table////////////////");
			for(Map.Entry<Node, LWDFActor> entry : _dsgContextMap.entrySet()){
				int indexhld1=0;
				LWDFActor actor = entry.getValue();
	        	String fileName= actor.getFileName();
	        	ArrayList<Port> inputPorts = actor.getPorts(1); 
	        	ArrayList<Port> outputPorts = actor.getPorts(0); 
	        	ArrayList<String> inputPortNames = new ArrayList<String>();
	        	ArrayList<String> outputPortNames = new ArrayList<String>();
				
	        	for (Port inPort : inputPorts){
	        		Edge inEdge = inPort.edge; 
	        		
	        		inputPortNames.add(_dsgGraph.getName(inEdge)); 
	        	}
	        	
	        	for (Port outPort : outputPorts){
	        		Edge outEdge = outPort.edge;
		        		        		
	        		outputPortNames.add(_dsgGraph.getName(outEdge)); 
	        	}
	        	
	        	for (String name : inputPortNames){
						indexhld1+=1;
								_gcm.write("context->dsg_sink_array[DSG_FIFO_"+name.toUpperCase()+"]=");
								_gcm.write("(lide_ocl_dsg_actor_context_type*)");
								_gcm.write("context->actors[DSG_ACTOR_"+actor.getInstanceName().toUpperCase()+"];");
					        	_gcm.next();

				}	
					        	
			}
			_gcm.writeLine("////////////////////////////////////////////////");

			_gcm.writeLine("  ");
			_gcm.writeLine("////////////////////Source Table////////////////");
			for(Map.Entry<Node, LWDFActor> entry : _dsgContextMap.entrySet()){
				int indexhld1=0;
				LWDFActor actor = entry.getValue();
	        	String fileName= actor.getFileName();
	        	ArrayList<Port> inputPorts = actor.getPorts(1); 
	        	ArrayList<Port> outputPorts = actor.getPorts(0); 
	        	ArrayList<String> inputPortNames = new ArrayList<String>();
	        	ArrayList<String> outputPortNames = new ArrayList<String>();
				
	        	for (Port inPort : inputPorts){
	        		Edge inEdge = inPort.edge; 
	        		
	        		inputPortNames.add(_dsgGraph.getName(inEdge)); 
	        	}
	        	
	        	for (Port outPort : outputPorts){
	        		Edge outEdge = outPort.edge;
		        		        		
	        		outputPortNames.add(_dsgGraph.getName(outEdge)); 
	        	}
	        	
	        	for (String name : outputPortNames){
						indexhld1+=1;
								_gcm.write("context->dsg_source_array[DSG_FIFO_"+name.toUpperCase()+"]=");
								_gcm.write("(lide_ocl_dsg_actor_context_type*)");
								_gcm.write("context->actors[DSG_ACTOR_"+actor.getInstanceName().toUpperCase()+"];");
								_gcm.next();
				}	
					        	
	        	
	        	
			}
			_gcm.writeLine("//////////////////////////////////////////////");
			
			_gcm.writeLine("return context;");		
			_gcm.decIndent();
			_gcm.write("}");
}

	private void writeHeaders() {
		_gcm.writeLine("#include <stdio.h>");
		_gcm.writeLine("#include <stdlib.h>");
		
		_gcm.writeLine("#include <string.h>");
		_gcm.writeLine("#include \"lide_ocl_basic.h\"");
		_gcm.writeLine("#include \"lide_ocl_actor.h\"");
		_gcm.writeLine("#include \""+_dsgGraphHeaderFileName+"\"");
		_gcm.nextLine();
		_gcm.writeLine("#define NAME_LENGTH 20");		
		
		_gcm.nextLine();	
		_gcm.flush1();
	}		
			
		/*
		_gcm.writeLine("#include \"define.h\"");
		
		//---------------------------------------------------
		
		_gcm.write("void lide_ocl_dsg_scheduler");
		_gcm.write("(");
		_gcm.write("lide_ocl_actor_context_type");
		_gcm.write("*actors[]");
        _gcm.write(",int ");
        _gcm.write("actor_count");
        _gcm.write(",char *descriptors[]){");
        _gcm.next();
        _gcm.incIndent(); 


    	String fifoDec= "lide_ocl_dsg_fifo_pointer"+ " ";
		_gcm.write(fifoDec);
		int i=0;
		
		for (Map.Entry<Edge, LWDFEdge> entry : dsgFifoMap.entrySet()){
			i+=1;
			if (i<dsgFifoMap.size()){
				fifoDec=entry.getValue().getName()+" = NULL, ";
				_gcm.write(fifoDec);
			}
			else{
				fifoDec=entry.getValue().getName()+" = NULL; ";
				_gcm.write(fifoDec);
			}
				
		}
		_gcm.next();
		for (Map.Entry<Edge, LWDFEdge> entry : dsgFifoMap.entrySet()){
	        //_gcm.writeLine(entry.getValue().getName()+" = lide_ocl_fifo_new(BUFFER_CAPACITY, token_size);");
			_gcm.nextLine();
			_gcm.writeLine("token_size = sizeof("+ entry.getValue().getEdgeType()+");");
			_gcm.write(entry.getValue().getName());
			_gcm.write(" = lide_ocl_dsg_fifo_new(BUFFER_CAPACITY, token_size);");
			_gcm.next();
		}
		_gcm.nextLine();
		
		int count=0;
		_gcm.write("const char *dsgDescriptors[ACTOR_COUNT] = {");
		for (Map.Entry<Node, LWDFActor> entry : dsgContextMap.entrySet()){
			count+=1;
			LWDFActor actor1 = entry.getValue();
			if (count==dsgContextMap.entrySet().size())
				_gcm.write("\""+actor1.getInstanceName()+"\"");
			else
				_gcm.write("\""+actor1.getInstanceName()+"\""+",");
			
				
		}
		
		_gcm.write("};");
		
		_gcm.next();
		
		_gcm.nextLine();
		_gcm.writeLine("i = 0;");
		_gcm.nextLine();
		
		for (Map.Entry<Node, LWDFActor> entry : dsgContextMap.entrySet()){
        	
        	LWDFActor actor = entry.getValue();
        	String refName= actor.getRefName();
        	ArrayList<Port> inputPorts = actor.getPorts(1); 
        	ArrayList<Port> outputPorts = actor.getPorts(0); 
        	ArrayList<String> inputPortNames = new ArrayList<String>();
        	ArrayList<String> outputPortNames = new ArrayList<String>();
        	
        	for (Port inPort : inputPorts){
        		Edge inEdge = inPort.edge; 
        		
        		inputPortNames.add(dsggraph.getName(inEdge)); 
        	}
        	
        	for (Port outPort : outputPorts){
        		Edge outEdge = outPort.edge;
	        		        		
        		outputPortNames.add(dsggraph.getName(outEdge)); 
        	}
        	
        	int indexhld=0;
        	
        	_gcm.write("dsgActors[ACTOR_");
        	_gcm.write(actor.getInstanceName().toUpperCase());
        	_gcm.write("] = (lide_ocl_actor_context_type *)");
        	_gcm.write("(lide_ocl_"+actor.getName()+"_new(");
        	
        	if(actor.getName().equals("ref_actor")){
        		if(!refName.equals("no_name")){
        			_gcm.write("actors[");
        			_gcm.write("ACTOR_");
        			_gcm.write(refName.toUpperCase()+"],");
        			
        		}
        	}
        			
        		if (inputPortNames.size()>0){
        			for (String name : inputPortNames){
        				indexhld+=1;
        				if (indexhld<inputPortNames.size())
        					_gcm.write(name+",");
        				else
        					if(outputPortNames.size()>0)
        						_gcm.write(name+",");
        					else
        						_gcm.write(name);
        			}
        		}
        		else {
        			_gcm.write("NULL,");
        		}
        
        	indexhld=0;
        	if(outputPortNames.size()>0){
        		for (String name : outputPortNames){
        			indexhld+=1;
        			if (indexhld<outputPortNames.size())
        				_gcm.write(name+",");
        			else
        				_gcm.write(name);
        		}
        	}
        	else{
        		
        		_gcm.write(",NULL");
        		
        		
        	}*/
        		
        /*	if (actor.isCUDAEnabled()){
        		_gcm.write(",gpu,");
        		_gcm.write("1");
        		_gcm.write("));");
		}
        		else
*/        	
        	/*_gcm.write("));");
       	
        	_gcm.next();
        	_gcm.nextLine();
        	
        	
        	
			
		
		
		
		
		
        
		//---------------------------------------------------
		
		}
		_gcm.write("lide_ocl_util_simple_scheduler");
		_gcm.write("(dsgActors,");
		_gcm.write("DSG_ACTOR_COUNT,");
		_gcm.write("(char **)dsgDescriptors);");
		_gcm.next();
		_gcm.nextLine();


		for (Map.Entry<Edge, LWDFEdge> entry1 : dsgFifoMap.entrySet()){
		       _gcm.writeLine("lide_ocl_dsg_fifo_free("+entry1.getValue().getName()+");");
		       
				}
			
			_gcm.nextLine();

			for (Map.Entry<Node, LWDFActor> entry1 : dsgContext.entrySet()){
	        	LWDFActor actor1 = entry1.getValue();
	        	String indexName = "lide_ocl_"+actor1.getName()+"_terminate((lide_ocl_file_source_context_type *)(actors[ACTOR_"+actor1.getInstanceName().toUpperCase()+"]));";
				_gcm.write("lide_ocl_");
				_gcm.write(actor1.getName());
				_gcm.write("_terminate((lide_ocl_"+actor1.getName()+"_context_type *)");
				_gcm.write("(actors[ACTOR_");
				_gcm.write(actor1.getInstanceName().toUpperCase()+"]));");
				_gcm.next();
			}
			
			_gcm.nextLine();
		*/
	
	
	
	
	
    private LinkedList<String> _headers;  
    private PrintWriter _sourcePrinter; 
    
//    private final int _nThreads; 
    //private final int _repetitions; // number of repetitions 
    private final SDFGraph _dsgGraph; 
    private final SDFGraph _graph; 
	private final String _dsgGraphHeaderFileName; 
	private final String _dsgGraphName; 
	private final String _graphName;
	private final HashMap<Node, LWDFActor> _contextMap; 
	private final HashMap<Edge, LWDFEdge> _fifoMap; 
	private final HashMap<Node, LWDFActor> _dsgContextMap; 
	private final HashMap<Edge, LWDFEdge> _dsgFifoMap; 
	private  Gcm _gcm;

	private  boolean _isDSG;
}


