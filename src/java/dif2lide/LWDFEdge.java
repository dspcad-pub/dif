/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif2lide;

import dif.DIFAttribute;
import dif.csdf.sdf.SDFEdgeWeight;
import dif.csdf.sdf.SDFGraph;
import dif.util.graph.Edge;

public class LWDFEdge {
	public LWDFEdge(Edge e, SDFGraph graph, int id, String name) {
		_edge = e; 
		//_id = id; 
		_name = name; 
		initEdge(graph); 
	}
	
	private void initEdge(SDFGraph graph){
		// Set type
		DIFAttribute typeAttr = graph.getAttribute(_edge, "edgeType");
		_edgeType = (String) typeAttr.getValue();
		_capacity = graph.BMUB(_edge);
		_weight = (SDFEdgeWeight) _edge.getWeight();
	}
	
	// get functions
	public int getConsumptionRate(){
		return _weight.getSDFConsumptionRate();  
	}
	public int getProductionRate(){
		return _weight.getSDFProductionRate();  
	}
	public int getCapacity(){
		return _capacity; 
	}
	public Edge getEdge(){
		return _edge; 
	}
	public int getID(){
		return _id; 
	}
	public String getName(){
		return _name; 
	}
	// set functions
	public void setCapacity(int cap){
		_capacity = cap; 
	}
	public void setEdge(Edge e){
		_edge = e; 
	}
	public void setID(int id){
		_id = id; 
	}
	public void setName(String name){
		_name = name; 
	}
	public String getEdgeType() {
		return _edgeType;
	}
	public void setEdgeType(String edgeType) {
		_edgeType = edgeType;
	}

	private int _id; 
	private int _capacity; 
	private Edge _edge;
	private String _name;
	private String _edgeType;
	private SDFEdgeWeight _weight; 
	
}
