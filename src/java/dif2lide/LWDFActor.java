/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif2lide;

import dif.DIFAttribute;
import dif.csdf.sdf.SDFGraph;
import dif.util.graph.Edge;
import dif.util.graph.Node;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;

import java.util.*;

//TODO: for getting execution time, add something to support h2d and d2h actor execution 
// time queries. 

public class LWDFActor {
	/** Create an LWDFActor context
	 * @param node Node in DIF graph
	 * @param graph Graph
	 * @param type 0: computation node. 1 h2d/d2h node
	 * @param stringParam: if type == 0, then stringParam indicates the profile directory. 
	 *   if type == 1 or 2, then stringParam indicates the type of token of memcpy actor. 
	 */
    public LWDFActor(Node node, SDFGraph graph, int id/*, int type, String stringParam*/) {
    	// Set names
        _instanceName = graph.getName(node); 
        _actorName = getNodeName(node, graph);
        _iterNum = getIterNum(node, graph);
        _fileName=getFileName(node,graph);
        _refName=getRefName(node,graph);
        _preRef=getPreRef(node,graph);
        _postRef=getPostRef(node,graph);
        _raExe=getRaExe(node,graph);
        _hasGPU = isCUDAEnabled(node, graph); 
        _node = node;
        //_type = type;   //_type = 0: computation
        _id = id; 
        // load connections
        initPortConnections(graph); 
        // load parameters 
    	_parameterNames = new ArrayList<String>(); 
    	_parameterTypes = new ArrayList<String>(); 
    	//if (_type == 0){
    	getParameters(node, graph, _parameterNames, _parameterTypes);
    	//}
    	
    	//if (_type == 1 || _type == 2){
       // 	_tokenSizeUnit = "sizeof(" + stringParam + ")"; 
        //}*/
        
        // load cpu profiles
    	/*
        _cpuVecDegrees = new ArrayList<Double>(); 
        _cpuTimes = new ArrayList<Double>(); 
        _gpuVecDegrees = new ArrayList<Double>(); 
        _gpuTimes = new ArrayList<Double>();         
        if (_type == 0){
        	String directory = stringParam; 
        	LinearInterpolator interpolator = new LinearInterpolator(); 
        	String profile_name = directory + _lide_cuda_prefix + _actorName + "_cpu.csv"; 
        	Utilities.readPairValues(profile_name, _cpuVecDegrees, _cpuTimes); 
        	// Convert LinkedList<Double> to double[]; 
        	double vectorizations[] = new double[this._cpuVecDegrees.size()];
        	double times[] = new double[this._cpuTimes.size()];
        	for (int i = 0; i < this._cpuTimes.size(); i++){
        		vectorizations[i] = _cpuVecDegrees.get(i); 
        		times[i] = _cpuTimes.get(i); 
        	}
        	_func_cpu = interpolator.interpolate(vectorizations, times);
        	if (this._hasGPU) {
				profile_name = directory + _lide_cuda_prefix + _actorName + "_gpu.csv";
				Utilities.readPairValues(profile_name, _gpuVecDegrees, _gpuTimes);
        		vectorizations = new double[this._gpuVecDegrees.size()];
        		times = new double[this._gpuTimes.size()];
				for (int i = 0; i < this._gpuTimes.size(); i++){
	        		vectorizations[i] = _gpuVecDegrees.get(i); 
					times[i] = _gpuTimes.get(i);
				}
				_func_gpu = interpolator.interpolate(vectorizations, times);
        	}
        } */
    }
    
    /*
    // Construct an actor without profiles or parameters
    public LWDFActor(Node node, SDFGraph graph, int id, int type){
        //public LWDFActor(Node node, SDFGraph graph, int type, int id){
    	 _instanceName = graph.getName(node); 
         _actorName = getNodeName(node, graph); 
         _node = node;
         _type = type;   //_type = 0: computation, 1: h2d, 2:d2h
         _id = id;
         _hasGPU = isCUDAEnabled(node, graph); 
         initPortConnections(graph); 
         _parameterNames = new ArrayList<String>(); 
     	 _parameterTypes = new ArrayList<String>(); 
     	 if (type != 0){ // not a 
     		 
     	 }
    }
    */
    
	/**
	 * Get the execution time of a processor, 0 for CPU, 1 for GPU. If time does not exist
	 * return a negative value (-1).  
	 * @param vecDegree
	 * @return
	 */
	public double getExecutionTime(int vecDegree, int processorType){
		if (processorType == 0) {
			return _func_cpu.value((double)vecDegree);
		} else if (processorType == 1 && this._hasGPU){
			return _func_gpu.value((double)vecDegree);
		} else {
			return _maxActorRuntime; 
		}
	}
	
    /********************* Set and get ******************/
    public boolean isCUDAEnabled() {
        return _hasGPU; 
    }
    /* ID: the ID of the LWDF actor; also the index to refer to in actor array. */
    public void setID(int ID){
    	_id = ID; 
    }
    public int getID(){
    	return _id; 
    }
    public Node getNode() {
		return _node;
	}
	public void setNode(Node _node) {
		this._node = _node;
	}
    /* name: the name of LWDF actor that appears in output code. The macro is defined as 
     * #define LWDF_ACTOR_NAME [index]. 
     */
    public void setName(String name){
    	_actorName = name; 
    }
    public String getName(){
    	return _actorName; 
    }
    
    public String getInstanceName(){
    	return _instanceName; 
    }
    public void setInstanceName(String name){
    	_instanceName = name; 
    }
    
    public String getFileName(){
    	return _fileName; 
    }
    
    public String setFileName(String name){
    	return _fileName=name; 
    }
    
    public String getRefName(){
    	return _refName; 
    }
    
    public String setPreRef(String name){
    	return _preRef=name; 
    }
    
    public String getPreRef(){
    	return _preRef; 
    }
    
    public String setPostRef(String name){
    	return _postRef=name; 
    }
    
    public String getPostRef(){
    	return _postRef; 
    }
    
    public String getRaExe(){
    	return _raExe; 
    }
    
    public String getIterNum(){
    	return _iterNum; 
    }
    
    public String setRefName(String name){
    	return _refName=name; 
    }
    
	public int getType() {
		return _type;
	}
	public void setType(int _type) {
		this._type = _type;
	}
	public String getTokenSizeUnit(){
		return _tokenSizeUnit; 
	}
	public void setTokenSizeUnit(String tokenSizeUnit){
		_tokenSizeUnit = tokenSizeUnit; 
	}
	public ArrayList<String> getParameterNames(){
		ArrayList<String> params = (ArrayList<String>) _parameterNames.clone();
		return params; 
	}
	public ArrayList<String> getParameterTypes(){
		ArrayList<String> params = (ArrayList<String>) _parameterTypes.clone();
		return params; 
	}
	// get input or output ports, input first
	public ArrayList<Port> getPorts(int direction){
		ArrayList<Port> ports = new ArrayList<Port>();
		for (Port p : _ports){
			if (direction == p.direction) { // input ports
				ports.add(p); 
			}
		}
		return ports; 
	}
	public ArrayList<Port> getPorts(){
		ArrayList<Port> ports = new ArrayList<Port>();
		for (Port p : _ports){
			ports.add(p); 
		}
		return ports; 
	}
	
	// Replace the port connection specified by oldEdge to newEdge
	public void replacePort(Edge oldEdge, Edge newEdge){
		int idx = 0;
		int i = 0; 
		int size = _ports.size(); 
		for (i = 0; i < size; i++){
			Port p = _ports.get(i); 
			if (p.edge.equals(oldEdge)){
				idx = i; 
				break; 
			}
		}
		if (i == size){
			GlobalSettings.debugMessage("LWDFActor Warning: port not found"); 
		}
		Port port = _ports.get(idx); 
		port.edge = newEdge; 
	}
	
	private void initPortConnections(SDFGraph graph){
		/* parse the port_x specifications into two lists, input and output ports 
		 * Create two maps for input ports (id --> edgeName), 
		 * then sort the keys */ 
		TreeMap<Integer, Edge> smap = new TreeMap<Integer, Edge>();
		HashMap<Integer, Integer> directionMap = new HashMap<Integer, Integer>(); 
		LinkedList<DIFAttribute> attrList = graph.getAttributes(_node); 
    	for (DIFAttribute attr : attrList) {
    		String attrName = attr.getName(); 
    		String attrType = attr.getType(); 
    		if ( (!isReservedWords(attrName)) && (attrType.equals("INPUT") || 
    				attrType.equals("OUTPUT"))) {
    			String[] strs = attrName.split("_");
    			if (strs.length < 2){
    				GlobalSettings.debugMessage("LWDFActor Wrong format for port specification, skip this port"); 
    				continue; 
    			}
				Integer index = Integer.parseInt(strs[strs.length-1]); 
				Edge e = (Edge) attr.getValue();
				smap.put(index, e); 
				// set directions
    			if (attrType.equals("OUTPUT")){
    				directionMap.put(index, 0); 
    			} else {
    				directionMap.put(index, 1); 
    			}
    		}
    	}
    	// initialize members, sorting port names by its numeric orders
    	_ports = new ArrayList<Port>();
    	for (Map.Entry<Integer, Edge> entry : smap.entrySet()) {
    		Integer index = entry.getKey();
    		Edge e = entry.getValue();
    		DIFAttribute typeAttr = graph.getAttribute(e, "edgeType"); 
            String typeName = (String) typeAttr.getValue();
    		Port p = new Port(index, e, directionMap.get(index), typeName); 
    		_ports.add(p); 
    	}
    }
	
	private boolean isCUDAEnabled(Node node, SDFGraph graph){
		// tell whether the actor is CUDA enabled
        LinkedList<DIFAttribute> attrList = graph.getAttributes(node); 
    	for (DIFAttribute attr : attrList) {
    		String attrName = attr.getName(); 
    		if (attrName.equalsIgnoreCase("CUDAEnabled")){
    			int hasCUDA = (int) attr.getValue(); 
    			if (hasCUDA > 0){
    				return true; 
    			}
    		}
    	}
    	return false; 
	}
	
	private String getNodeName(Node node, SDFGraph graph) {
        DIFAttribute attr = graph.getAttribute(node, "name");
        String nodeName = ""; 
        if (attr != null) {
        	nodeName += (String) attr.getValue();
        } else {
        	nodeName = "no_name"; 
        }
        return nodeName; 
    }
	
	private String getIterNum(Node node, SDFGraph graph) {
        DIFAttribute attr = graph.getAttribute(node, "iterNum");
        String nodeName = ""; 
        if (attr != null) {
        	nodeName += (String) attr.getValue();
        } else {
        	nodeName = "no_name"; 
        }
        return nodeName; 
    }
	
	private String getFileName(Node node, SDFGraph graph) {
        DIFAttribute attr = graph.getAttribute(node, "file");
        String nodeName = ""; 
        if (attr != null) {
        	nodeName += (String) attr.getValue();
        } else {
        	nodeName = "no_name"; 
        }
        return nodeName; 
    }
	
	private String getRefName(Node node, SDFGraph graph) {
        DIFAttribute attr = graph.getAttribute(node, "ref");
        String nodeName = ""; 
        if (attr != null) {
        	nodeName += (String) attr.getValue();
        } else {
        	nodeName = "no_name"; 
        }
        return nodeName; 
        
	}
	
	private String getPreRef(Node node, SDFGraph graph) {
        DIFAttribute attr = graph.getAttribute(node, "pre_ref");
        String nodeName = ""; 
        if (attr != null) {
        	nodeName += (String) attr.getValue();
        } else {
        	nodeName = "NULL"; 
        }
        return nodeName; 
        
	}
	
	private String getPostRef(Node node, SDFGraph graph) {
        DIFAttribute attr = graph.getAttribute(node, "post_ref");
        String nodeName = ""; 
        if (attr != null) {
        	nodeName += (String) attr.getValue();
        } else {
        	nodeName = "NULL"; 
        }
        return nodeName; 
        
	}
	
	private String getRaExe(Node node, SDFGraph graph) {
        DIFAttribute attr = graph.getAttribute(node, "ra_exe");
        String nodeName = ""; 
        if (attr != null) {
        	nodeName += (String) attr.getValue();
        } else {
        	nodeName = "lide_ocl_ra_exe_default"; 
        }
        return nodeName; 
        
	}
	
	
    private boolean isReservedWords(String attrName){
    	if ((attrName.equals("name")) || attrName.equals("CUDAEnabled") || attrName.equals("file") || attrName.equals("ref") || attrName.equals("pre_ref")|| attrName.equals("post_ref")||attrName.equals("iterNum")||attrName.equals("ra_exe")) {
    		return true; 
    	}
    	return false; 
    }
    
    private void getParameters(Node node, SDFGraph graph, 
    		ArrayList<String> paramNames, ArrayList<String> paramTypes){
    	paramNames.clear();
    	paramTypes.clear();
    	String nodeName = getNodeName(node, graph); 
    	LinkedList<DIFAttribute> attrList = graph.getAttributes(node); 
    	for (DIFAttribute attr : attrList) {
    		String attrName = attr.getName(); 
    		String attrType = attr.getType(); 
    		if (!isReservedWords(attrName) && attrType.equals("PARAMETER")) {
				paramNames.add(attrName);
				String paramType = attr.getValue().toString(); 
				paramTypes.add(paramType);
    		}
    	}
    }
    
    /********************* Printers ******************/
	public void printProfile() {
		for (int i = 0; i < _cpuVecDegrees.size(); i++){
			String line = "" + _cpuVecDegrees.get(i) + " " + _cpuTimes.get(i); 
			if (_hasGPU){
				line += " " + _gpuTimes.get(i); 
			}
			System.out.println(line); 
		}
	}
    /* Print info: print information about this LWDF actor */
    public void printContext(){
    	System.out.println("Name: " + _actorName);
    	System.out.println("Instance: " + _instanceName); 
    	System.out.println("ID: " + _id); 
    	String s = _hasGPU ? "yes" : "no"; 
    	System.out.println("CUDA: " + s);
    	System.out.println("Parameters:"); 
    	for (int i = 0; i < _parameterNames.size(); i++){
    		String str = _parameterNames.get(i) + ":" + _parameterTypes.get(i); 
    		System.out.println(str); 
    	}
    	System.out.println("Connections:"); 
    	for (int i = 0; i < _ports.size(); i++){
    		String str = _ports.get(i).edge.toString();  
    		System.out.println(str); 
    	}
    }
    
	/************************** Members *****************************/

    private String _actorName;   // name of the LWDF actor, but not the instance.  
    private String _fileName; 
    private String _refName; 
    private String _iterNum; 
    private String _preRef; 
    private String _postRef; 
    private String _raExe; 
    private String _instanceName; //
    private String _refId;
    private int _id;
    private boolean _hasGPU; 
    private Node _node; 
    private int _type; // 0: computation; 1: h2d_memcpy; 2: d2h_memcpy
    private ArrayList<String> _parameterNames;
    private ArrayList<String> _parameterTypes;
    private ArrayList<Port> _ports; 
    
    // memcpy actor parameters
    private String _tokenSizeUnit; 
    
    // actor profiles. No profile if it is not a computation node (_type != 0). 
	private ArrayList<Double> _cpuVecDegrees; 
	private ArrayList<Double> _cpuTimes; 
	private ArrayList<Double> _gpuVecDegrees; 
	private ArrayList<Double> _gpuTimes; 
	private PolynomialSplineFunction _func_cpu; 
	private PolynomialSplineFunction _func_gpu;  
    
	/************************** Members for code generation *****************************/

    private static String _lide_cuda_prefix = "lide_cuda_"; 
    private final static double _maxActorRuntime = 1000 * 3600 * 365; // maximum runtime of an actor: 1 year


}; 
