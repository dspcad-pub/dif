/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif2lide;

import java.util.ArrayList;

public class DIFtoLWDFConfig {
	public DIFtoLWDFConfig(){
		vecDegrees = new ArrayList<Integer>(); 
		numberOfThreads = 1; 
		numberOfGPUs = 0; 
		repetitions = 1; 
	}
	public void print(){
		System.out.println("DIF File: " + difFile); 
		System.out.println("Profile Directory: " + profileDirectory); 
		System.out.println("Result Directory: " + resultDirectory); 
		System.out.println("Scheduler: " + schedulerName); 
		System.out.println("H2D and D2H files:" + h2dProfile + "," + d2hProfile); 
		System.out.println("# of Threads: " + numberOfThreads); 
		System.out.println("# of GPUs: " + numberOfGPUs);
		String vecs = ""; 
		for (Integer v : vecDegrees){
			vecs += v + ","; 
		}
		vecs = vecs.substring(0, vecs.length()-1); 
		System.out.println(vecs); 
	}
	
	public String difFile; 
	public String dsgFile;
	public String preRef; 
	public String postRef;
	public String profileDirectory;
	public String resultDirectory; 
	public String schedulerName; 
	public String h2dProfile;
	public String d2hProfile; 
	public int numberOfThreads;
	public int numberOfGPUs;
	public int repetitions; 
	public ArrayList<Integer> vecDegrees;
}
