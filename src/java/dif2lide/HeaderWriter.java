/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif2lide;

import dif.util.graph.Edge;
import dif.util.graph.Node;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
//import mocgraph.Node;

public class HeaderWriter {
	public HeaderWriter(String headerFileName, String graphName,  
			HashMap<Node, LWDFActor> contextMap, HashMap<Edge, LWDFEdge> fifoMap, 
			int nThreads){
		try {
			_headerPrinter = new PrintWriter(new BufferedWriter(new FileWriter(headerFileName)));
		} catch(IOException e){
			System.out.println("Error: Cannot find " + headerFileName); 
			e.printStackTrace(); 
		}
		_diag = false; 
		_indentation = 0; 
		_headers = new LinkedList<String>(); 
	    _contextMap = contextMap;
		_fifoMap = fifoMap; 
		_nThreads = nThreads; 
		_graphName = graphName; 
		
		// put all headers into _header (buffer)
		initIncludeHeaders(); 
	}
	
	/********************* Top level functions ******************/ 
	/** Top-level write functions 
	 */
    public void run() {
    	GlobalSettings.debugMessage("HeaderWriter: Writing header file ..."); 
        writeIncludedHeaders(); 
        writeSourceMacros(); 
        writeClassBody(); 
        _headerPrinter.flush();  
        diagMessage("HeaderWriter: Done."); 
    }
    
    /******************** Private functions *******************/
    private void writeIncludedHeaders() {
        for (String header : _headers) {
            _headerPrinter.println("#include " + header); 
        }
        _headerPrinter.println(""); 
    } 

    private void writeClassBody() {
        diagMessage("Writing Class Definition ...");
        _headerPrinter.println("class " + _graphName + " { "); 
        _headerPrinter.println("public:"); 
        _indentation += 4; 
        writeMethodDeclarations();         
        _indentation -= 4;
        _headerPrinter.println("private:"); 
        _indentation += 4; 
        writeMembers(); 
        _headerPrinter.println("}; ");  
    }
    
    private void writeMethodDeclarations() {
        // pirint full constructor
        diagMessage("HeaderWriter: Writing full constructor declaration..."); 
        printCodeLine(constructorDeclaration()); 
        // print default destructor
        printCodeLine("~" + _graphName + "( );"); 
        // print an execute method
        printCodeLine("void execute(); ");
    }
    
    private void writeMembers() {
    	// Print a code line for thread list. 
    	printCodeLine("lide_cuda_thread_list* thread_list; "); 
    	// Print a list of actors and the descriptors.  
    	printCodeLine("lide_c_actor_context_type* actors[ACTOR_COUNT];"); 
    	printCodeLine("char *descriptors[ACTOR_COUNT];");
    	for (Map.Entry<Edge,LWDFEdge> entry : _fifoMap.entrySet()){
    		LWDFEdge fifo = entry.getValue();
    		printCodeLine(_LIDE_CUDA_PREFIX + "fifo_pointer " + fifo.getName() + ";"); 
    	}
    }
    
    // Generates the constructor declaration
    private String constructorDeclaration() {
        String constructor = _graphName + "(";
    	ArrayList<String> argList = new ArrayList<String>(); 
    	for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()){
    		LWDFActor actor = entry.getValue();
    		ArrayList<String> paramNames = actor.getParameterNames(); 
    		ArrayList<String> paramTypes = actor.getParameterTypes(); 
    		for (int i = 0; i < paramNames.size(); i++) {
        		argList.add(paramTypes.get(i) + " " + paramNames.get(i));   
        	}
    	}
    	constructor += Utilities.commaSeparatedString(argList); 
    	constructor += ");"; 
    	return constructor; 
    }
    
    
    /** 
     * Write the macro definitions for the source files. 
     * */ 
    private void writeSourceMacros() {
    	LinkedHashMap<String, Integer> macroDefinitions = new LinkedHashMap<String, Integer>(); 
    	generateSourceMacros(macroDefinitions); 
    	for (Map.Entry<String, Integer> entry : macroDefinitions.entrySet()){
    		printCodeLine("#define " + entry.getKey() + " " + entry.getValue().toString());  
    	}
    	printCodeLine(""); // an empty line
    }
    private void generateSourceMacros(LinkedHashMap<String, Integer> macroDefinitions){
    	// All actor indices
    	for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()){
    		String actorName = entry.getValue().getInstanceName().toUpperCase(); 
    		Integer id = entry.getValue().getID();
    		// add all actor library files. 
    		macroDefinitions.put(actorName, id); 
    	}
    	macroDefinitions.put("ACTOR_COUNT", _contextMap.size()); 
    	// CPU, GPU macro convention
    	macroDefinitions.put("CPU", 0);
    	macroDefinitions.put("GPU", 1);
    	// Multithread information
    	macroDefinitions.put("NUMBER_OF_THREADS", _nThreads); 
    }

    private void initIncludeHeaders() {
        diagMessage("HeaderWriter: Generating Header Section...");
        // Standard C++ libraries. 
        _headers.add("<iostream>"); 
        _headers.add("<cstdio>"); 
        _headers.add("<cmath>"); 
        _headers.add("<cstdlib>");
        _headers.add("<ctime>");

        // Basic LIDE-C / LIDE-CUDA library. 
        _headers.add("\"lide_c_actor.h\"");  
        /* Note: there is no lide_cuda_actor.h in the LWDF implementation at this time. 
         * LIDE_C has no termination function; 
         * I believe Chung-Ching forgot it when he built LIDE-C up. Modifying LIDE-C, however, 
         * would make all current LIDE-C uncompilable because all structure definition has 
         * an undefined "terminate" function pointer. 
         * Therefore I will just stick with the C actor as the base class: all lide-cuda actors
         * are derived, in C style, from lide_c_actor.h, and provide termination function on
         * their own */

        // Generate LIDE utility headers. 
        _headers.add("\"" + _LIDE_C_PREFIX + "fifo.h\""); // LIDE-C FIFO
        _headers.add("\"" + _LIDE_C_PREFIX + "util.h\""); // LIDE-C Utilities
        _headers.add("\"" + _LIDE_CUDA_PREFIX + "fifo.h\""); // LIDE-CUDA FIFO
        _headers.add("\"" + _LIDE_CUDA_PREFIX + "util.h\""); // LIDE-CUDA tilities
        _headers.add("\"" + _LIDE_CUDA_PREFIX + "thread_list.h\""); // Multithread
        _headers.add("\"" + _LIDE_CUDA_PREFIX + "memcpy.h\""); // Memcpy actor
        
        HashSet<String> actorNames = new HashSet<String>(); 
        for (Map.Entry<Node, LWDFActor> entry : _contextMap.entrySet()) {
        	LWDFActor actor = entry.getValue(); 
        	String name = actor.getName(); 
        	if (actorNames.contains(name)){
        		continue; 
        	}
        	if (actor.getType() == 0){
        		actorNames.add(name); 
        		_headers.add("\"" + _LIDE_CUDA_PREFIX + name + ".h\"");
        	}
        }

    }
    
    private void printCodeLine(String code) {
        // Give correct indentation
        String spaces = new String(new char[_indentation]).replace('\0',' '); 
        _headerPrinter.println(spaces + code);
    } 
    
	private void diagMessage(String s){
        if (_diag) {
            System.out.println(s);
        }
	}
	
    /**************** Members ****************/
    // writers 
    private int _indentation; 
    private LinkedList<String> _headers;  
    private PrintWriter _headerPrinter; 
	private boolean _diag;  // diagnostic mode
	
	// Information
	private final int _nThreads; 
	private final String _graphName; 
	private final HashMap<Node, LWDFActor> _contextMap; 
	private final HashMap<Edge, LWDFEdge> _fifoMap; 
	
	// Constants
    private static final String _LIDE_C_PREFIX = "lide_c_"; 
    private static final String _LIDE_CUDA_PREFIX = "lide_cuda_";
    private static final String _SCHEDULE_FILE_PREFIX = "thread_";
    private static final String _multithreadInitFunctionName = "lide_cuda_thread_list_init_2"; 
    private static final String _multithreadScheduleFunctionName = "lide_cuda_thread_list_scheduler_2";
    private static final String _multithreadTerminateFunctionName = "lide_cuda_thread_list_terminate";

}
