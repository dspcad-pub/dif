/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif2lide;
import java.util.ArrayList;

public class CombinationEnum {
	public CombinationEnum(ArrayList<ArrayList<Integer>> matrix) {
		_matrix = matrix; 
		_indices = new ArrayList<Integer>(); 
		_overflow = false; 
		for (int i = 0; i < matrix.size(); i++){
			_indices.add(0);
		}
	}
	public void reset(){
		_overflow = false; 
		for (int i = 0; i < _indices.size(); i++){
			_indices.set(i, 0); 
		}
	}
	public boolean hasNext(){ 
		if (_overflow) return false; 
		for (int i = 0; i < _indices.size(); i++){
			if (_indices.get(i) <= _matrix.get(i).size() - 1){
				return true;
			}
		}
		return false; 
	}
	
	public ArrayList<Integer> next() {
		ArrayList<Integer> combination = new ArrayList<Integer> ();
		if (!hasNext()) return combination; 
		for (int j = 0; j < _indices.size(); j++){
			combination.add(_matrix.get(j).get(_indices.get(j))); //matrix[j][indices[j]] 
		}
		int i = 0; 
		if (i < _matrix.size()){
    		// to next indices
    		if (_indices.get(i) + 1 < _matrix.get(i).size()){
    			_indices.set(i, _indices.get(i)+1); 
    		} else {
    			int p = i; 
    			while (i < _matrix.size() && _indices.get(i) + 1 >= _matrix.get(i).size()){
    				i++; 
    			}
    			if (i >= _matrix.size()){
    				_overflow = true; 
    			} else {
    				for (int j = p; j < i; j++){
    					_indices.set(j, 0); 
    				}
    				_indices.set(i, _indices.get(i)+1);
    				i = 0;
    			}
    		}
		}
		return combination; 
	}
	private boolean _overflow; 
	private ArrayList<Integer> _indices; 
	private ArrayList<ArrayList<Integer>> _matrix; 
}
