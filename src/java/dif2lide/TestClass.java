/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif2lide;


//import mocgraph.Node;

import java.util.ArrayList;

public class TestClass {
	// All tests
	public static void main(String[] args){
//		testUtilityRead("./test/d2h_256.csv");
//		testLWDFActors("./test/test_graph_3.dif", "./test/"); 
//		testSchedulerOnDummy("FCFS");  //testSchedulerOnDummy("EFT"); testSchedulerOnDummy("ALLGPU");
		testDIF2LIDE(); 
		//testDIFApp("./test/test_graph_3.dif", "./profiles/", "./profiles/h2d.csv", 
				//"./profiles/d2h.csv", "FCFS", 2, 2, 1); 
		//testDIFApp("./test/test_graph_3.dif", "./profiles/", "./profiles/h2d.csv", 
		//		"./profiles/d2h.csv", "ALLGPU", 2, 2, 2);
		
	}
	public static void testDIF2LIDE(){
		String configFile = "./config.xml";
		Utilities.dif2lide(configFile); 
	}
	/*public static void testDIFApp(String DIFFileName, String profileDir, String h2dFileName, 
			String d2hFileName, String schedulerName, int nThreads, 
			int vecDegree, int testID){
		int nGPU = 1; 
		Integer tID = new Integer(testID); 
		String testIDString = "Test " + tID.toString(); 
		System.out.println("Testing DIF App (" + testIDString + ") ...");
		DIFtoLWDFApp app = new DIFtoLWDFApp(DIFFileName, profileDir, h2dFileName, 
				d2hFileName, schedulerName, nThreads, nGPU, vecDegree, "./", 1); 
		app.run(); 
		System.out.print("Test DIF App (" + testIDString + ") Done"); 
		System.out.flush(); 
	}*/
	public static void testSchedulerOnDummy(String schedulerName){
		System.out.println("Test " + schedulerName + " Scheduler..."); 
		String DIFFileName = "./test/test_graph_eft_1.dif";
		String profileDir = "./profiles/"; 
		String h2dFileName = "./profiles/h2d_const.csv"; 
		String d2hFileName = "./profiles/d2h_const.csv";
		int nThreads = 2; 
		int vecDegree = 1;
		int nGPU = 1; 
		/*DIFtoLWDFApp app = new DIFtoLWDFApp(DIFFileName, profileDir, h2dFileName, 
				d2hFileName, schedulerName, nThreads, nGPU, vecDegree, "./", 1); 
		app.run();
		*/System.out.println("Test done."); 
	}
	/*
	public static void testLWDFActors(String fileName, String dirName){
		System.out.print("Testing Loading LWDFActors...");
		try {
            Reader reader = new Reader(fileName);
            reader.compile();
            DIFHierarchy topHierarchy = reader.getTopHierarchy();
	        SDFGraph graph = (SDFGraph) topHierarchy.getGraph();
	        Collection<Node> nodeCollection = graph.nodes(); 
	        List<Node> nodeList = (List<Node>) graph.topologicalSort(nodeCollection);
	        ArrayList<LWDFActor> actors = new ArrayList<LWDFActor>();  
	        for (int i = 0; i < nodeList.size(); i++){
	        	Node node = nodeList.get(i); 
	        	LWDFActor actor = new LWDFActor(node, graph, i, 0, dirName); 
	        	actors.add(actor); 
	        }
	        if (actors.size() == nodeList.size()){
	        	System.out.print("OK\n");
	        } else {
	        	System.out.print("failed!\n"); 
	        }
	        /*for (int i = 0; i < actors.size(); i++){
	        	actors.get(i).printContext(); 
	        	actors.get(i).printProfile(); 
	        }
	        System.out.flush(); 
        } catch (IOException exp) {
            throw new RuntimeException(exp.getMessage());
        } catch (DIFLanguageException exp) {
            throw new RuntimeException(exp.getMessage());
        } catch (GraphActionException exp) {
        	System.out.println("Error in topoligical sort"); 
			exp.printStackTrace();
		}
		
	}*/
	public static void testUtilityRead(String fileName){
		System.out.print("Testing Utility Read...");
		ArrayList<Double> X = new ArrayList<Double>(); 
		ArrayList<Double> Y = new ArrayList<Double>();
		Utilities.readPairValues(fileName, X, Y); 
		if (X.size() == Y.size()){
			System.out.print("OK\n");
		} else {
			System.out.print("failed!\n"); 
		}
		System.out.flush(); 
	}	
}
