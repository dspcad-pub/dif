/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif2lide;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Gcm{
	//constructor takes in a string. generated code manager
	public Gcm(String sourceFileName)
	{
		try {
			_sourcePrinter = new PrintWriter(new BufferedWriter(new FileWriter(sourceFileName)));
		} catch(IOException e){
			System.out.println("Error: Cannot find " + sourceFileName); 
			e.printStackTrace(); 
		}
		_indent=0;
		_charPos=0;
		_charLimit=80;
		_extIndent=false;
		_setIndent=true;
	}
	
	public void close(){
		_sourcePrinter.close();
	}
	
	public void flush1(){
		_sourcePrinter.flush();
	}
	//Function to write a string.
	public void write(String s){
	    _stringLen=s.length();
	    setIndent();
	    if(_charPos+_stringLen<=_charLimit){
	    	
	        printCode(s);
	        _charPos=_charPos+_stringLen;
	    }
	    else{
	       
	        if(_extIndent==false){
	            incIndent();         
	        }
	        nextLine();
	        setIndent();
	        _extIndent=true;
	        printCode(s);
	        _charPos=_charPos+_stringLen;
	    }
	}
    
    //write one full line provided we have less than 80 chars in one line.
    public void writeLine(String s){
	    printCodeLine(s);
	}	
	//increase the indent by a unit.
	public void incIndent(){
		_indent+=4;
		_charPos+=4;

	}
	
	//decrease the indent by a unit.
	public void decIndent(){
		_indent-=4;
		_charPos-=4;
	}
	
	//Shifts to next line and resets the char position.
	public void nextLine(){
		_sourcePrinter.print("\n");
		_charPos=_indent;
		_setIndent=true;
	}
	
	//switch to the next statement.
	public void next(){
		
		if(_extIndent==true){
		    decIndent();
		    _extIndent=false;
		}
		nextLine();
	}
	
	//give a blank line space in the generated code.
	public void blankLine(){
	_sourcePrinter.println("");

	}
	
	//has the char limit of a single line been reached?.
	public boolean isLineFull(){
		if (_charPos > _charLimit){
			return true;
		}
		else
			return false;
	}
	
	//will the line be full after adding the given string. I am allowing 
	public boolean willLineFull(String s){
		
		if (_charPos+_stringLen > _charLimit){
			return true;
		}
		else
			return false;
	}
	
	//setting the indent
	private void setIndent(){
		if(_setIndent==true){
		String spaces = new String(new char[_indent]).replace('\0',' ');
		printCode(spaces);
		//_charPos=_charPos+spaces.length();
		_setIndent=false;}
		
	}
	//print the specified code.
	public void printCode(String s){
		//String spaces = new String(new char[_indent]).replace('\0',' ');
		_sourcePrinter.print(s);
	}
	
	//print the specified code line.
	public void printCodeLine(String s){
		String spaces = new String(new char[_indent]).replace('\0',' ');
		_sourcePrinter.println(spaces+s);
	}
	
	
	private int _charPos; //position based on characters printed till now. Postion where the enext character would be written.
	private int _indent; //the level of indent till now
	private int _stringLen; //length of the incoming string.
	private int _charLimit; //max char limit that we want to have in a line
	private PrintWriter _sourcePrinter; //variable to hold the printwriter variable.
	private boolean _extIndent;// has the line been indented exrta?
	private boolean _setIndent;// Variable to check if indent is supposed to be printed or not.

}
