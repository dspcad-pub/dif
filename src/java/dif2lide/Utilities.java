/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif2lide;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Utilities {
	public static void dif2lide(String configFileName){
		DIFtoLWDFXMLReader xmlReader = new DIFtoLWDFXMLReader(configFileName);
		DIFtoLWDFConfig config = xmlReader.getConfiguration();


		int length = config.difFile.lastIndexOf("/");
		String difFileName = config.difFile.substring(length+1);
		length = difFileName.lastIndexOf(".dif");
		difFileName = difFileName.substring(0, length);
		String outputDir = config.resultDirectory+"/";
		File file = new File(outputDir);
		if (!file.exists()){
			file.mkdirs();
		}
		//System.out.println("Writing to " + outputDir + "..."); 
		//DIFtoLWDFDsg dsg = new DIFtoLWDFDsg(config.dsgFile, outputDir);
		System.out.println(config.dsgFile);
		DIFtoLWDFApp app = new DIFtoLWDFApp(config.difFile,config.dsgFile, config.profileDirectory,
				config.h2dProfile, config.d2hProfile,
				config.schedulerName, config.numberOfThreads, config.numberOfGPUs,
				outputDir, config.repetitions);

		app.run();
		//dsg.run();
		/*
		for (int i = 0; i < config.vecDegrees.size(); i++){
		    int length = config.difFile.lastIndexOf("/");
			String difFileName = config.difFile.substring(length+1); 
		    length = difFileName.lastIndexOf(".dif");
		    difFileName = difFileName.substring(0, length); 
			String outputDir = "./result/" + difFileName + "/" + "/"; 
			File file = new File(outputDir); 
			if (!file.exists()){
				file.mkdirs(); 
			}
			System.out.println("Writing to " + outputDir + "..."); 
			DIFtoLWDFApp app = new DIFtoLWDFApp(config.difFile, config.profileDirectory, 
					config.h2dProfile, config.d2hProfile, 
					config.schedulerName, config.numberOfThreads, config.numberOfGPUs, 
					config.vecDegrees.get(i), outputDir, config.repetitions); 
			app.run();*/
	}



	public static ArrayList<String> combineString(ArrayList<String> strArray1,
												  ArrayList<String> strArray2, String delim){
		ArrayList<String> strList = new ArrayList<String>();
		int size = Math.min(strArray1.size(), strArray2.size());
		for (int i = 0; i < size; i++){
			strList.add(strArray1.get(i) + delim + strArray2.get(i));
		}
		return strList;
	}

	public static String commaSeparatedString(List<String> list){
		String str = "";
		for (int i = 0; i < list.size(); i++){
			String word = list.get(i);
			if (i < list.size() - 1){
				str += word + ",";
			} else {
				str += word;
			}
		}
		return str;
	}

	public static void readPairValues(String fileName, ArrayList<Double> X,
									  ArrayList<Double> Y){
		try {
			X.clear();
			Y.clear();
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			String line = "";
			while ((line = br.readLine()) != null) {
				String testEmptyLine = line.replaceAll("[,\\s]", "");
				if (testEmptyLine.length() == 0){
					continue;
				}
				String values[] = line.split(" ");
				if (values.length == 2) {
					String strValue = values[0].replaceAll("[,\\s]", "");
					X.add(Double.parseDouble(strValue));
					strValue = values[1].replaceAll("[,\\s]", "");
					Y.add(Double.parseDouble(strValue));
				} else if (values.length == 1){
					values = line.split(",");
					if (values.length == 2) {
						String strValue = values[0].replaceAll("[,\\s]", "");
						X.add(Double.parseDouble(strValue));
						strValue = values[1].replaceAll("[,\\s]", "");
						Y.add(Double.parseDouble(strValue));
					} else {
						System.out.println("Wrong format for the profile: " + fileName);
						break;
					}
				} else {
					System.out.println("Wrong format for the profile." + fileName);
					break;
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

}
