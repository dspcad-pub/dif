/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif2lide;

import org.apache.commons.math3.analysis.interpolation.LinearInterpolator;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;

import java.util.ArrayList;

/* Note: Use: 1 for GPU, 0 for CPU */

public class ArchitectureCPUGPU {
	public ArchitectureCPUGPU(String h2dFileName, String d2hFileName, int nCPUThreads, int nGPUThreads){
		this._nCPUThreads = nCPUThreads; 
		this._nGPUThreads = nGPUThreads; 
		int totalProcessors = nCPUThreads + nGPUThreads;
		_threadTypes = new ArrayList<Integer>(totalProcessors); 
		for (int i = 0; i < totalProcessors; i++){
			_threadTypes.add(i, i < nCPUThreads ? 0 : 1); 
		}
		_h2dSizes = new ArrayList<Double>(); 
		_h2dTimes = new ArrayList<Double>(); 
		_d2hSizes = new ArrayList<Double>(); 
		_d2hTimes = new ArrayList<Double>();
		Utilities.readPairValues(h2dFileName, _h2dSizes, _h2dTimes);
		Utilities.readPairValues(d2hFileName, _d2hSizes, _d2hTimes);
		// interpolation, h2d
		LinearInterpolator interpolator = new LinearInterpolator();
		double h2dvec[] = new double[_h2dSizes.size()];		
		double h2dTimes[] = new double[this._h2dTimes.size()];
		for (int i = 0; i < this._h2dTimes.size(); i++){
			h2dvec[i] = _h2dSizes.get(i);
			h2dTimes[i] = _h2dTimes.get(i); 
		}
		_func_h2d = interpolator.interpolate(h2dvec, h2dTimes);
		// interpolations, d2h
		double d2hvec[] = new double[_d2hSizes.size()];
		double d2hTimes[] = new double[this._d2hTimes.size()];
		for (int i = 0; i < this._d2hTimes.size(); i++){
			d2hvec[i] = _d2hSizes.get(i); 
			d2hTimes[i] = _d2hTimes.get(i); 
		}
		_func_d2h = interpolator.interpolate(d2hvec, d2hTimes);
	}
	
    public double getTransferTime(int dstPid, int srcPid, int dataSize){
    	if (dstPid == srcPid){
    		return 0; 
    	}
    	int dstType = _threadTypes.get(dstPid); 
    	int srcType = _threadTypes.get(srcPid);
    	if (dstType == 0 && srcType == 0){
    		return 0; 
    	} else {
    		if (srcType == 0 && dstType == 1){
    			return _func_h2d.value((double) dataSize); // H2D transfer
    		} else if (srcType == 1 && dstType == 0){
    			return _func_d2h.value((double) dataSize); // D2H transfer
    		} else {
    			GlobalSettings.debugMessage("Architecture: Warning: no GPU-to-GPU available at the time"); 
    			return 0; 
    		}
    	}
    }
    
    // Member query functions
    public int getProcessorType(int pid) {
    	return _threadTypes.get(pid); 
    }
    public void setProcessorType(int pid, int type) {
    	_threadTypes.set(pid, type);  
    }
    public int getNumCPUThreads(){
    	return _nCPUThreads; 
    }
    public int getNumGPUThreads(){
    	return _nGPUThreads; 
    }
    
    
    
	// Members
    private int _nCPUThreads; 
    private int _nGPUThreads; 
    // cpu_thread0 , ..., cpu_threadN, gpu_thread_0,..., gpu_threadN  
	private ArrayList<Integer> _threadTypes;  
	private ArrayList<Double>_h2dSizes;
	private ArrayList<Double>_h2dTimes;
	private ArrayList<Double>_d2hSizes;
	private ArrayList<Double> _d2hTimes; 
	private PolynomialSplineFunction _func_h2d; 
	private PolynomialSplineFunction _func_d2h; 
}
