/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif;

import dif.util.Conventions;

import java.util.LinkedHashMap;
import java.util.LinkedList;


/**
 * Container class for attributes used in DIFGraph.
 * This class contains the implementation details of how attributes
 * are handled in DIFGraph. For each ({@link dif.util.graph.Node} and
 * ({@link dif.util.graph.Edge}),
 * or {@link DIFGraph} object, there is an associated AttributeContainer
 * containing all the attributes of that object. Also there are {@link #setName}
 * and {@link #getName} methods of the AttributeContainer which is for storing
 * the name of the object (Edge/Node) in a graph.
 * <p>
 * Each DIFAttribute is recognized by its name. The AttributeContainer
 * utilizes LinkedHashMap
 * to store DIFAttributes.
 * <p>
 * To retrieve a DIFAttribute, using {@link DIFAttribute#getName()} as key.
 * (Note that
 * it is updated from using both name and type as a key to just name. In
 * general,
 * every actor attribute should have a unique name, and type is just used as
 * supplementary information. However, in this case, an actor having multiple
 * attributes with same name and different types is not supported.)
 * <p>
 * To get the list of DIFAttributes, the "linked" property can maintain the
 * order
 * of insertion.
 *
 * @author Chia-Jui Hsu based on the first version by Fuat Keceli
 * @version $Id: AttributeContainer.java 606 2008-10-08 16:29:47Z plishker $
 * @see DIFGraph
 * @see DIFAttribute
 */

public class AttributeContainer {

    public AttributeContainer() {
        _attributes = new LinkedHashMap<>();
    }

    public AttributeContainer(String name) {
        _attributes = new LinkedHashMap<>();
        setName(name);
    }

    ///////////////////////////////////////////////////////////////////
    ////                       public methods                      ////

    /**
     * Clone the AttributeContainer and return the cloned version.
     * All the DIFAttributes in the AttributeContainer are cloned by
     * calling {@link DIFAttribute#clone}
     *
     * @return The cloned AttributeContainer.
     */
    @SuppressWarnings("CloneDoesntCallSuperClone") public Object clone() {
        AttributeContainer container = new AttributeContainer();
        if (_name != null) {
            container.setName(_name);
        }
        LinkedList<DIFAttribute> attributes = new LinkedList<>(
                _attributes.values());
        for (Object attribute : attributes) {
            DIFAttribute attr = (DIFAttribute) attribute;
            container.setAttribute((DIFAttribute) attr.clone());
        }
        return container;
    }

    /**
     * Clone the AttributeContainer and return the cloned version
     * basedon <i>map</i> that contains the mapping from the original
     * reference value to the cloned reference value.
     * All the DIFAttributes in the AttributeContainer are cloned by
     * calling {@link DIFAttribute#clone(Object)}. The cloned version of
     * object that contains this AttributeContainer should call
     * _setContainer of this class.
     *
     * @param map The HashMap that contains the mapping
     *            from the original reference value to the cloned reference
     *            value.
     * @return The cloned AttributeContainer.
     * @see DIFAttribute
     */
    public Object clone(Object map) {
        AttributeContainer container = new AttributeContainer();
        if (_name != null) {
            container.setName(_name);
        }
        LinkedList<DIFAttribute> attributes = new LinkedList<>(
                _attributes.values());
        for (Object attribute : attributes) {
            DIFAttribute attr = (DIFAttribute) attribute;
            container.setAttribute((DIFAttribute) attr.clone(map));
        }
        return container;
    }

    /**
     * Test if an attribute container is equal to this one. It is equal
     * if it is of the same class and
     * all the attributes are the same. For two attributes to be the same
     * they should have the same names and same values but not necessarily the
     * same objects.
     *
     * @param container The attribute container with which to compare this
     *                  container.
     * @return True if the container is equal to this one.
     */
    public boolean equals(Object container) {
        if (container == null) {
            return false;
        }
        if (!(container instanceof AttributeContainer)) {
            return false;
        }
        if (!((((AttributeContainer) container).getName() == null &&
                _name == null) ||
                ((AttributeContainer) container).getName().equals(_name))) {
            return false;
        }
        if (((AttributeContainer) container).getAttributes().size() !=
                _attributes.size()) {
            return false;
        }
        LinkedList<DIFAttribute> attributes = new LinkedList<>(
                _attributes.values());
        for (Object attribute : attributes) {
            DIFAttribute attr = (DIFAttribute) attribute;
            if (!attr.equals(((AttributeContainer) container)
                                     .getAttribute(attr.getName()))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns the DIFAttribute in this container with the given name.
     *
     * @param name Name of the attribute.
     * @return The DIFAttribute. Null if it doesn't exist.
     */
    public DIFAttribute getAttribute(String name) {
        //return getAttribute(name, null);
        return (DIFAttribute) _attributes.get(name);
    }

    /**
     * Returns the first DIFAttribute with the same value,
     * ie, getValue() == value.
     * Or if DIFAttribute contains a list of objects and that list
     * contains the same value object.
     *
     * @param value The value object.
     * @return The DIFAttribute. Null if it doesn't exist.
     */
    public DIFAttribute getAttributeByContent(Object value) {
        LinkedList<DIFAttribute> attributes = new LinkedList<>(
                _attributes.values());
        for (DIFAttribute attr : attributes) {
            if (attr.getValue() == value) {
                return attr;
            } else if (attr.getValue() instanceof LinkedList) {
                if (((LinkedList) attr.getValue()).contains(value)) {
                    return attr;
                }
            }
        }
        return null;
    }

    /**
     * Returns the DIFAttributes in this container with the same value,
     * ie, getValue() == value.
     * Or if DIFAttribute contains a list of objects and that list
     * contains the same value object.
     *
     * @param value The value object.
     * @return The LinkedList of DIFAttributes containes the value.
     * Or an empty LinkedList if not found.
     */
    public LinkedList<DIFAttribute> getAttributesByContent(Object value) {
        LinkedList<DIFAttribute> returnValue = new LinkedList<>();
        LinkedList<DIFAttribute> attributes = new LinkedList<>(
                _attributes.values());
        for (DIFAttribute attr : attributes) {
            if (attr.getValue() == value) {
                returnValue.add(attr);
            } else if (attr.getValue() instanceof LinkedList) {
                if (((LinkedList) attr.getValue()).contains(value)) {
                    returnValue.add(attr);
                }
            }
        }
        return returnValue;
    }

    /**
     * Returns the LinkedList of DIFAttributes in this container.
     *
     * @return The LinkedList of DIFAttributes.
     */
    public LinkedList<DIFAttribute> getAttributes() {
        return new LinkedList<>(_attributes.values());
    }


    /**
     * Returns a LinkedList of the names of all DIFAttributes in this container.
     *
     * @return A LinkedList of Strings.
     */
    public LinkedList<String> getAttributeNames() {
        LinkedList<String> names = new LinkedList<>();
        LinkedList<DIFAttribute> attributes = new LinkedList<>(
                _attributes.values());
        for (DIFAttribute attribute : attributes) {
            names.add(((DIFAttribute) attribute).getName());
        }
        return names;
    }

    /**
     * Returns the object contains this AttributeContainer.
     * Note that this AttributeContainer may associate with a node, an
     * edge, or the graph itself.
     *
     * @return the object contains this AttributeContainer.
     */
    public Object getContainer() {
        return _container;
    }

    /**
     * Returns the name of this container set by <code>setName</code>
     * method.
     */
    public String getName() {
        return _name;
    }

    /**
     * Returns the hash code for this object.
     *
     * @return The hash code for this object.
     */
    public int hashCode() {
        int code = getClass().getName().hashCode();
        code += toString().hashCode();
        return code;
    }

    /**
     * Removes the DIFAttribute with the given name.
     *
     * @param name Name of the attribute.
     * @return The previous attribute. Null if it didn't exist.
     */
    public DIFAttribute removeAttribute(String name) {
        //return removeAttribute(name, null);
        return (DIFAttribute) _attributes.remove(name);
    }

    /**
     * Removes the DIFAttribute from the container.
     *
     * @param attribute The DIFAttribute object.
     * @return The previous attribute. Null if it didn't exist.
     */
    public DIFAttribute removeAttribute(DIFAttribute attribute) {
        if (_attributes.containsValue(attribute)) {
            return (DIFAttribute) _attributes.remove(attribute.getName());
        } else {
            return null;
        }
    }

    /**
     * Reset this attribute container.
     */
    public void removeAllAttributes() {
        _attributes = new LinkedHashMap<>();
    }

    /**
     * Set the attribute with the given name and DIFAttribute.
     *
     * @param attribute The DIFAttribute.
     * @return The previous DIFAttribute with the same name (and type)
     * if it existed before. Null otherwise.
     */
    public DIFAttribute setAttribute(DIFAttribute attribute) {
        DIFAttribute returnValue = (DIFAttribute) _attributes.put(
                attribute.getName(), attribute);
        //backward reference
        attribute._setContainer(this);

        return returnValue;
    }

    /**
     * Sets the name of this container. Note that DIFGraph use this name to
     * recognize the name
     * of edges and nodes.
     *
     * @return The previous name of this container.
     * @throws IllegalArgumentException If
     * {@link Conventions#labelConvention(String)} returns an error.
     */
    public String setName(String name) {
        String error = Conventions.labelConvention(name);
        if (error != null) {
            throw new IllegalArgumentException(
                    "Element/graph name error: " + error);
        }
        String returnValue = _name;
        _name = name;
        return returnValue;
    }

    /**
     * Returns the description of this attribute container. The description
     * contains the name of the object as defined
     * by {@link #setName} method and names and values of the attributes.
     * Attributes are sorted according to their names so this description is
     * canonical.
     *
     * @return Description of this container.
     */
    public String toString() {
        StringBuilder buffer = new StringBuilder();
        buffer.append("Name: ").append(getName()).append("\n");
        LinkedList<DIFAttribute> attributes = new LinkedList<>(
                _attributes.values());
        for (Object attribute : attributes) {
            buffer.append(((DIFAttribute) attribute).toString());
        }
        return buffer.toString();
    }

    public void flagAsArrayElement() {
        _isArrayElement = true;
    }

    public void flagAsNonArrayElement() {
        _isArrayElement = false;
    }

    public void setArrayIndex(int index) {
        _index = index;
    }


    void _setContainer(Object container) {
        _container = container;
    }


    private String _name;

    /* True if this element is an array element; False if this element is an
    scalar element */
    private boolean _isArrayElement;
    /* Store array index */
    private int _index;

    private LinkedHashMap<String, DIFAttribute> _attributes;
    private Object _container;

}


