/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif;

import dif.util.graph.Graph;
import dif.util.sched.ScheduleTree;
import dif.util.sched.ScheduleAnalyzer;

//////////////////////////////////////////////////////////////////////////
//// DIFScheduleStrategy

/**
 * An abstract class for all the scheduling strategies on DIF graphs.
 * The associated graph must be an instance of
 * {@link DIFGraph}.
 *
 * @author Mingyung Ko, Shuvra S. Bhattacharyya
 * @version $Id: DIFScheduleStrategy.java 606 2008-10-08 16:29:47Z plishker $
 */

public abstract class DIFScheduleStrategy implements ScheduleAnalyzer {

    /**
     * A constructor with a graph. The associated graph must be an
     * instance of {@link DIFGraph}.
     */
    public DIFScheduleStrategy(DIFGraph graph) {
        _graph = graph;
        _clusterManager = new DIFClusterManager(graph);
    }

    ///////////////////////////////////////////////////////////////////
    ////                        public methods                     ////

    /**
     * Get the manager for maintaining hierarchical graph clustering.
     *
     * @return The cluster manager.
     */
    public DIFClusterManager getClusterManager() {
        return _clusterManager;
    }

    /**
     * Given a collection of nodes in a DIF graph, replace the subgraph
     * /** Get the associated graph. The graph is an instance of
     * {@link DIFGraph}.
     *
     * @return The assocaited DIF graph.
     */
    public Graph graph() {
        return _graph;
    }

    /**
     * The schedule computation.
     *
     * @return An empty schedule in the base scheduler.
     */
    public ScheduleTree schedule() {
        return new ScheduleTree();
    }

    /**
     * A description of the scheduler.
     *
     * @return A brief description of the base scheduler.
     */
    public String toString() {
        return "A base class for DIFGraph schedulers.\n";
    }

    /**
     * Validity checking for certain DIFGraph schedulers.
     *
     * @return True always in this base scheduler.
     */
    public boolean valid() {
        return true;
    }

    ///////////////////////////////////////////////////////////////////
    ////                      protected variables                  ////

    /**
     * A class for managing hierarchical clustering.
     */
    protected DIFClusterManager _clusterManager;

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    /* The associated DIF graph. */
    private DIFGraph _graph;
}


