/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif;

import dif.difml.parser.DIFMLReader;
import dif.lang.Converter;

import java.io.File;
import java.util.List;

/**
 * DIFLoader class
 * DIFLoader class is used to encapsulate the process of bison parser and call
 * difml functions.
 *
 * @author jiahao wu
 * @see dif.difml.parser.DIFMLReader
 */

public class DIFLoader {
    private static String getOutputFilename(String inputFilename) {
        if (inputFilename.endsWith(".dif")) {
            return inputFilename + "ml";
        } else {
            return "output.xml";
        }
    }

    private static void dif2difml(String filename) {
        Converter.toDIFML(filename, getOutputFilename(filename));
    }

    private static void dif2difml(File file) {
        Converter.toDIFML(file, getOutputFilename(file.getName()));
    }

    public static DIFHierarchy loadDataflow(File file) {
        dif2difml(file);
        DIFMLReader reader = new DIFMLReader(
                file.getParent() + "/" +
                        getOutputFilename(file.getName()));
        reader.read();
        return reader.getHierarchy();
    }

    public static DIFHierarchy loadDataflow(String filename) {
        // convert dif to difml
        dif2difml(filename);
        // load difml as DIFHierarchy
        DIFMLReader reader = new DIFMLReader(getOutputFilename(filename));
        reader.read();
        return reader.getHierarchy();
    }

    public static List<DIFHierarchy> loadDIFML(String filename) {
        // load difml as DIFHierarchy
        DIFMLReader reader = new DIFMLReader(filename);
        reader.read();
        return reader.getHierarchyList();
    }

    public static List<DIFHierarchy> loadDataflowList(String filename) {
        dif2difml(filename);
        DIFMLReader reader = new DIFMLReader(getOutputFilename(filename));
        reader.read();

        return reader.getHierarchyList();
    }
}
