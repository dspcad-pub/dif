/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A thread that sets up the communication between a target application and a
client.
*/
package dif.util.command;

import java.net.Socket;
import java.io.IOException;
import java.io.DataOutputStream;
import java.io.DataInputStream;

//////////////////////////////////////////////////////////////////////////
//// CommunicationSetup
/** ExecutionSetup is a thread that sets up the exclusive communication with one
connected driver application.
<p>

@author Shahrooz Shahparnia based on a code by Mark Nadelson in the JavaPro
magazine. September 2002 issue.
@version $Id: CommunicationSetup.java 606 2008-10-08 16:29:47Z plishker $
*/

public class CommunicationSetup extends Thread {

    /** CommunicationSetup is a thread that sets up the exclusive communication
     *  with one connected driver application running as a ExecutionClient.
     *  @param socket The socket on which the communication is going to be
     *  setup
     *  @param server The ExecutionServer who is creating the communication
     *  channel.
     *  @exception IOException If the object fails to start the communication
     *  channel.
     */
    public CommunicationSetup(Socket socket, ExecutionServer server)
            throws IOException {
        _socket = socket;
        _server = server;
        _dataOutput = new DataOutputStream(socket.getOutputStream());
        _dataInput = new DataInputStream(_socket.getInputStream());
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////


    /** Run method of the thread.  This thread receives spawn related commands
     *  from the driver application. Sets up the communication with the target
     *  application (the command being executed on the host) and finishes
     *  execution.
     */
    public void run() {
        ExecutionSetup executionSetup = null;
        try {
            // The first command received is spawn method,
            // SPAWNATTACHED or SPAWNDETACHED
            String cmd = _dataInput.readUTF();
            if (cmd.equals("SPAWNATTACHED")) {
              String cmdArray[] = null;
            // The number of parameters used to spawn the target
            // application is recieved
            int numParams = _dataInput.readInt();
            // A static String array is constructed to store the
            // target application parameters
            cmdArray = new String[numParams];
            // Each target application parameter is recieved and stored
            for (int count = 0; count < numParams; count++) {
                cmdArray[count] = _dataInput.readUTF();
            }
            // An ExecutionSetup object is created with detached = false and is
            // used to spawn and monitor the target application
            executionSetup = new ExecutionSetup(cmdArray,
                    _dataOutput, _dataInput, false);
            executionSetup.spawn();
            } else if (cmd.equals("SPAWNDETACHED")) {
                String cmdArray[] = null;
                // The number of parameters used to spawn the target
                // application is recieved
                int numParams = _dataInput.readInt();
                // A static String array is constructed to store the target
                // application parameters
                cmdArray = new String[numParams];
                // Each target application parameter is recieved and stored
                for (int count = 0; count < numParams; count++) {
                    cmdArray[count] = _dataInput.readUTF();
            }
                // An ExecutionSetup object is created with detached =
                // false and is used to spawn
                // and monitor the target application
                executionSetup = new ExecutionSetup(cmdArray,
                        _dataOutput, _dataInput, true);
                executionSetup.spawn();
            }

            if (executionSetup != null && !executionSetup.isDetached()) {
                // The ExecutionSetup object does not return unless the process
                // spawned exits.
                // If it does exit and the target application is attached,
                // it is killed and all socket streams back to the driver
                // application are closed.
                executionSetup.kill();
                _dataInput.close();
                _dataOutput.close();
            }
        } catch(IOException ex) {
            System.out.println("Client Communicate: " + ex);
            if (executionSetup != null && !executionSetup.isDetached()) {
                // If there is a error receiving driver application commands
                // and the ExecutionSetup object was created and the target
                // app is attached, the target application is killed.
                executionSetup.kill();
            }
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    // The socket connected to the driver app
    private Socket _socket;
    private ExecutionServer _server;
    // Output stream used to communicate with the driver app
    private DataOutputStream _dataOutput = null;
    // Input stream used to communicate with the driver app
    private DataInputStream _dataInput = null;
}


