/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* The purpose of this class is to execute the target application on the host.
*/

package dif.util.command;

import java.io.IOException;
import java.io.OutputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.DataInputStream;

//////////////////////////////////////////////////////////////////////////
//// ExecutionSetup
/** The purpose of this class is to execute the target application on the host,
open input, output, and error streams to the target application, and monitor the
executing application.
<p>

@author Shahrooz Shahparnia based on a code by Mark Nadelson in the JavaPro
magazine. September 2002 issue.
@version $Id: ExecutionSetup.java 606 2008-10-08 16:29:47Z plishker $
*/

public class ExecutionSetup {

    /** Starts running the target application (command) on the host machine
     *  and connects the input, output and error streams of the application to
     *  the given data input/output streams through ProcessInput/OuputStreams.
     *  The error and output streams are both connected to the same data stream.
     *  @param command The command and parameters of the command that it going
     *  to be executed on the host.
     *  @param dataOutput The data channel which sends data to the client side.
     *  @param dataInput The data channel which receives data from the
     *  client side.
     *  @param detached The execution type requested by the client.
     */
    public ExecutionSetup(String[] command, DataOutputStream dataOutput,
            DataInputStream dataInput, boolean detached) {
        _dataOutput = dataOutput;
        _dataInput = dataInput;
        _detached = detached;
        _command = command;
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Returns whether the spawned target application is detached or attached.
     *  @return True If the spawned process is detached.
     */
    public boolean isDetached() {
        return _detached;
    }

    /** Kills the executing target application (the command being executed).
      */
    public void kill() {
        if (_currentProcess != null) {
            _currentProcess.destroy();
        }
    }

    /** Spawns the target application, creates threads to communicate with its
     * standard input, output, and error streams, and monitors process
     * execution. The threads are instances of ProcessInput/OutputStream.
     */
    public void spawn() {
        _application = Runtime.getRuntime();
        try {
            // Spawns the target application
            _currentProcess = _application.exec(_command);

            // Retrieves standard input, output, and error streams from
            // the target app
            InputStream inStream = _currentProcess.getInputStream();
            OutputStream outStream = _currentProcess.getOutputStream();
            InputStream errorStream = _currentProcess.getErrorStream();

            // Create threads to read the standard output and error streams.
            // These threads are created regardless of attached or detached mode
            // since the output buffers of the target app need to be flushed
            // otherwise the target app will block.
            _processStream = new ProcessOutputStream
                    (inStream, _dataOutput, _detached);
            _errorStream = new ProcessOutputStream
                    (errorStream, _dataOutput, _detached);
            _processStream.start();
            _errorStream.start();

            _commandStream = new ProcessInputStream
                    (outStream, _dataInput, _detached);
            _commandStream.start();
        //}

            // If the application was spawned successfully, a SPAWNEDOK message
            // is returned
            _dataOutput.writeUTF("SPAWNEDOK");

            try {
                // Wait for the spawned application to terminate.
                _currentProcess.waitFor();
            } catch(Exception ex) {
                System.out.println("Command Stream Exception: " + ex);
            }
        } catch(Exception ex) {
            try {
                // If there is a problem spawning the target application an
                // error message is returned to the driver application.
                _dataOutput.writeUTF("Error Spawning: " + ex);
            } catch(IOException ioEx) {
                System.out.println(
                        "Error Communicating back to client: " + ioEx);
            }
            kill();
        }
    }
    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    // A referense to the list of application parameters
    private String[] _command;
    // The driver's socket output stream
    private DataOutputStream _dataOutput = null;
    // The driver's socket output stream
    private DataInputStream _dataInput = null;
    // Holds the runtime environment
    private Runtime _application = null;
    // Reference to the spawned application
    private Process _currentProcess = null;
    // Processes the standard output stream from the target app
    private ProcessOutputStream _processStream = null;
    // Processes the standard error stream from the target app
    private ProcessOutputStream _errorStream = null;
    // Processes the standard input stream from the target app
    private ProcessInputStream _commandStream = null;
    // Indicates whether the target application is spawned in
    // attached or detached mode
    private boolean _detached = false;
}


