/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/*Data stream to process the output of the target application.
*/

package dif.util.command;

import java.io.IOException;
import java.io.OutputStream;
import java.io.DataInputStream;

//////////////////////////////////////////////////////////////////////////
//// ProcessInputStream
/** ProcessInputStream is a thread which reads from a given DataInputStream
and writes to the input stream of the target application.
<p>

@author Shahrooz Shahparnia, the remote execution is based on a code by
Mark Nadelson in the JavaPro magazine. September 2002 issue.

@version $Id: ProcessInputStream.java 606 2008-10-08 16:29:47Z plishker $
*/

public class ProcessInputStream extends Thread {

    /** A thread which reads from a given DataInputStream and writes to the
     *  input stream of the target application.
     *  @param out The input stream of the target application.
     *  @param dataInput the given DataInputStream.
     *  @param detached The execution type requested by the client.
     */
    public ProcessInputStream(OutputStream out, DataInputStream dataInput,
            boolean detached) {
        _detached = detached;
        _out = out;
        _dataInput = dataInput;
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /**
     * Thread method.  Reads from the socket output stream of the driver app
     * and redirects it to the standard input stream of the target app
     */
    public void run() {
        byte byteBuf[] = new byte[500];
        try {
            while(true) {
                int numBytes = _dataInput.read(byteBuf, 0, 500);
                if (numBytes > 0) {
                    if(!_detached) {
                        _out.write(byteBuf, 0, numBytes);
                        _out.flush();
                    }
                } else {
                    return;
                }
            }
        } catch(IOException ex) {
            // System.out.println("Process Input Stream: " + ex);
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    // Driver app socket output stream
    private OutputStream _out;
    // Target app standard input stream
    private DataInputStream _dataInput;
    // Indicates attached (false) or detached
    private boolean _detached = false;

}


