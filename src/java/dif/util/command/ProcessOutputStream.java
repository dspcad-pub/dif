/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Data stream to process the output of the target application.
*/

package dif.util.command;

import java.io.IOException;
import java.io.DataOutputStream;
import java.io.InputStream;

//////////////////////////////////////////////////////////////////////////
//// ProcessOutputStream
/** ProcessOutputStream is a thread which reads data from the standard output
stream of the target application and redirects it to a given DataOutputStream.
<p>

@author Shahrooz Shahparnia, the remote execution is based on a code by
Mark Nadelson in the JavaPro magazine. September 2002 issue.

@version $Id: ProcessOutputStream.java 606 2008-10-08 16:29:47Z plishker $
*/

public class ProcessOutputStream extends Thread {

    /** A thread which reads data from the standard output stream of the
     *  target application and redirects it to a given DataOutputStream.
     *  @param in The output stream of the target application.
     *  @param dataOutput the given DataOutputStream.
     *  @param detached The execution type requested by the client.
     */
    public ProcessOutputStream(InputStream in, DataOutputStream dataOutput,
            boolean detached) {
        _in = in;
        _dataOutput = dataOutput;
        _detached = detached;
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

   /**
    * Thread method.  Reads from the standard output stream of the target
    * app and redirects to the input socket stream of the driver app.
    */
    public void run() {
        byte byteBuf[] = new byte[500];
        try {
            while(true) {
                int numBytes = _in.read(byteBuf, 0, 500);
                if (numBytes > 0) {
              // If the process is "detached" do not forward output of the
              // target app to the driver app
                    if (!_detached) {
                        _dataOutput.write(byteBuf, 0, numBytes);
                        _dataOutput.flush();
                    }
                } else {
                    return;
                }
            }
        } catch(IOException ex) {
            System.out.println("Process Output Stream: " + ex);
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    // Input socket stream of driver app
    private InputStream _in = null;
    // Standard output stream of target app
    private DataOutputStream _dataOutput = null;
    // Indicates attached (false) or detached (true) mode
    private boolean _detached = false;
}


