/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* The ExecutionClient encapsulates the spawning of the target application and
target to driver application communication.
*/

package dif.util.command;

import java.net.Socket;
import java.net.ServerSocket;
import java.io.IOException;

//////////////////////////////////////////////////////////////////////////
//// CommandUtilities
/** The ExecutionClient encapsulates the spawning of the target application and
target to driver application communication.
<p>

@author Shahrooz Shahparnia based on a code by Mark Nadelson in the JavaPro
magazine. September 2002 issue.
@version $Id: ExecutionServer.java 606 2008-10-08 16:29:47Z plishker $
*/

public class ExecutionServer {

    /** ExecutionServer creates binds a TCP socket to the given port and
     *  listens for connections from driver applications.
     *  @param portNumber The given port number to listen to.
     *
     */
    public ExecutionServer(int portNumber) {
        try {
            _serverSocket = new ServerSocket(portNumber);
        } catch(IOException ex) {
            System.out.println("Error Creating Socket: " + ex);
            System.exit(0);
        }
        _acceptConnections();
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Starts the ExecutionServer.
     *  @param args The port number to listen for eventual clients.
     *  If not defined the default is 6001.
     */
    public static void main(String[] args) {
        ExecutionServer executionServer = new ExecutionServer(6001);
    }

    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////

    /* Loops forever accepting connections.
     */
    private void _acceptConnections() {
        while(true) {
            try {
                // When a connection is accepted a new TCP socket is created.
                Socket newSocket = _serverSocket.accept();
                 // A ClientCommunicate thread is created using new TCP socket
                // connection.
                // This thread handles all communication between ExecutionServer
                // and the driver application.
                CommunicationSetup newClient =
                        new CommunicationSetup(newSocket, this);
                System.out.println(newSocket);
                newClient.start();
            } catch(IOException ex) {
                System.out.println("Error Creating Socket "
                        + "For Connecting Client: " + ex);
            }
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    private ServerSocket _serverSocket;
}


