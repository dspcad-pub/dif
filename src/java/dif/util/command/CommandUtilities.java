/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Utilities for command prompt execution.
*/

package dif.util.command;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;

//////////////////////////////////////////////////////////////////////////
//// CommandUtilities
/** A class that included utilities that interact with the command prompt
of the underlying operating system.
Note that the executions in ExecutionClient are serial executions though the
ExecutionServer is a parallel executing server.
Therefore each ExecutionServer will be able to accept multiple communications
and run their commands in parallel, assuming that its underlying operating
system is a multitasking one.
In order to have parallel executions on the client side, multiple threads
should be created, each including one ExecutionClient.
<p>It is a known problem that the same
CommandUtilities object throws an exception if it is used to run more than two
local commands. Therefore it is advisable to use a new object for each new
local command. (added by Fuat Keceli)
<p>

@author Shahrooz Shahparnia, the remote execution method is based on a code by
Mark Nadelson in the JavaPro magazine. September 2002 issue.

@version $Id: CommandUtilities.java 713 2009-06-25 16:19:55Z kishans $
*/
public class CommandUtilities {

    /** The default constructor. It does nothing.
     */
    public CommandUtilities() {
        super();
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Return the execution message the program generates while executing
     *  a command using the runRemoteCommand and runCommand methods.
     *  @return The execution message.
     */
    public String getCommandResult() {
        return _commandResult.toString();
    }

    /** Return the error message the program generates while executing a
     *  command using the runRemoteCommand and runCommand methods.
     *  @return The error message.
     */
    public String getErrorMessage() {
        return _errorMessage.toString();
    }

    /** Runs a command on a remote machine and returns true on zero exit.
     *  @param host The host on which the ExecutionServer is running on.
     *  @param hostPort The port number which the ExecutionServer is listening
     *  to.
     *  @param command The command and parameters of the command that it going
     *  to be executed on the host.
     *  @return True If the program exits with a zero value.
     */
    public boolean runRemoteCommand(String host, int hostPort,
            String[] command) {
        _commands = command;
        boolean returnStatus = false;
        int exitValue = -1;
        String line = null;
        _commandResult = new StringBuffer();
        _errorMessage = new StringBuffer();

        if(_commands == null) {
            _errorMessage.append("Command was null, there was nothing to run.");
            return false;
        } else if((_commands.length < 1)) {
                _errorMessage.append("Usage: runRemoteCommand(<HOST>"
                        + ", <command to execute including arguments>");
            return false;
        }

        try {
            ExecutionClient client = new ExecutionClient
                    (ExecutionClient.ATTACHED, host, hostPort, _commands);
            String processInfo = null;
            while((processInfo = client.read(_LF)) != null) {
                _commandResult.append(processInfo + "\n");
            }
            returnStatus = true;
        } catch (RemoteExecutionException e) {
            System.out.println("Error Creating Client: " + e);
        } catch(IOException ex) {
            System.out.println("Error Reading Process Info: " + ex);
        }

        return returnStatus;
    }

    /** Runs a command on the local machine and returns true on zero exit.
     *  @param command The command and parameters of the command that it going
     *  to be executed on the local machine.
     *  @return True If the program exits with a zero value.
     */
    public boolean runCommand(String[] command) {
        _commands = command;
        while(!done);
        done = false;
        boolean returnStatus = false;
        int returnValue = 0;
        int exitValue = -1;
        String line = null;
        _commandResult = new StringBuffer();
        _errorMessage = new StringBuffer();

        if(_commands == null) {
            _errorMessage.append("Command was null, there was nothing to run.");
            return false;
        }

	String oneCommand = new String();

        try {
	    // Updated 04/07/2009. Problem executing commands in bash shell was resolved.
	    // Add "bash -c" at the start of the command and concatenate it into a one command,
	    // bash -c "<command>"

	    int length = Array.getLength(_commands);
	    String[] combinedCommand = new String[length + 2];
	    System.arraycopy(_commands, 0, combinedCommand, 1, length);
		/* for cygwin only */
	    //combinedCommand[0] = "bash -c \"";
	    //combinedCommand[length + 1] = "\"";

	    combinedCommand[0] = "";
	    combinedCommand[length + 1] = "";

	    for (int i = 0; i < length + 1; i++) {
		combinedCommand[i] += " ";
		oneCommand += combinedCommand[i];
	    }
	    oneCommand += combinedCommand[length + 1];

	    //	    System.out.println("oneCommand = " + oneCommand);

            _process = Runtime.getRuntime().exec(oneCommand);
        } catch(Exception e) {
            _errorMessage.append("\nException while executing command: "
                    + oneCommand + "\n" + e);
            return false;
        }
        _errThread.start();
        // read in the message
        BufferedReader msgReader = new BufferedReader(
                new InputStreamReader(_process.getInputStream()));
        try {
	    //WLP - may be a timing error without this fix
	    /*            char c = (char) msgReader.read();
            if (c != -1) {
		System.out.println("char: "+c);
                _commandResult.append(c);
		}*/
	    char c = (char)0;
            while(c != (char)-1) {
		//		System.out.println("Entering while");
		if (!msgReader.ready()) {
                    try {
                        // checks if the process is terminated
                        returnValue = _process.exitValue();
                        returnStatus = true;
                        // if terminated exit
                        break;
                    } catch (IllegalThreadStateException itse) {
                        try {
			    //			    System.out.println("Sleeping...");
                            Thread.sleep(100);
                        } catch (InterruptedException ex) {}
                    }
		} else {
                    c = (char) msgReader.read();
                    _commandResult.append(c);
		    //		    System.out.println("char: "+c + "(-1 = " + (char)-1 +")");
		}
	    }
	    //	    System.out.println("done with while");
        } catch(IOException ioe) {
            _errorMessage.append("Error running command: "
                    + _commands
                    + " : "
                    + ioe);
        } finally {
            _process.destroy();
            try {
                msgReader.close();
            } catch(IOException ie) {
                System.err.println("CommandUtil.runCommand() : Error closing "
                        + "InputStream " + ie);
            }
        }
        return returnStatus;
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private methods                     ////

    // a private class which implements the Runnable interface.
    private Runnable errorReader = new Runnable() {
        public void run() {
            String line = null;
            BufferedReader errorReaderBuffer = new BufferedReader(
                    new InputStreamReader(_process.getErrorStream()));
            try {
                char c = (char) errorReaderBuffer.read();
                if (c != -1) {
                    _errorMessage.append(c);
                }
                while(c != -1) {
                    if(!errorReaderBuffer.ready()) {
                        try {
                            // if the program was not terminated an
                            // IllegalThreadStateException is thrown
                            _process.exitValue();
                            break; // leaves the while
                        } catch (IllegalThreadStateException itse) {
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException ie) { }
                        }
                    } else {
                        c = (char) errorReaderBuffer.read();
                        _errorMessage.append(c);
                    }
                }
            } catch (IOException ioe) {
                _errorMessage.append("Error running command "
                        + _commands + " : " + ioe);
            } finally {
                try {
                    errorReaderBuffer.close();
                } catch (IOException ioe) {
                    System.err.println("CommandUtil.runCommand()"
                            + " : error closing error reader");
                }
            }
        done = true;
        }
    };

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    private StringBuffer _commandResult;
    private StringBuffer _errorMessage;
    private Thread _errThread = new Thread(errorReader, "Process error reader");
    private	Process _process;
    private String[] _commands;
    private static final byte _LF = 10;	// Terminator Line Feed byte
    public boolean done = true;
}


