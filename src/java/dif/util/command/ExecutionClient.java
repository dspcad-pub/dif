/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Client for remote execution of a command.
*/
package dif.util.command;

import java.net.Socket;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

//////////////////////////////////////////////////////////////////////////
//// CommandUtilities
/** The ExecutionClient encapsulates the spawning of the target application and
target to driver application communication.
<p>

@author Shahrooz Shahparnia based on a code by Mark Nadelson in the JavaPro
magazine. September 2002 issue.
@version $Id: ExecutionClient.java 606 2008-10-08 16:29:47Z plishker $
*/

public class ExecutionClient {

    /** Sets the execution of the target application in either attached or
     *  detached mode on a given host. It starts the communication.
     *  @param executionType The type of execution we want to have on the server
     *  which is either ATTACHED or DETACHED.
     *  @param host The host on which the ExecutionServer is running on.
     *  @param hostPort The port number which the ExecutionServer is listening
     *  to.
     *  @param args The command and parameters of the command that it going
     *  to be executed on the host.
     *  @exception RemoteExecutionException If the Client was not able to
     *  communicate with the server.
     */
    public ExecutionClient(int executionType, String host, int hostPort,
            String[] args) throws RemoteExecutionException {
        try {
        // Opens a socket connection to the ExecutionServer.
            Socket spawnSocket = new Socket(host, 6001);

        // Creates socket input and output streams
            dataOutputStream = new DataOutputStream(spawnSocket
                    .getOutputStream());
            dataInputStream = new DataInputStream(spawnSocket
                    .getInputStream());

        // Sends the command to spawn to the ExecutionServer.
        // This will be in either attached or detached mode.
            dataOutputStream.writeUTF(executionType == ATTACHED ?
                    "SPAWNATTACHED" : "SPAWNDETACHED");

        // Sends the number of parameters required to spawn the application.
            dataOutputStream.writeInt(args.length);

        // Sends the parameters
            for (int cmdCount = 0; cmdCount < args.length; cmdCount++) {
                dataOutputStream.writeUTF(args[cmdCount]);
            }

        // Wait for a response from the ExecutionServer.
        // If the response is not SPAWNEDOK an exception is thrown.
            String response = dataInputStream.readUTF();
            if (!response.equals("SPAWNEDOK")) {
                throw new RemoteExecutionException(
                        "Failure in the execution server: " + response);
            }
        } catch(IOException ex) {
            throw new RemoteExecutionException(
                    "Error Communicating With Execution Server At "
                    + host + ": ");
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Writes a buffer to the output stream ,
     *  terminated with the specified terminator to the output stream.
     *  @param byteBuffer The byte stream to be written.
     *  @param terminator The character to put at the end of the stream after
     *  writing the buffer to it.
     */
    public void write(byte[] byteBuffer, byte terminator) throws IOException {
        dataOutputStream.write(byteBuffer, 0, byteBuffer.length);
        dataOutputStream.write(terminator);
        dataOutputStream.flush();
    }


     /** Reads a block of data from the input stream into a given buffer.
      *  @param byteBuffer The given buffer.
      *  @param length The length of the block of data to be read.
      *  @returns The number of bytes read.
      */
    public int read(byte[] byteBuffer, int length) throws IOException {
        int numBytes = dataInputStream.read(byteBuffer, 0, length);
        return numBytes;
    }


     /** Read a stream of bytes from the input stream until the terminator
      *  is encountered.
      *  @param terminator The character to use as terminator in the input
      *  stream.
      *  @returns A String created from the stream of bytes.
      */
    public String read(byte terminator) throws IOException {
        byte byteBuf[] = new byte[1];
        StringBuffer data = null;
        int numBytes = 0;

      // Read 1 byte at a time until the stream is empty
        while((numBytes = dataInputStream.read(byteBuf, 0, 1)) > 0) {
        // Add the byte to the String buffer minus the terminator byte if
        // it is encountered.
            if (data == null) {
                data = new StringBuffer(new String(byteBuf, 0,
                        byteBuf[numBytes-1] == terminator
                        ? numBytes - 1 : numBytes));
            } else {
                data.append(new String(byteBuf, 0,
                        byteBuf[numBytes-1] == terminator
                        ? numBytes - 1 : numBytes));
            }

        // If the terminator byte is encountered return the resulting String
            if (byteBuf[numBytes-1] == terminator) {
               return data.toString();
            }
        }

      // If the input stream is empty return null
        return null;
    }

    /** Return the number of bytes available to read from the input stream.
     *  @return Return the number of bytes available to read from the input
     *  stream.
     */
    public int available() throws IOException {
        return dataInputStream.available();
    }

    /** Closes the socket input and output streams.
     *  @exception IOException If fails to close the streams.
     */
    public void close() throws IOException {
        dataOutputStream.close();
        dataInputStream.close();
    }

    /** Type of execution in which the executing command (target application)
     *  remains connected to the communication channel and sends/receives data
     *  to the ExecutionClient through the created socket.
     */
    public static final int ATTACHED = 0;

    /** Type of execution in which the executing command (target application)
     *  disconnects from the ExecutionServer, and does not communicate with
     *  the ExecutionClient. Though it executes on the server(host) and creates
     *  outputs there.
     */

    public static final int DETACHED = 1;

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    private DataInputStream dataInputStream;		// Socket input stream
    private DataOutputStream dataOutputStream;		// Socket output stream
}


