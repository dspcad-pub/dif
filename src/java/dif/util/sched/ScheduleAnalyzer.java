/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* An interface for all the scheduling strategies on graphs. */
package dif.util.sched;

import dif.util.graph.analysis.analyzer.GraphAnalyzer;

//////////////////////////////////////////////////////////////////////////
//// ScheduleAnalyzer

/**
 An interface for all the scheduling strategies on graphs.
 <p>
 @see GraphAnalyzer
 @author Shahrooz Shahparnia
 */
public interface ScheduleAnalyzer extends GraphAnalyzer {
    ///////////////////////////////////////////////////////////////////
    ////                         public classes                    ////

    /** Return the schedule computed by the associated analyzer.
     *
     *  @return Return the schedule computed by the associated analyzer.
     */
    public ScheduleTree schedule();
}
