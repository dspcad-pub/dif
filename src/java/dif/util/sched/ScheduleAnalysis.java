/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* An analysis for schedules on graphs.*/
package dif.util.sched;

import dif.util.graph.Graph;
import dif.util.graph.analysis.strategy.CachedStrategy;
import dif.util.graph.analysis.Analysis;
import dif.util.graph.analysis.analyzer.Analyzer;

//////////////////////////////////////////////////////////////////////////
//// ScheduleAnalysis

/**
 An analysis for schedules on graphs. The analyzer associate with this analysis
 which is supposed to implement the ScheduleAnalyzer interface generates a
 Schedule for a given Graph. The analyzer that for a given implementation would
 extend the Strategy ( or CachedStrategy) class contains the scheduling
 algorithm. Scheduling strategies can be dynamically changed, therefore dynamic
 schedulings are also supported.
 <p>
 @see Graph
 @see Analysis
 @see CachedStrategy
 @see ScheduleAnalysis
 @see ScheduleAnalyzer
 @author Shahrooz Shahparnia
 */
public class ScheduleAnalysis extends Analysis {
    /** Construct an instance of this class with the given analyzer.
     *
     *  @param analyzer The given analyzer.
     */
    public ScheduleAnalysis(ScheduleAnalyzer analyzer) {
        super(analyzer);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public classes                    ////

    /** Return the schedule computed by the associated analyzer.
     *
     *  @return Return the schedule computed by the associated analyzer.
     */
    public ScheduleTree schedule() {
        return ((ScheduleAnalyzer) analyzer()).schedule();
    }

    /** Check if a given analyzer is compatible with this analysis.
     *  In other words if it is possible to use it to compute the computation
     *  associated with this analysis.
     *  Derived classes should override this method to provide the valid type
     *  of analyzer that they need.
     *
     *  @param analyzer The given analyzer.
     *  @return True if the given analyzer is valid for this analysis.
     */
    public boolean validAnalyzerInterface(Analyzer analyzer) {
        return analyzer instanceof ScheduleAnalyzer;
    }
}
