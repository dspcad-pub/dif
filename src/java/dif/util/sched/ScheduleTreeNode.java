/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* The class for schedule tree node.*/

package dif.util.sched;

import dif.util.graph.Node;

//////////////////////////////////////////////////////////////////////////
//// ScheduleTreeNode

/** The class for schedule tree node.  This class is used to construct
 *  schedules along with ScheduleTree class.  If the schedule tree
 *  node represents a leaf node it has a reference to an actor. It
 *  just has an iteration count otherwise. The {@link #setActor}
 *  method is used to create the reference to an actor. The {@link #actor}
 *  method will return a reference to the actor referred to by the
 *  leaf node.
 *
 *  <p>This class has methods to access private variables associated
 *  with a schedule tree node.
 *
 *  @author Nimish Sane, Shuvra S. Bhattacharyya
 *  @version $Id: ScheduleTreeNode.java 416 2007-05-31 05:19:54Z plishker $
 *  @see ScheduleTree
 */
public class ScheduleTreeNode {
    /** Construct a schedule tree node.
     */
    public ScheduleTreeNode() {
	_actor = null;
	_loopCount = 1;
	_start = 0;
	_stop = 0;
	_currentLoopCount = 0;
	_guardedExecution = false;
    }

    /** Construct a schedule tree node corresponding to the given actor.
     *  @param actor The given actor.
     */
    public ScheduleTreeNode(Node actor) {
	_actor = actor;
	_loopCount = 1;
	_start = 0;
	_stop = 0;
	_currentLoopCount=0;
	_guardedExecution = false;
    }

    /** Construct a schedule tree node with given loop count.
     *  @param loopCount The given loop count.
     */
    public ScheduleTreeNode(int loopCount) {
	_actor = null;
	_loopCount = loopCount;
	_start = 0;
	_stop = 0;
	_currentLoopCount=0;
	_guardedExecution = false;
    }

    /** Construct a schedule tree node with given loop count and
     *  actor.
     *  @param loopCount A loop count.
     *  @param actor An actor node.
     */
    public ScheduleTreeNode(int loopCount, Node actor) {
	_actor = actor;
	_loopCount = loopCount;
	_start = 0;
	_stop = 0;
	_currentLoopCount=0;
	_guardedExecution = false;
    }

    // public ScheduleTreeNode() {
    //     super();
    // 	this._actor = null;
    // 	this._loopCount = 1;
    // 	this._duration = 0;
    // 	this._start = 0;
    // 	this._stop = 0;
    // 	this._currentLoopCount=0;
    // }

    // /** Construct a schedule tree node for given parent.
    //  *  @param parent The parent node.
    //  */
    // public ScheduleTreeNode(ScheduleTreeNode parent) {
    // 	super(parent);
    // 	this._actor = null;
    // 	this._loopCount = 1;
    // 	this._duration = 0;
    // 	this._start = 0;
    // 	this._stop = 0;
    // 	this._currentLoopCount = 0;
    // }

    // /** Construct a schedule tree node corresponding to the given actor.
    //  *  @param actor The given actor.
    //  */
    // public ScheduleTreeNode(Node actor) {
    // 	super();
    // 	this._actor = actor;
    // 	this._loopCount = 1;
    // 	this._duration = 0;
    // 	this._start = 0;
    // 	this._stop = 0;
    // 	this._currentLoopCount = 0;
    // }

    // /** Construct a schedule tree node corresponding to the given actor and computation.
    //  *  @param actor The given actor.
    //  *  @param computation the computation.
    //  */
    // public ScheduleTreeNode(Node actor, Object computation) {
    // 	super();
    // 	setComputation(computation);
    // 	this._actor = actor;
    // 	this._loopCount = 1;
    // 	this._duration = 0;
    // 	this._start = 0;
    // 	this._stop = 0;
    // 	this._currentLoopCount = 0;
    // }

    // /** Construct a schedule tree node corresponding to the given actor with a given parent node.
    //  *  @param actor The given actor.
    //  *  @param parent Then given parent.
    //  */
    // public ScheduleTreeNode(Node actor, ScheduleTreeNode parent) {
    // 	super(parent);
    // 	this._actor = actor;
    // 	this._loopCount = 1;
    // 	this._duration = 0;
    // 	this._start = 0;
    // 	this._stop = 0;
    // 	this._currentLoopCount = 0;
    // }

    // /** Construct a schedule tree node with given loop count.
    //  *  @param loopCount The given loop count.
    //  */
    // public ScheduleTreeNode(int loopCount) {
    // 	super();
    // 	this._actor = null;
    // 	this._loopCount = loopCount;
    // 	this._duration = 0;
    // 	this._start = 0;
    // 	this._stop = 0;
    // 	this._currentLoopCount = 0;
    // }

    // /** Construct a schedule tree node with given loop count and given parent.
    //  *  @param loopCount The given loop count.
    //  *  @param parent The given parent node.
    //  */
    // public ScheduleTreeNode(int loopCount, ScheduleTreeNode parent) {
    // 	super(parent);
    // 	this._actor = null;
    // 	this._loopCount = loopCount;
    // 	this._duration = 0;
    // 	this._start = 0;
    // 	this._stop = 0;
    // 	this._currentLoopCount = 0;
    // }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /*  Compute and set duration for given node.
     *  @return The node duration.
     */
    /*
    public int computeDuration() {
        if (this.isLeaf())
            this._duration = 1;
        else {
	    this._duration = 0;
	    Iterator iterChildren = this.getChildren().iterator();
	    while (iterChildren.hasNext()) {
		// The following line is giving null pointer exception and has been temporarily changed since, it is not critical right now.
		//		this._duration += ((ScheduleTreeNode)(iterChildren.next())).computeDuration();
		this._duration ++;
		// fix this.
		ScheduleTreeNode temp = (ScheduleTreeNode)iterChildren.next();
	    }
	    this._duration *= this._loopCount;
      	}
	return this._duration;
    }
    */

    /** Return the actor associated with the leaf schedule tree node.
     *  @return The corresponding actor.
     */
    public Node actor() {
        return _actor;
    }

    /** Return the current loop count associated with the schedule tree node.
     *  @return The current loop count.
     */
    public int currentLoopCount() {
        return _currentLoopCount;
    }

    /** Return the loop count associated with the schedule tree node.
     *  @return The loop count.
     */
    public int loopCount() {
        return _loopCount;
    }

    /** Return the start time associated with the schedule tree node.
     *  @return The start time.
     */
    public int startTime() {
        return _start;
    }

    /** Return the stop time associated with the schedule tree node.
     *  @return The stop time.
     */
    public int stopTime() {
        return _stop;
    }

    /** Returns if the execution of this node is guarded.
     *  @return true if the execution if guraded; false otherwise.
     */
    public boolean isGuardedExecution() {
	return _guardedExecution;
    }


    /** Increament the current loop count associated with the schedule tree node by 1.
     */
    public void increamentCurrentLoopCount() {
        this._currentLoopCount++;
    }

    /** Set the actor associated with the leaf schedule tree node.
     *  @param actor The corresponding actor.
     */
    public void setActor(Node actor) {
        this._actor = actor;
    }

    /** Set the current loop count associated with the schedule tree node to its default value.
     */
    public void setCurrentLoopCount() {
        this._currentLoopCount = 0;
    }

    /** Set the current loop count associated with the schedule tree node.
     *  @param count The given current loop count.
     */
    public void setCurrentLoopCount(int count) {
        this._currentLoopCount = count;
    }

    /** Set the loop count associated with the schedule tree node.
     *  @param loopCount The loop count.
     */
    public void setLoopCount(int loopCount) {
        this._loopCount = loopCount;
    }

    /** Set the start time associated with the schedule tree node.
     *  @param start The start time.
     */
    public void setStartTime(int start) {
        this._start = start;
    }

    /** Set the stop time associated with the schedule tree node.
     *  @param stop The stop time.
     */
    public void setStopTime(int stop) {
        this._stop = stop;
    }

    /** Set the execution type associated with the schedule tree node.
     *  @param flag Execution type: true if guarded, false otherwise.
     */
    public void setExecution(boolean flag) {
        this._guardedExecution = flag;
    }

    //NS:  Moved to ScheduleTree
    /** Print the firing in a parenthesis style. Return iteration count, if 
     *  it is an internal node. Return the firing element (actor) name, if
     *  it is a leaf node.
     *
     *  @param nameMap A mapping from firing element to its short name.
     *  @param delimiter The delimiter between iteration count and iterand.
     *  @return The parenthesis expression for this node.
     */
    /*    public String toParenthesisString(Map nameMap, String delimiter) {
	if (this.isLeaf()) {
	    String name = (String) nameMap.get(this._actor);
	   
	    if (this.isLastChild()) {
		ArrayList ancestors = getAncestors();
		ListIterator iterAncestors = ancestors.listIterator();
		String bracket = ")";

		while (iterAncestors.hasNext()) {
		    if (((ScheduleTreeNode)iterAncestors.next()).isLastChild()) {
			bracket += ")";
		    } else {
			break;
		    }
		}

		return name + bracket;

	    } else {
		return name;
	    }
	} else if (this._loopCount != 1) {
	    return "(" + this._loopCount + delimiter;
	} else {
	    return "(";
	}
    }
    */

    // /** Return a string representation of this actor Firing.
    //  *
    //  *  @return Return a string representation of this actor Firing.
    //  */
    // public String toString() {
    // 	if(this.isLeaf()) {
    // 	    String result = "Fire actor " + this._actor;
    // 	    if((ScheduleTreeNode)this.getParent() != null) {
    // 		int count = ((ScheduleTreeNode)this.getParent()).getLoopCount();

    // 		if (count > 1) {
    // 		    result += (" " + count + " times");
    // 		}
    // 	    }

    // 	    return result;
    // 	}

    // 	return " ";
    // }

    ///////////////////////////////////////////////////////////////////
    ////                     private methods                       ////

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    /** Associated actor with the leaf node
     */
    private Node _actor;

    /** Associated iteration (loop) count with the internal node
     */
    private int _loopCount;
    private int _currentLoopCount;

    /** Node start and stop times;
     */
    private int _start;
    private int _stop;

    /** Flag set to true if guarded execution.
     */
    private boolean _guardedExecution;
}
   



