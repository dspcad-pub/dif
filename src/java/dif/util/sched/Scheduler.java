/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A generalized schedule tree structure.*/

package dif.util.sched;

import java.util.Collection;
import java.util.Iterator;

import dif.util.graph.Node;
import dif.util.graph.Graph;

public class Scheduler {

    /** Constructor
     */
    public Scheduler(Graph g) {
	_graph = g;
    }

    public ScheduleTree schedule() {
	int count = 0;
	Collection nodeCol = _graph.nodes();
	Iterator nodeItr = nodeCol.iterator();
	_st = null;
	while(nodeItr.hasNext()) {
	    Node node = (Node)nodeItr.next();
	    // System.out.println("node" + count);
	    if(_st == null){
		_st = new ScheduleTree(node);
		/*System.out.println("node" + count);
		System.out.println(_st.nodeCount());
		System.out.println(node);*/
	    } else {
		_st.addScheduleElement(node);
		/*System.out.println("node" + count);
		System.out.println(_st.nodeCount());
		System.out.println(node);*/
	    }
	    count++;
	}
	//_st.printTree();
	return _st;
    }

    public Graph graph() {return _graph;};

    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    private Graph _graph;

    private ScheduleTree _st;

}
