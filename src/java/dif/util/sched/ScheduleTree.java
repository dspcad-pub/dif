/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A generalized schedule tree structure.*/

package dif.util.sched;

import java.util.Collection;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.ListIterator;

import dif.util.graph.Node;
import dif.util.graph.OrderedTree;

import dif.util.graph.GraphElementException;

//////////////////////////////////////////////////////////////////////////
//// ScheduleTree

/** A generalized schedule tree structure.

    <p>Schedule tree is an ordered tree, where all internal nodes
    represent schedule elements annotated with their iteration count
    in the schedule loop and leaves represent the actors. This
    structure generalizes the schedule tree representation previously
    developed for R-schedules.

    <p>This class represents a static schedule of firing elements
    invocation. An instance of this class is returned by the scheduler
    of a model to represent order of firing element firings in the
    model.

    <p>The addScheduleElement, and insertScheduleElement methods are
    used to add or remove schedule elements.

    <p>As an example, suppose that we have an SDF graph containing
    actors A, B, C, and D, with the firing order ABCBCBCDD. This
    firing order can be represented by a simple looped schedule. The
    code to create this schedule appears below.

    <p>
    <pre>
    ScheduleTree S = new ScheduleTree(A);
    ScheduleTree S1 = new ScheduleTree(B);
    S1.addScheduleElement(C);
    S.addScheduleElement(S1, 3);
    S.addScheduleElement(D, 2);
    </pre>
    <p>

    <p> If the schedule tree node represents a leaf node it has a
    reference to an actor. It just has an iteration count
    otherwise. The {@link #setActor} method is used to create the
    reference to an actor. The {@link #actor} method will return a
    reference to the actor referred to by the leaf node. Note that
    this implementation is not synchronized. It is therefore not safe
    for a thread to make modifications to the schedule structure while
    multiple threads are concurrently accessing the schedule.

    <h1>References</h1>

    @author Nimish Sane, Shuvra S. Bhattacharyya
    University of Maryland at College Park, based on a file by
    Shahrooz Shahparnia, Mingyung Ko.
    @version $Id: ScheduleTree.java 1634 2007-04-06 18:24:07Z ssb $
    @see ScheduleTreeNode
 */
public class ScheduleTree extends OrderedTree {

    /** Constructor
     */
    public ScheduleTree() {
	super();
	_treeWalker = null;
	//	_nodeInfoMap = new HashMap();
    }

    /** Constructor for a given actor.
     *  @param actor The given actor.
     */
    // public ScheduleTree(Node actor) {
    // 	super();

    // 	_treeWalker = null;
    // 	_nodeInfoMap = new HashMap();

    // 	Node root = new Node();
    // 	NodeInfo rootInfo = this.new NodeInfo(1);
    // 	_nodeInfoMap.put(root, rootInfo);

    // 	NodeInfo actorInfo = this.new NodeInfo(actor);
    // 	_nodeInfoMap.put(actor, actorInfo);

    // 	addNode(root);
    // 	addNode(actor);
    // 	addEdge(root, actor);
    // 	buildRootedTree(this, root);
    // 	_initializeTreeWalker();
    // }

    public ScheduleTree(Node actor) {
	super();

	_treeWalker = null;
	//	_nodeInfoMap = new HashMap();

	// Node root = new Node();
	// NodeInfo rootInfo = this.new NodeInfo(1);
	// _nodeInfoMap.put(root, rootInfo);
	ScheduleTreeNode rootWeight = new ScheduleTreeNode(1);
	Node root = new Node(rootWeight);

	// NodeInfo actorInfo = this.new NodeInfo(actor);
	// _nodeInfoMap.put(actor, actorInfo);
	ScheduleTreeNode nodeWeight = new ScheduleTreeNode(actor);
	Node node = new Node(nodeWeight);

	addNode(root);
	//	addNode(actor);
	addNode(node);
	//	addEdge(root, actor);
	addEdge(root, node);
	buildRootedTree(this, root);
	_initializeTreeWalker();
    }

    /** Constructor for a given actor and iteration loop count.
     *  @param actor The given actor.
     *  @param loopCount Loop count for the actor.
     */
    // public ScheduleTree(Node actor, int loopCount) {
    // 	super();

    // 	_treeWalker = null;
    // 	_nodeInfoMap = new HashMap();

    // 	Node root = new Node();
    // 	NodeInfo rootInfo = this.new NodeInfo(loopCount);
    // 	_nodeInfoMap.put(root, rootInfo);

    // 	NodeInfo actorInfo = this.new NodeInfo(actor);
    // 	_nodeInfoMap.put(actor, actorInfo);

    // 	addNode(root);
    // 	addNode(actor);
    // 	addEdge(root, actor);
    // 	buildRootedTree(this, root);
    // 	_initializeTreeWalker();
    // }

    public ScheduleTree(Node actor, int loopCount) {
	super();

	_treeWalker = null;
	//	_nodeInfoMap = new HashMap();

	// Node root = new Node();
	// NodeInfo rootInfo = this.new NodeInfo(loopCount);
	// _nodeInfoMap.put(root, rootInfo);

	ScheduleTreeNode rootWeight = new ScheduleTreeNode(loopCount);
	Node root = new Node(rootWeight);

	// NodeInfo actorInfo = this.new NodeInfo(actor);
	// _nodeInfoMap.put(actor, actorInfo);
	ScheduleTreeNode nodeWeight = new ScheduleTreeNode(actor);
	Node node = new Node(nodeWeight);


	addNode(root);
	//	addNode(actor);
	addNode(node);
	//	addEdge(root, actor);
	addEdge(root, node);
	buildRootedTree(this, root);
	_initializeTreeWalker();
    }


    /** Constructor for a given schedule tree.
     *  @param element The schedule element (schedule tree).
     */
    public ScheduleTree(ScheduleTree element) {
	super();

	_treeWalker = null;
	//	_nodeInfoMap = new HashMap();

	addGraph(element);
	buildRootedTree(this, element.root());

	//	_nodeInfoMap = element.nodeInfoMap();
	_initializeTreeWalker();
    }

    /** Constructor for a given schedule tree and its loop count.
     *  @param element The schedule element (schedule tree).
     *  @param loopCount Loop count of the schedule element.
     */
    public ScheduleTree(ScheduleTree element, int loopCount) {
	super();

	_treeWalker = null;
	//	_nodeInfoMap = new HashMap();

	addGraph(element);
	buildRootedTree(this, element.root());

	//	_nodeInfoMap = element.nodeInfoMap();
	//	setLoopCount(root(), loopCount * getLoopCount(root()));
	setLoopCount(root(), loopCount * loopCount(root()));

	_initializeTreeWalker();
    }

    /** Constructor for a given  {@link ScheduleElement}
     *  @param element {@link ScheduleElement}
     */
    public ScheduleTree(ScheduleElement element) {
	super();
	_constructScheduleTree(element);
	_initializeTreeWalker();
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /*  Compute and set duration for given node.
     *  @param node The given node.
     *  @return The node duration.
     */

    // public int computeDuration(Node node) {
    // 	if (isLeaf(node)) {
    // 	    ((NodeInfo)_nodeInfoMap.get(node))._duration = 1;
    // 	} else {
    // 	    ((NodeInfo)_nodeInfoMap.get(node))._duration = 0;
    // 	    Iterator iterChildren = children(node).iterator();
    // 	    while (iterChildren.hasNext()) {
    // 		Node nextNode = (Node)iterChildren.next();
    // 		((NodeInfo)_nodeInfoMap.get(node))._duration += computeDuration(nextNode);
    // 		//    ((NodeInfo)_nodeInfoMap.get(node))._duration += 1;
    // 	    }

    // 	    ((NodeInfo)_nodeInfoMap.get(node))._duration *= ((NodeInfo)_nodeInfoMap.get(node))._loopCount;
    // 	}

    // 	return ((NodeInfo)_nodeInfoMap.get(node))._duration;
    // }

    public int computeDuration(Node node) {
	int duration = 0;

	if (isLeaf(node)) {
	    duration = 1;
	} else {
	    Iterator iterChildren = children(node).iterator();
	    while (iterChildren.hasNext()) {
		Node nextNode = (Node)iterChildren.next();
		duration += computeDuration(nextNode);
	    }

	    duration *= loopCount(node);
	}

	return duration;
    }



    /** Return the actor associated with the schedule tree node.
     *
     *  @param node the node in the schedule tree
     *
     *  @return The corresponding actor.
     *
     *  @exception IllegalArgumentException If the spcified node is a null node.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     */
    public Node actor(Node node) {
	if (node == null) {
	    throw new IllegalArgumentException ("The specified node must not be null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

	//        return ((NodeInfo)_nodeInfoMap.get(node))._actor;
	return ((ScheduleTreeNode)node.getWeight()).actor();
    }

    /** Return the currentLoopCount associated with the schedule tree node.
     *
     *  @param node the node in the schedule tree
     *
     *  @return The corresponding currentLoopCount.
     *
     *  @exception IllegalArgumentException If the spcified node is a null node.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     */
    public int currentLoopCount(Node node) {
	if (node == null) {
	    throw new IllegalArgumentException ("The specified node must not be null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

	//        return ((NodeInfo)_nodeInfoMap.get(node))._currentLoopCount;
	return ((ScheduleTreeNode)node.getWeight()).currentLoopCount();
    }

    /** Return the loopCount associated with the schedule tree node.
     *
     *  @param node the node in the schedule tree
     *
     *  @return The corresponding loopCount.
     *
     *  @exception IllegalArgumentException If the spcified node is a null node.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     */
    public int loopCount(Node node) {
	if (node == null) {
	    throw new IllegalArgumentException ("The specified node must not be null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

	//        return ((NodeInfo)_nodeInfoMap.get(node))._loopCount;
	return ((ScheduleTreeNode)node.getWeight()).loopCount();
    }

    /** Return the startTime associated with the schedule tree node.
     *
     *  @param node the node in the schedule tree
     *
     *  @return The corresponding startTime.
     *
     *  @exception IllegalArgumentException If the spcified node is a null node.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     */
    public int startTime(Node node) {
	if (node == null) {
	    throw new IllegalArgumentException ("The specified node must not be null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

	//       return ((NodeInfo)_nodeInfoMap.get(node))._start;
	return ((ScheduleTreeNode)node.getWeight()).startTime();
    }

    /** Return the stopTime associated with the schedule tree node.
     *
     *  @param node the node in the schedule tree
     *
     *  @return The corresponding stopTime.
     *
     *  @exception IllegalArgumentException If the spcified node is a null node.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     */
    public int stopTime(Node node) {
	if (node == null) {
	    throw new IllegalArgumentException ("The specified node must not be null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

	//        return ((NodeInfo)_nodeInfoMap.get(node))._stop;
	return ((ScheduleTreeNode)node.getWeight()).stopTime();
    }


    /** Returns if the execution of this node is guarded.
     *
     *  @param node the node in the schedule tree
     *
     *  @return true if the execution if guraded; false otherwise.
     *
     *  @exception IllegalArgumentException If the spcified node is a null node.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     */
    public boolean isGuardedExecution(Node node) {
	if (node == null) {
	    throw new IllegalArgumentException ("The specified node must not be null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

	//        return ((NodeInfo)_nodeInfoMap.get(node))._guardedExecution;
	return ((ScheduleTreeNode)node.getWeight()).isGuardedExecution();
    }

    /** Increament the current loop count associated with the
     *  specified schedule tree node by 1.
     *
     *  @param node the node in the schedule tree
     *
     *  @exception IllegalArgumentException If the spcified node is a null node.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     */
    public void increamentCurrentLoopCount(Node node) {
	if (node == null) {
	    throw new IllegalArgumentException ("The specified node must not be null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

        // int count = getCurrentLoopCount(node);
	// setCurrentLoopCount(node, count + 1);
	((ScheduleTreeNode)node.getWeight()).increamentCurrentLoopCount();
    }

    /** Set the actor associated with the schedule tree node.
     *
     *  @param node a schedule tree node.
     *  @param actor The corresponding actor.
     *
     *  @exception IllegalArgumentException If the spcified node is a
     *  null node.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     */
    public void setActor(Node node, Node actor) {
	if (node == null) {
	    throw new IllegalArgumentException ("The specified node must not be null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

	//	NodeInfo info = (NodeInfo)_nodeInfoMap.get(node);
	//	info._actor = actor;
	//	_nodeInfoMap.put(node, info);
	((ScheduleTreeNode)node.getWeight()).setActor(actor);
    }

    /** Reset the currentLoopCount associated with the schedule tree
     *  node to its default value of <code>0</code>.
     *
     *  @param node a schedule tree node.
     *
     *  @exception IllegalArgumentException If the spcified node is a
     *  null node.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     */
    public void setCurrentLoopCount(Node node) {
	//	setCurrentLoopCount(node, 0);
	((ScheduleTreeNode)node.getWeight()).setCurrentLoopCount();
    }

    /** Set the currentLoopCount associated with the schedule tree node.
     *
     *  @param node a schedule tree node.
     *  @param currentLoopCount The corresponding currentLoopCount.
     *
     *  @exception IllegalArgumentException If the spcified node is a
     *  null node or the currentLoopCount is negative.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     */
    public void setCurrentLoopCount(Node node, int currentLoopCount) {
	if (node == null) {
	    throw new IllegalArgumentException ("The specified node must not be null. \n");
	}

	if (currentLoopCount < 0) {
	    throw new IllegalArgumentException ("The specified loop count must be non-negative. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

	// NodeInfo info = (NodeInfo)_nodeInfoMap.get(node);
	// info._currentLoopCount = currentLoopCount;
	// _nodeInfoMap.put(node, info);

	((ScheduleTreeNode)node.getWeight()).setCurrentLoopCount(currentLoopCount);
    }

    /** Set the loopCount associated with the schedule tree node.
     *
     *  @param node a schedule tree node.
     *  @param loopCount The corresponding loopCount.
     *
     *  @exception IllegalArgumentException If the spcified node is a
     *  null node or the loopCount is negative.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     */
    public void setLoopCount(Node node, int loopCount) {
	if (node == null) {
	    throw new IllegalArgumentException ("The specified node must not be null. \n");
	}

	if (loopCount < 0) {
	    throw new IllegalArgumentException ("The specified loop count must be non-negative. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

	// NodeInfo info = (NodeInfo)_nodeInfoMap.get(node);
	// info._loopCount = loopCount;
	// _nodeInfoMap.put(node, info);

	((ScheduleTreeNode)node.getWeight()).setLoopCount(loopCount);
    }

    /** Set the startTime associated with the schedule tree node.
     *
     *  @param node a schedule tree node.
     *  @param start The corresponding startTime.
     *
     *  @exception IllegalArgumentException If the spcified node is a
     *  null node or the start is negative.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     */
    public void setStartTime(Node node, int start) {
	if (node == null) {
	    throw new IllegalArgumentException ("The specified node must not be null. \n");
	}

	if (start < 0) {
	    throw new IllegalArgumentException ("The specified start time must be non-negative. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

	// NodeInfo info = (NodeInfo)_nodeInfoMap.get(node);
	// info._start = start;
	// _nodeInfoMap.put(node, info);

	((ScheduleTreeNode)node.getWeight()).setStartTime(start);
    }

    /** Set the stopTime associated with the schedule tree node.
     *
     *  @param node a schedule tree node.
     *  @param stop The corresponding stop time.
     *
     *  @exception IllegalArgumentException If the spcified node is a
     *  null node or the stop is negative.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     */
    public void setStopTime(Node node, int stop) {
	if (node == null) {
	    throw new IllegalArgumentException ("The specified node must not be null. \n");
	}

	if (stop < 0) {
	    throw new IllegalArgumentException ("The specified stop time must be non-negative. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

	// NodeInfo info = (NodeInfo)_nodeInfoMap.get(node);
	// info._stop = stop;
	// _nodeInfoMap.put(node, info);

	((ScheduleTreeNode)node.getWeight()).setStopTime(stop);
    }

    /** Sets the flag to indicated guarded execution.
     *
     *  @param node a schedule tree node.
     *  @param flag a boolean flag, true if guarded execution; false
     *  otherwise.
     *
     *  @exception IllegalArgumentException If the spcified node is a
     *  null node or the start is negative.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     */
    public void setExecution(Node node, boolean flag) {
	if (node == null) {
	    throw new IllegalArgumentException ("The specified node must not be null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

	// NodeInfo info = (NodeInfo)_nodeInfoMap.get(node);
	// info._guardedExecution = flag;
	// _nodeInfoMap.put(node, info);

	((ScheduleTreeNode)node.getWeight()).setExecution(flag);
    }

    /** Get loop count of the given schedule tree.
     *  @return The loop count
     */
    public int getTreeLoopCount() {
	//	return getLoopCount(root());
	return loopCount(root());
    }

    /** Get tree walker which points to the current leaf schedule tree
     *  node.  The tree walker returns to its initial position when it
     *  has finished traversing the entire schedule tree once.
     *  @return The tree walker.
     */
    public Node getCurrentTreeWalker() {
	return _treeWalker;
    }

    /** Advance the tree walker so that it points to the next leaf
     *  node in the schedule and return the same.  The tree walker
     *  returns to its initial position when it has finished
     *  traversing the entire scheudle once.
     *  @return The next tree walker element.
     */
    // public Node getNextTreeWalker() {
    // 	if(_treeWalker != null) {
    // 	    if (isLeaf(_treeWalker)) {
    // 		while (isLastChild(_treeWalker)) {
    // 		    _treeWalker = parent(_treeWalker);
    //                 increamentCurrentLoopCount(_treeWalker);
    // 		    if (getCurrentLoopCount(_treeWalker) < getLoopCount(_treeWalker)) {
    // 			_treeWalker = leftmostLeaf(_treeWalker);
    // 			return _treeWalker;
    // 		    } else {
    // 			setCurrentLoopCount(_treeWalker, 0);
    // 		    }
    // 		}

    // 		if (_treeWalker == root()) {
    // 		    _initializeTreeWalker();
    // 		} else {
    // 		    _treeWalker = leftmostLeaf(rightSibling(_treeWalker));
    // 		}

    // 		return _treeWalker;
    // 	    }

    // 	    return null;
    // 	}

    // 	_treeWalker = leftmostLeaf(root());

    // 	return _treeWalker;
    // }

    public Node getNextTreeWalker() {
	if(_treeWalker != null) {
	    if (isLeaf(_treeWalker)) {
		while (isLastChild(_treeWalker)) {
		    _treeWalker = parent(_treeWalker);
                    increamentCurrentLoopCount(_treeWalker);
		    if (currentLoopCount(_treeWalker) < loopCount(_treeWalker)) {
			_treeWalker = leftmostLeaf(_treeWalker);
			return _treeWalker;
		    } else {
			setCurrentLoopCount(_treeWalker, 0);
		    }
		}

		if (_treeWalker == root()) {
		    _initializeTreeWalker();
		} else {
		    _treeWalker = leftmostLeaf(rightSibling(_treeWalker));
		}

		return _treeWalker;
	    }

	    return null;
	}

	_treeWalker = leftmostLeaf(root());

	return _treeWalker;
    }

    /** Get the lexical order of firing elements. Only single appearance
     *  schedules are qualified for this operation. It's meaningless to
     *  have the order for multiple appearance schedules.
     *
     *  @return The lexical order of firing elements.
     *  @exception IllegalStateException If this is not a single
     *  appearance schedule.
     */
    public List lexicalOrder() {
        List lexicalList = new ArrayList();
	_initializeTreeWalker();
	_treeWalker = leftmostLeaf(root());
	lexicalList.add(_treeWalker);
	while((_treeWalker != null) && (_treeWalker != root())) {
	    while (isLastChild(_treeWalker)) {
		_treeWalker = parent(_treeWalker);
	    }

	    _treeWalker = leftmostLeaf(rightSibling(_treeWalker));
	    lexicalList.add(_treeWalker);
	}

        return Collections.unmodifiableList(lexicalList);
    }

    /** Return the parenthesized schedule to be printed and print the
     *  same.
     *  @param nameMap A mapping from an actor to its short name.
     *  @param delimiter The delimiter character to be used.
     *  @return The schedule to be printed.
     */
    public String printSchedule(Map nameMap, String delimiter) {
	String schedule = null;
	Node node = root();
	while (successor(node) != null) {
	    schedule += toParenthesisString(node, nameMap, delimiter);
	    node = successor(node);
	}

	System.out.println(schedule);
	return schedule;
    }

    /** Print the schedule tree information.  It prints label, actor,
     *  loop count, parent, children associated with each node in the
     *  schedule tree.
     *  @return the string that contains all the schedule tree information.
     */
    // public String printTree() {
    // 	Collection nodeL = nodes();
    // 	Iterator iter = nodeL.iterator();
    // 	String print = new String();
    // 	while(iter.hasNext()) {
    // 	    Node node = (Node)iter.next();
    // 	    print += "NodeLabel: " + nodeLabel(node) + "\n";
    // 	    print += "NodeActor: " + getActor(node) + "\n";
    // 	    print += "Loopcount: " + getLoopCount(node) + "\n";

    // 	    if (parent(node) == null) {
    // 		print += "Parent: Null \n";
    // 	    } else {
    // 		print += "Parent: Node # " + nodeLabel(parent(node)) + "\n";
    // 	    }

    // 	    print += "Children: " + childrenCount(node) + "\n";
    // 	    LinkedList children = children(node);

    // 	    for(int i = 0; i < children.size(); i++) {
    // 		if (children.get(i) != null)
    // 		    print += "child # " + i + " : Node # " + nodeLabel((Node)children.get(i)) + "\n";
    // 	    }

    // 	    print += "\n";
    // 	}

    // 	System.out.println("***************** Printing Tree Information *******************");
    // 	System.out.println(print);
    // 	System.out.println("****************** End of Tree Information ********************");

    // 	return print;
    // }

    public String printTree() {
	Collection nodeL = nodes();
	Iterator iter = nodeL.iterator();
	String print = new String();
	while(iter.hasNext()) {
	    Node node = (Node)iter.next();
	    print += "NodeLabel: " + nodeLabel(node) + "\n";
	    print += "NodeActor: " + actor(node) + "\n";
	    print += "Loopcount: " + loopCount(node) + "\n";

	    if (parent(node) == null) {
		print += "Parent: Null \n";
	    } else {
		print += "Parent: Node # " + nodeLabel(parent(node)) + "\n";
	    }

	    print += "Children: " + childrenCount(node) + "\n";
	    LinkedList children = children(node);

	    for(int i = 0; i < children.size(); i++) {
		if (children.get(i) != null)
		    print += "child # " + i + " : Node # " + nodeLabel((Node)children.get(i)) + "\n";
	    }

	    print += "\n";
	}

	System.out.println("***************** Printing Tree Information *******************");
	System.out.println(print);
	System.out.println("****************** End of Tree Information ********************");

	return print;
    }

    /** Set loop count of the given schedule tree.
     *  @param loopCount Given loop count
     *  @exception IllegalArgumentException If the spcified loop count is negative.
     */
    public void setTreeLoopCount(int loopCount) {
	if (loopCount < 0) {
	    throw new IllegalArgumentException ("The specified loop count must be non-negative. \n");
	}

	// int treeLoopCount = getLoopCount(root());
	int treeLoopCount = loopCount(root());

	setLoopCount(root(), treeLoopCount * loopCount);
    }

    /** Return a string representation of this actor Firing.
     *
     *  @return Return a string representation of this actor
     *  Firing.
     */
    // public String toString(Node node) {
    // 	if (isLeaf(node)) {
    // 	    String result = "Fire actor " + getActor(node).toString();
    // 	    if (parent(node) != null) {
    // 		int count = getLoopCount(parent(node));

    // 		if (count > 1) {
    // 		    result += (" " + count + " times");
    // 		}
    // 	    }

    // 	    return result;
    // 	}

    // 	return " ";
    // }

    public String toString(Node node) {
	if (isLeaf(node)) {
	    String result = "Fire actor " + actor(node).toString();
	    if (parent(node) != null) {
		int count = loopCount(parent(node));

		if (count > 1) {
		    result += (" " + count + " times");
		}
	    }

	    return result;
	}

	return " ";
    }

    /** Print the firing in a parenthesis style. Return iteration
     *  count, if the node is an internal node. Return the firing
     *  element (actor) name, if the node is a leaf node.
     *
     *  @param node the schedule tree node.
     *  @param nameMap A mapping from firing element to its short
     *  name.
     *  @param delimiter The delimiter between iteration count and
     *  iterand.
     *  @return The parenthesis expression for this node.
     */
    public String toParenthesisString(Node node, Map nameMap, String delimiter) {
	if (isLeaf(node)) {
	    //	    String name = (String)nameMap.get(getActor(node));
	    String name = (String)nameMap.get(actor(node));

	    if (isLastChild(node)) {
		ArrayList ancestors = ancestors(node);
		ListIterator iterAncestors = ancestors.listIterator();
		String bracket = ")";

		while (iterAncestors.hasNext()) {
		    Node temp = (Node)iterAncestors.next();
		    if (isLastChild(temp)) {
			bracket += ")";
		    } else {
			break;
		    }
		}

		return name + bracket;

	    } else {
		return name;
	    }
	// } else if (getLoopCount(node) != 1) {
	//     return "(" + getLoopCount(node) + delimiter;
	} else if (loopCount(node) != 1) {
	    return "(" + loopCount(node) + delimiter;
	} else {
	    return "(";
	}
    }

    /** Get {@link Schedule} representation.
     *  This method is currently supported for easy portability between both the representations.
     *  {@link ScheduleTree} should eventually replace any other representations.
     *  @return {@link Schedule} representation.
     */
    public Schedule toSchedule() {
    	Schedule schedule =  new Schedule();
    	Node root = root();
    	if (root != null) {
    	    schedule.setIterationCount(getTreeLoopCount());
    	    ListIterator iter = childrenIterator(root);
    	    while (iter.hasNext()) {
    		Node child = (Node)iter.next();
    		if (isLeaf(child)) {
    		    Firing firing = new Firing();
		    //    		    firing.setFiringElement(getActor(child));
		    firing.setFiringElement(actor(child));
    		    schedule.add(firing);
    		} else {
    		    Schedule sched = new Schedule();
    		    sched = ((ScheduleTree)subtree(child)).toSchedule();
    		    schedule.add(sched);
    		}
    	    }
    	}

    	return schedule;
    }

    /** Add the given actor to the existing schedule tree.  If the
     *  current schedule tree has a loop count of 1, the new actor is
     *  added as the last child of the root node.  Otherwise, a new
     *  schedule tree node with a loop count of 1 is created and added
     *  as a root.  The previous and new node are then added as
     *  children of the new root node.  If the current tree (and root)
     *  are null, the new node replaces the null tree.
     *  @param actor The given actor.
     *  @exception IllegalArgumentException If actor is null.
     */
    // public void addScheduleElement(Node actor) {
    // 	if (actor == null) {
    // 	    throw new IllegalArgumentException ("Tryingt to add null actor. \n");
    // 	}

    // 	NodeInfo actorInfo = this.new NodeInfo(actor);
    // 	_nodeInfoMap.put(actor, actorInfo);

    // 	if (this == null) {
    // 	    addRoot(actor);
    // 	} else {
    // 	    if (getLoopCount( root() ) > 1) {
    // 		Node root = new Node();
    // 		NodeInfo rootInfo = this.new NodeInfo(1);
    // 		_nodeInfoMap.put(root, rootInfo);

    // 		addRoot(root);
    // 	    }

    // 	    addChildLast(root(), actor);
    // 	}

    // 	_initializeTreeWalker();
    // }

    public void addScheduleElement(Node actor) {
	if (actor == null) {
	    throw new IllegalArgumentException ("Tryingt to add null actor. \n");
	}

	// NodeInfo actorInfo = this.new NodeInfo(actor);
	// _nodeInfoMap.put(actor, actorInfo);
	ScheduleTreeNode newWeight = new ScheduleTreeNode(actor);
	Node newNode = new Node(newWeight);

	if (this == null) {
	    //	    addRoot(actor);
	    addRoot(newNode);
	} else {
	    if (loopCount( root() ) > 1) {
		//		Node root = new Node();
		// NodeInfo rootInfo = this.new NodeInfo(1);
		// _nodeInfoMap.put(root, rootInfo);

		ScheduleTreeNode rootWeight = new ScheduleTreeNode(1);
		Node root = new Node(rootWeight);

		addRoot(root);
	    }

	    addChildLast(root(), actor);
	}

	_initializeTreeWalker();
    }

    /** Add the given actor, with given loop count to the existing
     *  schedule tree.  If the loop count is 1, the result is the same
     *  as that of {@link #addScheduleElement(Node)} method.
     *  @param actor The given actor.
     *  @param loopCount The loop count for the actor.
     *  @exception IllegalArgumentException If the actor is null or
     *  the loop count is negative.
     */
    public void addScheduleElement(Node actor, int loopCount) {
	if (actor == null) {
	    throw new IllegalArgumentException ("Trying to add null actor");
	}

	if (loopCount < 0) {
	    throw new IllegalArgumentException ("Trying to set negative loop count. \n");
	}

	if (loopCount == 1) {
	    addScheduleElement(actor);
	} else {
	    ScheduleTree element = new ScheduleTree(actor, loopCount);
	    addScheduleElement(element);
	}
    }

    /** Add the given schedule element to the existing schedule tree.
     *  If the gcd of loop counts of schedule element being added and
     *  the current schedule tree is 1, and the current loop count is
     *  greater than 1, then a new root with loop count of 1 is
     *  created and the current schedule tree and the one being added,
     *  are set as children of this new root.
     *
     *  If the gcd of loop counts of schedule element being added and
     *  the current schedule tree is 1, and the current loop count is
     *  1, then the schedule tree being added is added the last child
     *  of the current root node.
     *
     *  If the gcd of loop counts of schedule element being added and
     *  the current schedule tree is greater than 1, then a new root
     *  with loop count of gcd of both loop counts is created and the
     *  current schedule tree and the one being added, are set as
     *  children of this new root with modified loop counts.
     *
     *  If the current tree is null, the new tree replaces the current
     *  tree.
     *
     *  @param element The given element.
     *  @exception IllegalArgumentException if the given element is
     *  null.
     */
    // public void addScheduleElement(ScheduleTree element) {
    //     if (element == null) {
    // 	    throw new IllegalArgumentException ("Attempt to add a null schedule tree. \n");
    // 	}

    // 	if (this == null) {
    // 	    addOrderedTree(element);
    // 	} else {
    // 	    int presentLoopCount = getLoopCount(root());
    // 	    int loopCount = getLoopCount(element.root());
    // 	    int gcd = _gcd(presentLoopCount, loopCount);

    // 	    if (gcd == 1) {
    // 		if (presentLoopCount > 1) {
    // 		    Node root = new Node();
    // 		    NodeInfo rootInfo = this.new NodeInfo(1);
    // 		    _nodeInfoMap.put(root, rootInfo);

    // 		    addRoot(root);
    // 		}
    //  	    } else {
    // 		setLoopCount(root(), presentLoopCount / gcd);
    // 		setLoopCount(element.root(), loopCount / gcd);

    // 		Node root = new Node();
    // 		NodeInfo rootInfo = this.new NodeInfo(gcd);
    // 		_nodeInfoMap.put(root, rootInfo);

    // 		addRoot(root);
    // 	    }

    // 	    addOrderedTree(element, root());
    // 	}

    // 	_nodeInfoMap.putAll(element.nodeInfoMap());
    // 	_initializeTreeWalker();
    // }

    public void addScheduleElement(ScheduleTree element) {
        if (element == null) {
	    throw new IllegalArgumentException ("Attempt to add a null schedule tree. \n");
	}

	if (this == null) {
	    addOrderedTree(element);
	} else {
	    int presentLoopCount = loopCount(root());
	    int loopCount = loopCount(element.root());
	    int gcd = _gcd(presentLoopCount, loopCount);

	    if (gcd == 1) {
		if (presentLoopCount > 1) {
		    // Node root = new Node();
		    // NodeInfo rootInfo = this.new NodeInfo(1);
		    // _nodeInfoMap.put(root, rootInfo);
		    ScheduleTreeNode rootWeight = new ScheduleTreeNode(1);
		    Node root = new Node(rootWeight);

		    addRoot(root);
		}
     	    } else {
		setLoopCount(root(), presentLoopCount / gcd);
		setLoopCount(element.root(), loopCount / gcd);

		// Node root = new Node();
		// NodeInfo rootInfo = this.new NodeInfo(gcd);
		// _nodeInfoMap.put(root, rootInfo);

		ScheduleTreeNode rootWeight = new ScheduleTreeNode(gcd);
		Node root = new Node(rootWeight);

		addRoot(root);
	    }

	    addOrderedTree(element, root());
	}

	//	_nodeInfoMap.putAll(element.nodeInfoMap());
	_initializeTreeWalker();
    }

    /** Add the given schedule element with its loop count to the
     *  existing schedule tree.  This method first sets the loop count
     *  of the element being added to the new value and then adds the
     *  element with this value of loop count to the current schedule
     *  tree.
     *  @param element The given element.
     *  @param loopCount The given loopCount.
     *  @exception IllegalArgumentException if the given element is
     *  null.
     *  @exception IllegalArgumentException if the loop count is negative.
     */
    public void addScheduleElement(ScheduleTree element, int loopCount) {
        if (element == null || loopCount < 0) {
	    throw new IllegalArgumentException ("Invalid input arguments. \n");
	}

	element.setTreeLoopCount(loopCount);
	addScheduleElement(element);
    }

    /** Insert the given actor as the last child of the root of the
     *  existing schedule tree.
     *  @param actor The given actor.
     *  @exception IllegalArgumentException If the actor is null.
     */
    // public void insertScheduleElement(Node actor) {
    // 	if (actor == null) {
    // 	    throw new IllegalArgumentException ("Trying to add null actor. \n");
    // 	}

    // 	NodeInfo actorInfo = this.new NodeInfo(actor);
    // 	_nodeInfoMap.put(actor, actorInfo);

    // 	// Added if condition because addChildLast will not work
    // 	// if the current tree (and its root) are null.
    // 	if (this == null) {
    // 	    addRoot(actor);
    // 	} else {
    // 	    addChildLast(root(), actor);
    // 	}

    // 	_initializeTreeWalker();
    // }

    public void insertScheduleElement(Node actor) {
    	if (actor == null) {
	    throw new IllegalArgumentException ("Trying to add null actor. \n");
	}

	// NodeInfo actorInfo = this.new NodeInfo(actor);
	// _nodeInfoMap.put(actor, actorInfo);
	ScheduleTreeNode nodeWeight = new ScheduleTreeNode(actor);
	Node node = new Node(nodeWeight);

	// Added if condition because addChildLast will not work
	// if the current tree (and its root) are null.
	if (this == null) {
	    //	    addRoot(actor);
	    addRoot(node);
	} else {
	    //	    addChildLast(root(), actor);
	    addChildLast(root(), node);
	}

	_initializeTreeWalker();
    }

    /** Insert the given actor, with given loop count as the last
     *  child of the root of the existing schedule tree.
     *  @param actor The given actor.
     *  @param loopCount The loop count for the actor.
     *  @exception IllegalArgumentException If the actor is null or
     *  the loop count is negative.
     */
    public void insertScheduleElement(Node actor, int loopCount) {
	if (actor == null || loopCount < 0) {
	    throw new IllegalArgumentException("Trying to add null actor or actor with negative loop count. \n");
	}

	if (loopCount == 1) {
	    addScheduleElement(actor);
	} else {
	    ScheduleTree element  = new ScheduleTree(actor, loopCount);

	    // Added if condition because addOrderedTree will not work
	    // if the current tree (and its root) are null.
	    if (this == null) {
		addOrderedTree(element);
	    } else {
		addOrderedTree(element, root());
	    }

	    //	    _nodeInfoMap.putAll(element.nodeInfoMap());
	    _initializeTreeWalker();
	}
    }

    /** Insert the given schedule element as the last child of the
     *  root of the existing schedule tree.
     *  @param element The given element.
     *  @exception IllegalArgumentException If the given element is
     *  null.
     */
    public void insertScheduleElement(ScheduleTree element) {
        if ( element == null) {
	    throw new IllegalArgumentException("Trying to add a null schedule tree. \n");
	}

	// Added if condition because addOrderedTree will not work is
	// the current tree (and its root) are null.
	if (this == null) {
	    addOrderedTree(element);
	} else {
	    addOrderedTree(element, root());
	}

	//	_nodeInfoMap.putAll(element.nodeInfoMap());
	_initializeTreeWalker();
    }

    /** Insert the given schedule element with its loop count as the
     *  last child of the root of the existing schedule tree.
     *  @param element The given element.
     *  @param loopCount The given loopCount.
     *  @exception IllegalArgumentException if the element is null or
     *  the loop count is negative.
     */
    public void insertScheduleElement(ScheduleTree element, int loopCount) {
        if (element == null || loopCount < 0) {
	    throw new IllegalArgumentException("Invalid input arguments. \n");
	}

 	if (loopCount == 1) {
	    addScheduleElement(element);
	} else {
	    element.setTreeLoopCount(loopCount);
	    insertScheduleElement(element);
	}
    }

    // /** Return the node information map.
    //  *  @return the <code>_nodeInfoMap</code>
    //  */
    // HashMap nodeInfoMap() {
    // 	return _nodeInfoMap;
    // }

   /** Compute and set Start and Stop times for schedule tree nodes starting with given node.
     *  @param node The given schedule tree node.
     *  @param start A given start time.
     */
    public void computeNodeStartStop(Node node, int start) {
        setStartTime(node, start);
	int stop = start + computeDuration(node);
	setStopTime(node, stop);
        if (!isLeaf(node)) {
	    Iterator iterChildren = children(node).iterator();
	    while (iterChildren.hasNext()) {
		Node nextNode = (Node)iterChildren.next();
		computeNodeStartStop(nextNode, start);
		//		start += getStopTime(nextNode);
		start += stopTime(nextNode);
	    }
	}
    }

    /*  Compute and set Start and Stop times for all the schedule tree
     *  nodes
     *  @param start A given start time.
     */
    public void computeNodeStartStopAll(int start) {
	computeDuration(root());
	computeNodeStartStop(root(), start);
    }

    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////

    /** This method sets the _treeWalker to its default value and also
     *  resets currentLoopCount of all the nodes to zero.
     */
    private void _initializeTreeWalker() {
	_treeWalker = null;
	this._resetCurrentLoopCount(root());
    }

    /** Reset the current loop counts of all the nodes in the subtree
     *  rooted at the given node.
     *  @param node The root node of the subtree that needs to be reset.
     */
    private void _resetCurrentLoopCount(Node node) {
	Node lastNode = rightmostLeaf(node);
 	Node nextRoot = successor(lastNode);

	do {
	    setCurrentLoopCount(node);
	    node = successor(node);
	} while (node != nextRoot);
    }

    /** Function to evaluate GCD of two numbers
     *  @param a number a
     *  @param b number b
     *  @return gcd(a,b)
     */
    private int _gcd(int a, int b) {
	if (b == 0) {
	    return a;
	} else {
	    return _gcd(b, a % b);
	}
    }

    /** This method constructs a schedule tree from the given
	{@link ScheduleElement}.
     *  @param element {@link ScheduleElement}
     */
    // private void _constructScheduleTree(ScheduleElement element) {
    // 	if (element instanceof Schedule) {
    // 	    // element is instance of Schedule
    // 	    Node node = new Node();
    // 	    addRoot(node);

    // 	    NodeInfo nodeInfo = this.new NodeInfo(((Schedule)element).getIterationCount());
    // 	    _nodeInfoMap.put(node, nodeInfo);

    // 	    Iterator iter = ((Schedule)element).iterator();
    // 	    while (iter.hasNext()) {
    // 		ScheduleElement nextElement = (ScheduleElement)iter.next();
    // 		ScheduleTree st = new ScheduleTree(nextElement);
    // 		this.insertScheduleElement(st);
    // 	    }
    // 	} else {
    // 	    // element is instance of Firing
    // 	    Node node = (Node)((Firing)element).getFiringElement();
    // 	    addRoot(node);
    // 	}
    // }

    private void _constructScheduleTree(ScheduleElement element) {
	if (element instanceof Schedule) {
	    // element is instance of Schedule
	    //    Node node = new Node();
	    //	    addRoot(node);
	    //	    NodeInfo nodeInfo = this.new NodeInfo(((Schedule)element).getIterationCount());
	    //	    _nodeInfoMap.put(node, nodeInfo);

	    ScheduleTreeNode rootWeight = new ScheduleTreeNode(((Schedule)element).getIterationCount());
	    Node root = new Node(rootWeight);
	    addRoot(root);

	    Iterator iter = ((Schedule)element).iterator();
	    while (iter.hasNext()) {
		ScheduleElement nextElement = (ScheduleElement)iter.next();
		ScheduleTree st = new ScheduleTree(nextElement);
		this.insertScheduleElement(st);
	    }
	} else {
	    // element is instance of Firing
	    Node node = (Node)((Firing)element).getFiringElement();
	    addRoot(node);
	}
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    /** The tree walker which points to the current leaf node. */
    private Node _treeWalker;

    // /** A map between the nodes of the schedule tree and the
    //  * corresponding information associated with them. */
    // private HashMap _nodeInfoMap;

    // /** An inner class to store the information associated with a node
    //  *  in the schedule tree. */
    // private class NodeInfo {
    // 	/** Constructors */

    // 	/** Construct a schedule tree node.
    // 	 */
    // 	public NodeInfo() {
    // 	    _actor = null;
    // 	    _loopCount = 1;
    // 	    _duration = 0;
    // 	    _start = 0;
    // 	    _stop = 0;
    // 	    _currentLoopCount = 0;
    // 	    _guardedExecution = false;
    // 	}

    // 	/** Construct a schedule tree node corresponding to the given actor.
    // 	 *  @param actor The given actor.
    // 	 */
    // 	public NodeInfo(Node actor) {
    // 	    _actor = actor;
    // 	    _loopCount = 1;
    // 	    _duration = 0;
    // 	    _start = 0;
    // 	    _stop = 0;
    // 	    _currentLoopCount=0;
    // 	    _guardedExecution = false;
    // 	}

    // 	/** Construct a schedule tree node with given loop count.
    // 	 *  @param loopCount The given loop count.
    // 	 */
    // 	public NodeInfo(int loopCount) {
    // 	    _actor = null;
    // 	    _loopCount = loopCount;
    // 	    _duration = 0;
    // 	    _start = 0;
    // 	    _stop = 0;
    // 	    _currentLoopCount=0;
    // 	    _guardedExecution = false;
    // 	}

    // 	/* Public members */
    // 	/** Associated actor with the leaf node
    // 	 */
    // 	public Node _actor;

    // 	/** Associated iteration (loop) count with the internal node
    // 	 */
    // 	public int _loopCount;
    // 	public int _currentLoopCount;

    // 	/** Duration of the node
    // 	 */
    // 	public int _duration;

    // 	/** Node start and stop times;
    // 	 */
    // 	public int _start;
    // 	public int _stop;

    // 	/** Flag set to true if guarded execution.
    // 	 */
    // 	public boolean _guardedExecution;
    // }
}
