/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/** A Forest.*/
package dif.util.graph;

import java.util.ArrayList;

//////////////////////////////////////////////////////////////////////////
//// Forest.java

/**
 A Forest.

 Forest is an undirected acyclic graph that is possibly disconnected.
 The graphs created by this class must be undirected acyclic and disconnected. 
 The class Tree handles the case for undirected acyclic connected graphs.

 @author Nimish Sane, Shuvra S. Bhattacharyya
 @version $Id: Forest.java 1634 2007-04-06 18:24:07Z ssb $
 */

public class Forest extends UndirectedGraph {
    /** Construct an empty Forest.
     */
    public Forest() {
        super();
    }

    /** Construct an empty Forest with enough storage allocated
     *  for the specified number of nodes.  Memory management is more
     *  efficient with this constructor if the number of nodes is
     *  known. Note that number of edges in a forest can be at most nodeCount - 2.
     *  @param nodeCount The number of nodes.
     */
    public Forest(int nodeCount) {
        super(nodeCount, nodeCount-2);
    }

    /** Construct an empty forest with enough storage allocated for the
     *  specified number of edges, and number of nodes.  Memory
     *  management is more efficient with this constructor if the
     *  number of nodes and edges is known. Note that number of edges in a forest can be at most nodeCount - 2.
     *  @param nodeCount The number of nodes.
     *  @param edgeCount The number of edges.
     */
    public Forest(int nodeCount, int edgeCount) {
        super(nodeCount, edgeCount);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Return all the trees in the given forest.
     *  @return A collection of Trees in the given forest.
     */
    public ArrayList getTrees(){
        return (ArrayList)connectedComponents();
    }

    /** Create and add an edge with a specified source node, sink node,
     *  and optional weight.
     *  The third parameter specifies whether the edge is to be
     *  weighted, and the fourth parameter is the weight that is
     *  to be applied if the edge is weighted.
     *  Returns the edge that is added.
     *  @param node1 The source node of the edge.
     *  @param node2 The sink node of the edge.
     *  @param weighted True if the edge is to be weighted.
     *  @param weight The weight that is to be applied if the edge is to
     *  be weighted.
     *  @return The valid edge that was added to the forest
     *  @exception GraphConstructionException If the specified nodes
     *  are identical.
     *  @exception GraphConstructionException If the specified edge
     *  makes the graph connected or cyclic.
     *  @exception GraphElementException If either of the specified nodes
     *  is not in the graph.
     *  @exception NullPointerException If the edge is to be weighted, but
     *  the specified weight is null.
     */
    public Edge addEdgeToForest(Node node1, Node node2, boolean weighted, Object weight) {
	Edge edge = new Edge(node1, node2);
        if (node1 == node2) {
            throw new GraphConstructionException("Cannot add a self loop in "
                    + "a Forest.\nA self loop was attempted on the "
                    + "following node.\n" + node1.toString());
        } else {
	    if (weighted) {
		edge = addEdge(node1, node2, weight);
	    } else {
		edge = addEdge(node1, node2);
	    }
	    if(!(this.isAcyclic()) || this.isConnected()){
		this.removeEdge(edge);
		throw new GraphConstructionException("Cannot add an edge that makes graph cyclic or coonected ");
	    }
	}
	return edge;
    }
}
