/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* An undirected graph and some graph algorithms.*/
package dif.util.graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

import dif.util.graph.analysis.Analysis;
import dif.util.graph.analysis.CycleExistenceAnalysis;
import dif.util.graph.analysis.TransitiveClosureAnalysis;

//////////////////////////////////////////////////////////////////////////
//// UndirectedGraph

/**
 An undirected graph.
 Some methods in this class have two versions, one that operates
 on graph nodes, and another that operates on
 node weights. The latter form is called the <i>weights version</i>.
 More specifically, the weights version of an operation takes individual
 node weights or arrays of weights as arguments, and, when applicable, returns
 individual weights or arrays of weights.

 <p> Multiple edges in a graph can exist between the same pair of nodes.  Thus,
undirected multigraphs are supported.

 This implementation is based on {@link DirectedGraph}.

 @author Nimish Sane, Shuvra S. Bhattacharyya
 @version $Id: UndirectedGraph.java 1634 2007-04-06 18:24:07Z ssb $
 @see DirectedGraph
 */
public class UndirectedGraph extends Graph {
    /** Construct an empty undirected graph.
     */
    public UndirectedGraph() {
        super();
    }

    /** Construct an empty undirected graph with enough storage allocated
     *  for the specified number of nodes.  Memory management is more
     *  efficient with this constructor if the number of nodes is
     *  known.
     *  @param nodeCount The integer specifying the number of nodes
     */
    public UndirectedGraph(int nodeCount) {
        super(nodeCount);
    }

    /** Construct an empty undirected graph with enough storage allocated for the
     *  specified number of edges, and number of nodes.  Memory
     *  management is more efficient with this constructor if the
     *  number of nodes and edges is known.
     *  @param nodeCount The number of nodes.
     *  @param edgeCount The number of edges.
     */
    public UndirectedGraph(int nodeCount, int edgeCount) {
        super(nodeCount, edgeCount);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Return the nodes that are in cycles. If there are multiple cycles,
     *  the nodes in all the cycles will be returned.
     *  @return The collection of nodes that are in cycles; each element
     *  is a {@link Node}.
     */
    public Collection cycleNodeCollection() {
        boolean[][] transitiveClosure = transitiveClosure();

        ArrayList result = new ArrayList(transitiveClosure.length);
        Iterator nodes = nodes().iterator();

        while (nodes.hasNext()) {
            Node next = (Node) nodes.next();
            int label = nodeLabel(next);

            if (transitiveClosure[label][label]) {
                result.add(next);
            }
        }

        return result;
    }

    /** Return the nodes that are in cycles (weights version).
     *  If there are multiple cycles,
     *  the nodes in all the cycles will be returned.
     *  @return An array of node weights that are in cycles; each element
     *  is an {@link Object}.
     */
    public Object[] cycleNodes() {
        return weightArray(cycleNodeCollection());
    }

    /** Test if an edge exists from one node to another.
     *  @param node1 The first node.
     *  @param node2 The second node.
     *  @return True if the graph includes an edge from the first node to
     *  the second node; false otherwise.
     */
    public boolean edgeExists(Node node1, Node node2) {
        Iterator neighborNodes = neighbors(node1).iterator();

        while (neighborNodes.hasNext()) {
            if ((Node)neighborNodes.next() == node2) {
                return true;
            }
        }

        return false;
    }

    /** Test whether an edge exists from one node weight to another.
     *  More specifically, test whether there exists an edge (n1, n2)
     *  such that
     *
     *  <p><code>
     *      (n1.getWeight() == weight1) && (n2.getWeight() == weight2)
     *  </code>.
     *
     *  @param weight1 The first node weight.
     *  @param weight2 The second node weight.
     *  @return True if the graph includes an edge from the first node weight to
     *  the second node weight.
     */
    public boolean edgeExists(Object weight1, Object weight2) {
        Iterator sources = nodes(weight1).iterator();

        while (sources.hasNext()) {
            Node candidateSource = (Node) sources.next();
            Iterator sinks = nodes(weight2).iterator();

            while (sinks.hasNext()) {
                Node candidateSink = (Node) sinks.next();

                if (edgeExists(candidateSource, candidateSink)) {
                    return true;
                }
            }
        }

        return false;
    }

    /** Test if this graph is acyclic.
     *  @return True if the the graph is acyclic, or
     *  empty; false otherwise.
     */
    public boolean isAcyclic() {
        return !_acyclicAnalysis.hasCycle();
    }

    /** Test if this graph is connected.
     *  @return True if the the graph is connected or
     *  empty; false otherwise.
     */
    public boolean isConnected() {
	ArrayList connectedComponents = (ArrayList)connectedComponents();
	if (connectedComponents.size() == 1)
	    return true;
	else
	    return false;
    }

    /** Check if given graph is a forest.
     *  @return True if the graph is a forest; false otherwise.
     */
    public boolean isForest() {
        return (isAcyclic() && (!(isConnected())));
    }

    /** Check if the graph is a tree.
     *  @return True if the graph is tree; false otherwise.
     */
    public boolean isTree() {
	return (isAcyclic() && isConnected());
    }

    /** Find all the nodes that can be reached from the specified node.
     *  The reachable nodes do not include the specific one unless
     *  there is a loop from the specified node back to itself.
     *  @param node The specified node.
     *  @return The collection of nodes reachable from the specified one;
     *  each element is a {@link Node}.
     *  @exception GraphElementException If the specified node is
     *  not a node in this graph.
     */
    public Collection reachableNodes(Node node) {
        boolean[][] transitiveClosure = transitiveClosure();
        int label = nodeLabel(node);
        ArrayList result = new ArrayList(transitiveClosure.length);
        Iterator nodes = nodes().iterator();

        while (nodes.hasNext()) {
            Node next = (Node) nodes.next();

            if (transitiveClosure[label][nodeLabel(next)]) {
                result.add(next);
            }
        }

        return result;
    }

    /** Find all the nodes that can be reached from any node that has the
     *  specified node weight (weights version). If the specified weight
     *  is null, find all the nodes that can be reached from any node
     *  that is unweighted.
     *  @param weight The specified node weight.
     *  @return An array of node weights reachable from the specified weight;
     *  each element is an {@link Object}.
     *  @exception GraphWeightException If the specified node weight is
     *  not a node weight in this graph.
     *  @see #reachableNodes(Node)
     */
    public Object[] reachableNodes(Object weight) {
        Collection sameWeightNodes = nodes(weight);

        if (sameWeightNodes.size() == 0) {
            throw new GraphWeightException(weight, null, this,
                    "The specified weight is not a node weight in this graph.");
        }

        return weightArray(reachableNodes(sameWeightNodes));
    }

    /** Find all the nodes that can be reached from the specified collection
     *  of nodes (weights version). The reachable nodes do not include a
     *  specified one unless there is a loop from the specified node back to
     *  itself.
     *  @param weights An array of node weights; each element is an
     *  {@link Object}.
     *  @return The array of nodes that are reachable from
     *  the specified one; each element is an {@link Object}.
     *  @see #reachableNodes(Node)
     */
    public Object[] reachableNodes(Object[] weights) {
        return weightArray(reachableNodes(nodes(Arrays.asList(weights))));
    }

    /** Find all the nodes that can be reached from the specified collection
     *  of nodes. The reachable nodes do not include a specified one unless
     *  there is a loop from the specified node back to itself.
     *  @param nodeCollection The specified collection of nodes;
     *  each element is a {@link Node}.
     *  @return The collection of nodes that are reachable from
     *  the specified one; each element is a {@link Node}.
     */
    public Collection reachableNodes(Collection nodeCollection) {
        boolean[][] transitiveClosure = transitiveClosure();

        int N = nodeCollection.size();
        int[] labels = new int[N];
        Iterator nodes = nodeCollection.iterator();

        for (int i = 0; i < N; i++) {
            labels[i] = nodeLabel((Node) nodes.next());
        }

        ArrayList reachableNodes = new ArrayList(transitiveClosure.length);

        // Compute the OR of the corresponding rows.
        Iterator graphNodes = nodes().iterator();

        while (graphNodes.hasNext()) {
            Node nextGraphNode = (Node) graphNodes.next();
            int nextGraphLabel = nodeLabel(nextGraphNode);
            boolean reachable = false;

            for (int i = 0; i < N; i++) {
                if (transitiveClosure[labels[i]][nextGraphLabel]) {
                    reachable = true;
                    break;
                }
            }

            if (reachable) {
                reachableNodes.add(nextGraphNode);
            }
        }

        return reachableNodes;
    }

    /** Return transitive closure for the graph.
     *
     *  @return Transitive closure for the graph.
     */
    public boolean[][] transitiveClosure() {
        boolean[][]transitiveClosureMatrix = _transitiveClosureAnalysis.transitiveClosureMatrix();
	for(int i = 0; i < transitiveClosureMatrix.length; i++)
	    for(int j = 0; j <= i; j++)
		{
		    if(transitiveClosureMatrix[i][j] || transitiveClosureMatrix[j][i])
			transitiveClosureMatrix[i][j] = transitiveClosureMatrix[j][i] = true;
		}
	return transitiveClosureMatrix;
    }

    ///////////////////////////////////////////////////////////////////
    ////                         protected methods                 ////

    /** Initialize the list of analyses that are associated with this graph,
     *  and initialize the change counter of the graph.
     *  @see Analysis
     */
    protected void _initializeAnalyses() {
        super._initializeAnalyses();
        _transitiveClosureAnalysis = new TransitiveClosureAnalysis(this);
        _acyclicAnalysis = new CycleExistenceAnalysis(this);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         private methods                   ////


    ///////////////////////////////////////////////////////////////////
    ////                         private variables                 ////

    // The graph analysis for computation of acyclic property
    private CycleExistenceAnalysis _acyclicAnalysis;

    // The graph analysis for computation of transitive closure
    private TransitiveClosureAnalysis _transitiveClosureAnalysis;
}

