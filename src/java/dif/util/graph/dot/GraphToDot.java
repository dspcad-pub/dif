/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* GraphToDot class to use with mocgraph.Graph.*/

package dif.util.graph.dot;

import java.util.Iterator;

import dif.util.graph.Graph;
import dif.util.graph.Edge;
import dif.util.graph.Node;

//////////////////////////////////////////////////////////////////////////
//// GraphToDot

/** DOT file generator for Graph objects. It is used to create dot
files as an input to GraphViz tools. A DOT file is created by first
defining an GraphToDot object and then using the {@link #toFile}
method. Node and edge labels are set to element names in Graph.

@author Nimish Sane
@author  Fuat Keceli
@version $Id: GraphToDot.java 606 2008-10-08 16:29:47Z plishker $
*/

public class GraphToDot extends DotGenerator {
    /** Creates a DotGenerator object from a Graph.
     *  @param graph A Graph.
     */
    public GraphToDot(Graph graph) {
        super(graph);
        addLine("label = " + "\"" + ((Graph)_graph).name() + "\";");
        addLine("node[shape = circle];");
        addLine("center = true;");
        addLine("edge[fontcolor = red];");

        for(Iterator graphNodes = _graph.nodes().iterator();
            graphNodes.hasNext(); ) {
            Node graphNode = (Node) graphNodes.next();
	    setAttribute(graphNode, "label", graphNode.name());
        }

        for(Iterator graphEdges = _graph.edges().iterator();
            graphEdges.hasNext(); ) {
            Edge graphEdge = (Edge) graphEdges.next();
	    setAttribute(graphEdge, "label", graphEdge.name());
	    // setAttribute(graphEdge, "labeldistance", "1.75");
	    // setAttribute(graphEdge, "labelangle", "65");
	    // setAttribute(graphEdge, "fontsize", "10");
	}
    }
}


