/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Generates hierarchical HTML of DIF graphs.*/

package dif.util.graph.dot;

import dif.util.graph.Graph;
import dif.util.graph.util.CommandUtilities;

import java.io.IOException;

//////////////////////////////////////////////////////////////////////////
//// MocGraph Doc Generator

/**
 * Generates visualization for MoCGraph graphs.
 *
 * @author Nimish Sane
 * @author Ivan Corretjer
 * @version 1.0
 */
public class MoCGraphdoc {

    /**
     * <Constructor comment>
     */
    protected MoCGraphdoc() {
        // Empty constructor is forbidden.
    }

    /**
     * Constructor for generating an MoCGraphdoc object for just this
     * graph.
     *
     * @param graph Graph to use as basis for PNG generation.
     */
    public MoCGraphdoc(Graph graph) {
        _graph = graph;
        _graphName = graph.name();
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /**
     * Takes in a {@link Graph} g, and produces a PNG file
     * in the current working directory with name
     * <code>g.name()</code>.png
     *
     * @param type Figure type options supported by dot --- "ps",
     *             "png", etc.
     */
    public void generateGraphVisual(String type) {
        GraphToDot graphDot = new GraphToDot(_graph);
        String graphName = _graphName;

        try {
            graphDot.setGraphName(graphName);
            //make sure our nodes are ellipses not circles
            graphDot.addLine("node[shape = ellipse]");
            graphDot.setAsDirected(true);
            graphDot.toFile(graphName);
        } catch (IOException ex) {
            System.err.println("Problem writing Dot file for " + graphName);
        }

        // produce the PNG file from the newly created Dot file
        CommandUtilities system = new CommandUtilities();
        String[] command = new String[5];
        command[0] = "dot";
        command[1] = "-T" + type;
        command[2] = "-o";
        command[3] = graphName + ".png";
        command[4] = graphName + ".dot";

        system.runCommand(command);
        System.err.println(system.getErrorMessage());
    }

    /**
     * Takes in a {@link Graph} g, and produces a PNG file
     * in the current working directory with name
     * <code>g.name()</code>.png
     */
    public void generateGraphVisual() {
        generateGraphVisual("png");
    }

    ///////////////////////////////////////////////////////////////////
    ////                         private fields                    ////

    /**
     * The original graph given in the constructor.
     */
    private Graph _graph;

    /**
     * Name of the graph input to the constructore
     */
    private String _graphName;

}


