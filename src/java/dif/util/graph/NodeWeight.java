/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* NodeWeight to contain the information associated with a graph node. */

package dif.util.graph;

import dif.DIFNodeWeight;

import java.lang.String;

//////////////////////////////////////////////////////////////////////////
//// NodeWeight

/** Information associated with a node. {@link NodeWeight}s are
objects associated with {@link Node}s that represent nodes in
{@link Graph}s.  Any subclass of {@link Graph} will
have an associated node, which must be derived from this class. Note
that {@link Node} is final class. The corresponding classes
derived from this class will have methods to support graph
transformations and algortihms corresponding to that type of graph.
This implementation is based on {@link DIFNodeWeight}.

It is possible to associate any computation and/or data structure with the {@link NodeWeight} by setting it as
computation.

@author Nimish Sane, Shuvra S. Bhattacharyya
@version $Id: NodeWeight.java 606 2008-10-08 16:29:47Z plishker $
@see Node
@see DIFNodeWeight
*/

public class NodeWeight implements Cloneable {

    /** Construct a node weight.
     */
    public NodeWeight() {
	setComputation(null);
    }

    /** Construct a node weight that is associated with the given
     *  computation.
     */
    public NodeWeight(Object computation) {
        setComputation(computation);
    }

    ///////////////////////////////////////////////////////////////////
    ////                       public methods                      ////

    /** Overrides the protected method Object.clone(). It uses
     *  Object.clone() to "shallow" copy the fields and returns the cloned
     *  version.
     */
    public Object clone() {
        //getClass() can get the correct derived class
        try {
            Object returnObj = getClass().newInstance();
            ((NodeWeight)returnObj).setComputation(_computation);
            return returnObj;
        } catch (Exception exp) {
            throw new RuntimeException("Cannot clone node weight.");
        }
    }

    /** Return the computation that is represented by this node.
     *  Return null if no computation has been associated yet with the node.
     *  @return the represented computation.
     */
    public Object getComputation() {
        return _computation;
    }

    /** Set the computation to be represented by this node.
     *  @param computation the computation.
     */
    public void setComputation(Object computation) {
        _computation = computation;
    }

    /** Returns the string "NodeWeight".
     *  @return The string "NodeWeight".
     */
    public String toString() {
        return this.getClass().getName();
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    // The computation associated with the corresponding graph node.
    private Object _computation;
}




