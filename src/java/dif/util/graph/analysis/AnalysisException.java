/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Base exception for analyses.*/
package dif.util.graph.analysis;

//////////////////////////////////////////////////////////////////////////
//// AnalysisException

/**
 Base exception for non-checked exceptions in the analyses package.

 @author Shahrooz Shahparnia
 @version $Id: AnalysisException.java 1637 2007-04-07 13:51:28Z ssb $
 */
public class AnalysisException extends RuntimeException {
    /** The default constructor without arguments.
     */
    public AnalysisException() {
    }

    /** Constructor with a text description as argument.
     *
     *  @param message The text description of the exception.
     */
    public AnalysisException(String message) {
        super(message);
    }

    /** Constructor with a text description and cause as argument.
     *
     *  @param message The text description of the exception.
     *  @param cause The exception that caused this exception.
     */
    public AnalysisException(String message, Throwable cause) {
        super(message, cause);
    }
}

