/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Computation of source nodes in a graph. */
package dif.util.graph.analysis;

import java.util.List;

import dif.util.graph.Graph;
import dif.util.graph.Node;
import dif.util.graph.analysis.analyzer.Analyzer;
import dif.util.graph.analysis.analyzer.SourceNodeAnalyzer;
import dif.util.graph.analysis.strategy.SourceNodeStrategy;

//////////////////////////////////////////////////////////////////////////
//// SourceNodeAnalysis

/**
 Computation of source nodes in a graph.
 A source node in a graph is a node without input edges.
 <p>
 The returned collection cannot be modified when the client uses the default
 analyzer.
 <p>
 @author Shahrooz Shahparnia
 @version $Id: SourceNodeAnalysis.java 1637 2007-04-07 13:51:28Z ssb $
 */
public class SourceNodeAnalysis extends Analysis {
    /** Construct an instance of this class for a given graph.
     *
     *  @param graph The given graph.
     */
    public SourceNodeAnalysis(Graph graph) {
        super(new SourceNodeStrategy(graph));
    }

    /** Construct an instance of this class using a given analyzer.
     *
     *  @param analyzer The given analyzer.
     */
    public SourceNodeAnalysis(SourceNodeAnalyzer analyzer) {
        super(analyzer);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Return the source nodes in the graph under analysis.
     *  Each element of the collection is an {@link Node}.
     *
     *  @return Return the source nodes.
     */
    public List nodes() {
        return ((SourceNodeAnalyzer) analyzer()).nodes();
    }

    /** Return a description of the analysis and the associated analyzer.
     *
     *  @return A description of the analysis and the associated analyzer.
     */
    public String toString() {
        return "Source node analysis using the following analyzer:\n"
                + analyzer().toString();
    }

    /** Check if a given analyzer is compatible with this analysis.
     *  In other words if it is possible to use it to compute the computation
     *  associated with this analysis.
     *
     *  @param analyzer The given analyzer.
     *  @return True if the given analyzer is valid for this analysis.
     */
    public boolean validAnalyzerInterface(Analyzer analyzer) {
        return analyzer instanceof SourceNodeAnalyzer;
    }
}

