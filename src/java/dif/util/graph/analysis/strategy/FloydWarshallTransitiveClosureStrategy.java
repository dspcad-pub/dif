/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Computation of transitive closure of a directed graph using the
 Floyd-Warshall algorithm. */
package dif.util.graph.analysis.strategy;

import java.util.Iterator;

import dif.util.graph.DirectedGraph;
import dif.util.graph.Edge;
import dif.util.graph.Graph;
import dif.util.graph.Node;
import dif.util.graph.analysis.TransitiveClosureAnalysis;
import dif.util.graph.analysis.analyzer.TransitiveClosureAnalyzer;

//////////////////////////////////////////////////////////////////////////
//// FloydWarshallTransitiveClosureStrategy

/**
 Computation of transitive closure of a directed graph using the
 Floyd-Warshall algorithm described in:
 Thomas H. Cormen, Charles E. Leiserson and Ronald L. Rivest:
 Introduction to Algorithms. Cambridge: MIT Press, 1990.
 <p>
 The complexity of this algorithm is O(N^3), where N is the number of nodes.
 <p>
 @see Graph#nodeLabel
 @see TransitiveClosureAnalysis
 @author Shahrooz Shahparnia based on an initial implementation by Ming Yung Ko.
 @version $Id: FloydWarshallTransitiveClosureStrategy.java 1638 2007-04-07 14:06:24Z ssb $
 */
public class FloydWarshallTransitiveClosureStrategy extends
        FloydWarshallStrategy implements TransitiveClosureAnalyzer {
    /** Construct a transitive closure analysis for a given directed graph.
     *  @param graph The given directed graph.
     */
    public FloydWarshallTransitiveClosureStrategy(Graph graph) {
        super(graph);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Check if there exist a path between a starting node and an ending node
     *  on the analyzer's graph.
     *
     *  @param startNode The starting node.
     *  @param endNode The ending node.
     *  @return True if such a path exists.
     */
    public boolean pathExistence(Node startNode, Node endNode) {
        return _transitiveClosure[graph().nodeLabel(startNode)][graph()
                .nodeLabel(endNode)];
    }

    /** Return a description of the analyzer.
     *
     *  @return Return a description of the analyzer..
     */
    public String toString() {
        return "Transitive closure analyzer"
                + " based on the Floyd-Warshall algorithm.";
    }

    /** Compute the transitive closure of the graph under analysis in the
     *  form of two dimensional array. The first dimension represents
     *  source node label while the second one represents sink node label.
     *  Assume i and j are labels of two nodes.
     *  transitiveClosureMatrix()[i][j] is true if there is a path on the graph
     *  from "i" to "j".
     *
     *  @return The transitive closure in the form of 2D array.
     */
    public boolean[][] transitiveClosureMatrix() {
        return (boolean[][]) _result();
    }

    /** Check for validity of this strategy.
     *  A graph needs to be an instance of a {@link DirectedGraph} in order
     *  to use this algorithm.
     *
     *  @return True if the graph is a directed graph.
     */
    public boolean valid() {
        return (graph() instanceof DirectedGraph);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         protected methods                 ////

    /** The computation associated with the Floyd-Warshall algorithm.
     *
     *  @return Return the transitive closure matrix as an {@link Object}
     *  in order to be stored in the result-cache.
     */
    protected Object _compute() {
        int size = graph().nodeCount();

        // Initialize transitiveClosure to the adjacency matrix
        _transitiveClosure = new boolean[size][size];

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                _transitiveClosure[i][j] = false;
            }

            Node node = graph().node(i);
            Iterator outputEdges = ((DirectedGraph) graph()).outputEdges(node)
                    .iterator();

            while (outputEdges.hasNext()) {
                int sinkLabel = ((DirectedGraph) graph())
                        .nodeLabel(((Edge) outputEdges.next()).sink());
                _transitiveClosure[i][sinkLabel] = true;
            }
        }

        super._compute();
        return _transitiveClosure;
    }

    /** Overrides the computation associated with the implementation of the
     *  Floyd-Warshall algorithm, for the purpose of computing the transitive
     *  closure.
     */
    protected void _floydWarshallComputation(int k, int i, int j) {
        _transitiveClosure[i][j] |= (_transitiveClosure[i][k] & _transitiveClosure[k][j]);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         private variables                 ////
    // A reference to the result of the computation to be shared between the
    // two protected methods.
    private boolean[][] _transitiveClosure;
}

