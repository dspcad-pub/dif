/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Computation of cycle existence in directed graphs using an all pair shortest
 path algorithm based on the Floyd-Warshall algorithm. */
package dif.util.graph.analysis.strategy;

import dif.util.graph.DirectedGraph;
import dif.util.graph.Graph;
import dif.util.graph.analysis.CycleExistenceAnalysis;
import dif.util.graph.analysis.analyzer.CycleExistenceAnalyzer;

/**
 * Computation of cycle existence in directed graphs using an all pair shortest
 * path algorithm based on the Floyd-Warshall algorithm.
 * The complexity of this algorithm is O(N^3), where N is the number of nodes.
 *
 * @author Shahrooz Shahparnia
 * @version $Id: FloydWarshallCycleExistenceStrategy.java 1638 2007-04-07 14:06:24Z ssb $
 * @see CycleExistenceAnalysis
 */
public class FloydWarshallCycleExistenceStrategy extends CachedStrategy
        implements CycleExistenceAnalyzer {
    /**
     * Construct an instance of this analyzer for a given graph.
     *
     * @param graph The given graph.
     */
    public FloydWarshallCycleExistenceStrategy(Graph graph) {
        super(graph);
        _strategy = new FloydWarshallTransitiveClosureStrategy(graph());
    }

    /**
     * Check acyclic property of the graph.
     *
     * @return True if cyclic.
     */
    public boolean hasCycle() {
        return ((Boolean) _result()).booleanValue();
    }

    /**
     * Return a description of the analyzer.
     *
     * @return Return a description of the analyzer.
     */
    public String toString() {
        return "Cycle existence analyzer based on the Floyd-Warshall algorithm.";
    }

    /**
     * Check for compatibility between the analysis and the given
     * graph. A graph needs to be an instance of a {@link DirectedGraph}
     * in order to use this algorithm.
     *
     * @return True if the graph is a directed graph.
     */
    public boolean valid() {
        return (graph() instanceof DirectedGraph);
    }


    /**
     * The computation associated with the Floyd-Warshall algorithm.
     *
     * @return Return a true {@link Boolean} {@link Object} if the graph is
     * cyclic.
     */
    protected Object _compute() {
        boolean cyclic = false;
        boolean[][] transitiveClosure = _strategy.transitiveClosureMatrix();

        for (int i = 0; i < transitiveClosure.length; i++) {
            if (transitiveClosure[i][i] == true) {
                cyclic = true;
                break;
            }
        }

        return Boolean.valueOf(cyclic);
    }

    /**
     * he transitive closure analyzer used to check the existence of a cycle
     * in the associated graph.
     */
    private FloydWarshallTransitiveClosureStrategy _strategy;
}

