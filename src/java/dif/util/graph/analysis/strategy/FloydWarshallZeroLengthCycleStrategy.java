/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Analyzer to check if a given directed graph has a zero length cycle using the
 Floyd-Warshall all pair shortest path algorithm.

Now implements ZeroLengthCycleAnalyzer instead of NegativeLengthCycleAnalyzer
Replaced on Jan 08, 2009 by Nimish Sane
 */
package dif.util.graph.analysis.strategy;

import dif.util.graph.DirectedGraph;
import dif.util.graph.Graph;
import dif.util.graph.analysis.analyzer.ZeroLengthCycleAnalyzer;
import dif.util.graph.mapping.ToDoubleMapping;

//////////////////////////////////////////////////////////////////////////
//// FloydWarshallZeroLengthCycleStrategy

/**
 Analyzer to check if a given directed graph has a zero cycle using the
 Floyd-Warshall all pair shortest path algorithm.
 <p>
 @see graph.analysis.ZeroLengthCycleAnalysis
 @author Shahrooz Shahparnia, Nimish Sane
 @version $Id: FloydWarshallZeroLengthCycleStrategy.java 1638 2007-04-07 14:06:24Z ssb $
 */

/**
   Now implements ZeroLengthCycleAnalyzer instead of NegativeLengthCycleAnalyzer
   Replaced on Jan 08, 2009 by Nimish Sane
 */

public class FloydWarshallZeroLengthCycleStrategy extends CachedStrategy
        implements ZeroLengthCycleAnalyzer {
    /** Constructs negative cycle detection analyzer for a given graph and
     *  given edge values.
     *
     *  @param graph The given graph.
     *  @param edgeLengths The lengths associated with the given graph.
     */
    public FloydWarshallZeroLengthCycleStrategy(Graph graph,
            ToDoubleMapping edgeLengths) {
        super(graph);
        _edgeLengths = edgeLengths;
        _strategy = new FloydWarshallAllPairShortestPathStrategy(graph,
                _edgeLengths);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Return true if a zero length cycle exists in the graph under analysis.
     *
     *  @return True if the graph has a zero length cycle.
     */
    public boolean hasZeroLengthCycle() {
        return ((Boolean) _result()).booleanValue();
    }

    /** Return a description of the analyzer.
     *
     *  @return Return a description of the analyzer..
     */
    public String toString() {
        return "Zero Length analyzer"
                + " based on the Floyd-Warshall algorithm.";
    }

    /** Check for compatibility between the analysis and the given
     *  graph. A graph needs to be an instance of a DirectedGraph in order
     *  to use this algorithm.
     *
     *  @return True if the graph is a directed and cyclic graph.
     */
    public boolean valid() {
        return (graph() instanceof DirectedGraph);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         protected methods                 ////

    /** The computation associated with the Floyd-Warshall algorithm.
     *
     *  @return Return a true {@link Boolean} {@link Object} if the graph has
     *  a negative cycle.
     */
    protected Object _compute() {
        double[][] allPairShortestPath = _strategy.shortestPathMatrix();
        boolean zeroCycle = false;
        int n = graph().nodeCount();

        for (int i = 0; i < n; i++) {
            if (allPairShortestPath[i][i] == 0) {
                zeroCycle = true;
                break;
            }
        }

        return Boolean.valueOf(zeroCycle);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         private variables                 ////
    // The transitive closure analyzer used to check the existence of a zero
    // length cycle in the associated graph.
    private FloydWarshallAllPairShortestPathStrategy _strategy;

    private ToDoubleMapping _edgeLengths;
}

