/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Base class for all the analysis based on a Floyd-Warshall like 
computation.*/
package dif.util.graph.analysis.strategy;

import dif.util.graph.Graph;

//////////////////////////////////////////////////////////////////////////
//// FloydWarshallAnalysis

/**
 Base class for all the analysis based on a floyd-warshall like computation.
 This is an abstract class and cannot be instantiated.
 <p>
 @author Shahrooz Shahparnia
 @version $Id: FloydWarshallStrategy.java 1638 2007-04-07 14:06:24Z ssb $
 */
abstract public class FloydWarshallStrategy extends CachedStrategy {
    /** Construct an FloydWarshallStrategy.
     */
    public FloydWarshallStrategy(Graph graph) {
        super(graph);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         protected methods                 ////

    /** Basic computation performed by all the analysis implementing a
     *  floyd-warshall like analysis on a given graph.
     *  Derived classed need to override the (@link #_floydWarshallComputation}
     *  method of this class to provide the correct functionality.
     *
     */
    protected Object _compute() {
        int n = graph().nodeCount();
        Object floydWarshallResult = null;

        // Warshall's algorithm
        for (int k = 0; k < n; k++) {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    _floydWarshallComputation(k, i, j);
                }
            }
        }

        return floydWarshallResult;
    }

    /** Derived classed need to override the _floydWarshallComputation method
     *  of this class to provide the correct functionality.
     */
    protected void _floydWarshallComputation(int k, int i, int j) {
    }
}

