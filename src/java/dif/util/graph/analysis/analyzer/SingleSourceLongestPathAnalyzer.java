/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A common interface for all the single source longest path analyzers. */
package dif.util.graph.analysis.analyzer;

import java.util.List;

import dif.util.graph.Graph;
import dif.util.graph.Node;
import dif.util.graph.analysis.SingleSourceLongestPathAnalysis;

//////////////////////////////////////////////////////////////////////////
//// SingleSourceLongestPathAnalyzer

/**
 A common interface for all the single source longest path analyzers.
 <p>
 @see SingleSourceLongestPathAnalysis
 @author Shahrooz Shahparnia
 @version $Id: SingleSourceLongestPathAnalyzer.java 1638 2007-04-07 14:06:24Z ssb $
 */
public interface SingleSourceLongestPathAnalyzer extends GraphAnalyzer {
    /** Return the distance from the start node to all the other nodes in the
     *  graph. The result is a double[] indexed by the destination node label.
     *
     *  @see Graph#nodeLabel
     *  @return Return the distance from the start node to all the other nodes
     *  in the graph.
     */
    public double[] distance();

    /** Return the start node of this analyzer.
     *
     *  @return Return the start node of this analyzer.
     */
    public Node getStartNode();

    /** Return the longest path from node "startNode" to node "endNode" in the
     *  form of an ordered list.
     *
     *  @param endNode The ending node of the path.
     */
    public List path(Node endNode);

    /** Return the length of the longest path from node "startNode"
     *  to node "endNode". The source node can be
     *  set using {@link #setStartNode}.
     *
     *  @param endNode The ending node of the path.
     */
    public double pathLength(Node endNode);

    /** Set the single source node of this analyzer to the given node.
     *
     *  @param startNode The given node.
     */
    public void setStartNode(Node startNode);
}

