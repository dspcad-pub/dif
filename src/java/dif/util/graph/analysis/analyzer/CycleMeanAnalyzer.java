/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A common interface for all the cycle mean analyzers. */
package dif.util.graph.analysis.analyzer;

import dif.util.graph.analysis.CycleMeanAnalysis;

import java.util.List;

//////////////////////////////////////////////////////////////////////////
//// CycleMeanAnalyzer

/**
 A common interface for all the cycle mean analyzers.
 <p>
 @see CycleMeanAnalysis
 @author Shahrooz Shahparnia
 @version $Id: CycleMeanAnalyzer.java 1638 2007-04-07 14:06:24Z ssb $
 */
public interface CycleMeanAnalyzer extends GraphAnalyzer {
    /** Return the nodes on the cycle that corresponds to the maximum/minimum
     *  cycle mean as an ordered list. If there is more than one cycle with the
     *  same maximal/minimal cycle, one of them is returned randomly,
     *  but the same cycle is returned by different invocations of the method,
     *  unless the graph changes.
     *
     *  @return The nodes on the cycle that corresponds to one of the
     *  maximum/minimum cycle means as an ordered list.
     */
    public List cycle();

    /** Return the maximum cycle mean for a given directed graph.
     *
     *  @return The maximum cycle mean value.
     */
    public double maximumCycleMean();

    /** Return minimum cycle mean for a given directed graph.
     *
     *  @return The minimum cycle mean value.
     */
    public double minimumCycleMean();
}

