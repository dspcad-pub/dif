/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A base interface for transformers. */
package dif.util.graph.analysis.analyzer;

//////////////////////////////////////////////////////////////////////////
//// Transformer

/**
 A base interface for transformers. Transformers are graph analyzers which
 transform a graph into another graph and they provide a bilateral way to
 retrieve the original elements of the graph from the new (transformed) ones
 and vice versa. If only unilateral relation is being considered,
 this can be communicated to the client through the {@link #hasBackwardMapping()}
 and {@link #hasForwardMapping()} methods.
 <p>
 @author Shahrooz Shahparnia, Shuvra S. Bhattacharyya
 @version $Id: Transformer.java 1638 2007-04-07 14:06:24Z ssb $
 */
public interface Transformer extends GraphAnalyzer {
    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Specify if this transformer has a mapping from the transformed
     *  version to the original version or not.
     *
     *  @return True  if the implementation of the transformer supports backward
     *  mapping.
     */
    public boolean hasBackwardMapping();

    /** Specify if this transformer has a mapping from the original
     *  version to the transformed version or not.
     *
     *  @return True if the implementation of the transformer supports forward
     *  mapping.
     */
    public boolean hasForwardMapping();

    /** Return the original version of given object in the transformed graph.
     *  The method should be defined in implementing classes to return the
     *  original version of the given object. The transformed objects and a
     *  mapping between the original and the transformed objects are
     *  created during the transformation process in a specific algorithm
     *  (strategy).
     *
     *  @param transformedObject The given object in the transformed graph.
     *  @return Return the original version the given object.
     */
    public Object originalVersionOf(Object transformedObject);

    /** Return the transformed version of a given object in the original graph.
     *  The method should be defined in implementing classes to return the
     *  transformed version of the given object. The transformed objects and a
     *  mapping between the original and the transformed objects are
     *  created during the transformation process in a specific algorithm
     *  (strategy).
     *
     *  @param originalObject The given object in the original graph.
     *  @return Return the transformed version of the given object.
     */
    public Object transformedVersionOf(Object originalObject);
}

