/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Base interface for a mirror transformer for graphs. */
package dif.util.graph.analysis.analyzer;

import dif.util.graph.Graph;
import dif.util.graph.analysis.MirrorTransformation;

//////////////////////////////////////////////////////////////////////////
//// MirrorTransformerAnalyzer

/**
 Base interface for a mirror transformer for graphs.
 <p>
 In the {@link #cloneWeight} method, users can also specify whether to clone node
 and edge weights. For non cloneable
 weights a {@link java.lang.CloneNotSupportedException} will be thrown by
 the virtual machine.
 <p>
 @see MirrorTransformation
 @author Shahrooz Shahparnia
 @version $Id: MirrorTransformer.java 1638 2007-04-07 14:06:24Z ssb $
 */
public interface MirrorTransformer extends Transformer {
    /** Changes the status of the graph returned by the {@link #mirror} method.
     *  If set to true, the weights will also be cloned in the next calls to the
     *  {@link #mirror} method.
     *
     *  @param status If set to true, the weights will also be cloned.
     */
    public void cloneWeight(boolean status);

    /** Create a mirror of the graph associated with this analyzer with the
     *  same runtime class.
     *
     *  @return The resulting mirror graph.
     */
    public Graph mirror();

    /** Return a mirror of this graph in the form of the argument graph type
     *  (i.e., the run-time type of the returned graph is that of the
     *  argument graph).
     *  <p>
     *
     *  @param graph The type of the graph which the graph associated with
     *  this analyzer is being mirrored to.
     *  @param cloneWeights If set true, the weights will also be cloned.
     *  @return The resulting mirror graph.
     */
    public Graph mirror(Graph graph, boolean cloneWeights);
}

