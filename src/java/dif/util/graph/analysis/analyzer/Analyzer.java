/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A base interface for all the analyzers. */
package dif.util.graph.analysis.analyzer;

//////////////////////////////////////////////////////////////////////////
//// Analyzer

import dif.util.graph.analysis.Analysis;

/**
 A base interface for all the analyzers.
 <p>
 @see Analysis
 @author Shahrooz Shahparnia
 @version $Id: Analyzer.java 1638 2007-04-07 14:06:24Z ssb $
 */
public interface Analyzer {
    /** Return a description of the analyzer.
     *
     *  @return Return a description of the analyzer.
     */
    public String toString();

    /** Return the validity state of the analyzer.
     *
     *  @return Return the validity state of the analyzer.
     */
    public boolean valid();
}

