/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A common interface for all the maximum profit to cost analyzers. */
package dif.util.graph.analysis.analyzer;

import dif.util.graph.analysis.MaximumProfitToCostRatioAnalysis;

import java.util.List;

//////////////////////////////////////////////////////////////////////////
//// MaximumProfitToCostRatioAnalyzer

/**
 A common interface for all the maximum profit to cost analyzers.
 <p>
 @see MaximumProfitToCostRatioAnalysis
 @author Shahrooz Shahparnia
 @version $Id: MaximumProfitToCostRatioAnalyzer.java 1638 2007-04-07 14:06:24Z ssb $
 */
public interface MaximumProfitToCostRatioAnalyzer extends GraphAnalyzer {
    /** Return the nodes on the cycle that corresponds to the maximum profit
     *  to cost ratio as an ordered list. If there is more than one cycle with
     *  the same maximal/minimal cycle, one of them is returned randomly,
     *  but the same cycle is returned by different invocations of the method,
     *  unless the graph changes.
     *
     *  @return Return the nodes on the cycle that corresponds to the maximum
     *  profit to cost ratio as an ordered list.
     */
    public List cycle();

    /** Return the maximum profit to cost ratio.
     *
     *  @return Return the maximum profit to cost ratio.
     */
    public double maximumRatio();
}

