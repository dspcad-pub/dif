/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Exception thrown due to incorrect graph topology.*/
package dif.util.graph;

//////////////////////////////////////////////////////////////////////////
//// GraphTopologyException

/**
 The exception thrown due to incorrect graph topology. This is for
 functions of graphs with required topologies. For example, acyclic property
 is required in {@link DirectedAcyclicGraph} and the topology checking is
 necessary.

 @author Mingyung Ko
 @version $Id: GraphTopologyException.java 1634 2007-04-06 18:24:07Z ssb $
 */
public class GraphTopologyException extends GraphException {
    /** Constructor with an argument of text description.
     *  @param message The exception message.
     */
    public GraphTopologyException(String message) {
        super(message);
    }
}

