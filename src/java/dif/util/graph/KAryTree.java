/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A generic k-ary tree structure.*/

package dif.util.graph;

import java.util.Collection;
import java.util.LinkedList;
import java.util.ListIterator;

import java.lang.RuntimeException;
import java.lang.IndexOutOfBoundsException;
import java.lang.IllegalArgumentException;

//////////////////////////////////////////////////////////////////////////
////K-Ary Tree

/** A generalized kAry tree.

 <p>kAry tree differs from Ordered tree in that, the child of a node
 has a positional information.  A node with a single child in ordered
 tree is different from that in k-Ary tree. In k-ary tree such a node
 can have a single child at any position with an index from
 <code>0</code> to <code>k - 1</code>.

 <p>All the methods in this class operate on the graph. Every time
 there is a change in the graph topology, the corresponding parent and
 children maps are updated accordingly.

 <p>In general, any type of <code>Collection</code> returned by any of
 the methods can be modified, unless stated otherwise.

 <p>Implementation notes:

 <p>Method that set the child of a node in a way that may yield a
 graph that is not a K-ary Tree --- <code>setChild()</code> and its
 variants, are no longer supported. If required, appropriate methods
 in any of the parent classes must be used along with
 <code>addChild()</code> and its variants.

 <p>All the methods that remove node(s) or edge(s) from the tree, may
 result in a graph that is no longer a tree. Hence, the methods that
 remove children are no longer supported. The safer way is to remove
 node(s) or edge(s) using the methods in the {@link Graph}
 class, followed by <code>buildRootedTree</code> method when the tree
 property holds.

 <p>At any point the propery of the K-ary tree must be maintained. If
 a method tries to alter this property, it will result in an
 exception.

 <p>Based on Introduction to Algorithms, Cormen et al.

 @author Nimish Sane, Shuvra S. Bhattacharyya
 @version $Id: Tree.java 416 2007-05-31 05:19:54Z plishker $
 @see Tree
 @see RootedTree
 @see OrderedTree
 */
public class KAryTree extends OrderedTree {

    /** Construct an empty  tree.
     */
    public KAryTree() {
	super();
	_k = 2;
    }

    /** Construct an empty k-ary tree with enough storage allocated for the
     *  specified number of nodes.  Memory management is more
     *  efficient with this constructor if the number of nodes is
     *  known.
     *  @param nodeCount The number of nodes.
     */
    public KAryTree(int nodeCount) {
        super(nodeCount);
	_k = 2;
    }

    /** Construct an empty k-ary tree with given node as its root.
     *  @param root Root node.
     */
    public KAryTree(Node root) {
	super(root);
	_k = 2;
    }

    /** Construct an empty k-ary tree with enough storage allocated
     *  for the specified number of nodes, with given node as its
     *  root.  Memory management is more efficient with this
     *  constructor if the number of nodes is known.
     *  @param nodeCount The number of nodes.
     *  @param root Root node.
     */
    public KAryTree(int nodeCount, Node root) {
        super(nodeCount, root);
	_k = 2;
    }

    /** Constructor for a given tree and root.
     *  @param tree The given tree.
     *  @param root The given root node.
     */
    public KAryTree(Tree tree, Node root) {
        super(tree, root);
	_k = 2;
    }

    /** Constructor for the given value of <code>k</code>
     *  @param k The value of <code>k</code>
     */
    /*
    public KAryTree(int k) {
	super();  
	_k = k;
    }
    */

    /** Constructor for the given value of <code>k</code> and rooted
     *  tree.
     *  @param k The value of <code>k</code>
     *  @param tree the given rooted tree.
     */
    public KAryTree(int k, RootedTree tree) {
	super();
	_k = k;
	_buildKAryTree(tree);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Return the number of non-null children of the node. The method
     *  returns 0 for the leaf nodes.
     *
     *  @param node The specified node.
     *
     *  @return Number of non-null children of the specified node.
     *
     *  @exception IllegalArgumentException If the specified node is
     *  null.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     */
    public int childrenCount(Node node) {
	if (node == null) {
	    throw new IllegalArgumentException ("The specified node cannot be null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

	LinkedList children = children(node);
	ListIterator iter = children.listIterator();
	int count = 0;
	while (iter.hasNext()) {
	    Node child = (Node)iter.next();
	    if (child != null) {
		count++;
	    }
	}

        return count;
    }

    /** Set the maximum number of children each node in the tree can
     *	have.
     *  @param k the maximum number of children for each node.
     *  @exception GraphTopologyException If the value of
     *  <code>k</code> is less than <code>2</code>.
     */
    public void setK (int k) {
	if (k < 2) {
	    throw new GraphTopologyException("The maximum number of children each node can have should be at least 2. \n");
	}

	_k = k;
    }

    /** Get the maximum number of children each node in the tree can
     *	have.
     *  @return k the maximum number of children for each node.
     */
    public int getK () {
	return _k;
    }

    /** <p>Checks if the specified list of children is a valid list.
     *
     *  <p>A valid list of children cannot have any child appearing
     *  more than once in the list except for the <code>null</code>
     *  node.
     *
     *  @param children List of children.
     *  @return True if the list of children is a valid list; false
     *  otherwise.
     */
    public boolean validateChildren(LinkedList children) {
	if (children == null) {
	    return false;
	}

	int size = children.size();
	if (size != _k) {
	    return false;
	}

	for (int i = 0; i < size; i++) {
	    Node node1 = (Node)children.get(i);
	    if (node1 != null) {
		for (int j = i + 1; j < size; j++) {
		    Node node2 = (Node)children.get(j);
		    if (node1 == node2) {
			return false;
		    }
		}
	    }
	}

	return true;
    }

    /** <p>Update the KAryTree for the given value of k.
     *
     * @param k the given value of <code>k</code>
     */
    void updateKAryTree(int k) {
	_k  =k;

	Collection nodesCol = nodes();
	LinkedList nodes = new LinkedList(nodesCol);
	ListIterator iter = nodes.listIterator();
	while (iter.hasNext()) {
	    Node node = (Node)iter.next();
	    if (!isLeaf(node)) {
		LinkedList children = children(node);
		int size = children.size();
		if (children.size() > _k) {
		    throw new GraphTopologyException("Cannot build k-ary tree for k = " + _k + "\n");
		}

		for (int i = size; i < _k; i++) {
		    children.addLast(null);
		}

		this.updateChildren(node, children);
	    }
	}
    }

    /** Sets the specified child in the existing list of children at
     *  the specified index. This has no effect on actual graph
     *  topology.
     *  @param node The rooted tree node.
     *  @param child The child node.
     *  @param index The given index of the child node
     *
     *  @exception IllegalArgumentException If the specified node or
     *  the child node are null.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     *  @exception IndexOutOfBoundsException If the specified index is
     *  less than 0 or more than <code>k - 1</code>.
     */
    public void updateChild(Node node, Node child, int index) {
	if (node == null || child == null) {
	    throw new IllegalArgumentException ("The specified node and the child node must not be null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

	if (index < 0 || index >= _k) {
	    throw new IndexOutOfBoundsException("The index must be a non-negative integer less than " + _k  + ". \n");
	}

	LinkedList children = children(node);
	children.set(index, child);
	updateChildren(node, children);
    }

    /** <p>Inserts the specified <i>child</i> as the first child of
     *  the specified <i>parent</i>'s list of children, if the
     *  specified <i>parent</i> or <i>child</i> nodes are not null,
     *  and the <i>parent</i> node is in the
     *  <code>KAryTree</code>. If both the <i>child</i> and
     *  <i>parent</i> nodes are in the <code>KAryTree</code>, then
     *  the specified <i>child</i> must not be an ancestor of the
     *  <i>parent</i> node.
     *
     *  <p>See {@link #addChild(Node, Node, int)}
     *  for further details.
     *
     *  @param parent The specified parent node.
     *  @param child The specified child node to be added.
     *
     *  @exception IllegalArgumentException If the parent or the child
     *  node is null.
     *  @exception GraphElementException If the parent node is not in
     *  tree.
     *  @exception GraphTopologyException If the <i>child</i> is an
     *  ancestor of the <i>parent</i>.
     */
    public void addChildFirst(Node parent, Node child) {
	addChild(parent, child, 0);
    }

    /** <p>Adds the specified <i>child</i> to the specified
     *  <i>parent</i>'s list of children at the <code>k</code>th
     *  position, if the specified <i>parent</i> or <i>child</i> nodes
     *  are not null, and the <i>parent</i> node is in the
     *  <code>KAryTree</code>. If both the <i>child</i> and
     *  <i>parent</i> nodes are in the <code>KAryTree</code>, then the
     *  specified <i>child</i> must not be an ancestor of the
     *  <i>parent</i> node.
     *
     *  <p>See {@link #addChild(Node, Node, int)}
     *  for further details.
     *
     *  @param parent The specified parent node.
     *  @param child The specified child node to be added.
     *
     *  @exception IllegalArgumentException If the parent or the child
     *  node is null.
     *  @exception GraphElementException If the parent node is not in
     *  tree.
     *  @exception GraphTopologyException If the <i>child</i> is an
     *  ancestor of the <i>parent</i>.
     */
    public void addChildLast(Node parent, Node child) {
	addChild(parent, child, _k - 1);
    }

    /** <p>Inserts the specified children at the end of the specified
     *  parent's list of children. Children are added serially and
     *  hence the effect is same as calling {@link #addChild(Node, Node, int)}
     *  method as many times as the number of children in the
     *  specified list with index starting from <i>k - number of
     *  children being added</i> and incrementing it by <code>1</code>
     *  every time.
     *
     *  <p>See {@link #addChildren(Node, LinkedList, int)}
     *  for further details.
     *
     *  @param parent The specified parent node.
     *  @param children List of children to be added.
     *
     *  @exception IllegalArgumentException If the specified parent or
     *  list of children are null.
     *  @exception GraphElementException If the parent node is not in the tree.
     *  @exception RuntimeException If the specified list of children is invalid.
     *  @exception GraphTopologyException If any of the
     *  <i>children</i> is an ancestor of the <i>parent</i>.
     */
    public void addChildrenLast(Node parent, LinkedList children) {
	int startIndex = _k - children.size();
	addChildren(parent, children, startIndex);
    }

    /** <p>Sets the specified <i>child</i> at the specified
     *  <i>index</i> in the specified <i>parent</i>'s list of children
     *  if the specified <i>parent</i> or <i>child</i> nodes are not
     *  null, the <i>parent</i> node is in the
     *  <code>RootedTree</code>, and there does not exist a non-null
     *  child node at the specified <i>index</i>. The specified index
     *  must be between <code>0</code> and <code>k - 1</code>, both
     *  inclusive. If both the <i>child</i> and <i>parent</i> nodes
     *  are in the <code>RootedTree</code>, then the specified
     *  <i>child</i> must not be an ancestor of the <i>parent</i>
     *  node. The node present
     *
     *  <p>If the specified <i>child</i> is not present in the rooted
     *  tree, then it is added to the rooted tree along with an edge
     *  between the <i>parent</i> and the <i>child</i> node. The
     *  parent and children maps are modified accordingly.
     *
     *  <p>If the <i>child</i> node is in the <code>KAryTree</code>,
     *  but not in the <i>parent</i>'s list of children, then the
     *  existing edge between the <i>child</i> node and its current
     *  parent is removed. A new edge is added between the specified
     *  <i>parent</i> and the <i>child</i>. The parent and children
     *  maps are modified accordingly.
     *
     *  <p>For the <i>child</i> node that is already one of the
     *  children of the <i>parent</i> node, it is replaced by a null
     *  node. Note that this order of children may differ from the
     *  order of edges in the <code>_incidentEdgeMap</code> in the
     *  {@link Graph}.
     *
     *  @param parent The specified parent node.
     *  @param child The specified child node to be added.
     *  @param index The index at which the specified child must be
     *  inserted in the parent's list of children.
     *
     *  @exception IllegalArgumentException If the parent or the child
     *  node is null.
     *  @exception GraphElementException If the parent node is not in
     *  tree or there exists a non-null child node at the specified
     *  <i>index</i>.
     *  @exception IndexOutOfBoundsException If the specified index is
     *  not between <code>0</code> and <code>k - 1</code> (both
     *  inclusive).
     *  @exception GraphTopologyException If the <i>child</i> is an
     *  ancestor of the <i>parent</i>.
     */
    public void addChild(Node parent, Node child, int index) {
	if (parent == null || child == null) {
	    throw new IllegalArgumentException ("The specified parent or child is null. \n");
	}

	if (!containsNode(parent)) {
	    throw new GraphElementException ("The specified parent is not in the rooted tree. \n");
	}

	if (getChild(parent, index) != null) {
	    throw new GraphElementException ("Attempting to replace a non-null node. \n");
	}

	LinkedList children = children(parent);
	if (index < 0 || index >= _k) {
	    throw new IndexOutOfBoundsException ("The specified index must be between 0 and " + _k + " . \n");
	}

	if (containsNode(child)) {
	    if (isAncestor(child, parent)) {
		throw new GraphTopologyException ("The specified child is an ancestor of the specified parent node. \n");
	    }

	    if (!children.contains(child)) {
		/* Remove the existing edge between the child and its
		 * current parent (i.e. the parent before this method
		 * is called). Add a new edge between the specified
		 * parent and the child. Update the parent and
		 * children information for the affacted nodes
		 * accordingly. */
		Node currentParent = parent(child);
		Edge edge = new Edge(currentParent, child);

		removeEdge(edge);
		addEdge(parent, child);

		LinkedList currentParentChildren = children(currentParent);
		int childIndex = currentParentChildren.indexOf(child);
		currentParentChildren.set(childIndex, null);
		updateChildren(currentParent, currentParentChildren);

		updateParent(child, parent);
		updateChild(parent, child, index);
	    } else {
		updateChild(parent, null, indexOfChild(child));
		updateChild(parent, child, index);
	    }
	} else {
	    /* Add the child to the tree along with the edge between
	     * parent and the child. Update the parent and children
	     * information for the affacted nodes accordingly. */
	    addNode(child);
	    addEdge(parent, child);

	    updateParent(child, parent);
	    updateChild(parent, child, index);
	}

	/* Get the updated list of children */
	children = children(parent);

	/* Update Leaf nodes. */
	_updateLeafNodes();
    }

    /** <p>Inserts the specified children in the specified parent's
     *  list of children starting from the specified index. Children
     *  are added serially and hence the effect is same as calling
     *  {@link #addChild(Node, Node, int)} method
     *  as many times as the number of children in the specified list
     *  starting from the specified index and incrementing it by
     *  <code>1</code> every time.
     *
     *  <p>The specified list of children must be a valid list of
     *  children. For details about the valid list of children, see
     *  {@link #validateChildren(LinkedList)}.
     *  The list of children should not contain any child that is an
     *  ancestor of the specified parent. Also, if any child being
     *  added is already a child of the parent node, then its addition
     *  has the same effect as that of adding it using the method
     *  {@link #addChild(Node, Node, int)}.
     *
     *  @param parent The specified parent node.
     *  @param children List of children to be added.
     *  @param index The index at which the specified children must be added.
     *
     *  @exception IllegalArgumentException If the specified parent or
     *  list of children are null.
     *  @exception GraphElementException If the parent node is not in the tree.
     *  @exception IndexOutOfBoundsException If the specified index is
     *  not between <code>0</code> and <code>k - n</code> (both
     *  inclusive), where <code>n</code> is the current number of
     *  children of the <i>parent</i>.
     *  @exception RuntimeException If the specified list of children is invalid.
     *  @exception GraphTopologyException If any of the
     *  <i>children</i> is an ancestor of the <i>parent</i>.
     */
    public void addChildren(Node parent, LinkedList children, int index) {
	if (parent == null || children == null) {
	    throw new IllegalArgumentException ("The specified parent or list of children are null. \n");
	}

	if (!containsNode(parent)) {
	    throw new GraphElementException ("The specified parent node is not in the tree. \n");
	}

	int maxIndex = _k - children.size();

	if (children.size() != 0) {
	    if (index < 0 || index > maxIndex) {
		throw new IndexOutOfBoundsException ("The specified index must be between 0 and " + maxIndex + " . \n");
	    }

	    if (!validateChildren(children)) {
		throw new RuntimeException ("Invalid list of children. \n");
	    }

	    // Add all the elements of the specified list of children serially.
	    ListIterator iter = children.listIterator();
	    while (iter.hasNext()) {
		Node child = (Node)iter.next();
		addChild(parent, child, index++);
	    }
	}
    }

    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////

    /** <p>This method constructs a k-ary tree from the given rooted
     * tree by appending all the children lists with <code>null</code>
     * nodes to have exactly <code>k</code> elements in them.
     *
     * @param tree the given rooted tree
     * @return k-Ary tree.
     */
    void _buildKAryTree(RootedTree tree) {
	this.addGraph(tree);
	this.buildRootedTree(tree, tree.root());
	this.updateKAryTree(_k);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         private variables                 ////

    /** Number of children each node can have. Default is a binary
     * tree.
     */
    private int _k;
}
