/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A base class for graph elements (nodes and edges).*/
package dif.util.graph;

////////////////////////////////////////////////////////////////////////// //
//Element

/**
 * A base class for graph elements (nodes and edges).
 * A graph element consists of an optional <i>weight</i> (an arbitrary
 * object that is associated with the element).  We say that an element is
 * <i>unweighted</i> if it does not have an assigned weight. It is an error to
 * attempt to access the weight of an unweighted element. Element weights must
 * be non-null objects.
 *
 * @author Shuvra S. Bhattacharyya
 * @version $Id: Element.java 1634 2007-04-06 18:24:07Z ssb $
 * @see Edge
 * @see Node
 */
public abstract class Element {
    /**
     * Construct an unweighted element.
     */
    public Element() {
        _weight = null;
        _name = "element";
    }

    /**
     * Construct an element with a given weight.
     *
     * @param weight The given weight.
     * @throws IllegalArgumentException If the specified weight is
     *                                  <code>null</code>.
     */
    public Element(Object weight) {
        setWeight(weight);
        _name = "element";
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /**
     * A one-word description of the type of this graph element.
     *
     * @return The description.
     */
    public String descriptor() {
        return "element";
    }

    /**
     * Return the weight that has been associated with this element.
     *
     * @return The associated weight.
     * @throws IllegalStateException If this is an unweighted element.
     * @see #setWeight(Object)
     */
    public final Object getWeight() {
        if (!hasWeight()) {
            throw new IllegalStateException("Attempt to access the weight "
                    + "of the following unweighted " + descriptor() + ": "
                    + this + "\n");
        } else {
            return _weight;
        }
    }

    /**
     * Return <code>true</code> if and only if this is a weighted element.
     *
     * @return True if and only if this is a weighted element.
     */
    public final boolean hasWeight() {
        return _weight != null;
    }

    /**
     * Make the element unweighted. This method should be used with
     * caution since it may make the element incompatible with graphs that
     * already contain it. The method has no effect if the element is already
     * unweighted.
     *
     * @see Graph#validEdgeWeight(Object)
     * @see Graph#validNodeWeight(Object)
     * @see Graph#validateWeight(Node)
     */
    public final void removeWeight() {
        // FIXME: add @see Graph#validateWeight(Edge)
        _weight = null;
    }

    /**
     * Set or change the weight of an element. This method should be used with
     * caution since it may make the element incompatible with graphs that
     * already contain it.
     *
     * @param weight The new weight.
     * @throws IllegalArgumentException If the object that is passed as
     *                                  argument is null.
     * @see Graph#validEdgeWeight(Object)
     * @see Graph#validNodeWeight(Object)
     * @see Graph#validateWeight(Node)
     * @see #getWeight()
     */
    public final void setWeight(Object weight) {
        if (weight == null) {
            throw new IllegalArgumentException("Attempt to assign a null "
                    + "weight to the following " + descriptor() + ": " + this
                    + "\n");
        } else {
            _weight = weight;
        }
    }

    /**
     * Set element name.
     *
     * @param name A Element name string.
     */
    public void setName(String name) {
        _name = name;
    }

    /**
     * Get element name.
     *
     * @return the element name string.
     */
    public String name() {
        return _name;
    }

    /**
     * The weight that is associated with the element if the element is
     * weighted. If the element is not weighted,  the value of this
     * field is null.
     */
    protected Object _weight;

    /**
     * Element name.
     */
    private String _name;
}

