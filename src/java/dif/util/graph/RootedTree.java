/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A generic rooted tree structure.*/

package dif.util.graph;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Collection;

import java.lang.RuntimeException;
import java.lang.IndexOutOfBoundsException;
import java.lang.IllegalArgumentException;
import java.util.NoSuchElementException;

//////////////////////////////////////////////////////////////////////////
////Rooted Tree

/** A generalized rooted tree.

 <p>This class implements a rooted tree data structure. Rooted tree is a tree
 with one of its node identified as the root of the tree.

 <p>All the methods in this class operate on the graph. Every time there is a
 change in the graph topology, the corresponding parent and children maps are
 updated accordingly.

 <p>In general, any type of <code>Collection</code> returned by any of
 the methodscan be modified, unless stated otherwise.

 <p>The field <code>_nodeChildrenMap</code> maps the rooted tree nodes to their
 list of children nodes. The ordering of nodes in this linked list may be
 important for certain types of trees. Note that the order of nodes in the
 linked list in this map may differ from the order of edges in the
 <code>_incidentEdgeMap</code> in the {@link Graph}. All the derived
 classes that have rooted tree structure must use methods that operate on parent
 and children maps in this class.

 <p>Implementation notes:

 <p>Note that the list of children of a leaf node is an empty
 LinkedList and not a null pointer. It is in general invalid to have
 list of children set to null.

 <p>Method that set the child of a node in a way that may yield a
 graph that is not a tree --- <code>setChild()</code> and its
 variants, are no longer supported. If required, appropriate methods
 in any of the parent classes must be used along with
 <code>addChild()</code> and its variants.

 <p>All the methods that remove node(s) or edge(s) from the tree, may
 result in a graph that is no longer a tree. Hence, the methods that
 remove children are no longer supported. The safer way is to remove
 node(s) or edge(s) using the methods in the {@link Graph}
 class, followed by <code>buildRootedTree</code> method when the tree
 property holds.

 <p>Based on Introduction to Algorithms, Cormen et al.

 @author Nimish Sane, Shuvra S. Bhattacharyya
 @version $Id: Tree.java 416 2007-05-31 05:19:54Z plishker $
 @see Graph
 @see Tree
 */

public class RootedTree extends Tree {

    /** Construct an empty rooted tree.
     */
    public RootedTree() {
	super();
	_initializeRootedTree();
    }

    /** Construct an empty rooted tree with enough storage allocated for the
     *  specified number of nodes.  Memory management is more
     *  efficient with this constructor if the number of nodes is
     *  known.
     *  @param nodeCount The number of nodes.
     */
    public RootedTree(int nodeCount) {
        super(nodeCount);
	_initializeRootedTree();
    }

    /** Construct a rooted tree with given node as its root.
     *  @param root Root Node
     */
    public RootedTree(Node root) {
	super();
	_initializeRootedTree();

	if (root != null) {
	    addNode(root);
	    _root = root;
	    LinkedList children = new LinkedList();
	    _nodeParentMap.put(root, null);
	    _nodeChildrenMap.put(root, children);
	    _leafNodes.add(root);
	}
    }

    /** Construct a rooted tree with given root and enough storage allocated for
     *  the specified number of nodes.  Memory management is more efficient with
     *  this constructor if the number of nodes is known.
     *  @param nodeCount The number of nodes.
     *  @param root Root node.
     */
    public RootedTree(int nodeCount, Node root) {
        super(nodeCount);
	_initializeRootedTree();

	if (root != null) {
	    addNode(root);
	    _root = root;
	    LinkedList children = new LinkedList();
	    _nodeParentMap.put(root, null);
	    _nodeChildrenMap.put(root, children);
	    _leafNodes.add(root);
	}
    }

    /** Constructor for a given tree and root, where root is already
     *  in the tree.  This constructor could be used to instantiate a
     *  RootedTree from a given Tree and the root node.
     *  @param tree The given tree.
     *  @param root The given root.
     *  @exception IllegalArgumentException If the tree or root is null.
     *  @exception NoSuchElementException If the root is not in the tree.
     */
    public RootedTree(Tree tree, Node root) {
	super();

	if ((tree == null) ^ (root == null)) {
	    throw new IllegalArgumentException("Input tree and root node cannot be exclusively null. \n");
	}

	_initializeRootedTree();

	if (tree != null) {
	    if (!tree.containsNode(root)) {
		throw new NoSuchElementException("Root is not in the tree. \n");
	    }

	    addGraph(tree);
	    _root = root;
	    _updateRootedTree(this, _root);
	}
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Returns the list of children of the given node.
     *  @param node The tree node.
     *  @return the list of children of the given node.
     *
     *  @exception IllegalArgumentException If the spcified node is a
     *  null node.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     */
    public LinkedList children(Node node) {
	if (node == null) {
	    throw new IllegalArgumentException ("The specified node must not be null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

	return (LinkedList)_nodeChildrenMap.get(node);
    }

    /** Returns the parent of the given node.
     *  @param node The tree node.
     *  @return the parent of the given node.
     *
     *  @exception IllegalArgumentException If the spcified node is a null node.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     */
    public Node parent(Node node) {
	if (node == null) {
	    throw new IllegalArgumentException ("The specified node must not be null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

	return (Node)_nodeParentMap.get(node);
    }

    /** Replaces the current parent of the specified node with the
     *  specified one. This has no effect on actual graph topology.
     *  @param node The rooted tree node.
     *  @param parent The new parent of the specified node.
     *
     *  @exception IllegalArgumentException If the specified node is
     *  null.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     */
    public void updateParent(Node node, Node parent) {
	if (node == null) {
	    throw new IllegalArgumentException ("The specified node must not be null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}


	_nodeParentMap.put(node, parent);
    }

    /** Replaces the current list of children with the specified
     *  one. This has no effect on actual graph topology.
     *  @param node The rooted tree node.
     *  @param children The new list of children.
     *
     *  @exception IllegalArgumentException If the specified node or
     *  list of children is null.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     */
    public void updateChildren(Node node, LinkedList children) {
	if (node == null || children == null) {
	    throw new IllegalArgumentException ("The specified node and list of children must not be null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

	_nodeChildrenMap.put(node, children);
    }

    /** Adds the specified child to the existing list of
     *  children. This has no effect on actual graph topology.
     *  @param node The rooted tree node.
     *  @param child The child node.
     *
     *  @exception IllegalArgumentException If the specified node or
     *  the child node are null.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     */
    public void updateChild(Node node, Node child) {
	if (node == null || child == null) {
	    throw new IllegalArgumentException ("The specified node and the child node must not be null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

	if (!_nodeChildrenMap.containsKey(node)) {
	    LinkedList childrenNew = new LinkedList();
	    updateChildren(node, childrenNew);
	}

	LinkedList children = children(node);
	children.add(child);
	updateChildren(node, children);
    }

    /** <p>Checks if the specified list of children is a valid list.
     *
     *  <p>A valid list of children for a rooted tree node cannot have
     *  a null node and any child appearing more than once in the
     *  list.
     *
     *  @param children List of children.
     *  @return True if the list of children is a valid list; false
     *  otherwise.
     */
    public boolean validateChildren(LinkedList children) {
	if (children == null) {
	    return false;
	}

	int size = children.size();
	for (int i = 0; i < size; i++) {
	    Node node1 = (Node)children.get(i);
	    if (node1 == null) {
		return false;
	    } else {
		for (int j = i + 1; j < size; j++) {
		    Node node2 = (Node)children.get(j);
		    if (node2 == null || node1 == node2) {
			return false;
		    }
		}
	    }
	}

	return true;
    }

    /** Check if node1 is an ancestor of node2.
     *  @param node1 A graph node.
     *  @param node2 A graph node.
     *
     *  @return True if <i>node1</i> is an ancestor of <i>node2</i>,
     *  false otherwise.
     *
     *  @exception IllegalArgumentException If <i>node1</i> or
     *  <i>node2</i> is null.
     *  @exception NoSuchElementException If <i>node1</i> or
     *  <i>node2</i> is not in the graph.
     */
    public boolean isAncestor(Node node1, Node node2) {
	if (node1 == null || node2 == null) {
	    throw new IllegalArgumentException ("The input node mosy not be null. \n");
	}

	if (!(containsNode(node1) && containsNode(node2))) {
	    throw new NoSuchElementException ("The specified node(s) are not in the graph. \n");
	}

	Node node = parent(node2);

	while(node != null) {
	    if (node == node1) {
		return true;
	    }

	    node = parent(node);
	}

	return false;
    }

    /** <p>Inserts the specified <i>child</i> at the specified
     *  <i>index</i> in the specified <i>parent</i>'s list of
     *  children, if the specified <i>parent</i> or <i>child</i> nodes
     *  are not null, and the <i>parent</i> node is in the
     *  <code>RootedTree</code>. If the valid <i>parent</i> node
     *  currently has <code>k</code> children, then the specified
     *  index must be between <code>0</code> and <code>k</code>, both
     *  inclusive. If both the <i>child</i> and <i>parent</i> nodes
     *  are in the <code>RootedTree</code>, then the specified
     *  <i>child</i> must not be an ancestor of the <i>parent</i>
     *  node.
     *
     *  <p>If the specified <i>child</i> is not present in the rooted
     *  tree, then it is added to the rooted tree along with an edge
     *  between the <i>parent</i> and the <i>child</i> node. The
     *  parent and children maps are modified accordingly.
     *
     *  <p>If the <i>child</i> node is in the <code>RooteTree</code>,
     *  but not in the <i>parent</i>'s list of children, then the
     *  existing edge between the <i>child</i> node and its current
     *  parent is removed. A new edge is added between the specified
     *  <i>parent</i> and the <i>child</i>. The parent and children
     *  maps are modified accordingly.
     *
     *  <p>For the <i>child</i> node that is already one of the
     *  children of the <i>parent</i> node, the list of children is
     *  re-ordered in a way that the given <i>child</i> has the
     *  specified index while maintaining the exisiting partial
     *  ordering between other children i.e. the children starting
     *  from the one at the specified <i>index</i> (if any) are
     *  shifted to have an index one greater than their current
     *  index. Note that this order of children may differ from the
     *  order of edges in the <code>_incidentEdgeMap</code> in the
     *  {@link Graph}.
     *
     *  @param parent The specified parent node.
     *  @param child The specified child node to be added.
     *  @param index The index at which the specified child must be
     *  inserted in the parent's list of children.
     *
     *  @exception IllegalArgumentException If the parent or the child
     *  node is null.
     *  @exception GraphElementException If the parent node is not in
     *  tree.
     *  @exception IndexOutOfBoundsException If the specified index is
     *  not between <code>0</code> and <code>k</code> (both
     *  inclusive), where <code>k</code> is the current number of
     *  children of the <i>parent</i>.
     *  @exception GraphTopologyException If the <i>child</i> is an
     *  ancestor of the <i>parent</i>.
     */
    public void addChild(Node parent, Node child, int index) {
	if (parent == null || child == null) {
	    throw new IllegalArgumentException ("The specified parent or child is null. \n");
	}

	if (!containsNode(parent)) {
	    throw new GraphElementException ("The specified parent is not in the rooted tree. \n");
	}

	LinkedList children = children(parent);
	if (index < 0 || index > children.size()) {
	    throw new IndexOutOfBoundsException ("The specified index must be between 0 and " + children.size() + " . \n");
	}

	if (containsNode(child)) {
	    if (isAncestor(child, parent)) {
		throw new GraphTopologyException ("The specified child is an ancestor of the specified parent node. \n");
	    }

	    if (!children.contains(child)) {
		/* Remove the existing edge between the child and its
		 * current parent (i.e. the parent before this method
		 * is called). Add a new edge between the specified
		 * parent and the child. Update the parent and
		 * children information for the affacted nodes
		 * accordingly. */
		Node currentParent = parent(child);
		Edge edge = new Edge(currentParent, child);
		removeEdge(edge);
		addEdge(parent, child);

		LinkedList currentParentChildren = children(currentParent);
		currentParentChildren.remove(child);
		updateChildren(currentParent, currentParentChildren);

		updateParent(child, parent);
		updateChild(parent, child);
	    }
	} else {
	    /* Add the child to the tree along with the edge between
	     * parent and the child. Update the parent and children
	     * information for the affacted nodes accordingly. */
	    addNode(child);
	    addEdge(parent, child);

	    updateParent(child, parent);
	    updateChild(parent, child);
	}

	/* Get the updated list of children */
	children = children(parent);

	/* The specified child is already a child of the specified
	 * parent. If the current index of the child differs from the
	 * specified index, re-order children and set it as the new
	 * list of children for the parent. */
	if (index != children.indexOf(child)) {
	    children.remove(child);
	    children.add(index, child);
	    updateChildren(parent, children);
	}

	/* Update Leaf nodes. */
	_updateLeafNodes();
    }

    /** <p>Inserts the specified <i>child</i> as the first child of
     *  the specified <i>parent</i>'s list of children, if the
     *  specified <i>parent</i> or <i>child</i> nodes are not null,
     *  and the <i>parent</i> node is in the
     *  <code>RootedTree</code>. If both the <i>child</i> and
     *  <i>parent</i> nodes are in the <code>RootedTree</code>, then
     *  the specified <i>child</i> must not be an ancestor of the
     *  <i>parent</i> node.
     *
     *  <p>
     *  @see #addChild(Node, Node, int) for further details.
     *
     *  @param parent The specified parent node.
     *  @param child The specified child node to be added.
     *
     *  @exception IllegalArgumentException If the parent or the child
     *  node is null.
     *  @exception GraphElementException If the parent node is not in
     *  tree.
     *  @exception GraphTopologyException If the <i>child</i> is an
     *  ancestor of the <i>parent</i>.
     */
    public void addChildFirst(Node parent, Node child) {
	addChild(parent, child, 0);
    }

    /** <p>Appends the specified <i>child</i> to the specified
     *  <i>parent</i>'s list of children, if the specified
     *  <i>parent</i> or <i>child</i> nodes are not null, and the
     *  <i>parent</i> node is in the <code>RootedTree</code>. If both
     *  the <i>child</i> and <i>parent</i> nodes are in the
     *  <code>RootedTree</code>, then the specified <i>child</i> must
     *  not be an ancestor of the <i>parent</i> node.
     *
     *  <p>@see mocgraph.RootedTree.#addChild(Node, Node, int)
     *  for further details.
     *
     *  @param parent The specified parent node.
     *  @param child The specified child node to be added.
     *
     *  @exception IllegalArgumentException If the parent or the child
     *  node is null.
     *  @exception GraphElementException If the parent node is not in
     *  tree.
     *  @exception GraphTopologyException If the <i>child</i> is an
     *  ancestor of the <i>parent</i>.
     */
    public void addChildLast(Node parent, Node child) {
	int lastIndex = children(parent).size();
	addChild(parent, child, lastIndex);
    }

    /** <p>Inserts the specified children in the specified parent's
     *  list of children starting from the specified index. Children
     *  are added serially and hence the effect is same as calling
     *  {@link #addChild(Node, Node, int)} method
     *  as many times as the number of children in the specified list
     *  starting from the specified index and incrementing it by
     *  <code>1</code> every time.
     *
     *  <p>The specified list of children must be a valid list of
     *  children. For details about the valid list of children, see
     *  {@link #validateChildren(LinkedList)}.
     *  The list of children should not contain any child that is an
     *  ancestor of the specified parent. Also, if any child being
     *  added is already a child of the parent node, then its addition
     *  has the same effect as that of adding it using the method
     *  {@link #addChild(Node, Node, int)}.
     *
     *  @param parent The specified parent node.
     *  @param children List of children to be added.
     *  @param index The index at which the specified children must be added.
     *
     *  @exception IllegalArgumentException If the specified parent or
     *  list of children are null.
     *  @exception GraphElementException If the parent node is not in the tree.
     *  @exception IndexOutOfBoundsException If the specified index is
     *  not between <code>0</code> and <code>k</code> (both
     *  inclusive), where <code>k</code> is the current number of
     *  children of the <i>parent</i>.
     *  @exception RuntimeException If the specified list of children is invalid.
     *  @exception GraphTopologyException If any of the
     *  <i>children</i> is an ancestor of the <i>parent</i>.
     */
    public void addChildren(Node parent, LinkedList children, int index) {
	if (parent == null || children == null) {
	    throw new IllegalArgumentException ("The specified parent or list of children are null. \n");
	}

	if (!containsNode(parent)) {
	    throw new GraphElementException ("The specified parent node is not in the tree. \n");
	}

	if (children.size() != 0) {
	    if (index < 0 || index > children(parent).size()) {
		throw new IndexOutOfBoundsException ("The specified index must be between 0 and " + children.size() + " . \n");
	    }

	    if (!validateChildren(children)) {
		throw new RuntimeException ("Invalid list of children. \n");
	    }

	    // Add all the elements of the specified list of children serially.
	    ListIterator iter = children.listIterator();
	    while (iter.hasNext()) {
		Node child = (Node)iter.next();
		addChild(parent, child, index++);
	    }
	}
    }

    /** <p>Inserts the specified children at the beginhing of the
     *  specified parent's list of children. Children are added
     *  serially and hence the effect is same as calling
     *  {@link #addChild(Node, Node, int)}
     *  method as many times as the number of children in the
     *  specified list with index starting from <code>0</code> and
     *  incrementing it by <code>1</code> every time.
     *
     *  <p>See {@link #addChildren(Node, LinkedList, int)}
     *  for further details.
     *
     *  @param parent The specified parent node.
     *  @param children List of children to be added.
     *
     *  @exception IllegalArgumentException If the specified parent or
     *  list of children are null.
     *  @exception GraphElementException If the parent node is not in the tree.
     *  @exception RuntimeException If the specified list of children is invalid.
     *  @exception GraphTopologyException If any of the
     *  <i>children</i> is an ancestor of the <i>parent</i>.
     */
    public void addChildrenFirst(Node parent, LinkedList children) {
	addChildren(parent, children, 0);
    }

    /** <p>Inserts the specified children at the end of the specified
     *  parent's list of children. Children are added serially and
     *  hence the effect is same as calling {@link #addChild(Node, Node, int)}
     *  method as many times as the number of children in the
     *  specified list with index starting from <code>k</code> and
     *  incrementing it by <code>1</code> every time, where
     *  <code>k</code> is the current number of children of the parent
     *  node.
     *
     *  <p>See {@link #addChildren(Node, LinkedList, int)}
     *  for further details.
     *
     *  @param parent The specified parent node.
     *  @param children List of children to be added.
     *
     *  @exception IllegalArgumentException If the specified parent or
     *  list of children are null.
     *  @exception GraphElementException If the parent node is not in the tree.
     *  @exception RuntimeException If the specified list of children is invalid.
     *  @exception GraphTopologyException If any of the
     *  <i>children</i> is an ancestor of the <i>parent</i>.
     */
    public void addChildrenLast(Node parent, LinkedList children) {
	int lastIndex = children(parent).size();
	addChildren(parent, children, lastIndex);
    }

    /** Get the child located at the specified index for the specified
     *  node.
     *
     *  @param node The specified node.
     *  @param index Index of the child to be retrieved.
     *
     *  @return The child at the specified index for the specified
     *  node.
     *
     *  @exception IllegalArgumentException If the specified node is
     *  null.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     *  @exception IndexOutOfBoundsException If the specified index is
     *  less than <code>0</code> or greater than <code>k - 1</code>,
     *  where <code>k</code> is the current number of children of the
     *  <i>node</i>.
     */
    public Node getChild(Node node, int index) {
	if (node == null) {
	    throw new IllegalArgumentException ("The specified node cannot be null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

	LinkedList children = children(node);
	if (index < 0 || index >= children.size()) {
	    throw new IndexOutOfBoundsException ("The specified index must be a non-negative integer less than " + children.size() + " . \n");
	}

	return (Node)children.get(index);
    }

    /** Get the first child of the specified node.
     *
     *  @param node The specified node.
     *
     *  @return The first child of the specified node.
     *
     *  @exception IllegalArgumentException If the specified node is
     *  null.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     *  @exception IndexOutOfBoundsException If the specified node is
     *  a leaf node.
     */
    public Node getChildFirst(Node node) {
	return getChild(node, 0);
    }

    /** Get the last child of the specified node.
     *
     *  @param node The specified node.
     *
     *  @return The last child of the specified node.
     *
     *  @exception IllegalArgumentException If the specified node is
     *  null.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     *  @exception IndexOutOfBoundsException If the specified node is
     *  a leaf node.
     */
    public Node getChildLast(Node node) {
	return getChild(node, children(node).size() - 1);
    }

    /** Return an iterator over the list of children of the specified node.
     *
     *  @param node The specified node.
     *
     *  @return An iterator over the list of children of the specified node.
     *
     *  @exception IllegalArgumentException If the specified node is
     *  null.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     */
    public ListIterator childrenIterator(Node node) {
	if (node == null) {
	    throw new IllegalArgumentException ("The specified node cannot be null. \n");
	}

	return children(node).listIterator();
    }

    /** Return an iterator over the list of children of the given node
     *  starting at the given index.
     *
     *  @param node The specified node.
     *  @param index The given index.
     *
     *  @return An iterator over the list of children of the given
     *  node starting at the given index.
     *
     *  @exception IllegalArgumentException If the specified node is
     *  null.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     *  @exception IndexOutOfBoundsException If the specified index is
     *  less than <code>0</code> or greater than <code>k - 1</code>,
     *  where <code>k</code> is the current number of children of the
     *  <i>node</i>.
     */
    public ListIterator childrenIterator(Node node, int index) {
	if (node == null) {
	    throw new IllegalArgumentException ("The specified node cannot be null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

	if (index < 0 || index >= childrenCount(node)) {
	    throw new IndexOutOfBoundsException ("The specified index must be a non-negative integer less than " + childrenCount(node) + " . \n");
	}

	return children(node).listIterator(index);
    }

    /** Return an iterator over the list of children of the specified parent
     *  node starting with the given child.
     *
     *  @param parent The given parent.
     *  @param child The given child.
     *
     *  @return An iterator over the list of children of the parent node
     *  starting with the given child.
     *
     *  @exception IllegalArgumentException If the specified parent or child is
     *  null.
     *  @exception GraphElementException If the specified nodes are
     *  not in the tree.
     *  @exception NoSuchElementException If the specified child is
     *  not the child of the parent.
     */
    public ListIterator childrenIterator(Node parent, Node child) {
	if (parent == null | child == null) {
	    throw new IllegalArgumentException ("The specified parent or child node is null. \n");
	}

	if (!(containsNode(parent) && containsNode(child))) {
	    throw new GraphElementException ("The specified parent or child node is not in the tree. \n");
	}

	if (parent(child) != parent) {
	    throw new NoSuchElementException ("The specified child is not the child of the specified parent. \n");
	}

	LinkedList children = children(parent);
	int index = children.indexOf(child);

	return children.listIterator(index);
    }

    /** Return index of the specified child in the list of its parent's children
     *  with 0 for the leftmost (first) child. The method returns 0 for the root
     *  node.
     *
     *  @param node The specified node.
     *  @return index of the first occurence of specified node;
     *
     *  @exception IllegalArgumentException If the specified node is null.
     *  @exception GraphElementException If the specified node is not in the tree.
     */
    public int indexOfChild(Node node) {
	if (node == null) {
	    throw new IllegalArgumentException ("The specified node cannot be null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node in not in the tree. \n");
	}

	Node parent = parent(node);
	if (parent == null) {
	    return 0;
	} else {
	    LinkedList children = children(parent);
	    return 	children.indexOf(node);
	}
    }

    /** Check if the given node is the first (leftmost) child of its
     *  parent.  The method returns false if the given node is the
     *  root node. (FIXME: Why?? Not consistent with indexOfChild)
     *
     *  @param node The specified node.
     *  @return True if the node is not a root and is the first (leftmost) child
     *  of its parent; false otherwise.
     *
     *  @exception IllegalArgumentException If the node is null.
     *  @exception GraphElementException If the node is not in the tree.
     */
    public boolean isFirstChild(Node node) {
	if (node == null) {
	    throw new IllegalArgumentException("The specified node cannot be null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException("The specified node is not in the tree. \n");
	}

	if (_root == node) {
	    return false;
	}

	if (indexOfChild(node) == 0) {
	    return true;
	}

	return false;
    }

    /** Check if the given node is the last (rightmost) child of its
     *  parent.  The method returns false if the given node is the
     *  root node. (FIXME: Why?? Not consistent with indexOfChild)
     *
     *  @param node The specified node.
     *  @return True if the node is not a root and is the last (rightmost) child
     *  of its parent; false otherwise.
     *

     *  @exception IllegalArgumentException If the node is null.
     *  @exception GraphElementException If the node is not in the tree.
     */
    public boolean isLastChild(Node node) {
	if (node == null) {
	    throw new IllegalArgumentException("The specified node cannot be null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException("The specified node is not in the tree. \n");
	}

	if (_root == node) {
	    return false;
	}

	if (indexOfChild(node) == children(parent(node)).size() - 1) {
	    return true;
	}

	return false;
    }

    /** Check whether the specified node is a leaf node.
     *
     *  @param node The specified node.
     *
     *  @return True if the specified node is a leaf; false otherwise.
     *
     *  @exception IllegalArgumentException If the node is null.
     *  @exception GraphElementException If the node is not in the tree.
     */
    public boolean isLeaf(Node node) {
	if (node == null) {
	    throw new IllegalArgumentException("The specified node cannot be null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException("The specified node is not in the tree. \n");
	}

	return children(node).isEmpty();
    }

    /** Return the number of children of the node. The method returns
     *  0 for the leaf nodes.
     *
     *  @param node The specified node.
     *
     *  @return Number of children of the specified node.
     *
     *  @exception IllegalArgumentException If the specified node is
     *  null.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     */
    public int childrenCount(Node node) {
	if (node == null) {
	    throw new IllegalArgumentException ("The specified node cannot be null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

        return children(node).size();
    }

    /** Get a list of the tree leaf nodes.
     *  @return A list of leaf nodes.
     */
    public LinkedList getLeafNodes() {
        return _leafNodes;
    }

    /** Get the root node of the tree.
     *  @return The tree root.
     */
    public Node root() {
        return _root;
    }

    /** The least(youngest/closest/nearest) common parent of given
     *  pair of nodes. If one of the nodes is an ancestor of the other
     *  node, parent of the ancestor node is returned. If both the
     *  nodes are same, parent of this node is returned.
     *
     *  @param node1 A graph node.
     *  @param node2 A graph node.
     *
     *  @return The least common parent.
     *
     *  @exception IllegalArgumentException If any of the input nodes
     *  are null.
     *  @exception GraphElementException If any of the input nodes are
     *  not in the rooted tree.
     */
    public Node leastParent(Node node1, Node node2) {
	if (node1 == null || node2 == null) {
	    throw new IllegalArgumentException ("The specified node(s) is (are) null. \n");
	}

	if (!(containsNode(node1) && containsNode(node2))) {
	    throw new GraphElementException ("The specified node(s) not in the given rooted tree. \n");
	}

	if (node1 == node2) {
	    return parent(node1);
	}

        ArrayList ancestors1 = ancestors(node1);
	if (ancestors1.contains(node2)) {
	    return parent(node2);
	}

        ArrayList ancestors2 = ancestors(node2);
	if (ancestors2.contains(node1)) {
	    return parent(node1);
	}

        // Distinguish short and long parents.
        ArrayList shortParents = ancestors1;
        ArrayList longParents  = ancestors2;
        if (ancestors1.size() > ancestors2.size()) {
            shortParents = ancestors2;
            longParents  = ancestors1;
        }

        Node leastParent = null;
        for (int i = 0; i < shortParents.size(); i++) {
            Node parent = (Node)shortParents.get(i);
            if (longParents.contains(parent)) {
                leastParent = parent;
                break;
            }
        }

        return leastParent;
    }

    /** Return the leftmost leaf node that is descendant of the given
     *  node. For unbalanced trees, this method returns the leftmost
     *  leaf at a depth nearest to the given node.
     *
     *  @param node A graph node.
     *
     *  @return The leftmost leaf node that is descendant of the given
     *  node, if the given node is not a leaf; <code>null</code>
     *  otherwise.
     *
     *  @exception IllegalArgumentException If the specified node is
     *  null.
     *  @exception GraphElementException If any of the input nodes are
     *  not in the rooted tree.
     */
    public Node leftmostLeaf(Node node) {
	if (node == null) {
	    throw new IllegalArgumentException ("Input node is null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

	if (isLeaf(node)) {
	    return null;
	}

	while (!isLeaf(node)) {
	    node = getChildFirst(node);
	}

	return node;
    }

    /** Return the rightmost leaf node that is descendant of the given
     *  node. For unbalanced trees, this method returns the rightmost
     *  leaf at a depth nearest to the given node.
     *
     *  @param node A graph node.
     *
     *  @return The rightmost leaf node that is descendant of the given
     *  node, if the given node is not a leaf; <code>null</code>
     *  otherwise.
     *
     *  @exception IllegalArgumentException If the specified node is
     *  null.
     *  @exception GraphElementException If any of the input nodes are
     *  not in the rooted tree.
     */
    public Node rightmostLeaf(Node node) {
	if (node == null) {
	    throw new IllegalArgumentException ("Input node is null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

	if (isLeaf(node)) {
	    return null;
	}

	while (!isLeaf(node)) {
	    node = getChildLast(node);
	}

	return node;
    }

    /** Get common parents for a given pair of nodes. The parents are
     *  returned in the form of List, from the least(youngest) to the
     *  root.
     *
     *  @param node1 A graph node.
     *  @param node2 A graph node.
     *
     *  @return A list of common parents.
     *
     *  @exception IllegalArgumentException If any of the input nodes
     *  are null.
     *  @exception GraphElementException If any of the input nodes
     *  are not in the rooted tree.
     */
    public List commonParents(Node node1, Node node2) {
	if (node1 == null || node2 == null) {
	    throw new IllegalArgumentException ("The specified node(s) is (are) null. \n");
	}

	if (!(containsNode(node1) && containsNode(node2))) {
	    throw new GraphElementException ("The specified node(s) not in the given rooted tree. \n");
	}

        ArrayList parents = new ArrayList();
	Node commonParent = leastParent(node1, node2);
        parents.add(commonParent);
        parents.addAll(ancestors(commonParent));
        return (List)parents;
    }

    /** Get the subtree rooted at the given node.
     *
     *  @param node A graph node.
     *
     *  @return The subtree rooted at the given node, if the given
     *  node is not a leaf; <code>null</code> otherwise.
     *
     *  @exception IllegalArgumentException If the specified node is
     *  null.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     */
    public RootedTree subtree(Node node) {
	if (node == null) {
	    throw new IllegalArgumentException ("Input node is null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

	if (isLeaf(node)) {
	    return null;
	}

	RootedTree subtree = new RootedTree();
	LinkedList descendantsList = new LinkedList();
	descendants(node, descendantsList, 1);
	descendantsList.addFirst(node);
	// FIXME - Sure?
	subtree = (RootedTree)(subgraph(descendantsList));
	subtree.buildRootedTree(subtree, node);
	return subtree;
    }

    /** Return the successor of the given node in the tree.
     *
     *  Successor of an internal node is its leftmost child; successor
     *  of a leaf node is the first sibling to its right or the first
     *  sibling to the right of its first ancestor that has a sibling
     *  to its right.
     *
     *  @param node The given node
     *
     *  @return The successor of the given node if exists; null
     *  otherwise.
     *
     *  @exception IllegalArgumentException If the specified node is
     *  null.
     *  @exception GraphElementException If the specified node is not
     *  in the tree.
     */
    public Node successor(Node node) {
	if (node == null) {
	    throw new IllegalArgumentException ("Input node is null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

	if (!isLeaf(node)) {
	    return getChildFirst(node);
	} else if (!isLastChild(node)) {
	    return rightSibling(node);
	} else {
	    Node ancestor = parent(node);

	    while (ancestor != _root) {
		if (!isLastChild(ancestor)) {
		    return rightSibling(ancestor);
		}

		ancestor = parent(ancestor);
	    }
	}

	return null;
    }

    /** Return all the ancestors of the node.  The ancestors are
     *  returned in order from the parent of the node to the root of
     *  the tree.
     *
     *  @param node A graph node.
     *
     *  @return A list of ancestors from the parent to the root of the
     *  tree.
     *
     *  @exception IllegalArgumentException If the input node is null.
     *  @exception GraphElementException If the input node is not in
     *  the rooted tree.
     */
    public ArrayList ancestors(Node node) {
	if (node == null) {
	    throw new IllegalArgumentException ("Input node is null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

        ArrayList ancestors = new ArrayList();
	Node parentNode = parent(node);

        while (parentNode != null) {
	    ancestors.add(parentNode);
            parentNode = parent(parentNode);
        }

        return ancestors;
    }

    /** Return all the descendants of the specified node.  The
     *  descendants are returned in order from the children of the
     *  node to the leaf nodes in breadth-first-search(BFS) manner if
     *  <code>method</code> is <code>0</code> and in
     *  depth-first-search(DFS) manner otherwise.
     *
     *  Note: earlier implementation used DFS manner. Calls to this
     *  method must be modified accordingly.
     *
     *  @param node A graph node.
     *  @param descendantsList A Linked List which stores all the
     *  descendants of a node.
     *  @param method <code>0</code> for BFS, <code>1</code> for DFS.
     *
     *  @exception IllegalArgumentException If the specified node is null.
     *  @exception GraphElementException If the input node is not in the rooted tree.
     */
    public void descendants(Node node, LinkedList descendantsList, int method) {
	if (node == null) {
	    throw new IllegalArgumentException ("Input node is null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

	if (method != 0) {
	    if (!isLeaf(node)) {
		LinkedList childrenList = children(node);
		for (int i = 0; i < childrenList.size(); i++) {
		    Node temp = (Node)childrenList.get(i);
		    descendantsList.add(temp);
		    descendants(temp, descendantsList, method);
		}
	    } else {
		descendantsList.add(node);
	    }
	} else {
	    if (!isLeaf(node)) {
		LinkedList childrenList = children(node);
		descendantsList.addAll(childrenList);
		for (int i = 0; i < childrenList.size(); i++) {
		    Node temp = (Node)childrenList.get(i);
		    descendants(temp, descendantsList, method);
		}
	    } else {
		descendantsList.add(node);
	    }
	}
    }

    /** Return the sibling immediately to the left of the node.  If
     *  the node is the root or first child of its parent then return
     *  null.
     *
     *  @param node A given node.
     *
     *  @return The immediate left sibling of the node; null if the
     *  given node is either root or the leftmost child of its parent.
     *
     *  @exception IllegalArgumentException If the specified node is
     *  null.
     *  @exception NoSuchElementException If node is not in the rooted tree.
     */
    public Node leftSibling(Node node) {
	if (node == null) {
	    throw new IllegalArgumentException ("The specified node is null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

	if (node == _root) {
	    return null;
	}

	if (!isFirstChild(node)) {
	    int index = indexOfChild(node);
	    return getChild(parent(node), index - 1);
	}

	return null;
    }

    /** Return the sibling immediately to the right of the node.  If
     *  the node is the root or last child of its parent then return
     *  null.
     *
     *  @param node A given node.
     *
     *  @return The immediate right sibling of the node; null if the
     *  given node is either root or the rightmost child of its parent.
     *
     *  @exception IllegalArgumentException If the specified node is
     *  null.
     *  @exception NoSuchElementException If node is not in the rooted tree.
     */
    public Node rightSibling(Node node) {
	if (node == null) {
	    throw new IllegalArgumentException ("The specified node is null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the tree. \n");
	}

	if (node == _root) {
	    return null;
	}

	if (!isLastChild(node)) {
	    int index = indexOfChild(node);
	    return getChild(parent(node), index + 1);
	}

	return null;
    }

    /** Check whether the node1 is a child of node2.
     *
     *  @param node1 A graph node.
     *  @param node2 A graph node.
     *
     *  @return True if the node1 is a child of node2; false otherwise.
     *
     *  @exception IllegalArgumentException If either of the input
     *  nodes in null.
     *  @exception GraphElementException If either of the node is not
     *  in the rooted tree.
     */
    public boolean isChild(Node node1, Node node2) {
	if (node1 == null || node2 == null) {
	    throw new IllegalArgumentException ("The specified node(s) is (are) null. \n");
	}

	if (!(containsNode(node1) && containsNode(node2))) {
	    throw new GraphElementException ("The specified node(s) is (are) not in the tree. \n");
	}

	return (parent(node1) == node2);
    }

    /** Build Rooted tree structure from the given tree and root node
     *  using Breadth First Search algorithm.
     *  @param tree A tree.
     *  @param root A node in the tree.
     *
     *  @exception IllegalArgumentExcpetion if the specified root node
     *  or tree is null.
     *  @exception GraphElementException if the specified root node is
     *  not in the tree.
     */

    public void buildRootedTree(Tree tree, Node root) {
	if (root == null || tree == null) {
	    throw new IllegalArgumentException ("The specified tree or the root node are null. \n");
	}

	if (!tree.containsNode(root)) {
	    throw new GraphElementException ("The specified root node is not in the tree. \n");
	}

	_updateRootedTree(tree, root);
    }

    /** Find and set the tree leaf nodes.
     */
    public void setLeafNodes() {
	_updateLeafNodes();
    }

    /** Adds the given node to the rooted tree along with an edge from
     *  the specified node to the current root of the tree. It then
     *  sets the given node as the root of the tree.
     *
     *  If the current tree (and hence, its root) is null, then the
     *  node is added without any new edge. The resultant tree has
     *  just one node which also its root.
     *
     *  @param node A graph node.
     *
     *  @exception IllegalArgumentException If the specified node is
     *  null.
     */
    public void addRoot(Node node) {
	if (node == null) {
	    throw new IllegalArgumentException ("The specified node is null. \n");
	}

	addNode(node);

	if (_root != null) {
	    Edge edge = new Edge(node, _root);
	    addEdge(edge);
	}

	_updateRootedTree(this, node);
    }

    /** <p>Replace the current root of the tree with the specified
     *  <i>node</i>.
     *
     *  <p>If the specified <i>node</i> is in the tree, this method
     *  sets it as the new root of the tree, traverses the entire tree
     *  to update the parent and children information of each node
     *  accordingly.
     *
     *  <p>If the specified <i>node</i> is not in the tree, it is
     *  added to the tree and set as the root. This is equivalent to
     *  calling the <code>addRoot()</code> method.
     *
     *  @param node The given node.
     *
     *  @exception IllegalArgumentException If the the specified node
     *  is null.
     */
    public void setRoot(Node node) {
	if (node == null) {
	    throw new IllegalArgumentException ("The specified node is null. \n");
	}

	if (containsNode(node)) {
	    _updateRootedTree(this, node);
	} else {
	    addRoot(node);
	}
    }

    /** Add the specified rooted tree to the current tree as the child
     *  of the specified node.
     *
     *  @param subtree A tree.
     *  @param node A graph node.
     *
     *  @exception IllegalArgumentException If any of the arguments
     *  are null.
     *  @exception GraphElementException If the specified node is not
     *  in the current rooted tree.
     */
    public void addRootedTree(RootedTree subtree, Node node) {
	if (node == null || subtree == null) {
	    throw new IllegalArgumentException ("The specified subtree or the node is null. \n");
	}

	if (!containsNode(node)) {
	    throw new GraphElementException ("The specified node is not in the rooted tree: " + toString() + "\n");
	}

	addGraph(subtree);

	Edge edge = new Edge(node, subtree.root());
	addEdge(edge);

	_updateRootedTree(this, _root);
    }

    /** Add the given rooted tree. It adds the rooted tree as the last
     *  child of the current root, if the current root (and tree) are
     *  not null.  If current root and tree are null, it replaces the
     *  current tree with the new subtree.
     *
     *  @param subtree A tree.
     *
     *  @exception IllegalArgumentException If the specified subtree
     *  is null.
     */
    public void addRootedTree(RootedTree subtree) {
	if (subtree == null) {
	    throw new IllegalArgumentException ("The specified tree is null. \n");
	}

	if (_root != null) {
	    addRootedTree(subtree, _root);
	} else {
	    Node root = subtree.root();

	    addGraph(subtree);
	    _updateRootedTree(this, root);
	}
    }

    ///////////////////////////////////////////////////////////////////
    ////                      protected methods                    ////

    /** Sets leaf nodes. This method when called, clears
     * <code>_leafNodes</code> and sets it with the current leaf
     * nodes.
     */
    protected  void _updateLeafNodes() {
	LinkedList leafNodes = new LinkedList();

	_leafNodes.clear();

	descendants(_root, leafNodes, 0);
	while (!leafNodes.isEmpty()) {
	    Node node = (Node)leafNodes.removeFirst();
	    if (node != null && isLeaf(node)) {
		_leafNodes.add(node);
	    }
	}
    }

    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////

    /** Initialize private members of the class.
     */
    private void _initializeRootedTree() {
    	_root = null;
	_leafNodes = new LinkedList();
	_nodeParentMap = new HashMap();
	_nodeChildrenMap = new HashMap();
    }

    /** Build Rooted tree structure from the given tree and root node
     *  using Breadth First Search algorithm.
     *  @param tree A tree.
     *  @param root A node in the tree.
     *
     *  @exception IllegalArgumentExcpetion if the specified root node
     *  or tree is null.
     *  @exception GraphElementException if the specified root node is
     *  not in the tree.
     */

    private void _updateRootedTree(Tree tree, Node root) {
	if (root == null || tree == null) {
	    throw new IllegalArgumentException ("The specified tree or the root node are null. \n");
	}

	if (!tree.containsNode(root)) {
	    throw new GraphElementException ("The specified root node is not in the tree. \n");
	}

	// Reset the parent and children maps.
	_nodeParentMap.clear();
	_nodeChildrenMap.clear();

	boolean[] nodeFlag = new boolean[tree.nodeCount()];

	for (int i = 0; i < nodeFlag.length; i++) {
	    nodeFlag[i] = false;
	}

	LinkedList nodeQueue = new LinkedList();
	_root = root;
	updateParent(root, null);
	nodeFlag[tree.nodeLabel(root)] = true;
	nodeQueue.addLast(root);

	while (!(nodeQueue.isEmpty())) {
	    Node nodeU = (Node)nodeQueue.getFirst();
	    Collection adjacentNodesCol = tree.neighbors(nodeU);
	    LinkedList adjacentNodes = new LinkedList(adjacentNodesCol);

	    int adjCount = adjacentNodes.size();
	    for (int i = 0; i < adjCount; i++) {
		Node nodeV = (Node)adjacentNodes.getFirst();

		if (nodeFlag[tree.nodeLabel(nodeV)] == false) {
		    nodeFlag[tree.nodeLabel(nodeV)] = true;
		    updateParent(nodeV, nodeU);
		    if (!isChild(nodeV, nodeU)) {
			updateChild(nodeU, nodeV);
		    }

		    nodeQueue.addLast((Node)adjacentNodes.removeFirst());
		} else {
		    adjacentNodes.removeFirst();
		}
	    }

	    nodeQueue.removeFirst();
	}

	_updateLeafNodes();
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    /** The tree root node. */
    private Node _root;

    /** Ordered leaf nodes. */
    private LinkedList _leafNodes;

    /** Mapping from nodes of the rooted tree to their parent
     *  node. Each key in this map is an instance of
     *  <code>Node</code>. Each value is also an instance of
     *  <code>Node</code>.
     */
    private HashMap _nodeParentMap;

    /** Mapping from nodes of the rooted tree to their children nodes.
     *
     *  Each key in this map is an instance of <code>Node</code>. Each
     *  value is an instance of <code>LinkedList</code> whose elements
     *  are instances of <code>Node</code>. The ordering of nodes in
     *  the linked list may be important for certain types of
     *  trees. Note that the order of nodes in the linked list may
     *  differ from the <code>_incidentEdgeMap</code> in the {@link Graph}.
     *
     *  All the derived classes that have rooted tree structure must
     *  use methods that operate on parent and children maps in this
     *  class.
     */
    private HashMap _nodeChildrenMap;
}
