/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A generic tree structure.*/

package dif.util.graph;

/*import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Collection;
import java.util.Iterator;

import mocgraph.Node;
*/

//////////////////////////////////////////////////////////////////////////
////Tree

/** A generalized Tree.
 This class implements a tree data structure.

 Tree is an undirected graph that is acyclic and connected.
 This implies that number of edges in a tree always equals one less than number of nodes.

 Based on Introduction to Algorithms, Cormen et al.

 @author Nimish Sane, Shuvra S. Bhattacharyya
 @version $Id: Tree.java 416 2007-05-31 05:19:54Z plishker $
 */
public class Tree extends UndirectedGraph {

    /** Construct an empty tree.
     */
    public Tree() {
	super();
    }

    /** Construct an empty tree with enough storage allocated for the
     *  specified number of nodes.  Memory management is more
     *  efficient with this constructor if the number of nodes is
     *  known.
     *  @param nodeCount The number of nodes.
     */
    public Tree(int nodeCount) {
        super(nodeCount, nodeCount - 1);
    }

    /** Construct an empty tree with enough storage allocated for the
     *  specified number of edges.  Memory
     *  management is more efficient with this constructor if the
     *  number of edges is known.
     *  @param edgeCount The number of edges.
     */
    /*  public Tree(int edgeCount) {
	super(edgeCount + 1, edgeCount);
	} */


    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    // This method is no longer supported. Removing and edge from a tree yields an undirected graph that is no longer connected.

    /*
    public boolean removeEdge(Node node1, Node node2) {
	Collection edges = neighborEdges(node1, node2);
	Iterator iter = edges.iterator();
	boolean flag = false;
	while(iter.hasNext()) {
	    flag = removeEdge((Edge)iter.next());
	}

	return flag;
    }
    */
}

