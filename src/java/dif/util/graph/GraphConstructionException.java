/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Exception of modifying graph in wrong ways.*/
package dif.util.graph;

//////////////////////////////////////////////////////////////////////////
//// GraphConstructionException

/**
 The exception of modifying graph elements in wrong ways. The
 modification is due to the change of topology or improper weight
 association. Function evaluations incurred by graph reading do not
 cause this exception.

 @author Mingyung Ko, Shuvra S. Bhattacharyya
 @version $Id: GraphConstructionException.java 1634 2007-04-06 18:24:07Z ssb $
 */
public class GraphConstructionException extends GraphException {
    /** Constructor with an argument of text description.
     *  @param message The exception message.
     */
    public GraphConstructionException(String message) {
        super(message);
    }
}

