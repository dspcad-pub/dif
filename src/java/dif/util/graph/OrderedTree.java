/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A generic ordered tree structure.*/

package dif.util.graph;

import java.lang.IllegalArgumentException;

//////////////////////////////////////////////////////////////////////////
////Ordered Tree

/** A generalized ordered tree.

 <p>This class implements an ordered tree data structure.

 <p>Ordered tree is a rooted tree in which the set of children for any
 node is an ordered set.

 <p>Note that it is invalid for an ordered tree node to have a list of
 children that contains null elements or has any element appearing
 more than once.

 Implementation changes:
 addOrderedTreeNode() method supported null arguments. It no longer supports that.
 Use RootedTreeNode.addRoot() method to add a node to a null tree.

 addOrderedTreeNode() method is now depricated. Use
 {@link RootedTree#addChild(Node, Node, int)} instead.

 Based on Introduction to Algorithms, Cormen et al.

 @author Nimish Sane, Shuvra S. Bhattacharyya
 @version $Id: OrderedTree.java 416 2007-05-31 05:19:54Z plishker $
 @see Tree
 @see RootedTree
 */

public class OrderedTree extends RootedTree{
    /** Construct an empty ordered tree.
     */
    public OrderedTree() {
	super();
    }

    /** Construct an empty ordered tree with enough storage allocated
     *  for the specified number of nodes.  Memory management is more
     *  efficient with this constructor if the number of nodes is
     *  known.
     *  @param nodeCount The number of nodes.
     */
    public OrderedTree(int nodeCount) {
        super(nodeCount);
    }

    /** Construct an empty ordered tree with given ordered tree node
     *  as its root.
     *  @param root A graph node.
     */
    public OrderedTree(Node root) {
	super(root);
    }

    /** Construct a ordered tree with given root and enough storage
     *  allocated for the specified number of nodes, with given
     *  ordered tree node as its root.  Memory management is more
     *  efficient with this constructor if the number of nodes is
     *  known.
     *  @param nodeCount The number of nodes.
     *  @param root A graph node.
     */
    public OrderedTree(int nodeCount, Node root) {
        super(nodeCount, root);
    }

    /** Constructor for a given tree and root, where root is already
     *  in tree.
     *  @param tree The given tree.
     *  @param root The given root.
     */
    public OrderedTree(Tree tree, Node root) {
	super(tree, root);
    }

    ///////////////////////////////////////////////////////////////////////////////
    ////                          public methods                               ////

    /** Add the given ordered tree to the existing tree and set its
     * root as the last child of the specified node.
     *
     * This method no longer supports adding a subtree to a null
     * ordered tree. Instead, use {@link #addOrderedTree(OrderedTree)}
     * version of this method to add a subtree to a null tree.
     *
     *  @param subtree An ordered tree.
     *  @param node A specified node.
     *
     *  @exception IllegalArgumentException If the specified subtree
     *  or node are null.
     *  @exception GraphElementException If the specified node is not
     *  the current ordered tree.
     */
    public void addOrderedTree(OrderedTree subtree, Node node)  {
	addRootedTree(subtree, node);
    }

    /** Add the specified ordered tree to the current tree. It adds
     *  the ordered tree as the last child of the current root, if the
     *  current root (and tree) are not null.  If current root and
     *  tree are null, it replaces the current tree with the new
     *  subtree.
     *
     *  @param subtree The specified ordered tree.
     *
     *  @exception IllegalArgumentException If the specified subtree is null.
     */
    public void addOrderedTree(OrderedTree subtree) {
	addRootedTree(subtree);
    }
}
