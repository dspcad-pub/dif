/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A base class for a Mapping that is based on a Map.*/
package dif.util.graph.mapping;

import java.util.Map;

//////////////////////////////////////////////////////////////////////////
//// MapMapping

/** A Mapping that is based on a Map. The domain of the Mapping is the
 set of keys in the Map. MapMappings are immutable in the
 sense that the underlying Map cannot be changed (although the keys and
 values associated with the Map can be changed).

 @author Shuvra S. Bhattacharyya
 @version $Id: MapMapping.java 1636 2007-04-06 18:30:09Z ssb $
 */
public abstract class MapMapping implements Mapping {
    /** Construct a MapMapping from a given Map.
     *  Modifications to the argument Map after construction
     *  of this mapping will be reflected in the Mapping.
     */
    public MapMapping(Map map) {
        _map = map;
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Return true if the given object is of the same Class and based
     *  on the same Map as this one.
     *  @param object The given object.
     *  @return True if the given object is of the same class and based
     *  on the same Map as this one.
     */
    public boolean equals(Object object) {
        if ((object == null) || (object.getClass() != getClass())) {
            return false;
        }

        return _map.equals(((MapMapping) object)._map);
    }

    /** Return the hash code of this MapMapping. The hash code is
     *  simply that of the Map that this Mapping is based on.
     */
    public int hashCode() {
        return _map.hashCode();
    }

    /** Return true if the given object is a key in the Map that is associated
     *  with this mapping.
     *  @param object The given object.
     *  @return True if the given object is a key in the Map that is associated
     *  with this mapping.
     */
    public boolean inDomain(Object object) {
        return _map.containsKey(object);
    }

    /** Return a string representation of this MapMapping. The
     *  string representation is the class name, followed by a
     *  delimiting string, followed by a
     *  string representation of the underlying Map.
     */
    public String toString() {
        return getClass().getName() + "based on the following Map\n"
                + _map.toString() + "\n";
    }

    ///////////////////////////////////////////////////////////////////
    ////                         protected variables               ////
    // The Map that this Mapping is based on.
    protected Map _map;
}

