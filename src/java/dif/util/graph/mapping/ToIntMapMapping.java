/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A ToIntMapping that is based on a Map.*/
package dif.util.graph.mapping;

import java.util.Map;

//////////////////////////////////////////////////////////////////////////
//// ToIntMapMapping

/** A ToIntMapping that is based on a Map. The values in the Map
 must be instances of Integer. ToIntMapMappings are immutable in the
 sense that the underlying Map cannot be changed (although the keys and
 values associated with the Map can be changed).

 @author Shuvra S. Bhattacharyya
 @version $Id: ToIntMapMapping.java 1636 2007-04-06 18:30:09Z ssb $
 */
public class ToIntMapMapping extends MapMapping implements ToIntMapping {
    /** Construct a ToIntMapMapping from a given map. The values in the
     *  must be instances of Integer; otherwise, unpredictable behavior
     *  may result. Modifications to the argument Map after construction
     *  of this mapping will be reflected in the mapping. The Map modifications
     *  must follow the restriction that all added values to the Map
     *  must be instances of Integer.
     */
    public ToIntMapMapping(Map map) {
        super(map);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Return true if the given object is in the domain of this Mapping.
     *  More precisely, return true if the given object is a valid argument
     *  to {@link #toInt(Object)}, which means that the object is a
     *  key in the Map that is associated with this Mapping and the value
     *  in the Map is an instance of Integer.
     *  @param object The given object.
     *  @return True if the given object is in the domain of this Mapping.
     */
    public boolean inDomain(Object object) {
        return ((_map.containsKey(object)) && (_map.get(object) instanceof Integer));
    }

    /** Return the int value that is associated with given object under
     *  this mapping. For efficiency, no error checking is performed
     *  on the argument, and consequently, a runtime exception may result as
     *  noted below. To perform argument validity checking before mapping an
     *  object.
     *  @param object The given object.
     *  @return The int value that is associated with given object under
     *  this mapping.
     *  @exception RuntimeException If the given object is not an instance
     *  of {@link java.lang.Integer} or if the given object is not in the
     *  domain of the mapping.
     */
    public int toInt(Object object) {
        return ((Integer) (_map.get(object))).intValue();
    }

    public Object toObject(Object object) {
        return _map.get(object);
    }
}

