/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A mapping from objects into multiple double values.*/
package dif.util.graph.mapping;

//////////////////////////////////////////////////////////////////////////
//// ToDoubleMultipleMapping

/**
 A mapping from objects into multiple double values.

 @author Shuvra S. Bhattacharyya, Shahrooz Shahparnia
 @version $Id: ToDoubleMapping.java 1636 2007-04-06 18:30:09Z ssb $
 */
public interface ToDoubleMapping extends Mapping {
    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Return the double value associated with the given object.
     *
     *  @return Return the double value associated with the given object.
     */
    public double toDouble(Object object);
}

