/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

 /* Common conventions for sythesis tree.*/

package dif.util;

import dif.DIFGraph;

import java.util.StringTokenizer;

/**
 This class includes static methods for convention checks in synthesis tree
 classes. If there is a common convention for similar parameters of different
 classes, related checks can be placed to this class to avoid code duplication.
 @author Fuat Keceli
 @version $Id: Conventions.java 606 2008-10-08 16:29:47Z plishker $
 */

public class Conventions {

    public static void main(String args[]) {
        System.out.println(labelConvention(args[0]));
    }

    protected Conventions() {
    }

    /** Label convention check for using with data structures that require
     *  element labels such as {@link dif.graph.hierarchy.Hierarchy}
     *  or {@link DIFGraph}. Following is the definition for
     *  strings acceptable by this method:<pre>
     digit = ['0' .. '9']
     non_digit = [[['a' .. 'z'] + ['A' .. 'Z']] + '_']
     s_char = [all -['"' + ['\' + [10 + 13]]]] | escape_sequence;
     s_char_sequence = s_char*;

     label = non_digit (digit | non_digit)*
     or
     label = '$' s_char_sequence '$';
     </pre>
     *  This definition is compatible with DIF Language definition
     *  in <code>mapss/dif/language/sablecc/Compiler.grammar</code>.
     *  @param label String to check.
     *  @return Error string. Null in case of no errors.
     */
    public static String labelConvention(String label) {
        String allUpperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String allLowerCase = allUpperCase.toLowerCase();
        String digits = "0123456789";
        String nonDigits = allUpperCase + allLowerCase + "_";
        String delimiters = digits + nonDigits;
        if (label == null) {
            return "Invalid label (null).";
        }
        if (label.length() == 0) {
            return "Invalid label (empty string).";
        }
        if (label.startsWith("$") && label.endsWith("$")) {
            return null;
        }
        if (nonDigits.indexOf(label.charAt(0)) == -1) {
            return "Invalid label. First character is not a non-digit in "
                    + label + "." + " Arbitrary name should be enclosed by $.";
        }
        StringTokenizer tokenizer = new StringTokenizer(label, delimiters);
        if (tokenizer.hasMoreTokens()) {
            return "Invalid label. " + label + " contains illegal character ("
                    + tokenizer.nextToken() + ")"
                    + " Arbitrary name should be enclosed by $.";
        }
        return null;
    }

    /** Type convention check for DIF actor attribute type. 
     *  Note that '$' s_char_sequence '$' is not allowed in 
     *  this method. Following is the definition for
     *  strings acceptable by this method:<pre>
     digit = ['0' .. '9']
     non_digit = [[['a' .. 'z'] + ['A' .. 'Z']] + '_']
     label = non_digit (digit | non_digit)* ('.' non_digit (digit | non_digit)* )*;
     </pre>
     *  @param label String to check.
     *  @return Error string. Null in case of no errors.
     */
    public static String typeConvention(String label) {
        String allUpperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String allLowerCase = allUpperCase.toLowerCase();
        String digits = "0123456789";
        String nonDigits = allUpperCase + allLowerCase + "_";
        String dot = ".";
        String delimiters = digits + nonDigits + dot;
        if (label == null) {
            return "Invalid label (null).";
        }
        if (label.length() == 0) {
            return "Invalid label (empty string).";
        }
        if (nonDigits.indexOf(label.charAt(0)) == -1) {
            return "Invalid label. First character is not a non-digit in "
                    + label + "." + " Arbitrary name should be enclosed by $.";
        }
        if (label.charAt(0) == dot.charAt(0)
                || label.charAt(label.length() - 1) == dot.charAt(0)) {
            return ". should not be the first or last position.";
        }
        StringTokenizer tokenizer = new StringTokenizer(label, delimiters);
        if (tokenizer.hasMoreTokens()) {
            return "Invalid label. " + label + " contains illegal character ("
                    + tokenizer.nextToken() + ")"
                    + " Arbitrary name should be enclosed by $.";
        }
        return null;
    }

    /** Type convention check for DIF actor attribute type. 
     *  Note that '$' s_char_sequence '$' is not allowed in 
     *  this method. Following is the definition for
     *  strings acceptable by this method:<pre>
     digit = ['0' .. '9']
     non_digit = [[['a' .. 'z'] + ['A' .. 'Z']] + '_']
     label = non_digit (digit | non_digit)* ('.' non_digit (digit | non_digit)* )*;
     </pre>
     *  @param label String to check.
     *  @return Error string. Null in case of no errors.
     */
    public static String datatypeConvention(String label) {
        String allUpperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String allLowerCase = allUpperCase.toLowerCase();
        String digits = "0123456789";
        String nonDigits = allUpperCase + allLowerCase + "_";
        String star = "*";
        String delimiters = digits + nonDigits + star;
        if (label == null) {
            return "Invalid label (null).";
        }
        if (label.length() == 0) {
            return "Invalid label (empty string).";
        }
        if (nonDigits.indexOf(label.charAt(0)) == -1) {
            return "Invalid label. First character is not a non-digit in "
                    + label + "." + " Arbitrary name should be enclosed by $.";
        }
        if (!(label.indexOf('*') == -1
                || label.indexOf('*') == label.length() - 1)) {
            return "* should be the last position.";
        }
        StringTokenizer tokenizer = new StringTokenizer(label, delimiters);
        if (tokenizer.hasMoreTokens()) {
            return "Invalid label. " + label + " contains illegal character ("
                    + tokenizer.nextToken() + ")";
        }
        return null;
    }
}


