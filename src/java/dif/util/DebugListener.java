/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Interface debug listeners.*/
package dif.util;

//////////////////////////////////////////////////////////////////////////
//// DebugListener

/**
 Interface for listeners that receive debug messages.

 @author  Edward A. Lee, Elaine Cheong
 @version $Id: DebugListener.java,v 1.19 2005/07/08 19:59:16 cxh Exp $
 @since Ptolemy II 0.3
 @Pt.ProposedRating Green (eal)
 @Pt.AcceptedRating Green (cxh)
 @see NamedObj

 */
public interface DebugListener {
    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** React to the given event.
     *  @param event The event.
     */
    public void event(DebugEvent event);

    /** React to a debug message.
     *  @param message The debug message.
     */
    public void message(String message);
}
