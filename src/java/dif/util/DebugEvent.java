/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Interface for debug events.*/
package dif.util;

//////////////////////////////////////////////////////////////////////////
//// DebugEvent

/**
 An interface for events that can be used for debugging.  These events will
 generally be subclassed to create events with more meaning (such as
 a FiringEvent).  Debug events should always have a useful string
 representation, so that the generic listeners (such as StreamListener)
 can display them reasonably.  This string representation should be
 provided by the toString() method.

 @author  Steve Neuendorffer
 @version $Id: DebugEvent.java,v 1.19 2005/07/08 19:59:16 cxh Exp $
 @since Ptolemy II 1.0
 @Pt.ProposedRating Green (neuendor)
 @Pt.AcceptedRating Green (neuendor)
 @see DebugListener
 @see Debuggable
 */
public interface DebugEvent {
    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Return the source of the event.
     *  @return The ptolemy object that published this event.
     */
    public NamedObj getSource();

    /** Return a string representation of this event.
     *  @return A user-readable string describing the event.
     */
    public String toString();
}
