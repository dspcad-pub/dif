/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Value utilities for DIF*/

package dif.util;

import dif.data.Complex;
import dif.data.Token;
import dif.data.ArrayToken;
import dif.data.IntToken;
import dif.data.DoubleToken;
import dif.data.ComplexToken;
import dif.data.DoubleMatrixToken;
import dif.data.IntMatrixToken;
import dif.data.ComplexMatrixToken;
import dif.data.StringToken;
import dif.data.BooleanToken;

import java.util.ArrayList;
import java.util.Iterator;

//////////////////////////////////////////////////////////////////////////
//// Value
/**Value utilities for DIF. This class provides method for cloning value
object and generating DIF string for the value object.<p>
Value object can be instanceof:
Integer, Double, Complex, String, Boolean, int[], double[],
Complex[], int[][], double[][], Complex[][], ArrayList.<p>

@author Chia-Jui Hsu
@version $Id: Value.java 606 2008-10-08 16:29:47Z plishker $
*/

public class Value {

    /** A private constructor to prevent the object to be accidentally
     *  created.
     */
    private Value() {}

    ///////////////////////////////////////////////////////////////////
    ////                       public methods                      ////

    /** Clone value object. <i>value</i> can be instanceof:
     *  Integer, Double, Complex, String, Boolean, int[], double[],
     *  Complex[], int[][], double[][], Complex[][], ArrayList.
     *  @param value The value object to be cloned.
     */
    public static Object cloneValue(Object value) {
        if (value instanceof Integer) {
            return new Integer(((Integer)value).intValue());
        } else if (value instanceof Double) {
            return new Double(((Double)value).doubleValue());
        } else if (value instanceof Complex) {
            return new Complex(((Complex)value).real, ((Complex)value).imag);
        } else if (value instanceof String) {
            return new String((String)value);
	    //        } else if (value instanceof Boolean) {
	    //            return new Boolean(((Boolean)value).booleanValue());
        } else if (value instanceof ArrayList) {
            ArrayList returnValue = new ArrayList();
            for (Iterator elements = ((ArrayList)value).iterator();
                    elements.hasNext();) {
                returnValue.add(cloneValue(elements.next()));
            }
            return returnValue;
        } else if (value instanceof int[]) {
            int colN = ((int[])value).length;
            int[] returnValue = new int[colN];
            for (int j=0; j<colN; j++) {
	        returnValue[j] = ((int[])value)[j];
	    }
	    return returnValue;
        } else if (value instanceof double[]) {
            int colN = ((double[])value).length;
            double[] returnValue = new double[colN];
	    for (int j=0; j<colN; j++) {
    	        returnValue[j] = ((double[])value)[j];
            }
	    return returnValue;
        } else if (value instanceof Complex[]) {
            int colN = ((Complex[])value).length;
            Complex[] returnValue = new Complex[colN];
	    for (int j=0; j<colN; j++) {
                Complex complex = (Complex)((Complex[])value)[j];
    	        returnValue[j] = new Complex(complex.real, complex.imag);
            }
	    return returnValue;
        } else if (value instanceof int[][]) {
            int[][] mtx = (int[][])value;
            int rowN = mtx.length;
            int colN = mtx[0].length;
            int[][] returnValue = new int[rowN][colN];
	    for (int i=0; i<rowN; i++) {
                for (int j=0; j<colN; j++) {
                    returnValue[i][j] = mtx[i][j];
                }
	    }
	    return returnValue;
        } else if (value instanceof double[][]) {
	    double[][] mtx = (double[][])value;
            int rowN = mtx.length;
            int colN = mtx[0].length;
            double[][] returnValue = new double[rowN][colN];
	    for (int i=0; i<rowN; i++) {
                for (int j=0; j<colN; j++) {
                    returnValue[i][j] = mtx[i][j];
                }
	    }
	    return returnValue;
        } else if (value instanceof Complex[][]) {
	    Complex[][] mtx = (Complex[][])value;
            int rowN = mtx.length;
            int colN = mtx[0].length;
            Complex[][] returnValue = new Complex[rowN][colN];
            for (int i=0; i<rowN; i++) {
                for (int j=0; j<colN; j++) {
                    Complex complex = (Complex)mtx[i][j];
                    returnValue[i][j] =
                        new Complex(complex.real, complex.imag);
                }
	    }
	    return returnValue;
        } else {
            return null;
        }
    }

    /** Check to see if <i>value</i> is instanceof Integer, Double,
     *  Complex, String, Boolean, int[], double[],
     *  Complex[], int[][], double[][], Complex[][], ArrayList.
     *  @param value
     *  @return True, otherwise, false.
     */
    public static boolean isValue(Object value) {
        if (value instanceof Integer
                || value instanceof Double
                || value instanceof Complex
                || value instanceof String
                || value instanceof Boolean
                || value instanceof int[]
                || value instanceof double[]
                || value instanceof Complex[]
                || value instanceof int[][]
                || value instanceof double[][]
                || value instanceof Complex[][]
                || value instanceof ArrayList ) {
            return true;
        } else {
            return false;
        }
    }

    /** Check to see if <i>value</i> is C primitive type or not, i.e., int,
     *  long, float, double, char.
     *  @param value
     *  @return True if yes, false otherwise.
     */
    public static boolean isCPrimitive(Object value) {
        if (value instanceof Integer
                || value instanceof Double) {
            return true;
        } if (value instanceof String && ((String)value).length() == 1) {
            return true;
        } else {
            return false;
        }
    }

    /** Convert Object <i>value</i> to string used in DIF.
     *  <i>value</i> can be instanceof:
     *  Integer, Double, Complex, String, Boolean, int[], double[],
     *  Complex[], int[][], double[][], Complex[][], ArrayList.
     *  @return A converted string.
     */
    public static String toDIFString(Object value) {
        if (value instanceof Integer) {
            return value.toString();
        } else if (value instanceof Double) {
            return value.toString();
        } else if (value instanceof Complex) {
	    Complex complex = (Complex)value;
	    return new String("(" + Double.toString(complex.real) + ","
		    + Double.toString(complex.imag) + ")");
	} else if (value instanceof String) {
            String returnString = _formatString((String)value);
	    return new String("\"" + returnString + "\"" );
	} else if (value instanceof Boolean) {
	    return value.toString();
	} else if (value instanceof int[]) {
            int colN = ((int[])value).length;
	    StringBuffer buffer = new StringBuffer();
	    buffer.append("[");
	    for (int j=0; j<colN; j++) {
		buffer.append(Integer.toString(((int[])value)[j]));
		buffer.append(",");
	    }
	    buffer.deleteCharAt(buffer.lastIndexOf(","));
	    buffer.append("]");
	    return buffer.toString();
        } else if (value instanceof double[]) {
	    int colN = ((double[])value).length;
	    StringBuffer buffer = new StringBuffer();
	    buffer.append("[");
	    for (int j=0; j<colN; j++) {
		buffer.append(Double.toString(((double[])value)[j]));
		buffer.append(",");
	    }
	    buffer.deleteCharAt(buffer.lastIndexOf(","));
	    buffer.append("]");
	    return buffer.toString();
        } else if (value instanceof Complex[]) {
	    int colN = ((Complex[])value).length;
	    StringBuffer buffer = new StringBuffer();
	    buffer.append("[");
	    for (int j=0; j<colN; j++) {
                Complex complex = ((Complex[])value)[j];
		buffer.append( "(" + Double.toString(complex.real) + ","
			+ Double.toString(complex.imag) + ")");
	    }
	    buffer.deleteCharAt(buffer.lastIndexOf(","));
	    buffer.append("]");
	    return buffer.toString();
        } else if (value instanceof int[][]) {
            int[][] mtx = (int[][])value;
	    int rowN = mtx.length;
	    int colN = mtx[0].length;

	    StringBuffer buffer = new StringBuffer();
	    buffer.append("[");
	    for (int i=0; i<rowN; i++) {
	    	for (int j=0; j<colN; j++) {
		    buffer.append(mtx[i][j]);
		    buffer.append(",");
		}
		buffer.deleteCharAt(buffer.lastIndexOf(","));
		buffer.append(";");
	    }
	    buffer.deleteCharAt(buffer.lastIndexOf(";"));
	    buffer.append("]");
	    return buffer.toString();
	} else if (value instanceof double[][]) {
	    double[][] mtx = (double[][])value;
	    int rowN = mtx.length;
	    int colN = mtx[0].length;

	    StringBuffer buffer = new StringBuffer();
	    buffer.append("[");
	    for (int i=0; i<rowN; i++) {
	    	for (int j=0; j<colN; j++) {
		    buffer.append(mtx[i][j]);
		    buffer.append(",");
		}
		buffer.deleteCharAt(buffer.lastIndexOf(","));
		buffer.append(";");
	    }
	    buffer.deleteCharAt(buffer.lastIndexOf(";"));
	    buffer.append("]");
	    return buffer.toString();
	} else if (value instanceof Complex[][]) {
	    Complex[][] mtx = (Complex[][])value;
	    int rowN = mtx.length;
	    int colN = mtx[0].length;

	    StringBuffer buffer = new StringBuffer();
	    buffer.append("[");
	    for (int i=0; i<rowN; i++) {
	    	for (int j=0; j<colN; j++) {
		    Complex complex = (Complex)mtx[i][j];
		    buffer.append( "(" + Double.toString(complex.real) + ","
			    + Double.toString(complex.imag) + ")");
		    buffer.append(",");
		}
		buffer.deleteCharAt(buffer.lastIndexOf(","));
		buffer.append(";");
	    }
	    buffer.deleteCharAt(buffer.lastIndexOf(";"));
	    buffer.append("]");
	    return buffer.toString();
	} else if (value instanceof ArrayList) {
            StringBuffer buffer = new StringBuffer();
            buffer.append("{");
            for (Iterator elements = ((ArrayList)value).iterator();
                    elements.hasNext();) {
                buffer.append(toDIFString(elements.next()));
                buffer.append(",");
            }
            buffer.deleteCharAt(buffer.lastIndexOf(","));
            buffer.append("}");
            return buffer.toString();
        } else {
            return null;
	}
    }

    /** Convert Object <i>value</i> to string used in C.
     *  <i>value</i> can be instanceof:
     *  Integer, Double, String, int[], double[], int[][], double[][].
     *  If <i>value</i> is a single character String, it returns 'char'.
     *  If <i>value</i> is a multiple-character String, it returns
     *  "string".
     *  @return A converted string, or null if not supported.
     */
    public static String toCString(Object value) {
        if (value instanceof Integer) {
            return value.toString();
        } else if (value instanceof Double) {
            return value.toString();
        } else if (value instanceof String) {
            /*String returnString = _formatString((String)value);
            if (returnString.length() == 1) {
                return new String("\'" + returnString + "\'");
            } else {
                return new String("\"" + returnString + "\"" );
            }*/
            return new String((String)value);
	} else if (value instanceof int[]) {
            int colN = ((int[])value).length;
	    StringBuffer buffer = new StringBuffer();
	    buffer.append("{");
	    for (int j=0; j<colN; j++) {
		buffer.append(Integer.toString(((int[])value)[j]));
		buffer.append(",");
	    }
	    buffer.deleteCharAt(buffer.lastIndexOf(","));
	    buffer.append("}");
	    return buffer.toString();
        } else if (value instanceof double[]) {
	    int colN = ((double[])value).length;
	    StringBuffer buffer = new StringBuffer();
	    buffer.append("{");
	    for (int j=0; j<colN; j++) {
		buffer.append(Double.toString(((double[])value)[j]));
		buffer.append(",");
	    }
	    buffer.deleteCharAt(buffer.lastIndexOf(","));
	    buffer.append("}");
	    return buffer.toString();
        } else if (value instanceof int[][]) {
            int[][] mtx = (int[][])value;
	    int rowN = mtx.length;
	    int colN = mtx[0].length;

	    StringBuffer buffer = new StringBuffer();
	    buffer.append("{");
	    for (int i=0; i<rowN; i++) {
                buffer.append("{");
	    	for (int j=0; j<colN; j++) {
		    buffer.append(mtx[i][j]);
		    buffer.append(",");
		}
		buffer.deleteCharAt(buffer.lastIndexOf(","));
		buffer.append("},");
	    }
	    buffer.deleteCharAt(buffer.lastIndexOf(","));
	    buffer.append("}");
	    return buffer.toString();
	} else if (value instanceof double[][]) {
	    double[][] mtx = (double[][])value;
	    int rowN = mtx.length;
	    int colN = mtx[0].length;

	    StringBuffer buffer = new StringBuffer();
	    buffer.append("{");
	    for (int i=0; i<rowN; i++) {
                buffer.append("{");
	    	for (int j=0; j<colN; j++) {
		    buffer.append(mtx[i][j]);
		    buffer.append(",");
		}
		buffer.deleteCharAt(buffer.lastIndexOf(","));
		buffer.append("},");
	    }
	    buffer.deleteCharAt(buffer.lastIndexOf(","));
	    buffer.append("}");
	    return buffer.toString();
	} else {
            return null;
	}
    }

    /** If <i>token</i> is instanceof Integer, Double,
     *  Complex, String, Boolean, int[], double[],
     *  Complex[], int[][], double[][], Complex[][], ArrayList,
     *  return the corresponding dif.data.Token: IntToken, DoubleToken,
     *  ComplexToken, IntMatrixToken, DoubleMatrixToken, ComplexMatrixToken,
     *  StringToken, BooleanToken, and ArrayToken;
     *  else, return StringToken containing the string version of that value.
     *  @param value
     *  @return The converted Token.
     */
    public static Token generateToken(Object value) {
        if (value == null) {
            return null;
        }
      try {
        if (value instanceof Integer) {
            return new IntToken(((Integer)value).intValue());
        } else if (value instanceof Double) {
            return new DoubleToken(((Double)value).doubleValue());
        } else if (value instanceof Complex) {
            return new ComplexToken(
                    new Complex(((Complex)value).real, ((Complex)value).imag));
        } else if (value instanceof String) {
            return new StringToken((String)value);
        } else if (value instanceof Boolean) {
            return new BooleanToken(((Boolean)value).booleanValue());
        } else if (value instanceof int[]) {
            int[][] newValue = {(int[])value};
	    return new IntMatrixToken(newValue);
        } else if (value instanceof double[]) {
            double[][] newValue = {(double[])value};
	    return new DoubleMatrixToken(newValue);
        } else if (value instanceof Complex[]) {
            Complex[][] newValue = {(Complex[])value};
            return new ComplexMatrixToken(newValue);
        } else if (value instanceof int[][]) {
            return new IntMatrixToken((int[][])value);
        } else if (value instanceof double[][]) {
	    return new DoubleMatrixToken((double[][])value);
        } else if (value instanceof Complex[][]) {
	    return new ComplexMatrixToken((Complex[][])value);
        } else if (value instanceof ArrayList) {
            int size = ((ArrayList)value).size();
            Token[] newValue = new Token[size];
            for (int i=0; i<size; i++) {
                newValue[i] = generateToken(((ArrayList)value).get(i));
            }
            return new ArrayToken(newValue);
        } else {
            if (value.toString() != null) {
                System.out.println("Value type " + value.getClass().toString()
                    + " is not support, so outout StringToken with value: "
                    + value.toString());
                return new StringToken(value.toString());
            } else {
                System.out.println("Value type is not support, output null.");
                return null;
            }
        }
      } catch (IllegalActionException exp) {
        throw new RuntimeException(exp.getMessage());
      }
    }

    /** If <i>token</i> is instanceof IntToken, DoubleToken, ComplexToken,
     *  IntMatrixToken, DoubleMatrixToken, ComplexMatrixToken, StringToken,
     *  BooleanToken, and ArrayToken, return the corresponding value for DIF,
     *  i.e., Integer, Double, Complex, String, Boolean, int[], double[],
     *  Complex[], int[][], double[][], Complex[][], ArrayList;
     *  else, return String containing the string version of that value.
     *  @param token
     *  @exception RuntimeException If catches IllegalActionException.
     */
    public static Object generateValue(Token token) {
        if (token == null) {
            return null;
        } else if (token instanceof IntToken) {
            return new Integer(((IntToken)token).intValue());
        } else if (token instanceof DoubleToken) {
            return new Double(((DoubleToken)token).doubleValue());
        } else if (token instanceof ComplexToken) {
            Complex complex = ((ComplexToken)token).complexValue();
            return new Complex(complex.real, complex.imag);
        } else if (token instanceof StringToken) {
            //StringToken.toString adds '\"' at the first and end.
            return new String(((StringToken)token).stringValue());
        } else if (token instanceof BooleanToken) {
            return new Boolean(((BooleanToken)token).booleanValue());
        } else if (token instanceof IntMatrixToken) {
            IntMatrixToken mtxToken = (IntMatrixToken)token;
            int row = mtxToken.getRowCount();
            if (row == 1) {
                return (int[])((mtxToken.intMatrix())[0]);
            } else {
                return mtxToken.intMatrix();
            }
        } else if (token instanceof DoubleMatrixToken) {
            DoubleMatrixToken mtxToken = (DoubleMatrixToken)token;
            int row = mtxToken.getRowCount();
            if (row == 1) {
                return (double[])((mtxToken.doubleMatrix())[0]);
            } else {
                return mtxToken.doubleMatrix();
            }
        } else if (token instanceof ComplexMatrixToken) {
            ComplexMatrixToken mtxToken = (ComplexMatrixToken)token;
            int row = mtxToken.getRowCount();
            if (row == 1) {
                return (Complex[])((mtxToken.complexMatrix())[0]);
            } else {
                return mtxToken.complexMatrix();
            }
        } else if (token instanceof ArrayToken && (
                ((ArrayToken)token).getElement(0) instanceof IntToken ||
                ((ArrayToken)token).getElement(0) instanceof DoubleToken ||
                ((ArrayToken)token).getElement(0) instanceof ComplexToken ||
                ((ArrayToken)token).getElement(0) instanceof IntMatrixToken ||
                ((ArrayToken)token).getElement(0) instanceof
                        DoubleMatrixToken ||
                ((ArrayToken)token).getElement(0) instanceof
                        ComplexMatrixToken ||
                ((ArrayToken)token).getElement(0) instanceof BooleanToken) ) {
            ArrayList returnValue = new ArrayList();
            for (int i=0; i<((ArrayToken)token).length(); i++) {
                returnValue.add(
                        generateValue(((ArrayToken)token).getElement(i)));
            }
            return returnValue;
        } else {
            if (token.toString() != null) {
                System.out.println("Token type " + token.getClass().toString()
                    + " is not support, " +
                    "so outout as StringToken with value: "
                    + token.toString());
                return token.toString();
            } else {
                System.out.println("Token type is not support, output null.");
                return null;
            }
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////                  private methods                          ////

    private static String _formatString(String str) {
        StringBuffer buffer = new StringBuffer(str);
            for(int i = 0; i < buffer.length(); i++ ) {
                switch(buffer.charAt(i)) {
                case '\b':
                    buffer.deleteCharAt(i);
                    buffer.insert(i, "\\b");
                    i += 1;
                    break;
                case '\f':
                    buffer.deleteCharAt(i);
                    buffer.insert(i, "\\f");
                    i += 1;
                    break;
                case '\n':
                    buffer.deleteCharAt(i);
                    buffer.insert(i, "\\n");
                    i += 1;
                    break;
                case '\r':
                    buffer.deleteCharAt(i);
                    buffer.insert(i, "\\r");
                    i += 1;
                    break;
                case '\t':
                    buffer.deleteCharAt(i);
                    buffer.insert(i, "\\t");
                    i += 1;
                    break;
                case '\\':
                    buffer.deleteCharAt(i);
                    buffer.insert(i, "\\\\");
                    i += 1;
                    break;
                case '\'':
                    buffer.deleteCharAt(i);
                    buffer.insert(i, "\\\'");
                    i += 1;
                    break;
                case '\"':
                    buffer.deleteCharAt(i);
                    buffer.insert(i, "\\\"");
                    i += 1;
                    break;
            }
        }
        return buffer.toString();
    }
}


