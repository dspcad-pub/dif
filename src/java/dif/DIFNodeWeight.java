/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif;

//////////////////////////////////////////////////////////////////////////
//// DIFNodeWeight

import dif.util.graph.Graph;
import dif.util.graph.Node;

/**
 * Information associated with an DIF node. {@link DIFNodeWeight}s
 * are objects associated with {@link Node}s that represent DIF
 * nodes in {@link Graph}s.  This class caches frequently-used
 * data associated with DIF nodes.  It is also useful for representing
 * intermediate DIF graph representations that do not correspond to
 * Ptolemy II models, and performing graph transformations
 * (e.g. conversion to and manipulation of single-rate graphs).  It is
 * intended for use with analysis/synthesis algorithms that operate on
 * generic graph representations of DIF models.  <p> Since computation is
 * already associated with nodes at this level, we add the functions
 * necessary to run elements.
 *
 * @author Michael Rinehart, Shuvra S. Bhattacharyya
 * @version $Id: DIFNodeWeight.java 606 2008-10-08 16:29:47Z plishker $
 * @see Node
 * @see DIFEdgeWeight
 */

public class DIFNodeWeight implements Cloneable {

    /**
     * Construct a DIF node weight that is not associated with any computation.
     */
    public DIFNodeWeight() {
        setComputation(null);
    }

    /**
     * Construct a DIF node weight that is associated with the given
     * computation.
     */
    public DIFNodeWeight(Object computation) {
        setComputation(computation);
    }

    ///////////////////////////////////////////////////////////////////
    ////                       public methods                      ////

    /**
     * Overrides the protected method Object.clone(). It uses
     * Object.clone() to "shallow" copy the fields and returns the cloned
     * version.
     */
    public Object clone() {
        //getClass() can get the correct derived class
        try {
            Object returnObj = getClass().newInstance();
            ((DIFNodeWeight) returnObj).setComputation(_computation);
            return returnObj;
        } catch (Exception exp) {
            throw new RuntimeException("Connot clone node weight.");
        }
    }

    /**
     * Return the computation that is represented by the CSDF node.
     * Return null if no computation has been associated yet with the node.
     *
     * @return the represented computation.
     */
    public Object getComputation() {
        return _computation;
    }

    /**
     * Set the computation to be represented by the CSDF node.
     *
     * @param computation the computation.
     */
    public void setComputation(Object computation) {
        _computation = computation;
    }

    /**
     * Returns the string "DIFNodeWeight".
     *
     * @return The string "DIFNodeWeight".
     */
    public String toString() {
        return this.getClass().getName();
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    // The computation represented by the associated DIF graph node.
    private Object _computation;

}


