/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif;

import dif.util.Conventions;
import dif.util.graph.*;

import java.util.*;

/**
 * Information associated with an DIF graph.
 * This class caches frequently-used data associated with DIF graphs.
 * It is also useful for intermediate DIF graph representations
 * that do not correspond to Ptolemy II models, and performing graph
 * transformations (e.g. conversion to and manipulation of single-rate graphs).
 * It is intended for use with analysis/synthesis algorithms that operate
 * on generic graph representations of CSDF models.
 * <p>
 * The attribute mechanism in this class can be used to bind arbitrary
 * attributes
 * to elements ({@link dif.util.graph.Node} and ({@link dif.util.graph.Edge})
 * or the graph itself.
 * Implementation details of this mechanism can be found in
 * {@link AttributeContainer}
 * and {@link DIFAttribute}.
 * <p>
 * DIF supports parameterized values as parameters. The parameter mechanism of
 * DIFGraph handles this task. Implementation details can be found in
 * {@link DIFParameter}.
 * <p>
 * Containers for AttributeContainer and containers for DIFParameter are
 * implemented as LinkedHashMap. The "HashMap" property provides a good way to
 * retrieve contents and the "linked" property maintains the order of insertion.
 *
 * @author Chia-Jui Hsu, Fuat Keceli, Michael Rinehart, Shuvra S. Bhattacharyya
 * @version $Id: DIFGraph.java 720 2009-08-07 22:23:33Z nsane $
 * @see DIFEdgeWeight
 * @see DIFNodeWeight
 * @see AttributeContainer
 * @see DIFAttribute
 * @see DIFParameter
 */

public class DIFGraph extends DirectedGraph {

    // This is a hash map that has nodes/edges or String("graph")
    // (representing this graph) as keys and AttributeContainer's as values.
    //
    // Object(String("graph")/Edge/Node) -> AttributeContainer
    private LinkedHashMap _containers = new LinkedHashMap();
    // Stores the parameters that will be used in attributes.
    private LinkedHashMap<String, DIFParameter> _parameters =
            new LinkedHashMap<>();
    private HashMap _hideEdges = new HashMap();

    ///////////////////////////////////////////////////////////////////
    ////                       public methods                      ////
    // Stores the arrays, array names (string) -> LinkedList
    private LinkedHashMap _nodeArrays = new LinkedHashMap();
    private LinkedHashMap _edgeArrays = new LinkedHashMap();

    /**
     * Construct an empty DIF graph. Default name of the graph is "_graph".
     * Default names of elements are <code>n + </code><i>label</i> for nodes
     * and <code>e + </code><i>label</i> for edges.
     */
    public DIFGraph() {
        super();
        _setAttributeContainer(this, new AttributeContainer());
        setName("_graph");
        for (Iterator nodes = nodes().iterator(); nodes.hasNext(); ) {
            Element node = (Element) nodes.next();
            setName(node, "n" + nodeLabel(node));
        }
        for (Iterator edges = edges().iterator(); edges.hasNext(); ) {
            Element edge = (Element) edges.next();
            setName(edge, "e" + edgeLabel(edge));
        }
    }

    /**
     * Construct an empty DIF graph with enough storage allocated for the
     * specified number of nodes. Default name of the graph is "_graph".
     * Default names of elements are <code>n + </code><i>label</i> for nodes
     * and <code>e + </code><i>label</i> for edges.
     *
     * @param nodeCount The number of nodes.
     */
    public DIFGraph(int nodeCount) {
        super(nodeCount);
        _setAttributeContainer(this, new AttributeContainer());
        setName("_graph");
        for (Iterator nodes = nodes().iterator(); nodes.hasNext(); ) {
            Element node = (Element) nodes.next();
            setName(node, "n" + nodeLabel(node));
        }
        for (Iterator edges = edges().iterator(); edges.hasNext(); ) {
            Element edge = (Element) edges.next();
            setName(edge, "e" + edgeLabel(edge));
        }
    }

    /**
     * Construct an empty DIF graph with enough storage allocated for the
     * specified number of edges, and number of nodes. Default name of the
     * graph is "_graph". Default
     * names of elements are <code>n + </code><i>label</i> for nodes and
     * <code>e + </code><i>label</i> for edges.
     *
     * @param nodeCount The integer specifying the number of nodes
     * @param edgeCount The integer specifying the number of edges
     */
    public DIFGraph(int nodeCount, int edgeCount) {
        super(nodeCount, edgeCount);
        _setAttributeContainer(this, new AttributeContainer());
        setName("_graph");
        for (Iterator nodes = nodes().iterator(); nodes.hasNext(); ) {
            Element node = (Element) nodes.next();
            setName(node, "n" + nodeLabel(node));
        }
        for (Iterator edges = edges().iterator(); edges.hasNext(); ) {
            Element edge = (Element) edges.next();
            setName(edge, "e" + edgeLabel(edge));
        }
    }

    /**
     * This operation is unsupported and always throws a
     * GraphWeightException. This is because unweighted edges in
     * DIFGraphs are not allowed.
     *
     * @throws GraphWeightException Always thrown.
     */
    @Override public Collection addEdge(Object weight1, Object weight2) {
        throw new GraphWeightException(
                "DIFGraph doesn't accept unweighted edges.");
    }

    /**
     * This operation is unsupported and always throws a
     * GraphWeightException. This is because unweighted edges in
     * DIFGraphs are not allowed.
     *
     * @throws GraphWeightException Always thrown.
     */
    @Override public Edge addEdge(Node node1, Node node2) {
        throw new GraphWeightException(
                "DIFGraph doesn't accept unweighted edges.");
    }

    /**
     * Replaces a node in the graph. Overloaded to make sure
     * attribute containers carry over to new graph.
     *
     * @param oldnode The node to be replaced
     * @param newnode The node to replace it with
     * @return True if successful
     */
    @Override public boolean replaceNode(Node oldnode, Node newnode) {
        AttributeContainer ac = _getAttributeContainer(oldnode);
        if (super.replaceNode(oldnode, newnode)) {
            _setAttributeContainer(newnode, ac);
            return true;
        }
        return false;
    }

    /**
     * Test if a graph is equal to this one. It is equal
     * if it is of the same class, and has the same sets of nodes/edges and
     * all the attributes are the same. For two attributes to be the same
     * they should have the same names and same values but not necessarily the
     * same objects.
     * If parameters and attributes refer to other objects, it may not
     * "equal" because comparing refered objects are hard to implemented.
     *
     * @param graph The graph with which to compare this graph.
     * @return True if the graph is equal to this one.
     * TODO need test
     */
    @Override public boolean equals(Object graph) {
        if (super.equals(graph)) {
            if (!_getAttributeContainer(this).equals(
                    ((DIFGraph) graph)._getAttributeContainer(graph))) {
                return false;
            }
            for (Iterator edges = edges().iterator(); edges.hasNext(); ) {
                Element edge = (Element) edges.next();
                if (!_getAttributeContainer(edge).equals(
                        ((DIFGraph) graph)._getAttributeContainer(edge))) {
                    return false;
                }
            }
            for (Iterator nodes = nodes().iterator(); nodes.hasNext(); ) {
                Element node = (Element) nodes.next();
                if (!_getAttributeContainer(node).equals(
                        ((DIFGraph) graph)._getAttributeContainer(node))) {
                    return false;
                }
            }
            if (!_parameters.equals(((DIFGraph) graph)._parameters)) {
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * Returns the DIFAttribute associated with this graph.
     *
     * @param name The name of this DIFAttribute.
     * @return The DIFAttribute with the given name.
     * Null if the attribute doesn't exist.
     * @see DIFAttribute
     * @see AttributeContainer
     */
    public DIFAttribute getAttribute(String name) {
        AttributeContainer container = _getAttributeContainer(this);
        return container.getAttribute(name);
    }

    /**
     * Returns the DIFAttribute associated with the element.
     *
     * @param element An edge or node in this graph.
     * @param name    The name of this DIFAttribute.
     *                use {@link #getAttribute(Element, String)}.
     * @return The DIFAttribute with the given name.
     * Null if the attribute doesn't exist.
     * @throws GraphElementException If the element is not contained in
     *                               the graph.
     * @see DIFAttribute
     * @see AttributeContainer
     */
    public DIFAttribute getAttribute(Element element, String name) {
        AttributeContainer container = _getAttributeContainer(element);
        return container.getAttribute(name);
    }

    /**
     * Returns a LinkedList of DIFAttribute associated with this graph.
     *
     * @return A LinkedList of DIFAttribute associated with this graph.
     * @see AttributeContainer
     */
    public LinkedList getAttributes() {
        AttributeContainer container = _getAttributeContainer(this);
        return container.getAttributes();
    }

    /**
     * Returns a LinkedList of DIFAttribute associated with this element.
     *
     * @param element An edge or node in this graph.
     * @return A LinkedList of DIFAttribute associated with this element.
     * @see AttributeContainer
     */
    public LinkedList getAttributes(Element element) {
        AttributeContainer container = _getAttributeContainer(element);
        return container.getAttributes();
    }

    /**
     * Returns the first DIFAttribute of this graph that contains the given
     * value object.
     *
     * @param value The value object (token or object reference) contained
     *              by this DIFAttribute.
     * @return The DIFAttribute if found, otherwise, null.
     */
    public DIFAttribute getAttributeByContent(Object value) {
        AttributeContainer container = _getAttributeContainer(this);
        return container.getAttributeByContent(value);
    }

    /**
     * Returns the DIFAttributes in this graph that contain the given
     * value object.
     *
     * @param value The value object.
     * @return The LinkedList of DIFAttributes containes the value.
     * Or an empty LinkedList if not found.
     */
    public LinkedList getAttributesByContent(Object value) {
        AttributeContainer container = _getAttributeContainer(this);
        return container.getAttributesByContent(value);
    }

    /**
     * Returns the first DIFAttribute of the element that contains the given
     * value.
     *
     * @param element An edge or node in this graph.
     * @param value   The value object (token or object reference) contained
     *                by this DIFAttribute.
     * @return attribute if found, otherwise, null.
     */
    public DIFAttribute getAttributeByContent(Element element, Object value) {
        AttributeContainer container = _getAttributeContainer(element);
        return container.getAttributeByContent(value);
    }

    /**
     * Returns the DIFAttributes of the element that contain the given
     * value object.
     *
     * @param value The value object.
     * @return The LinkedList of DIFAttributes containes the value.
     * Or an empty LinkedList if not found.
     */
    public LinkedList getAttributesByContent(Element element, Object value) {
        AttributeContainer container = _getAttributeContainer(element);
        return container.getAttributesByContent(value);
    }

    /**
     * Returns the description of the graph attributes as defined
     * in {@link #getAttributeDescriptions(Element)}.
     *
     * @return Attribute description of the graph.
     */
    public String getAttributeDescriptions() {
        AttributeContainer container = _getAttributeContainer(this);
        return container.toString();
    }

    /**
     * Returns the description of a graph element (node/edge) with its
     * attributes.This description contains the name of the object as defined
     * by {@link #setName} method, names and values of the attributes.
     *
     * @param element An edge or node in this graph.
     * @return Attribute description of the object.java -cp "../..;." dif
     * .CoreFunctionNodeTest  bdf-csdf-test.dif  bdf-csdf-test-sched.dif
     * @throws GraphElementException If the element is not contained in
     *                               the graph.
     */
    public String getAttributeDescriptions(Element element) {
        AttributeContainer container = _getAttributeContainer(element);
        return container.toString();
    }

    /**
     * Returns a list of attribute names that can be found associated with
     * this graph. The list will be sorted.
     *
     * @return A list of Strings.
     */
    public LinkedList getAttributeNames() {
        AttributeContainer container = _getAttributeContainer(this);
        return container.getAttributeNames();
    }

    /**
     * Returns a list of attribute names that can be found associated with
     * this element. The list will be sorted.
     *
     * @param element An edge or node in this graph.
     * @return A list of Strings.
     * @throws GraphElementException If the element is not contained in
     *                               the graph.
     */
    public LinkedList getAttributeNames(Element element) {
        AttributeContainer container = _getAttributeContainer(element);
        return container.getAttributeNames();
    }

    /**
     * Returns the edge by <i>name</i>.
     * Returns null if no such edge.
     *
     * @param name A name string.
     * @return An edge or null.
     */
    public Edge getEdge(String name) {
        for (Iterator edges = edges().iterator(); edges.hasNext(); ) {
            Edge edge = (Edge) edges.next();
            if (getName(edge).equals(name)) {
                return edge;
            }
        }
        return null;
    }

    /**
     * Returns the name of this graph which is assigned by the
     * {@link #setName} method. If no name or null was assigned as the name
     * it is set to empty string.
     *
     * @return The name of this graph.
     */
    public String getName() {
        return _getAttributeContainer(this).getName();
    }

    /**
     * Sets the name of this graph. If it is not set default is "_graph".
     *
     * @param name Name string.
     *             See {link dif.AttributeContainer#setName} for restrictions
     *             imposed on the name.
     * @return Previous name associated with this graph. Null if no name was
     * assigned previously.
     * @throws IllegalArgumentException If
     *                                  {@link Conventions#labelConvention}
     *                                  returns an error.
     */
    public void setName(String name) {
        AttributeContainer container = _getAttributeContainer(this);
        String returnValue = container.getName();
        container.setName(name);
    }

    /**
     * Returns the name of this edge/node which is assigned by
     * the {@link #setName} method. If no name or null was assigned as the
     * name it is set to empty string.
     *
     * @param element An edge or node in this graph.
     * @return The name of this object.
     * @throws GraphElementException If the element is not contained in
     *                               the graph.
     */
    public String getName(Element element) {
        return _getAttributeContainer(element).getName();
    }

    /**
     * Returns the node by <i>name</i>.
     * Returns null if no such node.
     *
     * @param name A name string.
     * @return A node or null.
     */
    public Node getNode(String name) {
        for (Iterator nodes = nodes().iterator(); nodes.hasNext(); ) {
            Node node = (Node) nodes.next();
            if (getName(node).equals(name)) {
                return node;
            }
        }
        return null;
    }


    /* Regarding to "public ScheduleTree makeSchedule(DIFGraph sched) "
        NS:  Modified to be consistent with mocgraph changes. (08/07/09)
       LIN: 09/28/15: NS's note is incorrect.
            The method is not consistent at all with multiple compilation
            errors!
            Has to be commented out on 09/28/15.
       WU:  08/29/16: commented function is remove from this file,  find this
        function
            in git repo commits before 08/29/16. At line 538.
    */

    /**
     * Returns the first object that has this name specified by getName
     * method. Returns null if no such object is found.
     *
     * @param name A string.
     * @return A node/edge in this graph or this graph itself. Null if name
     * is not found.
     */
    public Object getObject(String name) {
        Object returnValue = null;
        if (getName().equals(name)) {
            return this;
        }
        for (Iterator edges = edges().iterator(); edges.hasNext(); ) {
            Element edge = (Element) edges.next();
            if (getName(edge).equals(name)) {
                return edge;
            }
        }
        for (Iterator nodes = nodes().iterator(); nodes.hasNext(); ) {
            Element node = (Element) nodes.next();
            if (getName(node).equals(name)) {
                return node;
            }
        }
        return null;
    }

    /**
     * Returns the DIFParameter with the given name.
     *
     * @param name The name of the parameter.
     * @return The DIFParameter that is defined by the given parameter name.
     * Null
     * if not found.
     */
    public DIFParameter getParameter(String name) {
        return (DIFParameter) _parameters.get(name);
    }

    /**
     * Returns all the parameter names based on the order of insertion.
     *
     * @return A list of Strings.
     */
    public LinkedList<String> getParameterNames() {
        return new LinkedList<>(_parameters.keySet());
    }

    /**
     * Returns all {@link DIFParameter}s.
     *
     * @return A list of Strings.
     */
    public LinkedList<DIFParameter> getParameters() {
        return new LinkedList<>(_parameters.values());
    }

    /**
     * Returns the hash code for this graph.
     *
     * @return The hash code for this graph.
     */
    @Override public int hashCode() {
        int code = super.hashCode();
        for (Iterator edges = edges().iterator(); edges.hasNext(); ) {
            Element edge = (Element) edges.next();
            code += _getAttributeContainer(edge).hashCode();
        }
        for (Iterator nodes = nodes().iterator(); nodes.hasNext(); ) {
            Element node = (Element) nodes.next();
            code += _getAttributeContainer(node).hashCode();
        }
        code += _getAttributeContainer(this).hashCode();
        code += _parameters.hashCode();
        return code;
    }

    /**
     * Hide an edge in this graph. The AttributeContainer of this edge and
     * all attributes
     * in this graph that contains this edge as value can be recovered.
     *
     * @param edge The edge to be removed.
     * @return True if the edge was removed.
     * @see Graph#hideEdge
     */
    public boolean hideEdge(Edge edge) {
        boolean returnValue = super.hideEdge(edge);
        if (returnValue) {
            LinkedList restores = _removeReferenceFromAttributes(edge);
            restores.add(_removeAttributeContainer(edge));
            _hideEdges.put(edge, restores);
        }
        return returnValue;
    }

    /**
     * Merge <i>graph</i> into this graph.
     * Nodes, edges, node attributes, and edge attributes are transferred
     * merged (not copied) to this graph. Self attributes and parameters of
     * <i>graph</i> are also transferred/merged (not copied) but their names
     * are all prefixed by the <i>graph</i> name.
     * <p>
     * Note that after performing this method, use of the original
     * <i>graph</i> is not recommended since certain cross references are
     * set to this graph.
     *
     * @param graph The graph to add.
     * @return True if this graph changed as a result of this call.
     * @throws GraphConstructionException If a node or edge in the source
     *                                    graph already exists in this graph.
     */
    public boolean mergeGraph(Graph graph) {
        boolean returnValue = super.addGraph(graph);
        if (graph instanceof DIFGraph) {
            for (Iterator nodes = graph.nodes().iterator(); nodes.hasNext(); ) {
                Node node = (Node) nodes.next();
                //prevent naming conflict
                setName(
                        node, ((DIFGraph) graph).getName() + "_" +
                                ((DIFGraph) graph).getName(node));
                for (Iterator attributes = ((DIFGraph) graph).getAttributes(
                        node).iterator(); attributes.hasNext(); ) {
                    DIFAttribute attribute = (DIFAttribute) attributes.next();
                    setAttribute(node, attribute);
                }
            }
            for (Iterator edges = graph.edges().iterator(); edges.hasNext(); ) {
                Edge edge = (Edge) edges.next();
                setName(
                        edge, ((DIFGraph) graph).getName() + "_" +
                                ((DIFGraph) graph).getName(edge));
                for (Iterator attributes = ((DIFGraph) graph).getAttributes(
                        edge).iterator(); attributes.hasNext(); ) {
                    DIFAttribute attribute = (DIFAttribute) attributes.next();
                    setAttribute(edge, attribute);
                }
            }
            for (Iterator attributes =
                 ((DIFGraph) graph).getAttributes().iterator();
                 attributes.hasNext(); ) {
                DIFAttribute attribute = (DIFAttribute) attributes.next();
            }
            for (Iterator parameters =
                 ((DIFGraph) graph).getParameters().iterator();
                 parameters.hasNext(); ) {
                DIFParameter param = (DIFParameter) parameters.next();

                //new way to handle parameter name conflicting
                if (getParameter(param.getName()) != null) {
                    param._setName(((DIFGraph) graph).getName() + "_" +
                                           param.getName());
                }
                setParameter(param);
            }
        }
        return returnValue;
    }

    /**
     * Mirror this graph. For completely mirroring, please use
     * {@link DIFHierarchy#mirror()}.
     */
    public DIFGraph mirror() {
        return mirror(new HashMap());
    }

    /**
     * Mirror this graph. For completely mirroring, please use
     * {@link DIFHierarchy#mirror()}.
     *
     * @param mirrorMap external mappings of elements in hierarchy.
     *                  This method will also put mappings of graph elements,
     *                  parameters,
     *                  attribute containers into mirrorMap.
     * @return The mirrored graph
     */
    public DIFGraph mirror(Map mirrorMap) {
        //mirrorGraph has runtime type equal to the derived graph class.
        DIFGraph mirrorGraph = (DIFGraph) _emptyGraph();

        // mirror nodes
        for (Iterator nodes = nodes().iterator(); nodes.hasNext(); ) {
            Node node = (Node) nodes.next();
            DIFNodeWeight mirrorWeight =
                    (DIFNodeWeight) ((DIFNodeWeight) node.getWeight()).clone();
            Node mirrorNode = new Node(mirrorWeight);
            mirrorGraph.addNode(mirrorNode);
            mirrorMap.put(node, mirrorNode);
        }
        // mirror edges
        for (Iterator edges = edges().iterator(); edges.hasNext(); ) {
            Edge edge = (Edge) edges.next();
            Node mirrorSource = (Node) mirrorMap.get(edge.source());
            Node mirrorSink = (Node) mirrorMap.get(edge.sink());
            DIFEdgeWeight mirrorWeight =
                    (DIFEdgeWeight) ((DIFEdgeWeight) edge.getWeight()).clone();
            Edge mirrorEdge = new Edge(mirrorSource, mirrorSink, mirrorWeight);
            mirrorGraph.addEdge(mirrorEdge);
            mirrorMap.put(edge, mirrorEdge);
        }

        // mirror paramteres
        for (Iterator parameters = getParameters().iterator();
             parameters.hasNext(); ) {
            DIFParameter param = (DIFParameter) parameters.next();
            DIFParameter mirrorParam = (DIFParameter) param.clone(mirrorMap);
            mirrorGraph.setParameter(mirrorParam);
            mirrorMap.put(param, mirrorParam);
        }

        // mirror node attributes
        for (Iterator nodes = nodes().iterator(); nodes.hasNext(); ) {
            Node node = (Node) nodes.next();
            Node mirrorNode = (Node) mirrorMap.get(node);
            AttributeContainer container = _getAttributeContainer(node);
            AttributeContainer mirrorContainer =
                    (AttributeContainer) container.clone(mirrorMap);
            mirrorGraph._setAttributeContainer(mirrorNode, mirrorContainer);
            mirrorMap.put(container, mirrorContainer);
        }

        // mirror edge attributes
        for (Iterator edges = edges().iterator(); edges.hasNext(); ) {
            Edge edge = (Edge) edges.next();
            Edge mirrorEdge = (Edge) mirrorMap.get(edge);
            AttributeContainer container = _getAttributeContainer(edge);
            AttributeContainer mirrorContainer =
                    (AttributeContainer) container.clone(mirrorMap);
            mirrorGraph._setAttributeContainer(mirrorEdge, mirrorContainer);
            mirrorMap.put(container, mirrorContainer);
        }

        // mirror graph attributes
        mirrorGraph._setAttributeContainer(mirrorGraph,
                               (AttributeContainer) _getAttributeContainer(
                                   this).clone(mirrorMap)
                                      );

        return mirrorGraph;
    }

    /**
     * Remove all DIFAttribute from the AttributeContainer associated with
     * this graph.
     */
    public void removeAllAttributes() {
        AttributeContainer container = _getAttributeContainer(this);
        container.removeAllAttributes();
    }

    /**
     * Remove all DIFAttribute from the AttributeContainer associated with
     * this element.
     *
     * @param element An edge or node in this graph.
     */
    public void removeAllAttributes(Element element) {
        AttributeContainer container = _getAttributeContainer(element);
        container.removeAllAttributes();
    }

    /**
     * Removes the DIFAttribute from the attribute container of the graph.
     *
     * @param attribute The DIFAttribute object.
     * @return Previous DIFAttribute if it exists, null otherwise.
     * @see AttributeContainer
     */
    public DIFAttribute removeAttribute(DIFAttribute attribute) {
        return _getAttributeContainer(this).removeAttribute(attribute);
    }

    /**
     * Removes the DIFAttribute with the given name
     * from the attribute container of the graph.
     *
     * @param name The name of the DIFAttribute to be deleted.
     * @return Previous DIFAttribute if it exists, null otherwise.
     * @see AttributeContainer
     */
    public DIFAttribute removeAttribute(String name) {
        return _getAttributeContainer(this).removeAttribute(name);
    }

    /**
     * Removes the DIFAttribute from the attribute container of the
     * element.
     *
     * @param element   An edge or node in this graph.
     * @param attribute The DIFAttribute object.
     * @return Previous DIFAttribute if it exists, null otherwise.
     * @see AttributeContainer
     */
    public DIFAttribute removeAttribute(Element element,
                                        DIFAttribute attribute) {
        return _getAttributeContainer(element).removeAttribute(attribute);
    }

    /**
     * Removes the DIFAttribute with the given name
     * from the attribute container of this element.
     *
     * @param element An edge or node in this graph.
     * @param name    The name of the attribute to be deleted.
     * @return Previous attribute if it exists, null otherwise.
     * @throws GraphElementException If the element is not contained in
     *                               the graph.
     * @see AttributeContainer
     */
    public DIFAttribute removeAttribute(Element element, String name) {
        return _getAttributeContainer(element).removeAttribute(name);
    }

    //Note: Grpah should not aware of Hierarchy
    /* Sets the back reference to the hierarchy that contains this graph.
     *  Users should not use this method. It's public because
     *  dif.graph.hierarchy.Hierarchy needs to use this method to set
     *  the back reference.
     *  @param hierarchy
     */
    /*public void setHierarchy(DIFHierarchy hierarchy) {
        _hierarchy = hierarchy;
    }*/

    /**
     * Restore an edge in this graph.
     * The AttributeContainer of this edge
     * and all attributes in this graph that
     * contains thie edge as value can be recovered.
     *
     * @param edge The edge to be removed.
     * @return True if the edge was removed.
     * @see Graph#hideEdge
     */
    public boolean restoreEdge(Edge edge) {
        boolean returnValue = super.restoreEdge(edge);
        LinkedList restores = (LinkedList) _hideEdges.get(edge);
        for (Iterator objs = restores.iterator(); objs.hasNext(); ) {
            Object obj = objs.next();
            if (obj instanceof DIFAttribute) {
                if (((DIFAttribute) obj).getValue() == null) {
                    ((DIFAttribute) obj).setValue(edge);
                } else if (((DIFAttribute) obj)
                        .getValue() instanceof LinkedList) {
                    ((LinkedList) ((DIFAttribute) obj).getValue()).add(edge);
                }
            } else if (obj instanceof AttributeContainer) {
                _setAttributeContainer(edge, (AttributeContainer) obj);
            }
        }
        return returnValue;
    }

    /**
     * Remove an edge from this graph.
     * An edge that is removed from a graph can be re-inserted
     * into the graph at a later time (using {@link #addEdge(Edge)}),
     * provided that the incident nodes are still in the graph. However
     * previous attributes of the edge won't be recovered.
     *
     * @param edge The edge to be removed.
     * @return True if the edge was removed.
     */
    public boolean removeEdge(Edge edge) {
        boolean returnValue = super.removeEdge(edge);
        if (returnValue) {
            _removeReferenceFromAttributes(edge);
            _removeAttributeContainer(edge);
        }
        return returnValue;
    }

    /**
     * Remove a node from this graph.
     * All edges incident to the node are also removed.
     *
     * @param node The node to be removed.
     * @return True if the node was removed.
     */
    public boolean removeNode(Node node) {
        boolean returnValue = super.removeNode(node);
        if (returnValue) {
            _removeReferenceFromAttributes(node);
            _removeAttributeContainer(node);
        }
        return returnValue;
    }

    /**
     * Removes the parameter defined by the given name from the parameter list.
     *
     * @param name Name of the interval to be removed.
     * @return Removed parameter if it exists, null otherwise.
     */
    public DIFParameter removeParameter(String name) {
        DIFParameter parameter = getParameter(name);
        _removeReferenceFromAttributes(parameter);
        return (DIFParameter) _parameters.remove(name);
    }

    /**
     * Sets an attribute of the graph. If there exist another attribute with
     * the same name and type, the previous attribute is overridden.<p>
     *
     * @param attribute A DIFAttribute.
     * @throws IllegalArgumentException If the name of attribute is null.
     */
    public void setAttribute(DIFAttribute attribute) {
        AttributeContainer container = _getAttributeContainer(this);
        container.setAttribute(attribute);
    }

    /**
     * Sets an attribute of an element. If there exist another attribute with
     * the same name previous value is overridden.<p>
     *
     * @param element   An edge or node in this graph.
     * @param attribute A DIFAttribute.
     * @throws GraphElementException    If the element is not contained in
     *                                  the graph.
     * @throws IllegalArgumentException If the name of attribute is null.
     */
    public void setAttribute(Element element, DIFAttribute attribute) {
        AttributeContainer container = _getAttributeContainer(element);
        container.setAttribute(attribute);
    }

    /**
     * Sets the name of this edge/node. Default
     * names of elements are <code>n + </code><i>label</i> for nodes and
     * <code>e + </code><i>label</i> for edges.
     *
     * @param element An edge or node in this graph.
     * @param name    Name string.
     * @return Previous name associated with this object. Null if no name was
     * assigned previously.
     * @throws GraphElementException    If the element is not contained in
     *                                  the graph.
     * @throws IllegalArgumentException {@link Conventions#labelConvention}
     * returns an error.
     */
    public String setName(Element element, String name) { //WLP
        AttributeContainer container = _getAttributeContainer(element);
        String returnValue = container.getName();
        container.setName(name);
        return returnValue;
    }

    /**
     * Add a DIFParameter to the graph.
     *
     * @param parameter A DIFParameter with a name.
     * @return Old DIFParameter with the same name, null otherwise.
     */
    public DIFParameter setParameter(DIFParameter parameter) {
        if (parameter.getName() == null) {
            throw new IllegalArgumentException("Null parameter name.");
        }

        DIFParameter returnValue = null;
        if (_parameters.get(parameter.getName()) != null) {
            returnValue = (DIFParameter) _parameters.get(parameter.getName());
        }

        //set the back reference to this graph
        parameter.setContainer(this);
        //HashMap.put(String, object) will replace the entry with the same
        //key. Note that the same key for String is depends on their
        //content (String.equals()) not their object address.
        _parameters.put(parameter.getName(), parameter);

        return returnValue;
    }

    /**
     * Overrides the method in <code>mocgraph.Graph</code> to copy the
     * attributes as well. For each element copied, attributes will be copied
     * together. Special attention should be given that parameter values
     * are not copied therefore values of the parameterized attributes should
     * be copied from the original graph manually (if desired).
     *
     * @param nodes The collection of nodes; each element is a {@link Node}.
     * @return The induced subgraph with a runtime type of DIFGraph.
     */
    public Graph subgraph(Collection nodes) {
        DIFGraph subgraph = (DIFGraph) super.subgraph(nodes);
        for (Iterator subNodes = subgraph.nodes().iterator();
             subNodes.hasNext(); ) {
            Element node = (Element) subNodes.next();
            String nodeName = getName(node);
            if (nodeName != null) {
                subgraph.setName(node, nodeName);
            }
            for (Iterator attributes = getAttributes(node).iterator();
                 attributes.hasNext(); ) {
                subgraph.setAttribute(
                        node, (DIFAttribute) ((DIFAttribute) attributes.next())
                                .clone());
            }
        }
        for (Iterator edges = subgraph.edges().iterator(); edges.hasNext(); ) {
            Element edge = (Element) edges.next();
            String edgeName = getName(edge);
            if (edgeName != null) {
                subgraph.setName(edge, edgeName);
            }
            for (Iterator attributes = getAttributes(edge).iterator();
                 attributes.hasNext(); ) {
                subgraph.setAttribute(
                        edge, (DIFAttribute) ((DIFAttribute) attributes.next())
                                .clone());
            }
        }
        return subgraph;
    }

    /**
     * Overrides the method in <code>mocgraph.Graph</code> to copy the
     * attributes as well. For each element copied, attributes will be copied
     * together. Special attention should be given that parameter values
     * are not copied therefore values of the parameterized attributes should
     * be copied from the original graph manually (if desired).
     *
     * @param nodeCollection The subset of nodes; each element is an instance
     *                       of {@link Node}.
     * @param edgeCollection The subset of edges. Each element is an instance
     *                       of {@link Edge}.
     * @return The subgraph with a runtime type of DIFGraph.
     */
    public Graph subgraph(Collection nodeCollection,
                          Collection edgeCollection) {
        DIFGraph subgraph = (DIFGraph) super.subgraph(nodeCollection,
                                                      edgeCollection
                                                     );
        for (Iterator subNodes = subgraph.nodes().iterator();
             subNodes.hasNext(); ) {
            Element node = (Element) subNodes.next();
            String nodeName = getName(node);
            if (nodeName != null) {
                subgraph.setName(node, nodeName);
            }
            for (Iterator attributes = getAttributes(node).iterator();
                 attributes.hasNext(); ) {
                subgraph.setAttribute(
                        node, (DIFAttribute) ((DIFAttribute) attributes.next())
                                .clone());
            }
        }
        for (Iterator edges = subgraph.edges().iterator(); edges.hasNext(); ) {
            Element edge = (Element) edges.next();
            String edgeName = getName(edge);
            if (edgeName != null) {
                subgraph.setName(edge, edgeName);
            }
            for (Iterator attributes = getAttributes(edge).iterator();
                 attributes.hasNext(); ) {
                subgraph.setAttribute(
                        edge, (DIFAttribute) ((DIFAttribute) attributes.next())
                                .clone());
            }
        }
        return subgraph;
    }

    /**
     * Returns true if the given object is a valid edge weight for this graph.
     *
     * @return True if the given object is a valid edge weight for this graph.
     */
    public boolean validEdgeWeight(Object object) {
        return object instanceof DIFEdgeWeight;
    }

    /**
     * Returns true if the given object is a valid node weight for this graph.
     *
     * @return True if the given object is a valid node weight for this graph.
     */
    public boolean validNodeWeight(Object object) {
        return object instanceof DIFNodeWeight;
    }

    public void flagAsArrayElement(Element element) {
        AttributeContainer container = _getAttributeContainer(element);
        container.flagAsArrayElement();
    }

    public void flagAsNonArrayElement(Element element) {
        AttributeContainer container = _getAttributeContainer(element);
        container.flagAsNonArrayElement();
    }

    public void setArrayIndex(Element element, int index) {
        AttributeContainer container = _getAttributeContainer(element);
        container.setArrayIndex(index);
    }

    public void setNodeArray(String arrayName) {
        LinkedList array = new LinkedList();
        _nodeArrays.put(arrayName, array);
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    public void setEdgeArray(String arrayName) {
        LinkedList array = new LinkedList();
        _edgeArrays.put(arrayName, array);
    }

    public void addToNodeArray(String arrayName, Node node) {
        LinkedList array = (LinkedList) _nodeArrays.get(arrayName);
        array.add(node);
    }

    public void addToEdgeArray(String arrayName, Edge edge) {
        LinkedList array = (LinkedList) _edgeArrays.get(arrayName);
        array.add(edge);
    }

    public LinkedList getNodeArray(String name) {
        return (LinkedList) _nodeArrays.get(name);
    }

    public LinkedList getEdgeArray(String name) {
        return (LinkedList) _edgeArrays.get(name);
    }

    /**
     * Returns a container in which attributes of
     * an edge, a node or the graph are stored.<P>
     * In setName method name of the returned container is used as the name
     * of the edge/node or graph.
     *
     * @param object A graph edge/node or the graph itself.
     * @return A container which contains all the attributes (if any) of this
     * edge/node.
     * @throws GraphElementException If the runtime class of this object
     *                               is an edge/node and it is not contained
     *                               in the graph.
     */
    protected AttributeContainer _getAttributeContainer(Object object) {
        if (object instanceof Edge) {
            if (!containsEdge((Edge) object)) {
                throw new GraphElementException((Edge) object, this,
                                                "Attempt to get the" +
                                                        " attributes of an " +
                                                        "edge that is not in " +
                                                        "the graph."
                );
            }
            return (AttributeContainer) _containers.get(object);
        }
        if (object instanceof Node) {
            if (!containsNode((Node) object)) {
                throw new GraphElementException((Node) object, this,
                                                "Attempt to get the" +
                                                        " attributes of a " +
                                                        "node that is not in " +
                                                        "the graph."
                );
            }
            return (AttributeContainer) _containers.get(object);
        }
        if (object == this) {
            return (AttributeContainer) _containers.get("graph");
        }
        throw new GraphStateException("This exception should not happen.");
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    /**
     * Register a new edge in the graph. The edge is assumed to
     * be non-null, unique, and consistent with the node set.
     *
     * @param edge The new edge.
     * @throws IllegalArgumentException If the edge is unweighted or
     *                                  has a weight that is not an instance
     *                                  of {@link DIFEdgeWeight}.
     */
    protected void _registerEdge(Edge edge) {
        if (!edge.hasWeight()) {
            throw new IllegalArgumentException(
                    "Attempt to add an unweighted " + "edge to " +
                            getClass().getName() + ".\n");
        } else if (!validEdgeWeight(edge.getWeight())) {
            throw new IllegalArgumentException("Invalid edge weight class: " +
                                                       edge.getWeight()
                                                           .getClass()
                                                           .getName() + ".\n" +
                                                       "A dump of the " +
                                                       "offending edge weight" +
                                                       " follows.\n" +
                                                       edge.getWeight()
                                                           .toString() + ".\n");
        } else {
            super._registerEdge(edge);
            _setAttributeContainer(edge, new AttributeContainer());
        }
    }

    /**
     * Register a new node in the graph. The node is assumed to
     * be non-null and unique.
     *
     * @param node The new node.
     * @throws IllegalArgumentException If the node is unweighted or
     *                                  has a weight that is not an instance
     *                                  of {@link DIFNodeWeight}.
     */
    protected void _registerNode(Node node) {
        //WLP - Why require this?
        if (!node.hasWeight()) {
            throw new IllegalArgumentException(
                    "Attempt to add an unweighted " + "node to " +
                            getClass().getName() + ".\n");
        } else if (!validNodeWeight(node.getWeight())) {
            throw new IllegalArgumentException("Invalid node weight class: " +
                                                       node.getWeight()
                                                           .getClass()
                                                           .getName() + ".\n" +
                                                       "A dump of the " +
                                                       "offending node weight" +
                                                       " follows.\n" +
                                                       node.getWeight()
                                                           .toString() + ".\n");
        } else {
            super._registerNode(node);
            _setAttributeContainer(node, new AttributeContainer());
        }
    }

    //The back reference to the DIFHierarchy that contains this graph.
    //private DIFHierarchy _hierarchy;

    /**
     * If any attribute in graph or elements of graph contains <i>refObj</i>
     * as value, remove that element object from the attribute.
     *
     * @return The LinkedList of DIFAttributes that contains <i>refObj</i>
     * as value.
     */
    protected LinkedList _removeReferenceFromAttributes(Object refObj) {
        LinkedList returnValue = new LinkedList();
        for (Iterator nodes = nodes().iterator(); nodes.hasNext(); ) {
            Node node = (Node) nodes.next();
            LinkedList atts = getAttributesByContent(node, refObj);
            returnValue.addAll(atts);
            for (Iterator attributes = atts.iterator();
                 attributes.hasNext(); ) {
                DIFAttribute attribute = (DIFAttribute) attributes.next();
                if (attribute.getValue() instanceof LinkedList) {
                    ((LinkedList) attribute.getValue()).remove(refObj);
                } else {
                    attribute.setValue(null);
                }
            }
        }
        for (Iterator edges = edges().iterator(); edges.hasNext(); ) {
            Edge edge = (Edge) edges.next();
            LinkedList atts = getAttributesByContent(edge, refObj);
            returnValue.addAll(atts);
            for (Iterator attributes = atts.iterator();
                 attributes.hasNext(); ) {
                DIFAttribute attribute = (DIFAttribute) attributes.next();
                if (attribute.getValue() instanceof LinkedList) {
                    ((LinkedList) attribute.getValue()).remove(refObj);
                } else {
                    attribute.setValue(null);
                }
            }
        }
        //For graph attributes
        LinkedList atts = getAttributesByContent(refObj);
        returnValue.addAll(atts);
        for (Iterator attributes = atts.iterator(); attributes.hasNext(); ) {
            DIFAttribute attribute = (DIFAttribute) attributes.next();
            if (attribute.getValue() instanceof LinkedList) {
                ((LinkedList) attribute.getValue()).remove(refObj);
            } else {
                attribute.setValue(null);
            }
        }
        return returnValue;
    }

    /**
     * Sets a container for this object in which
     * attributes of an edge, a node or the graph can be stored. Returns the
     * previous container.
     * In setName method name of the returned container is used as the name
     * of the edge/node or graph.
     *
     * @param object    A graph edge/node or the graph itself.
     * @param container An AttributeContainer.
     * @return Previous container if it exists, null otherwise.
     */
    protected AttributeContainer _setAttributeContainer(Object object,
                                            AttributeContainer container) {
        //set the backward reference to this graph.
        container._setContainer(this);
        // No need to check if the object is in the graph or it is the
        // graph itself since this method is not being used by the user
        // even indirectly.
        if (object == this) {
            return (AttributeContainer) _containers.put("graph", container);
        }
        return (AttributeContainer) _containers.put(object, container);
    }

    /**
     * Remove the AttributeContainer of graph itself or graph element.
     * Note that, after the AttributeContainer for an element or the graph
     * ifself is removed, access the name of that element or the graph
     * itself is impossible.
     * This method is used when edges or nodes are removed or hidden.
     *
     * @param object A graph edge/node or the graph itself.
     * @return Previous container if it exists, null otherwise.
     */
    protected AttributeContainer _removeAttributeContainer(Object object) {
        if (object == this) {
            return (AttributeContainer) _containers.remove("graph");
        }
        return (AttributeContainer) _containers.remove(object);
    }
}


