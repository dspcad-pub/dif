/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

//package mocgraph;
package dif;

import dif.util.graph.Graph;
import dif.util.graph.Node;
import dif.util.graph.Edge;
import dif.cfdf.CFDFGraph;
import dif.data.Fraction;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.Collection;

import dif.attributes.CFDFAttributeType;
import dif.graph.hierarchy.Port;

////////////////////////////////////////////////////////////////////////// //
//Core Function Node

/**
 * A core function node is the functional equivalent of an actor node in
 * DIF hierarchy.  This class provides methods for generic core
 * functionalities associated with an actor.
 *
 * @author Nimish Sane, Shuvra S. Bhattacharyya
 * @version $Id: CoreFunctionNode.java 1634 2007-04-06 18:24:07Z ssb $
 * @see Node
 */
public class CoreFunctionNode extends DIFNodeWeight {
    /**
     * Construct a core function node with a given node weight.
     * Adds the universal mode of <code>null</code>.
     */

    public CoreFunctionNode() {
        super();
        _modes = new LinkedHashMap();
        _inputs = new LinkedHashMap();
        _outputs = new LinkedHashMap();
        _parameters = new LinkedHashMap();

        _nullMode = addMode("nullMode", CFDFAttributeType.NullMode);
        _initMode = addMode("init", CFDFAttributeType.InitMode);


    }


    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /**
     * Determines if a particular mode is enabled.
     * <p>
     * We intend that non-trivial actors can call the super at the
     * end, (assuming no other matching modes were found), so that
     * parent classes (including the base class) can evenutally check
     * the universal mode of <code>null</code>.
     *
     * @return True if an actor can be fired in <code>mode</code>.
     */
    public boolean enable(CoreFunctionMode mode) {
        if (mode.getType() == CFDFAttributeType.NullMode) {
            return true;
        }
        //if we're waiting in the special case of a bdf mode,
        // then check
        if (mode.getType() == CFDFAttributeType.BooleanMode) {
            //Note: we are assuming a direct name mapping between
            //internal state and port names.  This should be okay
            //because we construct the bdf modes inside of Mode, in
            //this manner.
            if (peek(mode.getName()) > 0) {
                return true;
            }
        }
        return false;
    }

    ;

    /**
     * Fires the actor in <code>mode</code>.
     * <p>
     * Same deal as with <code>enable()</code>, where overrides can
     * call this at the end of their execution.
     *
     * @param mode The identifier of the mode to be run.
     * @return Returns <code>true</code> after a sucessful
     * invocation, <code>false</code) otherwise.
     */

    public CoreFunctionMode invoke(String mode) {
        if (hasMode(mode)) {
            return invoke(getMode(mode));
        }

        return null;
    }

    ;


    /**
     * Fires the actor, if it is enabled for a particular mode.
     *
     * @param mode The identifier of the mode to be run.
     * @return Mode of the next state or <code>null</code> (note not
     * <code>nullMode</code>!) if there was an error, such as no
     * matching mode.
     */

    public CoreFunctionMode invoke(CoreFunctionMode mode) {
        if (mode == _nullMode) {
            //verbosely:
            //System.out.println(toString() + " has executed in the null mode");
            //non-verbosely:
            System.out.print("!");

            return _nullMode; //running in the null mode does nothing
            //to advance mode state

        } else if (mode == _initMode) {
            //if the actor hasn't specified the initMode invoke,
            // see if there's a boolean mode to give
            Iterator iter = _modes.values().iterator();
            CoreFunctionMode m = null;
            while (iter.hasNext()) {
                m = (CoreFunctionMode) iter.next();
                //System.out.println(toString() + getName() + ": Checking: "
                // + m.getName() );
                if (m.getType() == CFDFAttributeType.BooleanMode) {
                    return m;
                }
            }
            //if we found no mode, then just return some mode that's
            //not null or init
            iter = _modes.values().iterator();
            while (iter.hasNext()) {
                m = (CoreFunctionMode) iter.next();
                //System.out.println(toString()+  getName() + ": Checking: "
                // + m.getType() );
                if ((m.getType() != CFDFAttributeType.NullMode) &&
                        (m.getType() != CFDFAttributeType.InitMode)) {
                    return m;
                }
            }

            //otherwise, the designer has added no modes, so
            //just return null (or should this be an exception?)
            System.out.println("ERROR: " + getName() +
                                       " - invoke was called with init but " +
                                       "there was no valid mode.");
            if (_modes.size() <= 2) {
                System.out.println("ERROR: " + getName() +
                                           " - You appear to have added no " +
                                           "modes.");
            }
            System.out.println("ERROR: " + getName() +
                                       " - Is the computational type correct?" +
                                       " (Found: " +
                                       toString() + " )");
            return null;

        } else if (mode.getType() == CFDFAttributeType.BooleanMode) {
            Object token = pullToken(mode.getName());
            //try to interpret the obj as either a boolean or a number
            if (token instanceof Boolean) {
                if (((Boolean) token).booleanValue()) {
                    return mode.getTrueNextMode();
                } else {
                    return mode.getFalseNextMode();
                }
            } else {
                //if it's not a boolean token, don't move
                return mode;
            }
        }
        System.out.println("ERROR: " + getName() +
                                   " - invoke was called with an invalid mode");
        return null;
    }

    ;


    /**
     * Determines if this actor has a particular mode
     *
     * @return <code>true</code> if it has the mode,
     * <code>false</code> otherwise.
     */
    public boolean hasMode(String mode) {
        return _modes.containsKey(mode);
    }

    /**
     * Determines if this actor has a particular input
     *
     * @return <code>true</code> if it has the input,
     * <code>false</code> otherwise.
     */
    public boolean hasInput(String input) {
        return _inputs.containsKey(input);
    }

    /**
     * Determines if this actor has a particular output
     *
     * @return <code>true</code> if it has the output,
     * <code>false</code> otherwise.
     */
    public boolean hasOutput(String output) {
        return _outputs.containsKey(output);
    }


    /**
     * Determines if this actor has a particular mode
     *
     * @return <code>true</code> if it has the mode,
     * <code>false</code> otherwise.
     */
    public boolean hasMode(CoreFunctionMode mode) {
        return _modes.containsValue(mode);
    }


    /**
     * Gets a mode to an actor
     *
     * @return The mode corresponding to the label
     */
    public CoreFunctionMode getMode(String mode) {
        return (CoreFunctionMode) _modes.get(mode);
    }

    /**
     * Get a modes of an actor
     *
     * @return The modes of the actor.
     */
    public Collection getModes() {
        return _modes.values();
    }

    /**
     * Get a inputs of an actor
     *
     * @return The inputs of the actor.
     */
    public Collection getInputs() {
        return _inputs.values();
    }

    /**
     * Get a outputs of an actor
     *
     * @return The outputs of the actor.
     */
    public Collection getOutputs() {
        return _outputs.values();
    }


    public String getName() {
        return _graph.getName(_node);
    }


    /**
     * Sets the graph of the node.
     * <p>
     * For the time being, we need the containing graph to allow this node
     * to push and pull tokens to edges.
     *
     * @param graph The graph associate with this NodeWeight.
     */

    public void setGraph(CFDFGraph graph) {
        _graph = graph;
    }

    ;


    /**
     * Sets the node of this NodeWeight.
     *
     * @param node The node associated with this NodeWeight.
     */

    public void setNode(Node node) {
        _node = node;
    }

    ;


    /**
     * Get the type of this node.
     *
     * @return CFDFAttributeType.*
     */
    public CFDFAttributeType getNodeType() {
        return _nodeType;
    }

    /**
     * Get the probability of false control tokens. Only for BDF?
     *
     * @return The probability of false control tokens.
     */
    public Fraction getFalseProbability() {
        Fraction falseProb = new Fraction(1);
        falseProb.subtract(_probability);
        return falseProb;
    }

    /**
     * Get the probability of true control tokens.
     *
     * @return The probability of true control tokens.
     */
    public Fraction getTrueProbability() {
        return _probability;
    }

    /**
     * Set the type of this node.
     *
     * @param type BDFAttributeType.BooleanNode or BDFAttributeType.RegularNode.
     */
    public void setNodeType(CFDFAttributeType type) {
        _nodeType = type;
    }

    /**
     * Set probability of trun control tokens of associated BDFComputation.
     * The probability is represented in rational, i.e. numerator /
     * denominator.
     *
     * @param numerator
     * @param denominator
     */
    public void setTrueProbability(int numerator, int denominator) {
        if (_nodeType == CFDFAttributeType.BooleanNode) {
            if (denominator >= numerator) {
                _probability = new Fraction(numerator, denominator);
            } else {
                //throw new GraphWeightException(
                System.out.println("Probability is greater than one.");
            }
        } else {
            //            throw new GraphWeightException(
            System.out.println("BDF node is regular not boolean.");
        }
    }

    /**
     * Check visited.  This is can be used by graph algorithms such as DFS.
     *
     * @return visited
     */
    public boolean visited() {
        return _visited;
    }

    /**
     * Set the visited flag to <code>val</code>.
     *
     * @param val The new value of the visited flag.
     */
    public void setVisited(boolean val) {
        _visited = val;
    }

    /**
     * If this node has been visited, then it may have a mode it was visited as.
     *
     * @return The mode the node was in when it was previously visited.
     */

    public CoreFunctionMode getVisitedMode() {
        return _visitedMode;
    }

    /**
     * Set the visited flag on all modes, except the one passed in.
     */

    public void setAllVisitedExcept(CoreFunctionMode cfm) {
        Iterator iter = _modes.values().iterator();
        while (iter.hasNext()) {
            CoreFunctionMode m = (CoreFunctionMode) iter.next();
            if ((m.getType() != CFDFAttributeType.NullMode) &&
                    (m.getType() != CFDFAttributeType.InitMode) && (m != cfm)) {
                m.setVisited(true);
            }
        }
    }


    /**
     * Reset the visited flag on all modes of this actor.
     */

    public void resetAllModes() {
        Iterator iter = _modes.values().iterator();
        while (iter.hasNext()) {
            CoreFunctionMode m = (CoreFunctionMode) iter.next();
            if ((m.getType() != CFDFAttributeType.NullMode) &&
                    (m.getType() != CFDFAttributeType.InitMode)) {
                m.setVisited(false);
            }
        }
    }


    /**
     * Check if all modes have been visited.
     *
     * @return <code>true</code> if all modes visited, <code>false</code>
     * otherwise.
     */

    public boolean allModesVisited() {
        Iterator iter = _modes.values().iterator();
        while (iter.hasNext()) {
            CoreFunctionMode m = (CoreFunctionMode) iter.next();
            if ((m.getType() != CFDFAttributeType.NullMode) &&
                    (m.getType() != CFDFAttributeType.InitMode)) {
                if (!m.visited()) {
                    System.out.println("   mode not visited:" + m.getName());
                    return false;
                }
            }
        }
        //else
        return true;
    }


    /**
     * Set the current mode.
     *
     * @param val The mode to set the current mode to.
     */

    public void setVisitedMode(CoreFunctionMode val) {
        _visitedMode = val;
    }


    public String toString() {
        return super.toString();
    }

    ///////////////////////////////////////////////////////////////////
    ////                         protected methods                 ////

    /**
     * Adds a mode to an actor
     *
     * @param mode    The mode label.
     * @param modeObj The mode object.
     * @return The object of the mode.
     */
    protected CoreFunctionMode addMode(String mode, CoreFunctionMode modeObj) {
        if (hasMode(mode)) {
            System.out.println(
                    "WARNING: " + mode + " already exists in object " +
                            toString() + ".");
            return (CoreFunctionMode) _modes.get(mode);
        }
        //	System.out.println( toString() + ": adding: " + mode );
        _modes.put(mode, modeObj);
        return modeObj;
    }


    /**
     * Adds a mode to an actor
     *
     * @param mode The mode label.
     * @param type The mode object type.
     * @return The object of the mode.
     */
    protected CoreFunctionMode addMode(String mode, CFDFAttributeType type) {
        CoreFunctionMode modeObj = new CoreFunctionMode(mode,
                                                        type); //just have
        // the mode objects be
        return addMode(mode, modeObj);
    }


    /**
     * Adds a mode to an actor
     *
     * @return The object of the actor
     */
    protected CoreFunctionMode addMode(String mode) {
        CoreFunctionMode modeObj = new CoreFunctionMode(
                mode); //just have the mode objects be
        return addMode(mode, modeObj);
    }

    protected CoreFunctionMode addMode(String mode, CoreFunctionMode cfm1,
                                       CoreFunctionMode cfm2) {
        CoreFunctionMode modeObj = new CoreFunctionMode(
                mode, cfm1, cfm2); //just have the mode objects be
        return addMode(mode, modeObj);
    }

    /**
     * Adds a input to an actor
     *
     * @return The object of the input
     */
    protected String addInput(String input) {
        //WLP PROPOSED TO DO:
        //  Keep hash table keys in terms of edges, not names
        //  This will help support list structures.
        //	_graph.getName(getEdgeName(input))
        if (hasInput(input)) {
            System.out.println(
                    "WARNING: " + input + " already exists in object " +
                            toString() + ".");
            return (String) _inputs.get(input);
        }
        //	System.out.println( toString() + ": adding: " + mode );
        //if we ever make an input object, we would add it here
        _inputs.put(input, input);

        return input;
    }

    /**
     * Adds a output to an actor
     *
     * @return The object of the output
     */
    protected String addOutput(String output) {
        if (hasOutput(output)) {
            System.out.println(
                    "WARNING: " + output + " already exists in object " +
                            toString() + ".");
            return (String) _outputs.get(output);
        }
        //	System.out.println( toString() + ": adding: " + mode );
        //if we ever make an output object, we would add it here
        _outputs.put(output, output);

        return output;
    }

    /**
     * Gets all the actor attributes.
     *
     * @return List of all the actor attributes.
     */
    protected LinkedList getAllAttributes() {
        return _graph.getAttributes(_node);
    }

    /**
     * Gets the actor configuration. This method must be defined also within
     * each of the actors.
     *
     * @return List of all the actor attributes.
     */
    public LinkedList getActorConfiguration() {
        return getAllAttributes();
    }


    /**
     * Gets an edge associated with the output name.
     *
     * @param name The name of the output as defined by the actor.
     * @return The object of the actor
     */
    //protected\
    //name is a Port name.
    public Edge getEdge(String name) {
        if (_graph == null) {
            System.out.println(
                    "WARNING: getEdge: no graph exists for node " + toString() +
                            ".");
        }

        if (_node == null) {
            System.out.println(
                    "WARNING: getEdge: no node exists for node " + toString() +
                            ".");
        }

        LinkedList attributes = _graph.getAttributes(_node);

        if (attributes.size() == 0) {
            throw new RuntimeException(
                    "No actor attributes of node " + _graph.getName(_node));
        }

        for (int i = 1; i < attributes.size(); i++) {
            DIFAttribute attribute = (DIFAttribute) attributes.get(i);

            if (attribute.getValue() instanceof Edge) {
                if (name.equals(attribute.getName())) {
                    Edge edge = (Edge) attribute.getValue();
                    if (!_graph.containsEdge(edge)) {
                        throw new RuntimeException("Edge is not in the graph");
                    }
                    return edge;
                }
            } else if (attribute.getValue() instanceof Port) {
                if (name.equals(attribute.getName())) {
                    Port port = (Port) attribute.getValue();
                    if (port.getHierarchy().getGraph() != _graph) {
                        throw new RuntimeException("port is not in the graph");
                    }
                    if (port.getConnection() instanceof Edge) {
                        Edge edge = (Edge) port.getConnection();
                        return edge;
                    }

                    //Wu's modification BEG
                    while (port.getConnection() instanceof Port) {
                        port = (Port) port.getConnection();
                    }

                    Edge edge = (Edge) port.getConnection();
                    return edge;
                    //Wu's modification END
                }
            }
        }

        System.out.println(
                "WARNING: no edge named " + name + " for node " + toString() +
                        ".");
        return null;
    }

    //Nimish has temporarily modified this methos. Both implementations
    // should exist in future.

    /**
     * Gets name of input or output associated with an edge.
     * <p>
     * For now, we return either an input or an output and bank on
     * the fact that this is unambiguous.  If cycles are allowed in
     * the graph, we need to add logic to determine it and parsel
     * this out to two functions.
     *
     * @param e The name of the output as defined by the actor.
     * @return The object of the actor
     */
    //protected
    /* This is Will's original implementation
     */
    public String getActorPortName(Edge e) {
        if (_graph == null) {
            System.out.println(
                    "WARNING: no graph exists for node " + toString() + ".");
        }
        if (_node == null) {
            System.out.println(
                    "WARNING: no node exists for node " + toString() + ".");
        }

        LinkedList attributes = _graph.getAttributes(_node);

        if (attributes.size() == 0) {
            throw new RuntimeException(
                    "No actor attributes of node " + _graph.getName(_node));
        }

        //Probably should convert this to a hashmap
        for (int i = 1; i < attributes.size(); i++) {
            DIFAttribute attribute = (DIFAttribute) attributes.get(i);

            if (attribute.getValue() instanceof Edge) {
                if (e == attribute.getValue()) {
                    return attribute.getName();
                }
            }
        }
        return null;
    }


    /**
     * Gets name of input or output associated with a port.
     *
     * @param p The port assocaited with the actor.
     * @return The name of the corresponding actor port
     */

    public String getActorPortName(Port p) {
        if (_graph == null) {
            System.out.println(
                    "WARNING: no graph exists for node " + toString() + ".");
        }
        if (_node == null) {
            System.out.println(
                    "WARNING: no node exists for node " + toString() + ".");
        }

        LinkedList attributes = _graph.getAttributes(_node);

        if (attributes.size() == 0) {
            throw new RuntimeException(
                    "No actor attributes of node " + _graph.getName(_node));
        }

        //Probably should convert this to a hashmap
        for (int i = 1; i < attributes.size(); i++) {
            DIFAttribute attribute = (DIFAttribute) attributes.get(i);

            if (attribute.getValue() instanceof Port) {
                if (p == attribute.getValue()) {
                    return attribute.getName();
                }
            }
        }
        return null;
    }

    public String getEdgeName(Edge edge) {
        if (_graph == null) {
            System.out.println(
                    "WARNING: no graph exists for node " + toString() + ".");
        }
        if (_node == null) {
            System.out.println(
                    "WARNING: no node exists for node " + toString() + ".");
        }
        if (edge == null) {
            System.out.println(
                    "WARNING: no edge exists for node " + toString() + ".");
        }

        if (!_graph.containsEdge(edge)) {
            throw new RuntimeException("Edge is not in the graph");
        }

        return _graph.getName(edge);
    }

    // FIXME: Why is pushToken method public?? 

    /**
     * Puts a queue on an edge associated with the output name.
     *
     * @param outputName The name of the output as defined by the actor.
     */
    //protected
    public void pushToken(String outputName, Object token) {
        CoreFunctionEdge cfe = (CoreFunctionEdge) getEdge(outputName)
                .getWeight();
        if (cfe != null) {
            cfe.enqueue(token);
        } else {
            System.out.println(
                    "WARNING: no edge named " + outputName + " for node " +
                            toString() + ".");
        }
    }

    //Nimish

    /**
     * Puts a queue on an edge.
     *
     * @param edge The output edge of the actor.
     */

    public void pushToken(Edge edge, Object token) {
        CoreFunctionEdge cfe = (CoreFunctionEdge) edge.getWeight();
        if (cfe != null) {
            cfe.enqueue(token);
        } else {
            System.out.println(
                    "WARNING: no edge named " + _graph.getName(edge) +
                            " for node " + toString() + ".");
        }
    }


    /**
     * Gets token from a queue on an edge associated with the input name.
     *
     * @param inputName The name of the input as defined by the actor.
     * @return The object of the actor
     */
    protected Object pullToken(String inputName) {
        CoreFunctionEdge cfe = (CoreFunctionEdge) getEdge(inputName)
                .getWeight();
        if (cfe != null) {
            return cfe.dequeue();
        } else {
            System.out.println(
                    "WARNING: no edge named " + inputName + " on actor " +
                            toString() + ".  No tokens consumed.");
        }
        return null;
    }

    //Nimish

    /**
     * Gets token from a queue on an edge.
     *
     * @param edge Input edge.
     * @return The object of the actor
     */
    protected Object pullToken(Edge edge) {
        CoreFunctionEdge cfe = (CoreFunctionEdge) edge.getWeight();
        if (cfe != null) {
            return cfe.dequeue();
        } else {
            System.out.println(
                    "WARNING: no edge named " + _graph.getName(edge) +
                            " on actor " + toString() +
                            ".  No tokens consumed.");
        }
        return null;
    }

    /**
     * Peeks at the queue on an edge associated with the input name.
     *
     * @param inputName The name of the input as defined by the actor.
     * @return The object of the actorb
     */

    protected int peek(String inputName) {
        if (_graph == null) {
            System.out.println(
                    "WARNING: no graph exists for node " + toString() +
                            ".  No tokens peeked.");
        }
        if (_node == null) {
            System.out.println(
                    "WARNING: no node exists for node " + toString() +
                            ".  No tokens peeked.");
        }

        LinkedList attributes = _graph.getAttributes(_node);

        if (attributes.size() == 0) {
            throw new RuntimeException(
                    "No actor attributes of node " + _graph.getName(_node));
        }

        for (int i = 1; i < attributes.size(); i++) {
            DIFAttribute attribute = (DIFAttribute) attributes.get(i);

            if (attribute.getValue() instanceof Edge) {
                if (inputName.equals(attribute.getName())) {
                    Edge edge = (Edge) attribute.getValue();
                    if (!_graph.containsEdge(edge)) {
                        throw new RuntimeException("Edge is not in the graph");
                    }
                    return ((CoreFunctionEdge) edge.getWeight()).peek();
                }
            }//Wu's modification BEG
            else if (attribute.getValue() instanceof Port) {
                if (inputName.equals(attribute.getName())) {
                    Port port = (Port) attribute.getValue();

                    while (port.getConnection() instanceof Port) {
                        port = (Port) port.getConnection();
                    }

                    Edge edge = (Edge) port.getConnection();
                    return ((CoreFunctionEdge) edge.getWeight()).peek();
                }

            }//Wu's modification END
        }
        return 0;
    }

    //Nimish
    //peek() method with edge as the input

    /**
     * Peeks at the queue on an input edge.
     *
     * @param edge Input edge.
     * @return Number of tokens on the edge.
     */

    protected int peek(Edge edge) {
        if (_graph == null) {
            System.out.println(
                    "WARNING: no graph exists for node " + toString() +
                            ".  No tokens peeked.");
        }
        if (_node == null) {
            System.out.println(
                    "WARNING: no node exists for node " + toString() +
                            ".  No tokens peeked.");
        }

// 	LinkedList attributes = _graph.getAttributes(_node);

//         if (attributes.size() == 0) {
//             throw new RuntimeException("No actor attributes of node "
// 				       + _graph.getName(_node));
// 	}

//         for (int i=1; i<attributes.size(); i++) {
// 	    DIFAttribute attribute = (DIFAttribute)attributes.get(i);

// 	    if (attribute.getValue() instanceof Edge) {
// 		if (inputName.equals(attribute.getName())){
// 		    Edge edge = (Edge)attribute.getValue();

        if (!_graph.containsEdge(edge)) {
            throw new RuntimeException("Edge is not in the graph");
        }

        return ((CoreFunctionEdge) edge.getWeight()).peek();

// 		}
// 	    }
// 	}
// 	return 0;
    }

    //Nimish
    //Method to check enable condition, when consumption rates are already
    // set for all inputs for a given mode.

    /**
     * Check if the mode is enabled based on consumption rates already set.
     *
     * @param mode Mode of the actor
     * @return true if the actor can be enabled in the given mod; false
     * otherwise.
     */
    protected boolean isEnabled(CoreFunctionMode mode) {
        Collection edges = _graph.inputEdges(_node);
        Iterator iter = edges.iterator();
        while (iter.hasNext()) {
            Edge edge = (Edge) iter.next();
            //System.out.println("Edge: " + getEdgeName(edge) + "has " + peek
            // (edge) + " tokens \n");
            //System.out.println("Edge: " + getEdgeName(edge) + "has
            // consumption set to" + mode.getConsumption(getEdgeName(edge)) +
            // " tokens \n");
            if (peek(edge) < mode.getConsumption(getEdgeName(edge)))
                return false;
        }

        return true;
    }

    //Nimish

    /**
     * This method has been added to take care of subgraphs and hierarchies
     * in CFDF graphs.
     * Some of the actor attributes require input/control/output edges to be
     * specified in the DIF specification.
     * However, for an actor inside a subgraph, it will not be possible to
     * specify any(or all) of these edges.
     * In those cases one can instead specify correponding input or output
     * ports.
     * This method replaces Port elements in the input linked lists (which
     * typically contain actor attributes) with
     * their corresponding edges in the supergraph.
     *
     * @param list Linked List containing actor attribute values.
     * @return true if the all the elements are successfully replaced; false
     * otherwise.
     */
    protected boolean replacePorts(LinkedList list) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) instanceof Port) {
                Port port = (Port) list.get(i);
                //traverse the hierarchy to get the edge associated with this
                // port.
                //Note that this method traverses the hierarchy till it finds
                // an edge connected to the port.
                boolean edgeFound = false;
                do {
                    if (port.getHierarchy().getGraph() != _graph) {
                        throw new RuntimeException("port is not in the graph");
                    }

                    if (port.getConnection() instanceof Edge) {
                        Edge edge = (Edge) port.getConnection();
                        list.set(i, edge);
                        edgeFound = true;
                    } else {
                        port = (Port) port.getConnection();
                    }
                } while (!edgeFound);
            }
        }

        return true;
    }

    /**
     * Attempts to automatically create modes for an actor, based on the
     * dataflow description
     */

    public void createModes() {
        if (_graph == null) {
            System.out.println(
                    "WARNING: no graph exists for node " + toString() +
                            ".  No tokens produced.");
        }
        if (_node == null) {
            System.out.println(
                    "WARNING: no node exists for node " + toString() +
                            ".  No tokens produced.");
        }

        LinkedList attributes = _graph.getAttributes(_node);

        if (attributes.size() == 0) {
            throw new RuntimeException(
                    "No actor attributes of node " + _graph.getName(_node));
        }

        //add BDF modes
        //	System.out.println(getName() + "?");
        if (getNodeType() == CFDFAttributeType.BooleanNode) {
            //	    System.out.println(getName() + " is a boolean node.");
            for (int i = 1; i < attributes.size(); i++) {
                DIFAttribute attribute = (DIFAttribute) attributes.get(i);
                if (attribute.getValue() instanceof Edge) {
                    if (attribute.getName().equals("control")) {
                        Edge edge = (Edge) attribute.getValue();
                        //			System.out.println("found control input: "
                        //			+ edge);
                        CoreFunctionMode mode = addMode(attribute.getName(),
                                                CFDFAttributeType.BooleanMode);
                        addMode(
                                mode.getTrueNextMode().getName(),
                                mode.getTrueNextMode()
                               );
                        addMode(
                                mode.getFalseNextMode().getName(),
                                mode.getFalseNextMode()
                               );
                    }
                }
            }
        }
    }

    public void setCount(int val) {
        _count = val;
    }

    public int getCount() {
        LinkedList attributes = _graph.getAttributes(_node);

        if (attributes.size() == 0) {
            return 1;
        }

        for (int i = 0; i < attributes.size(); i++) {
            DIFAttribute attribute = (DIFAttribute) attributes.get(i);
            if (attribute.getValue() instanceof Integer) {
                if (attribute.getName().equals("count")) {
                    return (Integer) attribute.getValue();
                }
            }
        }
        return 1;
    }

    public Object getAttribute(String str) {
        //after the node has been
        if (_graph == null) {
            System.out.println(
                    "WARNING: no graph exists for node " + toString() +
                            ".  No tokens produced.");
        }
        if (_node == null) {
            System.out.println(
                    "WARNING: no node exists for node " + toString() +
                            ".  No tokens produced.");
        }

        LinkedList attributes = _graph.getAttributes(_node);

        if (attributes.size() == 0) {
            throw new RuntimeException(
                    "No actor attributes of node " + getName());
        }

	/*	System.out.println("Attributes for " + toString() + " are: \n");
	for (int i=1; i<attributes.size(); i++) {
	    DIFAttribute attribute = (DIFAttribute)attributes.get(i);
	    System.out.println(attribute.getName() + "\n");
	    }*/

        for (int i = 1; i < attributes.size(); i++) {
            DIFAttribute attribute = (DIFAttribute) attributes.get(i);

            //FIXME: Nimish thinks this will return only String type actor
            // attributes, which is just a special
            //and restricted case. So the outer if condition was removed.
            //	    if (attribute.getValue() instanceof String) {
            if (attribute.getName().equals(str)) {
                if (str == "outputedges") {
                    if (attribute.getValue() instanceof LinkedList) {
                        // LinkedList l = (LinkedList)attribute.getValue();
                        System.out.println("outputedges for " + toString() +
                                               " is an instance of LinkedList");
                    }
                }
                return attribute.getValue();
            }
            //	    }
        }
        System.out.println(
                "WARNING: " + getName() + " - No attribute found (" + str +
                        ").");
        return null;
    }

    //Nimish

    /**
     * This a special method added which returns list of edges.
     * This method checks if the list of attribute values contains instances
     * of Port or Edge.
     * If it contains any element that is an instance of Port, that
     * particular element
     * is replaced by corresponding Edge connected to that port.
     * The method returns the resultant LinkedList in which all the elements
     * are instances of Edge.
     *
     * @param str DIFAttribute Name.
     * @return LinkedList in which all the elements are instances of Edge.
     */
    protected LinkedList getAttributeEdges(String str) {
        LinkedList list = (LinkedList) getAttribute(str);
        replacePorts(list);
        //System.out.println("# of edges with attribute name " + str + " for " +
        // toString() + " = " + list.size() + "\n");
        return list;
    }

    //Wu

    /**
     * This method is dedicated to parameterization.
     * This method retrieves the parameter value by the parameter name.
     *
     * @param param_name Parameter Name.
     * @return Object in which the corresponding parameter value.
     */
    public Object getParameter(String param_name) {
        return _parameters.get(param_name);
    }

    //Wu

    /**
     * This method is dedicated to parameterization.
     * This method records the parameter value with the parameter name.
     *
     * @param param_name  Parameter Name.
     * @param param_value parameter value.
     */
    public void setParameter(String param_name, Object param_value) {
    }


    ///////////////////////////////////////////////////////////////////
    ////                         private vaiables                   ////

    /**
     * State of the actor. True if enabled, false otherwise.
     */
    private boolean _state;

    /**
     * Mode of the actor in which it can be fired, if enabled.
     */
    private LinkedHashMap _modes;

    /**
     * Inputs to the actor
     */
    private LinkedHashMap _inputs;

    /**
     * Outputs to the actor
     */
    private LinkedHashMap _outputs;

    /**
     * Parameters to the actor
     */
    protected LinkedHashMap _parameters;

    /**
     * The universal mode of nothing happens.
     */
    protected final CoreFunctionMode _nullMode;

    /**
     * The universal mode to indicate the first mode of the actor
     * should be returned by invoke.
     */
    protected final CoreFunctionMode _initMode;

    /**
     * Graph in which this node exists.
     * <p>
     * This is a bit ackward, but nodes need to know what edges are
     * connected to them, and only <code>Graph</code> knows this.  The
     * other option is that nodes can hold lists of their own edges
     * and CFDF graph could have methods to refresh them as things
     * change in the graph (similar to the way <code>register</code>
     * works in {@link Graph}.  I feel like doing that will
     * create coherence issues between edge lists more error prone
     * than just letting <code>NodeWeights</code> have
     * <code>Graph</code> visibility, but then again, the perfomance
     * might be quite bad.  Either way the basic API to derived actors
     * should not change.  Should be private eventually.
     */
    protected CFDFGraph _graph = null;

    /**
     * Node associate with this NodeWeight.
     * <p>
     * Need this to prevent constant searching through the graph to
     * find the node needed to lookup edges in the graph.  As with
     * <code>_graph</code>, this may break some barriers intentionally
     * setup.
     */
    protected Node _node = null;

    private CFDFAttributeType _nodeType;


    /**
     * Custom Iteration Count
     */
    private Integer _count = 1;

    /**
     * Visited for performing certain graph algorithms.
     */
    private boolean _visited = false;
    private CoreFunctionMode _visitedMode = null;

    private Fraction _probability;

}


