/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/
package dif.lang;

import com.google.common.collect.ImmutableMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DIFConcreteLisenter extends DIFBaseListener {
    private Hierarchy.Builder hierBuilder = null;
    private Graph.Builder graphBuilder = null;
    private String attributeName = null;
    private String actorTypeName = null;
    private String actorName = null;
    private Vertex.Builder vb = null;
    private String graphType = null;
    private Map<String, ActorType> actorTypes = null;
    private Map<String, String> actorTypeMap = null;
    private List<Vertex.Builder> vertexList = new ArrayList<>();
    private List<Edge.Builder> edgeList = new ArrayList<>();
    private List<Port.Builder> portList = new ArrayList<>();
    private List<Refinement.Builder> refineList = new ArrayList<>();
    private Map<String, String> dataflowTypeMap = null;
    private Map<String, String> nodeWeightMap = null;
    private Map<String, String> edgeWeightMap = null;
    private Refinement.Builder refineBuilder = null;

    public DIFConcreteLisenter() {
        this.hierBuilder = Hierarchy.newBuilder();
        this.actorTypes = new HashMap<>();
        this.actorTypeMap = new HashMap<>();

        dataflowTypeMap = new ImmutableMap.Builder<String, String>()
                .put("sdf", "SDFGraph")
                .put("wsdf", "WSDFGraph")
                .put("psdf", "PSDFGraph")
                .put("pafg", "PAFGGraph")
                .build();

        nodeWeightMap = new ImmutableMap.Builder<String, String>()
                .put("sdf", "SDFNodeWeight")
                .put("wsdf", "WSDFNodeWeight")
                .put("psdf", "PSDFNodeWeight")
                .put("pafg", "PAFGNodeWeight")
                .build();

        edgeWeightMap = new ImmutableMap.Builder<String, String>()
                .put("sdf", "SDFEdgeWeight")
                .put("wsdf", "WSDFEdgeWeight")
                .build();
    }

    @Override
    public void enterActor_body(DIFParser.Actor_bodyContext ctx) {
        super.enterActor_body(ctx);
        actorName = ctx.start.getText();
    }

    @Override
    public void exitActor_item(DIFParser.Actor_itemContext ctx) {
        super.exitActor_item(ctx);
        String keyword = ctx.start.getText();
        vb = vertexList.get(graphBuilder.getNodeName2IdOrThrow(actorName));
        if ("type".equals(keyword)) {
            String type = ctx.Identifier(0).getText();
            actorTypeMap.put(actorName, type);
            vb.addAttributes(Attribute.newBuilder().setType(DataType.STRING).setName("type").setValue(type));
        } else if ("interface".equals(keyword)) {
            for (int i = 0; i < ctx.Identifier().size(); i += 2) {
                String id0 = ctx.Identifier(i).getText();
                String id1 = ctx.Identifier(i + 1).getText();

                if (graphBuilder.getEdgeName2IdMap().containsKey(id0)) {
                    vb.addAttributes(Attribute.newBuilder().setValue(id0).setType(DataType.EDGE).setName(id1));
                    Map<String, Integer> consSet = actorTypes.get(actorTypeMap.get(actorName)).consumption;
                    if (consSet.containsKey(id1)) {
                        edgeList.get(graphBuilder.getEdgeName2IdOrThrow(id0)).setProduction(consSet.get(id1));
                    }
                } else if (graphBuilder.getEdgeName2IdMap().containsKey(id1)) {
                    vb.addAttributes(Attribute.newBuilder().setValue(id1).setType(DataType.EDGE).setName(id0));
                    Map<String, Integer> prodSet = actorTypes.get(actorTypeMap.get(actorName)).production;
                    if (prodSet.containsKey(id0)) {
                        edgeList.get(graphBuilder.getEdgeName2IdOrThrow(id1)).setConsumption(prodSet.get(id0));
                    }
                } else {
                    System.err.println("Invalid identifer  on line " + ctx.start.getLine());
                }
            }
        } else if ("param".equals(keyword)) {
            return;
        } else {
            System.err.println("Unrecognized keyword " + keyword + " at line " + ctx.start.getLine());
        }
    }

    @Override
    public void exitDelay_item(DIFParser.Delay_itemContext ctx) {
        super.exitDelay_item(ctx);
        String eid = ctx.Identifier().getText();
        edgeList.get(graphBuilder.getEdgeName2IdOrThrow(eid)).setDelay(Integer.valueOf(ctx.IntegerLiteral().getText()));
    }

    @Override
    public void exitActor_param_list(DIFParser.Actor_param_listContext ctx) {
        super.exitActor_param_list(ctx);

        if (ctx.children.size() > 0) {
            Attribute.Builder ab = Attribute.newBuilder();
            ab.setName(ctx.Identifier().getText());

            if (ctx.elementValueArray() != null) {
                ab.setType(DataType.LIST);
                ab.setValue(arrayContext2String(ctx.elementValueArray()));
                vb.addAttributes(ab);
            } else if (ctx.literal() != null) {
                DataType type = getLiteralType(ctx.literal());
                ab.setType(type);
                String value = ctx.literal().getText();
                if (type == DataType.STRING) {
                    value = value.substring(1, value.length() - 1);
                }
                ab.setValue(value);
                vb.addAttributes(ab);
            } else {
                System.err.println("parameter parsing error at line " + ctx.start.getLine());
            }
        }
    }

    public Hierarchy getHierarchy() {
        return hierBuilder.build();
    }

    @Override
    public void enterGraph(DIFParser.GraphContext ctx) {
        super.enterGraph(ctx);
        graphBuilder = Graph.newBuilder();
        graphType = ctx.start.getText();
        String formalType = dataflowTypeMap.get(graphType);
        if (formalType != null) {
            graphBuilder.setType(formalType);
        } else {
            System.err.println("Unsupported dataflow type " + graphType + " at line " + ctx.start.getLine());
        }
    }

    @Override
    public void exitGraph(DIFParser.GraphContext ctx) {
        super.exitGraph(ctx);
        graphBuilder.setName(ctx.children.get(1).getText());
        for (Vertex.Builder vb : vertexList) {
            vb.build();
        }
        graphBuilder.addAllVertices(vertexList.stream().map(Vertex.Builder::build).collect(Collectors.toList()));
        graphBuilder.addAllEdges(edgeList.stream().map(Edge.Builder::build).collect(Collectors.toList()));
        graphBuilder.addAllPorts(portList.stream().map(Port.Builder::build).collect(Collectors.toList()));
        graphBuilder.addAllRefinements(refineList.stream().map(Refinement.Builder::build).collect(Collectors.toList()));
        Hierarchy.Builder builder = hierBuilder.addGraphs(graphBuilder);

        vertexList = new ArrayList<>();
        edgeList = new ArrayList<>();
        portList = new ArrayList<>();
        refineList = new ArrayList<>();
    }

    @Override
    public void exitTopology_item(DIFParser.Topology_itemContext ctx) {
        super.exitTopology_item(ctx);
        if ("nodes".equals(ctx.start.getText())) {
            ctx.Identifier().forEach(id -> {
                Vertex.Builder vb = Vertex.newBuilder();
                vb.setName(id.toString());
                vb.setNodeComputation(vb.getName());
                vb.setNodeWeightType(nodeWeightMap.get(graphType));
                graphBuilder.putNodeName2Id(vb.getName(), vertexList.size());
                vertexList.add(vb);
            });
        } else if ("edges".equals(ctx.start.getText())) {
            for (int i = 0; i < ctx.Identifier().size(); i += 3) {
                Edge.Builder eb = Edge.newBuilder();
                eb.setName(ctx.Identifier(i).toString());
                eb.setSrc(ctx.Identifier(i + 1).toString());
                eb.setSnk(ctx.Identifier(i + 2).toString());
                eb.setEdgeWeightType(edgeWeightMap.get(graphType));
                graphBuilder.putEdgeName2Id(eb.getName(), edgeList.size());
                edgeList.add(eb);
            }
        }
    }

    @Override
    public void enterAttribute_body(DIFParser.Attribute_bodyContext ctx) {
        super.enterAttribute_body(ctx);
        attributeName = ctx.start.getText();
    }

    @Override
    public void exitAttribute_item(DIFParser.Attribute_itemContext ctx) {
        super.exitAttribute_item(ctx);
        String id = ctx.start.getText();
        Attribute.Builder ab = Attribute.newBuilder();
        ab.setName(attributeName);

        if (ctx.literal() != null) {
            DataType type = getLiteralType(ctx.literal());
            String value = ctx.literal().getText();
            ab.setType(type);
            if (type == DataType.STRING) {
                value = value.substring(1, value.length() - 1);
            }
            ab.setValue(value);

            if (graphBuilder.containsEdgeName2Id(id)) {
                edgeList.get(graphBuilder.getEdgeName2IdOrThrow(id)).addAttributes(ab);
            } else if (graphBuilder.containsNodeName2Id(id)) {
                vertexList.get(graphBuilder.getNodeName2IdOrThrow(id)).addAttributes(ab);
            } else {
                System.err.println("Invalid Identifer " + id + " at line " + ctx.start.getLine());
            }
        } else if (ctx.elementValueArray() != null) {
            ab.setType(DataType.LIST);
            ab.setValue(arrayContext2String(ctx.elementValueArray()));
            if (graphBuilder.containsEdgeName2Id(id)) {
                edgeList.get(graphBuilder.getEdgeName2IdOrThrow(id)).addAttributes(ab);
            } else if (graphBuilder.containsNodeName2Id(id)) {
                vertexList.get(graphBuilder.getNodeName2IdOrThrow(id)).addAttributes(ab);
            } else {
                System.err.println("Invalid Identifer " + id + " at line " + ctx.start.getLine());
            }
        } else {
            System.err.println("Unrecognized data type at line " + ctx.start.getLine());
        }

    }

    @Override
    public void exitParameter_item(DIFParser.Parameter_itemContext ctx) {
        super.exitParameter_item(ctx);
        Attribute.Builder ab = Attribute.newBuilder();
        ab.setName(ctx.Identifier().getText());
        if (ctx.literal() != null) {
            DataType type = getLiteralType(ctx.literal());
            String value = ctx.literal().getText();
            ab.setType(type);
            if (type == DataType.STRING) {
                value = value.substring(1, value.length() - 1);
            }
            ab.setValue(value);
        } else if (ctx.elementValueArray() != null) {
            ab.setType(DataType.LIST);
            ab.setValue(arrayContext2String(ctx.elementValueArray()));
        } else {
            System.err.println("Unrecognized data type at line " + ctx.start.getLine());
            return;
        }
        graphBuilder.addAttributes(ab);
    }

    @Override
    public void enterActortype_body(DIFParser.Actortype_bodyContext ctx) {
        super.enterActortype_body(ctx);
        String name = ctx.start.getText();
        actorTypeName = name;
        actorTypes.put(name, new ActorType(name));
    }

    @Override
    public void exitActortype_defn(DIFParser.Actortype_defnContext ctx) {
        super.exitActortype_defn(ctx);
        ActorType actorType = actorTypes.get(actorTypeName);

        String keyword = ctx.start.getText();
        if ("input".equals(keyword)) {
            ctx.Identifier().forEach(id -> {
                actorType.inPorts.add(id.getText());
            });
        } else if ("output".equals(keyword)) {
            ctx.Identifier().forEach(id -> {
                actorType.outPorts.add(id.getText());
            });
        } else if ("production".equals(keyword)) {
            for (int i = 0; i < ctx.Identifier().size(); i++) {
                actorType.production.put(ctx.Identifier(i).getText(), Integer.parseInt(ctx.IntegerLiteral(i).getText()));
            }
        } else if ("consumption".equals(keyword)) {
            for (int i = 0; i < ctx.Identifier().size(); i++) {
                actorType.consumption.put(ctx.Identifier(i).getText(), Integer.parseInt(ctx.IntegerLiteral(i).getText()));
            }
        } else if ("param".equals(keyword)) {

        } else if ("mode".equals(keyword)) {

        } else {
            System.err.println("Invalid keyword " + keyword + " at line " + ctx.start.getLine());
        }
    }

    @Override
    public void exitInterface_item(DIFParser.Interface_itemContext ctx) {
        super.exitInterface_item(ctx);
        Direction direction = Direction.IN;
        switch (ctx.start.getText()) {
            case "inputs":
                direction = Direction.IN;
                break;
            case "outputs":
                direction = Direction.OUT;
                break;
            default:
                System.err.println("Invalid port type at line " + ctx.start.getLine());
                break;
        }

        for (int i = 2; i + 2 < ctx.children.size(); i += 4) {
            Port.Builder pb = Port.newBuilder();
            pb.setDirection(direction);
            String id = ctx.children.get(i).getText();
            pb.setId(id);
            String nodeId = ctx.children.get(i + 2).getText();
            pb.setNodeId(nodeId);
            portList.add(pb);
        }
    }

    @Override
    public void enterRefinement_body(DIFParser.Refinement_bodyContext ctx) {
        super.enterRefinement_body(ctx);
        refineBuilder = Refinement.newBuilder();
    }

    @Override
    public void exitRefinement_body(DIFParser.Refinement_bodyContext ctx) {
        super.exitRefinement_body(ctx);
        refineBuilder.setSubGraphName(ctx.children.get(1).getText());
        refineBuilder.setNodeId(ctx.children.get(3).getText());
        refineList.add(refineBuilder);

    }

    @Override
    public void exitRefinement_item(DIFParser.Refinement_itemContext ctx) {
        super.exitRefinement_item(ctx);
        String subID = ctx.children.get(0).getText();
        String topID = ctx.children.get(2).getText();

        boolean isPort = false;
        boolean isEdge = false;
        for (Port.Builder pb : portList) {
            if (pb.getId().equals(topID)) {
                isPort = true;
                break;
            }
        }

        for (Edge.Builder eb : edgeList) {
            if (eb.getName().equals(topID)) {
                isEdge = true;
                break;
            }
        }

        if (isPort) {
            refineBuilder.addPorts(PortMap.newBuilder().setTopPortID(topID).setSubPortID(subID).build());
        } else if (isEdge) {
            refineBuilder.addEdges(EdgeMap.newBuilder().setTopEdgeID(topID).setSubEdgeID(subID).build());
        } else {
            refineBuilder.addParams(ParamMap.newBuilder().setTopParamID(topID).setSubParamID(subID).build());
        }

    }

    private DataType getLiteralType(DIFParser.LiteralContext l) {
        if (l.BooleanLiteral() != null) {
            return DataType.BOOLEAN;
        } else if (l.CharacterLiteral() != null) {
            return DataType.CHAR;
        } else if (l.FloatingPointLiteral() != null) {
            return DataType.FLOAT;
        } else if (l.IntegerLiteral() != null) {
            return DataType.INTEGER;
        } else if (l.StringLiteral() != null) {
            return DataType.STRING;
        } else {
            return DataType.NULL;
        }
    }

    private class ActorType {
        String name;
        List<String> inPorts, outPorts;
        Map<String, Integer> production;
        Map<String, Integer> consumption;

        ActorType(String name) {
            this();
            this.name = name;
        }

        ActorType() {
            inPorts = new ArrayList<>();
            outPorts = new ArrayList<>();
            production = new HashMap<>();
            consumption = new HashMap<>();
        }
    }


    private String arrayContext2String(DIFParser.ElementValueArrayContext ctx) {
        StringBuilder sb = new StringBuilder();
        ctx.elementValueList().literal().forEach(l -> {
            String str = l.getText();
            if (str.startsWith("\"") && str.endsWith("\"")) {
                str = str.substring(1, str.length() - 1);
            }
            if (sb.length() == 0) {
                sb.append(str);
            } else {
                sb.append(',').append(str);
            }
        });
        return sb.toString();
    }
}
