/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif.lang;

import com.google.common.collect.ImmutableMap;

import org.apache.tools.ant.taskdefs.XSLTProcess;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

public class DIFMLWriter {


    public static void saveXML(Hierarchy hier, String outputFilename) {
        Map<DataType, String> typeMap = new ImmutableMap.Builder<DataType, String>()
                .put(DataType.INTEGER, "Integer")
                .put(DataType.BOOLEAN, "Boolean")
                .put(DataType.CHAR, "Char")
                .put(DataType.LIST, "List")
                .put(DataType.EDGE, "Edge")
                .put(DataType.FLOAT, "Float")
                .put(DataType.STRING, "String")
                .build();

        Document dom;
        Element e = null;

        // instance of a DocumentBuilderFactory
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            dom = db.newDocument();

            Element root = dom.createElement("difml");
            for (Graph graph : hier.getGraphsList()) {
                Element graphElement = dom.createElement("graph");
                root.appendChild(graphElement);
                Element imp = dom.createElement("implicitAttributes");
                Element tpl = dom.createElement("topology");
                Element pra = dom.createElement("parameters");
                Element itf = dom.createElement("interface");

                // process implicit attributes
                Element impName = dom.createElement("name");
                Element impType = dom.createElement("type");
                impName.setAttribute("val", graph.getName());
                impType.setAttribute("val", graph.getType());
                imp.appendChild(impName);
                imp.appendChild(impType);

                // process topology
                Element nodes = dom.createElement("nodes");
                for (Vertex vert : graph.getVerticesList()) {
                    Element vertElement = dom.createElement("node");
                    Element vertImp = dom.createElement("implicitAttributes");
                    Element vertImpId = dom.createElement("id");
                    vertImpId.setAttribute("val", vert.getName());
                    vertImp.appendChild(vertImpId);
                    vertElement.appendChild(vertImp);

                    Element vertBlt = dom.createElement("builtInAttributes");
                    Element vertWgt = dom.createElement("nodeWeight");
                    vertWgt.setAttribute("type", vert.getNodeWeightType());
                    vertWgt.setAttribute("computation", vert.getNodeComputation());
                    vertBlt.appendChild(vertWgt);
                    vertElement.appendChild(vertBlt);

                    Element vertUsr = dom.createElement("userDefinedAttributes");
                    for (Attribute attr : vert.getAttributesList()) {
                        Element attrEle = dom.createElement("attribute");
                        attrEle.setAttribute("name", attr.getName());
                        attrEle.setAttribute("type", typeMap.get(attr.getType()));
                        attrEle.setAttribute("val", attr.getValue());
                        vertUsr.appendChild(attrEle);
                    }
                    vertElement.appendChild(vertUsr);

                    nodes.appendChild(vertElement);
                }
                tpl.appendChild(nodes);
                Element edges = dom.createElement("edges");
                for (Edge edge : graph.getEdgesList()) {
                    Element edgeElement = dom.createElement("edge");
                    Element edgeimp = dom.createElement("implicitAttributes");
                    Element edgeimpId = dom.createElement("id");
                    Element edgeimpSrc = dom.createElement("sourceId");
                    Element edgeimpSnk = dom.createElement("sinkId");
                    edgeimpId.setAttribute("val", edge.getName());
                    edgeimpSrc.setAttribute("val", edge.getSrc());
                    edgeimpSnk.setAttribute("val", edge.getSnk());
                    edgeimp.appendChild(edgeimpId);
                    edgeimp.appendChild(edgeimpSrc);
                    edgeimp.appendChild(edgeimpSnk);
                    edgeElement.appendChild(edgeimp);
                    Element edgeblt = dom.createElement("builtInAttributes");
                    Element edgeWgt = dom.createElement("edgeWeight");
                    edgeWgt.setAttribute("comsumption",String.valueOf(edge.getConsumption()));
                    edgeWgt.setAttribute("delay",String.valueOf(edge.getDelay()));
                    edgeWgt.setAttribute("production",String.valueOf(edge.getProduction()));
                    edgeWgt.setAttribute("type",edge.getEdgeWeightType());
                    edgeblt.appendChild(edgeWgt);
                    edgeElement.appendChild(edgeblt);
                    Element edgeusr = dom.createElement("userDefinedAttributes");
                    for (Attribute attr : edge.getAttributesList()) {
                        Element attrEle = dom.createElement("attribute");
                        attrEle.setAttribute("name", attr.getName());
                        attrEle.setAttribute("type", typeMap.get(attr.getType()));
                        attrEle.setAttribute("val", attr.getValue());
                        edgeusr.appendChild(attrEle);
                    }
                    edgeElement.appendChild(edgeusr);
                    edges.appendChild(edgeElement);
                }
                tpl.appendChild(edges);

                for (Attribute attr : graph.getAttributesList()) {
                    Element attrEle = dom.createElement("parameter");
                    attrEle.setAttribute("id", attr.getName());
                    Element value = dom.createElement("value");
                    value.setAttribute("type", typeMap.get(attr.getType()));
                    value.setAttribute("value", attr.getValue());
                    attrEle.appendChild(value);
                    pra.appendChild(attrEle);
                }


                // Process interface / ports list
                for (Port port : graph.getPortsList()) {
                    Element pdom = dom.createElement("port");
                    Element pidom = dom.createElement("implicitAttributes");
                    pdom.appendChild(pidom);
                    itf.appendChild(pdom);

                    Element ddom = dom.createElement("direction");
                    pidom.appendChild(ddom);

                    ddom.setAttribute("id", port.getId());
                    ddom.setAttribute("nodeId", port.getNodeId());
                    if (port.getDirection()== Direction.IN) {
                        ddom.setAttribute("val", "IN");
                    } else  {
                        ddom.setAttribute("val", "OUT");
                    }
                }

                // add element to graph
                graphElement.appendChild(imp);
                graphElement.appendChild(tpl);
                if (graph.getAttributesCount() > 0) {
                    graphElement.appendChild(pra);
                }
                if (graph.getPortsCount() > 0) {
                    graphElement.appendChild(itf);
                }




                // Process refinement list
                for (Refinement refinement : graph.getRefinementsList()) {
                    Element refEle = dom.createElement("refinement");
                    graphElement.appendChild(refEle);

                    Element subGraph = dom.createElement("subGraph");
                    subGraph.setAttribute("name", refinement.getSubGraphName());
                    subGraph.setAttribute("nodeId", refinement.getNodeId());
                    refEle.appendChild(subGraph);
                    for (PortMap portMap : refinement.getPortsList()) {
                        Element pom =  dom.createElement("subPort");
                        pom.setAttribute("subPortID", portMap.getSubPortID());
                        pom.setAttribute("topPortID", portMap.getTopPortID());
                        refEle.appendChild(pom);
                    }

                    for (EdgeMap edgeMap : refinement.getEdgesList()) {
                        Element pom =  dom.createElement("subEdge");
                        pom.setAttribute("subEdgeID", edgeMap.getSubEdgeID());
                        pom.setAttribute("topEdgeID", edgeMap.getTopEdgeID());
                        refEle.appendChild(pom);
                    }

                    for (ParamMap paramMap : refinement.getParamsList()) {
                        Element pam = dom.createElement("subParam");
                        pam.setAttribute("subParamID", paramMap.getSubParamID());
                        pam.setAttribute("topParamID", paramMap.getTopParamID());
                        refEle.appendChild(pam);
                    }

                }

            }

            dom.appendChild(root);
            root.setAttribute("xmlns", "http://www.ece.umd.edu/DIFML");

            try {
                Transformer tr = TransformerFactory.newInstance().newTransformer();
                tr.setOutputProperty(OutputKeys.INDENT, "yes");
                tr.setOutputProperty(OutputKeys.METHOD, "xml");
                tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

                // send DOM to file
                tr.transform(new DOMSource(dom), new StreamResult(new FileOutputStream(outputFilename)));

            } catch (TransformerException | IOException te) {
                System.out.println(te.getMessage());
            }
        } catch (ParserConfigurationException e1) {
            e1.printStackTrace();
        }
    }
}
