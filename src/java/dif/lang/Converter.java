/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/
package dif.lang;

import org.anarres.cpp.CppReader;
import org.anarres.cpp.Feature;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;

/**
 * @author Jiahao Wu
 * @date 2018-01-15
 */
public class Converter {
    public static void main(String[] args) {
        if (args.length != 2) {
            System.err.println("Usage: \n" +
                    "\tjava -cp difpub-1.2.1-SNAPSHOT-all.jar:. dif.lang.Reader <input_dif_filename.dif> <output_difml_filename.difml>");
            System.exit(1);
        }
        toDIFML(args[0], args[1]);
    }

    public static void toDIFML(String input, String output) {
        try {
            FileReader in = new FileReader(input);
            toDIFMLCore(in, output);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void toDIFMLCore(FileReader in, String output) {
        CppReader cpp = new CppReader(in);
        cpp.getPreprocessor().addFeatures(Feature.CSYNTAX);
        cpp.getPreprocessor().setSystemIncludePath(
                Collections.singletonList(".")
        );
        BufferedReader b = new BufferedReader(cpp);

        CharStream stream = null;
        try {
            stream = CharStreams.fromReader(b);
        } catch (IOException e) {
            e.printStackTrace();
        }
        DIFLexer lexer = new DIFLexer(stream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        DIFParser parser = new DIFParser(tokens);
        DIFConcreteLisenter lisenter = new DIFConcreteLisenter();
        parser.addParseListener(lisenter);
        parser.graph_list().enterRule(lisenter);

        Hierarchy hier = lisenter.getHierarchy();
        DIFMLWriter.saveXML(hier, output);
    }

    public static void toDIFML(File inputFile, String output) {
        try {
            FileReader in = new FileReader(inputFile);
            toDIFMLCore(in,  inputFile.getParent() + "/" + output);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
