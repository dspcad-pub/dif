/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.graph;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import dif.util.graph.DirectedGraph;
import dif.util.graph.Graph;
import dif.util.graph.Node;

//////////////////////////////////////////////////////////////////////////
//// DFSTopSortStrategy
/**
A depth-first-search approach for finding a topological sorting order. This
class adopts a simple approach to pick the first available source node.

@see TopSortAnalysis
@author Ming-Yung Ko
@version $Id: DFSTopSortStrategy.java 606 2012-10-08 16:29:47Z plishker $
*/
public class DFSTopSortStrategy extends BaseTopSortStrategy {

    /** Construct a top sorting strategy for a given graph.
     *  @param graph The given graph.
     */
    public DFSTopSortStrategy(Graph graph) {
        super(graph);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /** Compute a topological sorting order. The result is
     *  returned as a <code>List</code>, where the order begins at index 0.
     *
     *  @return A topological sorting order.
     */
    final protected Object _compute() {
        _validate();
        _setup();
        return _DFSTopSort(directedGraph());
    }

    /** Return the next source node in determining the top sort.
     *  This method is supposed to be overriden by specific source
     *  selection approach.
     *
     *  @param graph The graph to get the next source node.
     *  @return The next source node.
     */
    protected Node _nextSource(DirectedGraph graph) {
        return (Node)graph.sourceNodes().iterator().next();
    }

    /** A preliminary step in computing topological sorting. This is a step
     *  executed in {@link #_compute()}. The default action here basically does
     *  nothing and users can define desired actions.
     */
    protected void _setup() {
        return;
    }

    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////

    /*  A depth-first search approach in finding a topological sorting order.
     *  This method is a recursive invokation because of the nature of depth
     *  first search. Any preliminary actions before the recursion can be
     *  gathered in {@link #_setup()}.
     *
     *  @param graph The target graph.
     *  @return A topological sorting order in <code>List</code>.
     */
    private List _DFSTopSort(DirectedGraph graph) {
        List sort = null;
        // Stop condition: Graph with 1 node only.
        if (graph.nodeCount() == 1) {
            sort = new LinkedList();
            sort.add(graph.node(0));
        } else {
            Node source = _nextSource(graph);
            Collection subNodes = new LinkedList(graph.nodes());
            subNodes.remove(source);
            DirectedGraph subGraph = (DirectedGraph)
                    graph.subgraph(subNodes).cloneAs(new DirectedGraph());
            sort = _DFSTopSort(subGraph);
            sort.add(0, source);
        }
        return sort;
    }
}


