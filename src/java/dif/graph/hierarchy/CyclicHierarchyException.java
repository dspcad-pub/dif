/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.graph.hierarchy;

import dif.util.graph.Edge;
import dif.util.graph.Node;

//////////////////////////////////////////////////////////////////////////
//// CyclicHierarchyException
/**
This exception is thrown when a cycle in the hierarchy of graphs is detected.
@author Fuat Keceli
@version $Id: CyclicHierarchyException.java 606 2012-10-08 16:29:47Z plishker $
@see Hierarchy
*/

public class CyclicHierarchyException extends HierarchyException {

    /** Constructs a Cyclic Hierarchy Exception detected upon encountering
     *  a hierarchy relation (defined in the parameter) between two
     *  Hierarchy objects.
     *  @param violation An edge in the hierarchy relation graph which violates
     *  the DAG property. The weights of the nodes connected to the edge should
     *  be graphs. The direction of the edge should be from the parent to
     *  the child.
     */
    public CyclicHierarchyException(Edge violation) {
        super("Violating hierarchy relation is: " +
                ((Hierarchy)violation.source().getWeight())
                .getName() + " -> "+
                ((Hierarchy)violation.sink().getWeight())
                .getName());
        _violation = violation;
    }

    ///////////////////////////////////////////////////////////////////
    ////                          public methods                   ////

    /** Returns a Cyclic Hierarchy Exception if a hierarchy relation cycle
     *  occurs when the child is added to parent
     *  via {@link Hierarchy#addSuperNode}.
     *  @param parent Parent hierarchy that will include the child in a super
     *  node.
     *  @param child The sub-hierarchy.
     *  @return A CyclicHierarchyException object that can be thrown if a cycle
     *  is detected. Null otherwise.
     */
    public static CyclicHierarchyException checkCycle(Hierarchy parent,
            Hierarchy child) {
        Hierarchy grandParent = parent.getParent();
        boolean throwException = (parent == child) ? true : false;
        while(grandParent != null) {
            if(grandParent == child) {
                throwException = true;
            }
            grandParent = grandParent.getParent();
        }
        if(throwException) {
            Edge edge = new Edge(new Node(parent), new Node(child));
            return new CyclicHierarchyException(edge);
        } else {
            return null;
        }
    }

    /** Returns an edge which is detected to cause a cycle during the
     *  traversal of the graph hierarchy. The weights of the nodes connected to
     *  the edge are graphs. The direction of the edge is from the parent to
     *  the child.
     *  @return A violating edge in the DAG representation of graph hierarchy.
     */
    public Edge getViolatingEdge() {
        return _violation;
    }

    ///////////////////////////////////////////////////////////////////
    ////           private variables                               ////

    private Edge _violation;
}


