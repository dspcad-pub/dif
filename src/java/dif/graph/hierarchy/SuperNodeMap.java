/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.graph.hierarchy;

import dif.util.graph.Node;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;

//////////////////////////////////////////////////////////////////////////
//// SuperNodeMap
/** List of super nodes in a hierarchy. It simulates a doubly
linked map so users can both give the super node and get the hierarchy and vice
versa.<p>
Hierarchy names should not be duplicated therefore it provides methods
to check if a hierarchy name is previously defined in the list.<p>
Entries are stored in the order they are added, {@link #getNodes}
and {@link #iterator} methods maintain this order.
@author Fuat Keceli
@version $Id: SuperNodeMap.java 606 2012-10-08 16:29:47Z plishker $
*/
public class SuperNodeMap {

    ///////////////////////////////////////////////////////////////////
    ////                         constructors                      ////

    /** Construct a super node map.
     */
    protected SuperNodeMap() {
        _nodeList = new Vector();
        _hierarchyMap = new HashMap();
        _nameIndex = 0;
    }

    ///////////////////////////////////////////////////////////////////
    ////                        public methods                     ////

    /** Returns true is a given hierarchy is defined as a sub-hierarchy.
     *  @return True is a given hierarchy is defined as a sub-hierarchy. False
     *  otherwise.
     */
    public boolean contains(Hierarchy hierarchy) {
        return _hierarchyMap.values().contains(hierarchy);
    }

    /** Returns true is a given node is defined as a super node.
     *  @return True is a given node is defined as a super node. False
     *  otherwise.
     */
    public boolean contains(Node node) {
        return _nodeList.contains(node);
    }

    /** Returns the super node that contains the given sub-hierarchy.
     *  @param hierarchy A sub-hierarchy.
     *  @return A graph node. Null if given hierarchy is not a child of this
     *  hierarchy.
     */
    public Node get(Hierarchy hierarchy) {
        for(Iterator nodes = _nodeList.iterator(); nodes.hasNext(); ) {
            Node superNode = (Node)nodes.next();
            if(get(superNode) == hierarchy) {
                return superNode;
            }
        }
        return null;
    }

    /** Returns the sub-hierarchy that is contained in a super node.
     *  @param superNode A super node in the graph.
     *  @return A hierarchy object. Null if the node is not a super node.
     */
    public Hierarchy get(Node superNode) {
        return (Hierarchy)_hierarchyMap.get(superNode);
    }

    /** Returns the sub-hierarchy that has the given name.
     *  @param name Name that is being searched.
     *  @return A hierarchy object. Null if not found.
     */
    public Hierarchy get(String name) {
        for(Iterator hierarchies = _hierarchyMap.values().iterator();
            hierarchies.hasNext(); ) {
            Hierarchy hierarchy = (Hierarchy)hierarchies.next();
            if(hierarchy.getName().equals(name)) {
                return hierarchy;
            }
        }
        return null;
    }

    /** Returns a collection of nodes that are defined as super nodes.
     *  Order of the nodes returned is the order they are added as super nodes.
     *  @return A collection of nodes. Returned collection can safely be
     *  modified.
     */
    public List getNodes() {
        return new Vector(_nodeList);
    }

    /** Returns true if the given name is the name of another hierarchy.
     *  @return True if the given name is the name of another hierarchy. False
     *  otherwise.
     */
    public boolean isDefined(String name) {
        return (get(name) == null) ? false : true;
    }

    /** Returns true if the hierarchy is a sub-hierarchy of this hierarchy.
     *  @return True if the hierarchy is a sub-hierarchy of this hierarchy.
     *  False otherwise.
     */
    public boolean isSubHierarchy(Hierarchy hierarchy) {
        return (get(hierarchy) == null) ? false : true;
    }

    /** Returns true if the node is a super node of this hierarchy.
     *  @return True if the node is a super node of this hierarchy.
     *  False otherwise.
     */
    public boolean isSuperNode(Node node) {
        return (get(node) == null) ? false : true;
    }

    /** Returns an iterator over the super nodes. The order is the order they
     *  are added.
     *  @return An iterator over the super nodes.
     */
   public ListIterator iterator() {
       return new Vector(_nodeList).listIterator();
   }

    /** Returns a string that is guaranteed to be unused in this port list as a
     *  port name.
     *  @return An port name.
     */
    public String newName() {
        String name = "hierarchy" + _nameIndex;
        while(isDefined(name)) {
            _nameIndex++;
            name = "hierarchy" + _nameIndex;
        }
        _nameIndex++;
        return name;
    }

    ///////////////////////////////////////////////////////////////////
    ////           protected methods                               ////

    /** Adds a super node - hierarchy pair to the map. No error checking is
     *  performed therefore originating code should maintain the consistency.
     *  @param node Node to be added.
     *  @param hierarchy Hierarchy to be added
     */
    protected void _put(Node node, Hierarchy hierarchy) {
        _hierarchyMap.put(node, hierarchy);
        _nodeList.add(node);
    }

    /** Removes a super node - hierarchy pair from the map. No error checking
     *  is performed therefore originating code should maintain the
     *  consistency.
     *  @param node Node to be removed.
     */
    protected void _remove(Node node) {
        _hierarchyMap.remove(node);
        _nodeList.remove(node);
    }

    ///////////////////////////////////////////////////////////////////
    ////           private methods and variables                   ////

    private Vector _nodeList;
    private HashMap _hierarchyMap;
    private int _nameIndex;
}


