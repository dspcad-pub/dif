/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.graph.hierarchy;

import dif.util.graph.Graph;
import dif.util.graph.Node;
import dif.util.graph.Edge;

import java.util.Iterator;

//////////////////////////////////////////////////////////////////////////
//// HierarchyException
/**
This exception can be thrown by the methods in Hierarchy class. In addition to
the exception functionality this class provides checks for maintaining
stability and consistency in Hierarchy classes.
@author Fuat Keceli, Chia-Jui Hsu
@version $Id: HierarchyException.java 606 2012-10-08 16:29:47Z plishker $
@see Hierarchy
*/


public class HierarchyException extends RuntimeException {

    ///////////////////////////////////////////////////////////////////
    ////                         constructors                      ////

    /**
     *  @param message Error string which can be accessed using
     *  <code>getMessage()</code> method.
     */
    public HierarchyException(String message) {
        super(message);
    }

    ///////////////////////////////////////////////////////////////////
    ////                          public methods                   ////

    /** Checks if an edge can be connected to a port.<p>
     *  Returns an exception if any of the following statements is true:
     *  <UL>
     *  <LI>Super node that contains the port is not sink or source of the
     *  edge.
     *  <LI>Edge is already connected to another port on the same super node
     *  (or two ports in case of a self loop),
     *  <LI>Edge direction and port direction don't match
     * (if port is directed). If edge is a self loop edge, both ports cannot
     *  have the same direction.
     *  @param edge Edge to be connected.
     *  @param port Port to be connected.
     *  @return A HierarchyException if any of the above statements is true,
     *  null otherwise.
     */
    public static HierarchyException checkConnection(Edge edge, Port port) {
        Hierarchy hierarchy = port.getHierarchy().getParent();
        Graph graph = hierarchy.getGraph();
        Node superNode = hierarchy.getSuperNodes().get(port.getHierarchy());
        String initialString = "Cannot connect edge to port " + port + ".";
        // Check 1.
        if(edge.source() != superNode && edge.sink() != superNode) {
            return new HierarchyException(initialString
                    + " Node is not the source or sink of the"
                    + " edge. Edge weight: " + edge.getWeight()
                    + " Node weight: " + superNode.getWeight());
        }
        // Check 2.
        PortList subPorts = port.getHierarchy().getPorts();
        Port[] connections = subPorts.getConnectedPort(edge);
        if((!edge.isSelfLoop() && subPorts.isConnected(edge))
                || (edge.isSelfLoop() && connections.length > 1)) {
            return new HierarchyException(initialString
                    + " Edge is already connected to port(s) " + connections[0]
                    + ((connections.length == 2)
                            ? ( " and " + connections[1]) : "")
                    + ".");
        }
        if(port.getDirection() != 0) {
            if(!edge.isSelfLoop()) {
                if((edge.sink() != superNode && port.getDirection() < 0) ||
                        (edge.source() != superNode &&
                                port.getDirection() > 0)) {
                    return new HierarchyException(initialString
                            + " Edge doesn't match with the direction of the"
                            + " port.");
                }
            } else {
                if((connections.length == 1) &&
                        (connections[0].getDirection()
                                == port.getDirection())) {
                    return new HierarchyException(initialString
                            + " Self loop edge is already connected to another"
                            + " port with the same direction.");
                }
            }
        }
        return null;
    }

    /** Checks if two ports can be connected.<p>
     *  Two ports can be connected only if their hierarchies are directly
     *  related (i.e. one is a super-hierarchy and the other one is a
     *  sub-hierarchy). Also directions of the ports should match.
     *  @param superPort Port in the super-hierarchy.
     *  @param subPort Port in the sub-hierarchy.
     *  @return A HierarchyException if any of the above statements is true,
     *  null otherwise.
     */
    public static HierarchyException checkConnection(Port superPort,
            Port subPort) {
        Hierarchy superHierarchy = superPort.getHierarchy();
        Hierarchy subHierarchy = subPort.getHierarchy();
        if(subHierarchy.getParent() != superHierarchy) {
            return new HierarchyException("Cannot connect port "
                    + superPort + " to port " + subPort
                    + ". " + subHierarchy.getName()
                    + " is not included in "
                    + superHierarchy.getName() + ".");
        }
        // Check directions.
        if(!((superPort.getDirection() > 0) == (subPort.getDirection() > 0) &&
                   (superPort.getDirection() == 0 ) ==
                   (subPort.getDirection() == 0))) {
            return new HierarchyException("Cannot connect port "
                    + superPort + " to port " + subPort
                    + ". Directions don't match.");
        }
        return null;
    }

    /** Checks if a super node in a hierarchy can be flattened without creating
     *  any errors or inconsistencies.
     *  Returns an exception if any of the following statements is true:
     *  <UL>
     *  <LI>If the super node is not present in its graphs,
     *  <LI>If child graph cannot be added to the parent graph (nodes or
     *  edges might return exceptions while adding - see
     *  {@link Graph#addNode(Node)} and
     *  {@link Graph#addEdge(Edge)}),
     *  <LI>If there are any unconnected ports in the child hierarchy.
     *  <LI>If a connection cannot be made in the underlying graph objects
     *  because of a missing node or edge,
     *  <LI>If any of the super nodes in the sub-hierarchy cannot be added to
     *  the parent hierarchy (see {@link #checkSuperNode}),
     *  @param hierarchy Parent hierarchy.
     *  @param node Super node to be flattened.
     *  @return A HierarchyException if any of the above statements is true,
     *  null otherwise.
     */
    public static HierarchyException checkFlatten(Hierarchy hierarchy,
            Node node) {
        Hierarchy subHierarchy = hierarchy.getSuperNodes().get(node);
        PortList subPorts = subHierarchy.getPorts();
        SuperNodeMap subSuperNodes = subHierarchy.getSuperNodes();
        String initialString = "Sub-hierarchy " + subHierarchy.getName()
            + " cannot be flattened.";
        // Check 1.
        if(!hierarchy.getSuperNodes().contains(node) ||
                !hierarchy.getGraph().containsNode(node)) {
            return new HierarchyException(initialString
                    + " Not a (super) node in " + hierarchy.getName() + "."
                    + " Node weight: " + node.getWeight());
        }
        // Check 2.
        try {
            ((Graph)hierarchy.getGraph().clone())
                .addGraph(subHierarchy.getGraph());
        } catch(Exception exception) {
            return new HierarchyException(initialString
                    + " Following exception is returned while merging graphs: "
                    + exception.getMessage());
        }
        for(Iterator childPorts = subPorts.iterator();
            childPorts.hasNext(); ) {
            Port subPort = (Port)childPorts.next();
            Object connection = subPort.getConnection();
            Node subPortNode = (Node)subPort.getNode();
            if(connection == null) {
                // Check 3.
                return new HierarchyException(initialString
                        + " Connection of " + subPort + " is not defined.");
            } else if(connection instanceof Edge) {
                // Check 4.
                Edge edge = (Edge)connection;
                if(!hierarchy.getGraph().containsEdge(edge)) {
                    return new HierarchyException(initialString
                            + " Connection edge cannot be found in graph of "
                            + hierarchy.getName() + ". Edge weight: "
                            + edge.getWeight());
                }
            }
            if(subPort.getRelatedPort() == null) {
                // Check 3
                if(subPortNode == null) {
                    return new HierarchyException(initialString
                            + " Inner connection of " + subPort
                            + " is not defined.");
                }
                if(!subHierarchy.getGraph().containsNode(subPortNode)) {
                    return new HierarchyException(initialString
                            + " Connection node cannot be found in graph of "
                            + subHierarchy.getName() + ". Node weight: "
                            + subPortNode.getWeight());
                }
            }
        }
        for(Iterator childSuperNodes = subSuperNodes.iterator();
            childSuperNodes.hasNext(); ) {
            Node subSuperNode = (Node)childSuperNodes.next();
            try {
                checkSuperNode(hierarchy, subHierarchy);
            } catch(HierarchyException exception) {
                return new HierarchyException(initialString
                        + " The reason is: " + exception.getMessage());
            }
        }
        return null;
    }

    /** Checks if port is suitable for adding to a hierarchy object.<p>
     *  Returns an exception if the name of the port already exists in the
     *  hierarchy that it is being added.
     *  @param hierarchy Hierarchy to which the port will be added.
     *  @param port Port to be added to the hierarchy.
     *  @return A HierarchyException if the name of the port already exists
     *  null otherwise.
     */
    public static HierarchyException checkPort(Hierarchy hierarchy,
            Port port) {
        if(hierarchy.getPorts().isDefined(port.getName())) {
            return new HierarchyException("Name " + port.getName()
                    + " is already defined in " + hierarchy.getName() + ".");
        }
        return null;
    }

    /** Checks if a node can be defined as a super node in a hierarchy.<p>
     *  Returns an exception if any of the following statements is true:
     *  <UL>
     *  <LI>The name of the sub-hierarchy is already used in another
     *  sub-hierarchy,
     *  <LI>This connection causes a relational cycle. (In this case the
     *  exception will be of
     *  type {@link dif.graph.hierarchy.CyclicHierarchyException}.
     *  </UL>
     *  @param parent The hierarchy that contains the node.
     *  @param child Sub-hierarchy to be added.
     *  @return A HierarchyException if any of the above statements is true,
     *  null otherwise.
     */
    public static HierarchyException checkSuperNode(Hierarchy parent,
            Hierarchy child) {
        // Check 1.
        if(parent._superNodes.isDefined(child.getName())) {
            return new HierarchyException("Cannot add " + child.getName()
                    + " as a sub-hierarchy to " + parent.getName()
                    + ". The name " + child.getName() + " already exists in "
                    + parent.getName() + ".");
        }
        // Check 2.
        return CyclicHierarchyException.checkCycle(parent, child);
    }
}


