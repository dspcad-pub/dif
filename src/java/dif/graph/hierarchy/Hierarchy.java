/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.graph.hierarchy;

import dif.util.graph.DirectedAcyclicGraph;
import dif.util.graph.Edge;
import dif.util.graph.Graph;
import dif.util.graph.Node;
import dif.util.graph.analysis.strategy.MirrorTransformerStrategy;
import dif.util.KernelException;
import dif.util.InternalErrorException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.lang.reflect.Constructor;

import dif.util.Conventions;

//////////////////////////////////////////////////////////////////////////
//// Hierarchy
/**
Hierarchy class is a wrapper around a <code>dif.util.graph.Graph</code> object
for implementing hierarchical features. These features include defining ports
for connecting a graph as a subgraph and defining connections between a
parent (super) graph and a child (sub) graph.
<p>
For more information about how to use this class refer to the package
documentation.
@author Fuat Keceli, Shuvra S. Bhattacharyya, Ming-Yung Ko
@version $Id: Hierarchy.java 606 2012-10-08 16:29:47Z plishker $
@see Graph
*/

public class Hierarchy {

    ///////////////////////////////////////////////////////////////////
    ////                         constructors                      ////

    /** Construct a hierarchy object with the name "_hierarchy".
     *  @param graph The graph object that backs the hierarchy.
     */
    public Hierarchy(Graph graph) {
        this(graph, "_hierarchy");
    }

    /** Construct a hierarchy object with a name.
     *  @param graph The graph object that backs the hierarchy.
     *  @param name Name of the hierarchy.
     *  @exception HierarchyException If
     *  {@link Conventions#labelConvention} returns an error.
     */
    public Hierarchy(Graph graph, String name) {
        String error = Conventions.labelConvention(name);
        if(error != null) {
            throw new HierarchyException("Hierarchy name error. " + error);
        }
        _graph = graph;
        _hierarchyName = name;
        _ports = new PortList();
        _superNodes = new SuperNodeMap();
    }

    ///////////////////////////////////////////////////////////////////
    ////                          public methods                   ////

    /** Converts a node to a super node. If the node is already a super node or
     *  or the hierarchy has another super hierarchy, they are disconnected
     *  first.
     *  @param node Super node.
     *  @param hierarchy Sub-hierarchy to be associated with the node.
     *  @exception HierarchyException If
     *  {@link HierarchyException#checkSuperNode} returns an exception.
     *  @exception CyclicHierarchyException If a cycle is detected in the
     *  hierarchy structure.
     *  (Checked by {@link CyclicHierarchyException#checkCycle})
     */
    public void addSuperNode(Node node, Hierarchy hierarchy) {
        HierarchyException exception =
            HierarchyException.checkSuperNode(this, hierarchy);
        if(exception != null) {
            throw exception;
        }
        hierarchy.disconnect();
        disconnectSuperNode(node);
        hierarchy._parent = this;
        _superNodes._put(node, hierarchy);
    }

    /** Flattens this hierarchy all the way down to its bottom. After this
     *  operation there will be no super nodes left in the hierarchy.
     *  @return True if the hierarchy has changed as a result of this
     *  operation.
     *  @exception HierarchyException If any of the individual flatten
     *  operations throw an exception.
     *  @see #deepPurge
     */
    public boolean deepFlatten() {
        boolean returnValue = false;
        while(flatten()) {
            returnValue = true;
        }
        return returnValue;
    }

    /** Runs {@link #purge} deep into the bottom of this hierarchy. After this
     *  operation {@link #deepFlatten} can be called with no exceptions.
     *  @return A list of sub-hierarchies that are disconnected.
     */
    public List deepPurge() {
        Vector returnValue = new Vector();
        returnValue.add(purge());
        Iterator superNodes = getSuperNodes().getNodes().iterator();
        for(; superNodes.hasNext(); ) {
            Hierarchy subHierarchy =
                getSuperNodes().get((Node)superNodes.next());
            returnValue.add(subHierarchy.deepPurge());
        }
        return returnValue;
    }

    /** Disconnects this hierarchy from its super hierarchy. This method is the
     *  same as <code>parent.disconnectSuperNode(node)</code> in which node is
     *  the super node of this hierarchy. It does nothing if this hierarchy has
     *  no super hierarchy.
     */
    public void disconnect() {
        if(getParent() == null) {
            return;
        }
        Node node = getParent().getSuperNodes().get(this);
        getParent().getSuperNodes()._remove(node);
        _parent = null;
        _ports.disconnectAll();
    }

    /** Disconnects a sub-hierarchy from this hierarchy. This method is the
     *  same as <code>child.disconnect()</code>. It does nothing if the node is
     *  not a super node.
     *  @param node A super node in this hierarchy.
     */
    public void disconnectSuperNode(Node node) {
        Hierarchy subHierarchy = getSuperNodes().get(node);
        if(subHierarchy != null) {
            subHierarchy.disconnect();
        }
    }

    /** Flattens all the super nodes on this level of the hierarchy.
     *  However super nodes coming from the sub hierarchies won't be flattened.
     *  @return True if the hierarchy has changed as a result of this
     *  operation.
     *  @exception HierarchyException If any of the individual flatten
     *  operations throw an exception.
     *  @see #purge
     */
    public boolean flatten() {
        boolean returnValue = false;
        Iterator superNodes = getSuperNodes().getNodes().iterator();
        for(; superNodes.hasNext(); ) {
            Node superNode = (Node)superNodes.next();
            flatten(superNode);
            returnValue = true;
        }
        return returnValue;
    }

    /** Flattens a super node by merging its sub-graph to this graph. All
     *  connections will be made as specified in the port definitions of the
     *  sub-hierarchy. Super node and its incident edges which are not
     *  connected to ports will be removed. Sub-hierarchy object is still
     *  usable but all of its super nodes are disconnected and moved to this
     *  hierarchy. Sub-hierarchy is also disconnected from this hierarchy.
     *  @param superNode A super node in this hierarchy.
     *  @return The hierarchy that has been flattened.
     *  @exception HierarchyException If an exception is returned
     *  by {@link dif.graph.hierarchy.HierarchyException#checkFlatten}.
     */
    public Hierarchy flatten(Node superNode) {
        HierarchyException exception =
            HierarchyException.checkFlatten(this, superNode);
        if(exception != null) {
            throw exception;
        }
        Hierarchy subHierarchy = getSuperNodes().get(superNode);
        // Copy connection info from ports of the sub-hierarchy.
        // This is because when the super nodes of the sub-hierarchy are
        // carried to higher level, all port passing information is lost.
        Vector relatedPorts = new Vector();
        Vector relatedNodes = new Vector();
        for(Iterator subPorts = subHierarchy.getPorts().iterator();
            subPorts.hasNext(); ) {
            Port subPort = (Port)subPorts.next();
            relatedPorts.add(subPort.getRelatedPort());
            relatedNodes.add(subPort.getNode());
        }
        // Merge graphs.
        getGraph().addGraph(subHierarchy.getGraph());
        // Reconnect all sub-hierarchies.
        for(Iterator subSuperNodes = subHierarchy.getSuperNodes().getNodes()
                .iterator();
            subSuperNodes.hasNext(); ) {
            Node subSuperNode = (Node)subSuperNodes.next();
            Hierarchy subSubHierarchy =
                subHierarchy.getSuperNodes().get(subSuperNode);
            subSubHierarchy.setName(subHierarchy.getName() + "."
                    + subSubHierarchy.getName());
            // Disconnecting the super nodes of subHierarchy will cause loss of
            // connections. If the connection is a port it will be later
            // recovered but we have to store the edge connections back.
            Vector subConnections = new Vector();
            for(Iterator subSubPorts = subSubHierarchy.getPorts().iterator();
                subSubPorts.hasNext(); ) {
                subConnections.add(((Port)subSubPorts.next()).getConnection());
            }
            addSuperNode(subSuperNode, subSubHierarchy);
            Iterator subConnectionsIterator = subConnections.iterator();
            for(Iterator subSubPorts = subSubHierarchy.getPorts().iterator();
                subSubPorts.hasNext(); ) {
                Port port = (Port)subSubPorts.next();
                Object subConnection = subConnectionsIterator.next();
                if(subConnection instanceof Edge) {
                    port.connect((Edge)subConnection);
                }
            }
        }
        // Handle connections.
        HashMap substitutedEdges = new HashMap();
        for(int i = 0; i < subHierarchy.getPorts().getAll().size(); i++) {
            Port subPort = (Port)subHierarchy.getPorts().getAll().get(i);
            Object connection = subPort.getConnection();
            Node relatedNode = (Node)relatedNodes.get(i);
            Port relatedPort = (Port)relatedPorts.get(i);
            int direction = subPort.getDirection();
            if(connection instanceof Port) {
                if(relatedPort == null) {
                    // Relate calls unrelate(), so no need to unrelate here.
                    ((Port)connection).relate(relatedNode);
                } else {
                    // "relatedPort" is automatically disconnected by the next
                    // call.
                    ((Port)connection).relate(relatedPort);
                }
            } else {
                Edge edge = (Edge)connection;
                if(substitutedEdges.keySet().contains(edge)) {
                    // Last connection on a formerly self loop edge.
                    edge = (Edge)substitutedEdges.get(edge);
                    substitutedEdges.remove(edge);
                }
                Node source = null;
                Node sink = null;
                Node otherEnd = null;
                if(direction > 0) {
                    if(edge.source() != superNode) {
                        sink = edge.source();
                    } else {
                        sink = edge.sink();
                    }
                    otherEnd = sink;
                    source = relatedNode;
                } else if(direction < 0) {
                    if(edge.sink() != superNode) {
                        source = edge.sink();
                    } else {
                        source = edge.source();
                    }
                    otherEnd = source;
                    sink = relatedNode;
                } else {
                    if(edge.source() == superNode) {
                        source = relatedNode;
                        sink = edge.sink();
                        otherEnd = sink;
                    } else {
                        source = edge.source();
                        sink = relatedNode;
                        otherEnd = source;
                    }
                }
                Edge newEdge = _copyEdge(edge, source, sink);
                if(otherEnd == superNode) {
                    substitutedEdges.put(edge, newEdge);
                } else {
                    if(getSuperNodes().get(otherEnd) != null) {
                        Port[] otherPort = getSuperNodes().get(otherEnd)
                            .getPorts().getConnectedPort(edge);
                        if(otherPort.length == 1) {
                            otherPort[0].connect(newEdge);
                        }
                    }
                }
                if(relatedPort != null) {
                    relatedPort.connect(newEdge);
                }
            }
        }
        // Disconnect super node.
        disconnectSuperNode(superNode);
        // Remove super node and incident edges.
        getGraph().removeNode(superNode);
        return subHierarchy;
    }

    /** Returns the graph object that backs this hierarchy.
     *  @return Graph object that backs this hierarchy.
     */
    public Graph getGraph() {
        return _graph;
    }

    /** Returns the name of this hierarchy.
     *  @return Name of this hierarchy.
     */
    public String getName() {
        return _hierarchyName;
    }

    /** Returns the hierarchy in which this hierarchy is defined as a
     *  sub-hierarchy.
     *  @return Parent hierarchy. Null is there is no parent.
     */
    public Hierarchy getParent() {
        return _parent;
    }

    /** Returns the list of ports in this hierarchy.
     *  @return The list of ports.
     */
    public PortList getPorts() {
        return _ports;
    }

    /** Returns a map of nodes that are defined as super nodes.
     *  Order of the nodes returned is the order they are added as super nodes.
     *  @return A map of nodes to hierarchies.
     */
    public SuperNodeMap getSuperNodes() {
        return _superNodes;
    }

    /** Returns a DAG representation of the hierarchy structure
     *  (i.e. If A is a subgraph of B there exists an edge that connects
     *  <code>node(A)</code> to <code>node(B)</code>) starting from this graph.
     *  @return DAG representation of the hierarchy structure starting from
     *  this hierarchy object. Weight of each node in the DAG is a hierarchy
     *  object.
     *  @see dif.graph.HierarchyToDot#hierarchyGraphToDot
     */
    public DirectedAcyclicGraph hierarchyGraph() {
        DirectedAcyclicGraph hierarchyGraph = new DirectedAcyclicGraph();
        Node topNode = hierarchyGraph.addNodeWeight(this);
        for(Iterator superNodes = getSuperNodes().iterator();
            superNodes.hasNext(); ) {
            DirectedAcyclicGraph subGraph =
                getSuperNodes().get((Node)superNodes.next()).hierarchyGraph();
            hierarchyGraph.addGraph(subGraph);
            hierarchyGraph.addEdge(topNode,
                    (Node)subGraph.sourceNodes().iterator().next());
        }
        return hierarchyGraph;
    }

    /** Returns true if the hierarchy contains at least one directed port.
     *  @return True if the hierarchy contains at least one directed port.
     */
    public boolean isDirected() {
        for(Iterator ports = getPorts().iterator(); ports.hasNext(); ) {
            Port port = (Port)ports.next();
            if(port.getDirection() != 0)
                return true;
        }
        return false;
    }

    /** Mirrors a hierarchy without copying the information about its
     *  parent so it can be connected to another parent.<p>
     *  The backing graph is mirrored using
     *  {@link MirrorTransformerStrategy}.
     *  Necessary edge and node conversions are done using
     *  <code>transformedVersionOf</code> of the transformer object.
     *  All ports are mirrored
     *  using {@link Port#mirror}.
     *  @param cloneWeights Weights of the nodes and edges will be cloned if
     *  true.
     *  @return Mirror hierarchy.
     */
    public Hierarchy mirror(boolean cloneWeights) {
        MirrorTransformerStrategy transformation = _mirrorGraph(getGraph());
        transformation.cloneWeight(cloneWeights);
        Class[] classes = new Class[2];
        classes[0] = _graphType().getClass();
        classes[1] = (new String()).getClass();
        Object[] parameters = new Object[2];
        parameters[0] = transformation.mirror();
        parameters[1] = getName();
        Hierarchy mirrorHierarchy = null;
        try {
            Constructor constructor = getClass().getConstructor(classes);
            mirrorHierarchy = (Hierarchy)constructor.newInstance(parameters);
        } catch(Exception exception) {
            System.out.println(KernelException.stackTraceToString(exception));
            throw new InternalErrorException(exception.getMessage());
        }
        for(Iterator superNodes = getSuperNodes().iterator();
            superNodes.hasNext(); ) {
            Node superNode = (Node)superNodes.next();
            Hierarchy subHierarchy = getSuperNodes().get(superNode);
            Node mirrorNode =
                (Node)transformation.transformedVersionOf(superNode);
            Hierarchy mirrorSubHierarchy = subHierarchy.mirror(cloneWeights);
            mirrorHierarchy.addSuperNode(mirrorNode, mirrorSubHierarchy);
            // Make edge-connections now. (Port connections will be dealt with
            // on the upper level in the last for loop)
            Iterator mirrorPorts = mirrorSubHierarchy.getPorts().iterator();
            for(Iterator ports = subHierarchy.getPorts().iterator();
                ports.hasNext(); ) {
                Port subPort = (Port)ports.next();
                Port mirrorSubPort = (Port)mirrorPorts.next();
                if(subPort.getConnection() instanceof Edge) {
                    Edge mirrorEdge = (Edge)transformation
                        .transformedVersionOf(subPort.getConnection());
                    mirrorSubPort.connect(mirrorEdge);
                }
            }
        }
        for(Iterator ports = getPorts().iterator();
            ports.hasNext(); ) {
            Port port = (Port)ports.next();
            Port mirrorPort = port.mirror(mirrorHierarchy, transformation);
        }
        return mirrorHierarchy;
    }

    /** Checks if all super nodes in this hierarchy can be flattened.
     *  Disconnects the super nodes that cannot be flattened. After this
     *  operation {@link #flatten} can be called with no exceptions.
     *  @return A list of sub-hierarchies that are disconnected.
     *  @see dif.graph.hierarchy.HierarchyException#checkFlatten
     */
    public List purge() {
        Vector returnValue = new Vector();
        Iterator superNodes = getSuperNodes().getNodes().iterator();
        for(; superNodes.hasNext(); ) {
            Node superNode = (Node)superNodes.next();
            if(HierarchyException.checkFlatten(this, superNode) != null) {
                returnValue.add(getSuperNodes().get(superNode));
                disconnectSuperNode(superNode);
            }
        }
        return returnValue;

    }

    /** Sets the name of the hierarchy to a new string.
     *  @param name New name.
     *  @return The old name.
     *  @exception HierarchyException If this is a sub-hierarchy and its parent
     *  already has another hierarchy with the same name. This check is
     *  performed
     *  using {@link dif.graph.hierarchy.SuperNodeMap#isDefined}.
     */
    public String setName(String name) {
        if(getParent() != null && getParent()._superNodes.isDefined(name)) {
            throw new HierarchyException("Cannot add " + getName()
                    + "as a sub-hierarchy to " + getParent().getName()
                    + " Name of the child already exists.");
        }
        String returnValue = _hierarchyName;
        _hierarchyName = name;
        return returnValue;
    }

    /** Returns the basic information about this hierarchy. This information
     *  includes name, port names, sub-hierarchy names and parent name of this
     *  hierarchy.
     *  @return Information string of this hierarchy.
     */
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("name: " + getName());
        buffer.append("\nports: " + getPorts());
        buffer.append("\nsub-hierarchies: ");
        for(Iterator superNodes = getSuperNodes().iterator();
            superNodes.hasNext(); ) {
            Node superNode = (Node)superNodes.next();
            buffer.append(getSuperNodes().get(superNode).getName()
                    + " ");
        }
        buffer.append("\nsuper-hierarchy: "
                + ((getParent() == null) ? "" : getParent().getName()) + "\n");
        return buffer.toString();
    }

    ///////////////////////////////////////////////////////////////////
    ////           protected methods                               ////

    /** Returns a copy of the edge with the given source and sink nodes as well
     *  as adding the new edge to the underlying graph. This
     *  function only copies (by reference) the weight of the edge in this
     *  implementation.
     *  If an extended version uses a different kind of graph, additional
     *  information attributed to an edge should be copied too.
     *  @param edge Original edge.
     *  @param source New source node.
     *  @param sink New sink node.
     *  @return The new edge.
     */
    protected Edge _copyEdge(Edge edge, Node source, Node sink) {
        Edge newEdge = null;
        if(edge.getWeight() != null) {
            newEdge = new Edge(source, sink, edge.getWeight());
        } else {
            newEdge = new Edge(source, sink);
        }
        getGraph().addEdge(newEdge);
        return newEdge;
    }

    /** Returns an empty graph of the most general type that this object can
     *  use as its backing graph.
     *  @return A graph of type {@link Graph}.
     */
    protected Graph _graphType() {
        return new Graph();
    }
    /** Returns a mirror transformation for the backing graph, which will be
     *  used in the {@link #mirror} function. This implementation returns a
     *  {@link MirrorTransformerStrategy}
     *  object. In case that a special type of graph backs an extension
     *  of the Hierarchy class, this method can be overridden to return the
     *  transformer of that class.
     *  @param graph The graph to be mirrored.
     *  @return Mirror graph.
     */

    protected MirrorTransformerStrategy _mirrorGraph(Graph graph) {
        return new MirrorTransformerStrategy(graph);
    }

    /** Put supernode to sub-hierarchy mapping in superNodeMap without
     *  disconnecting sub-hierarchy. If disconnecting sub-hierarchy, the
     *  related node and related port of the outer port also removed.
     *  However, in some cases, the derived subclasses may still want to have
     *  the inter references. Be careful to use this method. In other
     *  cases, use {@link #addSuperNode} instead.
     *  @param node Super node.
     *  @param hierarchy Sub-hierarchy to be associated with the node.
     *  @exception HierarchyException If
     *  {@link HierarchyException#checkSuperNode} returns an exception.
     *  @exception CyclicHierarchyException If a cycle is detected in the
     *  hierarchy structure.
     *  (Checked by {@link CyclicHierarchyException#checkCycle})
     */
    protected void _putSuperNode(Node node, Hierarchy hierarchy) {
        HierarchyException exception =
            HierarchyException.checkSuperNode(this, hierarchy);
        if(exception != null) {
            throw exception;
        }
        hierarchy._parent = this;
        _superNodes._put(node, hierarchy);
    }

    /** Remove supernode to sub-hierarchy mapping in superNodeMap without
     * disconnecting sub-hierarchy. If disconnecting sub-hierarchy, the
     *  related node and related port of the outer port also removed.
     *  However, in some cases, the derived subclasses may still want to have
     *  the inter references. Be careful to use this method. In other
     *  cases, use {@link #disconnectSuperNode} instead.
     *  @param node A super node in this hierarchy.
     */
    protected void _removeSuperNode(Node node) {
        Hierarchy subHierarchy = getSuperNodes().get(node);
        subHierarchy._parent = null;
        _superNodes._remove(node);
    }

    ///////////////////////////////////////////////////////////////////
    ////           protected variables                             ////

    /** Underlying graph of this hierarchy.
     */
    protected Graph _graph;

    /** Name of the hierarchy. Initially set to "_hierarchy".
     */
    protected String _hierarchyName;

    /** Ports of this hierarchy. All values are irreversible; new ports can be
     *  added any time but they cannot be changed or erased.
     */
    protected PortList _ports;

    /** Maps super nodes (type: Node) to sub-graphs (type: Hierarchy).
     */
    protected SuperNodeMap _superNodes;

    /** Parent of this graph (if it is set as a sub-graph of
     *  another hierarchy).
     */
    protected Hierarchy _parent;
}


