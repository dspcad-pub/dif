/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.graph.hierarchy;

import dif.util.graph.Edge;
import dif.util.graph.Graph;
import dif.util.graph.Node;
import dif.util.graph.analysis.strategy.MirrorTransformerStrategy;

import dif.util.Conventions;

//import psdfsim.PACM;

//////////////////////////////////////////////////////////////////////////
//// Port
/**
Port class for interfacing of hierarchy objects with other hierarchies.
<p>
Inside a hierarchy, A Port of this hierarchy can relate to a single Node
or relate to a port of a super node inside this hierarchy.
We use "relate" to associate node inside a hierarchy.
<p>
A port A connect to a port B (B in higher hierarchy), which means B relate
to A.
<p>
We restrict that a port can connect to either single edge or a single port
in higher hierarchy, but not both.
We use "connect" to associate edges or a port outside a hierarchy.
<p>
For more information about how to use this class refer to the package
documentation.
@author Fuat Keceli
@version $Id: Port.java 714 2009-06-30 18:12:32Z hhwu $
@see Graph
*/

public class Port {

    ///////////////////////////////////////////////////////////////////
    ////                         constructors                      ////

    /** Construct an undirected port.
     *  @param name Name of the port.
     *  @param hierarchy The hierarchy that contains this port.
     *  @exception HierarchyException If {@link HierarchyException#checkPort}
     *  returns an exception.
     *  @exception HierarchyException If
     *  {@link Conventions#labelConvention} returns an error.
     */
    public Port(String name, Hierarchy hierarchy) {
        this(name, hierarchy, INOUT);
    }

    /** Construct a directed port.
     *  @param name Name of the port.
     *  @param hierarchy The hierarchy that contains this port.
     *  @param direction {@link #IN} or {@link #OUT}.
     *  @exception HierarchyException If {@link HierarchyException#checkPort}
     *  returns an exception.
     *  @exception HierarchyException If
     *  {@link Conventions#labelConvention} returns an error.
     */
    public Port(String name, Hierarchy hierarchy, int direction) {
        String error = Conventions.labelConvention(name);
        if(error != null) {
            throw new HierarchyException("Port name error. " + error);
        }
        _disposed = false;
        _name = name;
        _hierarchy = hierarchy;
        _node = null;
        _relatedPort = null;
        _connection = null;
        _direction = direction;
//        _associatedPACM = null;

        HierarchyException exception =
            HierarchyException.checkPort(hierarchy, this);
        if(exception != null) {
            throw exception;
        }
        hierarchy.getPorts()._add(this);
    }

    ///////////////////////////////////////////////////////////////////
    ////                          public methods                   ////

    /** Connect a port to an edge.
     *  @param edge Edge to be connected.
     *  @return Previous connection if it existed, null otherwise.
     *  @exception HierarchyException If
     *  {@link HierarchyException#checkConnection} returns an exception.
     *  @exception HierarchyException If the port was disposed.
     */
    public Object connect(Edge edge) {
        if(_disposed) {
            throw new HierarchyException("Port " + getName() + " is disposed"
                    + " and can no longer be used.");
        }
        HierarchyException exception =
            HierarchyException.checkConnection(edge, this);
        if(exception != null) {
            throw exception;
        }
        Object returnValue = disconnect();
        _connection = edge;
        return returnValue;
    }

    /** Connect a port to another port.
     *  @param port A port in the super hierarchy of this hierarchy.
     *  @return Previous connection (a port or an edge), null if it doesn't
     *  exist.
     *  @return Previous connection if it existed, null otherwise.
     *  @exception HierarchyException If
     *  {@link HierarchyException#checkConnection} returns an exception.
     *  @exception HierarchyException If the port was disposed.
     */
    public Object connect(Port port) {
        Object returnValue = _connection;
        port.relate(this);
        return returnValue;
    }

    /** Disconnects a port from its edge or super-port. If the connection is a
     *  super-port <code>superPort.unrelate()</code> is called too.
     *  @return Previous connection (a port or an edge), null if it doesn't
     *  exist.
     */
    public Object disconnect() {
        if(_connection == null) {
            return null;
        }
        Object returnValue = _connection;
        if(_connection instanceof Port) {
            ((Port)_connection).unrelate();
        } else {
            _connection = null;
        }
        return returnValue;
    }

    /** Disposes an unconnected port. Removes it from the hierarchy port list
     *  and clears all connections of the port.
     *  @exception HierarchyException If the port was already disposed.
     */
    public void dispose() {
        if(_disposed) {
            throw new HierarchyException("Port " + getName() + " is disposed"
                    + " and can no longer be used.");
        }
        disconnect();
        unrelate();
        _disposed = true;
        _hierarchy.getPorts()._remove(this);
    }

    /** Returns the edge or port connected to this port.
     *  @return An edge or port in the super hierarchy.
     */
    public Object getConnection() {
        return _connection;
    }

    /** Returns the direction of a port.
     *  @return A positive integer for "out", a negative integer for "in" and
     *  zero for an undirected port.
     */
    public int getDirection() {
        return _direction;
    }

    /** Returns the hierarchy that this port belongs.
     *  @return The hierarchy that this port belongs.
     */
    public Hierarchy getHierarchy() {
        return _hierarchy;
    }

    /** Returns the name of this port.
     *  @return The name of this port.
     */
    public String getName() {
        return _name;
    }

    /** Returns the node associated with this port.
     *  @return A node of this hierarchy.
     */
    public Node getNode() {
        return _node;
    }

    /** Returns the port that is connected to this port from the inside of the
     *  hierarchy.
     *  @return A port from a sub-hierarchy. Null if it doesn't exist.
     */
    public Port getRelatedPort() {
        return _relatedPort;
    }

    /** Returns true if the port is connected, false otherwise.
     *  @return True if the port is connected, false otherwise.
     */
    public boolean isConnected() {
        return (getConnection() == null) ? false : true;
    }

    /** Returns true if this object is disposed and not available for future
     *  usage.
     *  @return True if the object is disposed.
     */
    public boolean isDisposed() {
        return _disposed;
    }

    /** Returns true if the port is related, false otherwise.
     *  @return True if the port is related, false otherwise.
     */
    public boolean isRelated() {
        return (getNode() == null) ? false : true;
    }

    /** Mirrors a port without copying its connection field. This method is
     *  mainly useful for mirroring hierarchies. To be able to use method
     *  a copy of the hierarchy graph should be obtained using
     *  {@link MirrorTransformerStrategy} and
     *  the hierarchy that is backed by the copy graph should be passed to this
     *  method. The associated node of the port will be automatically converted
     *  but connection of the port will be left undefined.
     *  @param hierarchy The copy hierarchy backed up by the mirrored graph.
     *  @param transformation Transformer that mirrored the original graph.
     *  @return The mirrored port.
     *  @see dif.graph.hierarchy.Hierarchy#mirror
     */
    public Port mirror(Hierarchy hierarchy,
            MirrorTransformerStrategy transformation) {
        if(_disposed) {
            throw new HierarchyException("Port " + getName() + " is disposed"
                    + " and can no longer be used.");
        }
        Port returnValue = new Port(getName(), hierarchy, getDirection());
        if(getNode() == null) {
            return returnValue;
        }
        Node node = (Node)transformation.transformedVersionOf(getNode());
        if(getRelatedPort() != null) {
            // Convert the port too.
            PortList subPorts = hierarchy.getSuperNodes().get(node).getPorts();
            Port subPort = subPorts.get(getRelatedPort().getName());
            subPort.connect(returnValue);
        } else {
            returnValue.relate(node);
        }
        return returnValue;
    }

    /** Defines an inner connection between a node and a port of a hierarchy.
     *  This method is different than {@link #connect} in the way that, connect
     *  defines a connection with a super hierarchy.
     *  @param node A node in the hierarchy to which this node belongs.
     *  @return Previous inner connection (a node or a port), null if it
     *  doesn't exist.
     */
    public Object relate(Node node) {
        Object returnValue = unrelate();
        _node = node;
        return returnValue;
    }

    /** Defines an inner connection between a sub-port and a port of a
     *  hierarchy.
     *  This method is different than {@link #connect} in the way that, connect
     *  defines a connection with a super hierarchy.
     *  @param port A port in a sub-hierarchy of this hierarchy.
     *  @return Previous inner connection (a node or a port), null if it
     *  doesn't exist.
     *  @exception HierarchyException If
     *  {@link HierarchyException#checkConnection} returns an exception.
     */
    public Object relate(Port port) {
        HierarchyException exception =
            HierarchyException.checkConnection(this, port);
        if(exception != null) {
            throw exception;
        }
        Object returnValue = unrelate();
        port.disconnect();
        port._connection = this;
        // Add the super node as the port node.
        _node = port.getHierarchy().getParent().getSuperNodes()
            .get(port.getHierarchy());
        _relatedPort = port;
        return returnValue;
    }

    /** Returns the long name of this port.
     *  @return Long name of the port: <i>hierarchyName.portName</i>
     */
    public String toString() {
        if(_disposed) {
            return "This object is no longer in use.";
        }
        return _hierarchy.getName() + "." + getName();
    }

    /** Disconnects the inner connection of this port. If the inner connection
     *  is another port outer connection of that port is disconnected too.
     *  @return The cancelled connection (a node or a port), null if it doesn't
     *  exist.
     */
    public Object unrelate() {
        Object returnValue = null;
        if(_relatedPort == null) {
            returnValue = _node;
        } else {
            returnValue = _relatedPort;
            _relatedPort._connection = null;
            _relatedPort = null;
        }
        _node = null;
        return returnValue;
    }

    /** Associate the PACM with the port.
     */
/*    public void setPACM(PACM pacm) {
        _associatedPACM = pacm;
    }
*/

    /** Return the associated PACM.
     */
/*    public PACM getPACM() {
        return _associatedPACM;
    }
*/
    ///////////////////////////////////////////////////////////////////
    ////           public variables                                ////

    // A negative value will mean input and a positive value will mean output.
    // Zero is reserved for undirected ports.

    /** Indicates that a port is an input port.
     */
    public static final int IN = -1;

    /** Indicates that a port is an output port.
     */
    public static final int OUT = 1;

    /** Indicates that a port is a bi-directional port.
     */
    public static final int INOUT = 0;

    ///////////////////////////////////////////////////////////////////
    ////           private variables                               ////

    private boolean _disposed;
    private Node _node;
    private Port _relatedPort;
    private Object _connection;
    private Hierarchy _hierarchy;
    private String _name;
    private int _direction;

    //private PACM _associatedPACM;
}


