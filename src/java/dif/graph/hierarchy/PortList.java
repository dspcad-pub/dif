/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.graph.hierarchy;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;

import dif.util.graph.Edge;
import dif.util.graph.Node;
import dif.util.InternalErrorException;

//////////////////////////////////////////////////////////////////////////
//// PortList
/** List of ports in a hierarchy. It provides methods to
check if a name is defined previously as a port or if an edge is connected to
another port prior to a connection. Ports are stored in the order they are
added, {@link #getAll} and {@link #iterator} methods maintain this order.
@author Fuat Keceli
@version $Id: PortList.java 606 2012-10-08 16:29:47Z plishker $
*/
public class PortList {

    ///////////////////////////////////////////////////////////////////
    ////                         constructors                      ////

    /** Construct a port list.
     */
    public PortList() {
        _portList = new Vector();
        _nameIndex = 0;
    }

    ///////////////////////////////////////////////////////////////////
    ////                        public methods                     ////

    /** Disconnects all port connections with the super-hierarchy.
     */
    public void disconnectAll() {
        for(Iterator ports = iterator(); ports.hasNext(); ) {
            Port port = (Port)ports.next();
            port.disconnect();
        }
    }

    /** Returns the port defined with the given name.
     *  @param name Name of the port that is being searched.
     *  @return Port with the given name. Null if not found.
     */
    public Port get(String name) {
        for(Iterator ports = iterator(); ports.hasNext(); ) {
            Port port = (Port)ports.next();
            if(port.getName().equals(name)) {
                return port;
            }
        }
        return null;
    }

    /** Returns the port related to the given node or one of its ports.
     *  @param node Node that is being searched.
     *  @return Port related to the given node. Null if not found.
     */
    public Port get(Node node) {
        for(Iterator ports = iterator(); ports.hasNext(); ) {
            Port port = (Port)ports.next();
            if(port.getNode() == node) {
                return port;
            }
        }
        return null;
    }

    /** Returns a list of all ports in the order they are added. Returned list
     *  is a copy so the user is free to modify it.
     *  @return A list of all ports in the order they are added.
     */
    public List getAll() {
        return new Vector(_portList);
    }

    /** Returns the port(s) that this edge is connected. If the edge is not
     *  connected length of the returned array is 0. If the edge is connected
     *  length of the returned array might be 1 or 2 (depending on the number
     *  of ends connected to ports of sub-hierarchies).
     *  @param edge A graph edge.
     *  @return An array of length 0, 1 or 2.
     */
    public Port[] getConnectedPort(Edge edge) {
        Vector connections = new Vector();
        for(Iterator ports = iterator(); ports.hasNext(); ) {
            Port port = (Port)ports.next();
            if(port.getConnection() == edge) {
                connections.add(port);
                if(edge.isSelfLoop()) {
                    break;
                }
            }
        }
        return (Port[])connections.toArray(new Port[0]);
    }

    /** Checks if an edge is already connected to a port in this list.
     *  @return True if the edge is connected. False otherwise.
     */
    public boolean isConnected(Edge edge) {
        return (getConnectedPort(edge).length == 0) ? false : true;
    }

    /** Checks if a port name is previously defined.
     *  @return True is the name is previously defined. False otherwise.
     */
    public boolean isDefined(String name) {
        return (get(name) == null) ? false : true;
    }

    /** Returns an iterator over the list of port added. The order of the
     *  iterator is the order that ports are added.
     *  @return An iterator over the added ports.
     */
    public ListIterator iterator() {
        return (new Vector(_portList)).listIterator();
    }

    /** Returns a string that is guaranteed to be unused in this port list as a
     *  port name.
     *  @return An port name.
     */
    public String newName() {
        String name = "port" + _nameIndex;
        while(isDefined(name)) {
            _nameIndex++;
            name = "port" + _nameIndex;
        }
        _nameIndex++;
        return name;
    }

    /** Returns the names of the ports listed in this list in the order they
     *  are added.
     *  @return The names of the ports listed in this list.
     */
    public String toString() {
        String value = "";
        for(Iterator ports = iterator(); ports.hasNext(); ) {
            value = value + ports.next() + " ";
        }
        String returnValue = (value.length() == 0) ?
            "" : value.substring(0, value.length() - 1);
        return returnValue;
    }

    ///////////////////////////////////////////////////////////////////
    ////           protected methods                               ////

    /** Adds a port to this list. This method doesn't perform any error
     *  checking so it is the users responsibility to preserve consistency of
     *  ports. <code>isDefined</code> and <code>isConnected</code> methods can
     *  be used to perform most of these checks.
     */
    protected void _add(Port port) {
        if(_hierarchy != null && _hierarchy != port.getHierarchy()) {
            throw new InternalErrorException("Trying to add " + port.toString()
                    + "to another hierarchy: " + _hierarchy.getName());
        }
        _hierarchy = port.getHierarchy();
        _portList.add(port);
    }

    /** Removes a port form the port list.
     */
    protected void _remove(Port port) {
        _portList.remove(port);
    }

    ///////////////////////////////////////////////////////////////////
    ////           private methods and variables                   ////

    private Vector _portList;
    private Hierarchy _hierarchy;
    private int _nameIndex;
}


