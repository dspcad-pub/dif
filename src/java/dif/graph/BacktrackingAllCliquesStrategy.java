/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.graph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import dif.util.graph.Graph;
import dif.util.graph.Node;
import dif.util.graph.analysis.strategy.CachedStrategy;

//////////////////////////////////////////////////////////////////////////
//// BacktrackingAllCliquesStrategy
/**
Finding all cliques in a graph. The collection of cliques returned cannot
be modified. This implementation is adapted from a backtracking algorithm,
as taken from
<em>Combinatorial Algorithms: generation, enumeration and search</em> by
D. Kreher and D. Stinson. (CRC Press, 1998)
<p>
@see mapss.graph.AllCliquesAnalysis
@author Ming-Yung Ko
@version $Id: BacktrackingAllCliquesStrategy.java 606 2012-10-08 16:29:47Z plishker $
*/
public class BacktrackingAllCliquesStrategy
        extends CachedStrategy implements AllCliquesAnalyzer {

    /** Construct a cliques finding strategy for a given graph.
     *  @param graph The given graph.
     */
    public BacktrackingAllCliquesStrategy(Graph graph) {
        super(graph);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Return the collection of all cliques. Each element is a collection
     *  of clique (completely connected) nodes.
     *
     *  @return The collection of cliques.
     */
    public Collection cliques() {
        return Collections.unmodifiableCollection(
                (Collection)((List)_result()).get(0));
    }

    /** Return the maximal cliques. A maximal clique is a clique not properly
     *  contained into another one. See also {@link #cliques()}
     *
     *  @return The collection of maximal cliques. Each element contains a
     *          maximal clique.
     */
    public Collection maximalCliques() {
        return Collections.unmodifiableCollection(
                (Collection)((List)_result()).get(1));
    }

    /** Return a description of the analysis in finding all cliques.
     *  @return A description of the analysis in finding all cliques.
     */
    public String toString() {
        String result = "Finding all cliques analysis for the following "
                + "graph.\n" + graph().toString();
        result += "All cliques (per line) in node labels:\n";
        result += Graphs.displayComponents(graph(), cliques());
        result += "Maximal cliques (per line) in node labels:\n";
        result += Graphs.displayComponents(graph(), maximalCliques());
        return result;
    }

    /** Check compatibility of the class of graph. The given graph
     *  must be an instance of Graph.
     *
     *  @return True if the given graph is of class Graph.
     */
    public boolean valid() {
        if (!(graph() instanceof Graph)) {
            return false;
        }
        return true;
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /** Find all cliques and maximal cliques of the graph. The result is
     *  returned as a <code>List</code> where the first element is a
     *  collection of all the cliques and the second one is a collection of
     *  maximal cliques.
     *  @return All cliques and maximal cliques.
     */
    protected Object _compute() {
        _cliques(new ArrayList());
        List result = new ArrayList();
        result.add(_cliques);
        result.add(_maximalCliques);
        return result;
    }

    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////

    /*  The body of cliques finding implementation.
     *  @param clique The clique to expand. It may not necessarily be a
     *         maximal clique.
     */
    private void _cliques(List clique) {
        if (clique.isEmpty()) {
            _cliques = new ArrayList();
            _maximalCliques = new ArrayList();
        } else
            _cliques.add(clique);

        // Maximal-clique verification.
        Collection nodesToExtend = _nodesToExtend(clique);
        if (nodesToExtend.isEmpty())
            _maximalCliques.add(clique);

        // Extend the present clique to a bigger clique.
        Iterator choices = _choiceSet(clique).iterator();
        while (choices.hasNext()) {
            Object choice = choices.next();
            ArrayList biggerClique = new ArrayList(clique);
            biggerClique.add(choice);
            _cliques(biggerClique);
        }
    }

    /*  Return neighbors with the given node. This is the A(v) in the
     *  algorithm.
     *
     *  @param node The given node label.
     *  @return The neighbors.
     */
    private Collection _neighbors(Node node) {
        return graph().neighbors(node);
    }

    /*  Return nodes of labels larger than that of the given node. This is
     *  the B(v) in the algorithm.
     *
     *  @param node The given node.
     *  @return The nodes of larger labels.
     */
    private Collection _nodesOfLargerLabel(Node node) {
        Collection largerNodes = new ArrayList();
        int nodeLabel = graph().nodeLabel(node);
        for (int i = nodeLabel + 1; i < graph().nodeCount(); i++)
            largerNodes.add(graph().node(i));
        return largerNodes;
    }

    /*  The choice set to expand a clique. This is the
     *  C(l) in the algorithm.
     *
     *  @param clique The clique.
     *  @return The node set to expand a clique.
     */
    private Collection _choiceSet(List clique) {
        Collection choiceSet = new ArrayList();
        if (clique.isEmpty())
            choiceSet.addAll(graph().nodes());
        else {
            Node lastCliqueNode = (Node)clique.get(clique.size() - 1);
            List previousClique = new ArrayList(clique);
            previousClique.remove(lastCliqueNode);
            // Intersection of A(v), B(v), and C(l).
            choiceSet.addAll(_neighbors(lastCliqueNode));
            choiceSet.retainAll(_nodesOfLargerLabel(lastCliqueNode));
            choiceSet.retainAll(_choiceSet(previousClique));
        }
        return choiceSet;

    /*  This is a non-recursive (looped style) implementation.
     *
        List choiceSet = new ArrayList(graph().nodes());
        Iterator cliqueNodes = clique.iterator();
        while (cliqueNodes.hasNext()) {
            Node cliqueNode = (Node)cliqueNodes.next();
            if (!choiceSet.isEmpty()) {
                choiceSet.retainAll(_neighbors(cliqueNode));
                choiceSet.retainAll(_nodesOfLargerLabel(cliqueNode));
            }
        }
        return choiceSet;
     */

    }

    /*  Return the nodes that can extend a clique to a maximal clique.
     *  This is the N(l) in the algorithm.
     *
     *  @param clique The clique.
     *  @return The nodes to extend.
     */
    private Collection _nodesToExtend(List clique) {
        Collection nodesToExtend = new ArrayList();
        if (clique.isEmpty())
            nodesToExtend.addAll(graph().nodes());
        else {
            Node lastCliqueNode = (Node)clique.get(clique.size() - 1);
            List previousClique = new ArrayList(clique);
            previousClique.remove(lastCliqueNode);
            // Intersection of A(v) and N(l).
            nodesToExtend.addAll(_neighbors(lastCliqueNode));
            nodesToExtend.retainAll(_nodesToExtend(previousClique));
        }
        return nodesToExtend;

    /*  This is a non-recursive (looped style) implementation.
     *
        Collection nodesToExtend = new ArrayList(graph().nodes());
        Iterator cliqueNodes = clique.iterator();
        while (cliqueNodes.hasNext()) {
            Node cliqueNode = (Node)cliqueNodes.next();
            if (!nodesToExtend.isEmpty())
                nodesToExtend.retainAll(_neighbors(cliqueNode));
        }
        return nodesToExtend;
     */

    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    // The collection of all cliques.
    private Collection _cliques;

    // The collection of maximal cliques.
    private Collection _maximalCliques;

    // Flag to turn on debugging output.
    private boolean _debug = false;
}


