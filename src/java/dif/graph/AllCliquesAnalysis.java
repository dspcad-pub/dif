/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.graph;

import java.util.Collection;

import dif.util.graph.analysis.analyzer.Analyzer;
import dif.util.graph.analysis.Analysis;

//////////////////////////////////////////////////////////////////////////
//// AllCliquesAnalysis
/**
Finding all cliques in a graph.
<p>
The returned collection cannot be modified when the client uses the default
strategy.
<p>

@author Ming-Yung Ko
@version $Id: AllCliquesAnalysis.java 606 2012-10-08 16:29:47Z plishker $
*/

public class AllCliquesAnalysis extends Analysis {

    /** Construct an instance of this class using a given analyzer.
     *
     *  @param analyzer The given analyzer.
     */
    public AllCliquesAnalysis(AllCliquesAnalyzer analyzer) {
        super(analyzer);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Return the collection of all cliques. Each element is a collection
     *  of clique (completely connected) nodes.
     *
     *  @return The collection of cliques.
     */
    public Collection cliques() {
        return ((AllCliquesAnalyzer)analyzer()).cliques();
    }

    /** Return the maximal cliques. A maximal clique is a clique not properly
     *  contained into another one. See also {@link #cliques()}
     *
     *  @return The collection of maximal cliques. Each element contains a
     *          maximal clique.
     */
    public Collection maximalCliques() {
        return ((AllCliquesAnalyzer)analyzer()).maximalCliques();
    }

    /** Return a description of the analysis and the associated analyzer.
     *
     *  @return A description of the analysis and the associated analyzer.
     */
    public String toString() {
        return "Finding all cliques using the following analyzer:\n"
                + analyzer().toString();
    }

    /** Check if a given analyzer is compatible with this analysis.
     *  In other words if it is possible to use it to compute the computation
     *  associated with this analysis.
     *
     *  @param analyzer The given analyzer.
     *  @return True if the given analyzer is valid for this analysis.
     */
    public boolean validAnalyzerInterface(Analyzer analyzer) {
        return analyzer instanceof AllCliquesAnalyzer;
    }
}


