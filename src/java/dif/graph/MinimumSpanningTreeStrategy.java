/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import dif.util.graph.Edge;
import dif.util.graph.Graph;
import dif.util.graph.Node;
import dif.util.graph.mapping.ToDoubleMapping;
import dif.util.graph.analysis.strategy.CachedStrategy;

//////////////////////////////////////////////////////////////////////////
//// MinimumSpanningTreeStrategy
/**
Computation of a minimum spanning tree in a graph.
The collection returned cannot be modified.
<p>
This implementation is adapted from Prim's algorithm, as taken from
<em>Introduction to Algorithms</em>
by Cormen, Leiserson, and Rivest (second edition).
@see MinimumSpanningTreeAnalysis
@author Shuvra S. Bhattacharyya
@version $Id: MinimumSpanningTreeStrategy.java 606 2012-10-08 16:29:47Z plishker $
*/

public class MinimumSpanningTreeStrategy extends CachedStrategy
        implements MinimumSpanningTreeAnalyzer {

    /** Construct a minimum spanning tree strategy for a given graph and
     *  a given set of edge weights.
     *  @param graph The given graph.
     *  @param edgeWeights The edge weights.
     */
    public MinimumSpanningTreeStrategy(Graph graph,
            ToDoubleMapping edgeWeights) {
        super(graph);
        _edgeWeights = edgeWeights;
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Compute a minimum spanning tree in the form of a
     *  a list. Each element of the list is an {@link Edge}.
     *  @return The edges in the minimum spanning tree.
     *  FIXME: why is this is a list?
     */
     public List edges() {
        return (List)_result();
     }

    /** Return a description of the minimal spanning tree.
     *
     *  @return A description of the minimal spanning tree.
     */
    public String toString() {
        String result = "Minimum spanning tree analysis for the following "
                + "graph.\n" + graph().toString();
        result += "The edges in the minimum spanning tree are:\n" + _result();
        return result;
    }

    /** Check compatibility of the class of graph. The given graph
     *  must be an instance of Graph.
     *
     *  @param graph The given graph.
     *  @return True if the given graph is of class Graph.
     */
    public boolean valid() {
        if (!(graph() instanceof Graph)) {
            return false;
        }
        return Graphs.isConnected(graph());
    }

    /** Return the weight of the minimum spanning tree (i.e., the sum of the
     *  weights of the edges in the tree.
     *  @return The weight of the minimum spanning tree.
     */
    public double weight() {
        _result(); // Ensure that the spanning tree is computed.
        return _spanningTreeWeight;
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /** Compute a minimal spanning tree of the graph in the form of
     *  a {@link List}. Each element of the list is an {@link Edge}.
     *  If the graph contains no edges, an empty list is returned.
     *  The graph is assumed to be connected.
     *  @return The list of edges in the minimal spanning tree.
     */
    protected Object _compute() {
        _spanningTreeList = new ArrayList(graph().nodeCount() - 1);
        _spanningTreeWeight = 0;
        if (graph().edgeCount() == 0) {
            return _spanningTreeList;
        }
        Edge[] keyEdges = new Edge[graph().nodeCount()];
        double keys[] = new double[graph().nodeCount()];
        HashSet nodeSet = new HashSet(graph().nodeCount());
        Iterator nodes = graph().nodes().iterator();
        Node root = (Node)(nodes.next());   // Select an arbitrary root vertex
        keys[graph().nodeLabel(root)] = 0;
        nodeSet.add(root);
        while (nodes.hasNext()) {
            Node node = (Node)(nodes.next());
            keys[graph().nodeLabel(node)] = Double.POSITIVE_INFINITY;
            keyEdges[graph().nodeLabel(node)] = null;
            nodeSet.add(node);
        }
        int nodeCount = 0;
        while (!nodeSet.isEmpty()) {
            // Extract a node with a minimal key value.
            Iterator remainingNodes = nodeSet.iterator();
            double minimumNodeKey = Double.POSITIVE_INFINITY;
            Node minimumNode = null;
            while (remainingNodes.hasNext()) {
                Node node = (Node)(remainingNodes.next());
                double key = keys[graph().nodeLabel(node)];
                if (key < minimumNodeKey) {
                    minimumNodeKey = key;
                    minimumNode = node;
                }
            }
            nodeSet.remove(minimumNode);
            if (_debug) {
                System.out.println("Extracted node " + minimumNode);
            }

            // Add the appropriate edge if this is not the first node in the
            // tree
            if (++nodeCount > 1) {
                Edge newEdge = keyEdges[graph().nodeLabel(minimumNode)];
                _spanningTreeWeight += _edgeWeights.toDouble(newEdge);
                _spanningTreeList.add(newEdge);
                if (_debug) {
                    System.out.println("Extracted edge " + newEdge);
                }
            }
            Iterator neighbors = graph().neighbors(minimumNode).iterator();
            while (neighbors.hasNext()) {
                Node neighbor = (Node)(neighbors.next());
                if (nodeSet.contains(neighbor)) {
                    if (_debug) {
                        System.out.println("Examining neighbor " + neighbor);
                    }
                    Iterator neighborEdges = graph().neighborEdges(minimumNode,
                            neighbor).iterator();
                    double minimumNeighborEdgeWeight = Double.POSITIVE_INFINITY;
                    Edge minimumWeightNeighborEdge = null;
                    while (neighborEdges.hasNext()) {
                        Edge neighborEdge = (Edge)(neighborEdges.next());
                        if (_edgeWeights.toDouble(neighborEdge) <
                                minimumNeighborEdgeWeight) {
                            minimumNeighborEdgeWeight =
                                    _edgeWeights.toDouble(neighborEdge);
                            minimumWeightNeighborEdge = neighborEdge;
                        }
                    }
                    if (minimumNeighborEdgeWeight <
                            keys[graph().nodeLabel(neighbor)]) {
                        // Update the key and re-insert the neighbor
                        nodeSet.remove(neighbor);
                        keys[graph().nodeLabel(neighbor)] =
                                minimumNeighborEdgeWeight;
                        keyEdges[graph().nodeLabel(neighbor)] =
                                minimumWeightNeighborEdge;
                        nodeSet.add(neighbor);
                        if (_debug) {
                            System.out.println("Updated neighbor with key "
                                    + minimumNeighborEdgeWeight);
                        }
                    }

                }
            }
        }
        return _spanningTreeList;
    }

    /** Return the result of this analysis in a form that cannot be modified.
     *  @param result The original result.
     *  @return The analysis result in unmodifiable form.
     */
    protected Object _convertResult(Object result) {
        return Collections.unmodifiableList((List)result);
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    // The edge weights to be used in the spanning tree computation.
    private ToDoubleMapping _edgeWeights;

    // The spanning tree. Each element is an Edge.
    private ArrayList _spanningTreeList;

    // The sum of the edge weights in the minimum spanning tree.
    private double _spanningTreeWeight;

    // Flag to turn on debugging output.
    private boolean _debug = false;
}


