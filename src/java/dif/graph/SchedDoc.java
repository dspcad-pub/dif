/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.graph;

import dif.DIFGraph;

import dif.util.command.CommandUtilities;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import java.util.HashMap;
import java.util.Iterator;

import dif.util.graph.Element;
import dif.util.graph.Graph;
import dif.util.graph.Node;
import dif.util.sched.ScheduleTree;

//////////////////////////////////////////////////////////////////////////
//// HTMLGenerator
/**
Generates hierarchical HTML of DIF graphs
<p>
<Class comment continued>
<p>
<etc.>

@author Ivan Corretjer
@version 0.1
*/
public class SchedDoc {

    /** <Constructor comment>
     */
    protected SchedDoc() {
        // Empty constructor is forbidden.
    }

    /** Constructor for generating an SchedDoc object for just this graph.
     * @param graph Graph to use as basis for HTML generation.
     */
    public SchedDoc(ScheduleTree st, DIFGraph g) {
        indentLevel = 0;
        indentAmount = "   ";
        htmlStringBuffer = new StringBuffer();
	//        _topHier = null;
        _schedTree = st;
	//        _appGraph = (DIFGraph)g.mirror();
        _appGraph = g;

        topLevelGraphName = g.getName();
	//        ((Graph)_schedTree).setName(topLevelGraphName);

        _elementAttributes = new HashMap();

        for(Iterator nodes = _schedTree.nodes().iterator(); nodes.hasNext();) {
            Node nextNode = (Node)nodes.next();

	    _elementAttributes.put(nextNode, new HashMap());
	    //((HashMap)_elementAttributes.get(nextNode)).put("label", String.valueOf(_schedTree.nodeLabel(nextNode)));
	    //	    ((HashMap)_elementAttributes.get(nextNode)).put("label", String.valueOf(getAttribute(nextNode)));
        }
    }

    /** Constructor for generating a hierarchical SchedDoc object.
     * @param hier Hierarchy to use as the top-level hierarchy for HTML
     * generation.
     */
    /*    public SchedDoc(Hierarchy hier) {
        indentLevel = 0;
        indentAmount = "   ";
        htmlStringBuffer = new StringBuffer();
        _topHier = (Hierarchy)hier.mirror(true);
        _schedTree = ((DIFGraph)_topHier.getGraph()).mirror();
        
        topLevelGraphName = ((DIFGraph)((DIFHierarchy)hier).getGraph()).getName();
        ((DIFGraph)_schedTree).setName(topLevelGraphName);
        
        _elementAttributes = new HashMap();
        
        for(Iterator nodes = _schedTree.nodes().iterator(); nodes.hasNext();) {
            Node nextNode = (Node)nodes.next();

            _elementAttributes.put(nextNode, new HashMap());
            ((HashMap)_elementAttributes.get(nextNode)).put("label", String.valueOf(_schedTree.nodeLabel(nextNode)));
        }
	}*/

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Sets the attribute for an element (node or edge) in the graph.
     * @param graphElement The element whose attribute we are setting
     * @param attribute Attribute that is going to be set
     * @param value The value of the attribute
     * @return The previous value of the attribute if it exists, else null.
     */
    public String setAttribute(Element graphElement, String attribute, String value) {
        return (String)((HashMap)_elementAttributes.get(graphElement)).put(attribute, value);
    }

    /** Creates "HTML" code string of the SchedDoc object.
     * @return The "HTML" code.
     */
    public String toString() {
        // HTML header
        htmlStringBuffer.append("<!-- HTML file created by HTMLGenerator -->\n"
                + "<html>\n"
                + "<body>\n"
                + "<pre>\n");

        // print top graph's Name fist
        //htmlStringBuffer.append(topLevelGraphName + "<br>\n");
        generateNodePNG(_schedTree, _appGraph);
        htmlStringBuffer.append("<a HREF=\"" + topLevelGraphName + ".png\">" + topLevelGraphName + "</a><br>\n");

	//        if(_topHier != null) {
	//            printHierGraph(_topHier, indentLevel, htmlStringBuffer);
	//        } else {
            //just a regular graph

            for(Iterator nodes = _schedTree.nodes().iterator(); nodes.hasNext();) {
                Node nextNode = (Node)nodes.next();

                // get the current node's label
                String curNodeLabel = (String)((HashMap)_elementAttributes.get(nextNode)).get("label");
                htmlStringBuffer.append(curNodeLabel + " <br>\n");
            }
	    //        }

        // HTML footer
        htmlStringBuffer.append("</pre>\n"
                + "</body>\n"
                + "</html>\n");

        return htmlStringBuffer.toString();
    }

    /** Creates "HTML" file fileName.html from HTMLGenerator object.
     * @param fileName Name of the HTML file to be written.
     */
    public void toFile(String fileName) throws IOException {
        FileOutputStream fileStream = new FileOutputStream(fileName + ".html");
        PrintWriter out = new PrintWriter(fileStream);

        out.print(toString());
        out.close();

        fileStream.close();
    }

    /**
     *
    protected void printHierGraph(Hierarchy curHier, int indentLevel, StringBuffer curString) {
        DIFGraph curGraph = (DIFGraph)((DIFHierarchy)curHier).getGraph();

        for(Iterator nodes = curGraph.nodes().iterator(); nodes.hasNext();) {
            Node node = (Node)nodes.next();

            if(curHier.getSuperNodes().isSuperNode(node)) {
                DIFHierarchy superNodeHier = (DIFHierarchy)curHier.getSuperNodes().get(node);
                DIFGraph superNodeGraph = (DIFGraph)superNodeHier.getGraph();
                generateNodePNG(superNodeGraph);

                curString.append(insertIndent(indentLevel) + "<a HREF=\"" + superNodeGraph.getName() + ".png\">" + curGraph.getName(node) + "</a><br>\n");

                indentLevel++;

                printHierGraph(superNodeHier, indentLevel, curString);

                indentLevel--;
            } else {
                curString.append(insertIndent(indentLevel) + curGraph.getName(node) + " <br>\n");
            }
        }
    }
     */


    /** Takes in a DIFGraph and produces a PNG file in the current working directory
     * with name DIFGraph.getName().png
     */
    public void generateNodePNG(ScheduleTree st, DIFGraph g) {
        ScheduleTreeToDot stDot = new ScheduleTreeToDot(st, g);
        String nodeGraphName = g.getName();

        try {
            stDot.setGraphName(nodeGraphName);
            //make sure our nodes are ellipses not circles
            stDot.addLine("node[shape = ellipse]");
            stDot.setAsDirected(true);
            stDot.toFile(nodeGraphName);
        } catch(IOException ex) {
            System.err.println("Problem writing Dot file for " + nodeGraphName);
        }

        // produce the PNG file from the newly created Dot file
        CommandUtilities system = new CommandUtilities();
        String[] command = new String[5];
        command[0] = "dot";
        command[1] = "-Tpng";
        command[2] = "-o";
        command[3] = nodeGraphName + ".png";
        command[4] = nodeGraphName + ".dot";

        system.runCommand(command);
        System.err.println(system.getErrorMessage());
    }


    /** Sets the amount of indent per level of the hierarchy.
     * @param indentAmount The "String" used to indent each level of the
     * hierarchy.
     */
    public void setIndentAmount(String indentString) {
        indentAmount = indentString;
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public fields                     ////


    ///////////////////////////////////////////////////////////////////
    ////                         protected methods                 ////
    /** Returns a string containing indentLevel many indents.  The amount
     * of indention is set by setIndentAmount.
     * @param indentLevel The amount of indents
     * @return A string containing the appropiate number of indents
     */
    protected String insertIndent(int indentLevel) {
        String indentedString = "   ";

        for(int i=0; i<indentLevel; i++) {
            indentedString += indentAmount;
        }
        return indentedString;
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected fields                    ////

    // Departing from DotGenerator in that we want our hierarchy
    // and graph to be DIF objects so that they retain all information
    // such as graph name.  If we only store the object as Graph when
    // we clone it in the constructor we loose this information
    // SO let's store the graph name in a variable for now...
    protected String topLevelGraphName;

    /** Clone of the original hierarchy given in the constructor.
     */
    //    protected Hierarchy _topHier;

    /** Clone of the original graph given in the constructor.
     * @see Graph#clone
     */
    protected ScheduleTree _schedTree;
    protected DIFGraph _appGraph;

    /** HashMap to store graph element attributes (such as node labels).
     */
    protected HashMap _elementAttributes;

    ///////////////////////////////////////////////////////////////////
    ////                         private methods                   ////


    ///////////////////////////////////////////////////////////////////
    ////                         private fields                    ////
    private String indentAmount;
    private int indentLevel;
    private StringBuffer htmlStringBuffer;
}


