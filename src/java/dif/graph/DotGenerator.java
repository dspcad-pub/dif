/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.graph;
import dif.DIFAttribute;
import dif.DIFGraph;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import java.util.LinkedList;
import dif.util.graph.Edge;
import dif.util.graph.Element;
import dif.util.graph.Graph;
import dif.util.graph.GraphException;
import dif.util.graph.Node;

//////////////////////////////////////////////////////////////////////////
//// DotGenerator
/**
Dot file or string generator for GraphViz graph visualization package. Any
graph that will be used as an input to this class should have a
valid <code>clone()</code> method. This is because the graph object passed
to the DotGenerator is cloned and cached.
<P>
Output files (or strings) obtained from this class can be fed to
Dot or Neato tools of the GraphViz package to generate graphical
representations of <code>mocgraph.Graph</code> objects. GraphViz package
is developed by AT&T labs and can be found at www.graphviz.org.
<P>
Although Dot and Neato are intended for directed and undirected graphs
respectively they can handle both kind of graph inputs. Input files are written
in "Dot File Format" and generated files will
automatically have a ".dot" extension. For more information about the usage of
these tools, refer to GraphViz documentation.
<P>
DotGenerator class provides two ways for creating dot files (or strings).
First is using the static <code>toFile</code> methods which don't require
constructing a
DotGenerator object. These methods use the default settings for node and edge
attributes. By default, node labels are copied from the graph using nodeLabel
method and node shapes are ellipses. Second way is by constructing an object
and using its methods. This is required if one needs to change the default
attributes or add new attributes to the visual graph. A clone of the original
graph is used in the DotGenerator object for caching.
<P>
To run dot or neato to create a Postscript file following
commands can be used in command prompt:
<P>
<code>dot | neato -Tps </code><i>inputFile</i><code> -o </code>
<i>outputFile</i>
<P>
For further commands see GraphViz documentation.
@author Fuat Keceli
@version $Id: DotGenerator.java 720 2009-08-07 22:23:33Z nsane $
@see Graph
*/

public class DotGenerator {

    protected DotGenerator() {
        // Empty constructor is forbidden.
    }

    /** Constructs DotGenerator object associated with a graph. It uses a clone
     *  of the provided graph to avoid consistency issues.
     *  <p>
     *  This is only required if you need to change the default attributes
     *  of the visual graph. Otherwise it is easier to use the static methods.
     *  @param g A Graph object.
     */
    public DotGenerator(Graph g) {
        _clusterAttributes = new HashMap();
        _clusters = new Vector();
        _extraLines = new StringBuffer();
        _elementAttributes = new HashMap();
	//        _difGraph = (DIFGraph)graph.clone();
        _difGraph = null;
        _graph = g;
        _isDirected = false;
        _name = "_graph";
        //Create a hash map for each node and put set label attribute to the
        //node label.
        for(Iterator nodes = _graph.nodes().iterator(); nodes.hasNext(); ) {
            Node nextNode = (Node) nodes.next();

	    _elementAttributes.put(nextNode, new HashMap());
	    ((HashMap)_elementAttributes.get(nextNode)).
		put("label", "\""
		    + String.valueOf(_graph.nodeLabel(nextNode)) + "\"");
        }
        //Create a hash map for each edge to prevent null pointer
        //exceptions.
        for(Iterator edges = _graph.edges().iterator(); edges.hasNext(); ) {
            Edge nextEdge = (Edge) edges.next();
            _elementAttributes.put(nextEdge, new HashMap());
        }
    }

    public DotGenerator(DIFGraph g) {
	DIFGraph graph = g;
        _clusterAttributes = new HashMap();
        _clusters = new Vector();
        _extraLines = new StringBuffer();
        _elementAttributes = new HashMap();
        _difGraph = (DIFGraph)graph.clone();
	_graph = (Graph)_difGraph;
        _isDirected = false;
        _name = "_difGraph";
        //Create a hash map for each node and put set label attribute to the
        //node label.
        for(Iterator nodes = g.nodes().iterator(); nodes.hasNext(); ) {
            Node nextNode = (Node) nodes.next();

	    LinkedList attributes = _difGraph.getAttributes(nextNode);

	    _elementAttributes.put(nextNode, new HashMap());

	    boolean found = false;
	    if (attributes.size() != 0) {
		for (int i=1; i<attributes.size(); i++) {
		    DIFAttribute attribute = (DIFAttribute)attributes.get(i);

		    if (attribute.getName().equals("ic")){
			Integer k = (Integer)attribute.getValue();
			((HashMap)_elementAttributes.get(nextNode)).
			    put("label", k );
			found = true;
		    }
		}
	    } else {
		//		System.out.println("WARNING: DotGenerator: no attributes found for:" + g.getName(nextNode));
	    }

	    if(!found) {
		((HashMap)_elementAttributes.get(nextNode)).
		    //WLP - seems like this should be a getName instead of the less used nodeLabel
		    put("label", "\""
                        + String.valueOf(graph.getName(nextNode)) + "\"");
			//                        + String.valueOf(graph.nodeLabel(nextNode)) + "\"");
	    }
        }
        //Create a hash map for each edge to prevent null pointer
        //exceptions.
        for(Iterator edges = _difGraph.edges().iterator(); edges.hasNext(); ) {
            Edge nextEdge = (Edge) edges.next();
            _elementAttributes.put(nextEdge, new HashMap());
        }
    }


    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Inserts a line to before the dot description in a graph. This can be
     *  used to
     *  add attributes to the graph such as <code>label = "graph_name";
     *  </code>. Semicolons should be included at the end of the line.
     *  @param line Line to be added to the dot description of the graph.
     *  A line feed will automatically be added to the end of the line.
     */
    public void addLine(String line) {
        _extraLines.append(line + "\n");
    }

    /** Returns true if this node is defined within a cluster previously.
     *  @param node A graph node.
     */
    public boolean isClusterNode(Node node) {
        for(Iterator clusters = _clusters.iterator(); clusters.hasNext(); ) {
            Collection cluster = (Collection)clusters.next();
            if(cluster.contains(node)) {
                return true;
            }
        }
        return false;
    }

    /** Sets the DotGenerator object to represent the graph as directed.
     *  Default value for this option is false (undirected).
     *  @return Previous value of the option.
     */
    public boolean setAsDirected(boolean directed) {
        boolean returnValue = _isDirected;
        _isDirected = directed;
        return returnValue;
    }

    /** Sets an attribute in the cluster. This is similar to the way that
     *  {@link #addLine} works.
     *  @param clusterNodes Cluster for which the attribute is going to be set.
     *  @param line Attribute string
     *  @return False if the list of nodes could not be found as a cluster.
     */
    public boolean setAttribute(Collection clusterNodes, String line) {
        if(_clusterAttributes.get(clusterNodes) != null) {
            ((StringBuffer)_clusterAttributes.get(clusterNodes))
                .append("    " + line + "\n");
            return true;
        }
        return false;
    }

    /** Sets an attribute for a node in the graph. For possible attributes and
     *  values consult to GraphViz documentation. Note that if an attribute
     *  value should include quotes they should be manually placed in the
     *  string. If a value is not needed for this attribute use null.<p>
     *  ex: <code>setAttribute(graphNode,"label","\"my_node\"")</code><p>
     *  This method is case sensitive for attributes and values.
     *  @param graphElement A node or edge of the graph to set an attribute
     *  for.
     *  @param attribute Attribute that is going to be set.
     *  @param value Value for the attribute. Use null if there is no need for
     *  a value.
     *  @return The previous value of this attribute if it exists, null
     *  otherwise.
     */
    public String setAttribute(Element graphElement, String attribute,
            String value) {
        _checkElement(graphElement, attribute, value);
        return (String)((HashMap)_elementAttributes.get(graphElement))
            .put(attribute, value);
    }

    /** Sets a group of nodes as a cluster. These nodes will be defined as a
     *  sub-group in the dot definition. This method will do nothing and return
     *  false in two cases: At least one of the nodes is not contained in
     *  the graph or at least one of the nodes is defined to be in another
     *  cluster. Note that once run this command can't be undone.
     *  @param clusterNodes A collection of nodes.
     *  @return False if there is an error.
     */
    public boolean setCluster(Collection clusterNodes) {
        for(Iterator nodeIterator = clusterNodes.iterator();
            nodeIterator.hasNext(); ) {
            Node node = (Node)nodeIterator.next();
            if(!_graph.containsNode(node) || isClusterNode(node)) {
                return false;
            }
        }
        _clusters.add(clusterNodes);
        _clusterAttributes.put(clusterNodes, new StringBuffer());
        setAttribute(clusterNodes, "label = \"\";");
        return true;
    }

    /** Sets the name for this graph and adds a line to the graph definition to
     *  print this name to GraphViz. This method is useful when multiple graph
     *  definitions are put in a single file.
     *  @param name Name of the graph. It shouldn't include white space or
     *  punctuation.
     *  @return Previous value of the name.
     */
    public String setGraphName(String name) {
        String returnValue = _name;
        _name = name;
        addLine("label = \"" + _name + "\";");
        return returnValue;
    }

    /** Creates <code>fileName.dot</code> from the DotGenerator object.
     *  @param fileName Name of the dot file to be created. A ".dot" extension
     *  will be appended to this name.
     *  @exception IOException If the file could not be created.
     */
    public void toFile(String fileName) throws IOException {
        FileOutputStream fileStream = new FileOutputStream(fileName + ".dot");
        PrintWriter out = new PrintWriter(fileStream);
        out.print(toString());
        out.close();
        fileStream.close();
    }

    /** Creates <code>fileName.dot</code> from the given graph.
     *  @param graph A Graph object.
     *  @param fileName Name of the dot file to be created. A ".dot" extension
     *  will be appended to this name.
     *  @param isDirected True if the graph is directed, false otherwise.
     *  @exception IOException If the file could not be created.
     */
    public static void toFile(Graph graph, String fileName,
            boolean isDirected) throws IOException {
        DotGenerator dotBuffer = new DotGenerator(graph);
        dotBuffer.setAsDirected(isDirected);
        dotBuffer.toFile(fileName);
    }

    //NS: Modified to be consistent with mocgraph changes. 08/07/09
    /** Creates "dot" code string of the DotGenerator object.
     *  @return The "dot" code.
     */
    public String toString() {
        StringBuffer dotStringBuffer = new StringBuffer();
        dotStringBuffer.append("//Dot file created by DotGenerator\n"
                + ((_isDirected) ? "digraph " : "graph ") + _name + " {\n"
                + "// Extra lines\n"
                + _extraLines
                + "  // Vertices\n");
        for(Iterator nodes = _graph.nodes().iterator(); nodes.hasNext();) {
            Node nextNode = (Node) nodes.next();
            HashMap attributeValues = ((HashMap)_elementAttributes
                    .get(nextNode));
	    //	    System.out.println(_elementAttributes);
            if(isClusterNode(nextNode)) {
                continue;
            }
            dotStringBuffer.append("  "
                    + _graph.nodeLabel(nextNode) + " [" );

            Object[] attributes = attributeValues.keySet().toArray();
            Arrays.sort(attributes);

	    //	    System.out.println( _graph.nodeLabel(nextNode) + " = " + attributes[0]);
	    //System.out.println(attributeValues);
            for(int i = 0; i < attributes.length; i++) {
                String attribute = (String) attributes[i];
                //Skip setting a value if value is null.
                if((String)attributeValues.get(attribute) == null)
                    dotStringBuffer.append(attribute + ",");
                else
                    dotStringBuffer.append(attribute
                            + "=" + attributeValues.get(attribute) + ",");
            }
            dotStringBuffer.append("];\n");
        }
        dotStringBuffer.append("  // Edges\n");

	//If we're printing an ordered tree, need to control the fanout
	/* if(_graph instanceof ScheduleTree) {
	    dotStringBuffer.append("  // (printing ordered tree) \n");
	    ScheduleTree st = (ScheduleTree)_graph;
	    ScheduleTreeNode stn = (ScheduleTreeNode)st.getRoot();
	    if(stn != null) {
		//		stn = (ScheduleTreeNode)stn.getFirstChild();
		stn = (ScheduleTreeNode)stn.getChildFirst();

		while((stn != st.getRoot()) && (stn != null)) {
		    //Get the edge info first
		    //System.out.println(" yup: " + stn);
		    //		    System.out.println(" <-- " + stn.getParent());
		    Node node = _graph.node(stn);
		    Node parent = _graph.node(stn.getParent());
		    //		    Collection edgeCol = _graph.neighborEdges(stn, stn.getParent());
		    Collection edgeCol = _graph.neighborEdges(node, parent);
		    Object[] edges = edgeCol.toArray();
		    Edge edge = (Edge)edges[0];
		    HashMap attributeValues = ((HashMap)_elementAttributes.get(edge));
		    int sourceLabel = _graph.nodeLabel(edge.source());
		    int sinkLabel = _graph.nodeLabel(edge.sink());
		    dotStringBuffer.append("  " + sourceLabel +
					   ((_isDirected) ? " -> " : " -- ") + sinkLabel + " [");
		    Object[] attributes = attributeValues.keySet().toArray();
		    Arrays.sort(attributes);
		    for(int i = 0; i < attributes.length; i++) {
			String attribute = (String) attributes[i];
			//Skip setting a value if value is null.
			if((String)attributeValues.get(attribute) == null)
			    dotStringBuffer.append(attribute + ",");
			else
			    dotStringBuffer.append(attribute
						   + "=" + attributeValues.get(attribute) + ",");
		    }
		    dotStringBuffer.append("];\n");

		    //Now traverse
		    if (stn.isLeaf()) {
			if(stn.isLastChild()) {
			    stn = (ScheduleTreeNode)stn.getParent();
			    while(stn.isLastChild() && (stn != st.getRoot())) {
				stn = (ScheduleTreeNode)stn.getParent();
			    }
			}
			if(stn != st.getRoot()) {
			    // stn = (ScheduleTreeNode)stn.getRightSibling();
			    stn = (ScheduleTreeNode)st.getRightSibling(stn);
			}
		    } else { // we need to get a child
			//			stn = (ScheduleTreeNode)stn.getFirstChild();
			stn = (ScheduleTreeNode)stn.getChildFirst();
		    }
		}
	    } else {
		System.out.println("WARNING: null root in DotGenerator for : " + st);
	    }
	} else { */
	    for(Iterator edges = _graph.edges().iterator(); edges.hasNext(); ) {
		Edge edge = (Edge) edges.next();
		HashMap attributeValues = ((HashMap)_elementAttributes.get(edge));
		int sourceLabel = _graph.nodeLabel(edge.source());
		int sinkLabel = _graph.nodeLabel(edge.sink());
		dotStringBuffer.append("  " + sourceLabel +
				       ((_isDirected) ? " -> " : " -- ") + sinkLabel + " [");
		Object[] attributes = attributeValues.keySet().toArray();
		Arrays.sort(attributes);
		for(int i = 0; i < attributes.length; i++) {
		    String attribute = (String) attributes[i];
                //Skip setting a value if value is null.
		    if((String)attributeValues.get(attribute) == null)
			dotStringBuffer.append(attribute + ",");
		    else
			dotStringBuffer.append(attribute
					       + "=" + attributeValues.get(attribute) + ",");
		}
		dotStringBuffer.append("];\n");
	    }
	//}
        // Clusters.
        int count = 0;
        for(Iterator clusters = _clusters.iterator(); clusters.hasNext();
            count++ ) {
            Collection cluster = (Collection)clusters.next();
            dotStringBuffer.append("  subgraph cluster" + count + " {\n");
            dotStringBuffer
                .append((StringBuffer)_clusterAttributes.get(cluster));
            for(Iterator nodes = cluster.iterator();
                nodes.hasNext(); ) {
                Node nextNode = (Node)nodes.next();
                HashMap attributeValues =
                    ((HashMap)_elementAttributes.get(nextNode));
                dotStringBuffer.append("    "
                        + _graph.nodeLabel(nextNode) + " [" );
                Object[] attributes = attributeValues.keySet().toArray();
                Arrays.sort(attributes);
                for(int i = 0; i < attributes.length; i++) {
                    String attribute = (String) attributes[i];
                    //Skip setting a value if value is null.
                    if((String)attributeValues.get(attribute) == null)
                        dotStringBuffer.append(attribute + ",");
                    else
                        dotStringBuffer.append(attribute
                                + "=" + attributeValues.get(attribute) + ",");
                }
                dotStringBuffer.append("];\n");
            }
            dotStringBuffer.append("  }\n");
        }
        dotStringBuffer.append("}\n");
        return dotStringBuffer.toString();
    }

    ///////////////////////////////////////////////////////////////////
    ////                     protected variables                   ////

    /** Clone of the original graph given in the constructor.
     *  @see Graph#clone
     */
    protected DIFGraph _difGraph;
    protected Graph _graph;

    /** Name of this graph.
     */
    protected String _name;

    /** HashMap: <code>Element -> (AttributeName -> AttributeValue)</code>
     */
    protected HashMap _elementAttributes;

    /** HashMap: <code>Collection cluster -> StringBuffer</code>
     */
    protected HashMap _clusterAttributes;

    /** Each element is a Collection of Nodes. Purpose of this field is to
     *  keep the definition order of clusters for deterministic output.
     */
    protected Vector _clusters;

    /** Extra lines/attributes to be added to a graph description before
     *  topology definitions.
     */
    protected StringBuffer _extraLines;

    /** Should be set to true if the graph is directed.
     */
    protected boolean _isDirected;

    ///////////////////////////////////////////////////////////////////
    ////                     private methods                       ////

    // Check for the existence of an element in the graph. Throw an exception
    // if not found.
    // @exception IllegalArgumentException If the element is not found in
    // _graph.
    private void _checkElement(Element element, String attribute,
            String value) {
        if((element instanceof Node && !_graph.containsNode((Node)element)) ||
                (element instanceof Edge &&
                        !_graph.containsEdge((Edge)element))) {
            if(attribute == null && value == null) {
                throw new IllegalArgumentException(
                        "Element not found in the graph:\n" +
                        GraphException.elementDump(element, _graph));
            } else {
                throw new IllegalArgumentException(
                        "Element not found in the graph while setting"
                        + " " + attribute + " to " + value + ".\n"
                        + GraphException.elementDump(element, _graph));
            }
        }
    }
}


