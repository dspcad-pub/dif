/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.graph;

import java.util.List;

import dif.util.graph.analysis.analyzer.GraphAnalyzer;

//////////////////////////////////////////////////////////////////////////
//// MinimumSpanningTreeAnalyzer
/**
Base interface for the computation of minimum spanning trees in a graph.
<p>
@see mocgraph.analysis.MinimumSpanningTreeAnalysis
@since Ptolemy II 2.0
@author Shuvra S. Bhattacharyya
@version $Id: MinimumSpanningTreeAnalyzer.java 606 2012-10-08 16:29:47Z plishker $
*/

public interface MinimumSpanningTreeAnalyzer extends GraphAnalyzer {

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Return the edges of a minimal spanning tree.
     *  Each element of the list is an {@link Edge}.
     *
     *  @return The edges in a minimal spanning tree.
     */
     public List edges();

    /** Return the weight (sum of edge weights) of the minimum spanning
     *  tree that is computed.
     *  @return The weight of the minimum spanning tree.
     */
    public double weight();
}


