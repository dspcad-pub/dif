/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.graph;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import dif.util.graph.DirectedAcyclicGraph;
import dif.util.graph.Graph;
import dif.util.graph.Element;
import dif.util.graph.Node;

import dif.graph.hierarchy.Hierarchy;
import dif.graph.hierarchy.HierarchyException;
import dif.graph.hierarchy.Port;

//////////////////////////////////////////////////////////////////////////
//// HierarchyToDot
/** DOT file generator for Hierarchy objects. It is used to create dot files as
an input to GraphViz tools. A dot file is created by first defining an
Hierarchy object and then using the {@link #toFile} method. Underlying
hierarchy object is cached during the constructor call and cannot be changed
afterwards.
@author Fuat Keceli
@version $Id: HierarchyToDot.java 606 2012-10-08 16:29:47Z plishker $
*/

public class HierarchyToDot {
    protected HierarchyToDot() {
    }

    /** Creates a DotGenerator object from a Hierarchy object.
     *  @param hierarchy Original hierarchy from which this generator is
     *  created. It is cached during constructor call.
     *  @param flattenSet Runs flatten on the given set of nodes and draws them
     *  as clusters. This parameter can be left null.
     *  @param useElementNames If true, {@link #_elementName} method will be
     *  used to get element names of the graph. Otherwise node/edge labels in
     *  {@link Graph} will be used.
     */
    public HierarchyToDot(Hierarchy hierarchy, Collection flattenSet,
            boolean useElementNames) {
        try {
            _hierarchy = hierarchy.mirror(false);
        } catch(HierarchyException exception) {
            throw new HierarchyException("Error(s) has been found in the"
                    + " source hierarchy.\nCorrect this first:\n"
                    + exception.getMessage());
        }
        Graph graph = _hierarchy.getGraph();
        Vector clusterSet = new Vector();
        HashMap clusterNames = new HashMap();
        if(flattenSet != null) {
            // Since we are using the mirror of the original graph flattenSet
            // doesn't match in the clone so find equivalent flatten node set.
            Vector flattenNodes = new Vector();
            for(Iterator nodes = flattenSet.iterator(); nodes.hasNext() ; ) {
                Node node = (Node)nodes.next();
                Node mirrorNode =
                    graph.node(hierarchy.getGraph().nodeLabel(node));
                if(!flattenNodes.contains(mirrorNode)) {
                    flattenNodes.add(mirrorNode);
                }
            }
            for(Iterator nodes = flattenNodes.iterator(); nodes.hasNext(); ) {
                Node node = (Node)nodes.next();
                Hierarchy subHierarchy = _hierarchy.flatten(node);
                Collection cluster = subHierarchy.getGraph().nodes();
                clusterSet.add(cluster);
                clusterNames.put(cluster, subHierarchy.getName());
            }
        }
        // This has to be created after flatten since graph is cached in
        // DotGenerator.
        _dotGenerator = new DotGenerator(graph);
        StringBuffer labelBuffer = new StringBuffer();
        labelBuffer.append(_hierarchy.getName());
        if(_hierarchy.getPorts().getAll().size() != 0) {
            labelBuffer.append("\\nPorts are:\\n");
        }
        Port[] ports = (Port[])_hierarchy.getPorts().getAll()
            .toArray(new Port[0]);
        for(int i = 0; i < ports.length; i++) {
            Port port = ports[i];
            Node portNode = ports[i].getNode();
            Port relatedPort = ports[i].getRelatedPort();
            labelBuffer.append(port.getName());
            String elementString = null;
            if(relatedPort == null && portNode != null) {
                if(useElementNames) {
                    elementString = _elementName((Element)portNode);
                } else {
                    elementString = graph.nodeLabel((Node)portNode) + "";
                }
            } else if(relatedPort != null) {
                elementString = (String)relatedPort.getName();
            }
            if(port.getDirection() != 0) {
                labelBuffer.append((port.getDirection() < 0) ?
                        "(IN)" : "(OUT)");
            }
            labelBuffer.append(((elementString != null) ?
                                       (":" + elementString) : "") + " ");
        }
        if(_hierarchy.getSuperNodes().getNodes().size() != 0) {
            labelBuffer.append("\\nSuper nodes are:" + "\\n");
        }
        for(Iterator nodes = _hierarchy.getSuperNodes().iterator();
            nodes.hasNext(); ) {
            Node nextNode = (Node)nodes.next();
            if(useElementNames) {
                labelBuffer.append(_elementName(nextNode)+ " ");
            } else {
                labelBuffer.append(graph.nodeLabel(nextNode) + " ");
            }
        }
        for(Iterator clusters = clusterSet.iterator();
            clusters.hasNext(); ) {
            Collection cluster = (Collection)clusters.next();
            _dotGenerator.setCluster(cluster);
            _dotGenerator.setAttribute(cluster,
                    "label = \"" + clusterNames.get(cluster) + "\"");
        }
        if(useElementNames) {
            for(Iterator nodes = graph.nodes().iterator(); nodes.hasNext(); ) {
                Node nextNode = (Node) nodes.next();
                _dotGenerator.setAttribute(nextNode,
                        "label", "\"" + _elementName(nextNode) + "\"");
            }
        } else {
            for(Iterator nodes = graph.nodes().iterator(); nodes.hasNext(); ) {
                Node nextNode = (Node) nodes.next();
                _dotGenerator.setAttribute(nextNode, "label", "\""
                        + graph.nodeLabel(nextNode) + "\"");
            }
        }
        _dotGenerator.addLine("label = " + "\"" + labelBuffer + "\";");
        _dotGenerator.addLine("node[shape = circle];");
        _dotGenerator.addLine("center = true;");
    }

    ///////////////////////////////////////////////////////////////////
    ////                       public methods                      ////

    /** Draw the hierarchy graph starting from this hierarchy.
     *  The hierarchy graph should be obtained using the
     *  {@link Hierarchy#hierarchyGraph} method.
     *  @param hierarchy The top level hierarchy.
     *  @return The dot generator object.
     *  @exception CyclicHierarchyException If hierarchy structure is found to
     *  be cyclic. The relation causing this exception can be obtained via
     *  a method in <code>CyclicHierarchyException</code>.
     *  @see Hierarchy#hierarchyGraph
     *  @see DotGenerator
     */
    public static DotGenerator hierarchyGraphToDot(Hierarchy hierarchy) {
        DirectedAcyclicGraph hierarchyGraph = hierarchy.hierarchyGraph();
        DotGenerator dot = new DotGenerator(hierarchyGraph);
        for(Iterator nodes = hierarchyGraph.nodes().iterator();
            nodes.hasNext(); ) {
            Node node = (Node)nodes.next();
            String name = ((Hierarchy)node.getWeight()).getName();
            dot.setAttribute(node, "label", "\"" + name + "\"");
        }
        dot.addLine("label = \" Starting from " + hierarchy.getName() + "\";");
        dot.addLine("center = true;");
        dot.setAsDirected(true);
        return dot;
    }

    /** Creates a "dot" file from the given hierarchy.
     *  @param fileName Name of the dot file to be created. A ".dot" extension
     *  will be appended to this name.
     *  @exception IOException If file cannot be created.
     */
    public void toFile(String fileName) throws IOException {
        if(_hierarchy.isDirected()) {
            _dotGenerator.setAsDirected(true);
        }
        _dotGenerator.toFile(fileName);
    }

    /** Creates a Dot string from the given hierarchy.
     *  @return A "dot" string representation of the hierarchy.
     */
    public String toString() {
        if(_hierarchy.isDirected()) {
            _dotGenerator.setAsDirected(true);
        }
        return _dotGenerator.toString();
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /** Returns the name of an element if it is stored in a special way. This
     *  implementation assumes the weight of the graph element is a
     *  String and uses it as the name. Other implementations can change this
     *  assumption.
     *  @param element An element (node or an edge) included in the backing
     *  graph of the hierarchy.
     *  @return Weight of the element printed as a String using its toString
     *  method.
     */
    protected String _elementName(Element element) {
        return element.getWeight().toString();
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected variables                 ////

    /** Cached hierarchy object.
     */
    protected Hierarchy _hierarchy;

    /** Dot generator object.
     */
    protected DotGenerator _dotGenerator;
}


