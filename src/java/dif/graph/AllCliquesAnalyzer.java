/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.graph;

import java.util.Collection;

import dif.util.graph.analysis.analyzer.GraphAnalyzer;

//////////////////////////////////////////////////////////////////////////
//// AllCliquesAnalyzer
/**
Base interface for finding all cliques.
<p>
@author Ming-Yung Ko
@version $Id: AllCliquesAnalyzer.java 606 2012-10-08 16:29:47Z plishker $
*/

public interface AllCliquesAnalyzer extends GraphAnalyzer {

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Return the collection of all cliques. Each element is a collection
     *  of clique (completely connected) nodes.
     *
     *  @return The collection of cliques.
     */
    public Collection cliques();

    /** Return the maximal cliques. A maximal clique is a clique not properly
     *  contained into another one. See also {@link #cliques()}
     *
     *  @return The collection of maximal cliques. Each element contains a
     *          maximal clique.
     */
    public Collection maximalCliques();

}


