/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.graph;

import dif.util.graph.Graph;
import dif.util.graph.Node;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

//////////////////////////////////////////////////////////////////////////
//// Graphs.
/**
Miscellaneous utilities for graphs.
<p>
@see Graph
@author Shuvra S. Bhattacharyya, Ming-Yung Ko
@version $Id: Graphs.java 606 2012-10-08 16:29:47Z plishker $
*/

public class Graphs {

    // Private constructor to prevent instantiation of the class.
    private Graphs() {
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Add edges among the given nodes in the graph such that these nodes
     *  are fully connected.
     *
     *  @param graph The graph to add the edges.
     *  @param nodes The nodes to which edges fully connect.
     *  @return The edges to add.
     */
    public static Collection addFullyConnectedEdges(
            Graph graph, Collection nodes) {

        Collection edges = new ArrayList();
        int nodeCount = nodes.size();
        List nodeList = new ArrayList(nodes);
        for (int i = 0; i < nodeCount - 1; i++) {
            for (int j = i + 1; j < nodeCount; j++) {
                Node nodeI = (Node)nodeList.get(i);
                Node nodeJ = (Node)nodeList.get(j);
                if (graph.neighborEdges(nodeI, nodeJ).isEmpty())
                    edges.add(graph.addEdge(nodeI, nodeJ));
            }
        }
        return edges;
    }

    /** Return disjoint components that is clique-decomposed from the graph.
     *  @param graph The graph.
     *  @return The clique components.
     */
    public static Collection cliqueComponents(Graph graph) {
        Collection components = new ArrayList();
        Collection nodesTraversed = new ArrayList();
        // Find all maximal cliques.
        AllCliquesAnalysis cliquesAnalysis = new AllCliquesAnalysis(
                new BacktrackingAllCliquesStrategy(graph));
        Iterator maxCliques = cliquesAnalysis.maximalCliques().iterator();
        while (maxCliques.hasNext()) {
            Collection component = new ArrayList((Collection)maxCliques.next());
            // Remove those nodes already stored in another component.
            component.removeAll(nodesTraversed);
            components.add(component);
            nodesTraversed.addAll(component);
        }
        return components;
    }

    /** Return a complement of the argument graph. A complement graph has
     *  all the edges not present in the original graph.
     *
     *  @param graph The original graph.
     *  @return The complement graph.
     */
    public static Graph complementGraph(Graph graph) {
        Graph complementGraph = new Graph();
        complementGraph.addNodes(graph.nodes());

        for (int i = 0; i < graph.nodeCount(); i++)
            for (int j = i + 1; j < graph.nodeCount(); j++) {
                Node nodeI = graph.node(i);
                Node nodeJ = graph.node(j);
                if (graph.neighborEdges(nodeI, nodeJ).isEmpty())
                    complementGraph.addEdge(nodeI, nodeJ);
            }

        return complementGraph;
    }

    /** Display a collection of node components in text. A node component
     *  is a collection of nodes. Node components is displayed per line with
     *  the composed nodes' labels.
     *
     *  @param graph The graph containing the nodes.
     *  @param components The collection of node components.
     *  @return The string to display the node components.
     */
    public static String displayComponents(Graph graph, Collection components) {
        String display = new String();
        Iterator componentsIterator = components.iterator();
        while (componentsIterator.hasNext()) {
            display += "{ ";
            Iterator nodes = ((Collection)componentsIterator.next()).iterator();
            while (nodes.hasNext()) {
                Node node = (Node)nodes.next();
                display += graph.nodeLabel(node);
                if (nodes.hasNext())
                    display += " ";
            }
            display += " }\n";
        }
        return display;
    }

    /** Return true if the given graph is connected. If the graph contains
     *  no nodes return true.
     *
     *  @param graph The graph.
     *  @return True if the given graph is connected.
     */
     public static boolean isConnected(Graph graph) {

         if (graph.nodeCount() == 0) {
             return true;
         }
         Collection connectedComponents = graph.connectedComponents();
         return (connectedComponents.size() == 1);
     }
}


