/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.graph;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import dif.util.graph.DirectedGraph;
import dif.util.graph.Graph;
import dif.util.graph.Node;

//////////////////////////////////////////////////////////////////////////
//// PartialTopSortStrategy
/**
A topological sorting approach by building partial orders.
One advantage of partial ordering is that optimization at later stage can make
better decision based on the partial order.

@see TopSortAnalysis
@author Ming-Yung Ko
@version $Id: PartialTopSortStrategy.java 606 2012-10-08 16:29:47Z plishker $
*/
public class PartialTopSortStrategy extends BaseTopSortStrategy {

    /** Construct a top sorting strategy for a given graph.
     *  @param graph The given graph.
     */
    public PartialTopSortStrategy(Graph graph) {
        super(graph);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /** Compute a topological sorting order. The result is
     *  returned as a <code>List</code>, where the order begins at index 0.
     *
     *  @return A topological sorting order.
     */
    protected Object _compute() {
        _validate();
        return _partialTopSort();
    }

    /** Return the next source node in determining the top sort.
     *  The next source node candidate is determined by the top sort computed
     *  so far as well the candidate sources to append to the parial order.
     *  This method is supposed to be overriden by specific source selection
     *  approach.
     *
     *  @param partialOrder The partial top sort computed so far.
     *  @param sources The candidate sources to append to the partial order.
     *  @return The next source node.
     */
    protected Node _nextSource(List partialOrder, Collection sources) {
        return (Node)sources.iterator().next();
    }

    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////

    /*  The actual partial order topological sorting implementation.
     *
     *  @param graph The graph to compute topological sorting.
     *  @return The topological sorting order.
     */
    private List _partialTopSort() {
        List topSort = new LinkedList();
        DirectedGraph remainedGraph = (DirectedGraph)directedGraph().clone();
        while (!remainedGraph.nodes().isEmpty()) {
            Node nextSource = _nextSource(topSort, remainedGraph.sourceNodes());
            topSort.add(nextSource);
            remainedGraph.removeNode(nextSource);
        }
        return topSort;
    }

}


