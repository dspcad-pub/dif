/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.graph;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import dif.util.graph.DirectedGraph;
import dif.util.graph.Graph;
import dif.util.graph.Node;
import dif.util.graph.analysis.strategy.CachedStrategy;

//////////////////////////////////////////////////////////////////////////
//// DFSAllTopSortsStrategy
/**
A depth-first-search approach for finding all topological sorting orders.

@see AllTopSortsAnalysis
@author Ming-Yung Ko
@version $Id: DFSAllTopSortsStrategy.java 606 2012-10-08 16:29:47Z plishker $
*/
public class DFSAllTopSortsStrategy
        extends CachedStrategy implements AllTopSortsAnalyzer {

    /** Constructor with an input graph.
     *  @param graph The given graph.
     */
    public DFSAllTopSortsStrategy(Graph graph) {
        super(graph);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Return a description of the analysis in finding all top sorting orders.
     *  @return A description of the analysis.
     */
    public String toString() {
        String result = "Finding all topological sorting orders "
                + "for the following graph.\n" + graph().toString();
        result += "The orders are:\n";
        Iterator sorts = topSorts().iterator();
        while (sorts.hasNext()) {
            List sort = (List)sorts.next();
            result += _topSortToString(sort) + "\n";
        }
        return result;
    }

    /** Return the collection of all topological sorting orders. Each element
     *  is an order in <code>List</code>.
     *
     *  @return The collection of topological sorting orders.
     */
    public Collection topSorts() {
        return (Collection)_result();
    }

    /** Check compatibility of the class of graph. The given graph
     *  must be an instance of {@link DirectedGraph} and have
     *  acyclic structure.
     *
     *  @return True if the given graph is a directed acyclic graph (DAG).
     */
    public boolean valid() {
        if ((graph().nodeCount() == 0)
                || !(graph() instanceof DirectedGraph)
                || !((DirectedGraph)graph()).isAcyclic())
            return false;
        return true;
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /** Compute all topological sorting orders. The result is
     *  returned as a <code>Collection</code>, where each element is a
     *  topological sorting order in <code>List</code>.
     *
     *  @return All topological sorting orders.
     */
    protected Object _compute() {
        if (!valid())
            throw new RuntimeException("Error in computing "
                    + "topological sorting orders: The target graph is either "
                    + "empty or not a directed acyclic graph (DAG).");
        return _topSorts((DirectedGraph)graph());
    }

    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////

    /*  A depth-first search approach in finding all topological sorting orders.
     *  @param graph The target graph.
     *  @return All the topological sorting orders in <code>List</code>.
     */
    private List _topSorts(DirectedGraph graph) {
        List allSorts = new LinkedList();

        // Stop condition: Graph with 1 node only.
        if (graph.nodeCount() == 1) {
            List sort = new LinkedList();
            sort.add(graph.node(0));
            allSorts.add(sort);

        } else {
            Iterator sources = graph.sourceNodes().iterator();
            while (sources.hasNext()) {
                Node source = (Node)sources.next();

                // Compute a subgraph with the source removed.
                Collection subNodes = new LinkedList(graph.nodes());
                subNodes.remove(source);
                DirectedGraph subGraph = (DirectedGraph)
                        graph.subgraph(subNodes).cloneAs(new DirectedGraph());

                // Append the source node as the first top-sort element
                // to all top-sort orderings of the subgraph.
                List subTopSorts = _topSorts(subGraph);
                for (int i = 0; i < subTopSorts.size(); i++) {
                    LinkedList subTopSort = (LinkedList)subTopSorts.get(i);
                    subTopSort.addFirst(source);
                }

                // Store top-sorts for the present source and continue with
                // the next source.
                allSorts.addAll(subTopSorts);
            }
        }
        return allSorts;
    }

    /*  Display the input topological sorting order in text. Nodes are
     *  displayed as the associated node labels.
     *
     *  @param topSort The topological sorting order.
     *  @return A string representing the order.
     */
    private String _topSortToString(List topSort) {
        String sortString = new String();
        sortString += graph().nodeLabel((Node)topSort.get(0));
        for (int i = 1; i < topSort.size(); i++)
            sortString += " " + graph().nodeLabel((Node)topSort.get(i));

        return sortString;
    }
}


