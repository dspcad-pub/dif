/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.graph;
import dif.DIFGraph;

import dif.util.sched.ScheduleTree;

//////////////////////////////////////////////////////////////////////////
//// DIFToDot
/** DOT file generator for DIFGraph objects. It is used to create dot files as
an input to GraphViz tools. A DOT file is created by first defining an
DIFToDot object and then using the {@link #toFile} method. Node and edge labels
are set to element names in DIFGraph.
@author Fuat Keceli
@version $Id: DIFToDot.java 563 2012-03-04 16:29:44Z plishker $
*/

public class ScheduleTreeToDot extends DotGenerator {

    //NS: Modified to be consistent with mocgraph changes. 08/07/09
    /** Creates a DotGenerator object from a DIFGraph.
     *  @param graph A DIFGraph.
     */
    public ScheduleTreeToDot(ScheduleTree st, DIFGraph g) {
        super(st);
	//        addLine("label = " + "\"" + ((DIFGraph)_graph).getName() + "\";");
        addLine("node[shape = circle];");
        addLine("center = true;");
        addLine("edge[fontcolor = red];");
	addLine("ordering = out;");
	System.out.println(g);
        System.out.println("2015-10-03: This function cannot be compiled due to changes in DIF structure"); 
/*        for(Iterator graphNodes = _graph.nodes().iterator();
            graphNodes.hasNext(); ) {
	    //	    ScheduleTreeNode graphNode = (ScheduleTreeNode) graphNodes.next();
	    Node node = (Node)graphNodes.next();
	    ScheduleTreeNode graphNode = (ScheduleTreeNode)node.getWeight();
	    if(graphNode.getActor() != null) {
		//System.out.println(graphNode.getActor() + " = " + g.getName(graphNode.getActor()));
		//		setAttribute(graphNode, "label", g.getName(graphNode.getActor()));
		setAttribute(node, "label", g.getName(graphNode.getActor()));
	    } else {
		//		setAttribute(graphNode, "label", String.valueOf(graphNode.getLoopCount()));
		setAttribute(node, "label", String.valueOf(graphNode.getLoopCount()));

	    }
        }
*/
    }
}


