/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.graph;

import java.util.List;
import dif.util.graph.DirectedGraph;
import dif.util.graph.Graph;
import dif.util.graph.Node;
import dif.util.graph.analysis.strategy.CachedStrategy;

//////////////////////////////////////////////////////////////////////////
//// BaseTopSortStrategy
/**
A base class for topological sorting.
This class merely provides common public function calls and does not perform
topological sorting computation.

@see TopSortAnalysis
@author Ming-Yung Ko
@version $Id: BaseTopSortStrategy.java 606 2012-10-08 16:29:47Z plishker $
*/
public class BaseTopSortStrategy
        extends CachedStrategy implements TopSortAnalyzer {

    /** Construct a top sorting strategy for a given graph.
     *  @param graph The given graph.
     */
    public BaseTopSortStrategy(Graph graph) {
        super(graph);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Return the graph in the form of <code>DirectedGraph</code>.
     *  @return The directed graph.
     */
    public DirectedGraph directedGraph() {
        return (DirectedGraph)graph();
    }

    /** Return a description of the analysis in finding a top sorting order.
     *  @return A description of the analysis.
     */
    public String toString() {
        String result = "Finding a topological sorting order "
                + "for the following graph.\n" + graph().toString();
        result += "The order is:\n";

        List topSort = topSort();
        result += graph().nodeLabel((Node)topSort.get(0));
        for (int i = 1; i < topSort.size(); i++)
            result += " " + graph().nodeLabel((Node)topSort.get(i));
        result += "\n";
        return result;
    }

    /** Return a topological sorting order.
     *  @return A topological sorting order.
     */
    public List topSort() {
        return (List)_result();
    }

    /** Check compatibility of the class of graph. The given graph
     *  must be an instance of {@link DirectedGraph} and have
     *  acyclic structure.
     *
     *  @return True if the given graph is a directed acyclic graph (DAG).
     */
    public boolean valid() {
        boolean valid = true;
        if ((graph().nodeCount() == 0)
                || !(graph() instanceof DirectedGraph)
                || !(directedGraph()).isAcyclic())
            valid = false;
        return valid;
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /** Validate the graph for topological sorting. This method should be
     *  invoked right before the topological sorting computation in
     *  {@link CachedStrategy#_compute()}.
     *
     *  @exception RuntimeException will be thrown if {@link #valid()}
     *             returns false.
     */
    protected void _validate() {
        if (!valid())
            throw new RuntimeException("Error in computing "
                    + "topological sorting orders: The target graph is either "
                    + "empty or not a directed acyclic graph (DAG).");
    }
}


