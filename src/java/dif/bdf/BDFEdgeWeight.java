/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Information associated with a BDF edge. */

package dif.bdf;

import dif.CoreFunctionEdge;
import dif.DIFEdgeWeight;
import dif.util.Value;
import dif.util.graph.Edge;
import dif.util.graph.Graph;
import dif.util.graph.GraphWeightException;

//////////////////////////////////////////////////////////////////////////
//// BDFEdgeWeight
/** Information associated with an BDF edge.
BDFEdgeWeights are objects associated with {@link Edge}s
that represent BDF edges in {@link Graph}s.
This class caches frequently-used data associated with BDF edges.
<P>
The BDF edge production and consumption rate can have two types.
One is regular edge rate, which is represented in positive int or Integer.
The other one is boolean edge rate, which is represented as
[true rate, false rate] in int[].
<P>
@author Chia-Jui Hsu
@version $Id: BDFEdgeWeight.java 606 2008-10-08 16:29:47Z plishker $
@see Edge
*/

public class BDFEdgeWeight extends CoreFunctionEdge {

    /** Construct an edge weight for a regular, homogeneous, zero-delay edge.
     * Production and consumption values are set to 1.
     */
    public BDFEdgeWeight() {
        super();
        setSinkPort(null);
        setSourcePort(null);
        setBDFProductionRate(1);
        setBDFConsumptionRate(1);
        setDelay(new Integer(0));
    }

    /** Construct an edge weight for a specified token production rate,
     *  token consumption rate, and delay.
     *  @param productionRate The token production rate.
     *  @param consumptionRate The token consumption rate.
     *  @param delay The delay.
     */
    public BDFEdgeWeight(Object productionRate, Object consumptionRate, int delay) {
        super();
        setSourcePort(null);
        setSinkPort(null);
        setProductionRate(productionRate);
        setConsumptionRate(consumptionRate);
        setDelay(new Integer(delay));
    }

    /** Construct an edge weight for a specified source port, sink port,
     *  token production rate, token consumption rate, and delay.
     *  The source port and sink port are ports that correspond to this edge.
     *  @param sourcePort The source port.
     *  @param sinkPort The sink port.
     *  @param productionRate The token production rate.
     *  @param consumptionRate The token consumption rate.
     *  @param delay The delay.
     */
    public BDFEdgeWeight(Object sourcePort, Object sinkPort,
           Object productionRate, Object consumptionRate, int delay) {
        super();
        setSinkPort(sinkPort);
        setSourcePort(sourcePort);
        setProductionRate(productionRate);
        setConsumptionRate(consumptionRate);
        setDelay(new Integer(delay));
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Get the token consumption rate of the associated BDF edge.
     *  @return The consumption rate.
     */
    public Object getBDFConsumptionRate() {
        return _getConsumptionRate();
    }

    /** Get the token production rate of the associated BDF edge.
     *  @return The production rate.
     */
    public Object getBDFProductionRate() {
        return _getProductionRate();
    }

    /** Get the delay of the associated BDF edge.
     *  @return The delay.
     */
    public int getIntDelay() {
        return ((Integer)getDelay()).intValue();
    }

    /** Set the token consumption rate of a regular BDF edge.
     *  @param consumptionRate The token consumption rate in
     *  <code>int</code>
     *  @exception GraphWeightException If the consumptionRate is not
     *  positive.
     */
    public void setBDFConsumptionRate(int consumptionRate) {
        if (consumptionRate > 0) {
            _setConsumptionRate(new Integer(consumptionRate));
        } else {
            throw new GraphWeightException("Regular consumption rate "
                    + "should be positive.");
        }
    }

    /** Set the token consumption rate of a boolean BDF edge.
     *  @param consumptionRate The token consumption rate in
     *  <code>int[]</code>.
     *  @exception GraphWeightException If the length of consumptionRate is
     *  not 2.
     */
    public void setBDFConsumptionRate(int[] consumptionRate) {
        if (consumptionRate.length == 2 /*&& (
                (consumptionRate[0] == 0 && consumptionRate[1] > 0) ||
                (consumptionRate[0] > 0 && consumptionRate[1] == 0) ) */) {
            _setConsumptionRate(consumptionRate);
        } else {
            throw new GraphWeightException("Boolean consumption rate "
                    + "should be [true rate, false rate] in int[].");
        }
    }

    /** Set the token production rate of a regular BDF edge.
     *  @param productionRate The token production rate in
     *  <code>int</code>.
     *  @exception GraphWeightException If the productionRate is not
     *  positive.
     */
    public void setBDFProductionRate(int productionRate) {
        if (productionRate > 0) {
            _setProductionRate(new Integer(productionRate));
        } else {
            throw new GraphWeightException("Regular production rate "
                    + "should be positive.");
        }
    }

    /** Set the token production rate of a boolean BDF edge.
     *  @param productionRate The token production rate in
     *  <code>int[]</code>.
     *  @exception GraphWeightException If the length of productionRate is
     *  not 2.
     */
    public void setBDFProductionRate(int[] productionRate) {
        if (productionRate.length == 2/* && (
                (productionRate[0] == 0 && productionRate[1] > 0) ||
                (productionRate[0] > 0 && productionRate[1] == 0) ) */) {
            _setProductionRate(productionRate);
        } else {
            throw new GraphWeightException("Boolean productionRate rate "
                    + "should be [true rate, false rate] in int[].");
        }
    }

    /** Override {@link DIFEdgeWeight#setConsumptionRate(Object)}
     *  to check the type of input object, and call
     *  {@link #setBDFConsumptionRate(int[])} to set the boolean consumption
     *  rate or {@link #setBDFConsumptionRate(int)} to set the regular
     *  consumption rate.
     *  @param consumptionRate The token consumption rate.
     *  @exception GraphWeightException If the input object is not instanceof
     *  int[] or positive Integer.
     */
    public void setConsumptionRate(Object consumptionRate) {
        if (consumptionRate instanceof Integer) {
            if (((Integer)consumptionRate).intValue() > 0) {
                _setConsumptionRate(consumptionRate);
            } else {
                throw new GraphWeightException("Regular consumption rate "
                    + "should be positive.");
            }
        } else if (consumptionRate instanceof int[]) {
            setBDFConsumptionRate((int[])consumptionRate);
        } else {
            throw new GraphWeightException("Invalid consumptionRate type."
                    + "Please use setBDFConsumptionRate(int[]) "
                    + "or setBDFConsumptionRate(int).");
        }
    }

    /** Set delay of a BDF edge.
     *  @param delay The delay in <code>int</code>.
     */
    public void setDelay(int delay) {
	setDelay(new Integer(delay));
    }

    /** Override {@link DIFEdgeWeight#setProductionRate(Object)}
     *  to check the type of input object, and call
     *  {@link #setBDFProductionRate(int[])} to set the boolean consumption
     *  rate or {@link #setBDFProductionRate(int)} to set the regular
     *  consumption rate.
     *  @param productionRate The token production rate.
     *  @exception GraphWeightException If the input object is not instanceof
     *  int[] or positive Integer.
     */
    public void setProductionRate(Object productionRate) {
        if (productionRate instanceof Integer) {
            if (((Integer)productionRate).intValue() > 0) {
                _setProductionRate(productionRate);
            } else {
                throw new GraphWeightException("Regular production rate "
                    + "should be positive.");
            }
        } else if (productionRate instanceof int[]) {
            setBDFProductionRate((int[])productionRate);
        } else {
            throw new GraphWeightException("Invalid productionRate type."
                    + "Please use setBDFProductionRate(int[]) "
                    + "or setBDFProductionRate(int).");
        }
    }

    /** Return a string representation of the edge weight. This string
     *  representation is in the following form:
     *  <p>
     *  <em> productionRate consumptionRate delay </em>
     *  <p>
     * @return The string representation of the edge weight.
     */
    public String toString() {
        return new String(Value.toDIFString(getBDFProductionRate())
                + Value.toDIFString(getBDFConsumptionRate()) + getIntDelay());
    }

}


