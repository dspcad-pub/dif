/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Information associated with a BDF graph. */

package dif.bdf;

import dif.cfdf.CFDFGraph;
import dif.CoreFunctionNode;

import dif.util.graph.Node;
import dif.util.graph.Edge;
import dif.data.Fraction;
import dif.data.ExtendedMath;

import java.util.Iterator;
import java.util.HashMap;

//////////////////////////////////////////////////////////////////////////
//// BDFGraph
/** Information associated with an BDF graph.
This class caches frequently-used data associated with BDF graphs.
It also provides methods to calculate BDF related statistics.
<p>
BDFGraph nodes and edges have weights in {@link CoreFunctionNode} and
{@link BDFEdgeWeight}, respectively.
<p>
@author Chia-Jui Hsu
@version $Id: BDFGraph.java 606 2008-10-08 16:29:47Z plishker $
@see BDFEdgeWeight
@see CoreFunctionNode
*/

public class BDFGraph extends CFDFGraph {
    /** Construct an empty BDF graph.
     */
    public BDFGraph() {
        super();
    }

    /** Construct an empty BDF graph with enough storage allocated for the
     *  specified number of nodes.
     *  @param nodeCount The number of nodes.
     */
    public BDFGraph(int nodeCount) {
        super(nodeCount);
    }

    /** Construct an empty BDF graph with enough storage allocated for the
     *  specified number of edges, and number of nodes.
     *  @param nodeCount The integer specifying the number of nodes
     *  @param edgeCount The integer specifying the number of edges
     */
    public BDFGraph(int nodeCount, int edgeCount) {
        super(nodeCount, edgeCount);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Compute topologyMatrix without adjusting probabilities of boolean
     *  edges. topologyMatrix[edgeLabel][nodeLabel] is the rate which
     *  node(nodeLabel) produce or consump on the edge(edgeLabel).
     *  @return A pure topology matrix.
     */
    public int[][] computePureTopologyMatrix() {
        int nodeCount = nodeCount();
        int edgeCount = edgeCount();
        _pureTopologyMatrix = new int[edgeCount][nodeCount];

        for (int i=0; i<nodeCount; i++) {
            Iterator outEdges = outputEdges(node(i)).iterator();
            while (outEdges.hasNext()) {
                Edge edge = (Edge)outEdges.next();
                _pureTopologyMatrix[edgeLabel(edge)][i] =
                    _getProductionRate((BDFEdgeWeight)edge.getWeight());
            }

            Iterator inEdges = inputEdges(node(i)).iterator();
            while (inEdges.hasNext()) {
                Edge edge = (Edge)inEdges.next();
                _pureTopologyMatrix[edgeLabel(edge)][i] = -1*
                    _getConsumptionRate((BDFEdgeWeight)edge.getWeight());
            }
        }

        return _pureTopologyMatrix;
    }

    /** Get topologyMatrix without adjusting probabilities of boolean edges.
     *  @return A pure topology matrix.
     */
    /*public int[][] getPureTopologyMatrix() {
        return _pureTopologyMatrix;
    }*/

    /** Compute topologyMatrix and adjust boolean edge's rate with given
     *  probabilities.
     *  @return A statistical topology matrix.
     *  @exception RuntimeException If boolean edge does not connect to or
     *  from a boolean node or the probability of boolean node is not set.
     */
    public Fraction[][] computeStatisticTopologyMatrix() {
        int nodeCount = nodeCount();
        int edgeCount = edgeCount();

        _statisticTopologyMatrix = new Fraction[edgeCount][nodeCount];

        computePureTopologyMatrix();

        for (int j=0; j<edgeCount; j++) {
            for (int i=0; i<nodeCount; i++) {
                _statisticTopologyMatrix[j][i] =
                    new Fraction(_pureTopologyMatrix[j][i]);
            }
        }

        for (int j=0; j<edgeCount; j++) {
            Edge edge = edge(j);
            BDFEdgeWeight edgeWeight = (BDFEdgeWeight)edge.getWeight();
            if (edgeWeight.getBDFProductionRate() instanceof int[]) {
                int[] rate = (int[])edgeWeight.getBDFProductionRate();
                Node source = edge.source();
                CoreFunctionNode nodeWeight = (CoreFunctionNode)source.getWeight();
                if (nodeWeight.getTrueProbability() != null) {
                    if (rate[0] != 0) {
                        _statisticTopologyMatrix[edgeLabel(edge)][nodeLabel(
                            source)].multiply(nodeWeight.getTrueProbability());
                    } else {
                        _statisticTopologyMatrix[edgeLabel(edge)][nodeLabel(
                            source)].multiply(nodeWeight.getFalseProbability());
                    }
                } else {
                    throw new RuntimeException("Node " + getName(source)
                            + " is not a boolean node"
                            + " or its probability is not set.");
                }
            }
            if (edgeWeight.getBDFConsumptionRate() instanceof int[]) {
                int[] rate = (int[])edgeWeight.getBDFConsumptionRate();
                Node sink = edge.sink();
                CoreFunctionNode nodeWeight = (CoreFunctionNode)sink.getWeight();
                if (nodeWeight.getTrueProbability() != null) {
                    if (rate[0] != 0) {
                        _statisticTopologyMatrix[edgeLabel(edge)][nodeLabel(
                            sink)].multiply(nodeWeight.getTrueProbability());
                    } else {
                        _statisticTopologyMatrix[edgeLabel(edge)][nodeLabel(
                            sink)].multiply(nodeWeight.getFalseProbability());
                    }
                } else {
                    throw new RuntimeException("Node " + getName(sink)
                            + " is not a boolean node"
                            + " or its probability is not set.");
                }
            }
        }

        return _statisticTopologyMatrix;
    }

    /** Get topologyMatrix with adjusting probabilities of boolean edges.
     *  @return A statistic topology matrix.
     */
    /*public double[][] getStatisticTopologyMatrix() {
        return _statisticTopologyMatrix;
    }*/

    /** Compute statistical repetitions according to probabilities of
     *  boolean nodes.
     *  @return A Hashmap with Node mapping to it's repetition.
     */
    public HashMap computeStatisticRepetitions() {
        if (connectedComponents().size() > 1) {
            throw new RuntimeException("Graph not completely connected");
        }
        //check for exception
        computeStatisticTopologyMatrix();

        _repetitions = new HashMap();

        int nodeCount = nodeCount();
        numerators = new int[nodeCount];
        denominators = new int[nodeCount];
        repetitions = new int[nodeCount];

        numerators[0] = denominators[0] = 1;

        //compute here
        _computeNodesRepetition(node(0));

        int multiplier=1;
        int gcd=1;
        for(int i=0; i<nodeCount; i++) {
            gcd = ExtendedMath.gcd(numerators[i], denominators[i]);
            numerators[i] /= gcd;
            denominators[i] /= gcd;
            multiplier = Fraction.lcm(multiplier, denominators[i]);
        }
        for (int i=0; i<nodeCount; i++) {
            repetitions[i] = numerators[i]*(multiplier/denominators[i]);
        }

        gcd = repetitions[0];
        for (int i=1; i<nodeCount; i++) {
            gcd = ExtendedMath.gcd(gcd, repetitions[i]);
        }
        if( gcd > 1 ) {
            for (int i=0; i<nodeCount; i++) {
                repetitions[i] /= gcd;
            }
        }

        Iterator edgeIter = edges().iterator();
        while (edgeIter.hasNext()) {
            Edge arc = (Edge)edgeIter.next();
            BDFEdgeWeight weight = (BDFEdgeWeight)arc.getWeight();
            int srcRept = repetitions[nodeLabel(arc.source())];
            int snkRept = repetitions[nodeLabel(arc.sink())];
            Fraction prd = _statisticTopologyMatrix[edgeLabel(arc)][nodeLabel(
                    arc.source())];
            Fraction cns = _statisticTopologyMatrix[edgeLabel(arc)][nodeLabel(
                    arc.sink())];
            if (srcRept * prd.getNumerator() / prd.getDenominator() +
                    snkRept * cns.getNumerator() / cns.getDenominator() != 0) {
                throw new RuntimeException("Inconsistent sampling rate.");
            }
        }

        for (int i=0; i < nodeCount; i++) {
            _repetitions.put(node(i), new Integer(repetitions[i]));
        }

        return _repetitions;
    }

    /** Verify edge weight for BDF graph.
     *  @param weight The edge weight to verify.
     *  @return True if the given edge weight is valid for BDF graph.
     */
    public boolean validEdgeWeight(Object weight) {
        return weight instanceof BDFEdgeWeight;
    }

    /** Verify node weight for BDF graph.
     *  @param weight The node weight to verify.
     *  @return True if the given node weight is valid for BDF graph.
     */
    public boolean validNodeWeight(Object weight) {
        return weight instanceof CoreFunctionNode;
    }

    ///////////////////////////////////////////////////////////////////
    ////                         private methods                   ////

    private void _computeNodesRepetition(Node node) {
        Iterator outEdgeIter = outputEdges(node).iterator();
        while (outEdgeIter.hasNext()) {
            Edge outEdge = (Edge)outEdgeIter.next();
            Node sink = outEdge.sink();
            Fraction prd = _statisticTopologyMatrix[edgeLabel(outEdge)][
                nodeLabel(node)];
            Fraction cns = _statisticTopologyMatrix[edgeLabel(outEdge)][
                nodeLabel(sink)];

            int nodeIndex = nodeLabel(node);
            int sinkIndex = nodeLabel(sink);
            //if not yet compute
            if (numerators[sinkIndex] == 0) {
                numerators[sinkIndex] = numerators[nodeIndex]
                        * prd.getNumerator() * cns.getDenominator();
                denominators[sinkIndex] = denominators[nodeIndex]
                        * cns.getNumerator() * prd.getDenominator();
                _computeNodesRepetition(sink);
            }
        }

        Iterator inEdgeIter = inputEdges(node).iterator();
        while (inEdgeIter.hasNext()) {
            Edge inEdge = (Edge)inEdgeIter.next();
            Node source = inEdge.source();
            Fraction prd = _statisticTopologyMatrix[edgeLabel(inEdge)]
                [nodeLabel(source)];
            Fraction cns = _statisticTopologyMatrix[edgeLabel(inEdge)]
                [nodeLabel(node)];

            int nodeIndex = nodeLabel(node);
            int sourceIndex = nodeLabel(source);
            //if not yet compute
            if ( numerators[sourceIndex] == 0 ) {
                numerators[sourceIndex] = numerators[nodeIndex]
                    * cns.getNumerator() * prd.getDenominator();
                denominators[sourceIndex] = denominators[nodeIndex]
                    * prd.getNumerator() * cns.getDenominator();
                _computeNodesRepetition(source);
            }
        }

    }

    ///////////////////////////////////////////////////////////////////
    ////                         private methods                   ////

    private int _getProductionRate(BDFEdgeWeight weight) {
        Object rate = weight.getBDFProductionRate();
        if (rate instanceof Integer) {
            return ((Integer)rate).intValue();
        } else if (rate instanceof int[]) {
            if (((int[])rate)[0] == 0 && ((int[])rate)[1] > 0) {
                return ((int[])rate)[1];
            } else if (((int[])rate)[0] > 0 && ((int[])rate)[1] == 0) {
                return ((int[])rate)[0];
            } else {
                throw new RuntimeException("Invalid boolean rate.");
            }
        } else {
            throw new RuntimeException("Invalid BDF rate type.");
        }
    }

    private int _getConsumptionRate(BDFEdgeWeight weight) {
        Object rate = weight.getBDFConsumptionRate();
        if (rate instanceof Integer) {
            return ((Integer)rate).intValue();
        } else if (rate instanceof int[]) {
            if (((int[])rate)[0] == 0 && ((int[])rate)[1] > 0) {
                return ((int[])rate)[1];
            } else if (((int[])rate)[0] > 0 && ((int[])rate)[1] == 0) {
                return ((int[])rate)[0];
            } else {
                throw new RuntimeException("Invalid boolean rate.");
            }
        } else {
            throw new RuntimeException("Invalid BDF rate type.");
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////                         private variables                 ////

    private int[][] _pureTopologyMatrix;
    private Fraction[][] _statisticTopologyMatrix;
    private HashMap _repetitions;

    private int[] numerators;
    private int[] denominators;
    private int[] repetitions;
}


