/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Information associated with a BDF node. */

package dif.bdf;

import dif.DIFNodeWeight;
import dif.attributes.BDFAttributeType;
import dif.data.Fraction;
import dif.util.graph.Graph;
import dif.util.graph.Node;
import dif.util.graph.GraphWeightException;


//////////////////////////////////////////////////////////////////////////
//// BDFNodeWeight
/** Information associated with an BDF node. {@link BDFNodeWeight}s are objects
associated with {@link Node}s
that represent BDF nodes in {@link Graph}s.
This class caches frequently-used data associated with BDF nodes.
<p>
There are two types of BDF node, one is regular node and the other is boolean
node. A regular node is restricted to be an SDF node. A boolean node can be a
switch or a select.
<p>
@author Chia-Jui Hsu
@version $Id: BDFNodeWeight.java 606 2008-10-08 16:29:47Z plishker $
@see Node
@see BDFEdgeWeight
*/

public class BDFNodeWeight extends DIFNodeWeight {

    /** Construct a regular BDF node.
     */
    public BDFNodeWeight() {
        _nodeType = BDFAttributeType.RegularNode;
        _probability = null;
        setComputation(null);
    }

    /** Construct a regular BDF node weight that is associated with the given
     *  computation.
     */
    public BDFNodeWeight(Object computation) {
        _nodeType = BDFAttributeType.RegularNode;
        _probability = null;
        setComputation(computation);
    }

    ///////////////////////////////////////////////////////////////////
    ////                       public methods                      ////

    /** Get the type of this node.
     *  @return BDFAttributeType.BooleanNode or BDFAttributeType.RegularNode.
     */
    public BDFAttributeType getBDFNodeType() {
        return _nodeType;
    }

    /** Get the probability of false control tokens.
     *  @return The probability of false control tokens.
     */
    public Fraction getFalseProbability() {
        Fraction falseProb = new Fraction(1);
        falseProb.subtract(_probability);
        return falseProb;
    }

    /** Get the probability of true control tokens.
     *  @return The probability of true control tokens.
     */
    public Fraction getTrueProbability() {
        return _probability;
    }

    /** Set the type of this node.
     *  @param type BDFAttributeType.BooleanNode or BDFAttributeType.RegularNode.
     */
    public void setBDFNodeType(BDFAttributeType type) {
        _nodeType = type;
    }

    /** Set probability of trun control tokens of associated BDFComputation.
     *  The probability is represented in rational, i.e. numerator /
     *  denominator.
     *  @param numerator
     *  @param denominator
     */
    public void setTrueProbability(int numerator, int denominator) {
        if (_nodeType == BDFAttributeType.BooleanNode) {
            if (denominator >= numerator) {
                _probability = new Fraction(numerator, denominator);
            } else {
                throw new GraphWeightException(
                    "Probability is greater than one.");
            }
        } else {
            throw new GraphWeightException(
                    "BDF node is regular not boolean.");
        }
    }

    /** Output BDF node weight information.
     *  @return BDF node weight information.
     */
    public String toString() {
        StringBuffer string = new StringBuffer();
        if (getComputation() != null) {
            if (getComputation() instanceof String) {
                string.append((String)getComputation());
            } else {
                string.append(getComputation().getClass().getName());
            }
        }
        string.append(" " + _nodeType.toString());
        if (_probability != null) {
            string.append(" " + _probability.toString());
        }
        return string.toString();
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    private BDFAttributeType _nodeType;
    private Fraction _probability;
}


