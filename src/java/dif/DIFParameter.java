/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif;

import dif.attributes.IntervalCollection;
import dif.util.Conventions;
import dif.util.Value;

import java.util.Map;
import java.util.Objects;

/**
 * Parameter class for DIF graph.
 * The value of a DIFParameter can be one of the following type:
 * Integer, Double, Complex, String, Boolean, int[], double[],
 * Complex[], int[][], double[][], Complex[][], ArrayList.
 * <p>
 * The DIF refinement block can set a sub-parameter by referring to
 * the parameter in the current graph. In other words, the<i>_value</i> of a
 * DIFParameter can refer to another DIFParameter in the super graph.
 * This semantics are useful in parameterized graph.
 * <p>
 * A parameter can be specified by its interval range, ie, a parameter's value
 * can be set as an {@link IntervalCollection}
 * containing the lower and upper bound or discrete region of this parameter.
 * <p>
 * In conclusion, a DIFParameter contains a valid value or
 * a valid DIFParameter in super graph or a valid {@link IntervalCollection}.
 *
 * @author Chia-Jui Hsu
 * @version $Id: DIFParameter.java 606 2008-10-08 16:29:47Z plishker $
 * @see DIFGraph
 */

public class DIFParameter {

    ///////////////////////////////////////////////////////////////////
    ////                       private fields                      ////
    private String _name;
    private DIFGraph _container;

    ///////////////////////////////////////////////////////////////////
    ////                       public methods                      ////
    private Object _value;
    //private String _type;
    private String _dataType;
    private String _refName;

    protected DIFParameter() {
    }

    /**
     * Constructs a new parameter with the given name.
     *
     * @param name Name of the parameter.
     * @throws IllegalArgumentException If
     *
     *
     *
     *
     *
     *
     *                            {@link Conventions#labelConvention(String)}
     *                            returns an error
     *                                  for the parameter name.
     */
    public DIFParameter(String name) {
        String error = Conventions.labelConvention(name);
        if (error != null) {
            throw new IllegalArgumentException(
                    "Parameter name error: " + error);
        }
        _name = name;
    }

    /**
     * Clone this DIFParameter and return the cloned DIFParameter with
     * the same name, same value.
     * For reference value referring to DIFParameter in the super
     * graph, we cannot simply copy the referred DIFParameter.
     * Thus, if <i>_value</i> contains
     * a reference, its cloned version is null.
     *
     * @return Cloned DIFParameter instance.
     */
    @SuppressWarnings("CloneDoesntCallSuperClone") public Object clone() {
        DIFParameter param = new DIFParameter(_name);
        if (_value != null) {
            param.setValue(_cloneValue(null));
        }
        if (_dataType != null) {
            param.setDataType(_dataType);
        }
        return param;
    }

    /**
     * Return the cloned version of DIFParameter with
     * the same name, same value, or cloned reference
     * DIFParameter.
     * For referred DIFParameter, the cloned object is obtained by
     * <i>map</i> that contains the mapping from the original reference
     * value to the cloned reference value.
     *
     * @param map The Map that contains the mapping
     *            from the original reference value to the mirrored reference
     *            value.
     * @return Cloned DIFAttribute instance.
     * @see #_cloneValue(Object)
     */
    public Object clone(Object map) {
        DIFParameter param = new DIFParameter(_name);
        if (_value != null) {
            if (map instanceof Map) {
                param.setValue(_cloneValue(map));
            } else {
                param.setValue(_cloneValue(null));
            }
        }
        if (_dataType != null) {
            param.setDataType(_dataType);
        }
        return param;
    }

    /**
     * Compare the DIFParameter object with the input object.
     * This method compares their name, and value.
     * To compare <i>_value</i> fields in both objects, we use their
     * toString() methods to make the comparison.
     *
     * @param object The DIFParameter instance.
     * @return True if equal, otherwise, false.
     */
    public boolean equals(Object object) {
        if (object instanceof DIFParameter) {
            boolean equalName = false;
            //boolean equalType = false;
            boolean equalDataType = false;
            boolean equalValue = false;
            if (((DIFParameter) object).getName().equals(_name)) {
                equalName = true;
            }

            if (_dataType != null && _dataType.equals(
                    ((DIFParameter) object).getDataType())) {
                equalDataType = true;
            } else if (_dataType == null &&
                    ((DIFParameter) object).getDataType() == null) {
                equalDataType = true;
            }

            if (_value == null && ((DIFParameter) object).getValue() == null) {
                equalValue = true;
            } else if (_value != null &&
                    ((DIFParameter) object).getValue() != null &&
                    _value.toString().equals(
                            ((DIFParameter) object).getValue().toString())) {
                equalValue = true;
            }

            if (equalName && equalDataType && equalValue) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get DIFGraph which contains this parameter.
     * Note that DIFGraph.setParameter() will set the back reference.
     *
     * @return DIFGraph
     */
    public DIFGraph getContainer() {
        return _container;
    }

    /**
     * DIFGraph.setParameter() will set the back reference.
     */
    protected void setContainer(DIFGraph container) {
        _container = container;
    }

    /**
     * Get datatype of the value of parameter.
     *
     * @return datatype
     */
    public String getDataType() {
        return _dataType;
    }

    /**
     * Set the datatype of the value of parameter. E.g., int, float,
     * int*, float*.
     */
    public void setDataType(String datatype) {
            /*String error = Conventions.datatypeConvention(datatype);
            if (error != null) {
                throw new IllegalArgumentException("Attribute type error: "
                        + error);
            }*/
        _dataType = datatype;
    }

    /**
     * Get parameter name.
     *
     * @return name
     */
    public String getName() {
        return _name;
    }

    /**
     * Get value. The return type can be one of the following:
     * value, DIFParameter, or IntervalCollection.
     *
     * @return The value of this parameter.
     */
    public Object getValue() {
        return _value;
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /**
     * Get value deeply. If this DIFParameter contains another
     * DIFParameter as value, this method will trace along the way
     * until the value is not a DIFParameter.
     *
     * @return The deepest value of this parameter. If the deepest
     * value is null, then return null.
     */
    public Object getValueDeeply() {
        if (_value instanceof DIFParameter) {
            DIFParameter parameter = (DIFParameter) _value;
            while (parameter.getValue() instanceof DIFParameter) {
                parameter = (DIFParameter) parameter.getValue();
            }
            if (parameter.getValue() != null) {
                return parameter.getValue();
            } else {
                return null;
            }
        } else {
            return getValue();
        }
    }

    /**
     * Get parameter deeply. If this DIFParameter contains another
     * DIFParameter as value, this method will trace along the way
     * until the deepest DIFParameter.
     *
     * @return The deepest DIFParameter. If the value of this
     * DIFParameter is not a DIFParameter, then return this.
     */
    public DIFParameter getParameterDeeply() {
        if (_value instanceof DIFParameter) {
            DIFParameter parameter = (DIFParameter) _value;
            while (parameter.getValue() instanceof DIFParameter) {
                parameter = (DIFParameter) parameter.getValue();
            }
            return parameter;
        } else {
            return this;
        }
    }

    /**
     * Hash code.
     */
    public int hashCode() {
        return _name.hashCode();
    }

    /**
     * Set parameter value.
     *
     * @param value A value object. The current DIF language supports
     *              are Integer, Double, Complex, String, Boolean, int[],
     *              double[],
     *              Complex[], int[][], double[][], Complex[][], ArrayList,
     *              other DIFParameter instance, or IntervalCollection.
     * @return The previous value object.
     */
    public Object setValue(Object value) {
        Object returnValue = _value;
        _value = value;
        if (value instanceof DIFParameter) {
            String pName = ((DIFParameter) value).getName();
            if (!Objects.equals(pName, this.getName())) {
                _refName = pName;
            }
        }
        return returnValue;
    }

    public String getRefName() {
        return _refName;
    }

    public String toString() {
        StringBuilder buffer;
        buffer = new StringBuilder("Attribute: " + _name);
        if (getDataType() != null) {
            buffer.append(", datatype = ").append(getDataType());
        }
        if (getValue() != null) {
            buffer.append(", value = ").append(getValue().toString());
        }
        return buffer.toString();
    }

    /**
     * Clone token value. If <i>_value</i> contains a reference to a
     * DIFParameter object, it finds the cloned object by using
     * <i>map</i>. Note that it
     * will return null if <i>map</i> does not contain
     * the mapping.
     *
     * @param map The HashMap object.
     * @see #clone(Object)
     */
    protected Object _cloneValue(Object map) {
        if (Value.isValue(_value)) {
            return Value.cloneValue(_value);
        } else if (_value instanceof IntervalCollection) {
            return ((IntervalCollection) _value).clone();
        } else if (_value instanceof DIFParameter && map instanceof Map) {
            return ((Map) map).get(_value);
        } else {
            return null;
        }
    }

    /**
     * {@link DIFGraph#addGraph} will modified the name if naming
     * conflict.
     */
    protected void _setName(String name) {
        _name = name;
    }
}


