/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* An enumeration of types of default SDF attributes. */

package dif.attributes;

//////////////////////////////////////////////////////////////////////////
//// SDFAttributeType
/**
An enumeration of types of default attributes for SDF.
It won't be possible to create an instance of this object and it doesn't
have any public fields. During the first access to any of
it, {@link AttributeType} class initializes it's
internal data structure.
<p>

@author Fuat Keceli based on a file by Shahrooz Shahparnia
@version $Id: SDFAttributeType.java 606 2008-10-08 16:29:47Z plishker $
*/
public class SDFAttributeType extends CFDFAttributeType {

    protected SDFAttributeType(String label) {
        super(label);
    }

    /** Enumeration type constant representing execution time (A hypothetical
     *  SDF attribute as an example).
     */
    public static final SDFAttributeType sdfComplexity =
        new SDFAttributeType("sdfComplexity");

    public static final SDFAttributeType SDFNode =
        new SDFAttributeType("sdfNode");
}


