/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Base class for enumeration of types of attributes. */

package dif.attributes;

import dif.DIFGraph;

import java.util.ArrayList;
import java.util.Collection;

//////////////////////////////////////////////////////////////////////////
//// AttributeType
/**
Base class for enumeration of types of attributes that can be used
in {@link DIFGraph}s.
It won't be possible to create an instance of this object and it doesn't
have any public fields. During the first access to any of its sub-classes, it
initializes it's internal data structure. For more information about the
usage of the methods refer to the package description.
<p>

@author Fuat Keceli based on a file by Shahrooz Shahparnia
@version $Id: AttributeType.java 606 2008-10-08 16:29:47Z plishker $
@see DIFGraph
*/
public class AttributeType {

    protected AttributeType(String label) {
        this._stringValue = label;
        _integerValue = _nextIndex;
        if (_attributeTypes == null) {
            _attributeTypes = new ArrayList();
            _attributeTypes.add(_nextIndex++, this);
        } else {
            _attributeTypes.add(_nextIndex++, this);
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Return a collection of attribute types available in the classes
     *  which extend this class and which are referred in the code through
     *  their public fields at least once before using this method.
     *  @return The type of attributes in the form of a Collection.
     */
    public final static Collection elements() {
        return _attributeTypes;
    }

    /** Return the amount of attribute types available in the classes
     *  which extend this class and which are referred in the code through
     *  their public fields at least once before using this method.
     *  @return The amount of attribute types in the form of an int.
     */
    public static int getNumLevels() {
        return _nextIndex;
    }

    /** Return the integer equivalent of the attribute type.
     *  @return An integer uniquely associated with a specific attribute type.
     */
    public final int intValue() {
        return _integerValue;
    }

    /** Return the string equivalent of the collection element.
     *  @return Equivalent of the collection element in the form of a String.
     */
    public final String toString() {
        return _stringValue;
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    private static int _nextIndex = 0;
    private static ArrayList _attributeTypes = null;

    private final String _stringValue;
    private final int _integerValue;
}


