/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* An enumeration of types of default BDF attributes. */

package dif.attributes;

//////////////////////////////////////////////////////////////////////////
//// RCDFAttributeType
/**
An enumeration of types of default attributes for BDF.
It won't be possible to create an instance of this object and it doesn't
have any public fields. During the first access to any of
it, {@link AttributeType} class initializes it's
internal data structure.
<p>

@author Chia-Jui Hsu
@version $Id: BDFAttributeType.java 362 2007-02-25 21:08:57Z plishker $
*/
public class RCDFAttributeType extends DIFAttributeType {

    protected RCDFAttributeType(String label) {
        super(label);
    }

    public static final RCDFAttributeType MTPEGroup =
        new RCDFAttributeType("MTPEGroup");

    /*
    public static final BDFAttributeType RegularEdge =
        new BDFAttributeType("RegularEdge");
    
    public static final BDFAttributeType BooleanEdge =
        new BDFAttributeType("BooleanEdge");

    public static final BDFAttributeType ControlEdge =
        new BDFAttributeType("ControlEdge");

    public static final BDFAttributeType SwitchTrueOut =
        new BDFAttributeType("SwitchTrueOut");
    
    public static final BDFAttributeType SwitchFalseOut =
        new BDFAttributeType("SwitchFalseOut");

    public static final BDFAttributeType SwitchControl =
        new BDFAttributeType("SwitchControl");

    public static final BDFAttributeType SelectTrueIn =
        new BDFAttributeType("SelectTrueIn");

    public static final BDFAttributeType SelectFalseIn =
        new BDFAttributeType("SelectFalseIn");

    public static final BDFAttributeType SelectControl =
        new BDFAttributeType("SelectControl");
    */
}


