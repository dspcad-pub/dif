/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* An enumeration of types of default BDF attributes. */

package dif.attributes;

//////////////////////////////////////////////////////////////////////////
//// CFDFAttributeType
/**
For now, an enumeration of types of default attributes for BDF.
It won't be possible to create an instance of this object and it doesn't
have any public fields. During the first access to any of
it, {@link AttributeType} class initializes it's
internal data structure.
<p>

@author Chia-Jui Hsu
@version $Id: BDFAttributeType.java 362 2007-02-25 21:08:57Z plishker $
*/
public class CFDFAttributeType extends DIFAttributeType {

    protected CFDFAttributeType(String label) {
        super(label);
    }

    public static final CFDFAttributeType SDFNode =
        new CFDFAttributeType("sdfNode");

    public static final CFDFAttributeType RegularNode =
        new CFDFAttributeType("regularNode");

    public static final CFDFAttributeType BooleanNode =
        new CFDFAttributeType("booleanNode");

    public static final CFDFAttributeType RegularMode =
        new CFDFAttributeType("regularMode");

    public static final CFDFAttributeType GroupMode =
        new CFDFAttributeType("groupMode");

    public static final CFDFAttributeType BooleanMode =
        new CFDFAttributeType("booleanMode");

    public static final CFDFAttributeType NullMode =
        new CFDFAttributeType("nullMode");

    public static final CFDFAttributeType InitMode =
        new CFDFAttributeType("initMode");

    public static final CFDFAttributeType Switch =
        new CFDFAttributeType("Switch");

    public static final CFDFAttributeType Select =
        new CFDFAttributeType("Select");

    public static final CFDFAttributeType TrueProbability =
        new CFDFAttributeType("trueProbability");

    /*
    public static final CFDFAttributeType RegularEdge =
        new CFDFAttributeType("RegularEdge");
    
    public static final CFDFAttributeType BooleanEdge =
        new CFDFAttributeType("BooleanEdge");

    public static final CFDFAttributeType ControlEdge =
        new CFDFAttributeType("ControlEdge");

    public static final CFDFAttributeType SwitchTrueOut =
        new CFDFAttributeType("SwitchTrueOut");
    
    public static final CFDFAttributeType SwitchFalseOut =
        new CFDFAttributeType("SwitchFalseOut");

    public static final CFDFAttributeType SwitchControl =
        new CFDFAttributeType("SwitchControl");

    public static final CFDFAttributeType SelectTrueIn =
        new CFDFAttributeType("SelectTrueIn");

    public static final CFDFAttributeType SelectFalseIn =
        new CFDFAttributeType("SelectFalseIn");

    public static final CFDFAttributeType SelectControl =
        new CFDFAttributeType("SelectControl");
    */
}


