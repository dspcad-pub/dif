/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* An enumeration of types of default DIF attributes. */

package dif.attributes;

//////////////////////////////////////////////////////////////////////////
//// DIFAttributeType
/**
An enumeration of types of default attributes for DIF.
It won't be possible to create an instance of this object and it doesn't
have any public fields. During the first access to any of
it, {@link AttributeType} class initializes it's
internal data structure.
<p>

@author Chia-Jui Hsu, Fuat Keceli based on a file by Shahrooz Shahparnia
@version $Id: DIFAttributeType.java 606 2008-10-08 16:29:47Z plishker $
*/
public class DIFAttributeType extends AttributeType {

    protected DIFAttributeType(String label) {
        super(label);
    }

    /*public boolean equals(DIFAttributeType type) {
        return toString().equals(type.toString());
    }*/

    /** Enumeration type constant representing execution time (A hypothetical
     *  DIF attribute as an example).
     */
    public static final DIFAttributeType complexity =
        new DIFAttributeType("complexity");

    /** Enumeration type constant representing hierarchical depth of
     *  a node if it is a super node. (A hypothetical DIF attribute as an
     *  example).
     */
    public static final DIFAttributeType depth =
        new DIFAttributeType("depth");

    /** Enumeration type of data type of Edge and Port.
     */
    public static final DIFAttributeType datatype =
        new DIFAttributeType("datatype");

    /** Enumeration type of buffer merging for in-place execution actors.
     */
    public static final DIFAttributeType merge =
        new DIFAttributeType("merge");

    /** Enumeration type of production rate.
     */
    public static final DIFAttributeType production =
        new DIFAttributeType("production");

    /** Enumeration type of consumption rate.
     */
    public static final DIFAttributeType consumption =
        new DIFAttributeType("consumption");

    /** Enumeration type of delay.
     */
    public static final DIFAttributeType delay =
        new DIFAttributeType("delay");

    /** Enumeration type of computation of a node.
     *  FIXME Move it to DIFNodeAttributeType
     */
    public static final DIFAttributeType computation =
        new DIFAttributeType("computation");
}


