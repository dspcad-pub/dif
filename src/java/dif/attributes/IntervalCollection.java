/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A collection of intervals or points. */

package dif.attributes;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import dif.util.graph.GraphException;

import dif.util.Conventions;

//////////////////////////////////////////////////////////////////////////
//// IntervalCollection
/**
This class is intented for use with parametrizable attributes. It carries
possible values for an interval which could be intervals and/or points.
@author Fuat Keceli based on a file by Shahrooz Shahparnia
@version $Id: IntervalCollection.java 606 2008-10-08 16:29:47Z plishker $
*/
public class IntervalCollection {

    protected IntervalCollection() {
    }

    /** Creates an interval collection with a name. Names are important for
     *  parametrizing DIFGraphs. For
     *  the {@link dif.language.DIFWriter}s to work properly,
     *  different parameters should have different names.
     *  @param name Name of the parameter.
     */
    public IntervalCollection(String name) {
        String error = Conventions.labelConvention(name);
        if(error != null) {
            throw new GraphException("Naming error. " + error);
        }
	_values = new Vector();
        _name = name;
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Adds an interval to the list of intervals in this collection. Maintains
     *  the order while adding.
     *  @param interval Interval that will be added to the collection. To add
     *  a single point 'a', [a, a] can be used.
     */
    public void add(Interval interval) {
        for(int i = 0; i < _values.size(); i++) {
            Interval value = (Interval)_values.get(i);
            Interval combinedInterval = combine((Interval)value, interval);
            if(combinedInterval != null) {
                _values.set(i, combinedInterval);
                return;
            }
            if(interval.compare(value) < 0) {
                _values.add(i, interval);
                return;
            }
        }
        _values.add(interval);
    }

    /** Clone this IntervalCollection.
     *  @return The cloned version of this IntervalCollection.
     */
    public Object clone() {
        IntervalCollection returnValue = new IntervalCollection(_name);
        returnValue._values = (Vector)_values.clone();
        return returnValue;
    }

    /** Combine two intervals if they can be expressed as a single interval.
     *  Return null otherwise.
     *  @return The combined interval. Null if the intervals cannot be
     *  combined.
     */
    public static Interval combine(Interval interval1, Interval interval2) {
        double lowerBound, higherBound;
        boolean includesLow, includesHigh;
	if(interval1.intersects(interval2)) {
	    if((interval1.getLowValue() == interval2.getLowValue())) {
		if(interval1.includesLow() == interval2.includesLow()) {
		    includesLow = true;
		} else {
		    includesLow = false;
		}
		lowerBound = interval1.getLowValue();
	    } else {
		lowerBound = Math.min(interval1.getLowValue(),
                        interval2.getLowValue());
		includesLow = (lowerBound == interval1.getLowValue()) ?
                    interval1.includesLow() : interval2.includesLow();
	    }
	    if((interval1.getHighValue() == interval2.getHighValue())) {
		if(interval1.includesHigh() == interval2.includesHigh()) {
		    includesHigh = true;
		} else {
		    includesHigh = false;
		}
		higherBound = interval1.getHighValue();
	    } else {
		higherBound = Math.max(interval1.getHighValue(),
                        interval2.getHighValue());
		includesHigh = (higherBound == interval1.getHighValue()) ?
                    interval1.includesHigh() : interval2.includesHigh();
	    }
	} else if((interval1.getHighValue() == interval2.getLowValue()) &&
                (interval1.includesHigh() || interval2.includesLow())) {
            higherBound = interval2.getHighValue();
            includesHigh = interval2.includesHigh();
            lowerBound = interval1.getLowValue();
            includesLow = interval1.includesLow();
        } else if((interval2.getHighValue() == interval1.getLowValue()) &&
                (interval2.includesHigh() || interval1.includesLow())) {
            higherBound = interval1.getHighValue();
            includesHigh = interval1.includesHigh();
            lowerBound = interval2.getLowValue();
            includesLow = interval2.includesLow();
        } else {
	    return null;
	}
        return new Interval(lowerBound, includesLow, higherBound,
                includesHigh);
    }

    /** Returns true if two collection contains the same mathematical
     *  intervals.
     *  @return True if two interval collections are mathematically the same.
     */
    public boolean equals(Object collection) {
        if(getClass() == collection.getClass() &&
                toString().equals(collection.toString()) &&
                getName().equals(((IntervalCollection)collection).getName())) {
            return true;
        }
        return false;
    }

    /** Returns a list of intervals and defines this collection.
     *  Returned list has the minimal property (which means there are no
     *  replication of points) and is ordered.
     *  @return A list of {@link Interval}s. It is a
     *  copy of the original list that defines this collection.
     */
    public List getIntervals() {
        return new Vector(_values);
    }

    /** Returns the name of this collection.
     *  @return The name.
     */
    public String getName() {
        return _name;
    }

    public int hashCode() {
        return toString().hashCode() + getName().hashCode();
    }

    /** Prints out the collection of intervals and points in this object.
     *  Returned string is cannonical (will always be the same for the same set
     *  of intervals).
     *  @return A string compatible with DIF.
     */
    public String toString() {
        String points = "";
        String intervals = "";
        for(Iterator values = _values.iterator(); values.hasNext(); ) {
            Interval interval = (Interval)values.next();
            if(interval.getHighValue() == interval.getLowValue()) {
                if(!points.equals("")) {
                    points = points + ", ";
                }
                points = points + interval.getHighValue();
            } else {
                if(!intervals.equals("")) {
                    intervals = intervals + " + ";
                }
                intervals = intervals + interval.toString();
            }
        }
        return ((!points.equals("")) ? ("{" + points + "}") : "")
            + ((!points.equals("") && !intervals.equals("")) ? " + " : "")
            + ((!intervals.equals("")) ? intervals : "");
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    private Vector _values;
    private String _name;
}


