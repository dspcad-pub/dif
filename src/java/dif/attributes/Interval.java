/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* One dimensional inclusive or exclusive intervals. */

package dif.attributes;

//////////////////////////////////////////////////////////////////////////
//// Interval
/**
Defines one dimensional inclusive or exclusive intervals. Intervals are
immutable.
@author Fuat Keceli based on a file by Shahrooz Shahparnia
@version $Id: Interval.java 606 2011-10-08 16:29:47Z plishker $
*/
public class Interval {

    /** Creates a point: [a,a].
     *  @param point The point to create.
     */
    public Interval(double point) {
        _lowerBound = point;
        _higherBound = point;
        _includesLow = true;
        _includesHigh = true;
    }

    /** Creates an interval.
     *
     *  @param lowerBound The lower bound of the interval.
     *  @param includesLow True if the lower bound is to be included in the
     *  interval.
     *  @param higherBound The lower bound of the interval.
     *  @param includesHigh True if the higher bound is to be included in the
     *  interval.
     *  @exception IllegalArgumentException If the interval is not valid.
     */
    public Interval(double lowerBound, boolean includesLow,
            double higherBound, boolean includesHigh) {
        if (_isValid(lowerBound, includesLow, higherBound, includesHigh)) {
            _lowerBound = lowerBound;
            _higherBound = higherBound;
            _includesLow = includesLow;
            _includesHigh = includesHigh;
        } else {
            throw new IllegalArgumentException("Invalid Interval, "
                    + "the parameters are wrong.");
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Compares two intervals.
     *  @param interval Interval to compare with this one.
     *  @return 0 if intervals intersect. 1 if all of this interval's values
     *  are bigger, -1 otherwise.
     */
    public int compare(Interval interval) {
        if(intersects(interval)) {
            return 0;
        }
        if(((_lowerBound + _higherBound) / 2) >
                ((interval._higherBound + interval._lowerBound) / 2)) {
            return 1;
        } else {
            return -1;
        }
    }

    /** Check if a given value is within this interval or not.
     *
     *  @param value The given value.
     *  @return True if the given value is within the interval.
     */
    public boolean contains(double value) {
        boolean result = false;
        if (_lowerBound < value && value < _higherBound) {
            result = true;
        } else if (_lowerBound == value && _includesLow) {
            result = true;
        } else if (_higherBound == value && _includesHigh) {
            result = true;
        }
        return result;
    }

    /** Return the higher bound of this interval.
     *
     *  @return The higher bound of this interval.
     */
    public double getHighValue() {
        return _higherBound;
    }

    /** Return the lower bound of this interval.
     *
     *  @return The lower bound of this interval.
     */
    public double getLowValue() {
        return _lowerBound;
    }

    /** True if the interval includes the higher bound.
     *
     *  @return True if the interval includes the higher bound.
     */
    public boolean includesHigh() {
        return _includesHigh;
    }

    /** True if the interval includes the low value.
     *
     *  @return True if the interval includes the lower bound.
     */
    public boolean includesLow() {
        return _includesLow;
    }

    /**
     *  @param interval Interval to compare with.
     *  @return True if two intervals share any points.
     */
    public boolean intersects(Interval interval) {
	if(_higherBound < interval._lowerBound ||
                ((_higherBound == interval._lowerBound)
                        && !(_includesHigh && interval._includesLow)) ||
                interval._higherBound < _lowerBound ||
                ((interval._higherBound == _lowerBound)
                        && !(interval._includesHigh && _includesLow))) {
            return false;
	}
	return true;
    }

    /** Return the string equivalent of this interval.
     *
     *  @return Return the string equivalent of this interval.
     */
    public String toString() {
        String description = new String();
        if (_includesLow && _includesHigh) {
            description = new String(description + "[" + _lowerBound + ","
                    + _higherBound + "]");
        } else if (_includesLow && !_includesHigh) {
            description = new String(description + "[" + _lowerBound + ","
                    + _higherBound + ")");
        } else if (!_includesLow && _includesHigh) {
            description = new String(description + "(" + _lowerBound + ","
                    + _higherBound + "]");
        } else if (!_includesLow && !_includesHigh) {
            description = new String(description + "(" + _lowerBound + ","
                    + _higherBound + ")");
        }
        return description;
    }

    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////

    /** Check if the change to the bounds can be applied or not.
     *
     *  @param lowerBound The lower bound of the interval.
     *  @param includesLow True if the lower bound is to be included in the
     *  interval.
     *  @param higherBound The lower bound of the interval.
     *  @param includesHigh True if the higher bound is to be included in the
     *  interval.
     */
    protected boolean _isValid(double lowerBound, boolean includesLow,
            double higherBound, boolean includesHigh) {
        if (includesLow && includesHigh) {
            return lowerBound <= higherBound;
        } else {
            return lowerBound < higherBound;
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    private double _lowerBound;
    private double _higherBound;
    private boolean _includesLow;
    private boolean _includesHigh;
}


