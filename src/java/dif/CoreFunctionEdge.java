/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

//package mocgraph;
package dif;

import dif.util.graph.Edge;

import java.util.LinkedList;

////////////////////////////////////////////////////////////////////////// //
//Core Function Edge

/**
 * A core function edge provides simulation cabalities to each edge.
 *
 * @author William Plishker, Shuvra S. Bhattacharyya
 * @version $Id: CoreFunctionEdge.java 1634 2007-04-06 18:24:07Z ssb $
 * @see Edge
 */
public class CoreFunctionEdge extends DIFEdgeWeight {
    /**
     * Construct a core function edge
     */

    public CoreFunctionEdge() {
        super();
        _queue = new LinkedList();
    }


    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /**
     * Enqueues a token.
     */
    public void enqueue(Object token) {
        _queue.addLast(token);

        if (_queue.size() > _max) {
            _max = _queue.size();
        }
        pushes++;
        System.out.println(
                "Reached here token successfully pushed onto buffer " +
                        toString() + " \n");
        //System.out.println(printStats());
    }

    /**
     * Dequeues a token.
     *
     * @return Returns the dequeued token.
     */

    public Object dequeue() {
        System.out.println(
                "Reached in deque. Buffer stats: " + toString() + "\n");
        //System.out.println(printStats());

        pulls++;
        return _queue.removeFirst();
    }

    /**
     * Returns the number of tokens on this edge.
     *
     * @return The number of tokens on the edge.
     */
    public int peek() {
        return _queue.size();
    }

    public String printStats() {
        return peek() + " (" + pushes + ", " + pulls + ")";
    }

    public int max() {
        return _max;
    }

    public String toString() {
        return super.toString();
    }
    ///////////////////////////////////////////////////////////////////
    ////                         protected methods                 ////


    ///////////////////////////////////////////////////////////////////
    ////                         private vaiables                   ////

    /**
     * The queue holding token data.
     */
    private LinkedList _queue;
    private int pushes = 0;
    private int pulls = 0;
    private int _max = 0;

}


