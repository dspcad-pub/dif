/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif;

//////////////////////////////////////////////////////////////////////////
//// DIFEdgeWeight

import dif.util.graph.Edge;
import dif.util.graph.Graph;

/**
 * Information associated with an DIF edge.
 * DIFEdgeWeights are objects associated with {@link Edge}s
 * that represent DIF edges in {@link Graph}s.
 * This class caches frequently-used data associated with DIF edges.
 * It is also useful for performing graph
 * transformations (e.g., vectorization and re-timing).
 * It is intended for use with analysis/synthesis algorithms that operate
 * on generic graph representations of DIF models.
 *
 * @author Michael Rinehart, Shuvra S. Bhattacharyya, Chia-Jui Hsu
 * @version $Id: DIFEdgeWeight.java 606 2008-10-08 16:29:47Z plishker $
 * @see Edge
 */

public class DIFEdgeWeight implements Cloneable {

    /**
     * Construct an edge weight for a homogeneous, zero-delay edge.
     */
    public DIFEdgeWeight() {
        _delay = null;
        _sourcePort = null;
        _sinkPort = null;
    }

    /**
     * Construct an edge weight for a specified production,
     * consumption, and delay.
     *
     * @param productionRate  The production object
     * @param consumptionRate The consumption object.
     * @param delay           The delay object.
     */
    public DIFEdgeWeight(Object productionRate, Object consumptionRate,
                         Object delay) {
        _productionRate = productionRate;
        _consumptionRate = consumptionRate;
        _delay = delay;
        _sourcePort = null;
        _sinkPort = null;
    }


    /**
     * Construct an edge weight for a specified production,
     * consumption, and delay.
     * The source port and sink port are ports that correspond to this edge.
     *
     * @param sourcePort      The source port.
     * @param sinkPort        The sink port.
     * @param productionRate  The production object.
     * @param consumptionRate The consumption object.
     * @param delay           The delay object.
     */
    public DIFEdgeWeight(Object sourcePort, Object sinkPort,
                         Object productionRate, Object consumptionRate,
                         Object delay) {
        _productionRate = productionRate;
        _consumptionRate = consumptionRate;
        _delay = delay;
        _sourcePort = sourcePort;
        _sinkPort = sinkPort;
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /**
     * Clone this DIFEdgeWeight object or the derived class object. All
     * the derived class must be back compatiable with the
     * {@link #setProductionRate}, {@link #setConsumptionRate}, and
     * {@link #setDelay}.
     *
     * @return A cloned version DIFEdgeWeight (or subclass) object.
     */
    public Object clone() {
        //getClass() can get the correct derived class
        try {
            Object returnObj = getClass().newInstance();
            if (_sourcePort != null) {
                ((DIFEdgeWeight) returnObj).setSourcePort(_sourcePort);
            }
            if (_sinkPort != null) {
                ((DIFEdgeWeight) returnObj).setSinkPort(_sinkPort);
            }
            if (_productionRate != null) {
                ((DIFEdgeWeight) returnObj).setProductionRate(_productionRate);
            }
            if (_consumptionRate != null) {
                ((DIFEdgeWeight) returnObj).setConsumptionRate(
                        _consumptionRate);
            }
            if (_delay != null) {
                ((DIFEdgeWeight) returnObj).setDelay(_delay);
            }
            return returnObj;
        } catch (Exception exp) {
            throw new RuntimeException(
                    exp.getMessage() + "\nConnot clone edge weight.");
        }

    }


    /**
     * Return the token consumption rate of the associated DIF edge.
     * The sum of the token consumption rates over all phases is returned.
     *
     * @return The token consumption rate.
     */
    public Object getConsumptionRate() {
        return _consumptionRate;
    }

    /**
     * Return the delay of the associated DIF edge.
     *
     * @return The delay.
     */
    public Object getDelay() {
        return _delay;
    }

    /**
     * Return the token production rate of the associated DIF edge.
     * The sum of the token production rates over all phases is returned.
     *
     * @return The token production rate.
     */
    public Object getProductionRate() {
        return _productionRate;
    }

    /**
     * Get the sink port of the associated DIF edge .
     *
     * @return The sink port.
     */
    public Object getSinkPort() {
        return _sinkPort;
    }

    /**
     * Get the source port of the associated DIF edge .
     *
     * @return The source port.
     */
    public Object getSourcePort() {
        return _sourcePort;
    }

    /**
     * Set the token consumption rate of the associated DIF edge.
     * This method should be overridden in more specialized
     * edge weight classes to ensure that the consumption
     * objects are compatible with the corresponding dataflow model.
     *
     * @param consumptionRate The new token consumption rates.
     */
    public void setConsumptionRate(Object consumptionRate) {
        _consumptionRate = consumptionRate;
    }

    /**
     * Set the delay of the associated DIF edge.
     * This method should be overridden in more specialized
     * edge weight classes to ensure that the delay
     * objects are compatible with the corresponding dataflow model.
     *
     * @param delay The new delay object.
     */
    public void setDelay(Object delay) {
        _delay = delay;
    }

    /**
     * Set the token production rate of the associated DIF edge.
     * This method should be overridden in more specialized
     * edge weight classes to ensure that the production
     * objects are compatible with the corresponding dataflow model.
     *
     * @param productionRate The new token production rates.
     */
    public void setProductionRate(Object productionRate) {
        _productionRate = productionRate;
    }

    /**
     * Set the sink port of the associated DIF edge to a port
     * that is represented by the sink of the edge.
     *
     * @param sinkPort The sink port.
     */
    public void setSinkPort(Object sinkPort) {
        _sinkPort = sinkPort;
    }

    /**
     * Set the source port of the associated DIF edge to a port
     * that is represented by the source of the edge.
     *
     * @param sourcePort The source port.
     */
    public void setSourcePort(Object sourcePort) {
        _sourcePort = sourcePort;
    }

    /**
     * Return a string representation of the edge weight (an empty string).
     *
     * @return The string representation of the edge weight (an empty string).
     */
    public String toString() {
        return new String("");
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /**
     * Return the token consumption rate of the associated DIF edge.
     *
     * @return The token consumption rate.
     */
    protected final Object _getConsumptionRate() {
        return _consumptionRate;
    }

    /**
     * Return the delay of the associated DIF edge.
     *
     * @return The delay.
     */
    protected final Object _getDelay() {
        return _delay;
    }

    /**
     * Return the token production rate of the associated DIF edge.
     *
     * @return The token production rate.
     */
    protected final Object _getProductionRate() {
        return _productionRate;
    }

    /**
     * Set the token consumption rate of the associated DIF edge.
     *
     * @param consumptionRate The new token consumption rate.
     */
    protected final void _setConsumptionRate(Object consumptionRate) {
        _consumptionRate = consumptionRate;
    }

    /**
     * Set the delay of the associated DIF edge.
     *
     * @param delay The delay.
     */
    protected final void _setDelay(Object delay) {
        _delay = delay;
    }

    /**
     * Set the token production rate of the associated DIF edge.
     *
     * @param productionRate The new token production rate.
     */
    protected final void _setProductionRate(Object productionRate) {
        _productionRate = productionRate;
    }


    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    // The delay of the associated DIF edge.
    private Object _delay;

    // The sink port associated with an DIF edge if there is one
    // (otherwise, null).
    private Object _sinkPort;

    // The source port associated with an DIF edge if there is one
    // (otherwise, null).
    private Object _sourcePort;

    // The token consumption rate of the associated DIF edge.
    private Object _consumptionRate;

    // The token production rate of the associated DIF edge.
    private Object _productionRate;
}


