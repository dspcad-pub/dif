/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.fsm;

import dif.DIFGraph;

import dif.util.graph.Node;

//////////////////////////////////////////////////////////////////////////
//// FSMGraph
/** Information and computations associated with FSMGraph.
FSMGraph represents the state transition graph of finite state machine (FSM)
model. In FSMGraph, Node represents state and Edge represents state transition.
<p>
@author Chia-Jui Hsu, Ruirui Gu
@version $Id: FSMGraph.java 1871 2008-01-08 22:13:59Z plishker $
*/

public class FSMGraph extends DIFGraph {

    /** Construct an empty FSM state transition graph.
     */
    public FSMGraph() {
        super();
    }

    /** Construct an empty FSM state transition graph with enough storage
     *  allocated for the specified number of nodes (states).
     *  @param nodeCount The number of nodes.
     */
    public FSMGraph(int nodeCount) { super(nodeCount); }

    /** Construct an empty FSM state transition graph with enough storage 
     *  allocated for the specified number of edges (state transitions), 
     *  and number of nodes (states).
     *  @param nodeCount The integer specifying the number of nodes
     *  @param edgeCount The integer specifying the number of edges
     */
    public FSMGraph(int nodeCount, int edgeCount) {
        super(nodeCount, edgeCount);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Get the initial state.
     *  @return The initial state (Node).
     */
    public Node getInitialState() {
        return _initialState;
    }

    /** Set the initial state.
     *  @param node The initial state.
     *  @exception IllegalArgumentException If node is not in the graph.
     */
    public void setInitialState(Node node) {
        if (containsNode(node)) {
            _initialState = node;
        } else {
            throw new IllegalArgumentException(node.toString() 
                    + " is not in the graph.");
        }
    }

    /** Verify edge weight for FSMGraph.
     *  @param weight The edge weight to verify.
     *  @return True if the given edge weight is valid for FSMGraph.
     */
    public boolean validEdgeWeight(Object weight) {
        return weight instanceof FSMEdgeWeight;
    }

    /** Verify node weight for FSMGraph.
     *  @param weight The node weight to verify.
     *  @return True if the given node weight is valid for FSMGraph.
     */
    public boolean validNodeWeight(Object weight) {
        return weight instanceof FSMNodeWeight;
    }
    
    ///////////////////////////////////////////////////////////////////
    ////                    protected methods                      ////

    ///////////////////////////////////////////////////////////////////
    ////                      private variables                    ////

    private Node _initialState = null;
}

