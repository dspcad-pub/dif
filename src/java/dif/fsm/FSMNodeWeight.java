/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.fsm;

import dif.DIFNodeWeight;
import dif.util.graph.Node;

//////////////////////////////////////////////////////////////////////////
//// FSMNodeWeight
/** Information associated with a FSM node (state).
{@link FSMNodeWeight}s are objects associated with {@link Node}s
that represent FSM nodes (states) in {@link dif.fsm.FSMGraph}s.
This class caches frequently-used data associated with FSM nodes.

@author Chia-Jui Hsu, Ruirui Gu
@version $Id: FSMNodeWeight.java 1871 2008-01-08 22:13:59Z plishker $
*/
    
public class FSMNodeWeight extends DIFNodeWeight {

    /** Createa a FSM node weight with null computation.
     */ 
    public FSMNodeWeight() {
        setComputation(null);
    }
    
    /** Create a FSM node weight given a computation that is to be 
     *  represented by the FSM node.
     *  @param computation the computation to be represented by the FSM node.
     */
    public FSMNodeWeight(Object computation) {
        setComputation(computation);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Return a string representation of the FSM node that is represented
     *  by this node weight. 
     *  @return the string representation.
     */
    public String toString() {
        String result = new String();
        result += "computation: ";
        if (getComputation() == null) {
            result += "null";
        } else {
            if (getComputation() instanceof String) {
                result += getComputation();
            } else {
                result += getComputation().getClass().getName();
            } 
        }
        return result; 
    }

}

