/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.fsm;

import dif.DIFHierarchy;
import dif.DIFParameter;
import dif.graph.hierarchy.Port;

import dif.util.graph.Node;
import dif.util.graph.Edge;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.HashMap;
import java.util.LinkedList;

import javax.xml.parsers.DocumentBuilder; 
import javax.xml.parsers.DocumentBuilderFactory;  
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import java.io.IOException;
import java.lang.String;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

//////////////////////////////////////////////////////////////////////////
//// CALReader
/** Export CAL actor to FSMGraph.
CALReader reads calml file (XML version of CAL) and exports a CAL actor 
to a DIFHierarchy-FSMGraph.

@author Chia-Jui Hsu, Ruirui Gu
@version $Id: CALReader.java 2031 2008-07-18 01:46:15Z ruirui $
*/

public class CALReader {
    
    /** Constructor.
     *  @param file The input calml file name.
     */
    public CALReader(String file) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            _document = builder.parse(file);
        } catch (SAXException exp) {
            throw new RuntimeException(exp.getMessage());
        } catch (ParserConfigurationException exp) {
            throw new RuntimeException(exp.getMessage());
        } catch (IOException exp) {
            throw new RuntimeException(exp.getMessage());
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////                       public methods                      ////

    /** Exports the input CAL actor to a DIFHierarchy containing a FSMGraph.
     *  @return The DIFHierarchy object.
     *  @exception RuntimeException If CALReader fails to export CAL-tag element
     *  to a DIFHierarchy.
     */
    public DIFHierarchy export() {
        //CAL XML contains only one CAL-tag element.
        Element element = _document.getDocumentElement();
        if (!element.getTagName().equals("Actor")) {
            throw new RuntimeException("The root element is not Actor-tag.");
        }
        
        return exportActor(element);
    }

    ///////////////////////////////////////////////////////////////////
    ////                     protected methods                     ////
    
    /** Exports an Actor-tag element, i.e., XML version of a CAL actor.
     *  An Actor-tag element is converted to a DIFHierarchy containing a FSMGraph.
     *  @param element An Actor-tag element.
     *  @return A DIFHierarchy object mimicking the input Actor-tag
     *  <i>element</i>.
     */
    protected DIFHierarchy exportActor(Element element) {
        //Hierarchy's and graph's name is set to the name attribute of 
        //the Actor element.
        String name = element.getAttribute("name");
        _graph = new FSMGraph();
        _graph.setName(name);
        _hierarchy = new DIFHierarchy(_graph, name);

        NodeList nodeList;
        int nodeLength;
        
        //Port <- Actor-Port
        nodeList = element.getElementsByTagName("Port");
        nodeLength = nodeList.getLength();
        for (int i=0; i<nodeLength; i++) {
            exportPort((Element)nodeList.item(i));
        }

        //DIFParameter <- Actor-Action
        nodeList = element.getElementsByTagName("Action");
        nodeLength = nodeList.getLength();
        for (int i=0; i<nodeLength; i++) {
            exportAction((Element)nodeList.item(i));
        }

        //FSMGraph <- Actor-Schedule (fsm)
        nodeList = element.getElementsByTagName("Schedule");
        nodeLength = nodeList.getLength();
        if (nodeLength == 1) {
            Element scheduleElement = (Element)nodeList.item(0);
            if (!scheduleElement.getAttribute("kind").equals("fsm")) {
                throw new RuntimeException("Actor " + name 
                    + "\'s schedule is not FSM");
            }
            exportSchedule(scheduleElement);
        } else if (nodeLength > 1) {
            throw new RuntimeException("Actor " + name 
                    + " has multiple schedules");
        }

        //Priority?

        return _hierarchy;
    }

    /** Exports a Port-tag element.
     *  A Port-tag element is converted to a Port of the DIFHierarchy.
     *  @param element A Port-tag element.
     */
    protected void exportPort(Element element) {
        //Port name <- Port--name
        String name = element.getAttribute("name");
        //Port direction <- Port--kind
        String direction = element.getAttribute("kind");
        
        //Either "Input" or "Output"
        if (direction.equals("Input")) {
            Port port = new Port(name, _hierarchy, Port.IN);
            _inPorts.add(port);
        } else if (direction.equals("Output")) {
            Port port = new Port(name, _hierarchy, Port.OUT);
            _outPorts.add(port);
        } else {
            throw new RuntimeException(
                    "Illegal \"kind\" attribute of Port-tag element: "
                    + direction);
        }        
    }

    /** Exports an Action-tag element.
     *  A CAL actor action is parameterized as a DIFParameter "actionId".
     *  Its value is an ArrayList of consumption rates in int[], production
     *  rates in int[], and a Boolean indicating whether the action is guarded.
     *  @param element An Action-tag element.
     */
    protected void exportAction(Element element) {
        //CAL actor action is parameterized as DIFParameter "actionId"
        //Id is the index of actions in order
        String name = "action" + String.valueOf(_actions.size());
        DIFParameter action = new DIFParameter(name);

        NodeList nodeList;
        int nodeLength;

        //initialize cnsRates and prdRates
	//ruirui: add one more parameter: rates 
        int numInPorts = _inPorts.size();
        int numOutPorts = _outPorts.size();
	int numPorts = numInPorts + numOutPorts;
        int[] cnsRates = new int[numInPorts];
        int[] prdRates = new int[numOutPorts];
	int[] rates = new int[numPorts];
        for (int i=0; i<numInPorts; i++) {
            cnsRates[i] = 0;
        }
        for (int i=0; i<numOutPorts; i++) {
            prdRates[i] = 0;
        }
	for (int i=0; i<numPorts; i++) {
	    rates[i] = 0;
	}

        //set cnsRates of this action
        nodeList = element.getElementsByTagName("Input");
        nodeLength = nodeList.getLength();
        for (int i=0; i<nodeLength; i++) {
            Element inputElement = (Element)nodeList.item(i);
            String portName = inputElement.getAttribute("port");

            int portIndex = 0;
            for (Iterator portIter = _inPorts.iterator(); 
                    portIter.hasNext();) {
                Port inPort = (Port)portIter.next();
                if (inPort.getName().equals(portName)) {
                    break;
                }
                portIndex++;
            }
            if (portIndex == _inPorts.size()) {
                throw new RuntimeException(
                        "Cannot find input port " + portName);
            }

            //rate is the number of "Decl" tags
            int cnsRate = inputElement.getElementsByTagName("Decl").getLength();
            cnsRates[portIndex] = cnsRate;
	    rates[portIndex] = cnsRate;
        }

        //set prdRates of this action
        nodeList = element.getElementsByTagName("Output");
        nodeLength = nodeList.getLength();
        for (int i=0; i<nodeLength; i++) {
            Element outputElement = (Element)nodeList.item(i);
            String portName = outputElement.getAttribute("port");

            int portIndex = 0;
            for (Iterator portIter = _outPorts.iterator(); 
                    portIter.hasNext();) {
                Port outPort = (Port)portIter.next();
                if (outPort.getName().equals(portName)) {
                    break;
                }
                portIndex++;
            }
            if (portIndex == _outPorts.size()) {
                throw new RuntimeException(
                        "Cannot find input port " + portName);
            }

            //rate is the number of "Decl" tags
            int prdRate = outputElement.getElementsByTagName("Expr").getLength();
            prdRates[portIndex] = prdRate;
	    rates[numInPorts+portIndex] = prdRate;
        }

        //set whether this action is guarded
        Boolean guard = new Boolean(
                element.getElementsByTagName("Guards").getLength() > 0);

        //action's value is an ArrayList of cnsRates, prdRates, and guard
        ArrayList value = new ArrayList();
	//here, the entry in each action is added in "single-data" way, no []
	for (int i=0; i<numPorts; i++) {
	    value.add(rates[i]); 
	}
   //    value.add(rates);
   //     value.add(prdRates);
        value.add(guard);
        action.setValue(value);

        //if the action has a tag, put it in _tags map
        nodeList = element.getElementsByTagName("QID");
        nodeLength = nodeList.getLength();
        if (nodeLength == 1) {
            String tag = ((Element)nodeList.item(0)).getAttribute("name");
            if (_tags.containsKey(tag)) {
                ((LinkedList)_tags.get(tag)).add(action);
            } else {
                LinkedList actions = new LinkedList();
                actions.add(action);
                _tags.put(tag, actions);
            }
        } else if (nodeLength > 1) {
            throw new RuntimeException("Action has multiple tags");
        }

        //put action in _actions and the FSMGraph
        _actions.add(action);
        _graph.setParameter(action);
    }

    /** Exports an Schedule-tag element.
     *  A CAL actor FSM schedule is represented by a FSMGraph in DIF.
     *  A FSM state is mapped to a Node, and a FSM state transition is mapped to
     *  an Edge. Initial state is also set.
     *  @param element An Schedule-tag element.
     */
    protected void exportSchedule(Element element) {
        //set initial state
        String name = element.getAttribute("initial-state");
        Node node = new Node(new FSMNodeWeight());
        _graph.addNode(node);
        _graph.setName(node, name);
        _graph.setInitialState(node);
        _nodes.put(name, node);

        NodeList nodeList = element.getElementsByTagName("Transition");
        int nodeLength = nodeList.getLength();
        for (int i=0; i<nodeLength; i++) {
            Element transitionElement = (Element)nodeList.item(i);
            String sourceName = transitionElement.getAttribute("from");
            String sinkName = transitionElement.getAttribute("to");
            Node source = null;
            Node sink = null;
            
            if (_nodes.containsKey(sourceName)) {
                source = (Node)_nodes.get(sourceName);
            } else {
                 source = new Node(new FSMNodeWeight());
                _graph.addNode(source);
                _graph.setName(source, sourceName);
                _nodes.put(sourceName, source);
            }

            if (_nodes.containsKey(sinkName)) {
                sink = (Node)_nodes.get(sinkName);
            } else {
                 sink = new Node(new FSMNodeWeight());
                _graph.addNode(sink);
                _graph.setName(sink, sinkName);
                _nodes.put(sinkName, sink);
            }

            Edge edge = new Edge(source, sink, new FSMEdgeWeight());

            //input actions of state transition
            LinkedList inputActions = new LinkedList();
            NodeList nList = ((Element)transitionElement.
                    getElementsByTagName("ActionTags").item(0)).
                    getElementsByTagName("QID");
            int nLength = nList.getLength();
            for (int j=0; j<nLength; j++) {
                String tag = ((Element)nList.item(j)).getAttribute("name");
                if (_tags.containsKey(tag)) {
                    LinkedList actions = (LinkedList)_tags.get(tag);
                    inputActions.addAll(actions);
                } else {
                    throw new RuntimeException("Cannot find action tag " + tag);
                }
            }

            if (inputActions.size() == 1) {
                ((FSMEdgeWeight)edge.getWeight()).setInput(inputActions.getFirst());
            } else if (inputActions.size() > 1) {
                ((FSMEdgeWeight)edge.getWeight()).setInput(inputActions);
            } else {
                throw new RuntimeException("No input action");
            }
            
            _graph.addEdge(edge);
            _graph.setName(edge, "e" + String.valueOf(_graph.edgeCount() - 1));
        }

    }

    ///////////////////////////////////////////////////////////////////
    ////                    private variables                      ////

    //actions (DIFParameter)
    LinkedList _actions = new LinkedList();

    //XML document
    Document _document;

    //FSMGraph that represents the CAL actor
    FSMGraph _graph;

    //DIFHierarchy that encloses the FSMGraph of the CAL actor
    DIFHierarchy _hierarchy;

    //input ports
    LinkedList _inPorts = new LinkedList();

    //map node name (state name) to Node
    HashMap _nodes = new HashMap();

    //output ports
    LinkedList _outPorts = new LinkedList();

    //map action_tag to LinkedList of actions (DIFParameter)
    HashMap _tags = new HashMap();
}

