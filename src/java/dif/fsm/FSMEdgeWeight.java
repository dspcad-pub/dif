/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.fsm;

import dif.DIFEdgeWeight;

import dif.util.graph.Edge;
//import ptolemy.math.ExtendedMath;

//////////////////////////////////////////////////////////////////////////
//// FSMEdgeWeight
/** Information associated with an FSM edge (state transition).
FSMEdgeWeights are objects associated with {@link Edge}s
that represent FSM edges in {@link dif.fsm.FSMGraph}s.
This class caches frequently-used data associated with FSM edges 
(state transitions), i.e., input and output.
</p>
Production rate, consumption rate, and delay in {@link DIFEdgeWeight}
are not used in FSMEdgeWeight.
</p>
@author Chia-Jui Hsu, Ruirui Gu
@version $Id: FSMEdgeWeight.java 1871 2008-01-08 22:13:59Z ruirui $
*/

public class FSMEdgeWeight extends DIFEdgeWeight {

    /** Construct an edge weight for a state transition.
     *  The input and output of the state transition is null.
     */
    public FSMEdgeWeight() {
        super();
        _input = null;
        _output = null;
    }

    /** Construct an edge weight for a state transition.
     *  @param input The input of the state transition.
     *  @param output The output of the state transition.
     */
    public FSMEdgeWeight(Object input, Object output) {
        super();
        _input = input;
        _output = output;
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Get input of the state transition.
     *  @return The input of the state transition.
     */
    public Object getInput() {
        return _input;
    }

    /** Get output of the state transition.
     *  @return The output of the state transition.
     */
    public Object getOutput() {
        return _output;
    }

    /** Set input of the state transition.
     *  @param input The input of the state transition.
     */
    public void setInput(Object input) {
        _input = input;
    }

    /** Set output of the state transition.
     *  @param output The output of the state transition.
     */
    public void setOutput(Object output) {
        _output = output;
    }

    /** Return a string representation of the edge weight. This string
     *  representation is in the following form:
     *  <p>
     *  <em> input: ...
     *       output: ...</em>
     *  <p>
     * @return the string representation of the edge weight.
     */
    public String toString() {
        StringBuffer str = new StringBuffer();
        str.append("input: ");
	if (_input!=null) {
        	str.append(_input.toString());
	}
        str.append("\noutput: ");
	if(_output!=null) {
        	str.append(_output.toString());
	}
        return str.toString();
    }

    ///////////////////////////////////////////////////////////////////
    ////                      private variables                    ////

    // The input of the state transition.
    private Object _input;

    // The output of the state transition.
    private Object _output;
}

