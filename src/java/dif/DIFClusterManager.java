/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import dif.util.graph.Edge;
import dif.util.graph.Node;

//////////////////////////////////////////////////////////////////////////
//// DIFClusterManager

/**
 * A graph class specializing in maintaining cluster hierarchy. It is usually
 * instanciated to distinguished away from the original flattened graph.
 * Clustering is quite often a useful methodology in algorithm design because
 * of the reduced graph size and complexity. The characteristic of hierarchy is
 * similar to {@link DIFHierarchy}. However, <code>DIFHierarchy</code> aims
 * more at application design style than at clustering functionality.
 * <p>
 * <em>Caution:</em> Any derived cluster manager should consider overriding
 * {@link #_newEdgeWeight()}, {@link #_newNodeWeight()}, and
 * {@link #_clusterNodesComplete(DIFGraph, Collection, Node)} with appropriate
 * edge weight, node weight, and clustering strategy, respectively.
 *
 * @author Mingyung Ko
 * @version $Id: DIFClusterManager.java 606 2008-10-08 16:29:47Z plishker $
 */

public class DIFClusterManager {

    /**
     * A constructor with the original graph. The original graph must be an
     * instance of {@link DIFGraph}.
     */
    public DIFClusterManager(DIFGraph graph) {
        _initialize(graph);
    }

    ///////////////////////////////////////////////////////////////////
    ////                        public methods                     ////

    /**
     * Cluster a collection of nodes in the given graph and replace it
     * with a super node. The newly created super node is returned. The
     * given graph can be the top level graph or any lower level subgraph.
     * If it is a subgraph, then the reachability won't be updated.
     * Reachability computation is only for the top level.
     *
     * @param graph          The given graph.
     * @param nodeCollection The collection of nodes to cluster.
     * @return The super node in place of the node collection.
     */
    public Node clusterNodes(DIFGraph graph, Collection nodeCollection) {
        Node superNode = _newSuperNode();
        List clusterResults = _clusterNodesComplete(
                graph, nodeCollection, superNode);
        // Save the mapping for super nodes.
        DIFGraph subgraph = (DIFGraph) clusterResults.get(0);
        _setSuperNode(superNode, subgraph);
        // Save the mapping for new and old edges.
        Map edgeMap = (Map) clusterResults.get(1);
        _edgeMap.putAll(edgeMap);
        return superNode;
    }

    /**
     * Cluster a collection of nodes and replace it with a super node.
     * All nodes in the collection must be in top level graph and cannot
     * be in the subgraph. The newly created super node is returned.
     *
     * @param nodeCollection The collection of nodes to cluster.
     * @return The super node in place of the node collection.
     */
    public Node clusterNodes(Collection nodeCollection) {
        Node superNode = clusterNodes(_graph, nodeCollection);
        _updateReachability(superNode, nodeCollection);
        return superNode;
    }

    /**
     * Get the clustered graph.
     *
     * @return The clustered graph.
     */
    public DIFGraph getGraph() {
        return _graph;
    }

    /**
     * Get the very original edge before any clustering. New edges are
     * created due to super nodes introduced. The relation between the
     * new edge and its original edge is kept. This may be a useful
     * information in determining alternative candidates selection or
     * tie breaking algorithms.
     *
     * @param newEdge The new edge induced by clustering.
     * @return The original edge before any clustering.
     */
    public Edge getOriginalEdge(Edge newEdge) {
        Edge start = newEdge;
        Edge next = (Edge) _edgeMap.get(start);
        while (next != null) {
            start = next;
            next = (Edge) _edgeMap.get(start);
        }
        return start;
    }

    /**
     * Get the root node if the graph is clustered into a single node.
     *
     * @return The root node.
     */
    public Node getRootNode() {
        if (_graph.nodeCount() > 1) throw new RuntimeException(
                "No valid root node can be found.");
        return _graph.node(0);
    }

    /**
     * Get the subgraph corresponding to the super node.
     *
     * @param superNode The super node.
     * @return The associated subgraph.
     */
    public DIFGraph getSubgraph(Node superNode) {
        return (DIFGraph) _superNodesMap.get(superNode);
    }

    /**
     * Test the given node super or not.
     *
     * @param node The given node.
     * @return True if it is a super node.
     */
    public boolean isSuperNode(Node node) {
        return (_superNodesMap.get(node) != null);
    }

    /**
     * Test whether the clustering of the node collection leads to acyclic
     * topology. This method is for testing purpose and the actual clustering
     * is not performed.
     * <p>
     * <em>Caution:</em> The present graph status should be acyclic. This
     * method tests cyclic/acyclic property ONLY due to clustering.
     *
     * @param nodeCollection The node collection to test acyclic clustering.
     * @return True if the clustering will lead to acyclic topology;
     * false otherwise.
     */
    public boolean testAcyclicClustering(Collection nodeCollection) {
        // Compute predecessors of the cluster. They are union of the given
        // nodes' predecessors except the nodes themselves.
        Set predecessorsSet = new HashSet();
        Iterator cNodes = nodeCollection.iterator();
        while (cNodes.hasNext()) predecessorsSet.addAll(
                _graph.predecessors((Node) cNodes.next()));
        predecessorsSet.removeAll(nodeCollection);

        // If any node (in the collection) reaches any node's predecessor,
        // then the clustering will lead to cyclic topology.
        return !_isReachable(nodeCollection, predecessorsSet);
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /**
     * Given a graph and a collection of nodes, replace the subgraph induced
     * by the node collection with a single node N. Depending on users'
     * demand of the clustering functions, multiple results can be
     * generated and returned. Therefore, a <code>List</code> is returned
     * which contains these results and users should be aware of what
     * they are (and their squence in the list). By default, this method
     * returns both the induced subgraph and a map from newly created
     * edges to old edges replaced.
     * <p>
     * <em>Caution:</em> The clustering implementation considers topology
     * only. Any attributes other than topology may also need to be changed
     * in clustering. Therefore, all sub-classes should consider
     * approriate modifications to all attributes.
     *
     * @param graph          The given graph.
     * @param nodeCollection The collection of nodes.
     * @param superNode      The super node that will replace the node
     *                       collection.
     * @return A list with the first element the subgraph and the second
     * element a map of edges.
     */
    protected List _clusterNodesComplete(DIFGraph graph,
                                         Collection nodeCollection,
                                         Node superNode) {

        // Collect the crossing edges between the subgraph and the
        // remaining part of the graph.
        System.out.println("ClusterNodesComplete");
        DIFGraph subGraph = (DIFGraph) graph.subgraph(nodeCollection);
        ArrayList oldEdges = new ArrayList();
        Iterator nodes = nodeCollection.iterator();
        while (nodes.hasNext()) {
            Node node = (Node) nodes.next();
            Iterator edges = graph.incidentEdges(node).iterator();
            while (edges.hasNext()) {
                Edge edge = (Edge) edges.next();
                if (!subGraph.containsEdge(edge)) oldEdges.add(edge);
            }
        }
        // Create new edges connected to/from the super node. The edges
        // are just created and not added to graph yet.
        ArrayList newEdges = new ArrayList();
        Iterator edges = oldEdges.iterator();
        while (edges.hasNext()) {
            Edge edge = (Edge) edges.next();
            Node source = edge.source();
            Node sink = edge.sink();
            if (nodeCollection.contains(source)) newEdges.add(
                    new Edge(superNode, sink, _newEdgeWeight()));
            else newEdges.add(new Edge(source, superNode, _newEdgeWeight()));
        }
        // Add the super node, new edges, and remove old edge/nodes in the
        // graph. Put the mapping from old to new edges in the edge map.
        graph.addNode(superNode);
        Map edgeMap = new HashMap();
        for (int i = 0; i < oldEdges.size(); i++) {
            Edge oldEdge = (Edge) oldEdges.get(i);
            Edge newEdge = (Edge) newEdges.get(i);
            graph.addEdge(newEdge);
            graph.removeEdge(oldEdge);
            edgeMap.put(newEdge, oldEdge);
        }
        nodes = nodeCollection.iterator();
        while (nodes.hasNext()) graph.removeNode((Node) nodes.next());

        List result = new ArrayList();
        result.add(subGraph);
        result.add(edgeMap);
        return result;
    }

    /**
     * Return the new/old edge map after/before a clustering step.
     *
     * @return The edge map.
     */
    protected Map _getEdgeMap() {
        return _edgeMap;
    }

    /**
     * Test whether the second node is reachable from the first.
     *
     * @param node1 The first node.
     * @param node2 The second node.
     * @return True if <code>node2</code> is reachable from
     * <code>node1</code>; false otherwise.
     */
    protected boolean _isReachable(Node node1, Node node2) {
        return _reachOutNodes(node1).contains(node2);
    }

    /**
     * Test whether the second node collection is reachable from the first.
     *
     * @param nodeCollection1 The first node collection.
     * @param nodeCollection2 The second node collection.
     * @return True if <code>nodeCollection2</code> is reachable from
     * <code>nodeCollection1</code>; false otherwise.
     */
    protected boolean _isReachable(Collection nodeCollection1,
                                   Collection nodeCollection2) {
        Collection reachableNodes = _reachOutNodes(nodeCollection1);
        // If the reachable nodes include any nodes in the second collection,
        // then it is reachable from the first node collection to the second.
        reachableNodes.retainAll(nodeCollection2);
        return (!reachableNodes.isEmpty());
    }

    /**
     * Return a valid edge weight for a newly created edge in clustering
     * process.
     *
     * @return A valid edge weight.
     */
    protected DIFEdgeWeight _newEdgeWeight() {
        return new DIFEdgeWeight();
    }

    /**
     * Return a valid node weight for a newly created node in clustering
     * process.
     *
     * @return A valid node weight.
     */
    protected DIFNodeWeight _newNodeWeight() {
        return new DIFNodeWeight();
    }

    /**
     * Return a new super node with weight type suitable for use in
     * this graph. This method should be overridden in derived classes
     * to return the appropriate type of node weight with the appropriate
     * initialization.
     *
     * @return A new super node with appropriate node weight.
     */
    protected Node _newSuperNode() {
        return new Node(_newNodeWeight());
    }

    /**
     * Get the nodes that reach to the given node.
     *
     * @param node The given node.
     * @return Nodes that reach to the given node.
     */
    protected Collection _reachInNodes(Node node) {
        Set reachInNodes = new HashSet();
        Iterator allNodes = _reachability.keySet().iterator();
        while (allNodes.hasNext()) {
            Node thisNode = (Node) allNodes.next();
            if (_isReachable(thisNode, node)) reachInNodes.add(thisNode);
        }
        return reachInNodes;
    }

    /**
     * Get the nodes that reach to the given node collection.
     *
     * @param nodeCollection The given node node collection.
     * @return Nodes that reach to the given node collection.
     */
    protected Collection _reachInNodes(Collection nodeCollection) {
        Set reachInNodes = new HashSet();
        Iterator nodes = nodeCollection.iterator();
        while (nodes.hasNext()) reachInNodes.addAll(
                _reachInNodes((Node) nodes.next()));
        return reachInNodes;
    }

    /**
     * Get the nodes that can be reached from the given node.
     *
     * @param node The given node.
     * @return The nodes that can be reached.
     */
    protected Collection _reachOutNodes(Node node) {
        return new HashSet((Collection) _reachability.get(node));
    }

    /**
     * Get the nodes that can be reached from the given node collection.
     *
     * @param nodeCollection The given node Collection.
     * @return The nodes that can be reached.
     */
    protected Collection _reachOutNodes(Collection nodeCollection) {
        Set reachOutNodes = new HashSet();
        Iterator nodes = nodeCollection.iterator();
        while (nodes.hasNext()) reachOutNodes.addAll(
                _reachOutNodes((Node) nodes.next()));
        return reachOutNodes;
    }

    /**
     * Set a node as super node with the associated graph.
     *
     * @param superNode The node to set as super.
     * @param subgraph  The associated subgraph.
     */
    protected void _setSuperNode(Node superNode, DIFGraph subgraph) {
        if (_superNodesMap.containsKey(superNode)) throw new RuntimeException(
                "The super node already exists.");
        else _superNodesMap.put(superNode, subgraph);
    }

    /**
     * Update reachability for a given super node and the associated
     * node collection.
     *
     * @param superNode      The super node.
     * @param nodeCollection The nodes associated with the super node.
     */
    protected void _updateReachability(Node superNode,
                                       Collection nodeCollection) {
        // Compute reachable nodes from the super node.
        Collection reachOutFromSuper = _reachOutNodes(nodeCollection);
        _reachability.put(superNode, reachOutFromSuper);

        // Update reachability.
        Collection reachInNodesCollection = _reachInNodes(nodeCollection);
        Iterator reachInNodes = reachInNodesCollection.iterator();
        while (reachInNodes.hasNext()) {
            Node node = (Node) reachInNodes.next();
            Collection reachOutNodes = (Collection) _reachability.get(node);
            // All nodes in the given collection are removed and cannot be
            // reached anymore. On the other hand, the super node and all
            // its reachable nodes can be reached.
            reachOutNodes.removeAll(nodeCollection);
            reachOutNodes.add(superNode);
            reachOutNodes.addAll(reachOutFromSuper);
        }
        // Remove nodes in the collection from _reachability.
        Iterator nodes = nodeCollection.iterator();
        while (nodes.hasNext()) _reachability.remove(nodes.next());
    }

    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////

    /*  Initialize <code>Map</code> variables which maintain cluster
     *  hierarchy bookkeeping.
     *  @param originalGraph The original flattened graph.
     */
    private void _initialize(DIFGraph originalGraph) {
        _graph = (DIFGraph) originalGraph.clone();

        // Initialize the map for new/old edges tracking.
        _edgeMap = new HashMap();
        Iterator edges = _graph.edges().iterator();
        while (edges.hasNext()) _edgeMap.put(edges.next(), null);

        // Initialize _superNodesMap and _reachability.
        boolean[][] closure = _graph.transitiveClosure();
        _reachability = new HashMap();
        _superNodesMap = new HashMap();
        Iterator nodes = _graph.nodes().iterator();
        while (nodes.hasNext()) {
            Node thisNode = (Node) nodes.next();
            // _superNodesMap maps super nodes to the corresponding subgraphs.
            _superNodesMap.put(thisNode, null);
            // _reachability maintains reachable nodes for every node.
            Set reachableNodes = new HashSet();
            int thisNodeLabel = _graph.nodeLabel(thisNode);
            for (int i = 0; i < _graph.nodeCount(); i++)
                if (closure[thisNodeLabel][i]) reachableNodes.add(
                        _graph.node(i));
            _reachability.put(thisNode, reachableNodes);
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    // The hierarchically clustered graph.
    private DIFGraph _graph;

    // A map from super node to the induced subgraph.
    private Map _superNodesMap;

    // A map from new edges to old edges due to clustering.
    private Map _edgeMap;

    // Reachability converted from a transitive closure matirx.
    // The map maps a node to all nodes reachable.
    private Map _reachability;
}


