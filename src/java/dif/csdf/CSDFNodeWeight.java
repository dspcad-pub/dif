/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Information associated with an CSDF node. */

package dif.csdf;

import dif.DIFNodeWeight;
import dif.util.graph.Graph;
import dif.util.graph.Node;

//////////////////////////////////////////////////////////////////////////
//// CSDFNodeWeight
/** Information associated with an CSDF node.
{@link CSDFNodeWeight}s are objects associated with {@link Node}s
that represent CSDF nodes in {@link Graph}s.
This class caches frequently-used data associated with CSDF nodes.
It is also useful for representing intermediate CSDF graph representations
that do not correspond to Ptolemy II models, and performing graph
transformations (e.g. convertion to and manipulation of single-rate graphs).
It is intended for use with analysis/synthesis algorithms that operate
on generic graph representations of CSDF models.

@author Shuvra S. Bhattacharyya
@version $Id: CSDFNodeWeight.java 606 2008-10-08 16:29:47Z plishker $
@see Node
@see CSDFEdgeWeight
*/

public class CSDFNodeWeight extends DIFNodeWeight {

    /** Create an CSDF node weight given a computation that is to be represented
     *  by the CSDF node, and a given instance number. The computation may be
     *  any Object that represents
     *  a computation in some way (or null if no computation is to be
     *  represented by the node).
     *  The represented computation may, for example, be a Ptolemy II
     *  AtomicActor.  The instance number distinguishes multiple CSDF nodes that
     *  represent the same computation (e.g., an actor that has been decomposed
     *  into multiple units in an intermediate representation, or multiple
     *  execution phases of the same subsystem). Instances
     *  are numbered consecutively, starting at zero.
     *  @param computation the computation to be represented by the CSDF node.
     *  @param instanceNumber the instance number.
     */
    public CSDFNodeWeight(Object computation, int instanceNumber) {
        setComputation(computation);
        _instanceNumber = instanceNumber;
    }

    /** Construct an CSDF node weight given a computation that is to be
     *  represented.  Assume this is the first (number 0) instance of the
     *  computation.
     *  @param computation the computation to be represented by the CSDF node.
     */
    public CSDFNodeWeight(Object computation) {
        this(computation, 0);
    }

    /** Construct an CSDF node weight with no computation to be represented
     *  by the associated CSDF node.
     *  @see #CSDFNodeWeight(Object, int)
     */
    public CSDFNodeWeight() {
        this(null, 0);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Return the instance number of the associated CSDF node.
     *  @return the instance number.
     *  @see #CSDFNodeWeight(Object, int).
     */
    public int getInstanceNumber() {
        return _instanceNumber;
    }

    /** Set the instance number of the CSDF node.
     *  @param instanceNumber the instance number.
     *  @see #CSDFNodeWeight(Object, int).
     */
    public void setInstanceNumber(int instanceNumber) {
        _instanceNumber = instanceNumber;
    }

    /** Return a string representation of the CSDF node that is represented
     *  by this node weight.
     *  @return the string representation.
     */
    public String toString() {
        String result = new String();
        result += "computation: ";
        if (getComputation() == null) {
            result += "null";
        } else {
            if (getComputation() instanceof String) {
                result += getComputation();
            } else {
                result += getComputation().getClass().getName();
            }
        }
        result += ", instance: " + _instanceNumber;
        return result;
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    // The instance number of the associated CSDF node with respect to the
    // associated computation.
    private int _instanceNumber;

}


