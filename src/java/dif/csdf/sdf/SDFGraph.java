/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Information associated with an SDF graph. */

package dif.csdf.sdf;

import java.util.Collection;
import java.util.Iterator;

import dif.util.graph.Edge;
import dif.util.graph.Node;
import dif.util.graph.Graph;
import dif.data.ExtendedMath;

import dif.csdf.CSDFGraph;
import dif.CoreFunctionNode;

//////////////////////////////////////////////////////////////////////////
//// SDFGraph
/** Information associated with an SDF graph.
This class caches frequently-used data associated with SDF graphs.
It is also useful for intermediate SDF graph representations
(between the application model and implementation), and performing graph
transformations (e.g. convertion to and manipulation of single-rate graphs).
It is intended for use with analysis/synthesis algorithms that operate
on generic graph representations of SDF models.
<p>
SDFGraph nodes and edges have weights of type {@link SDFNodeWeight} and
{@link SDFEdgeWeight}, respectively.

@author Ming-Yung Ko, Shuvra S. Bhattacharyya
@version $Id: SDFGraph.java 606 2008-10-08 16:29:47Z plishker $
@see SDFEdgeWeight
@see SDFNodeWeight
*/

public class SDFGraph extends CSDFGraph {

    /** Construct an empty SDF graph.
     */
    public SDFGraph() {
        super();
    }

    /** Construct an empty SDF graph with enough storage allocated for the
     *  specified number of nodes.
     *  @param nodeCount The number of nodes.
     */
    public SDFGraph(int nodeCount) {
        super(nodeCount);
    }

    /** Construct an empty SDF graph with enough storage allocated for the
     *  specified number of edges, and number of nodes.
     *  @param nodeCount The integer specifying the number of nodes
     *  @param edgeCount The integer specifying the number of edges
     */
    public SDFGraph(int nodeCount, int edgeCount) {
        super(nodeCount, edgeCount);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** <em>Absolute Buffer Memory Lower Bound</em> of the graph. It is
     *  sum of every edge's ABMLB value. See {@link #ABMLB(Edge)}.
     *  @return The absolute buffer lower bound for whole graph.
     */
    public int ABMLB() {
        int bound = 0;
        Iterator edges = edges().iterator();
        while (edges.hasNext())
            bound += ABMLB((Edge)edges.next());
        return bound;
    }

    /** <em>Absolute Buffer Memory Lower Bound</em> of a given edge.
     *  @param edge The given edge.
     *  @return The absolute buffer lower bound.
     */
    public int ABMLB(Edge edge) {
        SDFEdgeWeight weight = (SDFEdgeWeight)edge.getWeight();
        int production  = weight.productionPeriodRate();
        int consumption = weight.consumptionPeriodRate();
        int gcd = ExtendedMath.gcd(production, consumption);
        return (production + consumption - gcd);
    }

    /** <em>Buffer Memory Lower Bound</em> of the graph. It is sum
     *  of every edge's BMLB value. See {@link #BMLB(Edge)}. The bound is for
     *  single appearance schedules only.
     *  @return The lower bound for whole graph.
     */
    public int BMLB() {
        int bound = 0;
        Iterator edges = edges().iterator();
        while (edges.hasNext()) {
            bound += BMLB((Edge)edges.next());
        }
        return bound;
    }

    /** <em>Buffer Memory Lower Bound</em> of a given edge. The bound is for
     *  single appearance schedules only.
     *  @param edge The given edge.
     *  @return The lower bound.
     */
    public int BMLB(Edge edge) {
        SDFEdgeWeight weight = (SDFEdgeWeight)edge.getWeight();
        int production  = weight.productionPeriodRate();
        int consumption = weight.consumptionPeriodRate();
        int delay       = weight.getIntDelay();
        int divisor = ExtendedMath.gcd(production, consumption);
        int tokenExchange = production * consumption / divisor;
        if (delay < tokenExchange)
            return (delay + tokenExchange);
        else
            return delay;
    }

    /** <em>Buffer Memory Upper Bound</em> of the graph. It is sum
     *  of every edge's BMUB value. See {@link #BMUB(Edge)}. The bound is for
     *  single appearance schedules only.
     *  @return The buffer upper bound for whole graph.
     */
    public int BMUB() {
        int bound = 0;
        Iterator edges = edges().iterator();
        while (edges.hasNext()) {
            bound += BMUB((Edge)edges.next());
        }
        return bound;
    }

    /** <em>Buffer Memory Upper Bound</em> of a given edge. The bound is for
     *  single appearance schedules only.
     *  @param edge The given edge.
     *  @return The buffer upper bound.
     */
    public int BMUB(Edge edge) {
        int delay = ((SDFEdgeWeight)edge.getWeight()).getIntDelay();
        return (delay + TNSE(edge));
    }

    /** Compute repetitions for a cluster of nodes. It is equal to GCD
     *  value of all component nodes' repetitions.
     *  @param nodeCollection The node cluster in <code>Collection</code>.
     *  @return The cluster repetitions.
     */
    public int clusterRepetitions(Collection nodeCollection) {
        // Empty node collection.
        if (nodeCollection.size() == 0)
            return 0;
        // Cluster repetition is GCD value of all nodes' repetitions.
        Iterator nodes = nodeCollection.iterator();
        int repetition = getRepetitions((Node)nodes.next());
        while (nodes.hasNext())
            repetition = ExtendedMath.gcd(
                    repetition, getRepetitions((Node)nodes.next()));
        return repetition;
    }

    /** The maximal sample exchanging rate in the graph.
     *  @return The maximal rate.
     */
    public int maxRate() {
        int maxRate = 1;
        Iterator edges = edges().iterator();
        while (edges.hasNext()) {
            Edge edge = (Edge)edges.next();
            SDFEdgeWeight weight = (SDFEdgeWeight)edge.getWeight();
            int pRate = weight.getSDFProductionRate();
            int cRate = weight.getSDFConsumptionRate();
            int subMax = Math.max(pRate, cRate);
            maxRate = (maxRate < subMax) ? subMax : maxRate;
        }
        return maxRate;
    }

    /** Returns the ratio of sample exchanging rate of 1 of this graph.
     *  @return The ratio.
     */
    public double rateOneRatio() {
        int rateCount = edgeCount() * 2;
        int rateOneCount = 0;
        Iterator edges = edges().iterator();
        while (edges.hasNext()) {
            Edge edge = (Edge)edges.next();
            SDFEdgeWeight weight = (SDFEdgeWeight)edge.getWeight();
            if (weight.getSDFProductionRate() == 1)
                rateOneCount++;
            if (weight.getSDFConsumptionRate() == 1)
                rateOneCount++;
        }
        return (double)rateOneCount / (double)rateCount;
    }

    /** "Total Number of Samples Exchanged" of a given edge. The value
     *  is equal to product of sink node repetitions and consumption
     *  rate, or product of source node repetitions and production rate.
     *
     *  @param edge The given edge.
     *  @return The number of samples exchanged.
     */
    public int TNSE(Edge edge) {
        int sinkRepetitions = getRepetitions(edge.sink());
        SDFEdgeWeight edgeWeight = (SDFEdgeWeight)edge.getWeight();
        int consumptionRate = edgeWeight.getSDFConsumptionRate();
        return (sinkRepetitions * consumptionRate);
    }

    /** Verify edge weight for SDF graph.
     *  @param weight The edge weight to verify.
     *  @return True if the given edge weight is valid for SDF graph.
     */
    public boolean validEdgeWeight(Object weight) {
        return weight instanceof SDFEdgeWeight;
    }

    /** Verify node weight for SDF graph.
     *  @param weight The node weight to verify.
     *  @return True if the given node weight is valid for SDF graph.
     */
    public boolean validNodeWeight(Object weight) {
        return weight instanceof SDFNodeWeight;
    }

    /** Overrides the method in <code>mocgraph.Graph</code> to copy the
     *  attributes as well. For each element copied, attributes will be copied
     *  together. Special attention should be given that parameter values
     *  are not copied therefore values of the parameterized attributes should
     *  be copied from the original graph manually (if desired).
     *  @param nodes The collection of nodes; each element is a {@link Node}.
     *  @return The induced subgraph with a runtime type of DIFGraph.
     */
    public Graph subgraph(Collection nodes) {
        SDFGraph subgraph = (SDFGraph)super.subgraph(nodes);
        return subgraph;
    }

}


