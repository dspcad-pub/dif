/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* SDFToDot class to use with dif.csdf.sdf.SDFGraph. */

package dif.csdf.sdf;

import java.util.Iterator;

import dif.util.graph.Edge;

import dif.csdf.CSDFToDot;

//////////////////////////////////////////////////////////////////////////
//// SDFToDot
/** DOT file generator for SDFGraph objects. It is used to create dot files as
an input to GraphViz tools. A DOT file is created by first defining an
SDFToDot object and then using the {@link #toFile} method. Node labels are set
to element names in DIFGraph and edge labels are set to production, consumption
and delay values.
@author Fuat Keceli
@version $Id: SDFToDot.java 633 2008-11-19 20:46:41Z plishker $
*/

public class SDFToDot extends CSDFToDot {

    /** Creates a DotGenerator object from an SDFGraph.
     *  @param graph An SDFGraph.
     */
    public SDFToDot(SDFGraph graph) {
        super(graph);
        for(Iterator graphEdges = _graph.edges().iterator();
            graphEdges.hasNext(); ) {
            Edge graphEdge = (Edge) graphEdges.next();
            SDFEdgeWeight weight = (SDFEdgeWeight) graphEdge.getWeight();
	    //WLP - I find the zeros on every edge distracting
	    //            setAttribute(graphEdge, "label",
            //        String.valueOf(weight.getDelay()));
            setAttribute(graphEdge, "headlabel",
                    String.valueOf(weight.getSDFProductionRate()));
            setAttribute(graphEdge, "taillabel",
                    String.valueOf(weight.getSDFConsumptionRate()));
        }
    }
}


