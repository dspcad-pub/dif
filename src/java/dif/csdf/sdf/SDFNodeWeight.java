/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Information associated with an SDF node. */

package dif.csdf.sdf;

import dif.csdf.CSDFNodeWeight;
import dif.util.graph.Graph;
import dif.util.graph.Node;

//////////////////////////////////////////////////////////////////////////
//// SDFNodeWeight
/** Information associated with an SDF node.
{@link SDFNodeWeight}s are objects associated with {@link Node}s
that represent SDF nodes in {@link Graph}s.
This class caches frequently-used data associated with SDF nodes.
It is also useful for representing intermediate SDF graph representations
that do not correspond to Ptolemy II models, and performing graph
transformations (e.g. convertion to and manipulation of single-rate graphs).
It is intended for use with analysis/synthesis algorithms that operate
on generic graph representations of SDF models.

@author Shuvra S. Bhattacharyya
@version $Id: SDFNodeWeight.java 606 2008-10-08 16:29:47Z plishker $
@see Node
@see SDFEdgeWeight
*/

public class SDFNodeWeight extends CSDFNodeWeight {

    /** Create an SDF node weight given a computation that is to be represented
     *  by the SDF node, and a given instance number. The computation may be
     *  any Object that represents
     *  a computation in some way (or null if no computation is to be
     *  represented by the node).
     *  The represented computation may, for example, be a Ptolemy II
     *  AtomicActor.  The instance number distinguishes multiple SDF nodes that
     *  represent the same computation (e.g., an actor that has been decomposed
     *  into multiple units in an intermediate representation, or multiple
     *  execution phases of the same subsystem). Instances
     *  are numbered consecutively, starting at zero.
     *  @param computation The computation to be represented by the SDF node.
     *  @param instanceNumber The instance number.
     */
    public SDFNodeWeight(Object computation, int instanceNumber) {
        super(computation, instanceNumber);
    }

    /** Construct an SDF node weight given a computation that is to be
     *  represented.  Assume this is the first (number 0) instance of the
     *  computation.
     *  @param computation The computation to be represented by the SDF node.
     */
    public SDFNodeWeight(Object computation) {
        this(computation, 0);
    }

    /** Construct an SDF node weight with no computation to be represented
     *  by the associated SDF node.
     *  @see #SDFNodeWeight(Object, int)
     */
    public SDFNodeWeight() {
        this(null, 0);
    }

    /** Returns the string "SDFNodeWeight".
     *  @return The string "SDFNodeWeight".
     */
    public String toString() {
        return this.getClass().getName();
    }

}


