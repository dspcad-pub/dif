/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A manager for hierarchical SDF cluster maintenance. */

package dif.csdf.sdf;

import java.util.Collection;
import java.util.List;
import dif.util.graph.Node;
import dif.DIFEdgeWeight;
import dif.DIFGraph;
import dif.cfdf.CFDFClusterManager;

//////////////////////////////////////////////////////////////////////////
//// SDFClusterManager
/**
A graph class specializing in maintaining SDF cluster hierarchy. It is
usually instanciated to distinguished away from the original flattened
graph.

@author Mingyung Ko
@version $Id: SDFClusterManager.java 606 2008-10-08 16:29:47Z plishker $
*/

public class SDFClusterManager extends CFDFClusterManager {

    /** A constructor with the original graph. The original graph must be an
     *  instance of {@link SDFGraph}.
     */
    public SDFClusterManager(SDFGraph graph) {
        super(graph);
    }

    ///////////////////////////////////////////////////////////////////
    ////                        public methods                     ////

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /** Given a collection of nodes in the given graph, replace the subgraph
     *  induced by the node collection with a single node N. Depending on
     *  users' demand of the clustering functions, multiple results can be
     *  generated and returned. Therefore, a <code>List</code> is returned
     *  which contains these results and users should be aware of what
     *  they are (and their squence in the list). By default, this method
     *  returns both the induced subgraph and a map from newly created
     *  edges to old edges replaced.
     *
     *  @param nodeCollection The collection of nodes.
     *  @param superNode The super node that will replace the node collection.
     *  @return A list with the first element the subgraph and the second
     *          element a map of edges.
     */
    protected List _clusterNodesComplete(
            DIFGraph graph, Collection nodeCollection, Node superNode) {
        return SDFGraphs.clusterNodesComplete(
                (SDFGraph)graph, nodeCollection, superNode);
    }

    /** Return an SDF edge weight for a newly created edge in clustering
     *  process.
     *  @return An SDF edge weight.
     */
    protected DIFEdgeWeight _newEdgeWeight() {
        return new SDFEdgeWeight();
    }


    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

}


