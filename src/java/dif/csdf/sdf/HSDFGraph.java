/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Information associated with an HSDF Graph. */

package dif.csdf.sdf;

//////////////////////////////////////////////////////////////////////////
//// HSDFGraph
/**
Information associated with an HSDF graph. This class caches
frequently-used data associated with HSDF graphs.
HSDF Graphs are special cases of Single Rate Graphs in which the SDF
production and consumption rates on its edges are identically equal to one.
This class is intended for use with analysis/synthesis algorithms that
operate on HSDF Graphs.
<p>
HSDFGraph nodes and edges have weights of
type {@link SDFNodeWeight} and {@link HSDFEdgeWeight}, respectively.

@author Shahrooz Shahparnia
@version $Id: HSDFGraph.java 606 2008-10-08 16:29:47Z plishker $
@see HSDFEdgeWeight
*/
public class HSDFGraph extends SingleRateGraph {

    /** Construct an empty HSDF graph.
     */
    public HSDFGraph() {
        super();
    }

    /** Construct an empty HSDF graph with enough storage allocated
     *  for the specified number of nodes.
     *  @param nodeCount The number of nodes.
     */
    public HSDFGraph(int nodeCount) {
        super(nodeCount);
    }

    /** Construct an empty HSDF graph with enough storage allocated
     *  for the specified number of edges, and number of nodes.
     *  @param nodeCount The integer specifying the number of nodes
     *  @param edgeCount The integer specifying the number of edges
     */
    public HSDFGraph(int nodeCount, int edgeCount) {
        super(nodeCount, edgeCount);
    }

    /** Verify node weight for HSDF graph.
     *  @param weight The node weight to verify.
     *  @return True if the given node weight is valid for an HSDF graph.
     */
    public boolean validEdgeWeight(Object weight) {
        return weight instanceof HSDFEdgeWeight;
    }
}


