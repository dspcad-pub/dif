/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Utilities for synchronous dataflow (SDF) graphs. */

package dif.csdf.sdf;

///////////////////////////////////////////////////////////////////////
//// SDFUtilities
/** Utilities for synchronous dataflow (SDF) graphs.

@author Shuvra S. Bhattacharyya. Portions are adapted from a file by Jeff Tsay and Christopher Hylands.
@version $Id: SDFUtilities.java 606 2008-10-08 16:29:47Z plishker $
@see SDFGraph
 */

public class SDFUtilities {

    // private constructor to prevent instantiation of this class.
    private SDFUtilities() {}

    /** Return a diagnostic string with the elapsed time since a reference
     *  start time, and the amount of memory used. This method is useful
     *  for evaluating the time and memory efficiency of synthesis algorithms.
     *  @param startTime the reference start time.
     *  @return the diagnostic string.
     */
    public static String timeAndMemory(long startTime) {
    Runtime runtime = Runtime.getRuntime();
    long totalMemory = runtime.totalMemory()/1024;
    long freeMemory = runtime.freeMemory()/1024;
    return System.currentTimeMillis() - startTime
        + " ms. Memory: "
        + totalMemory + "K Free: " + freeMemory + "K ("
        + Math.round( (((double)freeMemory)/((double)totalMemory))
              * 100.0)
        + "%)";
    }

   /** Output a string representation of a looped schedule. In this
    *  string representation, each schedule loop
    * is represented by a string of the form
    * (<em>n</em> <em>S</em><sub>1</sub> <em>S</em><sub>2</sub>
    * ... <em>S</em><sub><em>m</em></sub>), where <em>n</em> is the iteration
    * count of the loop, and each <em>S</em><sub><em>i</em></sub>
    * is the string representation (as determined recursively by this method) of the
    * <em>i</em>th iterand.
    * @param element the looped schedule, which can be any SDF
    *        {@link ptolemy.actor.sched.ScheduleElement}.
    * @return the string representation of the schedule.
    */
    /*
    public static String toString(ScheduleElement element) {
	//Todo: switch schedule over to Sched?

             StringBuffer result = new StringBuffer();
        if (element.getIterationCount() > 1)
            result.append("(" + element.getIterationCount() + " ");
        if (element instanceof Firing) {
            result.append(((Firing)element).getActor());
        } else if (element instanceof Schedule) {
            Iterator body = ((Schedule)element).iterator();
            while(body.hasNext()) {
                ScheduleElement e = (ScheduleElement)(body.next());
                result.append(toString(e));
            }
        } else {
            throw new RuntimeException("Unsupported schedule element type: "
                     + ((element == null) ? "null" : (element.getClass().getName()
                     + " (value = '" + element + "')")));
        }
        if (element.getIterationCount() > 1)
            result.append(")");
        return result.toString();
	
	
	}
    */

}


