/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A class modeling procedure instantiation in software synthesis. */

package dif.csdf.sdf.sched;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import dif.util.sched.ScheduleElement;

//////////////////////////////////////////////////////////////////////////
//// Procedure
/**
A class modeling procedure instantiation in software synthesis.

@author Mingyung Ko
@version $Id: Procedure.java 606 2008-10-08 16:29:47Z plishker $
*/

public class Procedure {

    /** Constructor without any argument.
     */
    public Procedure() {
        _callees = new ArrayList();
        _callers = new HashSet();
        _calleeIterations = new ArrayList();
    }

    ///////////////////////////////////////////////////////////////////
    ////                        public methods                     ////

    /** Add (append) a callee with a loop iteration.
     *  Since callees are appended, users should be aware and responsible for
     *  the adding sequence.
     *
     *  @param callee The callee.
     *  @param iterations Loop iterations of the callee.
     */
    public void addCallee(Procedure callee, int iterations) {
        _callees.add(callee);
        _calleeIterations.add(new Integer(iterations));
    }

    /** Add a callee with a loop iteration at the specific call index.
     *
     *  @param index The call index.
     *  @param callee The callee.
     *  @param iterations Loop iterations of the callee.
     */
    public void addCallee(int index, Procedure callee, int iterations) {
        _callees.add(index, callee);
        _calleeIterations.add(index, new Integer(iterations));
    }

    /** Add the caller.
     *  @param caller The caller.
     */
    public void addCaller(Procedure caller) {
        _callers.add(caller);
    }

    /** Get the number of callees.
     *  @return The number of callees.
     */
    public int calleeCount() {
        return _callees.size();
    }

    /** Get the number of callers.
     *  @return The number of callers.
     */
    public int callerCount() {
        return _callers.size();
    }

    /** Get the callee given an index.
     *  @param index The callee index.
     *  @return The callee.
     */
    public Procedure getCallee(int index) {
        return (Procedure)_callees.get(index);
    }

    /** Get the callees.
     *  @return The callees in unmodifiable <code>List</code>.
     */
    public List getCallees() {
        return Collections.unmodifiableList(_callees);
    }

    /** Get the callers. Order of callers does not matter.
     *  @return The callers in unmodifiable <code>Collection</code>.
     */
    public Collection getCallers() {
        return Collections.unmodifiableCollection(_callers);
    }

    /** Get index of the callee.
     *  @param The callee.
     *  @return The callee index.
     */
    public int getCalleeIndex(Procedure callee) {
        return _callees.indexOf(callee);
    }

    /** Get the number of iterations of a callee.
     *  @param callee The callee.
     *  @return Loop iterations of the callee.
     */
    public int getCalleeIterations(Procedure callee) {
        return getCalleeIterations(_callees.indexOf(callee));
    }

    /** Get the number of iterations of a callee.
     *  @param index The callee index.
     *  @return Loop iterations of the callee.
     */
    public int getCalleeIterations(int index) {
        return ((Integer)_calleeIterations.get(index)).intValue();
    }

    /** Get the corresponding schedule.
     *  @return The schedule.
     */
    public ScheduleElement getSchedule() {
        return _schedule;
    }

    /** Remove the given callee.
     *  @param callee The callee.
     */
    public void removeCallee(Procedure callee) {
        int index = _callees.indexOf(callee);
        _callees.remove(index);
        _calleeIterations.remove(index);
    }

    /** Remove the given caller.
     *  @param callee The caller.
     */
    public void removeCaller(Procedure caller) {
        _callers.remove(caller);
    }

    /** Set the corresponding schedule.
     *  @param schedule The schedule.
     */
    public void setSchedule(ScheduleElement schedule) {
        _schedule = schedule;
    }

    /** Display the program structure, including calles with iterations,
     *  in text.
     *
     *  @return A text string of the call structure.
     */
    public String toString() {
        String display = new String();
        for (int i = 0; i < calleeCount(); i++)
            display += " (" + getCalleeIterations(i) + " callee_" + i + ")";
        return display;
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    // The callees. The order matters.
    private List _callees;

    // The callers. The order does not matter.
    private Collection _callers;

    // The loop iterations of callees.
    private List _calleeIterations;

    // The schedule (in ScheduleElement) associated with this procedure.
    private ScheduleElement _schedule;
}



