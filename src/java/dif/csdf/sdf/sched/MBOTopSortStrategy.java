/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A topological sorting algorithm for minimum buffer overlaps (MBO). */

package dif.csdf.sdf.sched;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import dif.DIFGraph;
import dif.graph.DFSTopSortStrategy;
import dif.util.graph.DirectedGraph;
import dif.util.graph.Node;

//////////////////////////////////////////////////////////////////////////
//// MBOTopSortStrategy
/**
A topological sorting algorithm for minimum buffer overlaps. The key points
lie in the source nodes selection. We adopt a greedy way to pick sources with
minimum output arcs. For the case of identical output arcs, sources with
maximum input arcs are picked.

@author Mingyung Ko
@version $Id: MBOTopSortStrategy.java 606 2008-10-08 16:29:47Z plishker $
*/

public class MBOTopSortStrategy extends DFSTopSortStrategy {

    /** Constructor with a {@link DIFGraph}.
     *  @param graph The given SDF graph.
     */
    public MBOTopSortStrategy(DIFGraph graph) {
        super(graph);
    }

    ///////////////////////////////////////////////////////////////////
    ////                        public methods                     ////

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /** Return the next source node in determining the top sort.
     *  This method is supposed to be overriden by specific source
     *  selection approach.
     *
     *  @param graph The graph to get the next source node.
     *  @return The next source node.
     */
    protected Node _nextSource(DirectedGraph graph) {
        List sortedSources = new ArrayList(_nodesUpOuts);
        sortedSources.retainAll(graph.sourceNodes());
        return (Node)sortedSources.get(0);
    }

    /** A preliminary step in computing topological sorting. This is a step
     *  executed in {@link #_compute()}. All nodes of the graph are sorted
     *  by their increasing output degrees.
     */
    protected void _setup() {
        _nodesUpOuts = new ArrayList(graph().nodes());
        Collections.sort(_nodesUpOuts, new UpNodes());
    }

    ///////////////////////////////////////////////////////////////////
    ////                      protected variables                  ////

    /** A sorted list of nodes by their increasing output degrees. */
    protected List _nodesUpOuts;

    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////

    ///////////////////////////////////////////////////////////////////
    ////                        private classes                    ////

    /*  A comparator implementation in sorting nodes in increasing
     *  output degrees.
     */
    private class UpNodes implements Comparator {
        public int compare(Object node1, Object node2) {
            int outArcs1 = directedGraph().outputEdgeCount((Node)node1);
            int outArcs2 = directedGraph().outputEdgeCount((Node)node2);
            int inArcs1  = directedGraph().inputEdgeCount((Node)node1);
            int inArcs2  = directedGraph().inputEdgeCount((Node)node2);
            // Sort nodes with increasing out arcs. If with same out arcs,
            // sort nodes with decreasing in arcs.
            int compareResult = (outArcs1 == outArcs2)
                    ? (inArcs2 - inArcs1) : (outArcs1 - outArcs2);
            return compareResult;
        }
    }
}



