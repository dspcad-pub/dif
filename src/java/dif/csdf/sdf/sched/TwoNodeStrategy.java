/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A recursive scheduler for two-node SDF graphs. */

package dif.csdf.sdf.sched;

import dif.util.graph.DirectedGraph;
import dif.util.graph.Edge;
import dif.util.graph.Node;
import dif.util.sched.Firing;
import dif.util.sched.Schedule;
import dif.util.sched.ScheduleElement;
import dif.util.sched.ScheduleTree;
import dif.DIFScheduleStrategy;
import dif.DIFScheduleUtilities;
import dif.csdf.sdf.SDFGraph;
import dif.csdf.sdf.SDFEdgeWeight;
import dif.csdf.sdf.SDFNodeWeight;

//////////////////////////////////////////////////////////////////////////
//// TwoNodeStrategy
/** A recursive scheduler for two-node graphs. The recursive method aims
at minimal buffer requirement (Absolute Buffer Memory Lower Bound, ABMLB)
and minimal procedure code space (i.e. polynomial space cost). However,
run time performance is still high (maybe exponential). Because of the
minimal buffer result, the schedule is generally of multiple appearances.
<p>
The class does NOT provide procedure synthesis. Since the 'two nodes' may
be two schedules (with their own loop structures), the synthesis process
might generate unexpected results. Therefore, only scheduling results are
available in this class.

@author Mingyung Ko
@version $Id: TwoNodeStrategy.java 720 2009-08-07 22:23:33Z nsane $
*/

public class TwoNodeStrategy extends DIFScheduleStrategy {

    /** Constructor for two schedule elements.
     *  A two-node SDF graph is also built with the given rates.
     *
     *  @param element1 The first schedule element.
     *  @param element2 The second schedule element.
     */
    public TwoNodeStrategy(
            ScheduleElement element1, ScheduleElement element2) {

        super(new SDFGraph());
        // Iteration counts are of reverse order of SDF rates.
        int production  = element2.getIterationCount();
        int consumption = element1.getIterationCount();
        _buildGraph(production, consumption);
        _element1 = DIFScheduleUtilities.cloneScheduleElement(element1);
        _element2 = DIFScheduleUtilities.cloneScheduleElement(element2);
        _element1.setIterationCount(1);
        _element2.setIterationCount(1);
        _initialize(production, consumption);
    }

    /** Constructor for two token exchanging rates.
     *  A two-node SDF graph is also built with the given rates.
     *
     *  @param productionRate The production rate of the only SDF edge.
     *  @param consumptionRate The consumption rate of the only SDF edge.
     */
    public TwoNodeStrategy(int productionRate, int consumptionRate) {
        super(new SDFGraph());
        _buildGraph(productionRate, consumptionRate);
        _initialize(productionRate, consumptionRate);
    }

    /** Constructor for a two-node SDF graph.
     *  @param twoNodeGraph The given graph.
     */
    public TwoNodeStrategy(SDFGraph twoNodeGraph) {
        super(twoNodeGraph);

        // Get token production and consumption rates.
        SDFEdgeWeight weight = (SDFEdgeWeight)
                ((Edge)twoNodeGraph.edges().iterator().next()).getWeight();
        int production  = weight.getSDFProductionRate();
        int consumption = weight.getSDFConsumptionRate();

        _initialize(production, consumption);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Construct the recursive procedural implementation schedule.
     *  @return The recursive procedure schedule.
     */
    /*    public Schedule schedule() {
        _decompose(_production, _consumption);
        return _schedule;
	} */

    public ScheduleTree schedule() {
        _decompose(_production, _consumption);
	ScheduleTree st = new ScheduleTree(_schedule);
        return st;
    }

    /** A desrciption of the scheduler.
     *  @return A text description.
     */
    public String toString() {
        return new String("Recursive scheduler for two-node graphs.");
    }

    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////

    /*  Build a two-node graph if none is given through any constructor.
     *  @param production SDF production rate.
     *  @param consumption SDF consumption rate.
     */
    private void _buildGraph(int production, int consumption) {
        Node nodeA = new Node(new SDFNodeWeight());
        Node nodeB = new Node(new SDFNodeWeight());
        Edge edge  = new Edge(nodeA, nodeB,
                new SDFEdgeWeight(production, consumption, 0));
        graph().addNode(nodeA);
        graph().addNode(nodeB);
        graph().addEdge(edge);
    }

    /*  Recursive decomposition of a two-element schedule.
     *  @param production The token production rate.
     *  @param consumption The token consumption rate.
     */
    private void _decompose(int production, int consumption) {
        // If production rate is bigger.
        if (production > consumption) {
            // Production rate is multiple of consumption rate.
            if (production % consumption == 0) {
                _schedule.get(1).setIterationCount(production / consumption);
            } else {
                // Create a new schedule to replace the first element.
                Schedule schedule = _createNewSchedule();
                schedule.get(1).setIterationCount(production / consumption);
                _schedule.remove(0);
                _schedule.add(0, schedule);
                _decompose(production % consumption, consumption);
            }
        // If consumption rate is bigger or equal to production rate.
        } else {
            // Production rate is multiple of consumption rate.
            if (consumption % production == 0) {
                _schedule.get(0).setIterationCount(consumption / production);
            } else {
                // Create a new schedule to replace the second element.
                Schedule schedule = _createNewSchedule();
                schedule.get(0).setIterationCount(consumption / production);
                _schedule.remove(1);
                _schedule.add(1, schedule);
                _decompose(production,  consumption % production);
            }
        }
    }

    /*  Create a new schedule in the recursive procedure substitution.
     *  @return The new schedule.
     */
    private Schedule _createNewSchedule() {
        Schedule schedule = new Schedule();
        schedule.add(
                DIFScheduleUtilities.cloneScheduleElement(_schedule.get(0)));
        schedule.add(
                DIFScheduleUtilities.cloneScheduleElement(_schedule.get(1)));
        return schedule;
    }

    /*  Initialization.
     *  @param production The SDF production rate.
     *  @param consumption The SDF consumption rate.
     */
    private void _initialize(int production, int consumption) {
        // Initialize the recursive procedure call computation.
        _production  = production;
        _consumption = consumption;
        _schedule = new Schedule();
        if ((_element1 == null) || (_element2 == null)) {
            Edge edge = graph().edge(0);
            _element1 = new Firing(edge.source());
            _element2 = new Firing(edge.sink());
        }
        _schedule.add(_element1);
        _schedule.add(_element2);

        // Initialize the procedure call graph.
        Edge callGraphEdge = graph().edge(0);
        _twoNodes = new Node[2];
        _twoNodes[0] = callGraphEdge.source();
        _twoNodes[1] = callGraphEdge.sink();
        _callGraph = new DirectedGraph();
        _callGraph.addNode(_twoNodes[0]);
        _callGraph.addNode(_twoNodes[1]);
    }

    /*  Create a new procedure call graph source node. The node is to
     *  replace one of the two nodes in forming the recursion.
     *  @param nodeIndex The index of the node to be replaced.
     *  @param repetition The repetition of the node NOT replaced.
     */
    private Node _newCallGraphNode(int nodeIndex, int repetition) {
        // Create a schedule (as node weight) associated with the new node.
        Firing firings[] = new Firing[2];
        firings[0] = new Firing(_twoNodes[0]);
        firings[1] = new Firing(_twoNodes[1]);
        Schedule schedule = new Schedule();
        schedule.add(firings[0]);
        schedule.add(firings[1]);
        if (nodeIndex == 0)
            firings[1].setIterationCount(repetition);
        else
            firings[0].setIterationCount(repetition);

        // Add the new node and procedure call edges.
        Node node = new Node(schedule);
        Edge edge0 = new Edge(node, _twoNodes[0]);
        Edge edge1 = new Edge(node, _twoNodes[1]);
        _callGraph.addNode(node);
        _callGraph.addEdge(edge0);
        _callGraph.addEdge(edge1);
        _twoNodes[nodeIndex] = node;

        return node;
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    // The production and consumption rates of the two-node SDF graph.
    private int _production, _consumption;

    // The schedule and the given schedule elements.
    // Schedule elements are given through constructor.
    private Schedule _schedule;
    private ScheduleElement _element1, _element2;

    // The procedure call graph.
    private DirectedGraph _callGraph;

    // The two nodes in every recursion step.
    private Node _twoNodes[];

}




