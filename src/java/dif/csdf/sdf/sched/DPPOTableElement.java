/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Table entry for Dynamic Programming Post Optimization. */

package dif.csdf.sdf.sched;

//////////////////////////////////////////////////////////////////////////
//// DPPOTableElement
/**
A base class for DPPO table element.

@author Mingyung Ko
@version $Id: DPPOTableElement.java 606 2008-10-08 16:29:47Z plishker $
*/

public class DPPOTableElement {
    /** The cost for the element */
    public double cost;

    /** The Greatest Common Divisor (gcd) value. */
    public int gcd;

    /** The split place for optimal solutions. */
    public int split;

    /** Indication for reverse or normal split for GDPPO. */
    public boolean reverseSplit;
}



