/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* The DPPO for code size optimization. */

package dif.csdf.sdf.sched;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import dif.DIFScheduleUtilities;
import dif.DIFScheduleStrategy;
import dif.util.sched.Firing;
import dif.util.sched.Schedule;
import dif.util.sched.ScheduleElement;
import dif.util.sched.ScheduleTree;

//////////////////////////////////////////////////////////////////////////
//// CDPPOStrategy
/**
The DPPO for code size optimization. CDPPO is an adapted DPPO approach to
minimize code size for arbitrary, not restricted to single appearance,
schedules. CDPPO does NOT require an SDF graph as input. The primary input
is an schedule, no matter flattened or hierarchicaly looped. Therefore,
the input data is potentially large, even in exponential order of the
associated SDF graph. For more details about CDPPO, please reference
<p>
"Renesting Single Appearance Schedules to Minimize Buffer Memory" by
S. S. Bhattacharyya, P. K. Murthy, and E. A. Lee. Memo UCB/ERL M95/43,
Electronics Research Lab., UC Berkeley, April 1995.
<p>
or
<p>
"Multidimensional exploration of software implementations for DSP algorithms"
by E. Zitzler, J. Teich, and S. S. Bhattacharyya. Journal of VLSI Signal
Processing Systems for Signal, Image, and Video Technology,
pages 83-98, February 2000.
<p>
CAUTION: This class does NOT extend {@link DPPOStrategy} nor
{@link DIFScheduleStrategy}.

@author Mingyung Ko
@version $Id: CDPPOStrategy.java 720 2009-08-07 22:23:33Z nsane $
*/

public class CDPPOStrategy {

    /** Constructor for an arbitrary list of objects. In this situation,
     *  CDPPO acts as an compression tool for an arbitrary object list.
     *  Objects are assumed to have code size of one and loops have zero costs.
     *
     *  @param objectList The list of objects.
     */
    public CDPPOStrategy(List objectList) {
        _actorFirings = objectList;
        _makeDefaultSizes(_actorFirings);
        _createCDPPOTable(_actorFirings.size());
    }

    /** Constructor for a schedule. Actors have
     *  default code size of one and loops have size zero.
     *
     *  @param actorFirings The actor firing sequence.
     */
    public CDPPOStrategy(ScheduleElement schedule) {
        _actorFirings = DIFScheduleUtilities.actorFirings(schedule);
        _makeDefaultSizes(_actorFirings);
        _createCDPPOTable(_actorFirings.size());
    }

    /** Constructor for a given graph, a list of actor firings, map for actors
     *  to their code sizes, and code size for loops.
     *
     *  @param graph The given SDF graph.
     *  @param actorFirings The actor firing sequence.
     */
    public CDPPOStrategy(ScheduleElement schedule,
            Map actorSizeMap, int loopSize) {

        _actorFirings = DIFScheduleUtilities.actorFirings(schedule);
        _actorSizeMap = actorSizeMap;
        _loopSize = loopSize;
        _createCDPPOTable(_actorFirings.size());
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Return the optimum code size of the CDPPO computation.
     *  @return The optimum code size.
     */
    public int codeSize() {
        return _tableElement(0, _actorFirings.size() - 1).codeSize;
    }

    /** Construct an SDF schedule from the DPPO computation.
     *  @return An SDF schedule.
     */
    /*   public Schedule schedule() {
        _computeCDPPO();
        ScheduleElement result =
                _tableElement(0, _actorFirings.size() - 1).schedule;
        Schedule schedule = new Schedule();
        if (result instanceof Firing)
            schedule.add(result);
        else
            schedule = (Schedule)result;
        return schedule;
	}*/

    public ScheduleTree schedule() {
        _computeCDPPO();
        ScheduleElement result =
                _tableElement(0, _actorFirings.size() - 1).schedule;
        Schedule schedule = new Schedule();
        if (result instanceof Firing)
            schedule.add(result);
        else
            schedule = (Schedule)result;

	ScheduleTree st = new ScheduleTree(schedule);
        return st;
    }

    /** A desrciption of the scheduler.
     *  @return A text description.
     */
    public String toString() {
        return new String("CDPPO scheduler.");
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /** Compare the top-level schedule loops' bodies of two schedules.
     *  The comparison is actor firings based (flattened), not loop structure
     *  based (hierarchical). See also
     *  {@link DIFScheduleUtilities#compareScheduleByActorFirings(
     *  ScheduleElement, ScheduleElement)}.
     *
     *  @param s1 The first schedule.
     *  @param s2 The second schedule.
     *  @return True if the loop bodies are equivalent; false otherwise.
     */
    protected boolean _compareScheduleBody(
            ScheduleElement s1, ScheduleElement s2) {
        int iteration1 = s1.getIterationCount();
        int iteration2 = s2.getIterationCount();
        s1.setIterationCount(1);
        s2.setIterationCount(1);
        boolean equivalence =
                DIFScheduleUtilities.compareScheduleByActorFirings(s1, s2);
        s1.setIterationCount(iteration1);
        s2.setIterationCount(iteration2);
        return equivalence;
    }

    /** The optimal results along the elements with index i to j.
     *  In the implementation, the optimal cost, split, and any relevant
     *  attributes should be computed and stored.
     *
     *  @param i The element with index i.
     *  @param j The element with index j.
     */
    protected void _optimumFor(int i, int j) {
        ScheduleElement optimumSchedule = null;
        int minCodeSize = Integer.MAX_VALUE;
        for (int split = i; split < j; split++) {
            // Get the left and right parts.
            CDPPOTableElement left  = _tableElement(i, split);
            CDPPOTableElement right = _tableElement(split + 1, j);

            int codeSize = left.codeSize + right.codeSize;
            ScheduleElement schedule = null;
            // The left and right subschedules can be merged into
            // a single schedule loop.
            if (_compareScheduleBody(left.schedule, right.schedule)) {
                schedule = DIFScheduleUtilities
                        .cloneScheduleElement(left.schedule);
                int lCount = left.schedule.getIterationCount();
                int rCount = right.schedule.getIterationCount();
                int newCount = lCount + rCount;
                schedule.setIterationCount(newCount);
                // Adjust code size due to loops. Code size for loops may
                // be repeated, or not considered in subschedules.
                if (_loopSize != 0) {
                    if ((lCount > 1) && (rCount > 1))
                        codeSize /= 2;
                    else if ((lCount == 1) && (rCount == 1))
                        codeSize = _loopSize + (codeSize / 2);
                    else
                        codeSize = (_loopSize + codeSize) / 2;
                } else {
                    codeSize /= 2;
                }
            // The left and right subschedules can NOT be merged.
            } else {
                schedule = new Schedule();
                ((Schedule)schedule).add(DIFScheduleUtilities
                        .cloneScheduleElement(left.schedule));
                ((Schedule)schedule).add(DIFScheduleUtilities
                        .cloneScheduleElement(right.schedule));
            }
            // Keep track of the optimum results for the present split.
            if (codeSize < minCodeSize) {
                minCodeSize = codeSize;
                optimumSchedule = schedule;
            }
        }
        CDPPOTableElement element = _tableElement(i, j);
        element.codeSize = minCodeSize;
        element.schedule = optimumSchedule;
    }

    /** Get the element of the CDPPO table.
     *
     *  @param i The index of the starting (left) node
     *  @param j The index of the ending (right) node
     *  @return The desired element.
     */
    protected CDPPOTableElement _tableElement(int i, int j) {
        ArrayList row = (ArrayList)_CDPPOTable.get(i);
        return (CDPPOTableElement)row.get(j - i);
    }

    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////

    /*  CDPPO computation.
     */
    private void _computeCDPPO() {
        int dimension = _actorFirings.size();
        for (int step = 1; step < dimension; step++)
            for (int i = 0; i < dimension - step; i++) {
                _optimumFor(i, i + step);
                _shareSchedule(i, step);
            }
    }

    /*  Creat a table for CDPPO computation.
     *  Only a triangular table is necessary. This method creates the table
     *  in the form of an ArrayList of ArrayLists.
     *  To access each element, see {@link #_tableElement()}.
     *
     *  @param dimension The number of actor firings.
     */
    private void _createCDPPOTable(int dimension) {
        // Create the table.
        _CDPPOTable = new ArrayList();
        for (int i = 0; i < dimension; i++) {
            ArrayList row = new ArrayList();
            for (int j = 0; j < (dimension - i); j++) {
                try {
                    row.add(new CDPPOTableElement());
                } catch (Exception exception) {}
            }
            _CDPPOTable.add(row);
        }
        // Create Firing for each actor execution and set up its code size.
        for (int i = 0; i < dimension; i++) {
            Object actor = _actorFirings.get(i);
            CDPPOTableElement element = _tableElement(i, i);
            element.schedule = new Firing(actor);
            element.codeSize = ((Integer)_actorSizeMap.get(actor)).intValue();
        }
    }

    /*  Make default code sizes for actors and loops.
     */
    private void _makeDefaultSizes(List actorFirings) {
        _loopSize = 0;
        _actorSizeMap = new HashMap();
        for (int i = 0; i < actorFirings.size(); i++) {
            Object actor = actorFirings.get(i);
            if (!_actorSizeMap.containsKey(actor))
                _actorSizeMap.put(actor, new Integer(1));
        }
    }

    /*  Share schedules by pointers. Schedules with the same dynamic
     *  programming level (step) have high chance to be equivalent. They
     *  can be shared by pointers rather than allocating seperate schedules.
     */
    private void _shareSchedule(int index, int step) {
        ScheduleElement schedule = _tableElement(index, index + step).schedule;
        for (int i = 0; i < index; i++) {
            ScheduleElement toShare =
                    _tableElement(i, i + step).schedule;
            // If any equivalent schedule is found, discard the current
            // schedule allocation and point to the equivalent one.
            if (DIFScheduleUtilities
                    .compareScheduleByActorFirings(schedule, toShare)) {
                schedule = toShare;
                break;
            }
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    /* The table for CDPPO. */
    private List _CDPPOTable;

    // The actor firing sequence.
    private List _actorFirings;

    // The map for actors to their code sizes.
    private Map _actorSizeMap;

    // The code size for loops.
    private int _loopSize;

}




