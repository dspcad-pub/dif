/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A Dynamic Loop Count (DLC) SAS scheduler for SDF graphs. */

package dif.csdf.sdf.sched;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import dif.util.graph.Edge;
import dif.util.graph.Node;
import dif.util.sched.Firing;
import dif.util.sched.Schedule;
import dif.util.sched.ScheduleTree;
import dif.util.sched.ScheduleElement;
import dif.DIFScheduleStrategy;
import dif.csdf.sdf.SDFEdgeWeight;
import dif.csdf.sdf.SDFGraph;

//////////////////////////////////////////////////////////////////////////
//// DLCStrategy
/**
Dynamic Loop Count (DLC) SAS scheduler for SDF graphs.
This is a partial implementation of Hyunok Oh's source-DLC SAS scheduling,
where delays, cycles are not handled here.

<p>
Hyunok Oh, Nikil Dutt, and Soonhoi Ha.
"Single Appearance Schedule with Dynamic Loop Count for Minimum Data Buffer
from Synchronous Dataflow Graphs", CASES 2005, pages 157-165.

@author Mingyung Ko
@version $Id: DLCStrategy.java 720 2009-08-07 22:23:33Z nsane $
*/

public class DLCStrategy extends DIFScheduleStrategy {

    /** Constructor of an {@link dif.csdf.sdf.SDFGraph}.
     *  @param graph The given SDF graph.
     */
    public DLCStrategy(SDFGraph graph) {
        super(graph);
        _SDFGraph = graph;
        _topSort = _computeTopSort(graph);
        _computeRates();
        _topLoop = _computeDLCLoops();
        _iterations = _computeIterations();
    }

    ///////////////////////////////////////////////////////////////////
    ////                        public methods                     ////

    /** Get the top sort of nodes by Oh's heuristic.
     *  @return The top sort.
     */
    public List getTopSort() {
        return Collections.unmodifiableList(_topSort);
    }

    //NS: Modified to change to ScheduleTree

    /** Compute a flat schedule for an acyclic SDF graph.
     *  The order is decided by an generic topplogical sorting result.
     *
     *  @return A flat SDF schedule.
     */
    /*   public Schedule schedule() {
        int loops = _topSort.size() - 1;
        // Build a schedule through converting the loop hierarchy.
        Schedule schedule = new Schedule();
        if (_iterations == 1)
            schedule = (Schedule)_topLoop.toSchedule();
        else {
            for (int i = 0; i < _iterations; i++) {
                ScheduleElement subschedule = _topLoop.toSchedule();
                if (subschedule instanceof Firing)
                    schedule.add(subschedule);
                else {
                    int iterands = ((Schedule)subschedule).size();
                    for (int j = 0; j < iterands; j++)
                        schedule.add(((Schedule)subschedule).get(j));
                }
            }
        }
        return schedule;
    }
    */
    public ScheduleTree schedule() {
        int loops = _topSort.size() - 1;
	// Build a schedule through converting the loop hierarchy.
        Schedule schedule = new Schedule();
        if (_iterations == 1)
            schedule = (Schedule)_topLoop.toSchedule();
        else {
            for (int i = 0; i < _iterations; i++) {
                ScheduleElement subschedule = _topLoop.toSchedule();
                if (subschedule instanceof Firing)
                    schedule.add(subschedule);
                else {
                    int iterands = ((Schedule)subschedule).size();
                    for (int j = 0; j < iterands; j++)
                        schedule.add(((Schedule)subschedule).get(j));
                }
            }
        }

	ScheduleTree st = new ScheduleTree(schedule);

        //return schedule;
	return st;
    }

    /** Display the dlcSAS synthesis results.
     *  @param nodeNameMap The map from SDF nodes to their names.
     *  @return The dlcSAS synthesis.
     */
    public String toSynthesisString(Map nodeNameMap) {
        String synthesis = new String("(" + _iterations + " ")
                + _topLoop.toSynthesisString(nodeNameMap) + ")";
        return synthesis;
    }

    /** A description of flat scheduler.
     *
     *  @return A description of flat scheduler.
     */
    public String toString() {
        return "Dynamic Loop Count (DLC) SAS scheduler.\n";
    }

    /** Acyclic property is validated for flat scheduling.
     *
     *  @return True if the graph is acyclic.
     */
    public boolean valid() {
        return _SDFGraph.isAcyclic();
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private methods                     ////

    /*  Compute the number of iterations to run the dlcSAS loop hierarchy.
     *  The iterations are required to constitute one schedule period.
     *  @return The required iterations.
     */
    private int _computeIterations() {
        int loops = _topSort.size() - 1;
        /* The desired iterations is just that for the (looped) sink. */
        int iterations = _SDFGraph.getRepetitions(
                (Node)_topSort.get(_topSort.size() - 1));
        if (_p[loops - 1] % _c[loops - 1] == 0)
            iterations /= (_p[loops - 1] / _c[loops - 1]);
        return iterations;
    }

    /*  Copy/compute the desired production/consumption rates.
     */
    private void _computeRates() {
        int loops = _topSort.size() - 1;
        _p = new int[loops];
        _c = new int[loops];
        for (int i = 0; i < loops; i++) {
            Node source = (Node)_topSort.get(i);
            Node sink   = (Node)_topSort.get(i + 1);
            Collection edges = _SDFGraph.successorEdges(source, sink);
            if (!edges.isEmpty()) {
                 SDFEdgeWeight edgeWeight = (SDFEdgeWeight)
                         ((Edge)edges.iterator().next()).getWeight();
                 _p[i] = edgeWeight.getSDFProductionRate();
                 _c[i] = edgeWeight.getSDFConsumptionRate();
            /* Get rates from virtual arcs. */
            } else {
                 _p[i] = _SDFGraph.getRepetitions(sink);
                 _c[i] = _SDFGraph.getRepetitions(source);
            }
        }
    }

    /*  Build the dlcSAS loop hierarchy.
     *  @return The top level loop.
     */
    private DLCLoop _computeDLCLoops() {
        int loops = _topSort.size() - 1;
        DLCLoop loop = new DLCLoop(_p[0], _c[0],
                (Node)_topSort.get(0), (Node)_topSort.get(1));
        for (int i = 1; i < loops; i++)
            loop = new DLCLoop(_p[i], _c[i], loop, (Node)_topSort.get(i + 1));
        return loop;
    }

    /*  Compute a top sort of nodes based on Oh's heuristic. The top sort
     *  is supposed to induce buffer optimal dlcSAS.
     *  @param graph The SDF graph.
     *  @return The dlcSAS top sort.
     */
    private List _computeTopSort(SDFGraph graph) {
        List chain = new ArrayList();
        List unchained = new ArrayList(graph.nodes());
        Node toChain = null;
        Node lastChained = null;
        while (!unchained.isEmpty()) {
            /* Choose any source as the 1st chain element. */
            if (chain.isEmpty()) {
                toChain = (Node)graph.sourceNodes().iterator().next();
            } else {
                /* Collect nodes that are schedulable (whose predecessors
                   are in the chain already). */
                List candidates = new ArrayList();
                for (int i = 0; i < unchained.size(); i++) {
                    Node node = (Node)unchained.get(i);
                    if (chain.containsAll(graph.predecessors(node)))
                        candidates.add(node);
                }
                /* Heuristic of picking the node for optimal buffer costs. */
                int lastChainedRepetitions = graph.getRepetitions(lastChained);
                int biggestRepetitions = 0;
                for (int i = 0; i < candidates.size(); i++) {
                    Node candidate = (Node)candidates.get(i);
                    int repetitions = graph.getRepetitions(candidate);
                    /* Search nodes with last chained node's repetitions */
                    if (repetitions == lastChainedRepetitions) {
                        toChain = candidate;
                        break;
                    /* Search nodes with biggest repetitions. */
                    } else if (repetitions > biggestRepetitions) {
                        biggestRepetitions = repetitions;
                        toChain = candidate;
                    }
                }
            }
            unchained.remove(toChain);
            chain.add(toChain);
            lastChained = toChain;
        }
        return chain;
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    /* The associated SDF graph. */
    private SDFGraph _SDFGraph;

    /* The top sort generated by Oh's heuristic. */
    private List _topSort;

    /* Top level DLC loop. */
    private DLCLoop _topLoop;

    /* The desired production/consumption rates. */
    private int[] _p, _c;

    /* The required iterations to constitute one schedule period. */
    private int _iterations;
}



