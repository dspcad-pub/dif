/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Acyclic Pair-wise Group of Adjacent Nodes (APGAN) scheduler. */

package dif.csdf.sdf.sched;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import dif.util.graph.Edge;
import dif.util.graph.Node;
import dif.util.sched.ScheduleTree;
import dif.data.ExtendedMath;

import dif.DIFScheduleStrategy;
import dif.csdf.sdf.SDFClusterManager;
import dif.csdf.sdf.SDFGraph;

//////////////////////////////////////////////////////////////////////////
//// APGANStrategy
/** Acyclic Pair-wise Grouping of Adjacent Nodes (APGAN) scheduler for
SDF graphs. It is described in the book
"Software Synthesis from Dataflow Graphs" by
Shuvra S. Bhattacharyya, Praveen K. Murthy, and Edward A. Lee, chapter 7.
<p>
This program provides a basic APGAN and can be accommodated to
arbitrary clustering priority or tie-breaking rules.
For the accommodation, {@link #_candidates()} or {@link #_tieBreak()}
are the methods to be overrided.

@author Mingyung Ko
@version $Id: APGANStrategy.java 606 2008-10-08 16:29:47Z plishker $
*/

public class APGANStrategy extends DIFScheduleStrategy {

    /** Constructor of APGAN. APGAN performs clustering and the graph
     *  topology will be changed. Therefore, graph cloning is needed
     *  and is done here.
     *
     *  @param graph The given SDF graph
     */
    public APGANStrategy(SDFGraph graph) {
        super(graph);
        _clusterManager = new SDFClusterManager(graph);
    }

    ///////////////////////////////////////////////////////////////////
    ////                        public methods                     ////

    /** Get a GDPPO schedule (see {@link GDPPOStrategy}) with a lexical
     *  order derived by APGAN. In usual, GDPPO is called to post optimize
     *  APGAN result. This method provides the integrated function.
     *
     *  @return The GDPPO schedule with APGAN lexical order.
     */
    public ScheduleTree gdppoSchedule() {
	/*        List lexicalOrder = schedule().lexicalOrder();
        GDPPOStrategy gdppo =
                new GDPPOStrategy((SDFGraph)graph(), lexicalOrder);
		return gdppo.schedule();*/
	return new ScheduleTree();
    }

    /** Run APGAN clustering and then expand it to a schedule.
     *  Arbitrary  for {@link #_candidates()}
     *
     *  @exception RuntimeException No valid nodes to cluster: error
     *             with candidate/tie-break law
     */
    public ScheduleTree schedule() {
        // Run APGAN clustering until no nodes to cluster.
        while (_clusterManager.getGraph().nodeCount() > 1) {
	    System.out.println("Clustering....");
            Iterator prioritizedEdges = _prioritizedEdges().iterator();

            while (prioritizedEdges.hasNext()) {
                Edge edge = (Edge)prioritizedEdges.next();

                if (_testAcyclicClustering(edge)) {
                    Collection nodes = new ArrayList();
                    nodes.add(edge.source());
                    nodes.add(edge.sink());
                    _clusterManager.clusterNodes(nodes);
                    break;

                // The edge's ending nodes are illegal to cluser. Skip
                // to the next edge.
                } else
                    continue;
            }
        }

        // expand graph resulting from APGAN
        Node root = _clusterManager.getGraph().node(0);
        return (ScheduleTree)_expandAPGAN(
                (SDFGraph)_clusterManager.getGraph(), root);
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /** Sort the edges in ascendent orders. Edge values are defined by
     *  GCD of ending nodes' repetition counts.
     *  @return The ascendent edge list.
     */
    protected final List _ascendentEdges() {
        ArrayList sortedEdges =
                new ArrayList(_clusterManager.getGraph().edges());
        Collections.sort(sortedEdges, new upComparator());
        return sortedEdges;
    }

    /** Sort the edges in descendent orders. Edge values are defined by
     *  GCD of ending nodes' repetition counts.
     *  @return The descendent edge list.
     */
    protected final List _descendentEdges() {
        ArrayList sortedEdges =
                new ArrayList(_clusterManager.getGraph().edges());
        Collections.sort(sortedEdges, new downComparator());
        return sortedEdges;
    }

    /** A prioritized list of edges. Candidate adjacent nodes are
     *  clustered accroding to the list. By default, priority is
     *  given to the larger GCD of source and sink nodes'
     *  repetition counts.
     *  @return The prioritized edges
     */
    protected List _prioritizedEdges() {
        return _descendentEdges();
    }

    /** Test acyclic clustering of the adjacent nodes represented by the
     *  given edge.
     *  @param The edge whose ending nodes are to be clustered.
     *  @return True if the clustering leads to acyclic topology;
     *          false otherwise.
     */
    protected boolean _testAcyclicClustering(Edge edge) {
        Collection nodes = new ArrayList();
        nodes.add(edge.source());
        nodes.add(edge.sink());
        return _clusterManager.testAcyclicClustering(nodes);
    }

    /** Impose priorities on edges with same values. See also
     *  {@link #_valueOf(Edge)}.
     *  In exploring candidate nodes to cluster, these edges
     *  sometimes exist. They cause edge sorting ambiguous.
     *  A tie breaking rule is usually necessary to evaluate
     *  an exact order.
     *  <p>
     *  It's positivity or negativity of the returned value that
     *  matters. If <code>edge1</code> is preferrable to
     *  <code>edge2</code>, than a negative number is returned.
     *  Otherwise, a positive number is returned. By default,
     *  the less label number the more preferrable an edge is.
     *  @param edge1 The first edge.
     *  @param edge2 The second edge.
     *  @return A comparison indicator in sorting.
     */
    protected int _tieBreak(Edge edge1, Edge edge2) {
        return (_clusterManager.getGraph().edgeLabel(edge1) -
                _clusterManager.getGraph().edgeLabel(edge2));
    }

    /** Value definition for an edge. The value returned is for sorting
     *  purpose. By default, it is defined as GCD of the ending nodes'
     *  repetition counts.
     *  @param edge The edge to get value.
     *  @return The associated value.
     */
    protected int _valueOf(Edge edge) {
        Node source = edge.source();
        Node sink = edge.sink();
        int sourceCount =
                ((SDFGraph)_clusterManager.getGraph()).getRepetitions(source);
        int sinkCount =
                ((SDFGraph)_clusterManager.getGraph()).getRepetitions(sink);
        return ExtendedMath.gcd(sourceCount, sinkCount);
    }

    ///////////////////////////////////////////////////////////////////
    ////                      private classes                      ////

    /*  A comparator for sorting edges. Edge values are defined by the
     *  GCD of ending nodes' repetition counts. This comparator produces
     *  a descendent result.
     */
    private class downComparator implements Comparator {
        public int compare(Object edge1, Object edge2) {
            int gcd1 = _valueOf((Edge)edge1);
            int gcd2 = _valueOf((Edge)edge2);
            if (gcd1 == gcd2)
                return _tieBreak((Edge)edge1, (Edge)edge2);
            else
                return (gcd2 - gcd1);
        }
    }

    /*  A comparator for sorting edges. Edge values are defined by the
     *  GCD of ending nodes' repetition counts. This comparator produces
     *  a ascendent result.
     */
    private class upComparator implements Comparator {
        public int compare(Object edge1, Object edge2) {
            int gcd1 = _valueOf((Edge)edge1);
            int gcd2 = _valueOf((Edge)edge2);
            if (gcd1 == gcd2)
                return _tieBreak((Edge)edge1, (Edge)edge2);
            else
                return (gcd1 - gcd2);
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////                      private methods                      ////

    // main routine expanding the APGAN-clustered graph
    // @param graph The graph containing the node
    // @param node The super node to expand
    // @return The schedule saving the expansion result
    private ScheduleTree _expandAPGAN(SDFGraph graph, Node superNode) {
        ScheduleTree st=null;
        SDFGraph childGraph =
                (SDFGraph)_clusterManager.getSubgraph(superNode);

        int iterations = graph.getRepetitions(superNode);

        // atomic node
        if (childGraph == null) {
	    System.out.println("adding child: " + superNode + " with iteration count " + iterations);
            st = new ScheduleTree(superNode,iterations);
        // super node
        } else {
	    System.out.println("calling _expandAPGAN");

            // expand super node with its content adjacent nodes
            Edge edge = (Edge)childGraph.edges().iterator().next();
            Node source = edge.source();
            Node sink   = edge.sink();

	    System.out.println("adding source: " + source + " with iteration count " + iterations);
	    st = new ScheduleTree( _expandAPGAN(childGraph, source), iterations);
	    System.out.println("adding sink: " + sink);
            st.addScheduleElement(_expandAPGAN(childGraph, sink), iterations);
	    System.out.println("doning with this superNode");
        }

        return st;
    }

    ///////////////////////////////////////////////////////////////////
    ////                     private varaibles                     ////

    // The hierarchical cluster manager.
    private SDFClusterManager _clusterManager;
}



