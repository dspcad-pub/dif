/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* ABMLB (Absolute Buffer Memory Lower Bound) DPPO scheduler for SDF graphs. */

package dif.csdf.sdf.sched;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import dif.util.graph.Edge;
import dif.data.ExtendedMath;
import dif.csdf.sdf.SDFGraph;

//////////////////////////////////////////////////////////////////////////
//// ABMLBDPPOStrategy
/** ABMLB (Absolute Buffer Memory Lower Bound) DPPO scheduler for SDF graphs.
This DPPO scheduler computes an R-schedule with the criterion of ABMLB. The
result R-schedule provides a good choice for {@link ProcedureStrategy}.

@author Mingyung Ko
@version $Id: ABMLBDPPOStrategy.java 606 2008-10-08 16:29:47Z plishker $
*/

public class ABMLBDPPOStrategy extends DPPOStrategy {

    /** Constructor for a given graph and a lexical order.
     *
     *  @param graph The given SDF graph.
     *  @param lexicalOrder The lexical order in the form of
     *          </code>List<code>.
     */
    public ABMLBDPPOStrategy(SDFGraph graph, List lexicalOrder) {
        super(graph, lexicalOrder);
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /** The optimal cost is defined as : prod + cons - gcd(prod, cons)
     *  of two SDF clusters.
     *  @param i The starting index in the lexical order.
     *  @param j The ending index in the lexical order.
     */
    protected void _optimumFor(int i, int j) {
        int minCost = Integer.MAX_VALUE;
        int minSplit = 0;

        for (int split = i; split < j; split++) {
            int cost = 0;
            // Repetitions for source and sink clusters.
            int srcRepetition  = _DPPOTableElement(i, split).gcd;
            int sinkRepetition = _DPPOTableElement(split + 1, j).gcd;

            // Sum up ABMLB buffer costs for all crossing edges.
            Collection crossingEdgeCollection =
                    _crossingSDFEdges(i, j, split);
            Iterator crossingEdges = crossingEdgeCollection.iterator();
            while (crossingEdges.hasNext()) {
                // Compute new rates after clustering.
                int tnse = ((SDFGraph)graph()).
                        TNSE((Edge)crossingEdges.next());
                int newProduction  = tnse / srcRepetition;
                int newConsumption = tnse / sinkRepetition;
                // compute ABMLB buffer cost
                int gcdRates =
                        ExtendedMath.gcd(newProduction, newConsumption);
                cost += newProduction + newConsumption - gcdRates;
            }
            cost += (int)_DPPOTableElement(i, split).cost
                        + (int)_DPPOTableElement(split + 1, j).cost;

            if (cost < minCost) {
                minCost = cost;
                minSplit = split;
            }
        }
        // Save cost and split for the minimum cost.
        DPPOTableElement dppoTableElement = _DPPOTableElement(i, j);
        dppoTableElement.cost = minCost;
        dppoTableElement.split = minSplit;
    }
}



