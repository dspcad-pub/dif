/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Generalized Dynamic Programming Post Optimization (GDPPO) SDF graph
 scheduler.
*/

package dif.csdf.sdf.sched;

import java.util.Iterator;
import java.util.List;
import dif.util.graph.Edge;
import dif.util.graph.Node;
//import dif.util.sched.Firing;
//import dif.util.sched.Schedule;
//import dif.util.sched.ScheduleElement;
import dif.util.sched.ScheduleTree;
import dif.csdf.sdf.SDFEdgeWeight;
import dif.csdf.sdf.SDFGraph;

//////////////////////////////////////////////////////////////////////////
//// GDPPOStrategy
/** GDPPO stands for "Generalized Dynamic Programming Post Optimization".
GDPPO aims at minimizing SDF communication buffer requirement
with a given lexical order of actors. The order may be changed because
of the available edge delays in cycles. For details, please reference
"Software Synthesis from Dataflow Graphs"
by Shuvra S. Bhattacharyya, Praveen K. Murthy, and Edward A. Lee ,1996.

@author Mingyung Ko
@version $Id: GDPPOStrategy.java 720 2009-08-07 22:23:33Z nsane $
*/

//NS: Modified to change to Schedule Tree 08/07/09.
public class GDPPOStrategy extends DPPOStrategy {

    /** Constructor for a given graph and a lexical order.
     *  @param graph The given SDF graph.
     *  @param lexicalOrder The lexical order in the form of
     *          </code>List<code>.
     */
    public GDPPOStrategy(SDFGraph graph, List lexicalOrder) {
        super(graph, lexicalOrder);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Buffer cost for the result schedule.
     *  @return The buffer cost.
     */
    public int bufferCost() {
        return (int)optimalCost();
    }

    /** Construct an SDF schedule from the GDPPO computation.
     *  @return An SDF schedule.
     */
    public ScheduleTree schedule() {
        _invalidCost = ((SDFGraph)graph()).BMUB() + 1;
        _computeDPPO();
        if (bufferCost() >= _invalidCost) {
            throw new RuntimeException("Invalid lexical ordering " +
                    "or the graph has deadlocks.");
        }
        return (ScheduleTree)_computeSchedule(0, _tableDimension - 1, 1);
    }

    /** A desrciption of the scheduler.
     *  @return A text description.
     */
    public String toString() {
        return new String("GDPPO scheduler.");
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /** Compute an SDF schedule from the GDPPO results. GDPPO schedules
     *  may be different from regular DPPO schedules due to possible
     *  reverse splits.
     *
     *  @param left Lexical ordering index of the leftmost node.
     *  @param right Lexical ordering index of the rightmost node.
     *  @param repetition The repetition count of the node sequence.
     *  @return A schedule in mocgsched.representation.
     */
    protected ScheduleTree _computeSchedule
            (int left, int right, int repetition) {

        ScheduleTree element;
        if (left == right) {
            element = new ScheduleTree((Node)_lexicalOrder.get(left));
        } else {
            element = new ScheduleTree();
        }
	//   element.setIterationCount(repetition);
	element.setTreeLoopCount(repetition);

        if (left < right) {
            DPPOTableElement entry = _DPPOTableElement(left, right);

            DPPOTableElement vectorLeft =
                    _DPPOTableElement(left, entry.split);
            DPPOTableElement vectorRight =
                    _DPPOTableElement(entry.split + 1, right);
            int leftCount  = vectorLeft.gcd / entry.gcd;
            int rightCount = vectorRight.gcd / entry.gcd;
            ScheduleTree leftSchedule =
                    _computeSchedule(left, entry.split, leftCount);
            ScheduleTree rightSchedule =
                    _computeSchedule(entry.split + 1, right, rightCount);

            if (entry.reverseSplit) {
                ((ScheduleTree)element).insertScheduleElement(rightSchedule);
                ((ScheduleTree)element).insertScheduleElement(leftSchedule);
            } else {
                ((ScheduleTree)element).insertScheduleElement(leftSchedule);
                ((ScheduleTree)element).insertScheduleElement(rightSchedule);
            }
        }
        return element;
    }

    /** Compute the minimum cost for the node sequence from
     *  'left' to 'right'. Also set up appropriate fields of the entry
     *  at (left, right)
     *
     *  @param left Lexical ordering index of the leftmost node.
     *  @param right Lexical ordering index of the rightmost node.
     */
    protected void _optimumFor(int left, int right) {
        int chainSize = right - left + 1;
        int minimumCost = Integer.MAX_VALUE;
        DPPOTableElement entry = _DPPOTableElement(left, right);
        for (int i = 0; i < chainSize - 1; i++) {
            int signedSplitCost = _splitCost(left, right, left + i);
            int splitCost = Math.abs(signedSplitCost);

            DPPOTableElement VectorLeft =
                    _DPPOTableElement(left, left + i);
            DPPOTableElement VectorRight =
                    _DPPOTableElement(left + i + 1, right);
            int cost = (int)(splitCost + VectorLeft.cost + VectorRight.cost);

            if (cost < minimumCost) {
                minimumCost = (int)(entry.cost = cost);
                entry.split = left + i;
                if (signedSplitCost < 0)
                    entry.reverseSplit = true;
                else
                    entry.reverseSplit = false;
            }
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////

    // Split (forward/reverse) cost computation. This is an incremental way
    // to compute the cost. Split cost for (left, right, split-1) will be
    // used to make efficient computation.
    //
    // The split is "forward" if the returned cost is positive; "reverse"
    // otherwise. "Forward" split means VLeft executes before VRight;
    // "reverse" split means VRight executes before VLeft.
    private int _splitCost(int left, int right, int split) {
        if (left == split)
            _forwardSplitCost = _reverseSplitCost = 0;

        Node splitNode = (Node)_lexicalOrder.get(split);
        Iterator incidentEdges =
                ((SDFGraph)graph()).incidentEdges(splitNode).iterator();
        while (incidentEdges.hasNext()) {
            Edge edge = (Edge)incidentEdges.next();

            // Successors of the split node.
            if (edge.source() == splitNode) {
                int successorIndex = _lexicalOrder.indexOf(edge.sink());

                if (successorIndex == split) {
                    throw new RuntimeException("Self loop.");

                // The edge used to be a split edge and is now subsumed in VLeft.
                } else if ((successorIndex < split) &
                        (successorIndex >= left)) {

                    _forwardSplitCost -=
                            _splitConsumptionCost(edge, left, right);
                    _reverseSplitCost -=
                            _splitProductionCost(edge, left, right);

                // The edge used to be in VRight and is now a split edge.
                } else if ((successorIndex > split) &
                        (successorIndex <= right)) {

                    _forwardSplitCost +=
                            _splitProductionCost(edge, left, right);
                    _reverseSplitCost +=
                            _splitConsumptionCost(edge, left, right);
                }

            // Predecessors of the split node.
            } else {
                int predecessorIndex = _lexicalOrder.indexOf(edge.source());

                if (predecessorIndex == split) {
                    throw new RuntimeException("Self loop.");

                // The edge used to be a split edge and is now subsumed in VLeft.
                } else if ((predecessorIndex < split) &
                        (predecessorIndex >= left)) {

                    _forwardSplitCost -=
                            _splitProductionCost(edge, left, right);
                    _reverseSplitCost -=
                            _splitConsumptionCost(edge, left, right);

                // The edge used to be in VRight and is now a split edge.
                } else if ((predecessorIndex > split) &
                        (predecessorIndex <= right)) {

                    _forwardSplitCost +=
                            _splitConsumptionCost(edge, left, right);
                    _reverseSplitCost +=
                            _splitProductionCost(edge, left, right);
                }
            }
        }

        if (_forwardSplitCost < _reverseSplitCost) {
            return _forwardSplitCost;
        } else {
            return (-_reverseSplitCost);
        }
    }

    /*  The cost for the production/forward edge in a split.
     *
     *  @param edge The given edge.
     *  @param left Lexical ordering index of the leftmost node.
     *  @param right Lexical ordering index of the rightmost node.
     *  @return The split cost.
     */
    private int _splitConsumptionCost(Edge edge, int left, int right) {
        int delay = ((SDFEdgeWeight)edge.getWeight()).getIntDelay();
        if (delay >= _tokenExchange(edge, left, right))
            return delay;
        else
            return _invalidCost;
    }

    /*  The cost for the consumption/reverse edge in a split.
     *
     *  @param edge The given edge.
     *  @param left Lexical ordering index of the leftmost node.
     *  @param right Lexical ordering index of the rightmost node.
     *  @return The split cost.
     */
    private int _splitProductionCost(Edge edge, int left, int right) {
        int delay = ((SDFEdgeWeight)edge.getWeight()).getIntDelay();
        return (delay + _tokenExchange(edge, left, right));
    }

    /*  Number of tokens exchanged on the given edge with repect
     *  to colletion of nodes from 'left' to 'right'.
     *
     *  @param edge The given edge.
     *  @param left Lexical ordering index of the leftmost node.
     *  @param right Lexical ordering index of the rightmost node.
     *  @return The split cost.
     */
    private int _tokenExchange(Edge edge, int left, int right) {
        SDFEdgeWeight weight = (SDFEdgeWeight)edge.getWeight();
        int production = weight.getSDFProductionRate();
        int repetition = ((SDFGraph)graph()).getRepetitions(edge.source());
        int gcd = _DPPOTableElement(left, right).gcd;
        return (production * repetition / gcd);
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    /* Cost for forward split (VLeft first and tehn VRight) */
    private int _forwardSplitCost;

    /* (BMUB + 1), used to invalidate illegal splits. */
    private int _invalidCost;

    /* Cost for reverse split (VRight first and then VLeft) */
    private int _reverseSplitCost;
}




