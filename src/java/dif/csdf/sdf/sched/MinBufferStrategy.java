/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A minimum buffer cost scheduler for SDF graphs. */

package dif.csdf.sdf.sched;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import dif.util.graph.Edge;
import dif.util.graph.Node;
import dif.util.sched.Firing;
import dif.util.sched.Schedule;
import dif.util.sched.ScheduleTree;
import dif.DIFScheduleStrategy;
import dif.csdf.sdf.SDFEdgeWeight;
import dif.csdf.sdf.SDFGraph;

//////////////////////////////////////////////////////////////////////////
//// MinBufferStrategy
/** A minimum buffer cost scheduler for SDF graphs. This is an implementation
of the algorithm available in the book "Software Synthesis from Dataflow
Graphs" by Shuvra S. Bhattacharyya, Praveen K. Murthy, and Edward A. Lee,
page 54.

@author Mingyung Ko
@version $Id: MinBufferStrategy.java 720 2009-08-07 22:23:33Z nsane $
*/

public class MinBufferStrategy extends DIFScheduleStrategy {

    /** Constructor of an {@link dif.csdf.sdf.SDFGraph}.
     *
     *  @param graph The given SDF graph.
     */
    public MinBufferStrategy(SDFGraph graph) {
        super(graph);
        _initialize();
    }

    ///////////////////////////////////////////////////////////////////
    ////                        public methods                     ////

    /** Get buffer cost for the SDF edge.
     *  @param edge The SDF edge.
     *  @return The buffer cost.
     */
    public int bufferCost(Edge edge) {
        return _getMaxTokens(edge);
    }

    /** Get total buffer cost for the associated graph and the
     *  computed schedule.
     *
     *  @return The total buffer cost.
     */
    public int bufferCost() {
        int cost = 0;
        Iterator edges = graph().edges().iterator();
        while (edges.hasNext()) {
            Edge edge = (Edge)edges.next();
            cost += _getMaxTokens(edge);
        }
        return cost;
    }

    /** Compute a minimum buffer schedule.
     *  @return A minimum buffer SDF schedule.
     */
    /*   public Schedule schedule() {
        Schedule schedule = new Schedule();
        Collection fireable = _fireableActors();
        Collection deferrable = _deferrableActors(fireable);
        while (!fireable.isEmpty()) {
            Collection nondeferrable = new HashSet(fireable);
            nondeferrable.removeAll(deferrable);
            Collection candidates = null;
            if (!nondeferrable.isEmpty())
                candidates = nondeferrable;
            else
                candidates = fireable;
            Node actor = _actorOfMinIncreasedTokens(candidates);
            _fire(actor);
            schedule.add(new Firing(actor));
            // Update "fireable" and "deferrable".
            fireable = _fireableActors();
            deferrable = _deferrableActors(fireable);
        }
        _validate();
        return schedule;
	} */

    public ScheduleTree schedule() {
        Schedule schedule = new Schedule();
        Collection fireable = _fireableActors();
        Collection deferrable = _deferrableActors(fireable);
        while (!fireable.isEmpty()) {
            Collection nondeferrable = new HashSet(fireable);
            nondeferrable.removeAll(deferrable);
            Collection candidates = null;
            if (!nondeferrable.isEmpty())
                candidates = nondeferrable;
            else
                candidates = fireable;
            Node actor = _actorOfMinIncreasedTokens(candidates);
            _fire(actor);
            schedule.add(new Firing(actor));
            // Update "fireable" and "deferrable".
            fireable = _fireableActors();
            deferrable = _deferrableActors(fireable);
        }
        _validate();

	ScheduleTree st = new ScheduleTree(schedule);
        return st;
    }

    /** A description of minimum buffer scheduler.
     *
     *  @return A description of minimum buffer scheduler.
     */
    public String toString() {
        return "SDF minimum buffer scheduler.\n";
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private methods                     ////

    /*  Get the actor with minimum tokens increased among an actors collection.
     */
    private Node _actorOfMinIncreasedTokens(Collection actors) {
        int min = Integer.MAX_VALUE;
        Node minActor = null;
        Iterator actorIter = actors.iterator();
        while (actorIter.hasNext()) {
            Node actor = (Node)actorIter.next();
            int tokensIncreased = _testTokensIncreased(actor);
            if (tokensIncreased < min) {
                min = tokensIncreased;
                minActor = actor;
            }
        }
        return minActor;
    }

    /*  Get deferrable actors among the given actors collection.
     */
    private Collection _deferrableActors(Collection actors) {
        Collection deferrableActors = new HashSet();
        Iterator actorIter = actors.iterator();
        while (actorIter.hasNext()) {
            Node actor = (Node)actorIter.next();
            if (_isDeferrable(actor))
                deferrableActors.add(actor);
        }
        return deferrableActors;
    }

    /*  Fire (execute) the actor. The implementation just takes care of
     *  updating buffer information.
     */
    private void _fire(Node actor) {
        Iterator edges = graph().incidentEdges(actor).iterator();
        while (edges.hasNext()) {
            Edge edge = (Edge)edges.next();
            int consume =
                    ((SDFEdgeWeight)edge.getWeight()).getSDFConsumptionRate();
            int produce =
                    ((SDFEdgeWeight)edge.getWeight()).getSDFProductionRate();
            int tokens = _getTokens(edge);
            // If an output edge, plus production rate to the tokens.
            if (actor == edge.source()) {
                tokens += produce;
                if (tokens > _getMaxTokens(edge))
                    _setMaxTokens(edge, tokens);
            // If an input edge, minus consumption rate from the tokens.
            } else {
                tokens -= consume;
                if (tokens < 0)
                    _valid = false;
            }
            _setTokens(edge, tokens);
        }
        // If the actor is a source.
        if (((SDFGraph)graph()).sourceNodes().contains(actor)) {
            int repetitions =
                    ((Integer)_sourceRepetitionMap.get(actor)).intValue();
            _sourceRepetitionMap.put(actor, new Integer(--repetitions));
        }
    }

    /*  Get fireable actors. Fireable actors are those with sufficient input
     *  tokens to consume.
     */
    private Collection _fireableActors() {
        Collection fireableActors = new ArrayList();
        Iterator actors = graph().nodes().iterator();
        while (actors.hasNext()) {
            Node actor = (Node)actors.next();
            if (_isFireable(actor))
                fireableActors.add(actor);
        }
        return fireableActors;
    }

    /*  Get the number of tokens on the edge.
     */
    private int _getTokens(Edge edge) {
        return _bufferTokens[graph().edgeLabel(edge)];
    }

    /*  Get the maximum number of tokens ever deposited on the edge.
     */
    private int _getMaxTokens(Edge edge) {
        return _maxBufferTokens[graph().edgeLabel(edge)];
    }

    /*  Initialization for the class.
     */
    private void _initialize() {
        _valid = true;
        int bufferCount = graph().edgeCount();
        _bufferTokens = new int[bufferCount];
        _maxBufferTokens = new int[bufferCount];
        _delays = new int[bufferCount];
        // Set delays and initial tokens.
        for (int i = 0; i < bufferCount; i++) {
            int delays =
                    ((SDFEdgeWeight)graph().edge(i).getWeight()).getIntDelay();
             _bufferTokens[i] = _delays[i] = delays;
        }
        // Source repetitions are used to determine their fireability.
        _sourceRepetitionMap = new HashMap();
        Iterator sources =((SDFGraph)graph()).sourceNodes().iterator();
        while (sources.hasNext()) {
            Node source = (Node)sources.next();
            int repetitions = ((SDFGraph)graph()).getRepetitions(source);
            _sourceRepetitionMap.put(source, new Integer(repetitions));
        }
    }

    /*  Check deferrability of the given actor.
     *  Deferrable actors are those with lower priority to fire.
     *  Because of the data dependene, deferrable actors are those with
     *  ANY non-transitive output edges (edges as the only paths to sinks)
     *  providing enough tokens for sinks to consume.
     *  <p>
     *  CAUTION: Deferrable does not mean that the sink is fireable. To check
     *  fireability, ALL input edges to the sink have to be tested.
     */
    private boolean _isDeferrable(Node actor) {
        boolean deferrable = false;
        Iterator edges = graph().incidentEdges(actor).iterator();
        while (edges.hasNext()) {
            Edge edge = (Edge)edges.next();
            // If ANY "output", "non-transitive", "enough tokens to
            // consume by the sink" edge exists, the actor is deferrable.
            if ((actor == edge.source())
                    && (!_isTransitive(edge))
                    && (_getTokens(edge) >= _toConsume(edge))) {
                deferrable = true;
                break;
            }
        }
        return deferrable;
    }

    /*  Check fireability of the given actor.
     */
    private boolean _isFireable(Node actor) {
        boolean fireable = true;
        // If the actor is a source. Fireability of sources is determined by
        // their firings happened so far. They are not fireable if the firings
        // reached the repetitions. The reason is that we consider only one
        // iteration of the whole schedule.
        if (((SDFGraph)graph()).sourceNodes().contains(actor)) {
            int repetitions =
                    ((Integer)_sourceRepetitionMap.get(actor)).intValue();
            if (repetitions <= 0)
                fireable = false;

        // If the actor is not a source.
        } else {
            // Check fireability for ALL input edges.
            Iterator edges = graph().incidentEdges(actor).iterator();
            while (edges.hasNext()) {
                Edge edge = (Edge)edges.next();
                // If "input edge" and "insufficient tokens to consume", this
                // actor is NOT fireable.
                if ((actor == edge.sink()) &&
                        (_getTokens(edge) < _toConsume(edge))) {
                    fireable = false;
                    break;
                }
            }
        }
        return fireable;
    }

    /*  Check the edge's transitivity. An edge is transitive if removal
     *  of the edge does not change the reachability from the source
     *  to the sink. That is, other path(s) exist from the source to the sink
     *  in addition to the edge.
     *
     *  @param edge The edge to check.
     *  @return True if the edge is transitive; false otherwise.
     */
    private boolean _isTransitive(Edge edge) {
        Node source = edge.source();
        Node sink = edge.sink();
        boolean transitive = false;
        Iterator viaNodes =
                ((SDFGraph)graph()).reachableNodes(source).iterator();
        while (viaNodes.hasNext()) {
            Node via = (Node)viaNodes.next();
            // If any path (from the source to the sink) other than this edge,
            // it is transitive. The test "via != source" is to prevent
            // cycles for the source.
            if ((via != sink) && (via != source)
                    && ((SDFGraph)graph()).reachableNodes(via).contains(sink)) {
                transitive = true;
                break;
            }
        }
        return transitive;
    }

    /*  Set the number of tokens on the edge.
     */
    private void _setTokens(Edge edge, int tokens) {
        _bufferTokens[graph().edgeLabel(edge)] = tokens;
    }

    /*  Set the maximum number of tokens ever deposited on the edge.
     */
    private void _setMaxTokens(Edge edge, int tokens) {
        _maxBufferTokens[graph().edgeLabel(edge)] = tokens;
    }

    /*  Test the total number of tokens increased by the actor firing.
     */
    private int _testTokensIncreased(Node actor) {
        int total = 0;
        Iterator edges = graph().incidentEdges(actor).iterator();
        while (edges.hasNext()) {
            Edge edge = (Edge)edges.next();
            int consume =
                    ((SDFEdgeWeight)edge.getWeight()).getSDFConsumptionRate();
            int produce =
                    ((SDFEdgeWeight)edge.getWeight()).getSDFProductionRate();

            if (actor == edge.source())
                total += produce;
            else
                total -= consume;
        }
        return total;
    }

    /*  Get the number of tokens to consume for one firing of the sink actor.
     */
    private int _toConsume(Edge edge) {
        return ((SDFEdgeWeight)edge.getWeight()).getSDFConsumptionRate();
    }

    /*  Check validity of the schedule. RuntimeException will be thrown for
     *  invalid schedules.
     */
    private void _validate() {
        String errorMessage = new String("Scheduling error.");
        if (!_valid)
            throw new RuntimeException(errorMessage);
        for (int i = 0; i < _bufferTokens.length; i++)
            if (_bufferTokens[i] != _delays[i])
                throw new RuntimeException(errorMessage);
    }

    ///////////////////////////////////////////////////////////////////
    ////                      private variables                    ////

    // The number of net tokens deposited on edges.
    private int[] _bufferTokens;

    // Keeps track of the maximum tokens deposited on edges. They are also
    // the buffer sizes.
    private int[] _maxBufferTokens;

    // The delays on edges.
    private int[] _delays;

    // A map from source actors to their repetitions. Source repetitions are
    // used to determine fireability of sources.
    private Map _sourceRepetitionMap;

    // Check validity of the schedule along the computation.
    private boolean _valid;
}



