/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* The class for schedule tree node. */

package dif.csdf.sdf.sched;

import java.util.ArrayList;
import java.util.List;
import dif.util.graph.Node;
import dif.util.sched.ScheduleElement;

//////////////////////////////////////////////////////////////////////////
//// ScheduleTreeNode
/** The class for schedule tree node.
 *
 *  @author Mingyung Ko
 *  @version $Id: ScheduleTreeNode.java 606 2008-10-08 16:29:47Z plishker $
 */
public class ScheduleTreeNode {

    /** Constructor for given parent.
     *  @param parent The parent node.
     */
    public ScheduleTreeNode(ScheduleTreeNode parent) {
        this.parent = parent;
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Check whether it's a leaf node.
     *  @return True if leaf; false otherwise.
     */
    public boolean isLeaf() {
        return (_children == null);
    }

    /** Return left child.
     *  @return Left child.
     */
    public ScheduleTreeNode leftChild() {
        return _children[0];
    }

    /** Return all parents including immediate and grand parents. Parents
     *  are returned in order from the closest to the grandest.
     *  @return A list of parents from the closest to the grandest.
     */
    public List parents() {
        List parents = new ArrayList();
        ScheduleTreeNode node = this;
        while (node.parent != null) {
            parents.add(node.parent);
            node = node.parent;
        }
        return parents;
    }

    /** Return right child.
     *  @return Right child.
     */
    public ScheduleTreeNode rightChild() {
        return _children[1];
    }

    /** Set both children.
     *  @param left Left child.
     *  @param right Right child.
     */
    public void setChildren(ScheduleTreeNode left, ScheduleTreeNode right) {
        _children = new ScheduleTreeNode[2];
        _children[0] = left;
        _children[1] = right;
    }

    ///////////////////////////////////////////////////////////////////
    ////                        public variables                   ////

    public ScheduleTreeNode parent;
    public Node graphNode;
    public int loopCount;
    public int start;
    public int stop;
    public int duration;
    public ScheduleElement scheduleElement;

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    private ScheduleTreeNode[] _children;
}



