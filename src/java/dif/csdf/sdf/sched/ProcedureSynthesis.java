/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Procedure synthesis for a schedule. */

package dif.csdf.sdf.sched;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import dif.DIFScheduleUtilities;
import dif.util.sched.Firing;
import dif.util.sched.Schedule;
import dif.util.sched.ScheduleElement;

//////////////////////////////////////////////////////////////////////////
//// ProcedureSynthesis
/**
Procedure synthesis for a schedule. This class is a helpful
companion tool for {@link ProcedureStrategy} or any schedule that will be
implemented in procedures.

@author Mingyung Ko
@version $Id: ProcedureSynthesis.java 606 2008-10-08 16:29:47Z plishker $
*/

public class ProcedureSynthesis {

    /** Constructor of a {@link Schedule}.
     *
     *  @param schedule The given schedule.
     */
    public ProcedureSynthesis(Schedule schedule) {
        _scheduleToProcedureMap = new HashMap();
        _schedules              = new ArrayList();
        _procedures             = new ArrayList();
        _bottomProcedures       = new ArrayList();
        _topProcedure           = _synthesize(schedule, null);
    }

    ///////////////////////////////////////////////////////////////////
    ////                        public methods                     ////

    /** Get the procedure with the given procedure label.
     *  @param label The procedure label.
     *  @return The procedure.
     */
    public Procedure getProcedure(int label) {
        return (Procedure)_procedures.get(label);
    }

    /** Get the synthesized procedure given a registered schedule.
     *  Return null if the schedule was not registered.
     *
     *  @param schedule The given schedule.
     *  @return The procedure. Null if the schedule was not registered.
     */
    public Procedure getProcedure(ScheduleElement schedule) {
        // Record the original schedule iteration count.
        int iterations = schedule.getIterationCount();
        schedule.setIterationCount(1);
        // Look for any registered schedule that matches the given schedule.
        Procedure procedure = null;
        Iterator registeredSchedules = _schedules.iterator();
        while (registeredSchedules.hasNext()) {
            ScheduleElement registeredSchedule =
                    (ScheduleElement)registeredSchedules.next();
            if (DIFScheduleUtilities.compareScheduleByStructure(
                    registeredSchedule, schedule) == true) {
                procedure = (Procedure)
                        _scheduleToProcedureMap.get(registeredSchedule);
                break;
            }
        }
        // Restore the original schedule iteration count.
        schedule.setIterationCount(iterations);
        return procedure;
    }

    /** The total number of procedures synthesized.
     *  @return The total number of procedures.
     */
    public int procedureCount() {
        return _procedures.size();
    }

    /** Get the integer label of a synthesized procedure.
     *  Every synthesized procedure has a unique, global integer label.
     *  Procedures are labeled from 0.
     *
     *  @param procedure The given procedure.
     *  @return The label associated with the procedure.
     */
    public int procedureLabel(Procedure procedure) {
        return _procedures.indexOf(procedure);
    }

    /** Display the synthesized procedures in text.
     *  @return The synthesis results in text.
     */
    public String toString() {
        return toString(null);
    }

    /** Display the synthesized procedures in text.
     *  @param nameMap Map from SDF nodes to their <code>String</code> names.
     *  @return The synthesized procedures.
     */
    public String toString(Map nameMap) {
        String display = new String();
        display += procedureCount() + " procedures are synthesized:\n";
        for (int i = 0; i < _procedures.size(); i++) {
            Procedure procedure = getProcedure(i);
            display += _getName(true, procedure, nameMap) + " =";
            int calleeCount = procedure.calleeCount();
            // Display the bottom-level procedures (those w/o callees).
            if (calleeCount == 0) {
                display += " " + _getName(false, procedure, nameMap);
            // Display the intermediate procedures (those /w callees).
            } else {
                for (int j = 0; j < calleeCount; j++) {
                    Procedure callee = procedure.getCallee(j);
                    int iterations   = procedure.getCalleeIterations(j);
                    int calleeLabel  = procedureLabel(callee);
                    display += " (" + iterations + " "
                            + _getName(true, callee, nameMap) + ")";
                }
            }
            display += "\n";
        }
        return display;
    }

    /** Display the synthesized procedures in text.
     *  @param nameMap Map from SDF nodes to their names.
     *  @return The synthesis results in text.
     */
    public String toStringWithSchedules(Map nameMap) {
        return toString(nameMap) + "\n" + _displaySchedules(nameMap);
    }

    ///////////////////////////////////////////////////////////////////
    ////                      protected methods                    ////

    /** Get a procedure's name depending on its type.
     *  General names (p0, p1, p2, ...) are returned if the option is selected
     *  or the procedure calls other procedures. Otherwise, an SDF node name
     *  is returned.
     *
     *  @param isGeneralName True if general name is desired; false otherwise.
     *  @param procedure The procedure.
     *  @param nameMap A map from SDF nodes to text names.
     *  @return The name in text.
     */
    protected String _getName(
            boolean isGeneralName, Procedure procedure, Map nameMap) {

        String name = new String();
        // General procedure names (p0, p1, p2, ...) are returned.
        if (isGeneralName || (procedure.calleeCount() > 0)) {
            name = new String("p" + procedureLabel(procedure));
        // Return an SDF node name.
        } else {
            ScheduleElement scheduleElement = procedure.getSchedule();
            if (scheduleElement instanceof Firing) {
                Object firingElement =
                        ((Firing)scheduleElement).getFiringElement();
                name += (nameMap == null)
                        ? firingElement
                        : nameMap.get(firingElement);
            }
        }
        return name;
    }

    /** Register the schedule with a new procedure instantiated.
     *  A new schedule is cloned, with iterations set to 1, from the
     *  given schedule to avoid interferences to the original schedule.
     *  <p>
     *  It is users' responsibility to ensure that the schedule
     *  was not registered before.
     *
     *  @param schedule The schedule to register.
     *  @return A newly instantiated procedure.
     */
    protected Procedure _register(ScheduleElement schedule) {
        // A new schedule is cloned such that the original schedule
        // won't be accidentally modified.
        ScheduleElement cloneSchedule =
                DIFScheduleUtilities.cloneScheduleElement(schedule);
        cloneSchedule.setIterationCount(1);
        Procedure procedure = new Procedure();
        // Setup's for the new procedure and the schedule.
        procedure.setSchedule(cloneSchedule);
        _procedures.add(procedure);
        _schedules.add(cloneSchedule);
        _scheduleToProcedureMap.put(cloneSchedule, procedure);
        return procedure;
    }

    /** Synthesize a procedure given a schedule and the caller procedure.
     *  This is the main computation of this class.
     *
     *  @param schedule The schedule for synthesis.
     *  @param caller The procedure calling the synthesized procedure.
     *  @return The synthesized procedure.
     */
    protected Procedure _synthesize(
            ScheduleElement schedule, Procedure caller) {

        Procedure procedure = getProcedure(schedule);
        // No registered/instantiated procedure found for this schedule.
        if (procedure == null) {
            procedure = _register(schedule);
            // _bottomProcedures are for inheritant classes' uses.
            // This class does not use _bottomProcedures anywhere.
            if (schedule instanceof Firing)
                _bottomProcedures.add(procedure);
            // Set up the 1st level subschedules.
            else {
                Schedule trueSchedule = (Schedule)schedule;
                for (int i = 0; i < trueSchedule.size(); i++) {
                    ScheduleElement subschedule = trueSchedule.get(i);
                    Procedure callee = _synthesize(subschedule, procedure);
                    procedure.addCallee
                            (callee, subschedule.getIterationCount());
                }
            }
        }
        // For procedures not at the toppest level.
        if (caller != null)
            procedure.addCaller(caller);

        return procedure;
    }

    ///////////////////////////////////////////////////////////////////
    ////                      protected variables                  ////

    /** A collection of bottom-level procedures. Order does not matter. */
    protected Collection _bottomProcedures;

    /** A list of all procedures synthesized. */
    protected List _procedures;

    /** A list of registered schedules. */
    protected List _schedules;

    /** A map from schedules to procedures. */
    protected Map _scheduleToProcedureMap;

    /** The toppest level procedure of the synthesis. */
    protected Procedure _topProcedure;

    ///////////////////////////////////////////////////////////////////
    ////                       private methods                     ////

    /*  Display all procedures' schedules.
     *  @param nameMap A map from SDF nodes to their names.
     *  @return A text description of all the associated schedules.
     */
    private String _displaySchedules(Map nameMap) {
        String display = new String();
        display += "Associated schedules:\n";
        for (int i = 0; i < _procedures.size(); i++) {
            Procedure procedure = getProcedure(i);
            ScheduleElement scheduleElement = procedure.getSchedule();
            display += "p" + i + " = ";
            // Display 'Firing'
            if (scheduleElement instanceof Firing) {
                display += nameMap.get(
                        ((Firing)scheduleElement).getFiringElement());
            // Display 'Schedule'
            } else {
                display += ((Schedule)scheduleElement)
                        .toParenthesisString(nameMap);
            }
            display += "\n";
        }
        return display;
    }
}



