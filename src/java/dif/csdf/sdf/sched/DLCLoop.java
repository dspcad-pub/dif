/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A simulator of Dynamic Loop Count (DLC) loop. */

package dif.csdf.sdf.sched;

import java.util.Map;
import dif.util.graph.Node;
import dif.util.sched.Firing;
import dif.util.sched.Schedule;
import dif.util.sched.ScheduleElement;

//////////////////////////////////////////////////////////////////////////
//// DLCLoop
/**
A simulator of Dynamic Loop Count (DLC) loop.
This class simulates source-dlcSAS loop iterations.

<p>
Hyunok Oh, Nikil Dutt, and Soonhoi Ha.
"Single Appearance Schedule with Dynamic Loop Count for Minimum Data Buffer
from Synchronous Dataflow Graphs", CASES 2005, pages 157-165.

@author Mingyung Ko
@version $Id: DLCLoop.java 606 2008-10-08 16:29:47Z plishker $
*/

public class DLCLoop {

    /** Constructor of an {@link dif.csdf.sdf.SDFGraph}.
     *
     *  @param graph The given SDF graph.
     */
    public DLCLoop(int p, int c, Object loop, Node SDFNode) {
        _p = p;
        _c = c;
        _loop = loop;
        _SDFNode = SDFNode;
        /* Compute ceiling of c/p. */
        _n = c / p;
        if (c % p > 0)
            _n++;
    }

    ///////////////////////////////////////////////////////////////////
    ////                        public methods                     ////

    /** Convert the loop into a schedule.
     *  @return A schedule.
     */
    public ScheduleElement toSchedule() {
        Schedule schedule = new Schedule();
        Firing firing = new Firing(_SDFNode);

        /* If the loop is inner most. */
        if (_loop instanceof Node) {
            /* Sink's repetitions are multile of the source. */
            if ((_p % _c) == 0) {
                firing.setIterationCount(_p / _c);
                schedule.add(new Firing(_loop));
                schedule.add(firing);
                return schedule;
            } else {
                int iterations = _computeLoopCount();
                if (iterations > 0) {
                    Firing leftFiring = new Firing(_loop);
                    leftFiring.setIterationCount(iterations);
                    schedule.add(leftFiring);
                    schedule.add(firing);
                    return schedule;
                /* The schedule contains the 2nd iterand only. */
                } else
                    return firing;
            }
        }
        /* If the loop is not inner most. */
        if ((_p % _c) == 0) {
            firing.setIterationCount(_p / _c);
            schedule.add(((DLCLoop)_loop).toSchedule());
            schedule.add(firing);
        } else {
            int iterations = _computeLoopCount();
            if (iterations > 0) {
                schedule = new Schedule();
                for (int i = 0; i < iterations; i++)
                    schedule.add(((DLCLoop)_loop).toSchedule());
                schedule.add(firing);

            /* Zero iterations for the 1st iterand. */
            } else
                return firing;
        }
        return schedule;
    }

    /** Display the synthesis results for this loop and its sub-loops.
     *  @param nodeNameMap The map from SDF nodes to their names.
     *  @return The dlcSAS synthesis.
     */
    public String toSynthesisString(Map nodeNameMap) {
        String synthesis = new String("(" + _p + "h" + _c + " ");
        synthesis += (_loop instanceof Node)
                ? nodeNameMap.get(_loop)
                : ((DLCLoop)_loop).toSynthesisString(nodeNameMap);
        synthesis += ") " + nodeNameMap.get(_SDFNode) ;
        return synthesis;
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private methods                     ////

    /*  Compute loop count of this loop.
     */
    private int _computeLoopCount() {
        int h;
        if (_r >= _c - (_n - 1) * _p) {
            h   = _n - 1;
            _r += (_n - 1) * _p - _c;
        } else {
            h   = _n;
            _r += _n * _p - _c;
        }
        return h;
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    /* Parameters of an arc: production and consumptaion rates. */
    private int _p, _c;

    /* A variable storing the value ceil(c/p). */
    private int _n;

    /* A variable storing the present tokens deposit. */
    private int _r;

    /* The 1st iterand of a loop. */
    private Object _loop;

    /* The 2nd iterand of a loop. */
    private Node _SDFNode;
}



