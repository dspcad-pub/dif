/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A generic version of Dynamic Programming Post Optimization (DPPO). */

package dif.csdf.sdf.sched;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import dif.util.graph.Edge;
import dif.util.graph.Node;
//import dif.util.sched.Firing;
import dif.util.sched.ScheduleTree;
//import dif.util.sched.ScheduleElement
import dif.data.ExtendedMath;
import dif.DIFScheduleStrategy;
import dif.csdf.sdf.SDFGraph;

//////////////////////////////////////////////////////////////////////////
//// DPPOStrategy
/**
The base class of Dynamic Programming Post Optimization (DPPO).
No particular optimization objective is targeted and this class is
therefore abstract. The key method is {@link #_optimumFor(int, int)}
for the implementation. Subclasses are meant to implement it ONLY and nothing
else should be modified.

@author Mingyung Ko
@version $Id: DPPOStrategy.java 720 2009-08-07 22:23:33Z nsane $
*/

public abstract class DPPOStrategy extends DIFScheduleStrategy {

    /** Constructor for a given graph and a lexical order.
     *
     *  @param graph The given SDF graph.
     *  @param lexicalOrder The lexical order in the form of
     *          </code>List<code>.
     */
    public DPPOStrategy(SDFGraph graph, List lexicalOrder) {
        super(graph);
        setLexicalOrder(lexicalOrder);
        _createDPPOTable(lexicalOrder.size(), new DPPOTableElement());
    }

    /** Constructor for a given graph, a lexical order, and the type of
     *  dynamic programming table element.
     *
     *  @param graph The given SDF graph.
     *  @param lexicalOrder The lexical order in the form of
     *          </code>List<code>.
     *  @param element The type of dynamic programming table element.
     */
    public DPPOStrategy(
            SDFGraph graph, List lexicalOrder, DPPOTableElement element) {
        super(graph);
        setLexicalOrder(lexicalOrder);
        _createDPPOTable(lexicalOrder.size(), element);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Return the optimal cost.
     *  @return The optimal cost in </code>double<code>.
     */
    public double optimalCost() {
        return _DPPOTableElement(0, _tableDimension - 1).cost;
    }

    /** Construct an SDF schedule from the DPPO computation.
     *  @return An SDF schedule.
     */
    public ScheduleTree schedule() {
        _computeDPPO();
        return (ScheduleTree)_computeSchedule(0, _tableDimension - 1, 1);
    }

    /** Set the lexical order of SDF actors.
     *  @param order The desired lexical order.
     */
    public void setLexicalOrder(List order) {
        _lexicalOrder = order;
        _tableDimension = order.size();
    }

    /** A desrciption of the scheduler.
     *  @return A text description.
     */
    public String toString() {
        return new String("Generic DPPO scheduler.");
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /** Compute the given edge's buffer cost in the subgraph induced by
     *  the index range.
     *  @param edge The edge.
     *  @param i The index of the starting node.
     *  @param j The index of the ending node.
     *  @return The buffer cost.
     */
    protected int _bufferCost(Edge edge, int i, int j) {
        return ((SDFGraph)graph()).TNSE(edge) / _DPPOTableElement(i, j).gcd;
    }

    /** Dynamic programming computation.
     */
    protected void _computeDPPO() {
        for (int step = 1; step < _tableDimension; step++)
            for (int i = 0; i < _tableDimension - step; i++)
                _optimumFor(i, i + step);
    }

    /** Compute an SDF schedule from the DPPO results.
     *
     *  @param left Lexical ordering index of the leftmost node.
     *  @param right Lexical ordering index of the rightmost node.
     *  @param repetition The repetition count of the node sequence.
     *  @return A schedule in mocgraph.ScheduleTree representation.
     */
    protected ScheduleTree _computeSchedule
            (int left, int right, int repetition) {

        ScheduleTree element;
        if (left == right) {
	    //            element = new Firing((Node)_lexicalOrder.get(left));
	    element = new ScheduleTree((Node)_lexicalOrder.get(left));
        } else {
            element = new ScheduleTree();
        }
	//        element.setIterationCount(repetition);
	element.setTreeLoopCount(repetition);

        if (left < right) {
            DPPOTableElement entry = _DPPOTableElement(left, right);
            DPPOTableElement vectorLeft =
                    _DPPOTableElement(left, entry.split);
            DPPOTableElement vectorRight =
                    _DPPOTableElement(entry.split + 1, right);

            int leftCount  = vectorLeft.gcd / entry.gcd;
            int rightCount = vectorRight.gcd / entry.gcd;
            ScheduleTree leftSchedule =
                    _computeSchedule(left, entry.split, leftCount);
            ScheduleTree rightSchedule =
                    _computeSchedule(entry.split + 1, right, rightCount);

	    //   ((ScheduleTree)element).add(leftSchedule);
	    //            ((ScheduleTree)element).add(rightSchedule);
	    ((ScheduleTree)element).insertScheduleElement(leftSchedule);
            ((ScheduleTree)element).insertScheduleElement(rightSchedule);
        }
        return element;
    }

    /** The SDF edges crossing the given split in the range from i to j.
     *
     *  @param i The index of the starting node.
     *  @param j The index of the ending node.
     *  @param split The split position.
     *  @return The SDF edges across the split.
     */
    protected Collection _crossingSDFEdges(int i, int j, int split) {
        _verifyIndices(i, j, split);

        Collection crossingEdges = new HashSet();
        Collection nodes1 = _SDFNodes(i, split);
        Collection nodes2 = _SDFNodes(split + 1, j);
        Iterator edges = _SDFEdges(i, j).iterator();
        while (edges.hasNext()) {
            Edge edge = (Edge)edges.next();
            // Forward edges.
            if (nodes1.contains(edge.source()) &&
                    nodes2.contains(edge.sink())) {
                crossingEdges.add(edge);
            // Backward edges.
            } else if (nodes1.contains(edge.sink()) &&
                    nodes2.contains(edge.source())) {
                crossingEdges.add(edge);
            }
        }
        return crossingEdges;
    }

    /** Get the element of the DPPO table. DPPO assumes a given lexical
     *  ordering of nodes. Index number i should be less or equal to j.
     *
     *  @param i The index of the starting (left) node
     *  @param j The index of the ending (right) node
     *  @return The desired element in {@link DPPOMatrixElement}.
     */
    protected DPPOTableElement _DPPOTableElement(int i, int j) {
        _verifyIndices(i, j);
        ArrayList row = (ArrayList)_DPPOTable.get(i);
        return (DPPOTableElement)row.get(j - i);
    }

    /** The optimal results along the elements with index i to j.
     *  In the implementation, the optimal cost, split, and any relevant
     *  attributes should be computed and stored.
     *
     *  @param i The element with index i.
     *  @param j The element with index j.
     */
    abstract protected void _optimumFor(int i, int j);

    /** Get the SDF edges for given nodes. The nodes are specified
     *  by their index range in the lexical order. The result returned
     *  is edges of the subgraph induced from the specified nodes.
     *
     *  @param i The index of the starting node.
     *  @param j The index of the ending node.
     *  @return The induced SDF edges.
     */
    protected Collection _SDFEdges(int i, int j) {
        _verifyIndices(i, j);

        Collection SDFEdges = new HashSet();
        Collection SDFNodes = _SDFNodes(i, j);
        Iterator edges = graph().edges().iterator();
        while (edges.hasNext()) {
            Edge edge = (Edge)edges.next();
            if (SDFNodes.contains(edge.source()) &&
                    SDFNodes.contains(edge.sink()))
                SDFEdges.add(edge);
        }
        return SDFEdges;
    }

    /** Get SDF nodes for the given lexical order indices.
     *
     *  @param i The index of the starting node.
     *  @param j The index of the ending node.
     *  @return The correspondent SDF nodes.
     */
    protected Collection _SDFNodes(int i, int j) {
        _verifyIndices(i, j);

        Collection SDFNodes = new HashSet();
        for (int index = i; index <= j; index++)
            SDFNodes.add(_lexicalOrder.get(index));
        return SDFNodes;
    }

    /** Verify the given pair of indices and a split. The index pair
     *  declares a section of the whole lexical order.
     *  The pair has to obey the sequence and boundaries of
     *  the lexical order.
     *
     *  @param i The starting index.
     *  @param j The ending index.
     *  @exception IllegalArgumentException Exception is thrown if
     *             boundaries are violated or </code>i > j<code>.
     */
    protected void _verifyIndices(int i, int j) {
        _verifyIndices(i, j, i);
    }

    /** Verify the given pair of indices and a split. The index pair
     *  declares a section of the whole lexical order.
     *  The pair has to obey the sequence and boundaries of
     *  the lexical order. The split position should only occur within
     *  the specified section.
     *
     *  @param i The starting index.
     *  @param j The ending index.
     *  @param split The split position.
     *  @exception IllegalArgumentException Exception is thrown if
     *             boundaries are violated, </code>i > j<code>, or
     *             illegal split position is specified.
     */
    protected void _verifyIndices(int i, int j, int split) {
        // Verify the sequence.
        if (i > j) {
            throw new IllegalArgumentException(
                    "i must be less or equal to j.");
        }
        // Verify the boundaries.
        if (i < 0 || j >= _tableDimension) {
            throw new IllegalArgumentException(
                    "Index number exceeds the table dimension.");
        }
        // Verify the split position.
        if ((i < j) && (split < i || split >= j)) {
            throw new IllegalArgumentException(
                    "Illegal split.");
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /** The given lexical order of nodes for DPPO. */
    protected List _lexicalOrder;

    /** The dimension of the dynamic programming table. It is equal
     *  to the length of lexical order.
     */
    protected int _tableDimension;

    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////

    /* Compute GCD for all DPPO table elements. */
    private void _computeGCD() {
        for (int i = 0; i < _tableDimension; i++) {
            // Set [i, i] to repetitions
            Node startNode = (Node)_lexicalOrder.get(i);
            int startRepetitions =
                    ((SDFGraph)graph()).getRepetitions(startNode);
            _DPPOTableElement(i, i).gcd = startRepetitions;
            // Compute [i, j]
            for (int j = (i + 1); j < _tableDimension; j++) {
                Node endNode = (Node)_lexicalOrder.get(j);
                int endRepetitions =
                        ((SDFGraph)graph()).getRepetitions(endNode);
                int previousGcd =
                        _DPPOTableElement(i, j - 1).gcd;
                int gcd = ExtendedMath.gcd(previousGcd, endRepetitions);
                _DPPOTableElement(i, j).gcd = gcd;
            }
        }
    }

    /*  Creat a table for the dynamic programming computation.
     *  Only a triangular table is necessary. This method creates the table
     *  in the form of an ArrayList of ArrayLists.
     *  To access each element, see {@link #_DPPOTableElement()}.
     *
     *  @param dimension Dimension (size) of the matrix.
     *  @param element The DPPO table element.
     */
    private void _createDPPOTable(int dimension, DPPOTableElement element) {
        // Create the table.
        _DPPOTable = new ArrayList();
        for (int i = 0; i < dimension; i++) {
            ArrayList row = new ArrayList();
            for (int j = 0; j < (dimension - i); j++) {
                try {
                    row.add(element.getClass().newInstance());
                } catch (Exception exception) {}
            }
            _DPPOTable.add(row);
        }
        _computeGCD();
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    /* The matrix for dynamic programming. */
    private List _DPPOTable;
}




