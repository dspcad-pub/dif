/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A base scheduler using multirate cycle breaking framework. */

package dif.csdf.sdf.sched;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import dif.util.graph.DirectedAcyclicGraph;
import dif.util.graph.DirectedGraph;
import dif.util.graph.Edge;
import dif.util.graph.Node;
import dif.util.sched.Firing;
import dif.util.sched.Schedule;
import dif.util.sched.ScheduleElement;
import dif.util.sched.ScheduleTree;
import dif.DIFScheduleStrategy;
import dif.csdf.sdf.SDFClusterManager;
import dif.csdf.sdf.SDFEdgeWeight;
import dif.csdf.sdf.SDFGraph;

//////////////////////////////////////////////////////////////////////////
//// MCBStrategy
/** A base scheduler using multirate cycle breaking framework.
The scheduler computes a schedule by removing qualified graph edges
in a hope to make it acyclic. Efficient acyclic schedulers can
be applied then.

<p>
In this class, {@link #_cycleBreaking(SDFGraph)},
{@link #_acyclicScheduling(SDFGraph, int)},
{@link #_tightScheduling(SDFGraph, int)}, and
{@link #_computeSchedule(SDFGraph, int)} are to be refined according to
a specific algorithm. This base class implements edge removal
with largest normalized delays (delay / TNSE). Generic
topological sorting is implemented in acyclic scheduling while
nothing is currently done in tight scheduling.

@author Mingyung Ko
@version $Id: MCBStrategy.java 720 2009-08-07 22:23:33Z nsane $
*/

public class MCBStrategy extends DIFScheduleStrategy {

    /** Constructor for a given SDF graph.
     *  @param graph The given SDF graph.
     */
    public MCBStrategy(SDFGraph graph) {
        super(graph);
        _removedEdgesMap = new HashMap();
        _clusterManager  = new SDFClusterManager(graph);
    }

    ///////////////////////////////////////////////////////////////////
    ////                        public methods                     ////

    /** Compute the multirate cycle breaking schedule.
     *  @return The schedule.
     */
    //    public Schedule schedule() {
	/*   return (Schedule)
	     _computeSchedule((SDFGraph)_clusterManager.getGraph(), 1); */
    public ScheduleTree schedule() {
	ScheduleTree st = new ScheduleTree((Schedule)_computeSchedule((SDFGraph)_clusterManager.getGraph(), 1));
	return st;
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /** Acyclic scheduling for the given graph. The graph
     *  must be acyclic and assumes at least 2 nodes.
     *  <p>
     *  The implementation is just a generic topologically sorted
     *  and linear schedule. Users can refine this method.
     *
     *  @param graph The given graph in {@link SDFGraph}.
     *  @param graphRepetitions Repetitions for the given graph.
     *  @return The acyclic schedule.
     */
    protected ScheduleElement _acyclicScheduling(
            SDFGraph graph, int graphRepetitions) {

        // Initialize a new schedule for the graph.
        Schedule schedule = new Schedule();
        schedule.setIterationCount(graphRepetitions);

        // Compute topological sorting as the acyclic schedule.
        DirectedAcyclicGraph acyclicGraph = graph.toDirectedAcyclicGraph();
        Object[] sortedWeights = acyclicGraph.topologicalSort();
        for (int i = 0; i < sortedWeights.length; i++) {
            Node node = graph.node(sortedWeights[i]);
            int repetitions = graph.getRepetitions(node);

            // Expand super nodes and compute associated subgraphs.
            if (_clusterManager.isSuperNode(node)) {
                SDFGraph subgraph = (SDFGraph)_clusterManager.getSubgraph(node);
                schedule.add(_computeSchedule(subgraph, repetitions));
            // Get a firing from SDFSchedule.
            } else {
                Firing firing = new Firing(node);
                firing.setIterationCount(repetitions);
                schedule.add(firing);
            }
        }
        return schedule;
    }

    /** Compute the schedule for the given SDF graph. The method handles
     *  the special case of one-node graph.
     *
     *  @param graph The given graph.
     *  @param graphRepetitions Repetitions for the given graph.
     *  @return The result Ptolemy schedule.
     */
    protected ScheduleElement _computeSchedule(
            SDFGraph graph, int graphRepetitions) {

        ScheduleElement element;

        // The graph has only one atomic node.
        if (graph.nodeCount() == 1) {
            Node node = (Node)graph.nodes().iterator().next();
            element = new Firing(node);
            element.setIterationCount(graphRepetitions);

        // Graphs with at least 2 nodes.
        } else {
            DirectedGraph[] sccs = graph.sccDecomposition();

            // The graph is strongly connected.
            if (sccs.length == 1) {
                do {
                    Collection edgesToRemove = _cycleBreaking(graph);
                    // No edges to break. The graph is tight.
                    if (edgesToRemove.isEmpty()) {
                        _putBackEdges(graph);
                        return _tightScheduling(graph, graphRepetitions);

                    // Remove an edge with maximum normalized delay.
                    } else {
                        Edge edge = (Edge)edgesToRemove.iterator().next();
                        ArrayList edgeList = new ArrayList();
                        edgeList.add(edge);
                        _removeEdges(graph, edgeList);
                    }
                    sccs = graph.sccDecomposition();
                } while (sccs.length == 1);
            }
            // The graph must be SCC partitionable.
            for (int i = 0; i < sccs.length; i++)
                _clusterManager.clusterNodes(graph, sccs[i].nodes());
            element = _acyclicScheduling(graph, graphRepetitions);
        }
        return element;
    }

    /** Cycle breaking rule for the graph. The candidate edges
     *  to remove to break cycles are return as a collection.
     *  However, these edges are not actually removed from graph.
     *  To remove edges, please use
     *  {@link #_removeEdges(SDFGraph, Collection)}.
     *  <p>
     *  The implementation here is to select the edge with
     *  largest normalized delays (delay / TNSE). However, users
     *  can redefine this method.
     *
     *  @param graph The given {@link SDFGraph} to break cycles.
     *  @return The collection of edges to remove to break cycles.
     */
    protected Collection _cycleBreaking(SDFGraph graph) {
        double maxNormalizedDelay = 0.0;
        Edge maxDelayEdge = null;

        // Compute and pick the edge with largest delays.
        Iterator edges = graph.edges().iterator();
        while (edges.hasNext()) {
            Edge edge = (Edge)edges.next();
            int delay = ((SDFEdgeWeight)edge.getWeight()).getIntDelay();
            double normalizedDelay =
                    (double)delay / (double)graph.TNSE(edge);
            if (normalizedDelay > maxNormalizedDelay) {
                maxNormalizedDelay = normalizedDelay;
                maxDelayEdge = edge;
            }
        }
        ArrayList edgesToRemove = new ArrayList();
        if (maxNormalizedDelay >= 1.0)
            edgesToRemove.add(maxDelayEdge);
        return edgesToRemove;
    }

    /** Test the graph's tight inter-dependency.
     *
     *  @graph The given {@link SDFGraph}.
     *  @return True if the graph is tightly interdependent.
     */
    protected boolean _isTight(SDFGraph graph) {
        SDFGraph cloneGraph = (SDFGraph)graph.clone();
        ArrayList bigDelayEdges = new ArrayList();

        // Collect edges with sufficient delays.
        Iterator edges = cloneGraph.edges().iterator();
        while (edges.hasNext()) {
            Edge edge = (Edge)edges.next();
            int delay = ((SDFEdgeWeight)edge.getWeight()).getIntDelay();
            if (delay >= cloneGraph.TNSE(edge))
                bigDelayEdges.add(edge);
        }
        // Remove edges with big delays.
        edges = bigDelayEdges.iterator();
        while (edges.hasNext())
            cloneGraph.removeEdge((Edge)edges.next());

        // Graph is tight if it's cyclic after edge removal.
        return !cloneGraph.isAcyclic();
    }

    /** Edges to put back into graph. This method is usually
     *  executed before tight scheduling.
     *
     *  @param graph The given {@link SDFGraph}.
     *  @param edges The edges to put back.
     */
    protected void _putBackEdges(SDFGraph graph) {
        if (!_removedEdgesMap.containsKey(graph))
            return;

        Iterator edgesToPutBack =
                ((Collection)_removedEdgesMap.get(graph)).iterator();
        while (edgesToPutBack.hasNext())
            graph.addEdge((Edge)edgesToPutBack.next());
        // Remove the map entry from _removedEdgesMap.
        _removedEdgesMap.remove(graph);

    }

    /** Remove edges from the given graph. The removed edges are
     *  kept in <code>_removedEdgesMap</code> for possibe
     *  future putting back.
     *
     *  @param graph The given {@link SDFGraph}.
     *  @param edgeCollection The edges to remove.
     */
    protected void _removeEdges(
            SDFGraph graph, Collection edgeCollection) {

        /* Record removed edges in _removedEdgesMap. */
        // Remove edges from graphs not existent in _removedEdgesMap.
        if (!_removedEdgesMap.containsKey(graph)) {
            HashSet edgesToRecord = new HashSet();
            edgesToRecord.addAll(edgeCollection);
            _removedEdgesMap.put(graph, edgesToRecord);
        // Remove edges from graphs existent in _removedEdgesMap.
        } else {
            HashSet edgesExistent = (HashSet)_removedEdgesMap.get(graph);
            edgesExistent.addAll(edgeCollection);
        }
        // Actual edges removal from graphs.
        Iterator edges = edgeCollection.iterator();
        while (edges.hasNext())
            graph.removeEdge((Edge)edges.next());
    }

    /** Tight scheduling for the given graph. The method assumes
     *  an input graph with at least 2 nodes.
     *  <p>
     *  <em>FIXME</em>: There is no implementation so far and
     *  users should define this method.
     *
     *  @param graph The given {@link SDFGraph}.
     *  @param graphRepetitions Repetitions for the given graph.
     *  @return The tight schedule.
     */
    protected ScheduleElement _tightScheduling(
            SDFGraph graph, int graphRepetitions) {
        throw new UnsupportedOperationException(
                "No implementation for tight scheduling.");
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    // The manager for maintaining hierarchical SDF clustered graph.
    private SDFClusterManager _clusterManager;

    // A map from graphs to the removed edges.
    private HashMap _removedEdgesMap;
}



