/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A topological sorting for minimum SDPPO buffer sharing cost. */

package dif.csdf.sdf.sched;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import dif.DIFGraph;
import dif.csdf.sdf.SDFGraph;
import dif.graph.BaseTopSortStrategy;
import dif.util.graph.Node;

//////////////////////////////////////////////////////////////////////////
//// SDPPOTopSortStrategy
/**
A topological sorting for minimum SDPPO buffer sharing cost.
Source node selection is based on the SDPPO buffer sharing cost for the
partially sorted list.

@author Mingyung Ko
@version $Id: SDPPOTopSortStrategy.java 606 2008-10-08 16:29:47Z plishker $
*/

public class SDPPOTopSortStrategy extends BaseTopSortStrategy {

    /** Constructor with a {@link DIFGraph}.
     *  @param graph The given SDF graph.
     */
    public SDPPOTopSortStrategy(SDFGraph graph) {
        super(graph);
    }

    ///////////////////////////////////////////////////////////////////
    ////                        public methods                     ////

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /** Compute a topological sorting order. The result is
     *  returned as a <code>List</code>, where the order begins at index 0.
     *
     *  @return A topological sorting order.
     */
    protected Object _compute() {
        _validate();

        // Add an arbitrary source node to the partially sorted list.
        // This step is necessary for the proper operation of SDPPO.
        // SDPPO cannot work on an SDF graph with single node.
        Node firstSource =
                (Node)directedGraph().sourceNodes().iterator().next();
        SDFGraph graphUnsorted = (SDFGraph)graph().cloneAs(new SDFGraph());
        graphUnsorted.removeNode(firstSource);
        List unsorted = new ArrayList(graphUnsorted.nodes());
        List sorted = new ArrayList();
        sorted.add(firstSource);

        while (graphUnsorted.nodeCount() > 1) {
            int minCost = Integer.MAX_VALUE;
            Node desiredSource = null;
            // For each source node appended to the partially sorted list,
            // test the SDPPO cost and get the source node with min cost.
            Iterator sources = graphUnsorted.sourceNodes().iterator();
            while (sources.hasNext()) {
                Node source = (Node)sources.next();
                List sortedReplica = new ArrayList(sorted);
                sortedReplica.add(source);
                // Create a subgraph containing the partially sorted nodes
                // and the source node to test.
                SDFGraph graphSorted =
                        (SDFGraph)graph().subgraph(sortedReplica)
                        .cloneAs(new SDFGraph());

                // If the source is also a source in the original SDF graph 
                // (i.e., this is a multi-source SDF graph), go ahead adding 
                // it without SDPPO computation. 
                // In this case, 'graphSorted' is a disconnected graph and
                // will crash SDPPO execution.
                if (graphSorted.connectedComponents().size() > 1) {
                    desiredSource = source;
                    break;
                }

                SDPPOStrategy sdppo =
                        new SDPPOStrategy(graphSorted, sortedReplica);
                sdppo.schedule();
                int cost = (int)sdppo.optimalCost();
                if (cost < minCost) {
                    minCost = cost;
                    desiredSource = source;
                }
            }
            // Remove the desired source node from consideration.
            // Add the desired source node to the partially sorted list.
            graphUnsorted.removeNode(desiredSource);
            unsorted.remove(desiredSource);
            sorted.add(desiredSource);
        }
        // Add the only remaining node.
        sorted.add(graphUnsorted.node(0));
        return sorted;
    }

    ///////////////////////////////////////////////////////////////////
    ////                      protected variables                  ////


    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////


    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

}



