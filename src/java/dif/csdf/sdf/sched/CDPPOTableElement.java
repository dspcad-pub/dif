/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Table entry for DPPO for code size optimization. */

package dif.csdf.sdf.sched;

import dif.util.sched.ScheduleElement;

//////////////////////////////////////////////////////////////////////////
//// CDPPOTableElement
/**
Table entry of adapted DPPO for code size optimization.
<p>
CAUTION: This class does NOT extend {@link DPPOTableElement}.

@author Mingyung Ko
@version $Id: CDPPOTableElement.java 606 2008-10-08 16:29:47Z plishker $
*/

public class CDPPOTableElement {

    /** The cost for the element */
    public int codeSize;

    /** The optimum sub-schedule. */
    public ScheduleElement schedule;
}



