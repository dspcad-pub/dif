/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A binary tree expression of R-schedule. */

package dif.csdf.sdf.sched;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import dif.util.graph.Node;
import dif.util.sched.Firing;
import dif.util.sched.Schedule;
import dif.util.sched.ScheduleElement;

//////////////////////////////////////////////////////////////////////////
//// ScheduleTree
/** A binary tree expression of R-schedule. Schedule tree is a binary tree
 *  where all nodes are associated with schedule loop counts and leaves with
 *  SDF actors in addition. The alternative representation helps visualizing a
 *  schedule and devising efficient scheduling algorithms.
 *
 *  @author Mingyung Ko
 *  @version $Id: ScheduleTree.java 606 2008-10-08 16:29:47Z plishker $
 */
public class ScheduleTree {

    /** Constructor for a given schedule.
     *  @param schedule The given schedule.
     */
    public ScheduleTree(Schedule schedule) {
        _graphNodeMap = new HashMap();
        _leafNodes = new ArrayList();
        _root = _buildTree(schedule, null);
        _computeNodeTimes(_root, 0);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Get schedule tree leaf node with given graph node.
     *  @param graphNode The given graph node.
     *  @return A schedule tree node corresponding to the given graph node.
     */
    public ScheduleTreeNode leafNode(Node graphNode) {
        return (ScheduleTreeNode)_graphNodeMap.get(graphNode);
    }

    /** Get a list of the tree leaf nodes.
     *  @return A list of leaf nodes.
     */
    public List leafNodes() {
        return _leafNodes;
    }

    /** The least(youngest/closet/nearest) common parent of given
     *  pair of schedule tree nodes
     *  @param uNode The first given node.
     *  @param vNode The second given node.
     *  @return The least common parent.
     */
    public ScheduleTreeNode leastParent(
            ScheduleTreeNode uNode, ScheduleTreeNode vNode) {
        List uParents = uNode.parents();
        List vParents = vNode.parents();

        // Distinguish short and long parents.
        List shortParents = uParents;
        List longParents  = vParents;
        if (uParents.size() > vParents.size()) {
            shortParents = vParents;
            longParents  = uParents;
        }

        ScheduleTreeNode leastParent = null;
        for (int i = 0; i < shortParents.size(); i++) {
            ScheduleTreeNode parent = (ScheduleTreeNode)shortParents.get(i);
            if (longParents.contains(parent)) {
                leastParent = parent;
                break;
            }
        }
        return leastParent;
    }

    /** Common parents for a given pair of nodes. The parents are returned
     *  in the form of List, from least(youngest) to greatest(oldest).
     *  @param uNode The first node.
     *  @param vNode The second node.
     *  @return A list of common parents.
     */
    public List parents(ScheduleTreeNode uNode, ScheduleTreeNode vNode) {
        ArrayList parents = new ArrayList();
        ScheduleTreeNode leastParent = leastParent(uNode, vNode);
        parents.add(leastParent);
        parents.addAll(leastParent.parents());
        return parents;
    }

    /** Get the tree root node.
     *  @return The tree root.
     */
    public ScheduleTreeNode root() {
        return _root;
    }

    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////

    /*  Build tree structure only, not including time computation.
     *  @param scheduleElement The given schedule.
     *  @param parent Parent schedule tree node.
     *  @param graphNodeMap A map from graph node to schedule tree node.
     *  @return Root node of the tree.
     */
    private ScheduleTreeNode _buildTree(
            ScheduleElement scheduleElement,
            ScheduleTreeNode parent) {

        ScheduleTreeNode root = new ScheduleTreeNode(parent);
        root.scheduleElement = scheduleElement;
        root.loopCount = scheduleElement.getIterationCount();
        root.parent    = parent;

        // Build tree by a depth first search way.
        if (scheduleElement instanceof Firing) {
            root.graphNode =
                    (Node)((Firing)scheduleElement).getFiringElement();
            _graphNodeMap.put(root.graphNode, root);
            _leafNodes.add(root);
        } else {
            Schedule schedule = (Schedule)scheduleElement;
            if (schedule.size() != 2)
                throw new RuntimeException(
                        "The input schedule is not an R-Schedule.");
            ScheduleElement first  = schedule.get(0);
            ScheduleElement second = schedule.get(1);
            root.setChildren(
                    _buildTree(first,  root), _buildTree(second, root) );
        }
        return root;
    }

    /*  Compute and set duration for given node.
     *  @param node The given node.
     *  @return The duration.
     */
    private int _computeDurations(ScheduleTreeNode node) {
        if (node.isLeaf())
            node.duration = 1;
        else {
            int leftDuration  = _computeDurations(node.leftChild());
            int rightDuration = _computeDurations(node.rightChild());
            node.duration = node.loopCount * (leftDuration + rightDuration);
        }
        return node.duration;
    }

    /*  Compute and set times for all tree nodes.
     *  @param root The tree root node.
     *  @param start A given start time for whole tree.
     */
    private void _computeNodeTimes(ScheduleTreeNode root, int start) {
        _computeDurations(root);
        _computeNodeStartStop(root, start);
    }

    /*  Compute and set Start and Stop times for all tree nodes.
     *  @param node The given node.
     *  @param start A given start time for this node.
     */
    private void _computeNodeStartStop(
            ScheduleTreeNode node, int start) {
        node.start = start;
        node.stop  = start + node.duration;
        if (!node.isLeaf()) {
            _computeNodeStartStop(node.leftChild(),  start);
            _computeNodeStartStop(node.rightChild(), node.leftChild().stop);
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    // The tree root.
    private ScheduleTreeNode _root;

    // A map from graph node to tree node.
    private Map _graphNodeMap;

    // Ordered leaf nodes.
    private List _leafNodes;
}



