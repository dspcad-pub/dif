/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A flat scheduler for SDF graphs. */

package dif.csdf.sdf.sched;

import dif.util.graph.DirectedGraph;
import dif.util.graph.DirectedAcyclicGraph;
import dif.util.graph.Node;
import dif.util.sched.ScheduleTree;
import dif.DIFScheduleStrategy;
import dif.csdf.sdf.SDFGraph;

//////////////////////////////////////////////////////////////////////////
//// FlatStrategy
/**
A flat scheduler for SDF graphs. Since a topological sorting is performed,
the graph is required to be acyclic (see {@link #valid()}).

@author Mingyung Ko
@version $Id: FlatStrategy.java 606 2008-10-08 16:29:47Z plishker $
*/

public class FlatStrategy extends DIFScheduleStrategy {

    /** Constructor of an {@link dif.csdf.sdf.SDFGraph}.
     *
     *  @param graph The given SDF graph.
     */
    public FlatStrategy(SDFGraph graph) {
        super(graph);
    }

    ///////////////////////////////////////////////////////////////////
    ////                        public methods                     ////

    /** Compute a flat schedule for an acyclic SDF graph.
     *  The order is decided by an generic topplogical sorting result.
     *
     *  @return A flat SDF schedule.
     */
    public ScheduleTree schedule() {
        DirectedAcyclicGraph acyclicGraph =
                ((SDFGraph)graph()).toDirectedAcyclicGraph();

	//        ScheduleTree schedule = new ScheduleTree();
        ScheduleTree schedule = null;

        Object[] sorted = acyclicGraph.topologicalSort();
        for (int i = 0; i < sorted.length; i++) {
            // "sorted" is an array of "nodeWeights", not Nodes.
            Node node = ((SDFGraph)graph()).node(sorted[i]);
	    if(schedule == null) {
		schedule = new ScheduleTree(node, ((SDFGraph)graph()).getRepetitions(node));
	    } else {
		schedule.addScheduleElement(node, ((SDFGraph)graph()).getRepetitions(node));
	    }
	}
        return schedule;
    }

    /** A description of flat scheduler.
     *
     *  @return A description of flat scheduler.
     */
    public String toString() {
        return "SDF flat scheduler.\n";
    }

    /** Acyclic property is validated for flat scheduling.
     *
     *  @return True if the graph is acyclic.
     */
    public boolean valid() {
        return ((DirectedGraph)graph()).isAcyclic();
    }
}



