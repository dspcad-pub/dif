/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Dynamic Programming Post Optimization (DPPO) for balancing bank capacity. */

package dif.csdf.sdf.sched;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import dif.util.graph.Edge;
import dif.csdf.sdf.SDFGraph;

//////////////////////////////////////////////////////////////////////////
//// BDPPOStrategy
/** This is a DPPO for balancing capacity over a dual-memory-bank architecture.

@author Mingyung Ko
@version $Id: BDPPOStrategy.java 606 2008-10-08 16:29:47Z plishker $
*/

public class BDPPOStrategy extends DPPOStrategy {

    /** Constructor for an SDF graph, a lexical order, a preliminary capacity
     *  gap, and two collections of edges. Collections of edges imply the
     *  bank assignment for SDF buffers. The gap must be the first
     *  bank capacity minus the second one.
     *
     *  @param graph The given SDF graph.
     *  @param lexicalOrder The lexical order.
     *  @param gap The preliminary capacity difference.
     *  @param edges1 SDF edges associated with the first bank.
     *  @param edges2 SDF edges associated with the second bank.
     */
    public BDPPOStrategy(SDFGraph graph, List lexicalOrder,
            double gap, Collection edges1, Collection edges2) {
        super(graph, lexicalOrder);
        _gap = gap;
        _sdfEdges1 = edges1;
        _sdfEdges2 = edges2;
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Get the optimal bank capacity difference. The difference is the first
     *  bank's capacity minus the second one's.
     *  @return The difference.
     */
    public double capacityDifference() {
        return _gap + optimalCost();
    }

    /** A desrciption of the scheduler.
     *  @return A text description.
     */
    public String toString() {
        return new String("BDPPO scheduler.");
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /** Compute the minimum cost for the node sequence from
     *  'left' to 'right'. Also set up appropriate fields of the entry
     *  at (left, right)
     *
     *  @param left Lexical ordering index of the leftmost node.
     *  @param right Lexical ordering index of the rightmost node.
     */
    protected void _optimumFor(int left, int right) {
        double optimalCost = 100000.0;
        int optimalSplit = left;
        int gcd = _DPPOTableElement(left, right).gcd;

        for (int split = left; split < right; split++) {
            double cost = _DPPOTableElement(left, split).cost +
                    _DPPOTableElement(split + 1, right).cost;
            Iterator cEdges = _crossingSDFEdges(left, right, split).iterator();
            while (cEdges.hasNext()) {
                Edge cEdge = (Edge)cEdges.next();
                double bufferSize = _bufferCost(cEdge, left, right);
                if (_sdfEdges1.contains(cEdge))
                    cost += bufferSize;
                else if (_sdfEdges2.contains(cEdge))
                    cost -= bufferSize;
                else
                    throw new RuntimeException("SDF edges are not "
                            + "partitioned properly");
            }
            // Compare absolute memory usage difference.
            if (Math.abs(cost + _gap) < Math.abs(optimalCost + _gap)) {
                optimalCost = cost;
                optimalSplit = split;
            }
        }
        _DPPOTableElement(left, right).cost = optimalCost;
        _DPPOTableElement(left, right).split = optimalSplit;
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    // The preliminary bank capacity difference.
    private double _gap;

    // SDF edge collections indexed by their bank assignment.
    private Collection _sdfEdges1, _sdfEdges2;
}





