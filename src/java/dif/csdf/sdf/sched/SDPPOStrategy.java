/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A Dynamic Programming Post Optimization (DPPO) for buffer sharing. */

package dif.csdf.sdf.sched;

import java.util.Iterator;
import java.util.List;
import dif.util.graph.Edge;
import dif.csdf.sdf.SDFGraph;

//////////////////////////////////////////////////////////////////////////
//// SDPPOStrategy
/** A DPPO scheduler for buffer sharing.

@author Mingyung Ko
@version $Id: SDPPOStrategy.java 606 2008-10-08 16:29:47Z plishker $
*/

public class SDPPOStrategy extends DPPOStrategy {

    /** Constructor for a given graph and a lexical order.
     *  @param graph The given SDF graph.
     *  @param lexicalOrder The lexical order in the form of
     *          </code>List<code>.
     */
    public SDPPOStrategy(SDFGraph graph, List lexicalOrder) {
        super(graph, lexicalOrder);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** A desrciption of the scheduler.
     *  @return A text description.
     */
    public String toString() {
        return new String("DPPO scheduler for buffer sharing.");
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /** The optimal buffer sharing results along elements of index i to j.
     *  The optimal cost and split are stored in the associated table element.
     *
     *  @param i The element with index i.
     *  @param j The element with index j.
     */
    protected void _optimumFor(int i, int j) {
        double minCost = Double.MAX_VALUE;
        int minSplit = i;
        for (int split = i; split < j; split++) {
            double cost = _crossingBufferCost(i, j, split) + Math.max(
                    _DPPOTableElement(i, split).cost,
                    _DPPOTableElement(split + 1, j).cost);
            if (cost < minCost) {
                minCost = cost;
                minSplit = split;
            }
        }
        _DPPOTableElement(i, j).cost  = minCost;
        _DPPOTableElement(i, j).split = minSplit;
    }

    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////

    /*  Buffer cost for crossing edges.
     *  @param i The starting index.
     *  @param j The ending index.
     *  @param split The split position.
     */
    private double _crossingBufferCost(int i, int j, int split) {
        int gcdRepetitions = _DPPOTableElement(i, j).gcd;
        double cost = 0.0;
        Iterator cEdges = _crossingSDFEdges(i, j, split).iterator();
        while (cEdges.hasNext()) {
            Edge cEdge = (Edge)cEdges.next();
            cost += (double)
                    (((SDFGraph)graph()).TNSE(cEdge) / gcdRepetitions);
        }
        return cost;
    }
}




