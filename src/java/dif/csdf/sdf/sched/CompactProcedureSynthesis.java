/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Compact procedure synthesis for a schedule. */

package dif.csdf.sdf.sched;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import dif.util.sched.Schedule;

//////////////////////////////////////////////////////////////////////////
//// CompactProcedureSynthesis
/**
Compact procedure synthesis for a schedule.
Synthesized procedures can be compacted by replacing single instantiated
procedures. Users can choose to replace all or bottom level only of such
procedures.

@author Mingyung Ko
@version $Id: CompactProcedureSynthesis.java 606 2008-10-08 16:29:47Z plishker $
*/

public class CompactProcedureSynthesis extends ProcedureSynthesis {

    /** Constructor of a schedule. By default, bottom-level procedures that
     *  are called exactly by single procedures are compacted.
     *
     *  @param schedule The given schedule.
     */
    public CompactProcedureSynthesis(Schedule schedule) {
        super(schedule);
        _makeSingleBottomsInlined();
    }

    ///////////////////////////////////////////////////////////////////
    ////                       public methods                      ////

    /** Display the synthesized procedures in text.
     *  @param nameMap Map from SDF nodes to their <code>String</code> names.
     *  @return The synthesized procedures.
     */
    public String toString(Map nameMap) {
        String display = new String();
        display += procedureCount() + " procedures are synthesized:\n";
        for (int i = 0; i < _procedures.size(); i++) {
            Procedure procedure = getProcedure(i);
            display += _getName(true, procedure, nameMap) + " =";
            int calleeCount = procedure.calleeCount();
            // Display the bottom-level procedures (those w/o callees).
            if (calleeCount == 0) {
                display += " " + _getName(false, procedure, nameMap);
            // Display the intermediate procedures (those /w callees).
            } else {
                for (int j = 0; j < calleeCount; j++) {
                    Procedure callee = procedure.getCallee(j);
                    int iterations   = procedure.getCalleeIterations(j);
                    int calleeLabel  = procedureLabel(callee);
                    display += " (" + iterations + " ";
                    // Display inlined procedures according to their types
                    // (could be SDF node names).
                    display += _singleBottomCollection.contains(callee)
                            ? _getName(false, callee, nameMap)
                            : _getName(true, callee, nameMap);
                    display += ")";
                }
            }
            display += "\n";
        }
        return display;
    }

    ///////////////////////////////////////////////////////////////////
    ////                      protected methods                    ////

    /** Make the procedures collection inlined into their callers.
     *  In other words, these procedures are going to be eliminated for
     *  compaction.
     *
     *  @param procedureCollection The collection of procedures.
     */
    protected void _makeSingleBottomsInlined() {
        _singleBottomCollection = new ArrayList();
        Iterator bottoms = _bottomProcedures.iterator();
        while (bottoms.hasNext()) {
            Procedure bottom = (Procedure)bottoms.next();
            if (bottom.callerCount() == 1)
                _singleBottomCollection.add(bottom);
        }
        Iterator singleBottoms = _singleBottomCollection.iterator();
        while (singleBottoms.hasNext()) {
            Procedure singleBottom = (Procedure)singleBottoms.next();
            _unregister(singleBottom);
        }
    }

    /** Unregister the procedure from the synthesis. Procedures unregistered
     *  will not appear in the synthesis and can not be restored back.
     *
     *  @param procedure The procedure to unregister.
     */
    protected void _unregister(Procedure procedure) {
        // Remove the registered procedure.
        _bottomProcedures.remove(procedure);
        _procedures.remove(procedure);
        // Remove the corresponding schedule entry.
        Object schedule = procedure.getSchedule();
        _schedules.remove(schedule);
        _scheduleToProcedureMap.remove(schedule);
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    // The single called bottom-level procedures.
    private Collection _singleBottomCollection;
}



