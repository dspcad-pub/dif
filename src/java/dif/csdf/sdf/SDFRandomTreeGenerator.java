/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Random graph generator in generating random SDF trees. */

package dif.csdf.sdf;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import dif.util.graph.Edge;
import dif.util.graph.Node;
import dif.DIFGraph;

//////////////////////////////////////////////////////////////////////////
//// SDFRandomTreeGenerator
/**
Random graph generator in generating random SDF trees. SDF trees are useful
in that it is possible for scheduling heuristics, like
{@link dif.csdf.sdf.sched.MinBufferStrategy}, to reach the minimum
buffer cost. Please reference
<p>
M. CUBRIC and P. PANANGADEN. "Minimal memory schedules for dataflow networks".
In E.Best, editor, In Proceedings of the 4th Intl. Conf. on Concurrency Theory
(CONCUR 93, Hildesheim, Germany, Aug. 1993), Lecture Notes in
Computer Science 715, 368-383.

@author Mingyung Ko
@version $Id: SDFRandomTreeGenerator.java 606 2008-10-08 16:29:47Z plishker $
*/

public class SDFRandomTreeGenerator extends SDFRandomGraphGenerator {

    /** Constructor with node count.
     */
    public SDFRandomTreeGenerator() {
        super();
    }

    ///////////////////////////////////////////////////////////////////
    ////                        public methods                     ////

    /** Generate a random acyclic SDF graph with the desired node count.
     *  @param nodeCount The desired node count.
     *  @return The random graph in {@link DIFGraph}.
     */
    public DIFGraph graph(int nodeCount) {
        _reset(nodeCount);
        _makeDAG(2);
        _trimEdges();
        _makeRandomRates();
        return _graph;
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    ///////////////////////////////////////////////////////////////////
    ////                       private methods                    ////

    /*  For nodes with multiple input edges, leave only one of them and
     *  remove the others.
     */
    private void _trimEdges() {
        Iterator nodes = _graph.nodes().iterator();
        while (nodes.hasNext()) {
            Node node = (Node)nodes.next();
            List inEdges = new ArrayList(_graph.inputEdges(node));
            // If more than 1 input edges, remove as many of them to make
            // the graph without directed/undirected cycles.
            if (inEdges.size() > 1) {
                for (int i = 0; i < inEdges.size(); i++) {
                    Edge edge = (Edge)inEdges.get(i);
                    _graph.hideEdge(edge);
                    // If the edge removal caused the graph disconnected,
                    // do NOT execute the removal.
                    if (_graph.connectedComponents().size() > 1)
                        _graph.restoreEdge(edge);
                    // If the removing the edge retains the graph's
                    // connectiveness, execute the removal.
                    else
                        _graph.removeEdge(edge);
                }
            }
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

}


