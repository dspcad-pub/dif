/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Information associated with a Single Rate Graph. */

package dif.csdf.sdf;

//////////////////////////////////////////////////////////////////////////
//// Single Rate Graph
/**
Information associated with a single rate SDF graph.
This class caches frequently-used data associated with single rate SDF graphs.
Single rate graphs are special cases of SDF Graphs in which the SDF production
and consumption rates on its edges are equal. In other words all elements of
the repetition vector are identically equal to one.
These graphs are a minor generalization of HSDF graphs.
This class is intended for use with analysis/synthesis algorithms that operate
on single rate SDF Graphs.
<p>
SingleRateGraph nodes and edges have weights of
type {@link SDFNodeWeight} and {@link SingleRateEdgeWeight}, respectively.

@author Shahrooz Shahparnia
@version $Id: SingleRateGraph.java 606 2008-10-08 16:29:47Z plishker $
@see SingleRateEdgeWeight
*/
public class SingleRateGraph extends SDFGraph {

    /** Construct an empty single rate graph.
     */
    public SingleRateGraph() {
        super();
    }

    /** Construct an empty single rate graph with enough storage allocated
     *  for the specified number of nodes.
     *  @param nodeCount The number of nodes.
     */
    public SingleRateGraph(int nodeCount) {
        super(nodeCount);
    }

    /** Construct an empty single rate graph with enough storage allocated
     *  for the specified number of edges, and number of nodes.
     *  @param nodeCount The integer specifying the number of nodes
     *  @param edgeCount The integer specifying the number of edges
     */
    public SingleRateGraph(int nodeCount, int edgeCount) {
        super(nodeCount, edgeCount);
    }

    /** Verify node weight for single rate graph.
     *  @param weight The node weight to verify.
     *  @return True if the given node weight is valid for a single rate graph.
     */
    public boolean validEdgeWeight(Object weight) {
        return weight instanceof SingleRateEdgeWeight;
    }
}


