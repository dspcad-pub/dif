/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif.csdf.sdf.apeg;

import com.google.common.base.Joiner;
import dif.DIFNodeWeight;

import java.util.ArrayList;
import java.util.List;

/**
 * Information associated with a APEG node.
 * {@link APEGNodeWeight}s are objects associated with {@link dif.util.graph.Node}s
 * that represent APEG nodes in {@link dif.csdf.sdf.apeg.APEGraph}s.
 * This class caches frequently-used data associated with APEG nodes.
 */
public class APEGNodeWeight extends DIFNodeWeight {
    private List<Integer> location;

    /**
     * Createa a APEG node weight with null computation.
     */
    public APEGNodeWeight() {
        setComputation(null);
        location = new ArrayList<>();
    }

    /**
     * Createa a APEG node weight with actor type.
     */
    public APEGNodeWeight(Object computation) {
        setComputation(computation);
        location = new ArrayList<>();
    }

    public List<Integer> getLocation() {
        return location;
    }

    public void setLocation(List<Integer> location) {
        this.location = location;
    }

    /**
     * Return a string representation of the APEG node that is represented
     * by this node weight.
     *
     * @return the string representation.
     */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("computation: ");

        if (getComputation() == null) {
            sb.append("null");
        } else {
            if (getComputation() instanceof String) {
                sb.append(getComputation());
                sb.append(":[").append(Joiner.on(',').join(location)).append("]");
            } else {
                sb.append(getComputation().getClass().getName());
            }
        }
        return sb.toString();
    }
}
