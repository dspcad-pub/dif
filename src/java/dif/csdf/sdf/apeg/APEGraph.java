/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif.csdf.sdf.apeg;

import dif.DIFAttribute;
import dif.csdf.sdf.HSDFGraph;
import dif.hmbe.proto.*;
import dif.util.graph.DirectedGraph;
import dif.util.graph.Edge;
import dif.util.graph.GraphActionException;
import dif.util.graph.Node;
import dif.wsdf.WSDFGraph;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The APEGraph class stores the Acyclic Precedence Expansion Graph (APEG).
 *
 * @author Jiahao Wu
 */
public class APEGraph extends HSDFGraph {
    private Set<Node> appNodeExpanded;
    private WSDFGraph appGraph;
    private Set<String> actorTypes;
    private Map<String, Map<Long, Node>> graphExpansionMap;
    private final static String FRAME_SIZE_NAME = "frameSize";

    /**
     * Construct an empty APEGraph.
     */
    public APEGraph() {
        super();
    }

    /**
     * Construct an empty APEGraph with enough storage allocated for the
     * specified number of nodes.
     *
     * @param nodeCount the number of APEG node
     */
    public APEGraph(int nodeCount) {
        super(nodeCount);
    }

    /**
     * Construct an empty APEGraph with enough storage allocated for the
     * specified number of edges, and number of nodes.
     *
     * @param nodeCount The integer specifying the number of nodes
     * @param edgeCount The integer specifying the number of edges
     */
    public APEGraph(int nodeCount, int edgeCount) {
        super(nodeCount, edgeCount);
    }

    /**
     * Verify edge weight for APEGraph.
     *
     * @param weight The edge weight to verify.
     * @return True if the given edge weight is valid for APEGraph.
     */
    @Override
    public boolean validEdgeWeight(Object weight) {
        return weight instanceof APEGEdgeWeight;
    }

    /**
     * Verify node weight for APEGraph.
     *
     * @param weight The node weight to verify.
     * @return True if the given node weight is valid for APEGraph.
     */
    @Override
    public boolean validNodeWeight(Object weight) {
        return weight instanceof APEGNodeWeight;
    }

    /**
     * @param wsdfGraph high-level representation of the aepplication
     * @return boolean variable represent whether conversion is successful
     */
    public boolean expandFromWSDF(WSDFGraph wsdfGraph) {
        appGraph = wsdfGraph;
        appNodeExpanded = new HashSet<>();
        graphExpansionMap = new HashMap<>();
        actorTypes = new HashSet<>();

        DirectedGraph graph = (DirectedGraph) appGraph;

        List appTopologyOrder;
        try {
            appTopologyOrder = graph.topologicalSort(graph.nodes());
        } catch (GraphActionException e) {
            e.printStackTrace();
            return false;
        }

        // construct type set of application graph actors
        for (Object o : appTopologyOrder) {
            Node node = (Node) o;
            String typeStr = (String) appGraph.getAttribute(node, "type").getValue();
            actorTypes.add(typeStr);
        }

        // autocomplete frame size
        for (Object o : appTopologyOrder) {
            Node node = (Node) o;
            String nodeName = appGraph.getName(node);
            if (appGraph.getAttribute(node, FRAME_SIZE_NAME) == null) {
                System.err.println("Attribute " + FRAME_SIZE_NAME + " for actor " + nodeName + " is missing");
                return false;
            }
            DIFAttribute frameSrc = appGraph.getAttribute(node, FRAME_SIZE_NAME);
            List<Integer> frameSrcList = (List<Integer>) frameSrc.getValue();

            for (Object it : appGraph.outputEdges(node)) {
                Edge edge = (Edge) it;
                Node adj = edge.sink();
                DIFAttribute window = appGraph.getAttribute(edge, "window");
                List<Integer> windowList = (List<Integer>) window.getValue();
                DIFAttribute stride = appGraph.getAttribute(edge, "stride");
                List<Integer> strideList = (List<Integer>) stride.getValue();
                int dimSize = ((List) window.getValue()).size();

                if (appGraph.getAttribute(adj, FRAME_SIZE_NAME) == null) {
                    // setup mode
                    DIFAttribute frameSize = new DIFAttribute(FRAME_SIZE_NAME);
                    frameSize.setType("List");
                    List<Integer> frameSizeList = new ArrayList<>();
                    for (int i = 0; i < dimSize; i++) {
                        frameSizeList.add(frameSrcList.get(i) - windowList.get(i) / strideList.get(i) + 1);
                    }
                    frameSize.setValue(frameSizeList);
                    appGraph.setAttribute(adj, frameSize);
                } else {
                    // verification mode
                    DIFAttribute frameSnk = appGraph.getAttribute(adj, FRAME_SIZE_NAME);
                    List<Integer> frameSnkList = (List<Integer>) frameSnk.getValue();
                    for (int i = 0; i < dimSize; i++) {
                        int expected = frameSrcList.get(i) - windowList.get(i) / strideList.get(i) + 1;
                        int actual = frameSnkList.get(i);
                        if (actual != expected) {
                            System.err.println("WSDF model verification failed");
                            return false;
                        }
                    }
                }

            }
        }

        // start expanding
        for (Object o : appTopologyOrder) {
            Node node = (Node) o;
            // if this is an source actor
            if (appGraph.inputEdgeCount(node) == 0) {
                processAppNode(node);
            }
            // expand adjacent actors
            for (Object it : appGraph.outputEdges(node)) {
                Edge edge = (Edge) it;
                Node adj = edge.sink();

                processAppNode(adj);
                processAppEdge(edge);

            }
        }

        // statistics
        System.out.printf("expansion complete with nodeCount = %d, edgeCount = %d in the APEG model\n",
                this.nodeCount(), this.edgeCount());
        return true;
    }

    private void processAppEdge(Edge edge) {
        Node src = edge.source();
        Node snk = edge.sink();

        System.out.printf("expanding edge (%s, %s)\n", appGraph.getName(src), appGraph.getName(snk));

        Map<Long, Node> srcMap = graphExpansionMap.get(appGraph.getName(src));
        Map<Long, Node> snkMap = graphExpansionMap.get(appGraph.getName(snk));

        DIFAttribute window = appGraph.getAttribute(edge, "window");
        List<Integer> windowList = (List<Integer>) window.getValue();
        DIFAttribute stride = appGraph.getAttribute(edge, "stride");
        List<Integer> strideList = (List<Integer>) stride.getValue();
        DIFAttribute frameSrc = appGraph.getAttribute(src, FRAME_SIZE_NAME);
        List<Integer> frameSrcList = (List<Integer>) frameSrc.getValue();
        List<Integer> dimSize = new ArrayList<>();

        for (int i = 0; i < windowList.size(); i++) {
            int c = windowList.get(i);
            int dc = strideList.get(i);
            int v = frameSrcList.get(i);
            dimSize.add(c * ((v - c) / dc + 1));
        }

        for (int i = 0; i < dimSize.get(0); i++) {
            for (int j = 0; j < dimSize.get(1); j++) {
                int src_i, src_j, snk_i, snk_j;
                src_i = e2src(i, windowList.get(0), strideList.get(0));
                src_j = e2src(j, windowList.get(1), strideList.get(1));

                snk_i = e2snk(i, windowList.get(0));
                snk_j = e2snk(j, windowList.get(1));

                Node apegSrc = srcMap.get(get2DHash(src_i, src_j));
                Node apegSnk = snkMap.get(get2DHash(snk_i, snk_j));

                DIFAttribute distance = new DIFAttribute("distance");
                distance.setValue(0);
                distance.setType("Integer");

                DIFAttribute location = new DIFAttribute("location");
                List<Integer> coordinates = Stream.of(i, j).collect(Collectors.toList());
                location.setValue(coordinates);
                location.setType("List");

                DIFAttribute appEdgeName = new DIFAttribute("appEdgeName");
                appEdgeName.setValue(appGraph.getName(edge));
                appEdgeName.setType("String");

                Edge apegEdge = new Edge(apegSrc, apegSnk);
                apegEdge.setWeight(new APEGEdgeWeight());
                this.addEdge(apegEdge);
                this.setName(apegEdge, "$" + appGraph.getName(edge) + coordinates.toString() + "$");
                this.setAttribute(apegEdge, location);
                this.setAttribute(apegEdge, distance);
                this.setAttribute(apegEdge, appEdgeName);
            }
        }

    }

    private void processAppNode(Node node) {
        String nodeName = appGraph.getName(node);
        if (appNodeExpanded.contains(node)) {
            System.out.println(nodeName + " has been expanded, skip");
            return;
        }
        System.out.println("expanding actor " + nodeName);
        DIFAttribute attribute = appGraph.getAttribute(node, FRAME_SIZE_NAME);
        List<Integer> frameSize = (List<Integer>) attribute.getValue();
        int dimSize = frameSize.size();
        Map<Long, Node> actorExpandMap = new HashMap<>();
        boolean isSequential = false;
        if (appGraph.getAttribute(node, "sequential") != null) {
            DIFAttribute seq = appGraph.getAttribute(node, "sequential");
            if ("Boolean".equals(seq.getType()) && "true".equals((String) seq.getValue())) {
                isSequential = true;
            }
        }

        assert (dimSize == 2);
        Node lastNode = null;
        for (int i = 0; i < frameSize.get(0); i++) {
            for (int j = 0; j < frameSize.get(1); j++) {
                Node v = this.addNode(new Node(new APEGNodeWeight()));
                actorExpandMap.put(get2DHash(i, j), v);
                if (isSequential) {
                    if (lastNode != null) {
                        // add edge for sequential nodes
                        Edge apegEdge = new Edge(lastNode, v);
                        apegEdge.setWeight(new APEGEdgeWeight());
                        this.addEdge(apegEdge);
                        this.setName(apegEdge, "$seq_" + nodeName+ "_"+String.valueOf(i * frameSize.get(0) + j) + "$");
                    }
                    lastNode = v;
                }
                ((APEGNodeWeight) v.getWeight()).getLocation().add(i);
                ((APEGNodeWeight) v.getWeight()).getLocation().add(j);
                ((APEGNodeWeight) v.getWeight()).setComputation(nodeName);

                DIFAttribute location = new DIFAttribute("location");
                location.setValue(Stream.of(i, j).collect(Collectors.toList()));
                location.setType("List");

                DIFAttribute appNode = new DIFAttribute("appNode");
                appNode.setValue(node);
                appNode.setType("Node");

                DIFAttribute appNodeType = new DIFAttribute("appNodeType");
                appNodeType.setValue(appGraph.getAttribute(node, "type").getValue());
                appNodeType.setType("String");

                DIFAttribute distanceToStart = new DIFAttribute("distanceToStart");
                distanceToStart.setValue(-1);
                distanceToStart.setType("Integer");

                DIFAttribute distanceToSink = new DIFAttribute("distanceToSink");
                distanceToSink.setValue(-1);

                this.setName(v, "$" + nodeName + location.getValue().toString() + "$");
                this.setAttribute(v, location);
                this.setAttribute(v, appNode);
                this.setAttribute(v, appNodeType);
                this.setAttribute(v, distanceToSink);
                this.setAttribute(v, distanceToStart);
            }
        }

        graphExpansionMap.put(nodeName, actorExpandMap);
        appNodeExpanded.add(node);
    }

    public PAPEG exportGlobalAPEG(Node startingNode) {
        PAPEG.Builder builder = PAPEG.newBuilder();

        // setup starting node
        builder.setStartNode(nodeLabel(startingNode));

        // setup nodes
        for (Object o : nodes()) {
            Node n = (Node) o;
            int id = nodeLabel(n);
            builder.addNodes(id);

            NodeAttribute attr = getPNodeAttribute(n);
            builder.putNodeAttributeContainer(id, attr);
            builder.putNodeName2Id(attr.getName(), id);
        }
        // cross check the number of node
        assert (builder.getNodeName2IdCount() == builder.getNodesCount());
        System.out.printf("exported %d nodes\n", builder.getNodesCount());

        // setup edges
        for (Object o : edges()) {
            Edge e = (Edge) o;
            int id = edgeLabel(e);
            int src = nodeLabel(e.source());
            int snk = nodeLabel(e.sink());
            PEdge pedge = PEdge.newBuilder()
                    .setId(id).setSrcNode(src)
                    .setSnkNode(snk).build();
            builder.addEdges(pedge);
            EdgeAttribute.Builder attrBuilder = EdgeAttribute.newBuilder();
            if (getAttribute(e, "location") != null) {
                List location = (List) getAttribute(e, "location").getValue();
                attrBuilder.addAllLocation(location);
            }
            String tmpName = getName(e);
            attrBuilder.setName(tmpName.substring(1, tmpName.length() - 1));
            if (getAttribute(e, "appEdgeName") != null) {
                attrBuilder.setAppEdgeName((String) getAttribute(e, "appEdgeName").getValue());
            }
            builder.putEdgeName2Id(attrBuilder.getName(), id);
            builder.putEdgeAttributeContainer(id, attrBuilder.build());
        }
        // cross check the number of edges
        assert (builder.getEdgeName2IdCount() == builder.getEdgesCount());
        System.out.printf("exported %d edges\n", builder.getEdgesCount());

        // setup meta data
        Meta.Builder metaBuilder = Meta.newBuilder();
        metaBuilder.addAllActortype(actorTypes);
        for (String it : actorTypes) {
            int cnt = 0;
            for (Object o : nodes()) {
                Node n = (Node) o;
                String at = (String) getAttribute(n, "appNodeType").getValue();
                if (at.equals(it)) {
                    cnt++;
                }
            }
            System.out.printf("number of APEG actor of type %s = %d\n", it, cnt);
            metaBuilder.putActorType2Cnt(it, cnt);
        }
        builder.setMetadata(metaBuilder.build());

        // build and return immutable
        return builder.build();
    }

    public PAPEG exportBookkeeperPartialAPEG(String producer) {
        PAPEG.Builder partialBuilder = PAPEG.newBuilder();
        // find the producer and consumer of the bookkeeper

        Set<String> consumers = new HashSet<>();
        Node producerNode = null;
        for (Object o : appGraph.nodes()) {
            Node n = (Node) o;
            if (appGraph.getName(n).equals(producer)) {
                producerNode = n;
                break;
            }
        }
        if (producerNode == null) {
            System.err.println("Bookkeeper producer not found for actor = " + producer);
            System.exit(-1);
        }

        for (Object o : appGraph.successors(producerNode)) {
            Node n = (Node) o;
            consumers.add(appGraph.getName(n));
        }


        // setup nodes
        for (Object o : nodes()) {
            Node n = (Node) o;
            int id = nodeLabel(n);
            String rawName = getName(n);
            String vertexName = rawName.substring(1, rawName.length() - 1);
            if (vertexName.startsWith(producer) || isInConsumer(consumers, vertexName)) {
                partialBuilder.addNodes(id);

                NodeAttribute attr = getPNodeAttribute(n);
                partialBuilder.putNodeAttributeContainer(id, attr);
                partialBuilder.putNodeName2Id(attr.getName(), id);
            }
        }

        // cross check the number of node
        assert (partialBuilder.getNodeName2IdCount() == partialBuilder.getNodesCount());

        Set<Integer> nodeSet = new HashSet<>(partialBuilder.getNodesList());
        // setup edges
        for (Object o : edges()) {
            Edge e = (Edge) o;
            int id = edgeLabel(e);
            int src = nodeLabel(e.source());
            int snk = nodeLabel(e.sink());

            if (nodeSet.contains(src) && nodeSet.contains(snk)) {
                PEdge pedge = PEdge.newBuilder()
                        .setId(id).setSrcNode(src)
                        .setSnkNode(snk).build();
                partialBuilder.addEdges(pedge);
                EdgeAttribute.Builder attrBuilder = EdgeAttribute.newBuilder();
                List location = (List) getAttribute(e, "location").getValue();
                attrBuilder.addAllLocation(location);
                String tmpName = getName(e);
                attrBuilder.setName(tmpName.substring(1, tmpName.length() - 1));
                attrBuilder.setAppEdgeName((String) getAttribute(e, "appEdgeName").getValue());
                partialBuilder.putEdgeName2Id(attrBuilder.getName(), id);
                partialBuilder.putEdgeAttributeContainer(id, attrBuilder.build());
            }
        }

        // cross check the number of edges
        assert (partialBuilder.getEdgeName2IdCount() == partialBuilder.getEdgesCount());

        // no meta data for bookkeeper APEG

        return partialBuilder.build();
    }

    private boolean isInConsumer(Set<String> consumers, String vertexName) {
        for (String con : consumers) {
            if (vertexName.startsWith(con)) {
                return true;
            }
        }
        return false;
    }

    public PAPEG exportSourcePartialAPEG(String ActorNames, Node startingNode) {
        PAPEG.Builder partialBuilder = PAPEG.newBuilder();

        // setup starting node
        partialBuilder.setStartNode(nodeLabel(startingNode));

        // setup nodes
        for (Object o : nodes()) {
            Node n = (Node) o;
            int id = nodeLabel(n);
            String rawName = getName(n);
            String vertexName = rawName.substring(1, rawName.length() - 1);
            if (vertexName.startsWith(ActorNames)) {
                partialBuilder.addNodes(id);
                if (predecessors(n).size() > 0) {
                    System.err.println(startingNode + " is not source actor");
                }

                NodeAttribute attr = getPNodeAttribute(n);
                partialBuilder.putNodeAttributeContainer(id, attr);
                partialBuilder.putNodeName2Id(attr.getName(), id);
            }
        }
        // no edge for source APEG

        // no meta data for source APEG

        return partialBuilder.build();
    }

    private NodeAttribute getPNodeAttribute(Node n) {
        NodeAttribute.Builder attrBuilder = NodeAttribute.newBuilder();
        String tmpName = getName(n);
        attrBuilder.setName(tmpName.substring(1, tmpName.length() - 1));
        attrBuilder.setAppNode(appGraph.getName((Node) getAttribute(n, "appNode").getValue()));
        attrBuilder.setAppNodeType((String) getAttribute(n, "appNodeType").getValue());
        attrBuilder.setDistanceToSink((int) getAttribute(n, "distanceToSink").getValue());
        attrBuilder.setDistanceToStart((int) getAttribute(n, "distanceToStart").getValue());
        List location = (List) getAttribute(n, "location").getValue();
        attrBuilder.addAllLocation(location);
        attrBuilder.setIndegree(inputEdgeCount(n));
        attrBuilder.setOutdegree(outputEdgeCount(n));
        attrBuilder.addAllPredecessors(((Collection<Node>) predecessors(n))
                .stream()
                .map(this::nodeLabel)
                .collect(Collectors.toList()));
        attrBuilder.addAllSucessors(((Collection<Node>) successors(n))
                .stream()
                .map(this::nodeLabel)
                .collect(Collectors.toList()));
        return attrBuilder.build();
    }

    private long get2DHash(int a, int b) {
        return a >= b ? a * a + a + b : a + b * b;
    }

    private int e2src(int i, int c, int dc) {
        return i / c * dc + i % c;
    }

    private int e2snk(int i, int c) {
        return i / c;
    }
}
