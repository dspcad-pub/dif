/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* The class for SDF vectorization. */

package dif.csdf.sdf.vect;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import dif.csdf.sdf.SDFEdgeWeight;
import dif.csdf.sdf.SDFGraph;
import dif.csdf.sdf.mem.BufferSharing;
import dif.csdf.sdf.mem.BufferUtilities;
import mocgraph.Edge;
import mocgraph.Node;
import mocgraph.mapping.ToIntMapMapping;
import mocgraph.sched.Schedule;

//////////////////////////////////////////////////////////////////////////
//// Vectorization
/** The class for SDF vectorization.
 *  The vectorization starts with a Single Appearance Schedule (SAS).
 *  Specifically, the vectorization is done through moving loop counts of
 *  the SAS.
 *
 *  @author Mingyung Ko
 *  @version $Id: Vectorization.java 606 2008-10-08 16:29:47Z plishker $
 */
public class Vectorization {

    /** A constructor with an SDF graph and a valid R-schedule.
     *  @param graph The SDF graph to vectorize.
     *  @param schedule The schedule.
     */
    public Vectorization(SDFGraph graph, Schedule schedule) {
        _SDFGraph = graph;
        _blockingFactor = schedule.getIterationCount();
        _tree = new ScheduleTree(schedule);
        _makeRestoreImages();
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Get the total number of activations for the tree. This is also the
     *  activations for the root. See also
     *  {@link #activations(ScheduleTreeNode)}.
     *
     *  @return The total number of activations.
     */
    public int activations() {
        return activations(_tree.root());
    }

    /** Get the number of activations for the schedule tree node.
     *  The number of activations is a way to measure DMC's performance.
     *  It equals to 1 for leaves. For non-leaves, it is the product of the
     *  loop count and sum of the left, right children's activations
     *
     *  @param node The node.
     *  @return The number of activations.
     */
    public int activations(ScheduleTreeNode node) {
        int activations = 1;
        if (!node.isLeaf())
            activations = node.loopCount * (
                    activations(node.leftChild)
                    + activations(node.rightChild));
        return activations;
    }

    /** Compute buffer cost for the SDF graph associated in constructors.
     *  @return The total buffer cost.
     */
    public int bufferCost() {
        return bufferCost(_SDFGraph.edges());
    }

    /** Compute buffer cost for the SDF subgraph induced from the tree root.
     *  @param node The root inducing the SDF subgraph.
     *  @return The buffer cost.
     */
    public int bufferCost(ScheduleTreeNode node) {
        return bufferCost(_tree.subtree(node).SDFEdges(_SDFGraph));
    }

    /** Compute buffer cost for the given SDF edge collection.
     *  @param SDFEdgeCollection The collection of SDF edges.
     *  @return The total buffer cost.
     */
    public int bufferCost(Collection SDFEdgeCollection) {
        int cost = 0;
        Iterator edges = SDFEdgeCollection.iterator();
        while (edges.hasNext())
            cost += bufferCost((Edge)edges.next());
        return cost;
    }

    /** Compute buffer cost for the given SDF edge.
     *  @param SDFEdge The SDF edge.
     *  @return The buffer cost.
     */
    public int bufferCost(Edge SDFEdge) {
        Node SDFSource = SDFEdge.source();
        Node SDFSink   = SDFEdge.sink();
        ScheduleTreeNode treeNodeU = _tree.leafOf(SDFSource);
        ScheduleTreeNode treeNodeV = _tree.leafOf(SDFSink);
        ScheduleTreeNode pathDestination =
                _tree.leastParent(treeNodeU, treeNodeV).leftChild;
        Iterator nodes = _tree.path(treeNodeU, pathDestination).iterator();
        int cost = ((SDFEdgeWeight)SDFEdge.getWeight()).getSDFProductionRate();
        while (nodes.hasNext()) {
            ScheduleTreeNode node = (ScheduleTreeNode)nodes.next();
            cost *= node.loopCount;
        }
        return cost;
    }

    /** Returns the vectorization results in {@link VectorizationResults}.
     *  Implementation of this class saves activations, buffer cost, and
     *  the vectorized schedule.
     *
     *  @return The results.
     */
    public VectorizationResults getVectorizationResults() {
        VectorizationResults results = new VectorizationResults();
        results.activations = activations();
        results.activationRate =
                (double)results.activations / (double)_blockingFactor;
        results.bufferCost = bufferCost();
        results.schedule = _tree.toSchedule();
        return results;
    }

    /** Return the schedule tree. The tree is useful on verifying the
     *  vectorization results or for further analysis.
     *
     *  @return The schedule tree.
     */
    public ScheduleTree scheduleTree() {
        return _tree;
    }

    /** Restores back all the information as if none DMF is ever
     *  performed. In this class, original loop counts are restored back.
     *  Sub-classes can determine more information to restore.
     */
    public void restore() {
        // Restores loop counts.
        Iterator nodes = _tree.nodes().iterator();
        while (nodes.hasNext()) {
            ScheduleTreeNode node = (ScheduleTreeNode)nodes.next();
            node.loopCount = ((Integer)_loopCountImage.get(node)).intValue();
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /** Keeps images of the original loop counts.
     *  This method must be called in constructors.
     */
    protected void _makeRestoreImages() {
        // Make an image of original loop counts.
        _loopCountImage = new HashMap();
        Iterator nodes = _tree.nodes().iterator();
        while (nodes.hasNext()) {
            ScheduleTreeNode node = (ScheduleTreeNode)nodes.next();
            _loopCountImage.put(node, new Integer(node.loopCount));
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////                      protected variables                  ////

    /** The SDF graph to vectorize. */
    protected SDFGraph _SDFGraph;

    /** The schedule tree to compute vectorization. */
    protected ScheduleTree _tree;

    /** Blocking factor of the associated schedule. */
    protected int _blockingFactor;

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    /* A map as an image from schedule tree nodes to original loop counts. */
    private Map _loopCountImage;
}


