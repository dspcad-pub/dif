/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Utilities for working with SDF graphs. */

package dif.csdf.sdf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import dif.util.graph.Edge;
import dif.util.graph.Graph;
import dif.util.graph.Node;
import dif.data.ExtendedMath;

//////////////////////////////////////////////////////////////////////////
//// SDFGraphs
/**
Utilities for working with SDF graphs.

@author Ming-Yung Ko
@version $Id: SDFGraphs.java 606 2008-10-08 16:29:47Z plishker $
@see Graph
@see SDFGraph
*/
public class SDFGraphs {

    // Private constructor to prevent instantiation of the class
    private SDFGraphs() {
    }

    /** Given a collection of nodes in a SDF graph, replace the subgraph
     *  induced by the nodes with a single node N. Besides topological
     *  clustering, this method also adjusts the token exchanging rates
     *  on affected edges and smartly computes repetitions of the newly
     *  created super node.
     *
     *  @param graph The SDF graph.
     *  @param nodeCollection The collection of nodes.
     *  @param superNode The SDF node that replaces the subgraph.
     *  @return The subgraph that is replaced.
     */
    public static SDFGraph clusterNodes(SDFGraph graph,
            Collection nodeCollection, Node superNode) {

        List result = clusterNodesComplete(graph, nodeCollection, superNode);
        return (SDFGraph)result.get(0);
    }

    /** Given a collection of nodes in a SDF graph, replace the subgraph
     *  induced by the nodes with a single node N. This method returns
     *  both the subgraph and a map from newly created edges to old edges
     *  replaced. The map is important for cluster status tracking.
     *
     *  @param graph The SDF graph.
     *  @param nodeCollection The collection of nodes.
     *  @param superNode The SDF node that replaces the subgraph.
     *  @return A list with the first element the subgraph and the second
     *          element a map of edges.
     */
    public static List clusterNodesComplete(
            SDFGraph graph, Collection nodeCollection, Node superNode) {

        // Compute the repetitions for super node. Instead of calling
        // SDFGraph.computeRepetitions(), this method just computes the
        // GCD(Greatest Common Divisor) from the repetitions vector of
        // the given node collection.
        Iterator nodes = nodeCollection.iterator();
        int superRepetitions = graph.getRepetitions((Node)nodes.next());
        while (nodes.hasNext()) {
            int nextRepetitions = graph.getRepetitions((Node)nodes.next());
            superRepetitions =
                    ExtendedMath.gcd(nextRepetitions, superRepetitions);
        }

        // Collect the crossing edges between the subgraph and the
        // remaining part of the graph.
        SDFGraph subGraph = (SDFGraph)graph.subgraph(nodeCollection);
        ArrayList oldEdges = new ArrayList();
        nodes = nodeCollection.iterator();
        while (nodes.hasNext()) {
            Node node = (Node)nodes.next();
            Iterator edges = graph.incidentEdges(node).iterator();
            while (edges.hasNext()) {
                Edge edge = (Edge)edges.next();
                if (!subGraph.containsEdge(edge))
                    oldEdges.add(edge);
            }
        }

        graph.addNode(superNode);
        ArrayList newEdges = new ArrayList();
        // Adjust token exchanging rates on all affected edges. Affected
        // edges are those edges linking the new super node and the
        // original nodes.
        Iterator edges = oldEdges.iterator();
        while (edges.hasNext()) {
            Edge edge = (Edge)edges.next();

            SDFEdgeWeight oldWeight = (SDFEdgeWeight)edge.getWeight();
            int consumption = oldWeight.getSDFConsumptionRate();
            int production = oldWeight.getSDFProductionRate();
            int delay = oldWeight.getIntDelay();
            int newRate;
            Node source = edge.source();
            Node sink = edge.sink();

            SDFEdgeWeight newWeight =
                    new SDFEdgeWeight(production, consumption, delay);

            // Compute production rate if the super node is a source.
            if (nodeCollection.contains(source)) {
                newEdges.add(new Edge(superNode, sink, newWeight));
                newRate = (graph.getRepetitions(sink) * consumption)
                        / superRepetitions;
                newWeight.setSDFProductionRate(newRate);

            // Compute consumption rate if the super node is a sink.
            } else {
                newEdges.add(new Edge(source, superNode, newWeight));
                newRate = (graph.getRepetitions(source) * production)
                        / superRepetitions;
                newWeight.setSDFConsumptionRate(newRate);
            }
        }

        // add new edges, remove old edges, and put the correspondence
        // of old and new edges in the edge map.
        Map edgeMap = new HashMap();
        for (int i = 0; i < oldEdges.size(); i++) {
            Edge oldEdge = (Edge)oldEdges.get(i);
            Edge newEdge = (Edge)newEdges.get(i);
            graph.addEdge(newEdge);
            graph.removeEdge(oldEdge);
            edgeMap.put(newEdge, oldEdge);
        }

        // remove old nodes
        nodes = nodeCollection.iterator();
        while (nodes.hasNext())
            graph.removeNode((Node)nodes.next());

        // set new repetition counts
        graph.setRepetitions(superNode, superRepetitions);

        List result = new ArrayList();
        result.add(subGraph);
        result.add(edgeMap);
        return result;
    }



}


