/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Random graph generator in generating random SDF graphs. */

package dif.csdf.sdf;

import java.util.Iterator;
import dif.util.graph.Edge;
import dif.util.graph.Node;
import dif.data.ExtendedMath;
import dif.data.Fraction;
import dif.DIFGraph;
import dif.DIFRandomGraphGenerator;

//////////////////////////////////////////////////////////////////////////
//// SDFRandomGraphGenerator
/**
Random graph generator in generating random SDF graphs. This class generates
SDF graphs of random topology and production/consumption rates.

@author Mingyung Ko
@version $Id: SDFRandomGraphGenerator.java 606 2008-10-08 16:29:47Z plishker $
*/

public class SDFRandomGraphGenerator extends DIFRandomGraphGenerator {

    /** Constructor with node count.
     */
    public SDFRandomGraphGenerator() {
        super();
        _initialize();
    }

    ///////////////////////////////////////////////////////////////////
    ////                        public methods                     ////

    /** Get repetitions of the node. The method has the same function as
     *  {@link dif.csdf.CSDFGraph#getRepetitions(Node)}. Because
     *  repetitions are also computed in the random graph generation process,
     *  they can be obtained without re-computation.
     *
     *  @param node The node to get repetitions.
     *  @return The repetitions.
     */
    public int getRepetitions(Node node) {
        return _repetitions[_graph.nodeLabel(node)];
    }

    /** Generate a random acyclic SDF graph with the desired node count.
     *  @param nodeCount The desired node count.
     *  @return The random graph in {@link DIFGraph}.
     */
    public DIFGraph graph(int nodeCount) {
        super.graph(nodeCount);
        _makeRandomRates();
        return _graph;
    }

    /** Set the maximal production/consumption rate.
     *  @param maxRate The maximal rate.
     */
    public void setMaxRate(int maxRate) {
        _maxRate = maxRate;
    }

    /** Set the probability of rate 1 for production/consumption.
     *  Parameter <code>probability</code> should range from 0 to 1000.
     *  @param oneProbability The probability of rate 1.
     */
    public void setOneProbability(int oneProbability) {
        _oneProbability = oneProbability;
        if (oneProbability < 0 || oneProbability > 1000)
            throw new RuntimeException(
                    "Probability should range from 0 to 1000.");
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /** Initialize the generator.
     */
    protected void _initialize() {
        _initialize(new SDFGraph(), new SDFNodeWeight(), new SDFEdgeWeight());
        _maxRate = 1;
        _oneProbability = 1000;
    }

    /** Generate random rates for a given graph topology.
     */
    protected void _makeRandomRates() {
        // Generate adjacency matrix. Generation of adjacency matrix is
        // the main function to compute random rates.
        _makeAdjacency();
        // Get rates from adjacency matrix.
        SDFGraph graph = (SDFGraph)_graph;
        Iterator edges = graph.edges().iterator();
        while (edges.hasNext()) {
            Edge edge = (Edge)edges.next();
            int sourceLabel = graph.nodeLabel(edge.source());
            int sinkLabel   = graph.nodeLabel(edge.sink());
            SDFEdgeWeight edgeWeight = (SDFEdgeWeight)edge.getWeight();
            edgeWeight.setSDFProductionRate(
                    _adjacency[sourceLabel][sinkLabel]);
            edgeWeight.setSDFConsumptionRate(
                    -(_adjacency[sinkLabel][sourceLabel]));
        }
    }

    /** Reset all configurations to generate a new random SDF graph.
     *  @param nodeCount The desired node count.
     */
    protected void _reset(int nodeCount) {
        super._reset(nodeCount);
        _numerators   = new int[nodeCount];
        _repetitions  = new int[nodeCount];
        _denominators = new int[nodeCount];
        for (int i = 0; i < nodeCount; i++)
            _denominators[i] = 1;
        _adjacency    = new int[nodeCount][nodeCount];
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private methods                    ////

    /*  Generate adjacency matrix from an incidence matrix. Adjacency
     *  matrix contains edges and token production/consumption rates.
     */
    private void _makeAdjacency() {
        int nodeCount = _graph.nodeCount();
        // First, "copy" incidence to adjacency.
        for (int i = 0; i < nodeCount; i++)
            for (int j = 0; j < nodeCount; j++)
                if (_incidence[i][j])
                    _adjacency[i][j] = 1;

        int lcmDenominators = 1;
        for (int i = 0; i < nodeCount; i++)
            for (int j = 0; j < nodeCount; j++)
                if (_adjacency[i][j] > 0) {

                    if ((_numerators[i] == 0) || (_numerators[j] == 0)) {
                        _adjacency[i][j] = _randomRate();
                        _adjacency[j][i] = -(_randomRate());
                    }

                    if ((_numerators[i] == 0) && (_numerators[j] == 0)) {
                        _numerators[i] = 1;
                        _numerators[j] = _numerators[i] * _adjacency[i][j];
                        _denominators[j] = -(_adjacency[j][i]);

                    } else if ((_numerators[i] == 0) && (_numerators[j] != 0)) {
                        _numerators[i] = -(_numerators[j] * _adjacency[j][i]);
                        _denominators[i] = _denominators[j] * _adjacency[i][j];

                    } else if ((_numerators[i] != 0) && (_numerators[j] == 0)) {
                        _numerators[j] = _numerators[i] * _adjacency[i][j];
                        _denominators[j] =
                                -(_denominators[i] * _adjacency[j][i]);

                    } else {
                        int lcm, gcd, ri, rj;
                        lcm = Fraction.lcm(_denominators[i], _denominators[j]);
                        ri  = _numerators[i] * lcm / _denominators[i];
                        rj  = _numerators[j] * lcm / _denominators[j];
                        gcd = ExtendedMath.gcd(ri, rj);
                        _adjacency[i][j] = rj / gcd;
                        _adjacency[j][i] = -(ri / gcd);
                    }

                    lcmDenominators = Fraction.lcm(lcmDenominators,
                            Fraction.lcm(_denominators[i], _denominators[j]));
                }

        // Compute repetitions vector.
        for (int i = 0; i < nodeCount; i++)
            _repetitions[i] =
                    _numerators[i] * lcmDenominators / _denominators[i];
        int gcdRepetitions = _repetitions[0];
        for (int i = 1; i < nodeCount; i++)
            gcdRepetitions = ExtendedMath.gcd(gcdRepetitions, _repetitions[i]);
        for (int i = 0; i < nodeCount; i++)
            _repetitions[i] /= gcdRepetitions;
    }

    /*  Generate a random production/consumption rate.
     *  @return The rate.
     */
    private int _randomRate() {
        int rate = 1;
        if (_randomNonNegativeInt(1001) >= _oneProbability)
            rate = 2 + _randomNonNegativeInt(_maxRate - 1);
        return rate;
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    // The max production/consumption rate.
    private int _maxRate;

    // The probability of rate 1. The value should range from 0 through 1000.
    private int _oneProbability;

    // The arrays for computing repetitions.
    private int[] _numerators;
    private int[] _denominators;
    private int[] _repetitions;

    // Adjacency matrix.
    private int[][] _adjacency;
}


