/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A weight for an edge of an HDSF Graph */

package dif.csdf.sdf;

//////////////////////////////////////////////////////////////////////////
//// HSDFEdgeWeight

import dif.util.graph.Edge;

/**
Information associated with an HSDF graph edge.
HSDFEdgeWeights are objects associated with {@link Edge}s
that represent HSDF edges in {@link HSDFGraph}s.
HSDF Graphs are special cases of Single Rate Graphs in which the production and
consumption rates on its edges are equal to one.
This class is intended for use with analysis/synthesis algorithms that
operate on HSDF Graphs.

@author Shahrooz Shahparnia
@version $Id: HSDFEdgeWeight.java 606 2008-10-08 16:29:47Z plishker $
@see Edge
@see SDFEdgeWeight
@see SingleRateEdgeWeight
*/
public class HSDFEdgeWeight extends SingleRateEdgeWeight{

    /** Construct an edge weight for a homogeneous, zero-delay edge.
     */
    public HSDFEdgeWeight() {
        super();
    }

    /** Construct an edge weight for a given token production/consumption
     *  rate, and delay.
     *  @param delay The delay associated to this Edge.
     */
    public HSDFEdgeWeight(int delay) {
        super(1, delay);
    }

    /** Set the token production/consumption rate of the associated Single
     *  Rate Edge.
     *  This method and setTokenProductionRate are the same and they both set
     *  the production and consumption rates to "1" regardless of the
     *  tokenRate.
     *  This method is to take the ability to set the token
     *  consumption/production rate from the user to something else than "1"
     *  @param tokenRate The token consumption/production rate of this Edge.
     */
    public void setHSDFConsumptionRate(int tokenRate) {
        setSingleConsumptionRate(1);
        setSingleProductionRate(1);
    }

    /** Set the token production/consumption rate of the associated Single
     *  Rate Edge.
     *  This method and setTokenConsumptionRate are the same and they both set
     *  the production and consumption rates to "1" regardless of the
     *  tokenRate.
     *  This method is to take the ability to set the token
     *  consumption/production rate from the user to something else than "1"
     *  @param tokenRate The token consumption/production rate of this Edge.
     */
    public void setHSDFProductionRate(int tokenRate) {
        setSingleConsumptionRate(1);
        setSingleProductionRate(1);
    }
}


