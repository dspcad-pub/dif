/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* The class for data partitioning in dual homogeneous memory bank
 architectures.
*/

package dif.csdf.sdf.mem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import dif.graph.Graphs;

import dif.util.graph.Edge;
import dif.util.graph.Graph;
import dif.util.graph.Node;
import dif.util.graph.mapping.ToDoubleMapMapping;

//////////////////////////////////////////////////////////////////////////
//// DataPartitioning
/** The class for data partitioning in dual homogeneous memory bank
architectures. It is developed for Mingyung's research. Basically, an
conflict graph should be provided. Generally, nodes representing data
variables while edges' meaning depends on applications.

@author Mingyung Ko
@version $Id: DataPartitioning.java 606 2008-10-08 16:29:47Z plishker $
*/

public class DataPartitioning {

    /** A constructor with an conflict graph.
     *  @param conflictGraph The conflict graph to partition.
     */
    public DataPartitioning(ConflictGraph conflictGraph) {
        _conflictGraph = conflictGraph;
        // Set two partitions as default.
        initPartitions(2);
        // Set the minimal partition size to -1, an invalid value. The
        // correct size calculation comes with minimalPartitionSize().
        _minPartSize = -1;
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** A two coloring algorithm. Two coloring is of polynomial complexity.
     *  @return The result as a {@link ConflictGraph}.
     */
    public ConflictGraph twoColoringStrategy() {
        Iterator components =
                _conflictGraph.connectedComponents().iterator();
        while (components.hasNext()) {
            Collection component = (Collection)components.next();
            Node seedNode = (Node)component.iterator().next();
            Graph graph = _conflictGraph.subgraph(component);
            // Two coloring for connected subgraphs.
            _twoColoringFor(seedNode, graph, 0);
        }
        return _conflictGraph;
    }

    /** The SPF heuristic for two memory banks.
     *  See also {@link #SPFStrategy(int)}.
     *
     *  @return The result as a {@link ConflictGraph}.
     */
    public ConflictGraph SPFStrategy() {
        return SPFStrategy(2);
    }

    /** The "Smallest Partition First (SPF)" strategy in solving data
     *  partitioning problems. It is basically a greedy approach.
     *  The algorithm is based on the fact of sparse connection of
     *  conflict graphs.
     *
     *  @param partitionCounts The number of partitions available.
     *  @return The result as a {@link ConflictGraph}.
     */
    public ConflictGraph SPFStrategy(int partitionCounts) {
        if (partitionCounts > 2)
            initPartitions(partitionCounts);
        // Sort connected components by decreasing size.
        Iterator downComponents = _conflictGraph.descendentListOf(
                _conflictGraph.connectedComponents()).iterator();
        while (downComponents.hasNext()) {
            Collection component = (Collection)downComponents.next();
            Node seed = (Node)
                    _conflictGraph.descendentListOf(component).get(0);
            _alternateAssignment(seed);
        }
        return _conflictGraph;
    }

    /** An extended SPF strategy considering sharing conflicts. It assumes
     *  two memory banks only.
     *
     *  @return The result as a {@link ConflictGraph}.
     *  @throw DataPartitioningException If the conflict graph does not have
     *         the proper topology for data partitioning.
     */
    public ConflictGraph sharedSPFStrategy()
            throws DataPartitioningException {

        Iterator downComponents = _conflictGraph.descendentListOf(
                _conflictGraph.connectedComponents()).iterator();
        while (downComponents.hasNext()) {
            Collection component = (Collection)downComponents.next();
            Node seed = (Node)
                    _conflictGraph.descendentListOf(component).get(0);
            try {
                _prioritizedAlternateAssignment(seed);
            } catch (DataPartitioningException e) {
                throw e;
            }
        }
        return _conflictGraph;
    }

    /** Partitioning strategy with consolidated shared buffers. Before
     *  actual partitioning, the strategy first consolidates shared buffers
     *  into single nodes and leads to a simpler conflict graph topology.
     *  After the consolidation, {@link #SPFStrategy()} is invoked for
     *  actual partitioning.
     *
     *  @return The result as a {@link ConflictGraph}.
     *  @throw DataPartitioningException If the conflict graph does not have
     *         the proper topology for data partitioning.
     */
    public ConflictGraph consolidateSharedBuffersStrategy()
            throws DataPartitioningException {

        // Create a new conflict graph with shared buffers consolidated.
        Graph compatibleGraph = _bufferCompatibleGraph();
        _setupBufferConsolidation(
                Graphs.cliqueComponents(compatibleGraph));
        ConflictGraph newConflictGraph = _toConsolidatedConflictGraph();
        // Partition the buffer-consolidated conflict graph by the shared
        // SPF strategy.
        try {
            (new DataPartitioning(newConflictGraph)).sharedSPFStrategy();
            _minPartSize = (int)
                    newConflictGraph.valueOf(_minPartition(newConflictGraph));
            // Translate the partitions back to the original conflict graph.
            _translatePartitions(newConflictGraph);

        } catch (DataPartitioningException e) {
            throw e;
        }
        return _conflictGraph;
    }

    /** Initialize partitions by the given counts.
     *  @param counts Number of partitions to initialize.
     */
    public void initPartitions(int counts) {
        if (_conflictGraph.getPartitions().size() > 0)
            _conflictGraph.resetPartitions();
        for (int i = 0; i < counts; i++)
            _conflictGraph.addPartition(new GraphPartition());
    }

    /** Return the minimal partition size. The size is not necessarily a sum
     *  of node sizes. Users should refer to the particular size calculation
     *  of a partitioning algorithm. However, by default, this method simply
     *  returns the sum of all node sizes.
     *
     *  @return The minimal partition size in <code>int</code>.
     */
    public int minimalPartitionSize() {
        if (_minPartSize < 0)
            _minPartSize = (int)
                    _conflictGraph.valueOf(_minPartition(_conflictGraph));
        return _minPartSize;
    }

    /** Transform to OPBDP file format. OPBDP is an integer programming
     *  solver available on the site below.
     *  http://www.mpi-sb.mpg.de/units/ag2/software/opbdp/
     *  @return The format OPBDP accepts.
     */
    public String toOPBDPString() {
        String out = new String();
        String objective = new String();

        // compute total node values/weights.
        int total = 0;
        Iterator nodes = _conflictGraph.nodes().iterator();
        while (nodes.hasNext())
            total += _conflictGraph.valueOf(nodes.next());

        // optimization objective function (column variables)
        out += "min:";
        for (int i = 0; i < _conflictGraph.nodeCount(); i++) {
            Node node = _conflictGraph.node(i);
            int doubleValue = 2 * (int)_conflictGraph.valueOf(node);
            objective += " + " + doubleValue  + " C" + i;
        }
        objective += " - " + total;
        out += objective + ";\n\n";

        // constraint functions (row variables)
        int conflictCount = _conflictGraph.edgeCount();
        for (int i = 0; i < conflictCount; i++) {
            Edge edge = _conflictGraph.edge(i);
            Node node1 = edge.source();
            Node node2 = edge.sink();
            int node1Label = _conflictGraph.nodeLabel(node1);
            int node2Label = _conflictGraph.nodeLabel(node2);
            out += "R" + i + " : " +
                    "+ C" + node1Label + " + C" + node2Label + " = 1;\n";
        }
        out += "R" + conflictCount + " :" + objective + " >= 0;\n";
        return out;
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    ///////////////////////////////////////////////////////////////////
    //////// Sub-routines for SPFStrategy()

    /** Alternate partition assignment for a given node as seed.
     *  The assignment is for connected components (including one-node
     *  trivial singletons) only. This method is called both in SPF and
     *  max weighted edge cut approaches.
     *
     *  @param seed The seed node.
     */
    protected void _alternateAssignment(Node seed) {
        boolean assigned = false;
        List upPartitionList = _conflictGraph.ascendentPartitions();
        Iterator upPartitions = upPartitionList.iterator();
        while (upPartitions.hasNext()) {
            GraphPartition partition = (GraphPartition)upPartitions.next();
            if (!_linksExistFor(partition, seed)) {
                partition.addNode(seed);
                assigned = true;
                // The seed node is assigned properly. Quit the loop.
                break;
            }
        }
        // Certain cycles exist such that the connected component is not
        // bipartite. Some pair of variables can not be accessed in parallel.
        // They are assigned to the same bank and are accessed sequentially
        // one after the other.
        if (assigned == false) {
            // Assign the variable just to the bank with least usage so far.
            ((GraphPartition)upPartitionList.get(0)).addNode(seed);
        }
        _processNeighborsOf(seed);
    }

    /** Bank assignment for neighbor nodes of a seed. This is to facilitate
     *  {@link #_alternateAssignment(Node)}.
     *  @param node The node whose neighbors are to be processed.
     */
    protected void _processNeighborsOf(Node node) {
        Iterator neighbors = _conflictGraph.neighbors(node).iterator();
        // The recursion does a depth-first (not breadth-first)
        // alternate assignment.
        while (neighbors.hasNext()) {
            Node neighbor = (Node)neighbors.next();
            if (_conflictGraph.partitionOf(neighbor) == null)
                _alternateAssignment(neighbor);
        }
    }

    ///////////////////////////////////////////////////////////////////
    //////// Sub-routines for sharedSPFStrategy()

    /** An SPF-like alternate assignment which favors partitioning conflicts.
     *  While partitioning neighbors (of the node) are still alternately
     *  assigned, sharing conflicts are done so only the condition permits.
     *  The results guarantee maximum edge cut for partitioning conflicts
     *  just like SPF. Maximum edge cut is also tried for sharing conflicts
     *  only if the cut for partitioning conflicts is kept.
     *
     *  @param node The node to start the alternate assignment.
     *  @throw DataPartitioningException If the conflict graph does not posses
     *         proper topology for data partitioning.
     */
    protected void _prioritizedAlternateAssignment(Node node)
            throws DataPartitioningException {

        // Skip nodes with partitions already assigned.
        if (_conflictGraph.partitionOf(node) != null)
            return;

        Collection neighbors   = _conflictGraph.neighbors(node);
        Collection pNeighbors  = _conflictGraph.getPartitioningNeighbors(node);
        Collection sNeighbors  = _conflictGraph.getSharingNeighbors(node);
        Collection pPartitions = _conflictGraph.partitionsOf(pNeighbors);
        Collection sPartitions = _conflictGraph.partitionsOf(sNeighbors);
        // The case of trivial singleton connected component.
        if (neighbors.isEmpty()) {
            _minPartition(_conflictGraph).addNode(node);
            return;
        }

        // If any partitioning neighbor is assigned.
        if (!pPartitions.isEmpty()) {
            // Internal partitioning conflicts are impossible according to
            // the observed conflict graphs of practical cases.
            if (pPartitions.size() == 2)
                throw new DataPartitioningException(
                        "Invalid conflict graph topology.");
            // Assign the node to the dual partition to maximize the edge cut
            // for the partitioning conflicts.
            else {
                GraphPartition dualPartition = _conflictGraph.dualOf(
                        (GraphPartition)pPartitions.iterator().next());
                dualPartition.addNode(node);
            }

        // No partitioning neighbors are assigned to graph partition(s).
        } else {
            // No sharing neighbors or all of them have not been assigned.
            if (sNeighbors.isEmpty() || sPartitions.isEmpty()) {
                _minPartition(_conflictGraph).addNode(node);
            // Some sharing neighbors are assigned.
            } else {
                // Internal sharing conflicts exist no matter what partition
                // the node is assigned to. Pick the smallest partition.
                if (sPartitions.size() == 2) {
                    _minPartition(_conflictGraph).addNode(node);
                // Only one partition is associated with all the assigned
                // sharing neighbors. Put the node in the dual partition.
                } else {
                    GraphPartition dualPartition = _conflictGraph.dualOf(
                            (GraphPartition)sPartitions.iterator().next());
                    dualPartition.addNode(node);
                }
            }
        }

        // Recursive invocation of all neighbors, no matter partitioning or
        // sharing neighbors.
        Iterator neighborsIterator = neighbors.iterator();
        while (neighborsIterator.hasNext())
            _prioritizedAlternateAssignment((Node)neighborsIterator.next());
    }

    ///////////////////////////////////////////////////////////////////
    //////// Common sub-routines for all partitioning strategies.

    /** The partition with minimal sum of node values.
     *  @param pGraph The partitioned graph.
     *  @return The minimal partition.
     */
    protected GraphPartition _minPartition(PartitionedGraph pGraph) {
        return (GraphPartition)pGraph.ascendentPartitions().get(0);
    }

    /** Check existence of links between the partition and node.
     *  @param partition The partition.
     *  @param node The node.
     *  @return True if there are links between them, false otherwise.
     */
    protected boolean _linksExistFor(GraphPartition partition, Node node) {
        Iterator neighbors = _conflictGraph.neighbors(node).iterator();
        while (neighbors.hasNext()) {
            Node neighbor = (Node)neighbors.next();
            if (partition.containsNode(neighbor))
                return true;
        }
        return false;
    }

    ///////////////////////////////////////////////////////////////////
    ////                      protected variables                  ////

    /** The conflict graph. */
    protected ConflictGraph _conflictGraph;

    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////

    /*  Two coloring a node of a connected graph. Coloring is actually
     *  done by node assignment to partitions.
     *  @param node The node to color.
     *  @param graph The connected sub-graph.
     *  @param neighborIndex The partition index of the neighbor node.
     */
    private void _twoColoringFor(Node node, Graph graph, int neighborIndex) {
        if (_conflictGraph.partitionOf(node) == null) {
            int thisIndex;
            Collection neighborCollection = graph.neighbors(node);
            // Trivial components with only one node.
            if (neighborCollection.size() == 0) {
                ((GraphPartition)_conflictGraph.
                        getPartitions().get(0)).addNode(node);
                /* ---- Random selection of partition ----
                thisIndex = (Math.random() < 0.5) ? 0 : 1;
                ((GraphPartition)_conflictGraph.
                        getPartitions().get(thisIndex)).addNode(node);
                */

            } else {
                thisIndex = 1 - neighborIndex;
                ((GraphPartition)_conflictGraph.
                        getPartitions().get(thisIndex)).addNode(node);
                Iterator neighbors = neighborCollection.iterator();
                while (neighbors.hasNext()) {
                    Node neighbor = (Node)neighbors.next();
                    _twoColoringFor(neighbor, graph, thisIndex);
                }
            }
        }
    }

    ///////////////////////////////////////////////////////////////////
    //////// Sub-routines for consolidateSharedBuffersStrategy()

    /*  Return a lifetime compatible graph for the SDF buffers.
     *  @return The compatible graph.
     */
    private Graph _bufferCompatibleGraph() {
        Graph bufferConflictGraph = new Graph();
        bufferConflictGraph.addNodes(_conflictGraph.getSDFBuffers());
        bufferConflictGraph.addEdges(_conflictGraph.getSharingConflicts());
        return Graphs.complementGraph(bufferConflictGraph);
    }

    /*  Save the relation for regular buffers, consolidated buffers, and
     *  clique components.
     *  @param components The clique components.
     */
    private void _setupBufferConsolidation(Collection components) {
        _toConsolidatedBuffers = new HashMap();
        _toCliqueComponents    = new HashMap();
        _consolidatedBuffers   = new ArrayList();
        Map sizeMap            = new HashMap();

        Iterator componentIter = components.iterator();
        while (componentIter.hasNext()) {
            Collection component = (Collection)componentIter.next();
            // Create a new buffer to replace the regular
            // buffers to consolidate.
            Node consolidatedBuffer = new Node();
            _consolidatedBuffers.add(consolidatedBuffer);
            // Set the mapping from consolidated buffers to the groups
            // of regular buffers.
            _toCliqueComponents.put(consolidatedBuffer, component);

            Iterator buffers = component.iterator();
            double bufferSize = 0.0;
            while (buffers.hasNext()) {
                Node buffer = (Node)buffers.next();
                // Set the mapping from regular buffers to consolidated ones.
                _toConsolidatedBuffers.put(buffer, consolidatedBuffer);
                // Set the max size of regular buffers as the consolidated
                // buffer's size
                bufferSize = Math.max(
                        bufferSize, _conflictGraph.valueOf(buffer));
            }
            sizeMap.put(consolidatedBuffer, new Double(bufferSize));
        }
        _consolidatedBufferSizes = new ToDoubleMapMapping(sizeMap);
    }

    /*  Return a new conflict graph which incorporated consolidated buffers.
     *  @return The new conflict graph.
     */
    private ConflictGraph _toConsolidatedConflictGraph() {
        ConflictGraph consolidatedGraph = new ConflictGraph();

        // add non-shared regular buffers and state variables
        Iterator nodes = _conflictGraph.nodes().iterator();
        while (nodes.hasNext()) {
            Node node = (Node)nodes.next();
            if (!_toConsolidatedBuffers.containsKey(node)) {
                consolidatedGraph.addNode(node);
                consolidatedGraph.setElementValue(
                        node, _conflictGraph.valueOf(node));
            }
        }
        // add shared buffers
        consolidatedGraph.addNodes(_consolidatedBuffers);
        Iterator cBuffers = _consolidatedBuffers.iterator();
        while (cBuffers.hasNext()) {
            Node cBuffer = (Node)cBuffers.next();
            consolidatedGraph.setElementValue(
                    cBuffer, _consolidatedBufferSizes.toDouble(cBuffer));
        }

        // Add and set edges in the buffer-consolidated graph.
        Iterator edges = _conflictGraph.edges().iterator();
        while (edges.hasNext()) {
            Edge edge = (Edge)edges.next();

            // Find the corresponding source and sink nodes in the
            // buffer-consolidated graph.
            Node cSource, cSink;
            if (_toConsolidatedBuffers.containsKey(edge.source())) {
                cSource = (Node)_toConsolidatedBuffers.get(edge.source());
            } else {
                cSource = edge.source();
            }
            if (_toConsolidatedBuffers.containsKey(edge.sink())) {
                cSink = (Node)_toConsolidatedBuffers.get(edge.sink());
            } else {
                cSink = edge.sink();
            }
            // Add an edge between the source and sink of the
            // buffer-consolidated graph and set appropriate conflict type.
            if (consolidatedGraph.neighborEdges(cSource, cSink).isEmpty()) {
                Edge newEdge = consolidatedGraph.addEdge(cSource, cSink);
                if (_conflictGraph.isSharingConflict(edge))
                    consolidatedGraph.setSharingConflict(newEdge);
                else
                    consolidatedGraph.setPartitioningConflict(newEdge);
            }
        }

        return consolidatedGraph;
    }

    /*  Translate partitions in the buffer-consolidated conflict graph
     *  to the original conflict graph.
     *  @param consolidatedConflictGraph The consolidated conflict graph.
     */
    private void _translatePartitions(ConflictGraph consolidatedConflictGraph) {
        // Translate the results partition by partition.
        for (int i = 0; i < 2; i++) {
            GraphPartition cPartition =
                    consolidatedConflictGraph.partitionOf(i);
            GraphPartition partition = _conflictGraph.partitionOf(i);
            // Translate the partitions in buffer-consolidated conflict graph
            // to the original conflict graph.
            Iterator cNodes = cPartition.nodes().iterator();
            while (cNodes.hasNext()) {
                Node cNode = (Node)cNodes.next();
                if (_toCliqueComponents.containsKey(cNode))
                    partition.addNodes(
                            (Collection)_toCliqueComponents.get(cNode));
                else
                    partition.addNode(cNode);
            }
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////

    // Mapping from regular buffers to consolidated buffers.
    private Map _toConsolidatedBuffers;

    // Mapping from consolidated buffers to clique components.
    private Map _toCliqueComponents;

    // The mapping from a consolidated buffer to its size.
    private ToDoubleMapMapping _consolidatedBufferSizes;

    // Nodes of consolidated buffers.
    private Collection _consolidatedBuffers;

    // The minimal partition size.
    private int _minPartSize;
}


