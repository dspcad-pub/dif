/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* The conflict graph for data partitioning. */

package dif.csdf.sdf.mem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import dif.util.graph.Edge;
import dif.util.graph.Graph;
import dif.util.graph.GraphElementException;
import dif.util.graph.Node;
import dif.util.graph.mapping.ToIntMapMapping;
import dif.util.sched.Schedule;
import dif.csdf.sdf.SDFGraph;

//////////////////////////////////////////////////////////////////////////
//// ConflictGraph
/** The conflict graph for data partitioning.

@author Mingyung Ko
@version $Id: ConflictGraph.java 606 2008-10-08 16:29:47Z plishker $
*/

public class ConflictGraph extends PartitionedGraph {

    /** A constructor.
     */
    public ConflictGraph() {
        super();
        _initialize();
    }

    /** Constructor for a given graph. Construct the partitioned graph by
     *  duplicating all nodes and edges.
     *  @param graph The graph to construct from.
     */
    public ConflictGraph(Graph graph) {
        super(graph);
        _initialize();
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Get all the edges of partitioning conflicts. The edges are
     *  returned in an unmodifiable form.
     *  @return The conflicts in an unmodifiable <code>Collection</code>.
     */
    public Collection getPartitioningConflicts() {
        return Collections.unmodifiableCollection(_partitioningConflicts);
    }

    /** Get neighbors with partitioning conflicts for the given node. The
     *  neighbors are returned in an unmodifiable form.
     *
     *  @param node The given node to get neighbors.
     *  @return The neighbors in an unmodifiable <code>Collection</code>.
     */
    public Collection getPartitioningNeighbors(Node node) {
        if (!_partitioningNeighborsMap.containsKey(node))
            return new HashSet();
        return Collections.unmodifiableCollection(
                (Collection)_partitioningNeighborsMap.get(node));
    }

    /** Get all the edges of sharing conflicts. The edges are
     *  returned in an unmodifiable form.
     *  @return The conflicts in an unmodifiable <code>Collection</code>.
     */
    public Collection getSharingConflicts() {
        return Collections.unmodifiableCollection(_sharingConflicts);
    }

    /** Get neighbors with sharing conflicts for the given node. The
     *  neighbors are returned in an unmodifiable form.
     *
     *  @param node The given node to get neighbors.
     *  @return The neighbors in an unmodifiable <code>Collection</code>.
     */
    public Collection getSharingNeighbors(Node node) {
        if (!_sharingNeighborsMap.containsKey(node))
            return new HashSet();
        return Collections.unmodifiableCollection(
                (Collection)_sharingNeighborsMap.get(node));
    }

    /** Get the SDF buffer by a given SDF edge. SDF buffers are
     *  represented as conflict graph nodes.
     *
     *  @param sdfEdge The given SDF edge.
     *  @return The conflict graph node representing the SDF buffer.
     */
    public Node getSDFBufferOf(Edge sdfEdge) {
        GraphElementException.checkEdge(sdfEdge, _sdfGraph);
        return (Node)_sdfBufferMapping.get(sdfEdge);
    }

    /** Get all the nodes representing SDF buffers. The nodes are returned
     *  in an unmodifiable form.
     *
     *  @return Nodes of SDF buffers.
     */
    public Collection getSDFBuffers() {
        return Collections.unmodifiableCollection(_sdfBuffers);
    }

    /** Get the SDF edge corresponding to the given SDF buffer
     *  (represented as conflict graph node).
     *
     *  @param buffer The given node representing an SDF buffer.
     *  @return The corresponding SDF edge.
     */
    public Edge getSDFEdgeOf(Node buffer) {
        _checkGraphElement(buffer);
        return (Edge)_sdfBufferMapping.get(buffer);
    }

    /** Get all the corresponding SDF edges for the given partition.
     *  @param partition The given partition.
     *  @return The collection of the corresponding SDF edges.
     */
    public Collection getSDFEdgesOf(GraphPartition partition) {
        List edges = new ArrayList();
        Iterator variables = partition.nodes().iterator();
        while (variables.hasNext()) {
            Node variable = (Node)variables.next();
            if (isSDFBuffer(variable))
                edges.add(getSDFEdgeOf(variable));
        }
        return edges;
    }

    /** Get the SDF graph associated with the conflict graph.
     *  @return The associated SDF graph.
     */
    public SDFGraph getSDFGraph() {
        return _sdfGraph;
    }

    /** Check whether all partitioning conflicts are in the edge cut.
     *  @return True if the parallelism is maximal.
     */
    public boolean isParallelismMaximized() {
        Collection edgeCut = edgeCut();
        Iterator pConflicts = _partitioningConflicts.iterator();
        while (pConflicts.hasNext())
            if (!edgeCut.contains(pConflicts.next()))
                return false;
        return true;
    }

    /** Check whether the given node is an SDF buffer.
     *  @param node The node to check.
     *  @return True if the given node represents an SDF buffer.
     */
    public boolean isSDFBuffer(Node node) {
        _checkGraphElement(node);
        return _sdfBufferMapping.containsKey(node);
    }

    /** Check whether the edge is a sharing conflict.
     *  @param edge The edge to check.
     *  @return True if it is a sharing conflict edge.
     */
    public boolean isSharingConflict(Edge edge) {
        _checkGraphElement(edge);
        return _sharingConflicts.contains(edge);
    }

    /** Display partitions in a text string.
     *  @return The output.
     */
    public String partitionString() {
        String out = new String();
        int partitionCounts = getPartitions().size();
        out += "partition counts = " + partitionCounts + "\n\n";
        for (int i = 0; i < partitionCounts; i++) {
            GraphPartition partition =
                    (GraphPartition)getPartitions().get(i);
            out += "partition " + i +
                    " of total value " + valueOf(partition) + "\n";
            Iterator nodes = partition.nodes().iterator();
            while (nodes.hasNext()) {
                Node node = (Node)nodes.next();
                out += "node" + nodeLabel(node) + "(";
                // Show that the node is SDF buffer (B) or state variable (S).
                out += isSDFBuffer(node) ? "B" : "S";
                out += "),\tvalue=" + valueOf(node) + "\n";
            }
            // Delimiter to display next partition.
            out += "\n";
        }
        return out;
    }

    /** Remove all the sharing conflict edges.
     */
    public void removeSharingConflicts() {
        // Remove the corresponding graph edges.
        Iterator sharingConflicts = _sharingConflicts.iterator();
        while (sharingConflicts.hasNext())
            removeEdge((Edge)sharingConflicts.next());

        _sharingConflicts = new ArrayList();
        _sharingNeighborsMap = new HashMap();
    }

    /** Set buffer mapping for a node. Conflict graph nodes represent
     *  SDF buffers (allocated for SDF edges) and program variables.
     *  Buffers are not implemented and simply represented as SDF edges.
     *
     *  @param buffer The node representing SDF buffers.
     *  @param sdfEdge The SDF edge the buffer corresponds to.
     */
    public void setSDFBufferMapping(Node buffer, Edge sdfEdge) {
        _checkGraphElement(buffer);
        // Keep the nodes of SDF buffers.
        _sdfBuffers.add(buffer);
        // Two-way mapping between buffers and SDF edges.
        _sdfBufferMapping.put(buffer, sdfEdge);
        _sdfBufferMapping.put(sdfEdge, buffer);
    }

    /** Set the edge as partitioning conflict. Value of a partitioning
     *  conflict edge equals to the maximal edge counts of an N-node
     *  graph. That is, the value equals </code>N(N-1)/2<code>.
     *
     *  @param edge The partitioning conflict.
     */
    public void setPartitioningConflict(Edge edge) {
        _checkGraphElement(edge);
        setElementValue(edge, nodeCount());
        _partitioningConflicts.add(edge);
        _addToNeighborsMap(edge, _partitioningNeighborsMap);
    }

    /** Set the associated SDF graph. SDF buffers are also incorporated as
     *  conflict graph nodes.
     *
     *  @param sdfGraph The associated SDF graph.
     */
    public void setSDFGraph(SDFGraph sdfGraph) {
        _sdfGraph = sdfGraph;

        // Add buffers (nodes) to the conflict graph and associate
        // the buffer size as node values.
        Iterator sdfEdges = sdfGraph.edges().iterator();
        while (sdfEdges.hasNext()) {
            Edge sdfEdge = (Edge)sdfEdges.next();
            Node node = addNode();
            // Set default buffer size to 1.
            setElementValue(node, 1.0);
            setSDFBufferMapping(node, sdfEdge);
        }
    }

    /** Set the edge as sharing conflict. Value of a sharing
     *  conflict edge equals one.
     *
     *  @param edge The partitioning conflict.
     */
    public void setSharingConflict(Edge edge) {
        _checkGraphElement(edge);
        setElementValue(edge, 1.0);
        _sharingConflicts.add(edge);
        _addToNeighborsMap(edge, _sharingNeighborsMap);
    }

    /** Total state variable cost of the given partition. The cost excludes
     *  buffer sizes which are changing dynamically or not yet figured out.
     *
     *  @param partition The partition.
     *  @return The capacity requirement (or memory space cost).
     */
    public double stateVariableCost(GraphPartition partition) {
        double cost = 0.0;
        Iterator nodes = partition.nodes().iterator();
        while (nodes.hasNext()) {
            Node node = (Node)nodes.next();
            if (!isSDFBuffer(node))
                cost += valueOf(node);
        }
        return cost;
    }

    /** Update SDF buffer sizes for nodes representing SDF buffers.
     *  @param sizes The buffer costs.
     */
    public void updateSDFBufferCost(ToIntMapMapping sizes) {
        Iterator sdfEdges = _sdfGraph.edges().iterator();
        while (sdfEdges.hasNext()) {
            Edge sdfEdge = (Edge)sdfEdges.next();
            Node sdfBufferNode = getSDFBufferOf(sdfEdge);
            setElementValue(sdfBufferNode, sizes.toInt(sdfEdge));
        }
    }

    /** Update SDF buffer sizes from a schedule. The schedule must be
     *  consistent with the associated SDF graph.
     *  @param schedule The schedule to derive the cost.
     */
    public void updateSDFBufferCost(Schedule schedule) {
        updateSDFBufferCost(
                BufferUtilities.bufferSizeMapping(getSDFGraph(), schedule));
    }

    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////

    /*  Add the edge's ending nodes to the appropriate neighbors map. */
    private void _addToNeighborsMap(Edge edge, Map neighborsMap) {
        Node source = edge.source();
        Node sink   = edge.sink();
        _addToNeighborsMap(source, sink,   neighborsMap);
        _addToNeighborsMap(sink,   source, neighborsMap);
    }

    /*  Add the node and the neighbor to the given neighbors map.
     */
    private void _addToNeighborsMap(Node node, Node neighbor, Map theMap) {
        if (!theMap.containsKey(node))
            theMap.put(node, new HashSet());
        ((Collection)theMap.get(node)).add(neighbor);
    }

    /*  Allocate space for private variables. */
    private void _initialize() {
        _sdfBufferMapping = new HashMap();
        _partitioningConflicts    = new ArrayList();
        _sharingConflicts         = new ArrayList();
        _partitioningNeighborsMap = new HashMap();
        _sharingNeighborsMap      = new HashMap();
        _sdfBuffers               = new ArrayList();
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    // Mapping between SDF buffer nodes (a subset of conflict nodes)
    // and SDF edges. The key-value pairs are stored bi-directionally
    // (mapping either way between buffer nodes and SDF edges).
    private Map _sdfBufferMapping;

    // Partitioning and sharing conflict edges.
    private Collection _partitioningConflicts;
    private Collection _sharingConflicts;

    // Neighbors with partitioning and sharing conflicts.
    private Map _partitioningNeighborsMap;
    private Map _sharingNeighborsMap;

    // The associated SDF graph for buffer information.
    private SDFGraph _sdfGraph;

    // Nodes representing SDF buffers.
    private Collection _sdfBuffers;
}


