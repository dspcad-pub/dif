/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Utilities for various buffer information. */

package dif.csdf.sdf.mem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import dif.util.graph.Edge;
import dif.util.graph.Node;
import dif.util.graph.mapping.ToIntMapMapping;
import dif.util.sched.Firing;
import dif.util.sched.Schedule;
import dif.csdf.sdf.SDFEdgeWeight;
import dif.csdf.sdf.SDFGraph;

//////////////////////////////////////////////////////////////////////////
//// BufferUtilities
/** Utilities for buffer related computation. This class is just a
collection of static methods and should not be allocated as objects.

@author Mingyung Ko
@version $Id: BufferUtilities.java 606 2008-10-08 16:29:47Z plishker $
*/

public class BufferUtilities {

    /** A private constructor to prevent object allocation. */
    private BufferUtilities() {}

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Compute total buffer cost for an SDF graph and a schedule.
     *  This is a universal, though naive, implementation. Users may wish to
     *  override this with an efficient one.
     *
     *  @param graph The given SDF graph.
     *  @param schedule The given schedule.
     *  @return Total buffer cost.
     */
    static public int bufferCost(SDFGraph graph, Schedule schedule) {
        return bufferCost(graph.edges(), graph, schedule);
    }

    /** Compute buffer cost for the SDF edge collection and the schedule.
     *  This is a universal, though naive, implementation. Users may wish to
     *  override this with an efficient one.
     *
     *  @param edgeCollection The collection of edges.
     *  @param graph The given SDF graph.
     *  @param schedule The given schedule.
     *  @return Buffer cost for the edge collection.
     */
    static public int bufferCost(
            Collection edgeCollection, SDFGraph graph, Schedule schedule) {

        int cost = 0;
        int[] bSize = bufferSize(graph, schedule);
        Iterator edges = edgeCollection.iterator();
        while (edges.hasNext())
            cost += bSize[graph.edgeLabel((Edge)edges.next())];
        return cost;
    }

    /** Display buffer lifetime overlaps in text. The method is primary for
     *  debugging purpose.
     *
     *  @param graph The given SDF graph.
     *  @param intersectionsMap The given buffer intersections.
     *  @return The buffer lifetime overlaps in text.
     */
    static public String bufferIntersectionsToString(
            SDFGraph graph, Map intersectionsMap) {

        String intersectionString = "A description of the buffer lifetime overlaps\n";
        for (int i = 0; i < graph.edgeCount(); i++) {
            Edge buffer = graph.edge(i);
            intersectionString += _bufferToString(graph, buffer) + ":";
            Iterator intersectedBuffers =
                    ((Collection)intersectionsMap.get(buffer)).iterator();
            while (intersectedBuffers.hasNext()) {
                Edge intersectedBuffer = (Edge)intersectedBuffers.next();
                intersectionString +=
                        " " + _bufferToString(graph, intersectedBuffer);
            }
            intersectionString += "\n";
        }
        return intersectionString;
    }

    /** Display buffer lifetime overlaps in text. The method is primary for
     *  debugging purpose.
     *
     *  @param graph The given SDF graph.
     *  @param schedule The given schedule.
     *  @return The buffer lifetime overlaps in text.
     */
    static public String bufferIntersectionsToString(
            SDFGraph graph, Schedule schedule) {

        return bufferIntersectionsToString(
                graph, bufferIntersections(graph, schedule));
    }

    /** Build buffer intersections (lifetime overlaps). Buffer intersections
     *  are used in buffer sharing. The intersections are saved in the
     *  form of <code>Map</code> of <code>Set</code>s. Each map entry is
     *  an edge to a set of edges (excluding itself) intersecting with it.
     *  The code here is naive but universal for intersection calculation.
     *  Users may wish to override this with an efficient one.
     *
     *  @param graph The given SDF graph.
     *  @param schedule The given schedule.
     *  @return The buffer intersections in the form of <code>Map</code>
     *          of <code>Set</code>s.
     */
    static public Map bufferIntersections(
            SDFGraph graph, Schedule schedule) {

        Iterator history = _getBufferHistory(graph, schedule).iterator();

        // initialize the buffer intersection map
        HashMap intersectionMap = new HashMap();
        Iterator edges = graph.edges().iterator();
        while (edges.hasNext()) {
            intersectionMap.put((Edge)edges.next(), new HashSet());
        }

        // Go through the history and check all living buffers.
        while (history.hasNext()) {
            Set buffers = ((Map)history.next()).keySet();
            Iterator alives = buffers.iterator();
            while (alives.hasNext()) {
                Edge buffer = (Edge)alives.next();
                Set intersection = (Set)intersectionMap.get(buffer);
                intersection.addAll(buffers);
                // exclude the buffer itself
                intersection.remove(buffer);
            }
        }
        return intersectionMap;
    }

    /** Get the lower bound for buffer sharing cost. Sharing cost means the
     *  total memory space needed for shared buffers. This is a universal,
     *  though naive, implementation. Users may wish to override this with
     *  an efficient one.
     *
     *  @param graph The given SDF graph.
     *  @param schedule The given schedule.
     *  @return The lower bound for buffer sharing cost.
     */
    static public int bufferSharingLowerBound(
            SDFGraph graph, Schedule schedule) {

        int[] size = bufferSize(graph, schedule);
        // Buffer history is cached in _bufferHistory during bufferSize().
        Iterator history = _bufferHistory.iterator();

        // Go through buffer history and check buffer cost at every instant.
        int bound = 0;
        while (history.hasNext()) {
            // Total buffer cost for this instant.
            int cost = 0;
            Iterator buffers = ((Map)history.next()).keySet().iterator();
            while (buffers.hasNext()) {
                int label = graph.edgeLabel((Edge)buffers.next());
                cost += size[label];
            }
            if (cost > bound)
                bound = cost;
        }
        return bound;
    }

    /** Compute buffer size for a given SDF graph and schedule.
     *  This is a universal, though naive, implementation. Users may wish to
     *  override this with an efficient one.
     *
     *  @param graph The given SDF graph.
     *  @param schedule The given schedule.
     *  @return An array of buffer size.
     */
    static public int[] bufferSize(SDFGraph graph, Schedule schedule) {

        Iterator history = _getBufferHistory(graph, schedule).iterator();
        int[] bufferSize = new int[graph.edgeCount()];
        // Go through buffer living history to check the maximum
        // tokens residing in each buffer.
        while (history.hasNext()) {
            Map bufferMap = (Map)history.next();
            Iterator buffers = bufferMap.keySet().iterator();
            while (buffers.hasNext()) {
                Edge buffer = (Edge)buffers.next();
                int label = graph.edgeLabel(buffer);
                int token = ((Integer)bufferMap.get(buffer)).intValue();
                if (token > bufferSize[label])
                    bufferSize[label] = token;
            }
        }
        return bufferSize;
    }

    /** Compute buffer size for a given SDF graph and schedule.
     *  This is a universal, though naive, implementation. Users may wish to
     *  override this with an efficient one.
     *
     *  @param graph The given SDF graph.
     *  @param schedule The given schedule.
     *  @return A map from edges to buffer sizes.
     */
    static public Map bufferSizeMap(SDFGraph graph, Schedule schedule) {
        Map sizeMap = new HashMap();
        int[] sizes = bufferSize(graph, schedule);
        Iterator buffers = graph.edges().iterator();
        while (buffers.hasNext()) {
            Edge buffer = (Edge)buffers.next();
            sizeMap.put(buffer, new Integer(sizes[graph.edgeLabel(buffer)]));
        }
        return sizeMap;
    }

    /** Compute buffer size for a given SDF graph and schedule.
     *  This is a universal, though naive, implementation. Users may wish to
     *  override this with an efficient one.
     *
     *  @param graph The given SDF graph.
     *  @param schedule The given schedule.
     *  @return The buffer sizes in <code>ToIntMapMapping</code>.
     */
    static public ToIntMapMapping
            bufferSizeMapping(SDFGraph graph, Schedule schedule) {
        return (new ToIntMapMapping(bufferSizeMap(graph, schedule)));
    }

    /** Verify the correctness of the given buffer intersections. The given
     *  intersections are compared to those computed by
     *  {@link #bufferIntersections(SDFGraph, Schedule)}, where the results
     *  are computed in an exhausted and brute force way. This method is
     *  primary for debugging purpose.
     *
     *  @param graph The given SDF graph.
     *  @param schedule The given schedule.
     *  @param intersectionsMap The buffer intersections to verify.
     *  @return True if the intersections are correct; false otherwise.
     */
    static public boolean verifyBufferIntersections(
            SDFGraph graph, Schedule schedule, Map intersectionsMap) {

        Map intersectionsMapToCompare = bufferIntersections(graph, schedule);
        boolean verificationResult = true;
        Iterator buffers = graph.edges().iterator();
        while (buffers.hasNext()) {
            Edge buffer = (Edge)buffers.next();
            Collection intersections =
                    (Collection)intersectionsMap.get(buffer);
            Collection intersectionsToCompare =
                    (Collection)intersectionsMapToCompare.get(buffer);
            // Compute (intersections - intersectionsToCompare) and
            // (intersectionsToCompare - intersections). If either results in
            // non-empty set, then intersections and intersectionsToCompare are
            // NOT identical.
            Collection subset1 = new HashSet(intersections);
            Collection subset2 = new HashSet(intersectionsToCompare);
            subset1.removeAll(intersectionsToCompare);
            subset2.removeAll(intersections);
            if (!subset1.isEmpty() || !subset2.isEmpty()) {
                verificationResult = false;
                break;
            }
        }
        return verificationResult;
    }

    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////

    /*  Get buffer living history. The history is saved in the form of
     *  <code>List</code>. Each List element is a map from living
     *  buffers to the number of tokens deposited so far.
     *  The order of the List is the same as
     *  {@link SDFSchedule#firingIterator()}. It means that the
     *  referenced time is based on <code>Firing</code>s.
     *  This method is mainly for other buffer computation.
     *
     *  @param graph The given SDF graph.
     *  @param schedule The given schedule.
     *  @return A List of Maps from living buffers/edge to token counts.
     */
    static private List _getBufferHistory(
            SDFGraph graph, Schedule schedule) {

        int totalBuffers = graph.edgeCount();
        int[] tokens = new int[totalBuffers];
        ArrayList history = new ArrayList();

        // Set initial buffer cost to delay counts.
        Iterator edges = graph.edges().iterator();
        while (edges.hasNext()) {
            Edge edge = (Edge)edges.next();
            int edgeLabel = graph.edgeLabel(edge);
            // The meaning of delay is the same in CSDF and SDF.
            tokens[edgeLabel] =
                    ((SDFEdgeWeight)edge.getWeight()).getIntDelay();
        }

        // Expand the hierarchical schedule to flattened firings. Firing
        // is the 'time unit' of buffer history.
        Iterator firings = schedule.firingIterator();
        while (firings.hasNext()) {
            Firing firing = (Firing)firings.next();
            Node node = (Node)firing.getFiringElement();
            int count = firing.getIterationCount();

            // Check all buffers linked to the fired actor.
            Map livingBuffers = new HashMap();
            Iterator incidentEdges = graph.incidentEdges(node).iterator();
            while (incidentEdges.hasNext()) {
                Edge edge = (Edge)incidentEdges.next();
                int edgeLabel = graph.edgeLabel(edge);
                SDFEdgeWeight weight = (SDFEdgeWeight)edge.getWeight();

                // Save token results.
                if (node == edge.source()) {
                    int production = weight.getSDFProductionRate();
                    tokens[edgeLabel] += production * count;
                } else {
                    int consumption = weight.getSDFConsumptionRate();
                    tokens[edgeLabel] -= consumption * count;
                }

                // Buffers are alive due to execution of this Firing.
                livingBuffers.put(edge, new Integer(tokens[edgeLabel]));
            }

            // Count in other already living buffers.
            for (int i = 0; i < totalBuffers; i++) {
                Edge edge = graph.edge(i);
                if ( (tokens[i] > 0) & (!livingBuffers.containsKey(edge)) )
                    livingBuffers.put(edge, new Integer(tokens[i]));
            }

            // Save buffers living during this Firing execution.
            history.add(livingBuffers);
        }

        return (_bufferHistory = history);
    }

    /*  Return a name in text for the given SDF buffer (edge).
     */
    static private String _bufferToString(SDFGraph graph, Edge buffer) {
        return (new String("e") + graph.edgeLabel(buffer));
    }

    /*  Return a name in text for the given SDF node.
     */
    static private String _nodeToString(SDFGraph graph, Node node) {
        return (new String("n") + graph.nodeLabel(node));
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    // A static variable to cache the history just computed.
    static private List _bufferHistory;
}


