/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Partition of a graph. */

package dif.csdf.sdf.mem;

import java.util.List;

import dif.util.graph.Graph;

//////////////////////////////////////////////////////////////////////////
//// GraphPartition
/** A partition of a graph. A partition is itself a graph, too. Basically,
partitions are subgraphs with additional functions.

@author Mingyung Ko
@version $Id: GraphPartition.java 606 2008-10-08 16:29:47Z plishker $
*/

public class GraphPartition extends PartitionBase {

    /** A constructor.
     */
    public GraphPartition() {
        super();
    }

    /** Constructor for a given graph and element values. Construct the
     *  partition by duplicating all nodes and edges.
     *
     *  @param graph The graph to construct from.
     */
    public GraphPartition(Graph graph) {
        super(graph);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Get an ascendently sorted edges.
     *  @return The sorted edges in <code>List</code>.
     */
    public List ascendentEdges() {
        return ascendentListOf(edges());
    }

    /** Get an ascendently sorted nodes.
     *  @return The sorted nodes in <code>List</code>.
     */
    public List ascendentNodes() {
        return ascendentListOf(nodes());
    }

    /** Get a descendently sorted edges.
     *  @return The sorted edges in <code>List</code>.
     */
    public List descendentEdges() {
        return descendentListOf(edges());
    }

    /** Get a descendently sorted nodes.
     *  @return The sorted nodes in <code>List</code>.
     */
    public List descendentNodes() {
        return descendentListOf(nodes());
    }

    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////
}


