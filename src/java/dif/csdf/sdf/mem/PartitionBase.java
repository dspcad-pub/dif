/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A base class for partitioned graph structure. */

package dif.csdf.sdf.mem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import dif.util.graph.Edge;
import dif.util.graph.Element;
import dif.util.graph.Graph;
import dif.util.graph.GraphElementException;
import dif.util.graph.Node;
import dif.util.graph.mapping.ToDoubleMapMapping;

//////////////////////////////////////////////////////////////////////////
//// PartitionBase
/** A base class for partitioned graph structure. This class provides
commonly used methods for
{@link GraphPartition} and {@link PartitionedGraph}.
Basically, this class is still an instance of graph because all partition
structures are indeed graphs.

@author Mingyung Ko
@version $Id: PartitionBase.java 606 2008-10-08 16:29:47Z plishker $
*/

public class PartitionBase extends Graph {

    /** A constructor.
     */
    public PartitionBase() {
        _elementValueMap = new HashMap();
        _elementValueMapping = new ToDoubleMapMapping(_elementValueMap);
    }

    /** Constructor for a given graph. Construct the partitioned graph by
     *  duplicating all nodes and edges.
     *
     *  @param graph The graph to construct from.
     */
    public PartitionBase(Graph graph) {
        _elementValueMap = new HashMap();
        _elementValueMapping = new ToDoubleMapMapping(_elementValueMap);
        addNodes(graph.nodes());
        addEdges(graph.edges());
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** An ascendently ordered list of objects. It is the associated
     *  values, see {@link #valueOf(Object)}, being sorted. The
     *  argument comes from nodes()/edges() methods in class {@link Graph}
     *  or a collection of {@link GraphPartition}s.
     *
     *  @param objects The objects to be ordered.
     *  @return An ascendent list of the objects.
     */
    public List ascendentListOf(Collection objects) {
        ArrayList objectList = new ArrayList(objects);
        Collections.sort(objectList, new upComparator());
        return objectList;
    }

    /** A descendently ordered list of objects. It is the associated
     *  values, see {@link #valueOf(Object)}, being sorted. The
     *  argument comes from nodes()/edges() methods in class {@link Graph}
     *  or a collection of {@link GraphPartition}s.
     *
     *  @param elements The objects to be ordered.
     *  @return A descendent list of the objects.
     */
    public List descendentListOf(Collection objects) {
        ArrayList objectList = new ArrayList(objects);
        Collections.sort(objectList, new downComparator());
        return objectList;
    }

    /** Return index of this partition base where it is contained.
     *  @return The index number.
     */
    public int getIndex() {
        return _index;
    }

    /** Remove a graph edge and the associated value.
     *
     *  @param edge The given edge.
     *  @return True if the edge was removed.
     */
    public boolean removeEdge(Edge edge) {
        _elementValueMap.remove(edge);
        return super.removeEdge(edge);
    }

    /** Remove a graph node and the associated value.
     *
     *  @param node The given node.
     *  @return True if the node was removed.
     */
    public boolean removeNode(Node node) {
        _elementValueMap.remove(node);
        return super.removeNode(node);
    }

    /** Set the value of a graph element. The value must be of type
     *  </code>double<code>. The previous value is overwritten
     *  if it exists.
     *
     *  @param element The given graph element.
     *  @param value The associated value double.
     */
    public void setElementValue(Element element, double value) {
        _checkGraphElement(element);
        _elementValueMap.put(element, new Double(value));
    }

    /** Set values for graph elements. The previous values are
     *  overwritten if they exist.
     *
     *  @param valueMap A map of the given elements and values.
     */
    public void setElementValues(Map valueMap) {
        Iterator elements = valueMap.keySet().iterator();
        while (elements.hasNext())
            _checkGraphElement((Element)elements.next());
        _elementValueMap.putAll(valueMap);
    }

    /** Set index of this partition base where it is contained.
     *  @param The desired index number.
     */
    public void setIndex(int index) {
        _index = index;
    }

    /** Get the value associated with the object. Valid objects are
     *  node, edge, collection of nodes/edges, and graph partition.
     *  Validity checking is enforced by throwing exceptions.
     *  For graph partitions, the total value of NODEs is returned.
     *
     *  @param object The object to get the associated value.
     *  @return The associated value.
     *  @exception IllegalArgumentException If object invalid to get value.
     */
    public double valueOf(Object object) {
        if (object instanceof Element) {
            _checkGraphElement((Element)object);
            return _elementValueMapping.toDouble(object);

        } else if (object instanceof GraphPartition) {
            // Nodes are chosen to amount up their values.
            return valueOf(((GraphPartition)object).nodes());

        } else if (object instanceof Collection) {
            double total = 0.0;
            Iterator objects = ((Collection)object).iterator();
            while (objects.hasNext())
                total += valueOf(objects.next());
            return total;

        } else {
            throw new IllegalArgumentException(
                    "Invalid object to get value.");
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////

    /*  Check existence in the graph for a given element.
     *  @param element The element to check.
     *  @exception GraphElementException If the element is nonexistent.
     */
    protected void _checkGraphElement(Element element) {
        if (element instanceof Node)
            GraphElementException.checkNode((Node)element, this);
        else
            GraphElementException.checkEdge((Edge)element, this);
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    // The map of elements to the associated values.
    private Map _elementValueMap;

    // The mapping of elements to the associated double values.
    private ToDoubleMapMapping _elementValueMapping;

    // The index number of this partition base in a container object.
    private int _index;

    ///////////////////////////////////////////////////////////////////
    ////                        private classes                    ////

    // Ascendently compare partition's total value.
    private class upComparator implements Comparator {
        public int compare(Object object1, Object object2) {
            int results = (int)(valueOf(object1) - valueOf(object2));

            if (results == 0) {
                if ((object1 instanceof Node) && (object2 instanceof Node)) {
                    results = nodeLabel((Node)object1)
                        - nodeLabel((Node)object2);
                } else if ((object1 instanceof GraphPartition)
                        && (object2 instanceof GraphPartition)) {
                    results = ((GraphPartition)object1).getIndex()
                        - ((GraphPartition)object2).getIndex();
                } else
                    results = 1;
            }
            return results;
        }
    }

    // Descendently compare partition's total value.
    private class downComparator implements Comparator {
        public int compare(Object object1, Object object2) {
            int results = (int)(valueOf(object2) - valueOf(object1));
            if (results == 0) {
                if ((object1 instanceof Node) && (object2 instanceof Node)) {
                    results = nodeLabel((Node)object2)
                            - nodeLabel((Node)object1);
                } else if ((object1 instanceof GraphPartition)
                        && (object2 instanceof GraphPartition)) {
                    results = ((GraphPartition)object2).getIndex()
                            - ((GraphPartition)object1).getIndex();
                } else
                    results = 1;
            }
            return results;
        }
    }
}


