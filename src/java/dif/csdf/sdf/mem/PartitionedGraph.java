/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A graph with partitions. */

package dif.csdf.sdf.mem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import dif.util.graph.Edge;
import dif.util.graph.Element;
import dif.util.graph.Graph;
import dif.util.graph.Node;

//////////////////////////////////////////////////////////////////////////
//// PartitionedGraph
/** A graph of partitions with additional partition related tools. The
partitions are not necessarily disjoint. Disjointness is up to the uers
to maintain.

@author Mingyung Ko
@version $Id: PartitionedGraph.java 606 2008-10-08 16:29:47Z plishker $
*/

public class PartitionedGraph extends PartitionBase {

    /** A constructor.
     */
    public PartitionedGraph() {
        super();
        resetPartitions();
    }

    /** Constructor for a given graph. Construct the partitioned graph by
     *  duplicating all nodes and edges.
     *  @param graph The graph to construct from.
     */
    public PartitionedGraph(Graph graph) {
        super(graph);
        resetPartitions();
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Add the given graph partition.
     *  @param partition The graph partition to add.
     */
    public void addPartition(GraphPartition partition) {
        _partitions.add(partition);
        // List.add() actually appends to the tail of a list.
        partition.setIndex(_partitions.size() - 1);
    }

    /** Add the given number of graph partitions.
     *  @param partitionNumber The number of partitions to add.
     */
    public void addPartitions(int partitionNumber) {
        for (int i = 0; i < partitionNumber; i++)
            addPartition(new GraphPartition());
    }

    /** An ascendently ordered list of partitions. It is the associated
     *  total values, see {@link PartitionBase#valueOf(Object)},
     *  being sorted.
     *  @return An ascendent list of partitions.
     */
    public List ascendentPartitions() {
        return ascendentListOf(getPartitions());
    }

    /** A descendently ordered list of partitions. It is the associated
     *  total values, see {@link partitionBase#valueOf(Object)},
     *  being sorted.
     *  @return A descendent list of partitions.
     */
    public List descendentPartitions() {
        return descendentListOf(getPartitions());
    }

    /** The dual/other partition of the given partition, assuming totally
     *  two partitions of the graph.
     *
     *  @param partition The given partition.
     *  @return The dual partition.
     */
    public GraphPartition dualOf(GraphPartition partition) {
        int partitionNumber = _partitions.size();
        if (partitionNumber != 2)
            throw new RuntimeException(partitionNumber
                    + " partitions are found."
                    + " On calling this method, exactly two partitions"
                    + " are allowed.");
        int dualIndex = 1 - partitionIndexOf(partition);
        return (GraphPartition)_partitions.get(dualIndex);
    }

    /** Get the edge cut set over all partitions. See also
     *  {@link edgeCutOf(Collection)}.
     *  @return The set of edge cut.
     */
    public Collection edgeCut() {
        return edgeCutOf(getPartitions());
    }

    /** Get edge cut set over a collection of partitions. Edge cut is a
     *  set of edges with ending nodes in distinct partitions.
     *  @param partitionCollection A collection of partitions.
     *  @return The set of edge cut.
     */
    public Collection edgeCutOf(Collection partitionCollection) {
        ArrayList cut = new ArrayList();
        Iterator edges = edges().iterator();
        while (edges.hasNext()) {
            Edge edge = (Edge)edges.next();
            GraphPartition sourcePartition = partitionOf(edge.source());
            GraphPartition sinkPartition = partitionOf(edge.sink());
            if (partitionCollection.contains(sourcePartition) &&
                    partitionCollection.contains(sinkPartition)) {
                if (sourcePartition != sinkPartition)
                    cut.add(edge);
            }
        }
        return cut;
    }

    /** Get the edge cut set of two partitions. See also
     *  {@link #edgeCutOf(Collection)}.
     *  @param p1 The first partition.
     *  @param p2 The second partition.
     *  @return The set of edge cut.
     */
    public Collection edgeCutOf(GraphPartition p1, GraphPartition p2) {
        ArrayList partitions = new ArrayList();
        partitions.add(p1);
        partitions.add(p2);
        return edgeCutOf(partitions);
    }

    /** Display edge cut in a text string.
     *  @param edgeCut The edge cut to print.
     *  @return The output.
     */
    public String edgeCutString(Collection edgeCut) {
        String out = new String();
        Iterator cuts = edgeCut.iterator();
        while (cuts.hasNext()) {
            Edge cut = (Edge)cuts.next();
            Node n1 = cut.source();
            Node n2 = cut.sink();
            int p1Index = getPartitions().indexOf(partitionOf(n1));
            int p2Index = getPartitions().indexOf(partitionOf(n2));
            out += "edge cut set :\n";
            out += "(node" + nodeLabel(n1) + ",\tvalue=" + valueOf(n1) +
                    ", part" + p1Index + ") : (" +
                    "node" + nodeLabel(n2) + ",\tvalue=" + valueOf(n2) +
                    ", part" + p2Index + ")\n";
        }
        return out;
    }

    /** Evaluate the total value of cut edges incident to the node.
     *  It is also called "external cost" in Kernighan and Lin's graph
     *  partitioning heuristic. See also {@link #edgeNonCutValueOf(Node)}.
     *  <p>
     *  Caution: It is assumed that all nodes are assigned with partitions
     *  before calling this method.
     *
     *  @param node The node to evaluate the edge cut value.
     *  @return Value of the edge cut.
     */
    public double edgeCutValueOf(Node node) {
        Collection edgeCut = edgeCut();
        Collection edgeCutOfTheNode = new ArrayList();
        Iterator incidentEdges = incidentEdges(node).iterator();
        while (incidentEdges.hasNext()) {
            Edge incidentEdge = (Edge)incidentEdges.next();
            if (edgeCut.contains(incidentEdge))
                edgeCutOfTheNode.add(incidentEdge);
        }
        return valueOf(edgeCutOfTheNode);
    }

    /** Evaluate the total value of non-cut edges incident to the node.
     *  It is also called "internal cost" in Kernighan and Lin's graph
     *  partitioning heuristic. See also {@link #edgeCutValueOf(Node)}.
     *  <p>
     *  Caution: It is assumed that all nodes are assigned with partitions
     *  before calling this method.
     *
     *  @param node The node to evaluate the non-cut edges' value.
     *  @return Value of the non-cut edges.
     */
    public double edgeNonCutValueOf(Node node) {
        return valueOf(incidentEdges(node)) - edgeCutValueOf(node);
    }

    /** Get all the partitions. The partition list returned is not allowed
     *  to modify. This is to ensure that partition additions are through
     *  {@link addPartition(GraphPartition)}.
     *  @return Present partitions in type of <code>List</code>.
     */
    public List getPartitions() {
        return Collections.unmodifiableList(_partitions);
    }

    /** The partition index in the list of partitions.
     *  @param element The graph element to get the belonged partition.
     *  @return The partition index.
     */
    public int partitionIndexOf(Element element) {
        return partitionOf(element).getIndex();
        // Equivalent to the following statement.
        // return _partitions.indexOf(partitionOf(element));
    }

    /** Get the partition index for the given partition.
     *  @param partition The graph partition.
     *  @return The partition index.
     */
    public int partitionIndexOf(GraphPartition partition) {
        return partition.getIndex();
        // Equivalent to the following statement.
        // return _partitions.indexOf(partition);
    }

    /** Get the partition for the given partition index.
     *  @param index The partition index.
     *  @return The partition.
     */
    public GraphPartition partitionOf(int index) {
        return (GraphPartition)_partitions.get(index);
    }

    /** Get the container partition of an element.
     *  @param element The given element.
     *  @return The container partition.
     */
    public GraphPartition partitionOf(Element element) {
        List partitions = getPartitions();
        for (int i = 0; i < partitions.size(); i++) {
            GraphPartition partition = (GraphPartition)partitions.get(i);
            if ( (element instanceof Node) &&
                    partition.containsNode((Node)element) ) {
                return partition;

            } else if ( (element instanceof Edge) &&
                    partition.containsEdge((Edge)element) ) {
                return partition;
            }
        }
        return null;
    }

    /** Get the assigned partitions of a given collection of elements.
     *  @param elementCollection The collection of elements.
     *  @return The assigned partitions.
     */
    public Collection partitionsOf(Collection elementCollection) {
        Set partitionSet = new HashSet();
        Iterator elements = elementCollection.iterator();
        while (elements.hasNext()) {
            Element element = (Element)elements.next();
            GraphPartition partition = partitionOf(element);
            if (partition != null)
                partitionSet.add(partition);
        }
        return partitionSet;
    }

    /** Display partitions in a text string.
     *  @return The output.
     */
    public String partitionString() {
        String out = new String();
        int partitionCounts = getPartitions().size();
        out += "partition counts = " + partitionCounts + "\n\n";
        for (int i = 0; i < partitionCounts; i++) {
            GraphPartition partition =
                    (GraphPartition)getPartitions().get(i);
            out += "partition " + i +
                    " of total value " + valueOf(partition) + "\n";
            Iterator nodes = partition.nodes().iterator();
            while (nodes.hasNext()) {
                Node node = (Node)nodes.next();
                out += "node" + nodeLabel(node) +
                        ",\tvalue=" + valueOf(node) + "\n";
            }
            out += "\n";
        }
        return out;
    }

    /** Remove all partitions.
     */
    public void resetPartitions() {
        _partitions = new ArrayList();
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    // The list of partitions.
    private List _partitions;

}


