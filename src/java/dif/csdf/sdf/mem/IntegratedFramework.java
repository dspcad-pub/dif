/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* The class for integrated memory management framework (minimization of
 bank capacity and maximization of parallel bank accesses).
*/

package dif.csdf.sdf.mem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import dif.util.graph.Edge;
import dif.util.graph.Node;
import dif.util.graph.mapping.ToIntMapMapping;
import dif.util.sched.Schedule;

import dif.csdf.sdf.SDFGraph;
import dif.csdf.sdf.sched.APGANStrategy;
import dif.csdf.sdf.sched.DPPOStrategy;
import dif.csdf.sdf.sched.GDPPOStrategy;
import dif.csdf.sdf.sched.SDPPOStrategy;

//////////////////////////////////////////////////////////////////////////
//// IntegratedFramework
/** Integrated algorithm framework for data memory management. The management
considers optimization problems of both capacity requirement and
parallel data accesses. The processor architecture of dual homogeneous
on-chip memory banks is considered here instead of arbitrary number of banks.
<p>
<em>Caution:</em> To get any optimal results, {@link #runOptimization(int)}
MUST be called first.

@author Mingyung Ko
@version $Id: IntegratedFramework.java 606 2008-10-08 16:29:47Z plishker $
*/

public class IntegratedFramework {

    /** A constructor with a conflict graph.
     *  @param conflictGraph The conflict graph.
     */
    public IntegratedFramework(ConflictGraph conflictGraph) {
        _conflictGraph = conflictGraph;
        _sdfGraph = conflictGraph.getSDFGraph();
        _lexicalOrder =
                (new APGANStrategy(_sdfGraph)).schedule().lexicalOrder();
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Run the optimization for the given number of iterations and the
     *  desired way to update information across iterations.
     *  This method must be called before any other public methods.
     *
     *  @param iterations The number of iterations.
     *  @param updateOption The iterative update option.
     *         The available options are: 0, no update; 1, add sharing
     *         conflicts; 2, consolidate shared buffers into single nodes.
     */
    public void runOptimization(int iterations, int updateOption) {
        _allocateArrays(iterations);
        _iterativeFramework(iterations, updateOption);

        // Spot the optimal results and the associated iteration.
        int capacity = Integer.MAX_VALUE;
        for (int i = 0; i < iterations; i++)
            if ((_capacities[i] < capacity) && _optimalParallelism[i])
                capacity = _capacities[(_optimalIteration = i)];
    }

    /** The optimal (minimal) bank capacity requirement after running
     *  the given number of iterations. Please call
     *  {@link #runOptimization(int)} first.
     *
     *  @return The optimal bank capacity requirement.
     */
    public int optimalBankCapacity() {
        return _capacities[_optimalIteration];
    }

    /** The optimal partitioning (bank assignment) results after running
     *  the given number of iterations. Please call
     *  {@link #runOptimization(int)} first.
     *
     *  @return The optimal partitioning results.
     */
    public String optimalPartitionString() {
        return _partitionStrings[_optimalIteration];
    }

    /** The optimal scheduling result (schedule) after running
     *  the given number of iterations. Please call
     *  {@link #runOptimization(int)} first.
     *
     *  @return The optimal schedule.
     */
    public Schedule optimalSchedule() {
        return _optimalSchedules[_optimalIteration];
    }

    /** A complete description of the results.
     *  @return The results in <code>String</code>.
     */
    public String toString() {
        String results = new String();
        results += "Bank capacity requirement = "
                + optimalBankCapacity() + "\n";
        results += "Best iterative schedulers (0=GDPPO 1=SDPPO 2=BDPPO) : ";
        for (int i = 0; i < _presentIteration; i++)
            results += _optimalSchedulers[i] + " ";
        results += "\n";
        results += unOptimizedResults();

        return results;
    }

    /** Display of any un-optimized results. This is useful in debugging.
     *  @return Un-optimized results expressed in text.
     */
    public String unOptimizedResults() {
        String result = "Iterations with non-maximized parallelism: ";
        for (int i = 0; i < _optimalParallelism.length; i++)
            if (_optimalParallelism[i] == false)
                result += i + " ";
        result += "\n";

        return result;
    }

    ///////////////////////////////////////////////////////////////////
    ////      protected methods (main functions of each phase)     ////

    /** The body of the iterative algorithm framework.
     *  @param iterations The number of iterations to run the optimization.
     *  @param updateOption The iterative update option.
     *         The available options are: 0, no update; 1, add sharing
     *         conflicts; 2, consolidate shared buffers into single nodes.
     */
    public void _iterativeFramework(int iterations, int updateOption) {
        _newConflicts = new ArrayList();

        // The iterative approach.
        for (_presentIteration = 0;
                _presentIteration < iterations; _presentIteration++) {

            _partition();
            _cachePartitionResults();
            _conflictGraph.removeSharingConflicts();

            _schedule();
            _share();
            _wrapUpIteration(updateOption);
        }
    }

    /** The data partitioning phase. Determine bank assignment for variables.
     *  Both the input and output are stored in {@link #_conflictGraph}.
     */
    protected void _partition() {
	try {  //WLP - Not sure what this is all about
	    (new DataPartitioning(_conflictGraph)).sharedSPFStrategy();
	} catch(DataPartitioningException dpe) {
	}
    }

    /** The SDF scheduling phase. Determine SDF buffer sizes via scheduling
     *  techniques. Multiple schedulers are employed to get best results.
     *  The input data includes {@link #_sdfGraph},
     *  {@link #_lexicalOrder}, and probably a few partitioning results like
     *  {@link #_sEdges0} and {@link #_sEdges1}. The output result is in
     *  {@link #_schedules}.
     *
     *  @param presentIteration The present iteration number.
     */
    protected void _schedule() {
        _schedules = new ArrayList();
        _schedules.add(
                (new GDPPOStrategy(_sdfGraph, _lexicalOrder)).schedule());
        _schedules.add(
                (new SDPPOStrategy(_sdfGraph, _lexicalOrder)).schedule());
        _schedules.add(
                (new BDPPOStrategy(_sdfGraph, _lexicalOrder)).schedule());
    }

    /** The physical memory space sharing phase for SDF buffers. The input data
     *  includes {@link #_sdfGraph}, {@link #_schedules}, and
     *  {@link #_sEdges0}, {@link #_sEdges1} (which can be induced from
     *  {@link #_conflictGraph}).
     *  Output results are saved in {@link #_bufferSizes},
     *  {@link #_intersections}, {@link #_capacity0}, and {@link #_capacity1}.
     *  Optimal results in the present iteration are also saved simultaneously.
     *  They are {@link _capacities}, {@link_optimalSchedules}, and
     *  {@link _partitionStrings}.
     */
    protected void _share() {
        _capacity = Integer.MAX_VALUE;

        // Go through all schedules to select the one with smallest
        // bank capacity requirement.
        for (int i = 0; i < _schedules.size(); i++) {
            Schedule schedule = (Schedule)_schedules.get(i);
            List shareResults = _shareResults(schedule);
            int capacity = ((Integer)shareResults.get(0)).intValue();
            if (capacity < _capacity) {
                _capacity = capacity;
                _optimalSchedulers[_presentIteration] = i;
                // Variables below are used to update the conflict graph
                // for the next iteration.
                _bufferSizes = (ToIntMapMapping)shareResults.get(1);
                _intersections = (Map)shareResults.get(2);
            }
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////        protected methods (utilities for each phase)       ////

    /** Cache some useful local partitioning results that will be referenced
     *  frequently. Examples are {@link #cPart0}, {@link #cPart1},
     *  {@link #_sEdges0}, {@link #_sEdges1}, {@link #_capacity0}, and
     *  {@link #_capacity1}.
     */
    protected void _cachePartitionResults() {
        GraphPartition part0 =
                (GraphPartition)_conflictGraph.getPartitions().get(0);
        GraphPartition part1 =
                (GraphPartition)_conflictGraph.getPartitions().get(1);
        _sEdges0 = _conflictGraph.getSDFEdgesOf(part0);
        _sEdges1 = _conflictGraph.getSDFEdgesOf(part1);
        // Total state variable space cost of each bank.
        _stateVariableCost0 = (int)_conflictGraph.stateVariableCost(part0);
        _stateVariableCost1 = (int)_conflictGraph.stateVariableCost(part1);
    }

    /** Sharing results with the given schedule. The input data
     *  includes {@link #_sdfGraph} and {@link #_sEdges0},
     *  {@link #_sEdges1} (which can be induced from {@link #_conflictGraph}).
     *  Output results are saved in a <code>List</code> of bank capacity
     *  requirement (in <code>Integer</code>), SDF buffer sizes
     *  (in <code>ToIntMapMapping</code>), and SDF buffer lifetime
     *  intersections (in <code>Map</code>).
     *
     *  @param schedule The schedule for lifetime analysis.
     *  @return The bank capacity requirement, SDF buffer sizes, and SDF buffer
     *          lifetime intersections.
     */
    protected List _shareResults(Schedule schedule) {
        BufferSharing share = new BufferSharing(_sdfGraph, schedule);

        // Compute individual SDF buffer sizes.
        ToIntMapMapping sizeMapping =
                BufferUtilities.bufferSizeMapping(_sdfGraph, schedule);
        // Compute buffer lifetime intersections.
        Map intersections = share.bufferIntersections();

        // Enumerate buffers by their access start times. The
        // enumeration is needed for both banks.
        List allEnumeration = share.enumerateBuffersByStart();
        List enumeration0 = new ArrayList();
        List enumeration1 = new ArrayList();
        for (int i = 0; i < allEnumeration.size(); i++) {
            Object buffer = allEnumeration.get(i);
            if (_sEdges0.contains(buffer))
                enumeration0.add(buffer);
            else if (_sEdges1.contains(buffer))
                enumeration1.add(buffer);
            else
                throw new RuntimeException("Buffer enumeration error.");
        }

        // Compute capacity requirement for both banks. The capacity comprises
        // state variable size (non-sharable) and SDF buffer size (sharable).
        Map sizeMap = new HashMap();
        Iterator keys = _sdfGraph.edges().iterator();
        while (keys.hasNext()) {
            Object key = keys.next();
            sizeMap.put(key, sizeMapping.toObject(key));
        }
        int capacity0 = _stateVariableCost0 +
                BufferSharing.firstFit(enumeration0, intersections, sizeMap);
        int capacity1 = _stateVariableCost1 +
                BufferSharing.firstFit(enumeration1, intersections, sizeMap);

        List results = new ArrayList();
        results.add(new Integer(Math.max(capacity0, capacity1)));
        results.add(sizeMapping);
        results.add(intersections);
        return results;
    }

    /** Collect sharing conflicts to add in the conflict graph. This method
     *  requires {@link #_intersections} as input.
     *
     *  @return The sharing conflicts (in <code>Collection</code>) to add.
     */
    protected Collection _collectSharingConflicts() {
        Collection sharingConflicts = new ArrayList();
        Iterator sdfEdges = _sdfGraph.edges().iterator();
        while (sdfEdges.hasNext()) {
            Edge sdfEdge = (Edge)sdfEdges.next();
            Iterator conflictSdfEdges =
                    ((Collection)_intersections.get(sdfEdge)).iterator();
            while (conflictSdfEdges.hasNext()) {
                Edge conflictSdfEdge = (Edge)conflictSdfEdges.next();

                // Add sharing conflicts to the conflict graph.
                Node bufferA = _conflictGraph.getSDFBufferOf(sdfEdge);
                Node bufferB = _conflictGraph.getSDFBufferOf(conflictSdfEdge);
                if (_conflictGraph.neighborEdges(bufferA, bufferB).isEmpty())
                    sharingConflicts.add(new Edge(bufferA, bufferB));
            }
        }
        return sharingConflicts;
    }

    /** Allocate arrays for caching iterative results.
     *  @param iterations The number of iterations.
     */
    protected void _allocateArrays(int iterations) {
        _capacities         = new int     [iterations];
        _optimalSchedules   = new Schedule[iterations];
        _optimalSchedulers  = new int     [iterations];
        _partitionStrings   = new String  [iterations];
        _optimalParallelism = new boolean [iterations];
    }

    /** Incorporate sharing conflicts to the conflict graph.
     *  @param conflicts The sharing conflicts in <code>Collection</code>.
     */
    protected void _incorporateSharingConflicts(Collection conflicts) {
        Iterator conflictIterator = conflicts.iterator();
        while (conflictIterator.hasNext()) {
            Edge conflict = (Edge)conflictIterator.next();
            _conflictGraph.addEdge(conflict);
            _conflictGraph.setSharingConflict(conflict);
        }
    }

    /** Wrap up the results got in the present iteration and prepare
     *  feedback to the next iteration. The actions include:
     *  updating SDF buffer cost to the conflict graph, record the optimal
     *  capacity/schedule/partitions got in the present iteration, and the
     *  way to pass updated information to the next iteration.
     *
     *  @param updateOption The iterative update option.
     *         The available options are: 0, no update; 1, add sharing
     *         conflicts; 2, consolidate shared buffers into single nodes.
     */
    protected void _wrapUpIteration(int updateOption) {
        // Save optimial results got from the present iteration.
        _conflictGraph.updateSDFBufferCost(_bufferSizes);
        _capacities[_presentIteration] = _capacity;
        _optimalSchedules[_presentIteration] = _schedule;
        _partitionStrings[_presentIteration] =
                _conflictGraph.partitionString();
        _optimalParallelism[_presentIteration] =
                _conflictGraph.isParallelismMaximized();
        // How to pass lifetime overlaps information to the next iteration.
        switch (updateOption) {
            // Add sharing conflicts.
            case 1:
                _incorporateSharingConflicts(_collectSharingConflicts());
                break;
            // Consolidate shared buffers into single nodes.
            case 2:

                break;
            // No changes are made to the conflict graph.
            case 0:
            default:
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////                      protected variables                  ////

    //// Variables below don't change their values in any phase
    //// or iteration. They always stay unchanged.

    /** The SDF graph. */
    protected SDFGraph _sdfGraph;

    /** The lexical order required in DPPO scheduling technique. */
    protected List _lexicalOrder;

    //// Variables below are to keep results of the present iteration.

    /** The present iteration. */
    protected int _presentIteration;

    /** The conflict graph. */
    protected ConflictGraph _conflictGraph;

    /** The optimal schedule leads to minimal capacity requirement.
     *  It is updated in the invocation of {@link #_share()}.
     */
    protected Schedule _schedule;
    /** Multiple schedules can be generated in the scheduling phase. */
    protected List _schedules;

    /** SDF buffer sizes. */
    protected ToIntMapMapping _bufferSizes;

    /** Lifetime intersections of buffers. */
    protected Map _intersections;

    /** Partitions of SDF edges associated with the conflict graph. */
    protected Collection _sEdges0, _sEdges1;

    /** With multiple schedules being examined, the best capacity
     *  requirement is saved to this variable in the invocation
     *  of {@link #_share()}.
     */
    protected int _capacity;

    /** State variable cost according to the present bank assignment. */
    protected int _stateVariableCost0, _stateVariableCost1;

    /** New (sharing) conflicts to add in the conflict graph. */
    protected Collection _newConflicts;

    //// Variables to maintain optimal results in ALL iterations.
    //// Array elements are indexed by iteration.

    /** Iteration of the optimal results (bank capacity requirement). */
    protected int _optimalIteration;

    /** Optimal capacity requirement in every iteration. */
    protected int[] _capacities;

    /** Optimal bank assignments in every iteration. */
    protected String[] _partitionStrings;

    /** Optimal schedules in every iteration. */
    protected Schedule[] _optimalSchedules;

    /** Optimal scheduling algorithms in every iteration. */
    protected int[] _optimalSchedulers;

    /** Check whether the partitioning results in optimal parallelism. */
    protected boolean[] _optimalParallelism;

    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    ///////////////////////////////////////////////////////////////////
    ////                        private classes                    ////

    /*  The DPPO of cost criteria of bank capacity difference. The
     *  optimization aims at balanced capacity requirement for dual banks.
     */
    private class BDPPOStrategy extends DPPOStrategy {
        /* Constructor. */
        public BDPPOStrategy(SDFGraph graph, List order) {
            super(graph, order);
            _gap = _stateVariableCost0 - _stateVariableCost1;
        }

        /* Optimum results of the sub-list indexed from i to j. */
        protected void _optimumFor(int i, int j) {
            double optimalCost = 99999.0;
            int optimalSplit = i;
            int gcd = _DPPOTableElement(i, j).gcd;

            for (int split = i; split < j; split++) {
                double cost = _DPPOTableElement(i, split).cost +
                        _DPPOTableElement(split + 1, j).cost;
                Iterator cEdges = _crossingSDFEdges(i, j, split).iterator();
                while (cEdges.hasNext()) {
                    Edge cEdge = (Edge)cEdges.next();
                    double bufferSize = _bufferCost(cEdge, i, j);
                    if (_sEdges0.contains(cEdge))
                        cost += bufferSize;
                    else if (_sEdges1.contains(cEdge))
                        cost -= bufferSize;
                    else
                        throw new RuntimeException("SDF edges are not "
                                + "partitioned properly");
                }
                // Compare absolute memory usage difference.
                if (Math.abs(cost + _gap) < Math.abs(optimalCost + _gap)) {
                    optimalCost = cost;
                    optimalSplit = split;
                }
            }
            _DPPOTableElement(i, j).cost = optimalCost;
            _DPPOTableElement(i, j).split = optimalSplit;
        }

        /* Bank capacity gap from partitioning results. */
        private double _gap;
    }
}


