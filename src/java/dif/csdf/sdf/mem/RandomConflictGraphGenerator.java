/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Random graph generator in generating random conflict graphs. */

package dif.csdf.sdf.mem;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import dif.csdf.sdf.SDFGraph;
import dif.csdf.sdf.SDFRandomGraphGenerator;
import dif.util.graph.Edge;
import dif.util.graph.Node;

//////////////////////////////////////////////////////////////////////////
//// RandomConflictGraphGenerator
/**
Random graph generator in generating random conflict graphs.
Actualy, it is the SDF graph that is randomly generated. When the SDF graph
is obtained, partitioning conflicts are synthesized between SDF buffers.
The synthesis strategy is to chain all input buffers for each SDF actor.
No state variables are generated here and all nodes represent SDF buffers only.

@author Mingyung Ko
@version $Id: RandomConflictGraphGenerator.java 606 2008-10-08 16:29:47Z plishker $
*/

public class RandomConflictGraphGenerator {

    /** A constructor without arguments.
     */
    public RandomConflictGraphGenerator() {
        _SDFGenerator = new SDFRandomGraphGenerator();
        _isRandomSDFSet = false;
    }

    ///////////////////////////////////////////////////////////////////
    ////                        public methods                     ////

    /** Generate a random conflict graph.
     *  @return The random conflict graph.
     */
    public ConflictGraph graph() {
        if (_isRandomSDFSet == false)
            throw new RuntimeException(
                    "Parameters of random SDF graph generation "
                    + "must be set first.\n");
        // Each call to graph() will generate a new SDF graph.
        _savedSDFGraph =
                (SDFGraph)_SDFGenerator.graph(_minSDFNodes, _maxSDFNodes);
        return graph(_savedSDFGraph);
    }

    /** Generate a random conflict graph with the associated SDF graph.
     *  @param The associated SDF graph.
     *  @return The random conflict graph.
     */
    static public ConflictGraph graph(SDFGraph sGraph) {
        ConflictGraph graph = new ConflictGraph();
        graph.setSDFGraph(sGraph);
        // Synthesize partitioning conflict edges chaining incoming
        // buffers per SDF actor.
        _synthesizePartitioningConflicts(graph, sGraph);
        return graph;
    }

    /** Get a conflict graph from the SDF graph previously generated. This is
     *  used when random SDF graph generataion is not desired.
     */
    public ConflictGraph graphFromSavedSDFGraph() {
        if (_savedSDFGraph == null)
            throw new RuntimeException(
                    "No SDF graph previously saved.\n");
        return graph(_savedSDFGraph);
    }

    /** Generate the desired number of random conflict graph.
     *  @param graphCount The desired graph count.
     *  @return The random conflict graphs.
     */
    public ConflictGraph[] graphs(int graphCount) {
        ConflictGraph[] conflictGraphs = new ConflictGraph[graphCount];
        for (int i = 0; i < graphCount; i++)
            conflictGraphs[i] = graph();
        return conflictGraphs;
    }

    /** Set parameter values to make the SDF random graph.
     */
    public void setSDFRandomGraphParameters(
            int oneProb, int maxRate, int minNodes, int maxNodes) {
	    _SDFGenerator.setOneProbability(oneProb);
		_SDFGenerator.setMaxRate(maxRate);
		_minSDFNodes = minNodes;
		_maxSDFNodes = maxNodes;
		// Mark the SDF randomization flag.
        _isRandomSDFSet = true;
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////


    ///////////////////////////////////////////////////////////////////
    ////                       private methods                    ////

    /*  Synthesize partitioning conflict edges chaining SDF buffers.
     *  @param cGraph The conflict graph where edges are synthesized.
     *  @param sGraph The associated SDF graph.
     */
    static private void _synthesizePartitioningConflicts(
            ConflictGraph cGraph, SDFGraph sGraph) {
        Iterator SDFNodes = sGraph.nodes().iterator();
        while (SDFNodes.hasNext()) {
            Node SDFNode = (Node)SDFNodes.next();
            List inputSDFEdges = new ArrayList(sGraph.inputEdges(SDFNode));
            if (inputSDFEdges.size() > 1)
                for (int i = 0; i < inputSDFEdges.size() - 1; i++) {
                    Node buffer1 = cGraph.getSDFBufferOf(
                            (Edge)inputSDFEdges.get(i));
                    Node buffer2 = cGraph.getSDFBufferOf(
                            (Edge)inputSDFEdges.get(i + 1));
                    Edge edge = cGraph.addEdge(buffer1, buffer2);
                    cGraph.setPartitioningConflict(edge);
                }
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    // A flag to check whether SDF randomization has been set.
    private boolean _isRandomSDFSet;

    // SDF random graph generator.
    private SDFRandomGraphGenerator _SDFGenerator;

    // The min, max SDF nodes.
    private int _minSDFNodes, _maxSDFNodes;

    // Save the current SDF graph to make conflict graphs with identical
    // structure. Otherwise, SDF graphs will be generated randomly every time.
    private SDFGraph _savedSDFGraph;
}


