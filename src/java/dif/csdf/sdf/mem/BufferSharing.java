/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Buffer sharing for SDF graphs with given schedules. */

package dif.csdf.sdf.mem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import dif.util.graph.Edge;
import dif.util.graph.GraphTopologyException;
import dif.util.sched.Schedule;
import dif.csdf.sdf.SDFGraph;
import dif.csdf.sdf.sched.ScheduleTree;
import dif.csdf.sdf.sched.ScheduleTreeNode;

//////////////////////////////////////////////////////////////////////////
//// BufferSharing
/** Communication buffers can share physical memory spaces if their
lifetimes do not overlap. The purpose of sharing is to reduce the total
space cost.
<p>
This class implements the algorithm proposed in <em>Shared Buffer
Implementations of Signal Processing Systems Using Lifetime Analysis
Techniques</em> by Praveen K. Murthy and Shuvra S. Bhattcharyya.
In their algorithm, lifetime analysis is derived from an SDF schedule.
The periodical behavior presented in SDF schedule enables efficient
analysis on lifetimes. After that, the actual storage allocation is
done by dynamic storage allocation (DSA) algorithms, e.g., first-fit.
<p>
CAUTION: The buffer lifetime analysis in the above algorithm assumes the
input graph to be ACYCLIC. If not, an exception will be thrown.

@author Mingyung Ko
@version $Id: BufferSharing.java 606 2008-10-08 16:29:47Z plishker $
*/


public class BufferSharing {

    /** Constructor for given graph.
     *  @param graph The given SDF graph
     */
    public BufferSharing(SDFGraph graph, Schedule schedule) {
        if (!(_graph = graph).isAcyclic())
            throw new GraphTopologyException("Acyclic graph required.");
        _schedule = schedule;

        // Schedule tree, not the schedule itself, is sufficient in the
        // sharing algorithm.
        _scheduleTree = new ScheduleTree(schedule);
        _computeBufferTimes();
        // The default buffer enumeration.
        enumerateBuffersByStart();
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Get the total number of buffers.
     *  @return Total number of buffers.
     */
    public int bufferCount() {
        return _graph.edgeCount();
    }

    /** Compute buffer intersections. The result is returned as a map
     *  from edges to their intersecting edges (in <code>Set</code>).
     *  @return A map from edges to the set of edges intersecting with them.
     */
    public Map bufferIntersections() {
        return BufferUtilities.bufferIntersections(_graph, _schedule);

        /*------------------------------------------
        //  FIXME: There are bugs.

        // Create an empty intersection map
        Map intersections = new HashMap();
        for (int i = 0; i < bufferCount(); i++)
            intersections.put(_graph.edge(i), new HashSet());

        // Check intersections over all pairs of edges/buffers
        for (int i = 0; i < bufferCount(); i++) {
            Edge bufferI = (Edge)_bufferEnumeration.get(i);
            int  labelI  = _graph.edgeLabel(bufferI);
            Set  intersectionI = (Set)intersections.get(bufferI);

            for (int j = i + 1; j < bufferCount(); j++) {
                Edge bufferJ = (Edge)_bufferEnumeration.get(j);
                int  labelJ  = _graph.edgeLabel(bufferJ);
                Set intersectionJ = (Set)intersections.get(bufferJ);

                if (_buffersIntersect(bufferI, bufferJ)) {
                    intersectionI.add(bufferJ);
                    intersectionJ.add(bufferI);
                }
            }
        }
        return intersections;
        -------------------------------------------- */
    }

    /** Configure the internal buffer enumeration by durations. The
     *  result enumeration is also returned.
     *  @return The result enumeration.
     */
    public List enumerateBuffersByDuration() {
        _bufferEnumerationObsolete = true;
        _bufferEnumerationOption = _bufferEnumerationDuration;

        _bufferEnumeration = new ArrayList();
        Arrays.sort(_bufferTimes, new DurationComparator());
        for (int i = 0; i < bufferCount(); i++)
            _bufferEnumeration.add(_bufferTimes[i].graphEdge);
        return _bufferEnumeration;
    }

    /** Configure the internal buffer enumeration by start times
     *  (default enumeration). The result enumeration is also returned.
     *  @return The result enumeration.
     */
    public List enumerateBuffersByStart() {
        _bufferEnumerationObsolete = true;
        _bufferEnumerationOption = _bufferEnumerationStart;

        _bufferEnumeration = new ArrayList();
        Arrays.sort(_bufferTimes, new StartComparator());
        for (int i = 0; i < bufferCount(); i++)
            _bufferEnumeration.add(_bufferTimes[i].graphEdge);
        return _bufferEnumeration;
    }

    /** First-fit algorithm for Dynamic Storage Allocation (DSA).
     *  The capacity needed to allocate all shared storage is returned.
     *  Though the method is used in this class for SDF graph buffer
     *  sharing, it's designed for general purpose.
     *
     *  @param enumeration Enumeration over storages.
     *  @param intersection Storage intersections/lifetime overlaps.
     *  @param sizeMap A map from storage to Integer objects with integer
     *                 valued storage size.
     *  @return Capacity needed for first-fit.
     */
    static public int firstFit(
            List enumeration, Map intersections, Map sizeMap) {
        // Transform sizeMap into an integer array.
        int enumerationSize = enumeration.size();
        int[] size = new int[enumerationSize];

        // Initialize _bufferAllocations
        //_bufferAddresses = new int[enumerationSize];
        _bufferAddresses = new HashMap();

        for (int i = 0; i < enumerationSize; i++)
            size[i] = ((Integer)sizeMap.get(enumeration.get(i))).intValue();

        int capacity = 0;
        int[] allocate = new int[enumerationSize];

        // for each enumeration instance
        for (int i = 0; i < enumerationSize; i++) {
            allocate[i] = 0;
            Object buffer = enumeration.get(i);
            Set neighbors = (Set)intersections.get(buffer);
            ArrayList neighborsAllocated = new ArrayList();

            // collect sorted neighbors
            for (int j = 0; j < i; j++) {
                Object previousBuffer = enumeration.get(j);
                if (neighbors.contains(previousBuffer))
                    neighborsAllocated.add(previousBuffer);
            }

            // sort allocated buffers according to their memory addresses
            /*List sortedNeighborsAllocated = new ArrayList(enumeration);
            Collections.sort(sortedNeighborsAllocated,
                    new UpBuffers(enumeration, allocate));
            sortedNeighborsAllocated.retainAll(neighborsAllocated);*/

            List sortedNeighborsAllocated = new ArrayList(neighborsAllocated);
            Collections.sort(sortedNeighborsAllocated,
                    new AddressComparator(_bufferAddresses));

            // search the first fitting space
            for (int j = 0; j < sortedNeighborsAllocated.size(); j++) {
                int a = enumeration.indexOf(sortedNeighborsAllocated.get(j));
                // if 'i' & 'a' have conflicts, jump to the end of 'a'
                if ( (allocate[i] < allocate[a] + size[a])
                        && (allocate[a] < allocate[i] + size[i]) ) {
                    allocate[i] = allocate[a] + size[a];
                }
            }

            // compute the capacity needed
            int boundary = allocate[i] + size[i];
            if (boundary > capacity) {
                capacity = boundary;
            }

            // store buffer addresses
            //_bufferAddresses[i] = allocate[i];
            _bufferAddresses.put(enumeration.get(i), new Integer(allocate[i]));
        }
        return capacity;
    }

    /** Get buffer start addresses after calling {@link firstFit} to
     *  allocate buffers.
     *  End address of edge <i>e</i> = start address <i>e</i> + size <i>e</i>
     *  -1.
     *  @return An int[] stores the start address of every edge.
     *  The index of int[] is the index to access the edge from the graph.
     */
    //static public int[] getBufferAddresses() {
    static public Map getBufferAddresses() {
        if (_bufferAddresses == null) {
            throw new RuntimeException("Call firstFit first.");
        }
        return _bufferAddresses;
    }

    /** The space allocated to buffer sharing. It's the also the cost
     *  of dynamic storage allocation(DSA).
     *  @return Buffer sharing cost.
     */
    public int sharingCost() {
        return _computeBufferSharing();
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /** Get repetition counts for the given buffer.
     *  @param edge The given buffer's associated edge.
     *  @return The repetition counts.
     */
    protected int _bufferRepetition(Edge edge) {
        ScheduleTreeNode sourceLeaf = _scheduleTree.leafNode(edge.source());
        ScheduleTreeNode sinkLeaf = _scheduleTree.leafNode(edge.sink());
        List parents = _scheduleTree.parents(sourceLeaf, sinkLeaf);
        int product = 1;
        for (int i = 0; i < parents.size(); i++)
            product *= ((ScheduleTreeNode)parents.get(i)).loopCount;
        return product;
    }

    /** Evaluate buffer sharing and compute total buffer size needed.
     *  This is the computation method that should be overrided by
     *  descendant classes.
     *  @return An Integer object with value of buffer size.
     */
    protected int _computeBufferSharing() {
        // Compute buffers' size and save them to a map.
        _bufferSize = BufferUtilities.bufferSize(_graph, _schedule);
        Map sizeMap = new HashMap();
        for (int i = 0; i < bufferCount(); i++) {
            sizeMap.put(_graph.edge(i), new Integer(_bufferSize[i]));
        }
        int capacity = firstFit(
                _bufferEnumeration, bufferIntersections(), sizeMap);
        return capacity;
    }

    ///////////////////////////////////////////////////////////////////
    ////                      protected variables                  ////

    /** The associated SDF graph. */
    protected SDFGraph _graph;

    ///////////////////////////////////////////////////////////////////
    ////                      private methods                      ////

    /*  The first start/alive time for a given periodic buffer.
     *  @param edge The given buffer's associated edge.
     *  @return The first start time.
     */
    private int _bufferFirstStartTime(Edge edge) {
        return _scheduleTree.leafNode(edge.source()).start;
    }

    /*  The first stop/dead time for a given periodic buffer. The time
     *  is used for computing duration(of single period) of buffers.
     *  @param edge The given buffer's associated edge.
     *  @return The first stop time.
     */
    private int _bufferFirstStopTime(Edge edge) {
        ScheduleTreeNode leftLeaf  = _scheduleTree.leafNode(edge.source());
        ScheduleTreeNode rightLeaf = _scheduleTree.leafNode(edge.sink());

        // Compute the first stop time.
        ScheduleTreeNode leastParent =
                _scheduleTree.leastParent(leftLeaf, rightLeaf);
        int bufferStop = leastParent.rightChild().stop;
        ScheduleTreeNode traceNode = rightLeaf;
        while (traceNode != leastParent.rightChild()) {
            if (traceNode == traceNode.parent.leftChild())
                bufferStop -= traceNode.parent.rightChild().duration;
            traceNode = traceNode.parent;
        }

        return bufferStop;
    }

    /*  Test whether the given pair of buffers' lifetimes overlap.
     *  @param edge1 The first buffer's associated edge.
     *  @param edge2 The second buffer's associated edge.
     *  @return True if buffers' lifetimes overlap.
     */
    private boolean _buffersIntersect(Edge edge1, Edge edge2) {
        // Determine the early and the late starting buffers.
        Edge[] edges = new Edge[2];
        int edge1Label = _graph.edgeLabel(edge1);
        int edge2Label = _graph.edgeLabel(edge2);

        // Check intersection for both buffers' first occurance.
        if ((_bufferTimes[edge1Label].firstStart <
                _bufferTimes[edge2Label].firstStop)
                && (_bufferTimes[edge2Label].firstStart <
                _bufferTimes[edge1Label].firstStop))
            return true;

        if (_bufferTimes[edge1Label].firstStart <
                _bufferTimes[edge2Label].firstStart) {
            edges[0] = edge1;
            edges[1] = edge2;
        } else {
            edges[0] = edge2;
            edges[1] = edge1;
        }

        int earlyBufferLabel = _graph.edgeLabel(edges[0]);
        int lateBufferLabel  = _graph.edgeLabel(edges[1]);

        int lateBufferStart     = _bufferTimes[lateBufferLabel].firstStart;
        int lateBufferStop      = _bufferTimes[lateBufferLabel].firstStop;
        int lateBufferDuration  = _bufferTimes[lateBufferLabel].duration;
        int earlyBufferDuration = _bufferTimes[earlyBufferLabel].duration;

        int startRemaining = _sinceLatestStart(edges[0], lateBufferStart);
        int stopRemaining  = _sinceLatestStart(edges[0], lateBufferStop);
        boolean intersect = false;
        if ((startRemaining < earlyBufferDuration) ||
                ((0 < stopRemaining) && (stopRemaining < lateBufferDuration))) {
            intersect = true;
        }

        return intersect;
    }

    /*  Compute all times (start, stop, and duration) for all buffers.
     */
    private void _computeBufferTimes() {
        _bufferTimes = new BufferTime[bufferCount()];

        for (int i = 0; i < bufferCount(); i++) {
            Edge edge = _graph.edge(i);
            _bufferTimes[i] = new BufferTime();
            _bufferTimes[i].graphEdge  = edge;
            _bufferTimes[i].firstStart = _bufferFirstStartTime(edge);
            _bufferTimes[i].firstStop  = _bufferFirstStopTime(edge);
            _bufferTimes[i].duration   =
                    _bufferTimes[i].firstStop - _bufferTimes[i].firstStart;
        }
    }

    /*  For a given time, compute the remaining time since the latest
     *  start time of a buffer. This method is quite useful in testing
     *  buffer lifetime overlaps.
     *  @param edge The given buffer's associated edge.
     *  @param time The given time stamp.
     *  @return The remaining time since the latest start.
     */
    private int _sinceLatestStart(Edge edge, int time) {
        ScheduleTreeNode leftLeaf  = _scheduleTree.leafNode(edge.source());
        ScheduleTreeNode rightLeaf = _scheduleTree.leafNode(edge.sink());

        // Iteratively go through all parents' loops to figure
        // out the time remained.
        List parents = _scheduleTree.parents(leftLeaf, rightLeaf);
        int traceTime = time - ((ScheduleTreeNode)parents.get(0)).start;
        for (int i = parents.size() - 1; i >= 0; i--) {
            ScheduleTreeNode parent = (ScheduleTreeNode)parents.get(i);
            int onePeriod = parent.duration / parent.loopCount;
            int realLoops = traceTime / onePeriod;
            int legalPeriods = Math.min(realLoops, parent.loopCount - 1);

            traceTime -= legalPeriods * onePeriod;
        }

        return traceTime;
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    /*  Buffer enumeration. For example, enumeration over
     *  buffer's start time, duration.
     */
    private List _bufferEnumeration;

    /* A record of enumeration option */
    private int _bufferEnumerationOption;

    /* Possible options for buffer enumeration */
    private int _bufferEnumerationStart    = 0;
    private int _bufferEnumerationDuration = 1;

    /* Obsolete flag for _bufferEnumeration */
    private boolean _bufferEnumerationObsolete;

    /* Individual buffer's size and total size. */
    private int[] _bufferSize;

    /* Buffer intersections(lifetime overlaps) */
    private Map _bufferIntersections;

    /* A schedule tree for computation of buffer intersections. */
    private ScheduleTree _scheduleTree;

    /* The associated schedule. */
    private Schedule _schedule;

    /* An array of various buffer times. */
    private BufferTime[] _bufferTimes;

    /* An int[] stores the start address of every edge. */
    //static private int[] _bufferAddresses;
    static private HashMap _bufferAddresses;

    /* A class definition for the collection of buffer times. */
    private class BufferTime {
        public Edge graphEdge;
        public int  firstStart;
        public int  firstStop;
        public int  duration;
    }

    /* Comparator (increasing) for buffer allocation addresses. */
    /*static private class UpBuffers implements Comparator {
        public UpBuffers(List buffers, int[] addresses) {
            _buffers = buffers;
            _addresses = addresses;
        }
        public int compare(Object b1, Object b2) {
            int address1 = _addresses[_buffers.indexOf(b1)];
            int address2 = _addresses[_buffers.indexOf(b2)];
            return (address1 - address2);
        }
        private List _buffers;
        private int[] _addresses;
    }*/

    static private class AddressComparator implements Comparator {
        public AddressComparator(Map bufferAddress) {
            _addresses = bufferAddress;
        }
        public int compare(Object b1, Object b2) {
            int address1 = ((Integer)_addresses.get(b1)).intValue();
            int address2 = ((Integer)_addresses.get(b2)).intValue();
            int result = address1 - address2;
            // Force absolute comparison result.
            if (result == 0)
                result = 1;
            return result;
        }
        private Map _addresses;
    }

    /* Comparator for BufferTime.duration */
    private class DurationComparator implements Comparator {
        public int compare(Object time1, Object time2) {
            int duration1 = ((BufferTime)time1).duration;
            int duration2 = ((BufferTime)time2).duration;
            int result = duration1 - duration2;
            // Force absolute comparison result.
            if (result == 0)
                result = 1;
            return result;
        }
    }

    /* Comparator for BufferTime.firstStart */
    private class StartComparator implements Comparator {
        public int compare(Object time1, Object time2) {
            int start1 = ((BufferTime)time1).firstStart;
            int start2 = ((BufferTime)time2).firstStart;
            int result = start1 - start2;
            // Force absolute comparison result.
            if (result == 0)
                result = 1;
            return result;
        }
    }
}


