/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Information associated with an SDF edge. */

package dif.csdf.sdf;

import dif.csdf.CSDFEdgeWeight;

import dif.DIFEdgeWeight;
import dif.util.graph.Edge;
import dif.util.graph.Graph;
import dif.util.graph.GraphWeightException;

//////////////////////////////////////////////////////////////////////////
//// SDFEdgeWeight
/** Information associated with an SDF edge.
SDFEdgeWeights are objects associated with {@link Edge}s
that represent SDF edges in {@link Graph}s.
This class caches frequently-used data associated with SDF edges.
It is also useful for intermediate SDF graph representations
that do not correspond to Ptolemy II models, and performing graph
transformations (e.g., vectorization and retiming).
It is intended for use with analysis/synthesis algorithms that operate
on generic graph representations of SDF models.

@author Michael Rinehart, Shuvra S. Bhattacharyya, Ming-Yung Ko, Chia-Jui Hsu
@version $Id: SDFEdgeWeight.java 606 2008-10-08 16:29:47Z plishker $
@see Edge
*/

public class SDFEdgeWeight extends CSDFEdgeWeight {

    /** Construct an edge weight for a homogeneous, zero-delay edge. Production
     *  and consumption values are set to 1.
     */
    public SDFEdgeWeight() {
        setSinkPort(null);
        setSourcePort(null);
        setDelay(new Integer(0));
        setSDFProductionRate(1);
        setSDFConsumptionRate(1);
    }

    /** Construct an edge weight for a specified token production rate,
     *  token consumption rate, and delay.
     *  @param productionRate The token production rate.
     *  @param consumptionRate The token consumption rate.
     *  @param delay The delay.
     */
    public SDFEdgeWeight(int productionRate, int consumptionRate, int delay) {
        setSourcePort(null);
        setSinkPort(null);
        setDelay(new Integer(delay));
        setSDFProductionRate(productionRate);
        setSDFConsumptionRate(consumptionRate);
    }

    /** Construct an edge weight for a specified source port, sink port,
     *  token production rate, token consumption rate, and delay.
     *  The source port and sink port are ports that correspond to this edge.
     *  @param sourcePort The source port.
     *  @param sinkPort The sink port.
     *  @param productionRate The token production rate.
     *  @param consumptionRate The token consumption rate.
     *  @param delay The delay.
     */
    public SDFEdgeWeight(Object sourcePort, Object sinkPort,
           int productionRate, int consumptionRate, int delay) {

        setSinkPort(sinkPort);
        setSourcePort(sourcePort);
        setDelay(new Integer(delay));
        setSDFProductionRate(productionRate);
        setSDFConsumptionRate(consumptionRate);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Get the token consumption rate of the associated SDF edge.
     *  @return The consumption rate.
     */
    public int getSDFConsumptionRate() {
        return getCSDFConsumptionRate(0);
    }

    /** Get the token production rate of the associated SDF edge.
     *  @return The production rate.
     */
    public int getSDFProductionRate() {
        return getCSDFProductionRate(0);
    }

    /** Override {@link DIFEdgeWeight#setConsumptionRate(Object)}
     *  to check the type of input object.
     *  @param consumptionRate
     *  @exception GraphWeightException If consumptionRates is not Integer.
     */
    public void setConsumptionRate(Object consumptionRate) {
        if (consumptionRate instanceof Integer) {
            setSDFConsumptionRate(((Integer)consumptionRate).intValue());
        } else if (consumptionRate instanceof int[]) {
            setCSDFConsumptionRates((int[])consumptionRate);
        } else {
            throw new GraphWeightException("consumptionRate is not Integer. "
                    +"Please use setSDFConsumptionRate.");
        }
    }

    /** Override
     *  {@link dif.csdf.CSDFEdgeWeight#setCSDFConsumptionRates(int[])}
     *  to prevent illegal usage.
     *  @param consumptionRates
     *  @exception GraphWeightException If consumptionRates contains
     *  multiple elements.
     */
    public void setCSDFConsumptionRates(int[] consumptionRates) {
        if (consumptionRates.length == 1) {
            setSDFConsumptionRate(consumptionRates[0]);
        } else {
            throw new GraphWeightException(
                    "Please use setSDFConsumptionRate.");
        }
    }

    /** Override
     *  {@link dif.csdf.CSDFEdgeWeight#setCSDFProductionRates(int[])}
     *  to prevent illegal usage.
     *  @param productionRates
     *  @exception GraphWeightException If productionRates contains
     *  multiple elements.
     */
    public void setCSDFProductionRates(int[] productionRates) {
        if (productionRates.length == 1) {
            setSDFProductionRate(productionRates[0]);
        } else {
            throw new GraphWeightException("Please use setSDFProductionRate.");
        }
    }

    /** Override {@link DIFEdgeWeight#setProductionRate(Object)}
     *  to check the type of input object.
     *  @param productionRate
     *  @exception GraphWeightException If productionRate is not Integer.
     */
    public void setProductionRate(Object productionRate) {
        if (productionRate instanceof Integer) {
            setSDFProductionRate(((Integer)productionRate).intValue());
        } else if (productionRate instanceof int[]) {
            setCSDFProductionRates((int[])productionRate);
        } else {
            throw new GraphWeightException("productionRate is not Integer. "
                    +"Please use setSDFProductionRate.");
        }
    }

    /** Set the token consumption rate of the SDF edge.
     *  @param consumptionRate The token consumption rate in
     *  <code>int</code>.
     */
    public void setSDFConsumptionRate(int consumptionRate) {
        int[] rates = {consumptionRate};
        super.setCSDFConsumptionRates(rates);
    }

    /** Set the token production rate of the SDF edge.
     *  @param productionRate The new token production rate.
     */
    public void setSDFProductionRate(int productionRate) {
        int[] rates = {productionRate};
        super.setCSDFProductionRates(rates);
    }

    /** Return a string representation of the edge weight. This string
     *  representation is in the following form:
     *  <p>
     *  <em> productionRate consumptionRate delay </em>
     *  <p>
     * @return The string representation of the edge weight.
     */
    public String toString() {
        return new String(getSDFProductionRate() + " " +
                getSDFConsumptionRate() + " " + getIntDelay());
    }

}


