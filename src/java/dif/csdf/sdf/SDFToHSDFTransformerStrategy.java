/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A transformation to generate an HSDFGraph from an SDFGraph. */

package dif.csdf.sdf;

import dif.util.graph.Graph;
import dif.util.graph.Node;
import dif.util.graph.Edge;

//////////////////////////////////////////////////////////////////////////
//// SDFToHSDFGraphTransformation
/** A transformation to generate an HSDFGraph from an SDFGraph.
<p>
@author Shahrooz Shahparnia
@version $Id: SDFToHSDFTransformerStrategy.java 606 2008-10-08 16:29:47Z plishker $
*/

public class SDFToHSDFTransformerStrategy
        extends SDFToSingleRateTransformerStrategy {

    /** Construct a transformation for a given graph.
     *  @param graph The given graph.
     */
    public SDFToHSDFTransformerStrategy(Graph graph) {
        super(graph);
    }

    /** Return a description of the analysis. This method
     *  simply returns a description of the associated graph.
     *  It should be overridden in derived classes to
     *  include details associated with the associated analyses.
     *
     *  @return A description of the analysis.
     */
    public String toString() {
        return "Transformation from an SDFGraph to an HSDFGraph";
    }


    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////


    /** This method should be overridden in derived classes to change
     *  the way that edges are added to the graph constructed from an SDF graph.
     *  @param hSource  The source node
     *  @param hSink The sink node
     *  @param newDelay The delay on the edge
     */
    protected Edge _addDataflowEdge(Node hSource, Node hSink, int newDelay) {
        HSDFEdgeWeight edgeWeight = new HSDFEdgeWeight(newDelay);
        return _resultGraph.addEdge(hSource, hSink, edgeWeight);
    }

    /** This method should be overridden in derived classes to change
     *  the type of graph the resulting graph is reset to.
     *  If not overridden the default graph would be HSDFGraph.
     */
    protected void _resetResultGraph() {
        _resultGraph = new HSDFGraph();
    }

    ///////////////////////////////////////////////////////////////////////////
    ////                       private variables                           ////

}


