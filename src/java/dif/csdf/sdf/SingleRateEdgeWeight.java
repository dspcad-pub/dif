/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Information associated with an Single Rate Graph edge. */

package dif.csdf.sdf;

//////////////////////////////////////////////////////////////////////////
//// singleRateEdgeWeight

import dif.util.graph.Edge;
import dif.util.graph.Graph;

/**
Information associated with an single rate graph edge.
SingleRateEdgeWeights are objects associated with {@link Edge}s
that represent Single Rate edges in {@link Graph}s.
Single Rate Graphs are special cases of SDF Graphs in which the production and
consumption rates on its edges are equal. This graphs are a minor
generalization of HSDF graphs.
This class is intended for use with analysis/synthesis algorithms that operate
on Single Rate SDF Graphs.

@author Shahrooz Shahparnia
@version $Id: SingleRateEdgeWeight.java 606 2008-10-08 16:29:47Z plishker $
@see Edge
@see SDFEdgeWeight
@see HSDFEdgeWeight
*/
public class SingleRateEdgeWeight extends SDFEdgeWeight{

    /** Construct an edge weight for a homogeneous, zero-delay edge.
     */
    public SingleRateEdgeWeight() {
        super();
    }

    /** Construct an edge weight for a given token production/consumption
     *  rate, and delay.
     *  @param tokenRate The token consumption/production rate of this Edge.
     *  @param delay The delay associated to this Edge.
     */
    public SingleRateEdgeWeight(int tokenRate, int delay) {
        super(tokenRate, tokenRate, delay);
    }

    /** Set the token production/consumption rate of the associated Single
    * Rate Edge.
    * This method and setTokenProductionRate are the same and they both set
    * the production and consumption rates.
    * @param tokenRate The token consumption/production rate of this Edge.
    */

    public void setSingleConsumptionRate(int tokenRate) {
        setSDFConsumptionRate(tokenRate);
        setSDFProductionRate(tokenRate);
    }

    /** Set the tokenproduction/consumption rate of the associated Single
     *  Rate Edge.
     *  This method and setTokenConsumptionRate are the same and they both
     *  set the production and consumption rates.
     *  @param tokenRate The token consumption/production rate of this Edge.
     */
    public void setSingleProductionRate(int tokenRate) {
        setSDFConsumptionRate(tokenRate);
        setSDFProductionRate(tokenRate);
    }

    /** Set the token production/consumption rate of the associated Single
     *  Rate Edge.
     *  @param tokenRate The token consumption and production rate of this
     *  Edge.
     */
    public void setSingleRate(int tokenRate) {
        setSDFConsumptionRate(tokenRate);
        setSDFProductionRate(tokenRate);
    }

    /** Returns the token production/consumption rate of the associated Single
     *  Rate Edge.
     */
    public int getSingleRate() {
        return getSDFConsumptionRate();
    }
}


