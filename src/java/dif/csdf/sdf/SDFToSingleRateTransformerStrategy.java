/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A transformation to generate an SingleRateGraph from an SDFGraph. */

package dif.csdf.sdf;

import dif.util.graph.Graph;
import dif.util.graph.Node;
import dif.util.graph.Edge;
import dif.util.graph.analysis.strategy.CachedStrategy;
import dif.util.graph.analysis.analyzer.Transformer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Collection;
import java.util.HashSet;

//////////////////////////////////////////////////////////////////////////
//// SDFToSingleRateGraphTransformation
/** A transformation to generate an SingleRateGraph from an SDFGraph.
<p>
@author Shahrooz Shahparnia
@version $Id: SDFToSingleRateTransformerStrategy.java 606 2008-10-08 16:29:47Z plishker $
*/

public class SDFToSingleRateTransformerStrategy extends CachedStrategy
        implements Transformer {

    /** Construct a transformation for a given graph.
     *  @param graph The given graph.
     */
    public SDFToSingleRateTransformerStrategy(Graph graph) {
        super(graph);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Specify if this transformation has a mapping from the transformed
     *  version to the original version or not.
     *
     *  @return True If the implementation of the transformer supports backward
     *  mapping.
     */
    public boolean hasBackwardMapping() {
        return true;
    }

    /** Specify if this transformation has a mapping from the original
     *  version to the transformed version or not.
     *
     *  @return True If the implementation of the transformer supports forward
     *  mapping.
     */
    public boolean hasForwardMapping() {
        return true;
    }

    /** Return the original version of given object in the transformed graph.
     *  The method should be overridden in derived classes to return the
     *  original version of the given object. The transformed objects and a
     *  mapping between the original and the transformed objects are
     *  created during the tranformation process, when the _compute method is
     *  being overriden by the derived classes.
     *  If not overridden the base class returns a node for each
     *  transformed node and an edge for each transformed edge.
     *  The transformed objects are from the resulting graph.
     *  @param transformedObject The given object in the transformed graph.
     *  @return Return the original version the given object.
     */
    public Object originalVersionOf(Object transformedObject) {
        Object originalObject = _originalObjects.get(transformedObject);
        return originalObject;
    }


    /** Return the resulting graph after the transformation.
     *
     *  @return Return the resulting graph after the transformation.
     */
    public Object result() {
        return _result();
    }

    /** Return a description of the analysis. This method
     *  simply returns a description of the associated graph.
     *  It should be overridden in derived classes to
     *  include details associated with the associated analyses.
     *
     *  @return A description of the analysis.
     */
    public String toString() {
        return "Transformation from an SDFGraph to a SingleRateGraph";
    }

    /** Return the transformed version of a given object in the original graph.
     *  The method should be overridden in derived classes to return the
     *  transformed version of the given object. The transformed objects and a
     *  mapping between the original and the transformed objects are
     *  created during the tranformation process, when the _compute method is
     *  being overriden by the derived classes.
     *  If not overridden the base class returns a Set of nodes for each
     *  original node and a Set of edges for each original edge.
     *  The objects objects are from the original SDFGraph being transformed.
     *  @param originalObject The given object in the original graph.
     *  @return Return the transformed version the given object.
     */
    public Object transformedVersionOf(Object originalObject) {
        Object transformedObject = _transformedObjects.get(originalObject);
        return transformedObject;
    }

    /** Check compatibility for between the analysis and the given
     *  graph.
     *
     *  @return True if the graph is a <code>SDFGraph</code>.
     */
    public boolean valid() {
        if (graph() instanceof SDFGraph)
            return true;
        else
            return false;
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** This method should be overridden in derived classes to change
     *  the way that edges are added to the graph constructed from an SDF graph.
     *  @param hSource  The source node
     *  @param hSink The sink node
     *  @param newDelay The delay on the edge
     */
    protected Edge _addDataflowEdge(Node hSource,
            Node hSink, int newDelay) {
        Collection commonEdgeCollection =
                ((SingleRateGraph)_resultGraph).successorEdges(hSource, hSink);
        Object[] commonEdges = commonEdgeCollection.toArray();
        SingleRateEdgeWeight edgeWeight = null;
        Edge addedEdge = null;
        if (commonEdges.length == 1) {
            edgeWeight = (SingleRateEdgeWeight)((Edge)commonEdges[0])
                    .getWeight();
            edgeWeight.setSingleRate(edgeWeight.getSingleRate()+1);

        } else if (commonEdges.length == 0){
            edgeWeight = new SingleRateEdgeWeight(1, newDelay);
            addedEdge = _resultGraph.addEdge(hSource, hSink, edgeWeight);
        }
        return addedEdge;
    }

    /** Computes the transformation of an SDFGraph to a SingleRateGraph.
     *  For more information on the algorithm See: Page40, Figure 3.6,
     *  <p>
     *  S. Sriram and S. S. Bhattacharyya. Embedded Multiprocessors:
     *  Scheduling and Synchronization. Marcel Dekker, Inc., 2000. 327 pages.
     */
    protected Object _compute() {
        _resetResultGraph();
        SDFGraph sdfGraph = (SDFGraph)graph();

        _transformedObjects = new HashMap();
        _originalObjects = new HashMap();
        Iterator nodes = sdfGraph.nodes().iterator();

        sdfGraph.computeRepetitions();

        while (nodes.hasNext()) {
            Node node = (Node)(nodes.next());
            int n = sdfGraph.getRepetitions(node);
            Node[] referenceArray = new Node[n];
             Object computation = null;

            _transformedObjects.put(node, referenceArray);

            for (int j = 0; j < n; j++) {
                int nodeInstance = j;
                if (node.hasWeight()) {
                    SDFNodeWeight nodeWeight = (SDFNodeWeight)node.getWeight();
                    computation = nodeWeight.getComputation();
                }
                SDFNodeWeight newNodeWeight =
                    new SDFNodeWeight(computation, nodeInstance);
                referenceArray[j] = _resultGraph.addNodeWeight(newNodeWeight);
                _originalObjects.put(referenceArray[j], node);
            }
        }

        Collection edgeCollection = sdfGraph.edges();
        Iterator edges = edgeCollection.iterator();
        while (edges.hasNext()) {
            Edge edge = (Edge)(edges.next());
            Node a = (Node)edge.source();
            Node b = (Node)edge.sink();
            int nA = ((SDFEdgeWeight)edge.getWeight()).getSDFProductionRate();
            int qA = sdfGraph.getRepetitions(a);
            int d = ((SDFEdgeWeight)edge.getWeight()).getIntDelay();
            int nB = ((SDFEdgeWeight)edge.getWeight()).getSDFConsumptionRate();
            int qB = sdfGraph.getRepetitions(b);
            for (int i = 1; i <= qA; i++){
                for (int k = 1; k <= nA; k++) {
                    int l = ((d + (i-1) * nA + k - 1) % (nB * qB) + 1);
                    int j = (((d + (i-1) * nA + k - 1) % (nB * qB)) / nB + 1);
                    int newDelay = (d + (i-1) * nA + k - 1)/(nB * qB);
                    Node hSource = ((Node[])_transformedObjects.get(a))[i-1];
                    Node hSink = ((Node[])_transformedObjects.get(b))[j-1];
                    Edge addedEdge =
                            _addDataflowEdge(hSource, hSink, newDelay);

                    HashSet addedEdges = null;
                    if (_transformedObjects.containsKey(edge)) {
                        addedEdges = (HashSet)_transformedObjects.get(edge);
                    }
                    if (addedEdge != null) {
                        if (addedEdges == null) {
                            addedEdges = new HashSet();
                            _transformedObjects.put(edge, addedEdges);
                        }
                        addedEdges.add(addedEdge);
                        _originalObjects.put(addedEdge, edge);
                    }

                }
            }
        }

    return _resultGraph;
    }

    /** This method should be overridden in derived classes to change
     *  the type of graph the resulting graph is reset to.
     *  If not overridden the default graph would be SingleRateGraph.
     */
    protected void _resetResultGraph() {
        _resultGraph = new SingleRateGraph();
    }

    ///////////////////////////////////////////////////////////////////////////
    ////                       private variables                           ////

    protected Graph _resultGraph;
    private HashMap _transformedObjects;
    private HashMap _originalObjects;
}


