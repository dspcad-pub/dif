/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A class for storing a clustered-set of graph nodes. */

package dif.csdf.sdf.analysis;

import java.util.Collection;
import java.util.HashSet;
import dif.util.graph.Node;


//////////////////////////////////////////////////////////////////////////
//// Cluster
/** A class for storing a clusters of graph nodes.
<p>

@author Michael Rinehart
@version $Id: Cluster.java 606 2008-10-08 16:29:47Z plishker $
@see ClusterAnalysis
@see MergeAnalysis
*/
public class Cluster {

    /** Create a new cluster.
     */
    public Cluster() {
        _cluster =  new HashSet();
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////
    /** Adds a node to the cluster.
     *  @param node The node to add.
     */
    public void addNode(Node node) {
        _cluster.add(node);
    }

    /** Determines if a node exists in the cluster.
     *  @param node The node.
     *  @return True if the node exists in the cluster, false otherwise.
     */
    public boolean nodeExists(Node node) {
        return _cluster.contains(node);
    }

    /** Returns the all of the nodes in the cluster.
     *  @return The nodes in the cluster in the form of a collection.
     */
    public Collection nodes() {
        return _cluster;
    }

    /** Removes a node from the cluster.
     *  @param node The node to remove.
     */
    public void removeNode(Node node) {
        _cluster.remove(node);
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////
    private HashSet _cluster;
}


