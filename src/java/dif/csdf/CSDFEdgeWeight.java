/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Information associated with an CSDF edge. */

package dif.csdf;

import java.lang.reflect.Array;

import dif.DIFEdgeWeight;
import dif.util.graph.Edge;
import dif.util.graph.Graph;
import dif.util.graph.GraphWeightException;
import dif.CoreFunctionEdge;

//////////////////////////////////////////////////////////////////////////
//// CSDFEdgeWeight
/** Information associated with an CSDF edge.
CSDFEdgeWeights are objects associated with {@link Edge}s
that represent CSDF edges in {@link Graph}s.
This class caches frequently-used data associated with CSDF edges.
It is also useful for intermediate CSDF graph representations
that do not correspond to Ptolemy II models, and performing graph
transformations (e.g., vectorization and retiming).
It is intended for use with analysis/synthesis algorithms that operate
on generic graph representations of CSDF models.

@author Michael Rinehart, Shuvra S. Bhattacharyya, Chia-Jui Hsu
@version $Id: CSDFEdgeWeight.java 606 2008-10-08 16:29:47Z plishker $
@see Edge
*/

public class CSDFEdgeWeight extends CoreFunctionEdge implements Cloneable {

    /** Construct an edge weight for a homogeneous, zero-delay edge. Production
     *  and consumption values are set to [1].
     */
    public CSDFEdgeWeight() {
        int[] singleRate = {1};
        setCSDFProductionRates(singleRate);
        setCSDFConsumptionRates(singleRate);
        setSinkPort(null);
        setSourcePort(null);
        setDelay(new Integer(0));
    }

    /** Construct an edge weight for a specified token production rate,
     *  token consumption rate, and delay.
     *  @param productionRates The token production rates.
     *  @param consumptionRates The token consumption rates.
     *  @param delay The delay.
     */
    public CSDFEdgeWeight(int[] productionRates,
                          int[] consumptionRates, int delay) {
        setDelay(new Integer(delay));
        setSourcePort(null);
        setSinkPort(null);
        setCSDFProductionRates(productionRates);
        setCSDFConsumptionRates(consumptionRates);
    }

    /** Construct an edge weight for a specified source port, sink port,
     *  token production rate, token consumption rate, and delay.
     *  The source port and sink port are ports that correspond to this edge.
     *  @param sourcePort The source port.
     *  @param sinkPort The sink port.
     *  @param productionRates The token production rates.
     *  @param consumptionRates The token consumption rates.
     *  @param delay The delay.
     */
    public CSDFEdgeWeight(Object sourcePort, Object sinkPort,
                          int[] productionRates,
                          int[] consumptionRates, int delay) {
        setSourcePort(sourcePort);
        setSinkPort(sinkPort);
        setDelay(new Integer(delay));
        setCSDFProductionRates(productionRates);
        setCSDFConsumptionRates(consumptionRates);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** The sum of the token consumption rates over all phases is returned.
     *  @return The period token consumption rate.
     */
    public int consumptionPeriodRate() {
        int i, sum = 0, length = consumptionPhaseCount();

        for (i = 0; i < length; i++)
            sum += getCSDFConsumptionRate(i);

        return sum;
    }

    /** Returns the number of the phases on the consumed end of the edge
     *  @return The number of phases on the consumed end of the edge.
     */
    public int consumptionPhaseCount() {
        return ((int[])_getConsumptionRate()).length;
    }

    /** Returns the token consumption rate of the specified phase
     *  @param phase The phase.
     *  @return The token consumption rate.
     */
    public int getCSDFConsumptionRate(int phase) {
        return getCSDFConsumptionRates()[phase];
    }

    /** Get CSDF consumption rates for all phases.
     *  @return The rates in <code>int[]</code>.
     */
    public int[] getCSDFConsumptionRates() {
        return (int[])_getConsumptionRate();
    }

    /** Returns the token production rate of the specified phase
     *  @param phase The phase.
     *  @return The token production rate.
     */
    public int getCSDFProductionRate(int phase) {
        return getCSDFProductionRates()[phase];
    }

    /** Get CSDF production rates for all phases.
     *  @return The rates in <code>int[]</code>.
     */
    public int[] getCSDFProductionRates() {
        return (int[])_getProductionRate();
    }

    /** Get integer delay value on this edge.
     *  @return Integer delay value.
     */
    public int getIntDelay() {
        return ((Integer)getDelay()).intValue();
    }

    /** The sum of the token production rates over all phases is returned.
     *  @return The period token production rate.
     */
    public int productionPeriodRate() {
        int i, sum = 0, length = productionPhaseCount();

        for (i = 0; i < length; i++)
            sum += getCSDFProductionRate(i);

        return sum;
    }

    /** Returns the number of the phases on the produced end of the edge
     *  @return The number of phases on the produced end of the edge.
    **/
    public int productionPhaseCount() {
        return ((int[])_getProductionRate()).length;
    }

    /** Override {@link DIFEdgeWeight#setConsumptionRate(Object)}
     *  to check the type of input object, and call
     *  {@link #setCSDFConsumptionRates(int[])} to set the CSDF consumption
     *  rates.
     *  @param consumptionRate The new token consumption rates.
     *  @exception GraphWeightException If the input object is not instanceof
     *  int[].
     */
    public void setConsumptionRate(Object consumptionRate) {
        if (consumptionRate instanceof int[]) {
            setCSDFConsumptionRates((int[])consumptionRate);
        } else {
            throw new GraphWeightException("consumptionRate is not int[]."
                    + "Please use setCSDFConsumptionRates(int[]) "
                    + "or setCSDFConsumptionRate(int, int).");
        }
    }

    /** Set the token consumption rates
     *  @param consumptionRates The new token consumption rates as an array.
     */
    public void setCSDFConsumptionRates(int[] consumptionRates) {
        for (int i=0; i<Array.getLength(consumptionRates); i++) {
            if(consumptionRates[i] < 0) {
                throw new GraphWeightException("consumption rate is negative.");
            }
        }
        _setConsumptionRate(consumptionRates);
    }

    /** Set the token consumption rate of the specified phase
     *  @param phase The phase to modify.
     *  @param consumptionRate The new consumption rate.
     */
    public void setCSDFConsumptionRate(int phase, int consumptionRate) {
        if(consumptionRate < 0) {
            throw new GraphWeightException("consumption rate is negative.");
        }
        getCSDFConsumptionRates()[phase] = consumptionRate;
    }

    /** Set the token production rates
     *  @param productionRates The new token production rates as an array.
     */
    public void setCSDFProductionRates(int[] productionRates) {
        for (int i=0; i<Array.getLength(productionRates); i++) {
            if(productionRates[i] < 0) {
                throw new GraphWeightException("production rate is negative.");
            }
        }
        _setProductionRate(productionRates);
    }

    /** Set the token production rate of the specified phase
     *  @param phase The phase to modify.
     *  @param productionRate The new token production rates.
     */
    public void setCSDFProductionRate(int phase, int productionRate) {
        if(productionRate < 0) {
            throw new GraphWeightException("production rate is negative.");
        }
        getCSDFProductionRates()[phase] = productionRate;
    }

    /** Set the delay of the associated CSDF edge.
     *  @param delay An integer number.
     */
    public void setDelay(int delay) {
	setDelay(new Integer(delay));
    }

    /** Set the delay of the associated CSDF edge.
     *  @param delay The new Integer delay object.
     *  @exception GraphWeightException If runtime type of delay is not
     *  Integer.
     */
    public void setDelay(Object delay) {
        if(!(delay instanceof Integer)) {
            throw new GraphWeightException(
                    "Delay should be an Integer object. Dump of the violating"
                    + " delay object is:\n" + delay);
        }
        super.setDelay(delay);
    }

    /** Override {@link DIFEdgeWeight#setProductionRate(Object)}
     *  to check the type of input object, and call
     *  {@link #setCSDFProductionRates(int[])} to set the CSDF production
     *  rates.
     *  @param productionRate The new token production rates.
     *  @exception GraphWeightException If the input object is not instanceof
     *  int[].
     */
    public void setProductionRate(Object productionRate) {
        if (productionRate instanceof int[]) {
            setCSDFProductionRates((int[])productionRate);
        } else {
            throw new GraphWeightException("productionRate is not int[]."
                    + "Please use setCSDFProductionRates(int[]) "
                    + "or setCSDFProductionRate(int, int).");
        }
    }

    /** Return a string representation of the edge weight. This string
     *  representation is in the following form:
     *  <p>
     *  <em> [productionRate1 productionRate2 ...]
     *  [consumptionRate1 consumptionRate2 ...] delay </em>
     *  <p>
     * @return the string representation of the edge weight.
     */
    public String toString() {
        StringBuffer str = new StringBuffer("[");
        int i, clength = consumptionPhaseCount(),
            plength = productionPhaseCount();

        for (i = 0; i < plength; i++) {
            str.append(getCSDFProductionRate(i));
            if (i < (plength - 1)) str.append(" ");
        }
        str.append("] [");

        for (i = 0; i < clength; i++) {
            str.append(getCSDFConsumptionRate(i));
            if (i < (clength - 1)) str.append(" ");
        }
        str.append("] ").append(getIntDelay());

        return str.toString();
    }
}


