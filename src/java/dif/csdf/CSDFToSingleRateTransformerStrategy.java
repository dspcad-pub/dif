/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* A transformation to generate an SingleRateGraph from a CSDFGraph. */

package dif.csdf;

import dif.util.graph.Graph;
import dif.util.graph.Node;
import dif.util.graph.Edge;
import dif.util.graph.analysis.strategy.CachedStrategy;
import dif.util.graph.analysis.analyzer.Transformer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.lang.reflect.Array;

import dif.csdf.sdf.SingleRateEdgeWeight;
import dif.csdf.sdf.SDFNodeWeight;
import dif.csdf.sdf.SingleRateGraph;

//////////////////////////////////////////////////////////////////////////
//// CSDFToSingleRateGraphTransformation
/** A transformation to generate an SingleRateGraph from a CSDFGraph.
The computeRepetitions in CSDFGraph does not * period.
In Cyclo-Static Dataflow paper, Eq1, repetition vector is different than
CSDF repetition vector in DIF. So according to CSDF paper, we multiply
period of each node to each repetition.
<p>
For more information on the algorithm See: Page403, Figure 9,
Greet Bilsen, Marc Engels, Rudy Lauwereins, and Jean Peperstraete.
Cyclo-Static Dataflow, IEEE Transaction On Signal Processing, Feb 1996.
<p>
@author Chia-Jui Hsu
@version $Id: CSDFToSingleRateTransformerStrategy.java 606 2008-10-08 16:29:47Z plishker $
*/

public class CSDFToSingleRateTransformerStrategy extends CachedStrategy
        implements Transformer {

    /** Construct a transformation for a given graph.
     *  @param graph The given graph.
     */
    public CSDFToSingleRateTransformerStrategy(Graph graph) {
        super(graph);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Specify if this transformation has a mapping from the transformed
     *  version to the original version or not.
     *
     *  @return True If the implementation of the transformer supports backward
     *  mapping.
     */
    public boolean hasBackwardMapping() {
        return true;
    }

    /** Specify if this transformation has a mapping from the original
     *  version to the transformed version or not.
     *
     *  @return True If the implementation of the transformer supports forward
     *  mapping.
     */
    public boolean hasForwardMapping() {
        return true;
    }

    /** Return the original version of given object in the transformed graph.
     *  The method should be overridden in derived classes to return the
     *  original version of the given object. The transformed objects and a
     *  mapping between the original and the transformed objects are
     *  created during the tranformation process, when the _compute method is
     *  being overriden by the derived classes.
     *  If not overridden the base class returns a node for each
     *  transformed node and an edge for each transformed edge.
     *  The transformed objects are from the resulting graph.
     *  @param transformedObject The given object in the transformed graph.
     *  @return Return the original version the given object.
     */
    public Object originalVersionOf(Object transformedObject) {
        Object originalObject = _originalObjects.get(transformedObject);
        return originalObject;
    }


    /** Return the resulting graph after the transformation.
     *
     *  @return Return the resulting graph after the transformation.
     */
    public Object result() {
        return _result();
    }

    /** Return a description of the analysis. This method
     *  simply returns a description of the associated graph.
     *  It should be overridden in derived classes to
     *  include details associated with the associated analyses.
     *
     *  @return A description of the analysis.
     */
    public String toString() {
        return "Transformation from a CSDFGraph to a SingleRateGraph";
    }

    /** Return the transformed version of a given object in the original graph.
     *  The method should be overridden in derived classes to return the
     *  transformed version of the given object. The transformed objects and a
     *  mapping between the original and the transformed objects are
     *  created during the tranformation process, when the _compute method is
     *  being overriden by the derived classes.
     *  If not overridden the base class returns a Set of nodes for each
     *  original node and a Set of edges for each original edge.
     *  The objects objects are from the original SDFGraph being transformed.
     *  @param originalObject The given object in the original graph.
     *  @return Return the transformed version the given object.
     */
    public Object transformedVersionOf(Object originalObject) {
        Object transformedObject = _transformedObjects.get(originalObject);
        return transformedObject;
    }

    /** Check compatibility for between the analysis and the given
     *  graph.
     *
     *  @return True if the graph is a <code>CSDFGraph</code>.
     */
    public boolean valid() {
        if (graph() instanceof CSDFGraph)
            return true;
        else
            return false;
    }

    ///////////////////////////////////////////////////////////////////
    ////                         protected methods                 ////

    /** This method should be overridden in derived classes to change
     *  the way that edges are added to the graph constructed from a CSDF graph.
     *  @param hSource  The source node
     *  @param hSink The sink node
     *  @param newDelay The delay on the edge
     */
    protected Edge _addDataflowEdge(Node hSource,
            Node hSink, int rate, int newDelay) {
        return _resultGraph.addEdge(hSource, hSink,
                new SingleRateEdgeWeight(rate, newDelay));
    }

    /** Computes the transformation of a CSDFGraph to a SingleRateGraph.
     *  For more information on the algorithm See: Page403, Figure 9,
     *  <p>
     *  Greet Bilsen, Marc Engels, Rudy Lauwereins, and Jean Peperstraete.
     *  Cyclo-Static Dataflow, IEEE Transaction On Signal Processing, Feb
     *  1996.
     */
    protected Object _compute() {
        //debug
        //System.out.println("debug mode");

        // get an empty SingleRateGraph _resultGraph
        _resetResultGraph();
        CSDFGraph csdfGraph = (CSDFGraph)graph();
        _transformedObjects = new HashMap();
        _originalObjects = new HashMap();

        //Step 1
	//The computeRepetitions in CSDFGraph does not * period
        //In Cyclo-Static Dataflow paper, Eq1, repetition vector is
        //different than CSDF repetition vector in DIF
        //So according to CSDF paper, we multiply period of each node to
        //each repetition.

        /* For each node
         *      Replicate node Vj to Vj_(1) ... Vj_(qj)
         *      Note that Vj_(i) corrosponds to replicatedNodes(i-1)
         */
        //Note Single Rate graph has SingleRateEdgeWeight but does not have
        //SingleRateNodeWeight. We use SDFNodeWeight for single rate graph

        Iterator nodes = csdfGraph.nodes().iterator();
        while(nodes.hasNext()) {
            Node node = (Node)(nodes.next());

            //Actual csdf repetition value
            int qj = csdfGraph.getRepetitions(node)*csdfGraph._getPeriod(node);
            Node[] replicatedNodes = new Node[qj];
            Object computation = null;
            if(node.hasWeight()) {
                computation = ((CSDFNodeWeight)node.getWeight()).getComputation();
            }
            _transformedObjects.put(node, replicatedNodes);

            for(int i=0; i<qj; i++) {
                replicatedNodes[i] = _resultGraph.addNodeWeight(
                        new SDFNodeWeight(computation, i));
                _originalObjects.put(replicatedNodes[i], node);
            }
        }

        //Step 2
        /* For each Vj
         *      Connect Vj_(1) -> Vj_(2) -> ... -> Vj_(qj) --1D--> Vj_(1)
         */
        nodes = csdfGraph.nodes().iterator();
        while(nodes.hasNext()) {
            Node node = (Node)nodes.next();
            //Actual csdf repetition value
            int qj = csdfGraph.getRepetitions(node)*csdfGraph._getPeriod(node);
            Node[] transformedNodes = (Node[])_transformedObjects.get(node);

            for(int i=0; i<qj-1; i++) {
                _addDataflowEdge(transformedNodes[i],transformedNodes[i+1],1,0);
            }
            _addDataflowEdge(transformedNodes[qj-1],transformedNodes[0],1,1);
        }
        //Step 3
        /* For each edge Eu in CSDF graph G, Vj ----> Vk
         *                                       Eu
         */
        Iterator edges = csdfGraph.edges().iterator();

        while(edges.hasNext()) {
        /*
         *      (1)
         *      Determine the first phase, s_ju, of Vj that produce the first
         *      tokens on Eu.
         *      Note that phase starts from 1 in algorithm.
         *      In java code, we need to take care of the start index 0.
         */
            Edge edge = (Edge)(edges.next());
            Node Vj = (Node)edge.source();
            Node Vk = (Node)edge.sink();

            int qj = csdfGraph.getRepetitions(Vj)*csdfGraph._getPeriod(Vj);
            int qk = csdfGraph.getRepetitions(Vk)*csdfGraph._getPeriod(Vk);
            int P_j =
                ((CSDFEdgeWeight)edge.getWeight()).productionPhaseCount();
            int P_k =
                ((CSDFEdgeWeight)edge.getWeight()).consumptionPhaseCount();

            int s_ju = 0;
            int[] prodPhases =
                ((CSDFEdgeWeight)edge.getWeight()).getCSDFProductionRates();

            for(int p=1; p <= Array.getLength(prodPhases); p++) {
                if(prodPhases[p-1] != 0) {
                    s_ju = p;
                    continue;
                }
            }
            if(s_ju == 0) {
                throw new RuntimeException("Cannot find s_ju");
            }

            //Debug
            /*System.out.println("3-1:" + " qj " + qj + ", qk " + qk
                    + ", P_j " + P_j + ", P_k " + P_k
                    + ", s_ju " + s_ju);*/

        /*
         *      (2)
         *      Determine the first invocation, n_kjf of Vk that consumes the
         *      first tokens produced by Vj.
         */
            int iniTokens = ((CSDFEdgeWeight)edge.getWeight()).getIntDelay();
            // Y_ku = total # of tokens consumed in a period
            int Y_ku =
                ((CSDFEdgeWeight)edge.getWeight()).consumptionPeriodRate();
            int[] consPhases =
                ((CSDFEdgeWeight)edge.getWeight()).getCSDFConsumptionRates();

            int n_k = 0;
            for(int p=1; p <= Array.getLength(consPhases); p++) {
                if( (_rateCount(consPhases, p-1) <= iniTokens % Y_ku) &&
                        (iniTokens % Y_ku < _rateCount(consPhases, p)) ) {
                    n_k = p;
                    continue;
                }
            }
            if(n_k == 0) {
                throw new RuntimeException("Cannot find n_k");
            }

            int n_kjf = (int)(iniTokens/Y_ku)*P_k + n_k;
            int ik = (n_kjf - 1) % qk + 1;

            //Debug
            //System.out.println("3-2:" + " n_kjf " + n_kjf + ", ik " + ik);

        /*
         *      (3)
         *      Determine # of tokens, noVk, needed to make invocation n_kjf of Vk
         *      executable.
         */
            int noVk = consPhases[ ((n_kjf-1) % P_k + 1) -1 ] -
                ( iniTokens - _rateCount(consPhases, n_kjf - 1) );

            //Debug
            //System.out.println("3-3:" + " noVk " + noVk);
        /*
         *      (4)
         *      connecting edges
         */
            LinkedList replicatedEdges = new LinkedList();

            for(int ij = s_ju; ij <= qj; ij++) {
                int noVj = prodPhases[ ((ij-1) % P_j + 1) -1 ];
                //Debug
                //System.out.println("3-4-1:" + " noVj " + noVj);

                while(noVj !=0 ) {
                    int min_jk = Math.min(noVj, noVk);

                    Node source = ((Node[])_transformedObjects.get(Vj))[ij-1];
                    Node sink = ((Node[])_transformedObjects.get(Vk))[ik-1];

                    Edge newEdge = _addDataflowEdge( source, sink, min_jk,
                            (int)((n_kjf-1) / qk) );
                    _originalObjects.put(newEdge, edge);
                    replicatedEdges.add(newEdge);

                    noVk -= min_jk;
                    noVj -= min_jk;

                    //Debug
                    /*System.out.println("3-4-2:" + " min_jk " + min_jk
                        + ", ij " + ij + ", ik " + ik
                        + ", delay " + (int)((n_kjf-1) / qk)
                        + ", noVk " + noVk + ", voVj " + noVj );*/

                    while(noVk == 0) {
                        n_kjf++;
                        ik = (n_kjf - 1) % qk + 1;
                        noVk = consPhases[ ((n_kjf - 1) % P_k + 1) -1 ];
                        //Debug
                        /*System.out.println("3-4-3:" + " n_kjf " + n_kjf
                            + ", ik " + ik + ", noVk " + noVk);*/
                    }
                }
            }

            _transformedObjects.put(edge, replicatedEdges.toArray());
        }

        return _resultGraph;
    }

    /** This method should be overridden in derived classes to change
     *  the type of graph the resulting graph is reset to.
     *  If not overridden the default graph would be SingleRateGraph.
     */
    protected void _resetResultGraph() {
        _resultGraph = new SingleRateGraph();
    }

    ///////////////////////////////////////////////////////////////////////////
    ////                       private methods                             ////

    private int _rateCount( int[] rates, int phase ) {
        int sum = 0;
        for(int i=0; i<phase; i++) {
            sum += rates[i];
        }
        return sum;
    }

    ///////////////////////////////////////////////////////////////////////////
    ////                       private variables                           ////

    protected Graph _resultGraph;
    private HashMap _transformedObjects;
    private HashMap _originalObjects;
}


