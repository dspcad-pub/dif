/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Information associated with an BCSDF edge. */

package dif.csdf.bcsdf;

import java.lang.reflect.Array;
import java.util.BitSet;

import dif.csdf.CSDFEdgeWeight;

import dif.DIFEdgeWeight;
import dif.util.graph.Edge;
import dif.util.graph.Graph;
import dif.util.graph.GraphWeightException;

//////////////////////////////////////////////////////////////////////////
//// BCSDFEdgeWeight
/** Information associated with an BCSDF edge.
BCSDFEdgeWeights are objects associated with {@link Edge}s
that represent BCSDF edges in {@link Graph}s.
This class caches frequently-used data associated with BCSDF edges.
It is also useful for intermediate BCSDF graph representations
that do not correspond to Ptolemy II models, and performing graph
transformations (e.g., vectorization and retiming).
It is intended for use with analysis/synthesis algorithms that operate
on generic graph representations of BCSDF models.

The BCSDF production and consumption rates are stored as java.util.BitSet.

@author Chia-Jui Hsu
@version $Id: BCSDFEdgeWeight.java 606 2008-10-08 16:29:47Z plishker $
@see Edge
*/

public class BCSDFEdgeWeight extends CSDFEdgeWeight {

    /** Construct an edge weight for a homogeneous, zero-delay edge. Production
     *  and consumption values are set to [1].
     */
    public BCSDFEdgeWeight() {
        BitSet singleRate = new BitSet();
        singleRate.set(0, true);
        setBCSDFProductionRates(singleRate);
        setBCSDFConsumptionRates(singleRate);
        setSinkPort(null);
        setSourcePort(null);
        setDelay(new Integer(0));
    }

    /** Construct an edge weight for a specified token production rate,
     *  token consumption rate, and delay.
     *  @param productionRates The token production rates.
     *  @param consumptionRates The token consumption rates.
     *  @param delay The delay.
     */
    public BCSDFEdgeWeight(BitSet productionRates,
                          BitSet consumptionRates, int delay) {
        setDelay(new Integer(delay));
        setSourcePort(null);
        setSinkPort(null);
        setBCSDFProductionRates(productionRates);
        setBCSDFConsumptionRates(consumptionRates);
    }

    /** Construct an edge weight for a specified token production rate,
     *  token consumption rate, and delay.
     *  @param productionRates The token production rates.
     *  @param consumptionRates The token consumption rates.
     *  @param delay The delay.
     */
    public BCSDFEdgeWeight(int[] productionRates,
                          int[] consumptionRates, int delay) {
        setDelay(new Integer(delay));
        setSourcePort(null);
        setSinkPort(null);
        setBCSDFProductionRates(productionRates);
        setBCSDFConsumptionRates(consumptionRates);
    }

    /** Construct an edge weight for a specified source port, sink port,
     *  token production rate, token consumption rate, and delay.
     *  The source port and sink port are ports
     *  that correspond to this edge.
     *  @param sourcePort The source port.
     *  @param sinkPort The sink port.
     *  @param productionRates The token production rates.
     *  @param consumptionRates The token consumption rates.
     *  @param delay The delay.
     */
    public BCSDFEdgeWeight(Object sourcePort, Object sinkPort,
                          BitSet productionRates,
                          BitSet consumptionRates, int delay) {
        setSourcePort(sourcePort);
        setSinkPort(sinkPort);
        setDelay(new Integer(delay));
        setBCSDFProductionRates(productionRates);
        setBCSDFConsumptionRates(consumptionRates);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Returns the token consumption rate of the specified phase
     *  @param phase The phase.
     *  @return The token consumption rate.
     */
    public int getBCSDFConsumptionRate(int phase) {
        return getBCSDFConsumptionRates()[phase];
    }

    /** Get BCSDF consumption rates for all phases.
     *  @return The rates in <code>int[]</code>.
     */
    public int[] getBCSDFConsumptionRates() {
        BitSet consBits = (BitSet)_getConsumptionRate();
        int[] consRates = new int[consBits.length()];
        for(int i=0; i<consBits.length(); i++) {
            if(consBits.get(i) == true) {
                consRates[i] = 1;
            } else {
                consRates[i] = 0;
            }
        }
        return consRates;
    }

    /** Returns the token production rate of the specified phase
     *  @param phase The phase.
     *  @return The token production rate.
     */
    public int getBCSDFProductionRate(int phase) {
        return getBCSDFProductionRates()[phase];
    }

    /** Get BCSDF production rates for all phases.
     *  @return The rates in <code>int[]</code>.
     */
    public int[] getBCSDFProductionRates() {
        BitSet prodBits = (BitSet)_getProductionRate();
        int[] prodRates = new int[prodBits.length()];
        for(int i=0; i<prodBits.length(); i++) {
            if(prodBits.get(i) == true) {
                prodRates[i] = 1;
            } else {
                prodRates[i] = 0;
            }
        }
        return prodRates;
    }

    /** Set the token consumption rates
     *  @param consumptionRates The token consumption rates as BitSet.
     */
    public void setBCSDFConsumptionRates(BitSet consBits) {
        _setConsumptionRate(consBits);
    }

    /** Set the token consumption rates
     *  @param consumptionRates The new token consumption rates as an array.
     */
    public void setBCSDFConsumptionRates(int[] consumptionRates) {
        BitSet consBits = new BitSet(Array.getLength(consumptionRates));
        for (int i=0; i<Array.getLength(consumptionRates); i++) {
            if(consumptionRates[i] == 0) {
                consBits.set(i,false);
            } else if(consumptionRates[i] == 1) {
                consBits.set(i,true);
            } else {
                throw new GraphWeightException(
                        "consumption rate must be 0 or 1.");
            }
        }
        _setConsumptionRate(consBits);
    }

    /** Set the token consumption rate of the specified phase
     *  @param phase The phase to modify.
     *  @param consumptionRate The new consumption rate.
     */
    public void setBCSDFConsumptionRate(int phase, int consumptionRate) {
        if(consumptionRate == 0) {
            ((BitSet)_getConsumptionRate()).set(phase,false);
        } else if(consumptionRate == 1) {
            ((BitSet)_getConsumptionRate()).set(phase,true);
        } else {
            throw new GraphWeightException(
                    "consumption rate must be 0 or 1.");
        }
    }

    /** Set the token consumption rate of the specified phase
     *  @param phase The phase to modify.
     *  @param consumptionRate The new consumption rate.
     */
    public void setBCSDFConsumptionRate(int phase, boolean consumptionRate) {
        ((BitSet)_getConsumptionRate()).set(phase,consumptionRate);
    }

    /** Set the token production rates
     *  @param productionRates The new token production rates as a BitSet.
     */
    public void setBCSDFProductionRates(BitSet prodBits) {
        _setProductionRate(prodBits);
    }

    /** Set the token production rates
     *  @param productionRates The new token production rates as an array.
     */
    public void setBCSDFProductionRates(int[] productionRates) {
        BitSet prodBits = new BitSet(Array.getLength(productionRates));
        for (int i=0; i<Array.getLength(productionRates); i++) {
            if(productionRates[i] == 0) {
                prodBits.set(i,false);
            } else if(productionRates[i] == 1) {
                prodBits.set(i,true);
            } else {
                throw new GraphWeightException(
                        "production rate must be 0 or 1.");
            }
        }
        _setProductionRate(prodBits);
    }

    /** Set the token production rate of the specified phase
     *  @param phase The phase to modify.
     *  @param productionRate The new production rate.
     */
    public void setBCSDFProductionRate(int phase, int productionRate) {
        if(productionRate == 0) {
            ((BitSet)_getProductionRate()).set(phase,false);
        } else if(productionRate == 1) {
            ((BitSet)_getProductionRate()).set(phase,true);
        } else {
            throw new GraphWeightException(
                    "production rate must be 0 or 1.");
        }
    }

    /** Set the token production rate of the specified phase
     *  @param phase The phase to modify.
     *  @param productionRate The new production rate.
     */
    public void setBCSDFProductionRate(int phase, boolean productionRate) {
        ((BitSet)_getProductionRate()).set(phase,productionRate);
    }

    /** Override {@link DIFEdgeWeight#setConsumptionRate(Object)}
     *  to check the type of input object, and call
     *  {@link #setBCSDFConsumptionRates} to set the BCSDF consumption
     *  rates.
     *  @param consumptionRate The new token consumption rates.
     *  @exception GraphWeightException If the input object is not instanceof
     *  int[] or BitSet.
     */
    public void setConsumptionRate(Object consumptionRate) {
        if (consumptionRate instanceof int[]) {
            setBCSDFConsumptionRates((int[])consumptionRate);
        } else if (consumptionRate instanceof BitSet) {
            setBCSDFConsumptionRates((BitSet)consumptionRate);
        } else {
            throw new GraphWeightException(
                    "consumptionRate is not int[] or BitSet."
                    + "Please use setBCSDFConsumptionRates.");
        }
    }

    /** Override
     *  {@link dif.csdf.CSDFEdgeWeight#setCSDFConsumptionRates(int[])}
     *  to prevent illegal usage.
     *  @param consumptionRates
     *  @exception GraphWeightException
     */
    public void setCSDFConsumptionRates(int[] consumptionRates) {
        throw new GraphWeightException("Please use setBCSDFConsumptionRates.");
    }

    /** Override
     *  {@link dif.csdf.CSDFEdgeWeight#setCSDFProductionRates(int[])}
     *  to prevent illegal usage.
     *  @param productionRates
     *  @exception GraphWeightException
     */
    public void setCSDFProductionRates(int[] productionRates) {
        throw new GraphWeightException("Please use setBCSDFProductionRates.");
    }

    /** Override {@link DIFEdgeWeight#setProductionRate(Object)}
     *  to check the type of input object, and call
     *  {@link #setBCSDFProductionRates(int[])} to set the BCSDF production
     *  rates.
     *  @param productionRate The new token production rates.
     *  @exception GraphWeightException If the input object is not instanceof
     *  int[] or BitSet.
     */
    public void setProductionRate(Object productionRate) {
        if (productionRate instanceof int[]) {
            setBCSDFProductionRates((int[])productionRate);
        } else if (productionRate instanceof BitSet) {
            setBCSDFProductionRates((BitSet)productionRate);
        } else {
            throw new GraphWeightException(
                    "productionRate is not int[] or BitSet"
                    + "Please use setBCSDFProductionRates.");
        }
    }

    /** Return a string representation of the edge weight. This string
     *  representation is in the following form:
     *  <p>
     *  <em> [productionRate1 productionRate2 ...]
     *  [consumptionRate1 consumptionRate2 ...] delay </em>
     *  <p>
     * @return the string representation of the edge weight.
     */
    public String toString() {
        StringBuffer str = new StringBuffer("[");
        int i, clength = consumptionPhaseCount(),
            plength = productionPhaseCount();

        for (i = 0; i < plength; i++) {
            str.append(getBCSDFProductionRate(i));
            if (i < (plength - 1)) str.append(" ");
        }
        str.append("] [");

        for (i = 0; i < clength; i++) {
            str.append(getBCSDFConsumptionRate(i));
            if (i < (clength - 1)) str.append(" ");
        }
        str.append("] ").append(getIntDelay());

        return str.toString();
    }
}


