/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Information associated with an BCSDF graph. */

package dif.csdf.bcsdf;

import dif.csdf.CSDFGraph;

//////////////////////////////////////////////////////////////////////////
//// BCSDFGraph
/** Information associated with an BCSDF graph.
This class caches frequently-used data associated with BCSDF graphs.
It is also useful for intermediate BCSDF graph representations
that do not correspond to Ptolemy II models, and performing graph
transformations (e.g. convertion to and manipulation of single-rate graphs).
It is intended for use with analysis/synthesis algorithms that operate
on generic graph representations of BCSDF models.
<p>
BCSDFGraph nodes and edges have weights of type
{@link dif.csdf.CSDFNodeWeight} and
{@link BCSDFEdgeWeight}, respectively.

@author Chia-Jui Hsu
@version $Id: BCSDFGraph.java 606 2008-10-08 16:29:47Z plishker $
*/

public class BCSDFGraph extends CSDFGraph {

    /** Construct an empty BCSDF graph.
     */
    public BCSDFGraph() {
        super();
    }

    /** Construct an empty BCSDF graph with enough storage allocated for the
     *  specified number of nodes.
     *  @param nodeCount The number of nodes.
     */
    public BCSDFGraph(int nodeCount) {
        super(nodeCount);
    }

    /** Construct an empty BCSDF graph with enough storage allocated for the
     *  specified number of edges, and number of nodes.
     *  @param nodeCount The integer specifying the number of nodes
     *  @param edgeCount The integer specifying the number of edges
     */
    public BCSDFGraph(int nodeCount, int edgeCount) {
        super(nodeCount, edgeCount);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Verify edge weight for CSDF graph.
     *  @param weight The edge weight to verify.
     *  @return True if the given edge weight is valid for CSDF graph.
     */
    public boolean validEdgeWeight(Object weight) {
        return weight instanceof BCSDFEdgeWeight;
    }

    /* Verify node weight for CSDF graph.
     * Because BCSDFGraph uses CSDFNodeWeight, CSDFGraph.validNodeWeight
     * performs the task.
     */
}


