/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif.csdf.demo;

import dif.DIFHierarchy;
import dif.DIFLoader;
import dif.csdf.sdf.SDFGraph;
import dif.graph.DIFdoc;
import dif.util.graph.Node;

import java.io.IOException;
import java.util.HashMap;

public class RepetitionVector {
    static class SolveProblem {
        DIFHierarchy hier;

        void solve(String[] argv) {
            hier = DIFLoader.loadDataflow(argv[0]);
            generateDIFDoc();

            SDFGraph sdf = (SDFGraph) hier.getGraph();
            HashMap map = sdf.computeRepetitions();
            for (Object o : map.keySet()) {
                Node node = (Node) o;
                System.out.printf("Node ID = %s, Repetition Count = %d\n", sdf.getName(node), map.get(o));
            }
        }

        private void generateDIFDoc() {
            DIFdoc doc = new DIFdoc(hier);
            try {
                doc.toFile("index");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] argv) {
        SolveProblem sp = new SolveProblem();
        sp.solve(argv);
    }
}
