/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Information associated with an CSDF graph. */

package dif.csdf;

import dif.CoreFunctionNode;
import dif.cfdf.CFDFGraph;
import dif.data.ExtendedMath;
import dif.data.Fraction;
import dif.util.graph.Edge;
import dif.util.graph.Node;

import java.util.HashMap;
import java.util.Iterator;

//////////////////////////////////////////////////////////////////////////
//// CSDFGraph

/**
 * Information associated with an CSDF graph.
 * This class caches frequently-used data associated with CSDF graphs.
 * It is also useful for intermediate CSDF graph representations
 * that do not correspond to Ptolemy II models, and performing graph
 * transformations (e.g. convertion to and manipulation of single-rate graphs).
 * It is intended for use with analysis/synthesis algorithms that operate
 * on generic graph representations of CSDF models.
 * <p>
 * CSDFGraph nodes and edges have weights of type {@link CSDFNodeWeight} and \
 * {@link CSDFEdgeWeight}, respectively.
 * <p>
 * The repetition vector computed in this class is the minimal integer solution
 * of the topology matrix. Which is different from the repetition vector defined
 * in the "Cyclo-static Dataflow" paper by G. Bilsen, M. Engels, R. Lauwereins,
 * and J. Peperstraete. The repetition vector defined in that paper is the
 * "phase" repetition vector.
 *
 * @author Chia-Jui Hsu, Michael Rinehart, Ming-Yung Ko, Shuvra S. Bhattacharyya
 * @version $Id: CSDFGraph.java 606 2008-10-08 16:29:47Z plishker $
 * @see CSDFEdgeWeight
 * @see CSDFNodeWeight
 */

public class CSDFGraph extends CFDFGraph {

    /**
     * Construct an empty CSDF graph.
     */
    public CSDFGraph() {
        super();
        _repetitions = new HashMap();
    }

    /**
     * Construct an empty CSDF graph with enough storage allocated for the
     * specified number of nodes.
     *
     * @param nodeCount The number of nodes.
     */
    public CSDFGraph(int nodeCount) {
        super(nodeCount);
        _repetitions = new HashMap();
    }

    /**
     * Construct an empty CSDF graph with enough storage allocated for the
     * specified number of edges, and number of nodes.
     *
     * @param nodeCount The integer specifying the number of nodes
     * @param edgeCount The integer specifying the number of edges
     */
    public CSDFGraph(int nodeCount, int edgeCount) {
        super(nodeCount, edgeCount);
        _repetitions = new HashMap();
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /**
     * Check node's repetitions. Return <code>true</code> if the repetitions
     * have been evaluated, otherwise <code>false</code>. This method is
     * created to cooperate
     * new node addition.
     *
     * @param node The given node
     * @return <code>true</code> if repetitions evaluated,
     * <code>false</code> otherwise
     */
    public boolean checkRepetitions(Node node) {
        return _repetitions.containsKey(node);
    }

    /**
     * Compute repetitions vector for a given CSDF graph.  The repetitions
     * will be returned as a HashMap, with {@link Node} as
     * the key(s) and {@link Integer} as the repetition value(s).
     *
     * @return A HashMap (node - repetition)
     */
    public HashMap computeRepetitions() {
        int nodeCount = nodeCount();
        int edgeCount = edgeCount();

        if (connectedComponents() != null) {
            if (connectedComponents().size() > 1) {
                throw new RuntimeException("Graph not connected");
            }
        } else {
            throw new RuntimeException("connectedComponents returned null");
        }

        //check to see if there is period inconsistent node or not.
        //if yes, it will throw exception
        _checkPeriods();

        _repts = new int[nodeCount];
        _denominators = new int[nodeCount];
        _repts[0] = _denominators[0] = _multiplier = 1;

        //recursively computes all repetitions
        _computeNodeRepetition(node(0));

        for (int i = 0; i < nodeCount; i++) {
            _repts[i] *= (_multiplier / _denominators[i]);
        }

        // check consistency
        Iterator edgeIter = edges().iterator();
        while (edgeIter.hasNext()) {
            Edge arc = (Edge) edgeIter.next();
            int srcRept = _repts[nodeLabel(arc.source())];
            int snkRept = _repts[nodeLabel(arc.sink())];
            int cons = ((CSDFEdgeWeight) arc.getWeight()).
                    productionPeriodRate();
            int prod = ((CSDFEdgeWeight) arc.getWeight()).
                    consumptionPeriodRate();
            if ((srcRept * prod) != (snkRept * cons))
                throw new RuntimeException("Inconsistent sample rates");
        }

        int rep_gcd = _repts[0];
        for (int i = 0; i < nodeCount; i++) {
            rep_gcd = ExtendedMath.gcd(rep_gcd, _repts[i]);
        }
        if ((nodeCount > 1) && (rep_gcd > 1)) {
            throw new RuntimeException("Inconsistent sample rates");
        }

        // create _repetition HashMap
        for (int i = 0; i < nodeCount; i++) {
            _repetitions.put(node(i), new Integer(_repts[i]));
        }
        return _repetitions;
    }

    /**
     * Get the repetition count of a given CSDF node in this graph.
     * If the repetitions value has not yet been computed, then first
     * compute the repetitions vector for the entire graph.
     *
     * @param node The CSDF node.
     * @return The repetition count.
     */
    public int getRepetitions(Node node) {
        if ((_repetitions == null) || !_repetitions.containsKey(node)) {
            computeRepetitions();
        }
        if (!_repetitions.containsKey(node)) {
            throw new RuntimeException("Could not compute repetition count "
                    + "for the specified node. Dumps of the node and graph "
                    + "follow.\n" + node.toString() + "\n" + toString());
        } else return ((Integer) (_repetitions.get(node))).intValue();
    }

    /**
     * Remove a node from the CSDF graph. This method overrides
     * <code>removeNode()</code> in <code>Graph</code>. Besides removing
     * node from graph, it removes the entry of repetitions map.
     *
     * @param node The node to remove
     * @return True if the node was removed.
     */
    public boolean removeNode(Node node) {
        boolean result = super.removeNode(node);
        if (result) {
            if (_repetitions.containsKey(node)) {
                _repetitions.remove(node);
            } else {
                result = false;
            }
        }
        return result;
    }

    /**
     * Set repetition counts for a node. Sample rate consistency will be
     * checked. Therefore, proper sample rates should be assigned before
     * calling this method.
     *
     * @param node  The given node
     * @param count The given repetition counts
     * @throws IllegalArgumentException If the node is not contained in
     *                                  the graph.
     * @throws RuntimeException         Graph not connected
     * @throws RuntimeException         Inconsistent sample rates
     */
    public void setRepetitions(Node node, int count) {
        if (!containsNode(node))
            throw new IllegalArgumentException("The node is not "
                    + "contained in the graph.");

        // Special case: graph has only one node and no edges. This could
        // happen in clustering when all nodes are clustered into a single
        // root node.
        if (nodeCount() == 1) {
            _repetitions.put(node, new Integer(1));

            // Normal case. Graph is checked connected first to enable consistent
            // sample rates checking.
        } else {
            if (incidentEdges(node).isEmpty())
                throw new RuntimeException("Graph not connected");
            if (!_checkConsistency(node, count))
                throw new RuntimeException("Inconsistent sample rates");
            _repetitions.put(node, new Integer(count));
        }
    }

    /*public void setDebug(boolean debug) {
        _debugging = debug;
    }*/

    /**
     * Verify edge weight for CSDF graph.
     *
     * @param weight The edge weight to verify.
     * @return True if the given edge weight is valid for CSDF graph.
     */
    public boolean validEdgeWeight(Object weight) {
        return weight instanceof CSDFEdgeWeight;
    }

    /**
     * Verify node weight for CSDF graph.
     *
     * @param weight The node weight to verify.
     * @return True if the given node weight is valid for CSDF graph.
     */
    public boolean validNodeWeight(Object weight) {
        return weight instanceof CoreFunctionNode;
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /**
     * Computes and checks periods of nodes.
     * If the edges connected to a node with different periods, this
     * function throws an exception.
     *
     * @return A HashMap (node - period)
     */
    protected HashMap _checkPeriods() {
        _periods = new HashMap();

        Iterator nodeIter = nodes().iterator();
        while (nodeIter.hasNext()) {
            Node node = (Node) nodeIter.next();

            int period = 0;

            Iterator inputEdgeIter = inputEdges(node).iterator();
            while (inputEdgeIter.hasNext()) {
                Edge edge = (Edge) inputEdgeIter.next();
                CSDFEdgeWeight edgeWeight = (CSDFEdgeWeight) edge.getWeight();
                if (period == 0) {
                    period = edgeWeight.consumptionPhaseCount();
                } else {
                    if (period != edgeWeight.consumptionPhaseCount()) {
                        throw new RuntimeException("Period of node " +
                                getName(node) + " does not consistent.");
                    }
                }
            }

            Iterator outputEdgeIter = outputEdges(node).iterator();
            while (outputEdgeIter.hasNext()) {
                Edge edge = (Edge) outputEdgeIter.next();
                CSDFEdgeWeight edgeWeight = (CSDFEdgeWeight) edge.getWeight();
                if (period == 0) {
                    period = edgeWeight.productionPhaseCount();
                } else {
                    if (period != edgeWeight.productionPhaseCount()) {
                        throw new RuntimeException("Period of node " +
                                getName(node) + " does not consistent.");
                    }
                }
            }

            _periods.put(node, new Integer(period));
        }

        return _periods;
    }

    /**
     * Get the period of a CSDF node.
     */
    protected int _getPeriod(Node node) {
        if (_periods == null) {
            _checkPeriods();
        }
        return ((Integer) (_periods.get(node))).intValue();
    }


    ///////////////////////////////////////////////////////////////////
    ////                       private methods                   //////

    /**
     * Given an node and a new repetitions, check the consistency of
     * sampling rates.
     */
    private boolean _checkConsistency(Node node, int count) {
        boolean consistency = true;

        //check period of node is consistent or not
        if (_periods == null) {
            _checkPeriods();
        }

        Iterator edges = incidentEdges(node).iterator();
        while (edges.hasNext()) {

            Edge incidentEdge = (Edge) edges.next();
            int pRate = ((CSDFEdgeWeight) incidentEdge.getWeight()).
                    productionPeriodRate();
            int cRate = ((CSDFEdgeWeight) incidentEdge.getWeight()).
                    consumptionPeriodRate();
            int production;
            int consumption;
            Node source = incidentEdge.source();
            Node sink = incidentEdge.sink();


            // compute total number of samples exchanged
            if (node == source) {
                production = count * pRate;
                // Evluate total consumed tokens. Before doing that,
                // check repetitions of sink first. If it's unset, make
                // consumption equal to production. Sink is assumed to
                // have a repetitions in the future.
                consumption = (!checkRepetitions(sink)) ? production :
                        getRepetitions(sink) * cRate;
            } else {
                consumption = count * cRate;
                // Evluate total produced tokens. Before doing that,
                // check repetitions of source first. If it's unset, make
                // production equal to consumption. Source is assumed to
                // have a repetitions in the future.
                production = (!checkRepetitions(source)) ? consumption :
                        getRepetitions(source) * pRate;
            }
            // check consistency
            if (production != consumption) {
                consistency = false;
                break;
            }
        }
        return consistency;
    }

    /**
     * This method computes a node's repetition by recursion.  The
     * repetition is saved in the private _repts[].
     */
    private void _computeNodeRepetition(Node node) {
        int prod, cons;

        Iterator outEdgeIter = outputEdges(node).iterator();
        while (outEdgeIter.hasNext()) {
            Edge nextOut = (Edge) outEdgeIter.next();
            Node sink = nextOut.sink();
            prod = ((CSDFEdgeWeight) nextOut.getWeight()).
                    productionPeriodRate();
            cons = ((CSDFEdgeWeight) nextOut.getWeight()).
                    consumptionPeriodRate();
            if (_reptEdge(node, cons, sink, prod)) {
                _computeNodeRepetition(sink);
            }
        }

        Iterator inEdgeIter = inputEdges(node).iterator();
        while (inEdgeIter.hasNext()) {
            Edge nextIn = (Edge) inEdgeIter.next();
            Node src = nextIn.source();
            prod = ((CSDFEdgeWeight) nextIn.getWeight()).
                    productionPeriodRate();
            cons = ((CSDFEdgeWeight) nextIn.getWeight()).
                    consumptionPeriodRate();
            if (_reptEdge(node, prod, src, cons)) {
                _computeNodeRepetition(src);
            }
        }
    }

    /**
     * Check that an edge's sink node repetition is evaluated or not.
     */
    private boolean _reptEdge(Node p, int pParam, Node q, int qParam) {
        int pIndx = nodeLabel(p);
        int qIndx = nodeLabel(q);
        int gcd;
        if (_repts[qIndx] == 0) {
            _repts[qIndx] = _repts[pIndx] * pParam;
            _denominators[qIndx] = _denominators[pIndx] * qParam;
            // reduce both numbers by their GCD
            gcd = ExtendedMath.gcd(_repts[qIndx], _denominators[qIndx]);
            _repts[qIndx] /= gcd;
            _denominators[qIndx] /= gcd;
            _multiplier = Fraction.lcm(_multiplier, _denominators[qIndx]);
            return true;
        } else {
            return false;
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    // Flag for local debugging output.
    private boolean _debugging = false;

    //private static int[] _denominators;
    private int[] _denominators;

    private int _multiplier;

    // Node -> period of the node
    private HashMap _periods = null;

    // The repetitions vector of the CSDF graph. The keys of this HashMap are
    // CSDF nodes (of type Node), and the values are Integers.
    private HashMap _repetitions = null;

    //private static int[] _rept;
    private int[] _repts;
}


