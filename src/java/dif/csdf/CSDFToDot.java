/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* CSDFToDot class to use with dif.csdf.CSDFGraph. */

package dif.csdf;

import java.util.Iterator;

import dif.util.graph.Edge;

import dif.DIFToDot;
import dif.util.Value;

//////////////////////////////////////////////////////////////////////////
//// CSDFToDot
/** DOT file generator for CSDFGraph objects. It is used to create dot files as
an input to GraphViz tools. A DOT file is created by first defining an
CSDFToDot object and then using the {@link #toFile} method. Node labels are set
to element names in DIFGraph and edge labels are set to production, consumption
and delay values.
@author Fuat Keceli
@version $Id: CSDFToDot.java 606 2008-10-08 16:29:47Z plishker $
*/

public class CSDFToDot extends DIFToDot {

    /** Creates a DotGenerator object from a CSDFGraph.
     *  @param graph A CSDFGraph.
     */
    public CSDFToDot(CSDFGraph graph) {
        super(graph);
        for(Iterator graphEdges = _graph.edges().iterator();
            graphEdges.hasNext(); ) {
            Edge graphEdge = (Edge) graphEdges.next();
            CSDFEdgeWeight weight = (CSDFEdgeWeight) graphEdge.getWeight();
            setAttribute(graphEdge, "label",
                    String.valueOf(weight.getDelay()));
            setAttribute(graphEdge, "headlabel",
                    Value.toDIFString(weight.getCSDFConsumptionRates()));
            setAttribute(graphEdge, "taillabel",
                    Value.toDIFString(weight.getCSDFProductionRates()));
        }
    }
}


