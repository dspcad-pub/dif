/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif;

import dif.graph.DotGenerator;
import dif.util.graph.Edge;
import dif.util.graph.Node;

import java.lang.reflect.Array;
import java.util.Iterator;

//////////////////////////////////////////////////////////////////////////
//// DIFToDot

/**
 * DOT file generator for DIFGraph objects. It is used to create dot files as
 * an input to GraphViz tools. A DOT file is created by first defining an
 * DIFToDot object and then using the {@link #toFile} method. Node and edge
 * labels
 * are set to element names in DIFGraph.
 *
 * @author Fuat Keceli
 * @version $Id: DIFToDot.java 606 2008-10-08 16:29:47Z plishker $
 */

public class DIFToDot extends DotGenerator {

    /**
     * Creates a DotGenerator object from a DIFGraph.
     *
     * @param graph A DIFGraph.
     */
    public DIFToDot(DIFGraph graph) {
        super(graph);
        addLine("label = " + "\"" + ((DIFGraph) _graph).getName() + "\";");
        addLine("node[shape = circle];");
        addLine("center = true;");
        addLine("edge[fontcolor = red];");
        String str;
        for (Iterator graphNodes = _graph.nodes().iterator();
             graphNodes.hasNext(); ) {
            Node graphNode = (Node) graphNodes.next();
            str = graph.getName(graphNode);
            if (str.startsWith("$") && str.endsWith("$")) {
                str = str.substring(1, str.length() - 1);
            }
            str = "\"" + str + "\"";
            setAttribute(graphNode, "label", str);
        }
        for (Iterator graphEdges = _graph.edges().iterator();
             graphEdges.hasNext(); ) {
            Edge graphEdge = (Edge) graphEdges.next();
            str = graph.getName(graphEdge);
            if (str.startsWith("$") && str.endsWith("$")) {
                str = str.substring(1, str.length() - 1);
            }
            str = "\"" + str + "\"";
            setAttribute(graphEdge, "label", str);
            //setAttribute(graphEdge, "labeldistance", "1.75");
            //setAttribute(graphEdge, "labelangle", "65");
            //    setAttribute(graphEdge, "fontsize", "10");

            DIFEdgeWeight cfe = (DIFEdgeWeight) graphEdge.getWeight();

            Object prate = cfe.getProductionRate();
            if (prate != null) {
                if (prate instanceof int[]) {
                    int[] values = (int[]) prate;
                    str = new String("\"[");
                    for (int i = 0; i < Array.getLength(values); i++) {
                        if (i > 0) {
                            str += ",";
                        }
                        str += values[i];
                    }
                    str += "]\"";
                    //		    setAttribute(graphEdge, "taillabel",str);
                } else {
                    // setAttribute(graphEdge, "taillabel", cfe
                    // .getProductionRate().toString());
                }
            }

            Object crate = cfe.getConsumptionRate();
            if (crate != null) {
                if (crate instanceof int[]) {
                    int[] values = (int[]) crate;
                    str = new String("\"[");
                    for (int i = 0; i < Array.getLength(values); i++) {
                        if (i > 0) {
                            str += ",";
                        }
                        str += values[i];
                    }
                    str += "]\"";
                    //    setAttribute(graphEdge, "headlabel",str);
                } else {
                    //setAttribute(graphEdge, "headlabel", cfe
                    // .getConsumptionRate().toString());
                }
            }
        }
    }
}


