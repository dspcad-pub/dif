/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif;

/**
 * Version class for the DIF package.  DIFVersion is a class simply to
 * allow programmers to query what version of the DIF package is in use.
 *
 * @author William Plishker
 * @version $Id: DIFVersion.java 409 2007-05-13 19:47:16Z plishker $
 */


public class DIFVersion {

    /**
     * Empty default constructor.
     */
    protected DIFVersion() {
    }


    ///////////////////////////////////////////////////////////////////
    ////                       public methods                      ////


    /**
     * Return the version of the DIF package being used.
     * <p>
     * The latest version is <b>1.9.0</b>
     *
     * @return String of the version of the DIF package.
     */
    public static String DIFPackageVersion() {
        return "1.9.0";
    }
}


