/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import dif.util.graph.Edge;
import dif.util.graph.Node;
import dif.util.sched.Firing;
import dif.util.sched.Schedule;
import dif.util.sched.ScheduleElement;

//////////////////////////////////////////////////////////////////////////
//// DIFScheduleUtilities

/**
 * Utilities for DIF schedules. The utility tools are for deriving the
 * associated graph elements from a schedule.
 *
 * @author Mingyung Ko, Chia-Jui Hsu
 * @version $Id: DIFScheduleUtilities.java 606 2008-10-08 16:29:47Z plishker $
 */

public class DIFScheduleUtilities {

    /**
     * A protected constructor to prevent the object to be accidentally
     * created and allow more specialized schedule classes to extend from.
     */
    protected DIFScheduleUtilities() {
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /**
     * Return an actor firing sequence given by the
     * schedule element. The actor firings returned have the same
     * order as
     * {@link ScheduleElement#firingElementIterator()}.
     * Therefore, the functionality can be viewed as a <code>List</code>
     * version of <code>firingElementIterator</code>.
     * <p>
     * Clarification: Actor firings are in the type of
     * {@link Node}, not {@link Firing}.
     *
     * @param scheduleElement The schedule element.
     */
    static public List actorFirings(ScheduleElement scheduleElement) {
        List elementList = new ArrayList();
        Iterator elements = scheduleElement.firingElementIterator();
        while (elements.hasNext()) elementList.add(elements.next());
        return Collections.unmodifiableList(elementList);
    }

    /**
     * Clone a schedule element.
     *
     * @param element The original schedule element.
     * @return The cloned schedule element.
     */
    static public ScheduleElement cloneScheduleElement(
            ScheduleElement element) {

        // If the input element is a firing.
        if (element instanceof Firing) {
            Object firingElement = ((Firing) element).getFiringElement();
            Firing firing = (firingElement == null) ? new Firing() : new Firing(
                    firingElement);
            firing.setIterationCount(element.getIterationCount());
            return firing;
        }

        // If the input element is a schedule.
        Schedule schedule = new Schedule();
        schedule.setIterationCount(element.getIterationCount());
        Iterator elements = ((Schedule) element).iterator();
        while (elements.hasNext()) schedule.add(
                cloneScheduleElement((ScheduleElement) elements.next()));
        return schedule;
    }

    /**
     * Compute the repetitions as if the scheduled actors are treated as a
     * cluster. It is equal to GCD value of all the actors' repetitions.
     * See also
     * {@link dif.csdf.sdf.SDFGraph#clusterRepetitions(Collection)}
     *
     * @param scheduleElement The schedule element.
     * @return The cluster repetitions.
     */
    static public int clusterRepetitions(ScheduleElement scheduleElement) {
        int repetition = scheduleElement.getIterationCount();
        ScheduleElement parent = scheduleElement.getParent();
        if (parent != null) repetition *= clusterRepetitions(parent);
        return repetition;
    }

    /**
     * A simplified code size computation for a given schedule. All actors
     * are assumed to have code size 1, and loop size with 0. This is a
     * good way to get the total number of actor appearances for a schedule.
     *
     * @param sched The schedule.
     * @return The code size.
     */
    static public int codeSize(ScheduleElement sched) {
        Map sizeMap = new HashMap();
        Iterator elements = sched.firingElementIterator();
        while (elements.hasNext()) {
            Object element = elements.next();
            if (!sizeMap.containsKey(element)) sizeMap.put(
                    element, new Integer(1));
        }
        return codeSize(sched, sizeMap, 0);
    }

    /**
     * Compute the (inlined) code size of a given schedule. A map from
     * actors to corresponding code sizes as well as the code size overhead
     * for loops have to be specified.
     *
     * @param sched        The schedule.
     * @param sizeMap      The map from actors to code size (in class
     *                     <code>Integer</code>).
     * @param loopCodeSize The code size for loops.
     * @return The code size.
     */
    static public int codeSize(ScheduleElement sched, Map sizeMap,
                               int loopCodeSize) {

        // Count in code size for a loop if the iterations are more than 1.
        int loopSize = (sched.getIterationCount() > 1) ? loopCodeSize : 0;
        int bodySize = 0;

        // If it is a Firing, get the code size from the map.
        if (sched instanceof Firing) {
            Object actor = ((Firing) sched).getFiringElement();
            bodySize += ((Integer) sizeMap.get(actor)).intValue();
            // If it is a Schedule, sum up the code size of all iterands.
        } else {
            Iterator iterands = ((Schedule) sched).iterator();
            while (iterands.hasNext()) {
                ScheduleElement iterand = (ScheduleElement) iterands.next();
                bodySize += codeSize(iterand, sizeMap, loopCodeSize);
            }
        }
        return (bodySize + loopSize);
    }

    /**
     * Compare two schedules by their flattened actor firings. Two schedules
     * are equivalent in that they share the same <em>number</em> and
     * <em>sequence</em> of actor firings.
     * <p>
     * Clarification: Actor firings are in the type of
     * {@link Node}, not {@link Firing}.
     *
     * @param s1 The first schedule.
     * @param s2 The second schedule.
     * @return True if the two schedules have the same firing sequence; false
     * otherwise.
     */
    static public boolean compareScheduleByActorFirings(ScheduleElement s1,
                                                        ScheduleElement s2) {
        List firings1 = actorFirings(s1);
        List firings2 = actorFirings(s2);
        // Compare the number of actor firings.
        if (firings1.size() != firings2.size()) return false;
        // Compare actor firings of the two schedules.
        boolean equivalence = true;
        for (int i = 0; i < firings1.size(); i++)
            if (firings1.get(i) != firings2.get(i)) {
                equivalence = false;
                break;
            }
        return equivalence;
    }

    /**
     * Compare two schedules by their looping structures. At each loop level,
     * the comparison happens in the order: number of iterations, number of
     * iterands, type of iterand, and iterand body (recursive comparison).
     *
     * @param s1 The first schedule.
     * @param s2 The second schedule.
     * @return True if the two schedules have the same looping hierarchy;
     * false otherwise.
     */
    static public boolean compareScheduleByStructure(ScheduleElement s1,
                                                     ScheduleElement s2) {
        // Compare iterations count.
        if (s1.getIterationCount() != s2.getIterationCount()) return false;
        // Compare schedule type.
        if (!s1.getClass().equals(s2.getClass())) return false;

        // Compare for "Schedule" type
        if (s1 instanceof Schedule) {
            Schedule sched1 = (Schedule) s1;
            Schedule sched2 = (Schedule) s2;

            // Compare iterands count.
            if (sched1.size() != sched2.size()) return false;
            // Compare iterand types.
            for (int i = 0; i < sched1.size(); i++)
                if (!sched1.get(i).getClass().equals(sched2.get(i).getClass()))
                    return false;
            // Recursively compare all iterands.
            for (int i = 0; i < sched1.size(); i++)
                if (compareScheduleByStructure(sched1.get(i), sched2.get(i)) ==
                        false) return false;

            // Compare for "Firing" type
        } else {
            Firing f1 = (Firing) s1;
            Firing f2 = (Firing) s2;
            if (f1.getFiringElement() != f2.getFiringElement()) return false;
        }
        // All comparison lead to true.
        return true;
    }

    /**
     * Get the induced crossing edges between two schedule elements. A
     * schedule element usually represents a cluster of actors. This method
     * is to calculate the crossing edges between two actor clusters induced
     * from the schedule elements. See also
     * {@link #inducedGraph(ScheduleElement, DIFGraph)}.
     *
     * @param sched1 The first schedule element.
     * @param sched2 The second schedule element.
     * @param graph  The original graph in which to get the crossing edges.
     * @return The crossing edges.
     */
    static public Collection crossingEdges(ScheduleElement sched1,
                                           ScheduleElement sched2,
                                           DIFGraph graph) {

        Collection crossingEdges = new HashSet();
        Collection nodes1 = inducedGraph(sched1, graph).nodes();
        Collection nodes2 = inducedGraph(sched2, graph).nodes();
        Iterator edges = graph.edges().iterator();
        while (edges.hasNext()) {
            Edge edge = (Edge) edges.next();
            Node source = edge.source();
            Node sink = edge.sink();
            if ((nodes1.contains(source) && nodes2.contains(sink)) ||
                    (nodes1.contains(sink) && nodes2.contains(source)))
                crossingEdges.add(edge);
        }
        return crossingEdges;
    }

    /**
     * Generate a map that maps Node object to its String name. This
     * method is used for <code>dif.util.sched.Schedule.toParenthesisString(
     * Map, String)</code>.
     * Note that, each node in the input DIFGraph <i>graph</i> should have
     * its name.
     *
     * @param graph The DIFGraph or subclass of DIFGraph instance.
     * @return The map that maps Node object to its String name.
     */
    static public Map generateNameMap(DIFGraph graph) {
        HashMap map = new HashMap();
        for (Iterator nodes = graph.nodes().iterator(); nodes.hasNext(); ) {
            Node node = (Node) nodes.next();
            String name = graph.getName(node);
            map.put(node, name);
        }
        return map;
    }

    /**
     * Get the induced DIF graph from the schedule element. A schedule
     * element indicates the firing sequence of a certain cluster of DIF
     * actors. This method is to get the graph induced by the cluster.
     *
     * @param scheduleElement The schedule element.
     * @param graph           The original/parent graph to induce the subgraph.
     * @return The induced graph.
     */
    static public DIFGraph inducedGraph(ScheduleElement scheduleElement,
                                        DIFGraph graph) {

        Collection nodes = new HashSet();
        // scheduleElement is an instance of Firing
        if (scheduleElement instanceof Firing) {
            nodes.add(((Firing) scheduleElement).getFiringElement());

            // scheduleElement is an instance of Schedule
        } else {
            Iterator firingElements =
                    ((Schedule) scheduleElement).firingElementIterator();
            while (firingElements.hasNext()) {
                Object node = firingElements.next();
                nodes.add(node);
            }
        }
        return (DIFGraph) graph.subgraph(nodes);
    }

    /**
     * Convert a topological sorting order to a schedule for instances of
     * {@link dif.csdf.sdf.SingleRateGraph}. This method does not check
     * any error between the order and the associated graph. Users need to
     * make sure that the order contains the desired graph nodes.
     *
     * @param topSort The topological sorting order.
     * @return A schedule.
     */
    static public Schedule topSortToSchedule(List topSort) {
        Schedule schedule = new Schedule();
        for (int i = 0; i < topSort.size(); i++)
            schedule.add(new Firing(topSort.get(i)));
        return schedule;
    }
}


