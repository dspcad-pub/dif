/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import dif.util.graph.DirectedGraph;
import dif.util.graph.Edge;
import dif.util.graph.Node;

//////////////////////////////////////////////////////////////////////////
//// DIFRandomGraphGenerator

/**
 * Random graph generator in generating random DIF graphs. The generator
 * makes random topology only. Parameters like production rates, consumption
 * rates, and delays are to be determined in the specific class of dataflow.
 * After the object instanciation, every call to {@link #graph(int, int)}
 * generates
 * a new graph with the same node count. Therefore, multiple random graphs
 * can be obtained with only one time object creation.
 *
 * @author Mingyung Ko
 * @version $Id: DIFRandomGraphGenerator.java 606 2008-10-08 16:29:47Z
 * plishker $
 */

public class DIFRandomGraphGenerator {

    /**
     * Null constructor.
     */
    public DIFRandomGraphGenerator() {
        _initialize();
    }

    ///////////////////////////////////////////////////////////////////
    ////                        public methods                     ////

    /**
     * Generate a random DIF graph with the given node count. Presently,
     * only Directed Acyclic Graph (DAG) generation is implemented.
     *
     * @param nodeCount The desired node count.
     * @return The random graph in {@link DIFGraph}.
     */
    public DIFGraph graph(int nodeCount) {
        _reset(nodeCount);
        _makeDAG(2);
        return _graph;
    }

    /**
     * Generate a random DIF graph with the given node count range.Node
     * count is automatically set to 2 in the implementation if the given
     * minimum count is less than 2. The rationale is that data depedence
     * can not happen to graphs with one node only. Presently,
     * only Directed Acyclic Graph (DAG) generation is implemented.
     *
     * @param minNodes The minimal node count.
     * @param maxNodes The maximal node count.
     * @return The random graph in {@link DIFGraph}.
     */
    public DIFGraph graph(int minNodes, int maxNodes) {
        int nodeCount = minNodes + _randomNonNegativeInt(
                maxNodes - minNodes + 1);
        if (nodeCount < 2) nodeCount = 2;
        return graph(nodeCount);
    }

    /**
     * Generate a bunch of DIF graphs. Node count of each graph
     * is randomly determined within the minimum and maximum parameters.
     * The minimal node count and graph count should be at least two.
     *
     * @param minNodes   The minimal node count.
     * @param maxNodes   The maximal node count.
     * @param graphCount The desired number of random graphs.
     * @return Random graphs in array.
     */
    public DIFGraph[] graphs(int minNodes, int maxNodes, int graphCount) {
        DIFGraph[] graphArray = new DIFGraph[graphCount];
        for (int i = 0; i < graphCount; i++)
            graphArray[i] = graph(minNodes, maxNodes);
        return graphArray;
    }

    /**
     * Set the maximal number of edges that can fan into each node.
     *
     * @param maxFanIn The maximal number of fan-in edges.
     */
    public void setMaxFanIn(int maxFanIn) {
        _maxFanIn = maxFanIn;
    }

    /**
     * Set the maximal number of edges that can fan out of each node.
     *
     * @param maxFanOut The maximal number of fan-out edges.
     */
    public void setMaxFanOut(int maxFanOut) {
        _maxFanOut = maxFanOut;
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /**
     * Initialize the generator.
     */
    protected void _initialize() {
        _initialize(new DIFGraph(), new DIFNodeWeight(), new DIFEdgeWeight());
        _edgeId = 0;  //used for applying labels to edges
    }

    /**
     * Initialize the generator.
     *
     * @param graph      The desired dataflow graph type.
     * @param nodeWeight The desired node weight type.
     * @param edgeWeight The desired edge weight type.
     */
    protected void _initialize(DIFGraph graph, Object nodeWeight,
                               Object edgeWeight) {
        _graphClass = graph.getClass();
        _nodeWeightClass = nodeWeight.getClass();
        _edgeWeightClass = edgeWeight.getClass();
        _maxFanIn = _maxFanOut = Integer.MAX_VALUE;
        _random = new Random();
    }

    /**
     * Check the existence of an edge connecting the source to the sink.
     * The order of nodes matters since DIF graphs are directed. It is
     * equivalent to
     * {@link DirectedGraph#edgeExists(Node, Node)}. However,
     * the implementation here is more efficient because an incidence matrix
     * is built along with the random graph generation process. Incidence
     * checking is through retrieving the matrix element rather than
     * a computation from the ground.
     *
     * @param source The source node.
     * @param sink   The sink node.
     * @return True if there is an edge connecting the source to
     * the sink; false otherwise.
     */
    protected boolean _isIncident(Node source, Node sink) {
        int sourceLabel = _graph.nodeLabel(source);
        int sinkLabel = _graph.nodeLabel(sink);
        return _incidence[sourceLabel][sinkLabel];
    }

    /**
     * Check the existence of any path from the source to the sink. The order
     * of nodes matters since DIF graphs are directed. Reference also
     * <code>reachableNodes()</code> in {@link DirectedGraph}.
     * The implementation here is through retreiving matrix elements rather
     * than a computation from the ground.
     *
     * @param source The source node.
     * @param sink   The sink node.
     * @return True if the sink is reachable from the source; false otherwise.
     */
    protected boolean _isReachable(Node source, Node sink) {
        int sourceLabel = _graph.nodeLabel(source);
        int sinkLabel = _graph.nodeLabel(sink);
        return _reachability[sourceLabel][sinkLabel];
    }

    /**
     * Make a radom Directed Acyclic Graph (DAG).
     *
     * @param density The density of the number of edges.
     */
    protected void _makeDAG(int density) {
        // Note the following set of loops cannot generate a non-connected
        // graph because each node will have at least output edges.
        for (int i = 0; i < density; i++) {
            Iterator nodes = _graph.nodes().iterator();
            // Try to pick a neighbor for each node.
            while (nodes.hasNext()) {
                Node node = (Node) nodes.next();
                List candidates = _possibleNeighborsOf(node);
                Node candidate = null;
                boolean foundNeighbor = false;
                // Keep trying until we exhaust the possibilities or we
                // find a valid neighbor that won't introduce a cycle.
                while (candidates.size() > 0) {
                    int randomPick = _randomNonNegativeInt(candidates.size());
                    candidate = (Node) candidates.get(randomPick);
                    // Discard "candidate" since either
                    // (a) it will actually become a neighbor
                    // (b) it introduces a cycle
                    // In both cases, we never want to consider it as a
                    // neighbor candidate for the current node again.
                    candidates.remove(candidate);
                    if (!_isReachable(candidate, node) &&
                            (_fanIn[_graph.nodeLabel(node)] < _maxFanIn) &&
                            (_fanOut[_graph.nodeLabel(node)] < _maxFanOut)) {
                        foundNeighbor = true;
                        break;
                    }
                }
                if (foundNeighbor) {
                    _setReachable(node, candidate);
                    _setIncident(node, candidate);
                    _fanIn[_graph.nodeLabel(node)]++;
                    _fanOut[_graph.nodeLabel(node)]++;
                    try {
                        Edge edge = _graph.addEdge(new Edge(node, candidate,
                                                    _edgeWeightClass
                                                            .newInstance()
                        ));

                        //Give it some label so writer can handle it
                        _graph.setName(edge, "Edge" + _edgeId);
                        _edgeId++;

                    } catch (Exception e) {
                        throw new RuntimeException(
                                "Problem in creating edge weights.");
                    }
                }
            }
        }
    }

    /**
     * Return a random non-negative integer for a given bound.
     *
     * @param upperBound The upper bound (exclusive) of the random number.
     * @return The random non-negative integer.
     */
    protected int _randomNonNegativeInt(int upperBound) {
        int randomNumber = 0;
        try {
            randomNumber = _random.nextInt(upperBound);
        } catch (Exception e) {
            throw new RuntimeException(
                    "Non-negative upper bound requred in generating " +
                            "a random integer.");
        }
        return randomNumber;
    }

    /**
     * Reset all configurations to generate a new random graph.
     *
     * @param nodeCount The desired node count.
     */
    protected void _reset(int nodeCount) {
        _fanIn = new int[nodeCount];
        _fanOut = new int[nodeCount];

        // Create a graph with the desired node weight class.
        _graph = null;
        try {
            _graph = (DIFGraph) _graphClass.newInstance();
            for (int i = 0; i < nodeCount; i++) {
                Node node = _graph.addNodeWeight(
                        _nodeWeightClass.newInstance());
                _graph.setName(node, "Node" + i);
            }
        } catch (Exception e) {
            throw new RuntimeException("Problem in creating node weights.");
        }

        // Reset _candidateNeighbors
        _candidateNeighbors = new HashMap();
        Iterator nodes = _graph.nodes().iterator();
        while (nodes.hasNext()) {
            Node thisNode = (Node) nodes.next();
            List neighbors = new LinkedList(_graph.nodes());
            neighbors.remove(thisNode);
            _candidateNeighbors.put(thisNode, neighbors);
        }

        // Reset matrix variables: _reachability and _incidence.
        _reachability = new boolean[nodeCount][nodeCount];
        _incidence = new boolean[nodeCount][nodeCount];
        for (int i = 0; i < nodeCount; i++)
            for (int j = 0; j < nodeCount; j++) {
                _reachability[i][j] = (i == j) ? true : false;
                _incidence[i][j] = false;
            }
    }

    ///////////////////////////////////////////////////////////////////
    ////                      protected variables                  ////

    /**
     * The random graph.
     */
    protected DIFGraph _graph;

    /**
     * Reachability matrix.
     */
    protected boolean[][] _reachability;

    /**
     * Incidence matrix.
     */
    protected boolean[][] _incidence;

    ///////////////////////////////////////////////////////////////////
    ////                       private methods                     ////

    /*  Get all nodes unconnected yet to the given node. These unconnected
     *  nodes are the possible neighbors.
     *
     *  @param node The given node.
     *  @return The possible neighbors in <code>List</code>
     */
    private List _possibleNeighborsOf(Node node) {
        return (List) _candidateNeighbors.get(node);
    }

    /*  Update incidence for the node pair. The order of nodes matters
     *  since DIF graphs are directed.
     *
     *  @param source The source node.
     *  @param sink The sink node.
     */
    private void _setIncident(Node source, Node sink) {
        int sourceLabel = _graph.nodeLabel(source);
        int sinkLabel = _graph.nodeLabel(sink);
        _incidence[sourceLabel][sinkLabel] = true;
    }

    /*  Update reachability for the node pair. The order of nodes matters
     *  since DIF graphs are directed.
     *
     *  @param source The source node.
     *  @param sink The sink node.
     */
    private void _setReachable(Node source, Node sink) {
        int sourceLabel = _graph.nodeLabel(source);
        int sinkLabel = _graph.nodeLabel(sink);
        _reachability[sourceLabel][sinkLabel] = true;
        // If "i" reaches "source", then "i" reaches anything "sink" reaches.
        for (int i = 0; i < _graph.nodeCount(); i++)
            if (_reachability[i][sourceLabel])
                for (int j = 0; j < _graph.nodeCount(); j++)
                    if (_reachability[sinkLabel][j]) _reachability[i][j] = true;
    }

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    /* Java classes of the appropriate graph and node/edge weight. */
    private Class _graphClass, _nodeWeightClass, _edgeWeightClass;

    /* Maximal edges fanning in and out of a node. By default, there is no
       bound of fanning. */
    private int _maxFanIn, _maxFanOut;

    /* Number of edges fanning in and out of nodes. */
    private int[] _fanIn, _fanOut;

    /* Candidate neighbor nodes of each node. The map maps from a node
       to its candidate neighbors. */
    private Map _candidateNeighbors;

    /* Random number generator. */
    private Random _random;

    /* Edge Id counter */
    private int _edgeId;
}


