/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif.mdsdf;

import dif.DIFGraph;

import dif.data.ExtendedMath;
import dif.data.Fraction;
import dif.data.IntegerArrayMath;

import dif.util.graph.Edge;
import dif.util.graph.Node;

import java.util.HashMap;
import java.util.Iterator;

//////////////////////////////////////////////////////////////////////////
//// MDSDFGraph
/** Information and computations associated with MDSDFGraph.
Multidimensional synchronous dataflow is proposed by E. A. Lee and further 
developed by P. K. Murthy. Please refer to 
"Multidimensional Synchronous Dataflow" by P. K. Murthy and E. A. Lee.
<p>
The algorithm to compute multidimensional repetitions is based on 
Figure 3.5 in "Software Synthesis from Dataflow Graphs" 
by S. S. Bhattacharyya, P. K. Murthy, and E. A. Lee, and is modified from 
that SDF algorithm to support multi-dimensions.
<p>
Mix-dimensionality is supported in the MDSDFGraph, which means the 
multidimensional production rate and consumption rate can have different 
dimensionality and edges can have different dimensionality.
<p>
For synchronizing between mismatched dimensionality, the fewer dimensionality 
is assumed to be lower ones in the larger dimensionality, and the remaining 
dimensions are assumed to be 1.
<p>
@author Chia-Jui Hsu
@version $Id: MDSDFGraph.java 1687 2007-05-08 01:38:09Z jerryhsu $
*/

public class MDSDFGraph extends DIFGraph {

    /** Construct an empty MDSDF graph.
     */
    public MDSDFGraph() {
        super();
    }

    /** Construct an empty MDSDF graph with enough storage allocated for the
     *  specified number of nodes.
     *  @param nodeCount The number of nodes.
     */
    public MDSDFGraph(int nodeCount) {
        super(nodeCount);
    }

    /** Construct an empty MDSDF graph with enough storage allocated for the
     *  specified number of edges, and number of nodes.
     *  @param nodeCount The integer specifying the number of nodes
     *  @param edgeCount The integer specifying the number of edges
     */
    public MDSDFGraph(int nodeCount, int edgeCount) {
        super(nodeCount, edgeCount);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////
    
    /** Compute the dimensionality of the MDSDF graph. The dimensionality of a
     *  MDSDF graph is the maximum dimensionality among all edges.
     *  @return The dimensionality of the MDSDF graph.
     */
    public int computeDimensionality() {
        int dimensionality = 1;
        for (Iterator edges = edges().iterator(); edges.hasNext();) {
            Edge edge = (Edge)edges.next();
            MDSDFEdgeWeight weight = (MDSDFEdgeWeight)edge.getWeight();
            if (dimensionality < weight.getDimensionality()) {
                dimensionality = weight.getDimensionality();
            }
        }
        _dimensionality = dimensionality;
        return _dimensionality;
    }

    /** Compute the multidimensional repetitions of the MDSDF graph.
     *  @return The computed multidimensional repetitions of the MDSDF graph
     *  in <code>HashMap</code>, i.e., 
     *  <code>Node</code> to </code>int[]</code>.
     *  @exception RuntimeException If the MDSDF graph is inconsistent.
     */
    public HashMap computeRepetitions() {
        int dimensionality = getDimensionality();
        int nodeCount = nodeCount();
        _numerators = new int[nodeCount][dimensionality];
        _denominators = new int[nodeCount][dimensionality];
        _lcms = new int[dimensionality];
        _repetitions = new int[nodeCount][dimensionality];
        _repetitionMap = new HashMap();

        // Initialization
        for (int j=0; j<nodeCount; j++) {
            for (int i=0; i<dimensionality; i++) {
                _numerators[j][i] = 0;
                _denominators[j][i] = 0;
                _repetitions[j][i] = 0;
            }
        }
        
        // Initialize node(0)
        for (int i=0; i<dimensionality; i++) {
            _numerators[0][i] = 1;
            _denominators[0][i] = 1;
            _lcms[i] = 1;
        }
        
        // Recursively compute all nodes' repetitions
        _computeNodeRepetition(node(0));

        // Compute smallest integer vector repetitions for each node based
        // on _numerators, _denominators, and _lcms.
        for (int j=0; j<nodeCount; j++) {
            _repetitions[j] = IntegerArrayMath.divideElements(
                    IntegerArrayMath.multiply(_numerators[j], _lcms), 
                    _denominators[j]);
        }

        // create _repetitionMap
        for (int j=0; j<nodeCount; j++) {
            _repetitionMap.put(node(j), _repetitions[j]);
        }
        
        // check consistency
        if (_checkConsistency() == false) {
            throw new RuntimeException("Inconsistent!");
        }

        return _repetitionMap;
    }

    /** Get the computed dimensionality of the MDSDF graph.
     *  The dimensionality of a MDSDF graph is the maximum dimensionality 
     *  among all edges.
     *  @return The dimensionality of the MDSDF graph.
     *  @see #computeDimensionality()
     */
    public int getDimensionality() {
        if (_dimensionality != 0) {
            return _dimensionality;
        }
        return computeDimensionality();
    }

    /** Get the computed multidimensional repetition of <i>node</i>.
     *  The dimensionality of the multidimensional repetition is equal to
     *  the dimensionality of the graph.
     *  @param node
     *  @return The computed multidimensional repetition of <i>node</i>.
     */
    public int[] getRepetition(Node node) {
        return (int[])getRepetitions().get(node);
    }

    /** Get the computed multidimensional repetitions of the MDSDF graph.
     *  The dimensionality of the multidimensional repetition of every node 
     *  is equal to the dimensionality of the graph.
     *  @return The computed multidimensional repetitions of the MDSDF graph
     *  in <code>HashMap</code>, i.e., 
     *  <code>Node</code> to </code>int[]</code>.
     */
    public HashMap getRepetitions() {
        if (_repetitionMap != null) {
            return _repetitionMap;
        }
        return computeRepetitions();
    }
    
    /** Verify edge weight for MDSDF graph.
     *  @param weight The edge weight to verify.
     *  @return True if the given edge weight is valid for MDSDF graph.
     */
    public boolean validEdgeWeight(Object weight) {
        return weight instanceof MDSDFEdgeWeight;
    }

    /** Verify node weight for MDSDF graph.
     *  @param weight The node weight to verify.
     *  @return True if the given node weight is valid for MDSDF graph.
     */
    public boolean validNodeWeight(Object weight) {
        return weight instanceof MDSDFNodeWeight;
    }
    
    ///////////////////////////////////////////////////////////////////
    ////                    protected methods                      ////

    protected boolean _checkConsistency() {
        if (_repetitionMap == null) {
            throw new RuntimeException("Run computeRepetitions() first.");
        }

        int dimensionality = getDimensionality();
        for (Iterator edges = edges().iterator(); edges.hasNext();) {
            Edge edge = (Edge)edges.next();
            Node source = edge.source();
            Node sink = edge.sink();
            MDSDFEdgeWeight weight = (MDSDFEdgeWeight)edge.getWeight();
            int[] prd = weight.getMDSDFProductionRate();
            int[] cns = weight.getMDSDFConsumptionRate();
            int[] srcRep = (int[])_repetitionMap.get(source);
            int[] snkRep = (int[])_repetitionMap.get(sink);

            for (int i=0; i<dimensionality; i++) {
                if (srcRep[i] * (i<prd.length ? prd[i] : 1) != 
                        snkRep[i] * (i<cns.length ? cns[i] : 1)) {
                    return false;
                }
            }
        }

        return true;
    }
    
    protected void _computeNodeRepetition(Node node) {
        int nodeLabel = nodeLabel(node);
        int dimensionality = getDimensionality();
        
        for (Iterator outEdges = outputEdges(node).iterator(); 
                outEdges.hasNext();) {
            Edge outEdge = (Edge)outEdges.next();
            Node sink = outEdge.sink();
            int sinkLabel = nodeLabel(sink);
            
            // if sink is not computed
            if (_numerators[sinkLabel][0] == 0) {
                //set repetitions of sink
                MDSDFEdgeWeight weight = (MDSDFEdgeWeight)outEdge.getWeight();
                int[] prd = weight.getMDSDFProductionRate();
                int[] cns = weight.getMDSDFConsumptionRate();
                for (int i=0; i<dimensionality; i++) {
                    int numerator = _numerators[nodeLabel][i] * 
                            (i<prd.length ? prd[i] : 1);
                    int denominator = _denominators[nodeLabel][i] * 
                            (i<cns.length ? cns[i] : 1);
                    int gcd = ExtendedMath.gcd(numerator, denominator);
                    _numerators[sinkLabel][i] = numerator/gcd;
                    _denominators[sinkLabel][i] = denominator/gcd;
                    // compute lcm of denominators for each dimension
                    _lcms[i] = Fraction.lcm(_lcms[i],
                            _denominators[sinkLabel][i]);
                }
                
                // recursively to sink
                _computeNodeRepetition(sink);
            }
        }

        for (Iterator inEdges = inputEdges(node).iterator();
                inEdges.hasNext();) {
            Edge inEdge = (Edge)inEdges.next();
            Node source = inEdge.source();
            int sourceLabel = nodeLabel(source);

            // if source is not computed
            if (_numerators[sourceLabel][0] == 0) {
                // set repetitions of source
                MDSDFEdgeWeight weight = (MDSDFEdgeWeight)inEdge.getWeight();
                int[] prd = weight.getMDSDFProductionRate();
                int[] cns = weight.getMDSDFConsumptionRate();
                for (int i=0; i<dimensionality; i++) {
                    int numerator = _numerators[nodeLabel][i] * 
                            (i<cns.length ? cns[i] : 1);
                    int denominator = _denominators[nodeLabel][i] *
                            (i<prd.length ? prd[i] : 1);
                    int gcd = ExtendedMath.gcd(numerator, denominator);
                    _numerators[sourceLabel][i] = numerator/gcd;
                    _denominators[sourceLabel][i] = denominator/gcd;
                    // compute lcm of denominators for each dimension
                    _lcms[i] = Fraction.lcm(_lcms[i], 
                            _denominators[sourceLabel][i]);
                }
            }

            // recursively to source
            _computeNodeRepetition(source);
        }
    }
    
    ///////////////////////////////////////////////////////////////////
    ////                    protected variables                    ////

    protected int _dimensionality = 0;
    protected HashMap _repetitionMap = null;
    protected int[][] _repetitions = null;

    ///////////////////////////////////////////////////////////////////
    ////                      private variables                    ////

    private int[][] _numerators = null;
    private int[][] _denominators = null;
    private int[] _lcms = null;
}


