/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/


package dif.mdsdf;

import dif.DIFNodeWeight;
import dif.util.graph.Node;

//////////////////////////////////////////////////////////////////////////
//// MDSDFNodeWeight
/** Information associated with a MDSDF node.
{@link MDSDFNodeWeight}s are objects associated with {@link Node}s
that represent MDSDF nodes in {@link dif.mdsdf.MDSDFGraph}s.
This class caches frequently-used data associated with MDSDF nodes.

@author Chia-Jui Hsu
@version $Id: MDSDFNodeWeight.java 1687 2007-05-08 01:38:09Z jerryhsu $
*/
    
public class MDSDFNodeWeight extends DIFNodeWeight {

    /** Createa a MDSDF node weight with null computation.
     */ 
    public MDSDFNodeWeight() {
        setComputation(null);
    }
    
    /** Create a MDSDF node weight given a computation that is to be 
     *  represented by the MDSDF node.
     *  @param computation the computation to be represented by the MDSDF node.
     */
    public MDSDFNodeWeight(Object computation) {
        setComputation(computation);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Return a string representation of the MDSDF node that is represented
     *  by this node weight. 
     *  @return the string representation.
     */
    public String toString() {
        String result = new String();
        result += "computation: ";
        if (getComputation() == null) {
            result += "null";
        } else {
            if (getComputation() instanceof String) {
                result += getComputation();
            } else {
                result += getComputation().getClass().getName();
            } 
        }
        return result; 
    }

}


