/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif.mdsdf;

import dif.DIFEdgeWeight;

import dif.util.graph.Edge;
import dif.util.graph.GraphWeightException;
import dif.data.ExtendedMath;

//////////////////////////////////////////////////////////////////////////
//// MDSDFEdgeWeight
/** Information associated with an MDSDF edge.
MDSDFEdgeWeights are objects associated with {@link Edge}s
that represent MDSDF edges in {@link dif.mdsdf.MDSDFGraph}s.
This class caches frequently-used data associated with MDSDF edges.
</p>
The type of production rate, consumption rate, and delay is in 
<code>int[]</code>, and can support one- to multi-dimensional representation.
The dimensionality of an edge is the maximum dimensionality of production 
rate and consumption rate. A zero delay is {0}.
</p>
@author Chia-Jui Hsu
@version $Id: MDSDFEdgeWeight.java 1687 2007-05-08 01:38:09Z jerryhsu $
*/

public class MDSDFEdgeWeight extends DIFEdgeWeight {

    /** Construct an edge weight for a homogeneous, zero-delay, 
     *  one-dimensional edge. Production and consumption values are set to [1].
     */
    public MDSDFEdgeWeight() {
        int[] singleRate = {1};
        int[] zeroDelay = {0};
        setMDSDFProductionRate(singleRate);
        setMDSDFConsumptionRate(singleRate);
        setMDSDFDelay(zeroDelay);
        setSinkPort(null);
        setSourcePort(null);
    }

    /** Construct an edge weight for a specified token production rate,
     *  token consumption rate, and delay.
     *  @param productionRate The int[] production rate.
     *  @param consumptionRate The int[] consumption rate.
     *  @param delay The delay.
     */
    public MDSDFEdgeWeight(int[] productionRate,
                          int[] consumptionRate, int[] delay) {
        setMDSDFProductionRate(productionRate);
        setMDSDFConsumptionRate(consumptionRate);
        setMDSDFDelay(delay);
        setSourcePort(null);
        setSinkPort(null);
    }

    /** Construct an edge weight for a specified source port, sink port,
     *  token production rate, token consumption rate, and delay.
     *  The source port and sink port are ports that correspond to this edge.
     *  @param sourcePort The source port.
     *  @param sinkPort The sink port.
     *  @param productionRate The int[] production rate.
     *  @param consumptionRate The int[] consumption rate.
     *  @param delay The delay.
     */
    public MDSDFEdgeWeight(Object sourcePort, Object sinkPort,
                          int[] productionRate,
                          int[] consumptionRate, int[] delay) {
        setMDSDFProductionRate(productionRate);
        setMDSDFConsumptionRate(consumptionRate);
        setMDSDFDelay(delay);
        setSourcePort(sourcePort);
        setSinkPort(sinkPort);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Calculate the dimensionality of a MDSDF edge, i.e., the maximum
     *  dimensionality of production and consumption rates.
     *  @return The dimensionality of a MDSDF edge.
     */
    public int getDimensionality() {
        return Math.max(getMDSDFConsumptionRate().length, 
                getMDSDFProductionRate().length);
    }

    /** Get the MDSDF consumption rate.
     *  @return The MDSDF consumption rate in <code>int[]</code>.
     */
    public int[] getMDSDFConsumptionRate() {
        return (int[])_getConsumptionRate();
    }

    /** Get the consumption rate at dimension <i>dimension</i>.
     *  @param dimension The dimension.
     *  @return The consumption rate at dimension <i>dimension</i>.
     */
    public int getMDSDFConsumptionRate(int dimension) {
        return getMDSDFConsumptionRate()[dimension];
    }

    /** Get the MDSDF production rate.
     *  @return The MDSDF production rate in <code>int[]</code>.
     */
    public int[] getMDSDFProductionRate() {
        return (int[])_getProductionRate();
    }
    
    /** Get the production rate at dimension <i>dimension</i>.
     *  @param dimension The dimension.
     *  @return The production rate at dimension <i>dimension</i>..
     */
    public int getMDSDFProductionRate(int dimension) {
        return getMDSDFProductionRate()[dimension];
    }

    /** Get the MDSDF delay.
     *  @return The delay in <code>int[]</code>.
     */
    public int[] getMDSDFDelay() {
        return (int[])_getDelay();
    }

    /** Get the delay value at dimension <i>dimension</i>.
     *  @param dimension The dimension.
     *  @return The delay at dimension <i>dimension</i>.
     */
    public int getMDSDFDelay(int dimension) {
        return getMDSDFDelay()[dimension];
    }

    /** Get the minimum index space size of this edge in <code>int[]</code>.
     *  The minimum index space size is regardless of multi-dimensional 
     *  repetition vectors. The minimum index space size of en edge <i>e</i>  
     *  is computed as <i>prd(e,i) * cns(e,i) / gcd ( prd(e,i), cns(e,i) )</i>,
     *  where i is the dimension.
     *  @return The minimum index space size of this edge in 
     *  <code>int[]</code>.
     */
    public int[] getMinIndexSpaceSize() {
        int dimensionality = getDimensionality();
        int[] indexSpaceSize = new int[dimensionality];
        for (int i=0; i<dimensionality; i++) {
            int prd = i<getMDSDFProductionRate().length ? 
                    getMDSDFProductionRate()[i] : 1;
            int cns = i<getMDSDFConsumptionRate().length ? 
                    getMDSDFConsumptionRate()[i] : 1;
            indexSpaceSize[i] = (int) prd * cns / ExtendedMath.gcd(prd, cns);
        }
        return indexSpaceSize;
    }

    /** Override {@link DIFEdgeWeight#setConsumptionRate(Object)}
     *  to check the type of <i>consumptionRates</i>, and call 
     *  {@link #setMDSDFConsumptionRate(int[])} to set the MDSDF consumption
     *  rates.
     *  @param consumptionRate The consumption rate in <code>int[]</code>.
     *  @exception GraphWeightException If <i>consumptionRates</i> is not 
     *  instanceof int[].
     */
    public void setConsumptionRate(Object consumptionRate) {
        if (consumptionRate instanceof int[]) {
            setMDSDFConsumptionRate((int[])consumptionRate);
        } else {
            throw new GraphWeightException("consumptionRate is not int[]."
                    + "Please use setMDSDFConsumptionRates(int[]).");
        }
    }

    /** Override {@link DIFEdgeWeight#setDelay(Object)}
     *  to check the type of <i>delay</i>, and call 
     *  {@link #setMDSDFDelay(int[])} to set the MDSDF delay.
     *  @param delay The delay in <code>int[]</code>.
     *  @exception GraphWeightException If <i>delay</i> is not 
     *  instanceof int[].
     */
    public void setDelay(Object delay) {
        if (delay instanceof int[]) {
            setMDSDFDelay((int[])delay);
        } else {
            throw new GraphWeightException("Delay is not int[].");
        }
    }

    /** Set the MDSDF consumption rate.
     *  @param consumptionRate The MDSDF consumption rate in 
     *  <code>int[]</code>.
     *  @exception GraphWeightException If consumption rate in any
     *  dimension is not positive.
     */
    public void setMDSDFConsumptionRate(int[] consumptionRate) {
        for (int i=0; i<consumptionRate.length; i++) {
            if(consumptionRate[i] <= 0) {
                throw new GraphWeightException(
                        "Consumption rate is not positive.");
            }
        }
        _setConsumptionRate(consumptionRate);
    }

    /** Set the consumption rate at dimension <i>dimension</i>.
     *  @param dimension The dimension.
     *  @param consumptionRate The new consumption rate.
     *  @exception GraphWeightException If <i>consumptionRate</i> is not
     *  positive.
     */
    public void setMDSDFConsumptionRate(int dimension, int consumptionRate) {
        if(consumptionRate <= 0) {
            throw new GraphWeightException(
                    "Consumption rate is not positive.");
        }
        getMDSDFConsumptionRate()[dimension] = consumptionRate;
    }

    /** Set the MDSDF production rate.
     *  @param productionRate The MDSDF production rate in 
     *  <code>int[]</code>.
     *  @exception GraphWeightException If production rate in any
     *  dimension is not positive.
     */
    public void setMDSDFProductionRate(int[] productionRate) {
        for (int i=0; i<productionRate.length; i++) {
            if(productionRate[i] <= 0) {
                throw new GraphWeightException(
                        "Production rate is not positive.");
            }
        }
        _setProductionRate(productionRate);
    }

    /** Set the production rate at dimension <i>dimension</i>.
     *  @param dimension The dimension.
     *  @param productionRate The new token production rate.
     *  @exception GraphWeightException If <i>productionRate</i> is not
     *  positive.
     */
    public void setMDSDFProductionRate(int dimension, int productionRate) {
        if(productionRate <= 0) {
            throw new GraphWeightException("Production rate is not positive.");
        }
        getMDSDFProductionRate()[dimension] = productionRate;
    }
    
    /** Set the delay of the associated MDSDF edge.
     *  @param delay The MDSDF delay in <code>int[]</code>.
     *  @exception GraphWeightException If delay in any
     *  dimension is negative.
     */
    public void setMDSDFDelay(int[] delay) {
	for (int i=0; i<delay.length; i++) {
            if(delay[i] < 0) {
                throw new GraphWeightException("Delay is negative.");
            }
        }
        _setDelay(delay);
    }

    /** Override {@link DIFEdgeWeight#setProductionRate(Object)}
     *  to check the type of <i>productionRate</i>, and call 
     *  {@link #setMDSDFProductionRate(int[])} to set the MDSDF production 
     *  rate.
     *  @param productionRate The MDSDF production rate in <code>int[]</code>.
     *  @exception GraphWeightException If <i>productionRate</i> is not 
     *  instanceof int[].
     */
    public void setProductionRate(Object productionRate) {
        if (productionRate instanceof int[]) {
            setMDSDFProductionRate((int[])productionRate);
        } else {
            throw new GraphWeightException("productionRate is not int[]."
                    + "Please use setMDSDFProductionRates(int[]).");
        }
    }

    /** Return a string representation of the edge weight. This string
     *  representation is in the following form:
     *  <p>
     *  <em> [productionRate1 productionRate2 ...]
     *  [consumptionRate1 consumptionRate2 ...] delay </em>
     *  <p>
     * @return the string representation of the edge weight.
     */
    public String toString() {
        StringBuffer str = new StringBuffer("[");
        int i;
        int plength = getMDSDFProductionRate().length;
        int clength = getMDSDFConsumptionRate().length;
        int dlength = getMDSDFDelay().length;

        for (i = 0; i < plength; i++) {
            str.append(getMDSDFProductionRate(i));
            if (i < (plength - 1)) str.append(" ");
        }
        str.append("] [");

        for (i = 0; i < clength; i++) {
            str.append(getMDSDFConsumptionRate(i));
            if (i < (clength - 1)) str.append(" ");
        }
        str.append("] [");
                    
        for (i = 0; i < dlength; i++) {
            str.append(getMDSDFDelay(i));
            if (i < (dlength - 1)) str.append(" ");
        }
        str.append("]");

        return str.toString();
    }
}


