/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/


package dif.mdsdf.mem;

import dif.mdsdf.sched.MDSchedule;
import dif.mdsdf.sched.MDFiring;
import java.util.List;

//////////////////////////////////////////////////////////////////////////
//// MDBufferAccess
/** The class computes information regarding to accessing the
multidimensional buffer. Given a MDSDF graph and a single appearance schedule, 
an edge buffer will be accessed by its source and sink only within 
the least common schedule (the lowest subschedule that contains both 
firings of source and sink).<p>
The schedules of interest of source/sink consists of the subschedules and 
the firing that contain the source/sink, starting one level lower than the 
least common schedule to the firing of the source/sink. All the elements in the 
schedules of interest are {@link dif.mdsdf.sched.MDSchedule}, except the 
last element, which is {@link dif.mdsdf.sched.MDFiring}.
In order to construct the MDBufferAccess, the inputs of the constructor are 
the List of schedules of interest and the production/consumption rate of the 
source/sink.

@author Chia-Jui Hsu
@version $Id: MDBufferAccess.java 1687 2007-05-08 01:38:09Z jerryhsu $
*/

public class MDBufferAccess {
    
    /** Constructor.
     *  @param schedulesOfInterest A List contains the schedulesOfInterest
     *  of the source/sink of an edge.
     *  @param rate The multidimensional production/consumption rate of the
     *  source/sink.
     *  @exception RuntimeException If dimensions are different.
     */
    public MDBufferAccess(List schedulesOfInterest, int[] rate) {        
        _schedulesOfInterest = schedulesOfInterest;
        _level = schedulesOfInterest.size() + 1;
        _dimension = rate.length;
        _rates = new int[_level][_dimension];
        _size = new int[_dimension];
        int i,j;
        int[] iteration;

        for (j=0; j<_dimension; j++) {
            _size[j] = 1;
        }
        
        //subschedules
        for (i=0; i<_level-2; i++) {
            iteration = ((MDSchedule)schedulesOfInterest.get(i))
                    .getMDIterationCount();
            if (iteration.length != _dimension) {
                throw new RuntimeException("Multidimension iterations "
                        + "should be int[] with the same dimension");
            }
            _rates[i] = iteration;
            
            for (j=0; j<_dimension; j++) {
                _size[j] = _size[j] * iteration[j];
            }
        }

        //firing
        iteration = ((MDFiring)schedulesOfInterest.get(_level-2))
                .getMDIterationCount();
        if (iteration.length != _dimension) {
            throw new RuntimeException("Multidimension iterations "
                    + "should be int[] with the same dimension");
        }
        _rates[_level-2] = iteration;
        for (j=0; j<_dimension; j++) {
            _size[j] = _size[j] * iteration[j];
        }

        //production/consumption rate
        _rates[_level-1] = rate;
        for (j=0; j<_dimension; j++) {
            _size[j] = _size[j] * rate[j];
        }        
    }

    ///////////////////////////////////////////////////////////////////
    ////                       public methods                      ////
    
    /** Get the dimension of the buffer.
     *  @return The dimension of the buffer.
     */
    public int dimension() {
        return _dimension;
    }

    /** Get the number of level of buffer access. 
     *  The level is equal to the number of schedules of interest plus 1
     *  (production/consumption rate).
     *  @return The number of level of buffer access.
     */
    public int level() {
        return _level;
    }

    /** Get the access rate (iteration count or production/consumption) 
     *  in <i>level</i>.
     *  @param level From 0 to {@link #level}-1.
     *  @return The access rate in <i>level</i>.
     */
    public int[] rate(int level) {
        if (level < 0 || level > _level-1) {
            throw new RuntimeException("level should between 0 and "
                    + (_level - 1));
        }
        return _rates[level];
    }

    /** Get the access rate (iteration count or production/consumption) in  
     *  <i>dimension</i> in <i>level</i>.
     *  @param level From 0 to {@link #level}-1.
     *  @param dimension From 0 to {@link #dimension}-1.
     *  @return The access rate in <i>dimension</i> in <i>level</i>.
     */
    public int rate(int level, int dimension) {
        if (level < 0 || level > _level-1) {
            throw new RuntimeException("level should between 0 and "
                    + (_level-1));
        }
        if (dimension < 0 || dimension > _dimension-1) {
            throw new RuntimeException("dimension should between 0 and "
                    + (_dimension-1));
        }
        return _rates[level][dimension];
    }

    /** Get the product of rates from <i>startLevel</i> to <i>endLevel</i>
     *  in <i>dimension</i>.
     *  @param startLevel
     *  @param endLevel
     *  @param dimension
     *  @return The product of rates.
     */
    public int rateProduct(int startLevel, int endLevel, int dimension) {
        if (startLevel < 0 || startLevel > _level-1 || startLevel > endLevel) {
            throw new RuntimeException("startLevel should between 0 and "
                    + (_level-1) + ", and smaller than endLevel");
        }
        if (endLevel < 0 || endLevel > _level-1) {
            throw new RuntimeException("endLevel should between 0 and "
                    + (_level-1));
        }
        if (dimension < 0 || dimension > _dimension-1) {
            throw new RuntimeException("dimension should between 0 and "
                    + (_dimension-1));
        }

        int product = 1;
        for (int i=startLevel; i<=endLevel; i++) {
            product = product * _rates[i][dimension];
        }

        return product;
    }

    /** Get the schedules of interest of this buffer access.
     *  The schedules of interest of source/sink consists of the subschedules 
     *  and the firing that contain the source/sink, starting one level lower 
     *  than the least common schedule to the firing of the source/sink.
     *  All the elements in the List are 
     *  {@link dif.mdsdf.sched.MDSchedule}, except the last element, 
     *  which is {@link dif.mdsdf.sched.MDFiring}.
     *  @return A List of schedules of interest.
     */
    public List schedulesOfInterest() {
        return _schedulesOfInterest;
    }
    
    /** Get the multidimensional buffer size based on this access.
     *  @return The buffer size.
     */
    public int[] size() {
        return _size;
    }

    /** Get the buffer size in <i>dimension</i>.
     *  @param dimension From 1 to the dimension of the buffer.
     *  @return The buffer size in <i>dimension</i>.
     */
    public int size(int dimension) {
        if (dimension < 0 || dimension > _dimension-1) {
            throw new RuntimeException("dimension should between 0 and "
                    + (_dimension-1));
        }
        return _size[dimension];
    } 

    ///////////////////////////////////////////////////////////////////
    ////                      private variables                    ////
        
    private int _dimension = 0;
    private int _level = 0;
    private int[][] _rates = null;
    private List _schedulesOfInterest;
    private int[] _size = null;
}


