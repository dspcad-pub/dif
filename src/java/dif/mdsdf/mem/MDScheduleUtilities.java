/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/


package dif.mdsdf.mem;

import dif.mdsdf.sched.MDSchedule;
import dif.mdsdf.sched.MDFiring;

import dif.DIFScheduleUtilities;
import dif.DIFGraph;
import dif.util.Value;
import dif.csdf.sdf.SDFEdgeWeight;
import dif.csdf.sdf.SDFGraph;
import dif.csdf.sdf.sched.ScheduleTree;
import dif.csdf.sdf.sched.ScheduleTreeNode;

import dif.util.sched.ScheduleElement;
import dif.util.sched.Schedule;
import dif.util.sched.Firing;
import dif.util.graph.Edge;
import dif.util.graph.Node;

import java.util.Iterator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.LinkedList;
//import java.util.HashSet;
import java.util.Collection;

//////////////////////////////////////////////////////////////////////////
//// ScheduleUtilities
/** Schedule utilities.
This class extends from {@link DIFScheduleUtilities} for providing
more utilities for computing scheduling, schedule, or schedule tree related 
information.

@author Chia-Jui Hsu
@version $Id: ScheduleUtilities.java 1688 2007-05-09 02:22:30Z jerryhsu $
*/

public class MDScheduleUtilities extends DIFScheduleUtilities {

    /** A private constructor to prevent the object to be accidentally
     *  created.
     */
    private MDScheduleUtilities() {}

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Generate a string expression of <i>schedule</i>.
     *  @param graph
     *  @param schedule
     *  @param delimiter The delimiter to separate between schedules.
     *  @param printOne True to print 1 when iteration count is 1, false to remove the
     *  parenthesis without print 1 when iteration count is 1.
     */
    public static String scheduleString(DIFGraph graph, Schedule schedule, 
            String delimiter, boolean printOne) {
        // use mocgsched.*
        StringBuffer buffer = new StringBuffer();        
        int ite = schedule.getIterationCount();
        if (!(ite == 1 && printOne == false)) {
            buffer.append("(" + delimiter);
            buffer.append(ite);
        }
        Iterator schedEles = schedule.iterator();
        while (schedEles.hasNext()) {
            ScheduleElement schedEle = (ScheduleElement)schedEles.next();
            if (schedEle instanceof Firing) {
                ite = schedEle.getIterationCount();
                if (ite == 1) {
                    buffer.append(graph.getName((Node)((Firing)schedEle).getFiringElement()));
                } else {
                    buffer.append("(");
                    buffer.append(ite);
                    buffer.append(graph.getName((Node)((Firing)schedEle).getFiringElement()));
                    buffer.append(")");
                }                
            } else if (schedEle instanceof Schedule) {
                buffer.append(scheduleString(graph, (Schedule)schedEle, delimiter, printOne));
            }
        }
        if (!(ite == 1 && printOne == false)) {
            buffer.append(")" + delimiter);
        }
        return buffer.toString();
    }

    /** Compute the highest edge among <i>edges</i> in the schedule tree of
     *  <i>schedule</i>.
     *  @param schedule A R-schedule
     *  @param edges A collection of edges.
     */
    static public Edge greatestEdge(Schedule schedule, Collection edges) {
        ScheduleTree tree = new ScheduleTree(schedule);
        int highest = 65536; //Assume that no tree is deeper than 65536 levels
        Edge highestEdge = null;
        for (Iterator edgeIterator = edges.iterator(); 
                edgeIterator.hasNext();) {
            Edge edge = (Edge)edgeIterator.next();            
            Node source = edge.source();
            Node sink = edge.sink();
            ScheduleTreeNode treeNode = tree.leastParent(tree.leafNode(source), 
                    tree.leafNode(sink));
            int depth = treeNode.parents().size();
            if (depth < highest) {
                highest = depth;
                highestEdge = edge;
            }
        }
        if (highestEdge == null) {
            throw new RuntimeException("highest edge is not found.");
        }
        return highestEdge;
    }
    
    /** Calculate the least common schedule of an edge in a given R-schedule 
     *  <i>schedule</i>.
     *  The least common schedule of an edge <i>e</i> is the deepest
     *  schedule of <i>schedule</i> that contains both the firing of 
     *  <i>src(e)</i> and the firing of <i>snk(e)</i>. This least common
     *  schedule is called the schedule of interest of an edge.
     *  It utilizes {@link dif.csdf.sdf.sched.ScheduleTree} to do
     *  this work.
     *  @param graph
     *  @param schedule A R-schedule.
     *  @return A Map maps an edge to its least common scheduel.
     */
    static public Map leastCommonSchedule(DIFGraph graph, Schedule schedule) {
        HashMap scheduleOfInterests = new HashMap();
        ScheduleTree tree = new ScheduleTree(schedule);
        for (Iterator edges = graph.edges().iterator(); edges.hasNext();) {
            Edge edge = (Edge)edges.next();            
            Node source = edge.source();
            Node sink = edge.sink();
            ScheduleTreeNode treeNode = tree.leastParent(tree.leafNode(source), 
                    tree.leafNode(sink));
            Schedule scheduleOfInterest = (Schedule)treeNode.scheduleElement;
            scheduleOfInterests.put(edge, scheduleOfInterest);
        }
        return scheduleOfInterests;
    }

    /** Calculate the least common schedule of an edge in a given SAS-schedule 
     *  <i>schedule</i>.
     *  The least common schedule of an edge <i>e</i> is the deepest
     *  schedule of <i>schedule</i> that contains both the firing of 
     *  <i>src(e)</i> and the firing of <i>snk(e)</i>. This least common
     *  schedule is called the schedule of interest of an edge.
     *  The functionality of this method is equal to 
     *  {@link #leastCommonSchedule}. However, this methos does not use
     *  schedule tree (binary tree) nor require R-schedule.
     *  @param graph
     *  @param schedule A SAS-schedule.
     *  @return A Map maps an edge to its least common scheduel.
     */
    static public Map scheduleOfInterest(DIFGraph graph, Schedule schedule) {
        HashMap scheduleOfInterests = new HashMap();
        HashMap parentsMap = new HashMap();

        //Only meaning for SAS
        List firings = schedule.lexicalOrder();
        for (Iterator firingIter = firings.iterator(); firingIter.hasNext();) {
            Firing firing = (Firing)firingIter.next();
            if (parentsMap.put(firing.getFiringElement(), getParents(firing)) 
                    != null) {
                throw new RuntimeException("The scedule is not SAS");
            }
        }
        
        for (Iterator edges = graph.edges().iterator(); edges.hasNext();) {
            Edge edge = (Edge)edges.next();            
            Node source = edge.source();
            Node sink = edge.sink();
            List srcParents = (List)parentsMap.get(source);
            List snkParents = (List)parentsMap.get(sink);
            if (srcParents == null) {
                throw new RuntimeException("Cannot get the path of firing of "
                        + graph.getName(source));
            }
            if (snkParents == null) {
                throw new RuntimeException("Cannot get the path of firing of "
                        + graph.getName(sink));
            }
            int diverse = -1;
            int depth = (srcParents.size() < snkParents.size()) 
                    ? srcParents.size() : snkParents.size();
            for (int i=0; i<depth; i++) {
                if (srcParents.get(i) != snkParents.get(i)) {
                    diverse = i;
                    break;
                }
            }
            if (diverse == -1) {
                throw new RuntimeException("Cannot find diverse point.");
            }
            
            scheduleOfInterests.put(edge, srcParents.get(diverse-1));
        }

        return scheduleOfInterests;
    }

    /** Get parents of <i>element</i> all the way up to the root.
     *  @param element A ScheduleElement.
     *  @return A LinkedList of parents of <i>element</i>.
     */
    static public List getParents(ScheduleElement element) {
        LinkedList list = new LinkedList();
        ScheduleElement currentElement = element;
        
        while (currentElement.getParent() != null) {            
            list.addFirst(currentElement.getParent());
            currentElement = currentElement.getParent();
        }
        return list;
    }

    /** Calculate buffer size of edges in <i>graph</i> given <i>schedule</i>.
     *  <i>schedule</i> should be a R-schedule and <i>graph</i> should be
     *  sdf and delayless.
     *  This method utilizes the properties of R-schedule to calculate
     *  buffer size. This method does
     *  not simulate the given schedule to get the buffer size.
     *  @param graph A delayless sdf graph.
     *  @param schedule A R-schedule
     *  @exception RuntimeException If <i>schedule</i> is not a R-schedule or 
     *  <i>graph</i> is not delayless.
     */
    static public Map bufferSize(SDFGraph graph, Schedule schedule) {
        ScheduleTree tree = new ScheduleTree(schedule);
        HashMap buffers = new HashMap();

        for (int i = 0; i < graph.edgeCount(); i++) {
            Edge edge = graph.edge(i);
            SDFEdgeWeight weight = (SDFEdgeWeight)edge.getWeight();
            int delay = weight.getIntDelay();
            if (delay != 0) {
                throw new RuntimeException("The graph should be delayless.");
            }
            int production = weight.getSDFProductionRate();
            Node source     = edge.source();
            int repetition = ((SDFGraph)graph).getRepetitions(source);
            int tokensExchanged =
                    production * repetition / _bufferRepetition(tree, edge);
            buffers.put(edge, new Integer(tokensExchanged));
        }

        return buffers;
    }    
    
    /** Clone <i>element</i>. The runtime type of <i>element</i> can be
     *  either <code>dif.util.sched.Schedule</code>,
     *  <code>dif.util.sched.Firing</code>,
     *  <code>dif.mdsdf.sched.MDSchedule</code>, and 
     *  <code>dif.mdsdf.sched.MDFiring</code>.
     *  @param element
     *  @return The cloned object.
     */
    static public ScheduleElement cloneScheduleElement(
            ScheduleElement element) {
        // If the input element is a firing.
        if (element instanceof Firing) {
            Object firingElement = ((Firing)element).getFiringElement();
            Firing firing = (firingElement == null)
                    ? new Firing() : new Firing(firingElement);
            firing.setIterationCount(element.getIterationCount());
            return firing;
        } else if (element instanceof Schedule) {
        // If the input element is a schedule.
            Schedule schedule = new Schedule();
            schedule.setIterationCount(element.getIterationCount());
            Iterator elements = ((Schedule)element).iterator();
            while (elements.hasNext())
                schedule.add(
                    cloneScheduleElement((ScheduleElement)elements.next()));
            return schedule;
        } else if (element instanceof MDFiring) {
        // If the input element is a firing.
            MDFiring firing = new MDFiring();
            Object firingElement = ((MDFiring)element).getFiringElement();
            if (firingElement != null) {
                firing.setFiringElement(firingElement);
            }
            firing.setMDIterationCount((int[])Value.cloneValue(
                        ((MDFiring)element).getMDIterationCount()));
            return firing;
        } else if (element instanceof MDSchedule) {
        // If the input element is a schedule.
            MDSchedule schedule = new MDSchedule(
                ((MDSchedule)element).getDimension());
            schedule.setMDIterationCount((int[])Value.cloneValue(
                ((MDSchedule)element).getMDIterationCount()));
            Iterator elements = ((Schedule)element).iterator();
            while (elements.hasNext())
                schedule.add(
                    cloneScheduleElement((ScheduleElement)elements.next()));
            return schedule;
        }
        return null;
    }

    /** Get the depth of <i>schedule</i>. The depth of root <i>schedule</i> is
     *  1. This method compute the deepest level among all sub-schedules.
     *  Note that the deepest level contains only firings.
     *  @return The depth of <i>schedule</i>.
     */
    static public int depth(Schedule schedule) {
        _level = 1;
        _deepest = 1;
        _depth(schedule);
        return _deepest;
    }
    
    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////
    
    /** Recursively go through <i>schedule</i> and get the depth.
     *  @param schedule
     */
    static private void _depth(Schedule schedule) {
        for (Iterator elements = schedule.iterator(); elements.hasNext();) {
            ScheduleElement element = (ScheduleElement)elements.next();
            if (element instanceof Firing) {
                _level++;
                if (_level > _deepest) {
                    _deepest = _level;
                }
                _level--;
            } else if (element instanceof Schedule) {
                _level++;
                _depth((Schedule)element);
                _level--;
            }
        }
    }

    /** Get repetition counts for the given buffer.
     *  @param edge The given buffer's associated edge.
     *  @return The repetition counts.
     */
    static private int _bufferRepetition(ScheduleTree tree, Edge edge) {
        List parents = tree.parents(tree.leafNode(edge.source()), 
                tree.leafNode(edge.sink()));
        int product = 1;
        for (int i = 0; i < parents.size(); i++) {
            product *= ((ScheduleTreeNode)parents.get(i)).loopCount;
        }
        return product;
    }

    ///////////////////////////////////////////////////////////////////
    ////                      private variables                    ////

    static private int _level;
    static private int _deepest;
}


