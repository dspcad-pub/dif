/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/


package dif.mdsdf.mem;

import dif.mdsdf.MDSDFGraph;
import dif.mdsdf.MDSDFEdgeWeight;
import dif.mdsdf.sched.MDSchedule;
import dif.mdsdf.sched.MDFiring;

import dif.util.graph.Node;
import dif.util.graph.Edge;
import dif.util.sched.ScheduleElement;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Iterator;


//////////////////////////////////////////////////////////////////////////
//// MDBuffers
/** The class computes buffer information of the MDSDF graph.
Given the MDSDFGraph and its corresponding single appearance schedule, 
this class computes buffer size and collects buffer access information.

@author Chia-Jui Hsu
@version $Id: MDBuffers.java 1687 2007-05-08 01:38:09Z jerryhsu $
*/

public class MDBuffers {
    
    /** Constructor.
     *  @param graph
     *  @param schedule
     */
    public MDBuffers(MDSDFGraph graph, MDSchedule schedule) {
        _graph = graph;
        _schedule = schedule;
        _compute();
    }

    ///////////////////////////////////////////////////////////////////
    ////                       public methods                      ////

    /** Get the buffer size of <i>edge</i>.
     *  @param edge
     *  @return The buffer size of <i>edge</i>.
     */
    public int[] size(Edge edge) {
        return ((MDBufferAccess)_srcAccessMap.get(edge)).size();
    }

    /** Get the source buffer access of <i>edge</i>.
     *  @param edge
     *  @return The source buffer access of <i>edge</i>.
     */
    public MDBufferAccess srcAccess(Edge edge) {
        return (MDBufferAccess)_srcAccessMap.get(edge);
    }
    
    /** Get the sink buffer access of <i>edge</i>.
     *  @param edge
     *  @return The sink buffer access of <i>edge</i>.
     */
    public MDBufferAccess snkAccess(Edge edge) {
        return (MDBufferAccess)_snkAccessMap.get(edge);
    }

    ///////////////////////////////////////////////////////////////////
    ////                     protected methods                     ////
    
    /** Compuate buffer size and collects buffer access information of all
     *  edges.
     */
    protected void _compute() {
        for (Iterator firings = _schedule.firingIterator(); 
                firings.hasNext();) {
            MDFiring firing = (MDFiring)firings.next();
            List path = MDScheduleUtilities.getParents(firing);
            path.add(firing);
            Node node = (Node)firing.getFiringElement();
            if (_pathMap.put(node,path) != null) {
                throw new RuntimeException(
                        "The schedule is not single appearance schedule.");
            }
        }
        if (_pathMap.size() != _graph.nodeCount()) {
            throw new RuntimeException(
                    "The schedule does not contains all nodes.");
        }

        for (Iterator edges = _graph.edges().iterator(); edges.hasNext();) {
            Edge edge = (Edge)edges.next();
            Node source = edge.source();
            Node sink = edge.sink();
            List srcPath = (List)_pathMap.get(source);
            List snkPath = (List)_pathMap.get(sink);
            int diverse = -1;
            int length = (srcPath.size() < snkPath.size()) 
                    ? srcPath.size() : snkPath.size();
            for (int i=0; i<length; i++) {
                if (srcPath.get(i) != snkPath.get(i)) {
                    diverse = i;
                    break;
                }
            }
            if (diverse == -1) {
                throw new RuntimeException("Cannot find diverse point of "
                    + _graph.getName(source) + " and " + _graph.getName(sink));
            }
            if (diverse == 0) {
                throw new RuntimeException("The root of "
                    + _graph.getName(source) + " and " + _graph.getName(sink)
                    + " are different.");
            }
            LinkedList srcSchedsOfInterest = new LinkedList();
            for (int i=diverse; i<srcPath.size(); i++) {
                srcSchedsOfInterest.add(srcPath.get(i));
            }
            LinkedList snkSchedsOfInterest = new LinkedList();
            for (int i=diverse; i<snkPath.size(); i++) {
                snkSchedsOfInterest.add(snkPath.get(i));
            }
            int[] production = ((MDSDFEdgeWeight)edge.getWeight())
                    .getMDSDFProductionRate();
            int[] consumption = ((MDSDFEdgeWeight)edge.getWeight())
                    .getMDSDFConsumptionRate();
            MDBufferAccess srcAccess = new MDBufferAccess(
                    srcSchedsOfInterest, production);
            MDBufferAccess snkAccess = new MDBufferAccess(
                    snkSchedsOfInterest, consumption);
            _srcAccessMap.put(edge, srcAccess);
            _snkAccessMap.put(edge, snkAccess);

            int[] sizeBySrc = srcAccess.size();
            int[] sizeBySnk = snkAccess.size();
            for (int j=0; j<_schedule.getDimension(); j++) {
                if (sizeBySrc[j] != sizeBySnk[j]) {
                    throw new RuntimeException("Size computed by source access "
                            + "and sink access are different.");
                }
            }
        }
    }

    static public List getParents(ScheduleElement element) {
        LinkedList list = new LinkedList();
        ScheduleElement currentElement = element;
        
        while (currentElement.getParent() != null) {            
            list.addFirst(currentElement.getParent());
            currentElement = currentElement.getParent();
        }
        return list;
    }

   

    ///////////////////////////////////////////////////////////////////
    ////                     private variables                     ////

    private HashMap _srcAccessMap = new HashMap();
    private HashMap _snkAccessMap = new HashMap();
    private HashMap _pathMap = new HashMap();

    private MDSDFGraph _graph;
    private MDSchedule _schedule;
}


