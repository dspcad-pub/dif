/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/


package dif.mdsdf.sched;

import dif.util.Value;

import dif.util.sched.Firing;

import java.util.Map;

//////////////////////////////////////////////////////////////////////////
//// MDFiring
/** The firing class for multi-dimensional dataflow.
This class is extended from mocgraph.sched.Firing for supporting 
multi-dimensional (MD or m-d) iteration counts. <p>
The "iteration count" is still in one-dimensional because it is inherited 
from <code>mocgraph.sched.ScheduleElement</code>. The new variable 
"multi-dimensional iteration count" is created for specifying 
multi-dimensional iteration count.<p>
Suppose that <i>R = [r1, r2, ..., rM]</i> is a multi-dimensional 
iteration count, where <i>M</i>is the dimensionality. 
Then <i>r = r1 * r2 *  ... * rM</i> is the converted version of
original one-dimensional iteration count.<p>
Use {@link #setMDIterationCount(int[])} and {@link #getMDIterationCount()}
to set and get <i>R = [r1, r2, ..., rM]</i>.
<i>r = r1 * r2 *  ... * rM</i> will automatically be computed.
Using {@link #setIterationCount(int)} will cause an error but
using <code>getIterationCount()</code> inherited from 
<code>mocgraph.sched.ScheduleElement</code> 
will return <i>r = r1 * r2 *  ... * rM</i>.
The multi-dimensional iteration count is stored in <code>int[]</code>.
The dimension of the MDFiring is equal to the <code>int[].length</code> 
of the multi-dimensional iteration count.

@author Chia-Jui Hsu
@version $Id: MDFiring.java 1687 2007-05-08 01:38:09Z jerryhsu $
@see dif.util.sched.Firing
@see dif.util.sched.ScheduleElement
*/

public class MDFiring extends Firing {
    
    /** Construct a multi-dimensional firing with a default multi-dimensional 
     *  iteration count equal to {1} and with no parent schedule.
     */
    public MDFiring() {
        super();
    }

    /** Construct a multi-dimensional firing with a firingElement, 
     *  a multi-dimensional iteration count equal to {1} and no parent 
     *  schedule. A MDFiring constructed using this constructor will only 
     *  accept firing elements with the same class type of the given 
     *  <i>firingElement</i>.
     *  @param firingElement The firing element in the firing.
     */
    public MDFiring(Object firingElement) {
        super(firingElement);
    }

    /** Construct a multi-dimensional firing with a firingElement, 
     *  a multi-dimensional iteration count equal to {1} and no parent 
     *  schedule. A MDFiring constructed using this constructor will only 
     *  accept firing elements with the same class type as 
     *  <i>firingElementClass</i>.
     *  @param firingElementClass The class of the firing element in the firing.
     */
    public MDFiring(Class firingElementClass) {
        super(firingElementClass);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Get the dimension of this MDFiring, which is equal to the length of
     *  the <code>int[]</code> storing the multi-dimension iteration count.
     *  @return The dimension of this MDFiring.
     */
    public int getDimension() {
        return _mdIterationCount.length;
    }
    
    /** Get the multi-dimensional iteration count for this
     *  multi-dimensional firing.
     *  @return The multi-dimensional iteration count for this
     *  multi-dimensional firing.
     */
    public int[] getMDIterationCount() {
        return _mdIterationCount;
    }

    /** Invalidate <code>ScheduleElement.setIterationCount(int)</code>.
     *  Use {@link #setMDIterationCount(int[])} instead.
     *  @param count
     *  @exception MDScheduleException Throws exception in all case.
     */
    public void setIterationCount(int count) {
        throw new MDScheduleException("Use MDFiring.setMDIterationCount(int[])"
                + " to set multi-dimensional iteration count.");
    }
    
    /** Set the multi-dimensional iteration count for this
     *  multi-dimensional firing.
     *  @param iteration The multi-dimensional iteration count for this
     *  multi-dimensional firing.
     */
    public void setMDIterationCount(int[] iteration) {
        _incrementVersion();
        _mdIterationCount = iteration;

        // set ScheduleElement._iterationCount = accumulative multiply of
        // all iteration counts for all dimensions.
        int multiply = 1;
        for (int i=0; i<_mdIterationCount.length; i++) {
            multiply *= _mdIterationCount[i];
        }
        super.setIterationCount(multiply);
    }

    /** Print the firing in a parenthesis style.
     *  @param nameMap A mapping from firing element to its short name.
     *  @param delimiter The delimiter between iteration count and iterand.
     *  @return The parenthesis expression for this firing.
     */
    public String toParenthesisString(Map nameMap, String delimiter) {
        String name = (String)nameMap.get(getFiringElement());
        return "(" 
            + Value.toDIFString(_mdIterationCount) 
            + delimiter + name 
            + ")";
    }

    /** Return a string representation of this Firing.
     *  @return Return a string representation of this Firing.
     */
    public String toString() {
        String result = "Fire firing element " + getFiringElement();
        result += " " + Value.toDIFString(_mdIterationCount) + " times";
        return result;
    }


    ///////////////////////////////////////////////////////////////////
    ////                      private variables                    ////

    private int[] _mdIterationCount = {1};
}


