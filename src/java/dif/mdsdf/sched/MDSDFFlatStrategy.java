/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/


package dif.mdsdf.sched;

import dif.mdsdf.MDSDFGraph;

import dif.DIFScheduleStrategy;

import dif.util.graph.DirectedAcyclicGraph;
import dif.util.graph.DirectedGraph;
import dif.util.graph.Node;
import dif.util.sched.ScheduleTree;

import java.util.HashMap;

//////////////////////////////////////////////////////////////////////////
//// MDSDFFlatStrategy
/** A flat scheduler for MDSDF graphs.
Topological sort the nodes of input graph and repeat each node with its 
repetition. Since a topological sorting is performed, the graph is required 
to be acyclic (see {@link #valid()}).

@author Chia-Jui Hsu
@version $Id: MDSDFFlatStrategy.java 1687 2007-05-08 01:38:09Z jerryhsu $
*/

public class MDSDFFlatStrategy extends DIFScheduleStrategy {

    /** Constructor.
     *  @param graph The given {@link dif.mdsdf.MDSDFGraph} graph.
     */
    public MDSDFFlatStrategy(MDSDFGraph graph) {
        super(graph);
    }

    ///////////////////////////////////////////////////////////////////
    ////                        public methods                     ////

    /** Compute a flat schedule for an acyclic SDF graph.
     *  The order is decided by an generic topplogical sorting result.
     *  @return A flat MDSDF schedule with runtime type {@link MDSchedule}.
     */
    //  public Schedule schedule() {
    public ScheduleTree schedule() {
        int dimension = ((MDSDFGraph)graph()).computeDimensionality();
        HashMap repetitions = ((MDSDFGraph)graph()).computeRepetitions();
        
        DirectedAcyclicGraph acyclicGraph =
                ((MDSDFGraph)graph()).toDirectedAcyclicGraph();
        Object[] sorted = acyclicGraph.topologicalSort();
        
        MDSchedule schedule = new MDSchedule(dimension);
        for (int i = 0; i < sorted.length; i++) {
            // "sorted" is an array of "nodeWeights", not Nodes.
            Node node = ((MDSDFGraph)graph()).node(sorted[i]);
            MDFiring firing = new MDFiring(node);
            firing.setMDIterationCount((int[])repetitions.get(node));
            schedule.add(firing);
        }
	//    return schedule;
	// LIN: this cannot be compiled, removed instead
        //ScheduleTree st = new ScheduleTree(schedule);
        ScheduleTree st = new ScheduleTree(); 
	return st;
    }

    /** A description of flat scheduler.
     *  @return A description of flat scheduler.
     */
    public String toString() {
        return "MDSDF flat scheduler.\n";
    }

    /** Acyclic property is validated for flat scheduling.
     *  @return True if the graph is acyclic.
     */
    public boolean valid() {
        return ((DirectedGraph)graph()).isAcyclic();
    }
}


