/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/


package dif.mdsdf.sched;

//////////////////////////////////////////////////////////////////////////
//// MDScheduleException
/** The runtime exception class for multi-dimensional dataflow scheduling.
It is a runtime exception class extends from java.lang.RuntimeException.
There should be a DIFScheduleException class such that this class can extend 
from it.

@author Chia-Jui Hsu
@version $Id: MDScheduleException.java 1687 2007-05-08 01:38:09Z jerryhsu $
*/

public class MDScheduleException extends RuntimeException {

    /** The default constructor without arguments.
     */
    public MDScheduleException() {}

    /** Constructor with a text description as argument.
     *  @param message The text description of the exception.
     */
    public MDScheduleException(String message) {
        super(message);
    }
}


