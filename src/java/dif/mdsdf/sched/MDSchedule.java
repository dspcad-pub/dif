/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/


package dif.mdsdf.sched;

import dif.util.Value;

import dif.util.sched.Schedule;
import dif.util.sched.ScheduleElement;

import java.util.Map;
import java.util.Iterator;

//////////////////////////////////////////////////////////////////////////
//// MDSchedule
/** The schedule class for multi-dimensional dataflow. This class extends
from <code>mocgraph.sched.Schedule</code> for supporting 
multi-dimensional (MD or m-d) iteration counts.<p>

The "iteration count" is still in one-dimensional because it is inherited 
from <code>mocgraph.sched.ScheduleElement</code>. The new variable 
"multi-dimensional iteration count" is created for specifying 
multi-dimensional iteration count.<p>
Suppose that <i>R = [r1, r2, ..., rM]</i> is a multi-dimensional 
iteration count, where <i>M</i>is the dimensionality. 
Then <i>r = r1 * r2 *  ... * rM</i> is the converted version of
original one-dimensional iteration count.<p>
Use {@link #setMDIterationCount(int[])} and {@link #getMDIterationCount()}
to set and get <i>R = [r1, r2, ..., rM]</i>.
<i>r = r1 * r2 *  ... * rM</i> will automatically be computed.
Using {@link #setIterationCount(int)} will cause an error but
using <code>getIterationCount()</code> 
will return <i>r = r1 * r2 *  ... * rM</i>.<p>

Each schedule element in the MDSchedule must has the same dimension as 
the MDSchedule. The dimension of iteration count of the MDSchedule must be 
also consistent with the dimension of the MDSchedule. As a result, 
{@link MDSchedule(int)} is the most recommended constructor because it sets 
the dimension during initialization. An user can also call 
{@link #setDimension(int)} to set the dimension of the MDSchedule if he uses
other constructors. Once the dimension is set, it cannot be changed.
The reason to force this constraint is to prevent the schedule elements have 
different dimenisons than the parent schedule.<p>

This class represents a multi-dimensional static schedule of firing elements 
invocation. An instance of this class is returned by the scheduler of a 
multi-dimensional dataflow model to represent order of firing element 
firings in the model. A schedule consists of a list of schedule elements 
and the number of times the schedule should repeat.<p>

Each element of the schedule is represented by an instance of the 
{@link MDFiring} or {@link MDSchedule}. Each element may correspond 
to a number of firings of a single firing element (represented by the 
{@link MDFiring} class) or an entire sub-schedule (represented by a 
hierarchically contained instance of this class). This nesting allows this 
concise representation of looped schedules. The nesting can be arbitrarily 
deep, but must be a tree where the leaf nodes represent firings. 
It is up to the scheduler to enforce this requirement.<p>

The add() and remove() methods are used to add or remove schedule elements. 
Only elements of type MDSchedule or MDFiring can be added to the schedule list.
The iteration count is set by the {@link #setMDIterationCount(int[])} method.
If this method is not invoked, a default value of {1} will be used.<p>

Take a look of an example. Suppose that we have a MDSDF graph containing actors
A, B, C, and D, with the firing order 
<code>[1,8]A [8,1]B [2,2]([2,1]C [1,2]D)</code>.
The code to create this schedule appears below.
   <p>
   <pre>
   *       MDSchedule S1 = new MDSchedule(2);
   *       MDFiring FA = new MDFiring();
   *       MDFiring FB = new MDFiring();
   *       MDSchedule S2 = new MDSchedule(2);
   *       MDFiring FC = new MDFiring();
   *       MDFiring FD = new MDFiring();
   *       
   *       FA.setFiringElement(A);
   *       int[] itA = {1,8};
   *       FA.setMDIterationCount(itA);
   *       FB.setFiringElement(B);
   *       int[] itB = {8,1};
   *       FB.setMDIterationCount(itB);
   *       S1.add(FA);
   *       S1.add(FB);
   *       
   *       FC.setFiringElement(C);
   *       int[] itC = {2,1};
   *       FC.setMDIterationCount(itC);
   *       FD.setFiringElement(D);
   *       int[] itD = {1,2};
   *       FD.setMDIterationCount(itD);
   *       S2.add(FC)
   *       S2.add(FD);
   *       int[] itS2 = {2,2};
   *       S2.setMDIterationCount(itS2);
   *       
   *       S1.add(S2);
   </pre>
   <p>
   
@author Chia-Jui Hsu
@version $Id: MDSchedule.java 1687 2007-05-08 01:38:09Z jerryhsu $
@see MDFiring
@see dif.util.sched.Schedule
@see dif.util.sched.ScheduleElement
*/


public class MDSchedule extends Schedule {
    
    /** Construct a multi-dimensional schedule with iteration count {1}
     *  and an empty schedule list.
     */
    public MDSchedule() {
        super();
    }

    /** Construct a multi-dimensional schedule with iteration count {1}
     *  and an empty schedule list. If this constructor is used,
     *  {@link ScheduleElement}s containing firing elements of the given class
     *  would only be accepted as next elements for this schedule.
     *  @param firingElementClass The given class type.
     */
    public MDSchedule(Class firingElementClass) {
        super(firingElementClass);
    }

    /** Construct a multi-dimensional schedule with multi-dimensional 
     *  iteration count set to <code>int[dimension]</code> with all
     *  elements equal to one, and an empty schedule list. Is is very
     *  recommended to use this constructor or use {@link #setDimension(int)}
     *  after initialization.
     *  @param dimension The dimension of this schedule.
     *  @exception MDScheduleException If <i>dimension</i> is less
     *  than one.
     */
    public MDSchedule(int dimension) {
        super();
        setDimension(dimension);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /** Append the specified schedule element to the end of the schedule
     *  list. This element must be an instance of MDFiring or
     *  MDSchedule and has the same dimension.
     *  @param element The MDFiring or MDSchedule object to add.
     *  @exception MDScheduleException If <i>element</i> is not instanceof
     *  MDFiring or MDSchedule, or if <i>element</i> has dimension
     *  different than the dimension of this schedule.
     */
    public void add(ScheduleElement element) {
        if (element instanceof MDFiring) {
            if (((MDFiring)element).getDimension() == getDimension()) {
                super.add(_schedule.size(), element);
            } else {
                throw new MDScheduleException("The dimension of the input "
                        + "element is not equal to the dimension of this "
                        + "schedule.");
            }
        } else if (element instanceof MDSchedule) {
            if (((MDSchedule)element).getDimension() == getDimension()) {
                super.add(_schedule.size(), element);
            } else {
                throw new MDScheduleException("The dimension of the input "
                        + "element is not equal to the dimension of this "
                        + "schedule.");
            }
        } else {
            throw new MDScheduleException("The element is not instanceof "
                    + "MDFiring or MDSchedule.");
        }
    }

    /** Insert the specified schedule element at the specified position in
     *  the schedule list. This element must be an instance of MDFiring or 
     *  MDSchedule and has the same dimension.
     *  @param index The index at which the specified element is to be 
     *  inserted.
     *  @param element The MDFiring or MDSchedule object to add.
     *  @exception if <i>element</i> is not instanceof
     *  MDFiring or MDSchedule, or if <i>element</i> has dimension
     *  different than the dimension of this schedule.
     *  @exception IndexOutOfBoundsException If the specified index is out of
     *   range (index < 0 || index > size()).
     */
    public void add(int index, ScheduleElement element) {
        if (element instanceof MDFiring) {
            if (((MDFiring)element).getDimension() == getDimension()) {
                super.add(index, element);
            } else {
                throw new MDScheduleException("The dimension of the input "
                        + "element is not equal to the dimension of this "
                        + "schedule.");
            }
        } else if (element instanceof MDSchedule) {
            if (((MDSchedule)element).getDimension() == getDimension()) {
                super.add(index, element);
            } else {
                throw new MDScheduleException("The dimension of the input "
                        + "element is not equal to the dimension of this "
                        + "schedule.");
            }
        } else {
            throw new MDScheduleException("The element is not instanceof "
                    + "MDFiring or MDSchedule.");
        }
    }

    /** Get the dimension of this MDSchedule.
     *  @return The dimension of this MDFiring.
     */
    public int getDimension() {
        return _dimension;
    }
    
    /** Get the multi-dimensional iteration count for this
     *  multi-dimensional schedule.
     *  @return The multi-dimensional iteration count for this
     *  multi-dimensional schedule.
     */
    public int[] getMDIterationCount() {
        return _mdIterationCount;
    }

    /** Set the dimension of this schedule. If the existing
     *  multi-dimensional iteration count is in the same dimension, it is
     *  remain unchanged. Otherwise, the multi-dimensional iteration count
     *  will be set to a new <code>int[dimension]</code> with all elements
     *  equal to one.
     *  @param dimension The new dimension of this schedule.
     *  @exception MDScheduleException If <i>dimension</i> is less
     *  than one or the dimension of this schedule has been set.
     */
    public void setDimension(int dimension) {
        if (_setFlag == true) {
            throw new MDScheduleException("The dimension of this schedule "
                    + "has already been set and cannot be modified.");
        }
        if (dimension < 1) {
            throw new MDScheduleException("Dimension is less than one.");
        }

        _dimension = dimension;
        if (_mdIterationCount.length != _dimension) {
            _mdIterationCount = new int[_dimension];
            for (int i=0; i<_dimension; i++) {
                _mdIterationCount[i] = 1;
            }
        }

        _setFlag = true;
    }

    /** Invalidate <code>ScheduleElement.setIterationCount(int)</code>.
     *  Use {@link #setMDIterationCount(int[])} instead.
     *  @param count
     *  @exception MDScheduleException Throws exception in all case.
     */
    public void setIterationCount(int count) {
        throw new MDScheduleException(
                "Use MDSchedule.setMDIterationCount(int[])"
                + " to set multi-dimensional iteration count.");
    }
    
    /** Set the multi-dimensional iteration count for this
     *  multi-dimensional schedule.
     *  @param iteration The multi-dimensional iteration count for this
     *  multi-dimensional firing.
     *  @exception MDScheduleException If <i>iteration</i> is not equal to
     *  the dimension of this schedule.
     */
    public void setMDIterationCount(int[] iteration) {
        if (iteration.length != _dimension) {
            throw new MDScheduleException("The dimension of the input "
                    + "iteration is not equal to the dimension of this "
                    + "schedule. Please input the proper iteration or "
                    + "call setDimension(int) first.");
        }
        
        _incrementVersion();
        _mdIterationCount = iteration;

        // set ScheduleElement._iterationCount = accumulative
        // multiplication of all iteration counts for all dimensions.
        int multiply = 1;
        for (int i=0; i<_mdIterationCount.length; i++) {
            multiply *= _mdIterationCount[i];
        }
        super.setIterationCount(multiply);
    }
    
    /** Print the schedule in a nested parenthesis style.
     *  @param nameMap The map from firing elements to their short names.
     *  @param delimiter The delimiter between iterands.
     *  @return A nested parenthesis expression of the schedule.
     */
    public String toParenthesisString(Map nameMap, String delimiter) {
        String result = new String() + "(";
        result += Value.toDIFString(getMDIterationCount());
        Iterator schedules = iterator();
        while (schedules.hasNext()) {
            ScheduleElement schedule = (ScheduleElement)schedules.next();
            result += delimiter
                + schedule.toParenthesisString(nameMap, delimiter);
        }
        result += ")";
        return result;
    }

    /** Output a string representation of this Schedule.
     *
     *  @return Return a string representation of this Schedule.
     */
    public String toString() {
        String result = "Execute Schedule {\n";
        Iterator elements = iterator();
        while (elements.hasNext()) {
            ScheduleElement element = (ScheduleElement)elements.next();
            result += element.toString() + "\n";
        }
        result += "}";
        result += " " + Value.toDIFString(getMDIterationCount()) 
                + " times";
        return result;
    }

    ///////////////////////////////////////////////////////////////////
    ////                      private variables                    ////

    private int _dimension = 1;
    private int[] _mdIterationCount = {1};
    private boolean _setFlag = false;
}


