/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif;

import dif.util.graph.Element;

import dif.graph.HierarchyToDot;

//////////////////////////////////////////////////////////////////////////
//// DIFHierarchyToDot

/**
 * DOT file generator for DIFHierarchy objects. It is used to create dot files
 * as an input to GraphViz tools. A dot file is created by first defining an
 * DIFHierarchy object and then using the {@link #toFile} method. Underlying
 * hierarchy object is cached during the constructor call and cannot be changed
 * afterwards.
 *
 * @author Fuat Keceli
 * @version $Id: DIFHierarchyToDot.java 606 2008-10-08 16:29:47Z plishker $
 */

public class DIFHierarchyToDot extends HierarchyToDot {

    protected String _elementName(Element element) {
        return ((DIFGraph) _hierarchy.getGraph()).getName(element);
    }
}


