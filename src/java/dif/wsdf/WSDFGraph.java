/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/
package dif.wsdf;

import dif.DIFGraph;

/**
 * WSDFGraph class caches frequently used attributes of WSDF model,
 * including the window size and step size. Current WSDF model is limited to 2D.
 */
public class WSDFGraph extends DIFGraph {
    /**
     * Construct an empty WSDF graph.
     */
    public WSDFGraph() {
        super();
    }

    /**
     * Construct an empty WSDF graph with enough storage allocated for the
     * specified number of nodes.
     *
     * @param nodeCount the number of WSDF node
     */
    public WSDFGraph(int nodeCount) {
        super(nodeCount);
    }

    /**
     * Construct an empty WSDF graph with enough storage allocated for the
     * specified number of edges, and number of nodes.
     *
     * @param nodeCount The integer specifying the number of nodes
     * @param edgeCount The integer specifying the number of edges
     */
    public WSDFGraph(int nodeCount, int edgeCount) {
        super(nodeCount, edgeCount);
    }

    /**
     * Verify edge weight for WSDF graph.
     *
     * @param weight The edge weight to verify.
     * @return True if the given edge weight is valid for WSDF graph.
     */
    public boolean validEdgeWeight(Object weight) {
        return weight instanceof WSDFEdgeWeight;
    }

    /**
     * Verify node weight for WSDF graph.
     *
     * @param weight The node weight to verify.
     * @return True if the given node weight is valid for WSDF graph.
     */
    public boolean validNodeWeight(Object weight) {
        return weight instanceof WSDFNodeWeight;
    }
}
