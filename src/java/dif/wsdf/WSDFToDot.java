/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/
package dif.wsdf;

import dif.DIFAttribute;
import dif.DIFToDot;
import dif.util.graph.Node;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * DOT file generator for WSDFGraph objects. It is used to create dot files as
 * an input to GraphViz tools. A DOT file is created by first defining an
 * WSDFToDot object and then using the {@link #toFile} method. Node labels are set
 * to element names in DIFGraph and edge labels are set to production, consumption
 * and delay values.
 *
 * @author Jiahao Wu
 */

public class WSDFToDot extends DIFToDot {
    /**
     * Creates a DotGenerator object from a DIFGraph.
     *
     * @param graph A DIFGraph.
     */

    public WSDFToDot(WSDFGraph graph) {
        super(graph);
        String subsystem = null;
        Set<String> parameterNames = new HashSet<>(graph.getParameterNames());
        if (parameterNames.contains("subsystem")) {
            subsystem = (String) graph.getParameter("subsystem").getValue();
        }

        for (Iterator nodes = _graph.nodes().iterator(); nodes.hasNext(); ) {
            Node node = (Node) nodes.next();
            if (graph.getAttribute(node, "sequential") != null) {
                DIFAttribute seq = graph.getAttribute(node, "sequential");
                if ("Boolean".equals(seq.getType()) && "true".equals((String) seq.getValue())) {
                    setAttribute(node, "shape", "cds");
                    setAttribute(node, "fillcolor", "cadetblue1");
                    setAttribute(node, "style", "filled");
                }
            }
        }

        if (subsystem != null && subsystem.equals("HTGS")) {
            for (Iterator nodes = _graph.nodes().iterator(); nodes.hasNext(); ) {
                Node node = (Node) nodes.next();
                String type = (String) (graph.getAttribute(node, "type").getValue());
                if (type.equals("BKTask")) {
                    setAttribute(node, "shape", "Msquare");
                    setAttribute(node, "fillcolor", "aquamarine");
                    setAttribute(node, "style", "filled");
                } else if (type.equals("HTGSTask")) {
                    setAttribute(node, "shape", "invhouse");
                    setAttribute(node, "fillcolor", "lightyellow");
                    setAttribute(node, "style", "filled");
                }
            }
        }
    }
}
