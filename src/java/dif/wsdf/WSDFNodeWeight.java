/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/
package dif.wsdf;


import dif.DIFNodeWeight;

/**
 * Information associated with a WSDF node.
 * {@link WSDFNodeWeight}s are objects associated with {@link dif.util.graph.Node}s
 * that represent WSDF nodes in {@link dif.wsdf.WSDFGraph}s.
 * This class caches frequently-used data associated with WSDF nodes.
 */
public class WSDFNodeWeight extends DIFNodeWeight {
    /**
     * Createa a WSDF node weight with null computation.
     */
    public WSDFNodeWeight() {
        setComputation(null);
    }

    /**
     * Create a WSDF node weight given a computation that is to be
     * represented by the WSDF node.
     *
     * @param computation the computation to be represented by the WSDF node.
     */
    public WSDFNodeWeight(Object computation) {
        setComputation(computation);
    }

    /**
     * Return a string representation of the WSDF node that is represented
     * by this node weight.
     *
     * @return the string representation.
     */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("computation: ");

        if (getComputation() == null) {
            sb.append("null");
        } else {
            if (getComputation() instanceof String) {
                sb.append(getComputation());
            } else {
                sb.append(getComputation().getClass().getName());
            }
        }
        return sb.toString();
    }
}
