/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
package dif.difml.parser;

import dif.DIFHierarchy;
import dif.DIFLoader;
import dif.lang.DIFLanguageException;

import java.io.IOException;

public class DIFMLParser {

    /**
     * The DIFML parser implement bi-directional transfer between DIF
     * format and DIFML format.
     * The name of the generated DIFML/DIF file is the same as the
     * input DIF/DIFML file.
     *
     * @param args - option to choose from either "-diftodifml"
     *             or "-difmltodif". Followed by the input filename
     */
    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        if (args.length > 1) {
            String option = args[0];
            String fileName = args[1];
            String outputFileName = fileName.substring(0, fileName
                    .lastIndexOf("."));
            if (option.equals("-diftodifml")) {
                //TODO add multi-graph support to dif -> difml
                outputFileName = outputFileName + ".difml";
                DIFMLWriter writer = new DIFMLWriter(outputFileName);
                writer.write(DIFLoader.loadDataflowList(fileName));

            } else if (option.equals("-difmltodif")) {
                //TODO add multi graphs to difml -> dif
                outputFileName = outputFileName + ".dif";
                try {
                    DIFMLReader reader = new DIFMLReader(fileName);
                    reader.read();
                    //FIXME writer.compile(difhie) output strings, change reader.getHierarchy() to getHierarchies()
                    //FIXME to write multiple dif dataflow into one file
                    DIFHierarchy difhie = reader.getHierarchy();
                    XMLtoDIFWriter writer = new XMLtoDIFWriter(reader.getDIFType());
                    reader.write(outputFileName, writer.compile(difhie));
                    reader.getHierarchy().toString();
                } catch (IOException | DIFLanguageException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("Please choose from one of the options:\n");
                System.out.println("-diftodifml\n");
                System.out.println("-difmltodif\n");
            }
        }
    }
}
