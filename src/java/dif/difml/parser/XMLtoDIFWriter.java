/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.difml.parser;

import dif.*;
import dif.attributes.IntervalCollection;
import dif.graph.hierarchy.Port;
import dif.lang.DIFLanguageException;
import dif.util.Value;
import dif.util.graph.Edge;
import dif.util.graph.Node;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;

//////////////////////////////////////////////////////////////////////////
////  XMLtoDIFWriter

/**
 * XMLtoDIFWriter class.
 * XMLtoDIFWriter class for XML file transformed into DIF file.<p>
 * <p>
 * This class compiles a DIFHierarchy instance to dif specification.
 * It traces down to lowest level hierarchies and all subhierarchies
 * of the given hierarchy are compiled into dif specificaitons.<p>
 * <p>
 * The name of graph definition in the dif specificaiton is from
 * {@link dif.graph.hierarchy.Hierarchy#getName}
 * not from {@link DIFGraph#getName}.<p>
 *
 * @author Chia-Jui Hsu, Ruirui Gu
 * @version $Id: DIFWriter.java 606 2008-10-08 16:29:47Z plishker $
 * @see DIFMLNode
 * @see DIFMLNodeFactory
 * @see DIFMLWriter
 * @see DIFMLReader
 */

public class XMLtoDIFWriter {

    protected DIFGraph _graph;
    protected DIFHierarchy _hierarchy;


    ///////////////////////////////////////////////////////////////////
    ////                       public methods                      ////   
    /**
     * Constructor.
     */
    private String DIFType;
    private StringBuffer _commonBuffer;
    private HashSet _usedLabels;

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    public XMLtoDIFWriter(String difType) {
        DIFType = difType.toString();
    }

    /**
     * Compile the single level hierarchy to dif specification.
     *
     * @throws DIFLanguageException If the runtime type of the graph is
     *                              not the _getEmptyGraph() or encounter DIF language error in compiling.
     */
    public String compile(DIFHierarchy hierarchy)
            throws DIFLanguageException {

        //Initialize
        _commonBuffer = new StringBuffer();
        _usedLabels = new HashSet();
        _hierarchy = hierarchy;
        _graph = (DIFGraph) hierarchy.getGraph();

        //Graph Header.
        //use _hierarchy.getName() as graph(hierarchy) name.
        String graphName = _hierarchy.getName();
        _checkLabel(graphName);
        _commonBuffer.append(_graphType() + " " + graphName + " {\n");

        String difString;
        //topology
        difString = _writeTopology();
        if (difString != null) {
            _commonBuffer.append(difString);
        }
        //interface
        difString = _writeInterface();
        if (difString != null) {
            _commonBuffer.append(difString);
        }
        //parameter
        difString = _writeParameter();
        if (difString != null) {
            _commonBuffer.append(difString);
        }
        //refinement
        difString = _writeRefinement();
        if (difString != null) {
            _commonBuffer.append(difString);
        }
        //built-in attribute
        difString = _writeBuiltinAttribute();
        if (difString != null) {
            _commonBuffer.append(difString);
        }
        //user-defined attribute
        difString = _writeUserDefAttribute();
        if (difString != null) {
            _commonBuffer.append(difString);
        }
        //actor
        difString = _writeActor();
        if (difString != null) {
            _commonBuffer.append(difString);
        }

        _commonBuffer.append("}\n");

        return _commonBuffer.toString();
    }

    /**
     * Get the empty graph with runtime type supported by this class or
     * the derived classes.
     */
    public DIFGraph getSupportedGraph() {
        return _getEmptyGraph();
    }

    /**
     * Convert Object <i>value</i> to string. <i>value</i> can be instanceof:
     * Integer, Double,
     * Complex, String, Boolean, int[], double[],
     * Complex[], int[][], double[][], Complex[][], ArrayList.
     *
     * @param value
     * @return A converted string.
     */
    public String objectToString(Object value) {
        String returnString = Value.toDIFString(value);
        if (returnString != null) {
            return returnString;
        } else {
            System.out.println("Object type is not support\n."
                    + "So return a string: " + value.toString());
            return value.toString();
        }
    }

    /**
     * Output blank spaces.
     *
     * @param num Number of white spaces.
     * @return A StringBuffer of white spaces.
     */
    protected StringBuffer _blank(int num) {
        StringBuffer buffer = new StringBuffer();
        for (int i = 1; i <= num; i++) {
            buffer.append(' ');
        }
        return buffer;
    }

    /** Checks if a string is a keyword in DIF. Keywords should not be used as
     *  labels in graphs.
     *  @param word A string to be checked.
     *  @return True if the string is a keyword in DIF.
     */
 /*  protected boolean _isKeyWord(String word) {
        return LanguageAnalysis.isKeyWord(word);
    } */

    /**
     * Check to see if the name conflicts with reserved labels or not.
     *
     * @param label The string need to be checked.
     * @throws DIFLanguageException If <i>label</i> is a keyword.
     */
    protected void _checkLabel(String label) throws DIFLanguageException {
       /* if (_isKeyWord(label)) {
            throw new DIFLanguageException("Attempt to use a reserved word as"
                    + " a label: " + label + ".");
        } */
    }

    /**
     * Returns an empty graph with the runtime type of the graphs that are
     * going to be written by this writer. This is used for a class check
     * in the constructor to prevent usage of incompatible graph types.
     *
     * @return An empty DIFGraph.
     */
    protected DIFGraph _getEmptyGraph() {
        return new DIFGraph();
    }

    /**
     * Returns a string containing the graph type keyword to be written into
     * the dif file. It is "dif" for this implementation.
     *
     * @return A string.
     */
    protected String _graphType() {
        if (DIFType.equals("SDFGraph")) {
            return "sdf";
        } else if (DIFType.equals("CFDFGraph")) {
            return "cfdf";
        } else if (DIFType.equals("CSDFGraph")) {
            return "csdf";
        } else if (DIFType.equals("BDFGraph")) {
            return "bdf";
        } else {
            return "dif";
        }
    }

    /**
     * Get object name.
     *
     * @param reference An object of
     *                  Node/Edge/Port/DIFParameter or a LinkedList of above.
     * @return The name of that object or a list of names.
     * If it cannot find any one of the object's name in the LinkedList,
     * it returns null.
     */
    protected String _getObjectName(Object reference) {
        if (reference instanceof Edge) {
            return _graph.getName((Edge) reference);
        } else if (reference instanceof Node) {
            return _graph.getName((Node) reference);
        } else if (reference instanceof Port) {
            return ((Port) reference).getName();
        } else if (reference instanceof DIFParameter) {
            return ((DIFParameter) reference).getName();
        } else if (reference instanceof LinkedList) {
            StringBuffer listBuf = new StringBuffer();
            for (Iterator objs = ((LinkedList) reference).iterator();
                 objs.hasNext(); ) {
                Object obj = objs.next();
                String objId = _getObjectName(obj);
                if (objId == null) {
                    //A null return cause the calling method to throw
                    //exception
                    return null;
                }
                listBuf.append(objId);
                listBuf.append(", ");
            }
            listBuf.delete(listBuf.lastIndexOf(", "),
                    listBuf.lastIndexOf(", ") + 2);
            return listBuf.toString();
        }
        return null;
    }

    /**
     * Generate the actor block.
     *
     * @return The DIF specification of the actor block .
     */
    protected String _writeActor() throws DIFLanguageException {
        Iterator nodes = _graph.nodes().iterator();
        if (nodes.hasNext() == false) {
            return null;
        }

        StringBuffer buffer = new StringBuffer();

        while (nodes.hasNext()) {
            Node node = (Node) nodes.next();
            Iterator attributes = _graph.getAttributes(node).iterator();
            if (attributes.hasNext()) {
                buffer.append(_blank(4) + "actor " + _graph.getName(node)
                        + " {\n");

                while (attributes.hasNext()) {
                    DIFAttribute attribute = (DIFAttribute) attributes.next();

                    if (attribute.getValue() != null &&
                            Value.isValue(attribute.getValue())) {

                        buffer.append(_blank(8) + attribute.getName());
                      /*  if (attribute.getType() != null
                                && attribute.getDataType() == null) {
                            buffer.append(" : " + attribute.getType());
                        } else if (attribute.getType() == null
                                && attribute.getDataType() != null){
                            buffer.append(" :: " + "\""
                                    + attribute.getDataType() + "\"");
                        } else if (attribute.getType() != null
                                && attribute.getDataType() != null) {
                            buffer.append(" : " + attribute.getType()
                                    + " : " + "\""
                                    + attribute.getDataType() + "\"");
                        } */
                        if ((attribute.getName()).equals("computation")) {
                            buffer.append(" = "
                                    + objectToString(attribute.getValue())
                                    + ";\n");
                        } else {
                            buffer.append(" = "
                                    + attribute.getValue()
                                    + ";\n");
                        }

                    } else if (attribute.getValue() != null) {
                        String referenceId = _getObjectName(
                                attribute.getValue());
                        if (referenceId == null) {
                            throw new DIFLanguageException(
                                    "Cannot find name of "
                                            + attribute.getValue().toString());
                        }

                        buffer.append(_blank(8) + attribute.getName());
                   /*     if (attribute.getType() != null
                                && attribute.getDataType() == null) {
                            buffer.append(" : " + attribute.getType());
                        } else if (attribute.getType() == null
                                && attribute.getDataType() != null){
                            buffer.append(" :: " + attribute.getDataType());
                        } else if (attribute.getType() != null
                                && attribute.getDataType() != null) {
                            buffer.append(" : " + attribute.getType()
                                    + " : " + attribute.getDataType());
                        } */
                        buffer.append(" = " + referenceId + ";\n");
                    }
                }
                buffer.append(_blank(4) + "}\n");    //actor{}*/
            }
        }

        return buffer.toString();
    }

    /**
     * Generate the built-in attribute block.
     * The subpackage of dif package should have a xxxToDIFWriter.java which
     * inherits this class and override this method for the built-in
     * attribute of that dataflow graph.
     *
     * @return The DIF specification of the built-in attribute block.
     * @throws DIFLanguageException
     */
    protected String _writeBuiltinAttribute() throws DIFLanguageException {
        StringBuffer buffer1 = new StringBuffer();
        buffer1.append(_blank(4) + "production {\n");
        StringBuffer buffer2 = new StringBuffer();
        buffer2.append(_blank(4) + "consumption {\n");
        StringBuffer buffer3 = new StringBuffer();
        buffer3.append(_blank(4) + "delay {\n");

        for (Iterator edges = _graph.edges().iterator(); edges.hasNext(); ) {
            Edge edge = (Edge) edges.next();
            String edgeId = _graph.getName(edge);
            DIFEdgeWeight weight = (DIFEdgeWeight) edge.getWeight();
            Object production = weight.getProductionRate();
            Object consumption = weight.getConsumptionRate();
            Object delay = weight.getDelay();
            if (production != null) {
                if (Value.isValue(production)) {
                    buffer1.append(_blank(8) + edgeId + " = "
                            + objectToString(production) + ";\n");
                } else if (production instanceof DIFParameter) {
                    buffer1.append(_blank(8) + edgeId + " = "
                            + ((DIFParameter) production).getName() + ";\n");
                } else {
                    throw new DIFLanguageException(
                            "Production type is not supported in DIF."
                                    + "\nDump of the attribute is as follows: "
                                    + production.toString());
                }
            }
            if (consumption != null) {
                if (Value.isValue(consumption)) {
                    buffer2.append(_blank(8) + edgeId + " = "
                            + objectToString(consumption) + ";\n");
                } else if (consumption instanceof DIFParameter) {
                    buffer2.append(_blank(8) + edgeId + " = "
                            + ((DIFParameter) consumption).getName() + ";\n");
                } else {
                    throw new DIFLanguageException(
                            "Consumption type is not supported in DIF."
                                    + "\nDump of the attribute is as follows: "
                                    + consumption.toString());
                }
            }
            if (delay != null) {
                if (Value.isValue(delay)) {
                    buffer3.append(_blank(8) + edgeId + " = "
                            + objectToString(delay) + ";\n");
                } else if (delay instanceof DIFParameter) {
                    buffer3.append(_blank(8) + edgeId + " = "
                            + ((DIFParameter) delay).getName() + ";\n");
                } else {
                    throw new DIFLanguageException(
                            "Delay type is not supported in DIF."
                                    + "\nDump of the attribute is as follows: "
                                    + delay.toString());
                }
            }
        }

        for (Iterator ports = _hierarchy.getPorts().iterator();
             ports.hasNext(); ) {
            Port port = (Port) ports.next();
            DIFAttribute attribute;
            if (port.getDirection() == Port.IN) {
                attribute = _hierarchy.getAttribute(port, "consumption");
                if (attribute != null) {
                    Object consumption = attribute.getValue();
                    if (consumption != null) {
                        if (Value.isValue(consumption)) {
                            buffer2.append(_blank(8) + port.getName() + " = "
                                    + objectToString(consumption) + ";\n");
                        } else if (consumption instanceof DIFParameter) {
                            buffer2.append(_blank(8) + port.getName() + " = "
                                    + ((DIFParameter) consumption).getName()
                                    + ";\n");
                        } else {
                            throw new DIFLanguageException(
                                    "Consumption type is not supported in DIF."
                                            + "\nDump of the attribute is as follows: "
                                            + consumption.toString());
                        }
                    }
                }
            } else if (port.getDirection() == Port.OUT) {
                attribute = _hierarchy.getAttribute(port, "production");
                if (attribute != null) {
                    Object production = attribute.getValue();
                    if (production != null) {
                        if (Value.isValue(production)) {
                            buffer1.append(_blank(8) + port.getName() + " = "
                                    + objectToString(production) + ";\n");
                        } else if (production instanceof DIFParameter) {
                            buffer1.append(_blank(8) + port.getName() + " = "
                                    + ((DIFParameter) production).getName()
                                    + ";\n");
                        } else {
                            throw new DIFLanguageException(
                                    "Production type is not supported in DIF."
                                            + "\nDump of the attribute is as follows: "
                                            + production.toString());
                        }
                    }
                }
            }
        }

        StringBuffer returnValue = new StringBuffer();
        if (!buffer1.toString().equals(_blank(4) + "production {\n")) {
            returnValue.append(buffer1.toString() + _blank(4) + "}\n");
        }
        if (!buffer2.toString().equals(_blank(4) + "consumption {\n")) {
            returnValue.append(buffer2.toString() + _blank(4) + "}\n");
        }
        if (!buffer3.toString().equals(_blank(4) + "delay {\n")) {
            returnValue.append(buffer3.toString() + _blank(4) + "}\n");
        }
        return returnValue.toString();
    }

    /**
     * Generate the interface block.
     *
     * @return The DIF specification of the interface block .
     */
    protected String _writeInterface() throws DIFLanguageException {
        if (_hierarchy.getPorts().getAll().size() == 0) {
            return null;
        }

        StringBuffer buffer = new StringBuffer();
        buffer.append(_blank(4) + "interface {\n");
        StringBuffer inportBuffer = new StringBuffer();
        inportBuffer.append(_blank(8) + "inputs = ");
        StringBuffer outportBuffer = new StringBuffer();
        outportBuffer.append(_blank(8) + "outputs = ");

        for (Iterator ports = _hierarchy.getPorts().iterator();
             ports.hasNext(); ) {
            Port port = (Port) ports.next();
            _checkLabel(port.getName());

            if (port.getDirection() < 0) {
                if (port.getNode() != null) {
                    inportBuffer.append(port.getName() + ":"
                            + _graph.getName(port.getNode()) + ",\n" + _blank(17));
                } else {
                    inportBuffer.append(port.getName() + ",\n" + _blank(17));
                }
            } else if (port.getDirection() > 0) {
                if (port.getNode() != null) {
                    outportBuffer.append(port.getName() + ":"
                            + _graph.getName(port.getNode()) + ",\n" + _blank(18));
                } else {
                    outportBuffer.append(port.getName() + ",\n" + _blank(18));
                }
            }
        }

        //if interface contains input ports
        if (inportBuffer.lastIndexOf(",") != -1) {
            inportBuffer.delete(inportBuffer.lastIndexOf(","),
                    inportBuffer.length());
            inportBuffer.append(";\n");
            buffer.append(inportBuffer);
        }
        //if interface contains output ports
        if (outportBuffer.lastIndexOf(",") != -1) {
            outportBuffer.delete(outportBuffer.lastIndexOf(","),
                    outportBuffer.length());
            outportBuffer.append(";\n");
            buffer.append(outportBuffer);
        }

        buffer.append(_blank(4) + "}\n");
        return buffer.toString();
    }

    ///////////////////////////////////////////////////////////////////
    ////                  private methods                          ////
    /* private static String _formatString(String str) 
    has been moved to dif.util.Value.*/


    ///////////////////////////////////////////////////////////////////
    ////                  protected & private variables            ////

    /**
     * Generate the parameter block.
     *
     * @return The DIF specification of the parameter block .
     */
    protected String _writeParameter() throws DIFLanguageException {
        Iterator params = _graph.getParameters().iterator();
        if (params.hasNext() == false) {
            return null;
        }

        StringBuffer buffer = new StringBuffer();
        buffer.append(_blank(4) + "parameter {\n");
        while (params.hasNext()) {
            DIFParameter param = (DIFParameter) params.next();
            String paramId = param.getName();
            if (param == null) {
                throw new DIFLanguageException(
                        "Cannot find DIFParameter instance of parameter "
                                + paramId + ".");
            }

            buffer.append(_blank(8) + paramId);

            /*if (param.getType() != null && param.getDataType() == null) {
                buffer.append(" : " + param.getType());
            } else if (param.getType() == null && param.getDataType() != null){
                buffer.append(" :: " + param.getDataType());
            } else if (param.getType() != null && param.getDataType() != null){
                buffer.append(" : " + param.getType()
                        + " : " + param.getDataType());
            }*/

            if (param.getDataType() != null) {
                buffer.append(" : " + "\"" + param.getDataType() + "\"");
            }

            if (param.getValue() != null) {
                if (Value.isValue(param.getValue())) {
                    buffer.append(" = "
                            + objectToString(param.getValue()) + ";\n");
                } else if (param.getValue() instanceof DIFParameter) {
                    //parameter reference can only be set
                    //by higher hierarchy refinement block.
                    buffer.append(";\n");
                } else if (param.getValue() instanceof IntervalCollection) {
                    buffer.append(" : "
                            + ((IntervalCollection) param.getValue()).toString()
                            + ";\n");
                } else {
                    throw new DIFLanguageException(
                            "Non-supported value type of " + paramId + " with type"
                                    + param.getValue().getClass() + ".");
                }
            } else {
                //blank parameter
                buffer.append(";\n");
            }
        }
        buffer.append(_blank(4) + "}\n");    //parameter{}
        return buffer.toString();
    }

    /**
     * Generate the refinement block.
     *
     * @return The DIF specification of the refinement block .
     */
    protected String _writeRefinement() throws DIFLanguageException {
        Iterator superNodes = _hierarchy.getSuperNodes().iterator();
        if (superNodes.hasNext() == false) {
            return null;
        }
        StringBuffer buffer = new StringBuffer();

        while (superNodes.hasNext()) {
            //For each supernode, there is a refinement block for it.
            buffer.append(_blank(4) + "refinement {\n");

            Node superNode = (Node) superNodes.next();
            String nodeName = _graph.getName(superNode);
            DIFHierarchy subHierarchy =
                    (DIFHierarchy) _hierarchy.getSuperNodes().get(superNode);

            buffer.append(_blank(8) + subHierarchy.getName() + " = "
                    + nodeName + ";\n");

            for (Iterator ports = subHierarchy.getPorts().iterator();
                 ports.hasNext(); ) {
                Port port = (Port) ports.next();

                if (port.getConnection() != null) {
                    if (port.getConnection() instanceof Port) {
                        buffer.append(_blank(8) + port.getName() + " : "
                                + ((Port) port.getConnection()).getName() + ";\n");
                    } else if (port.getConnection() instanceof Edge) {
                        Edge edge = (Edge) port.getConnection();
                        buffer.append(_blank(8) + port.getName() + " : "
                                + _graph.getName(edge) + ";\n");
                    }
                }
            }

            DIFGraph subGraph = (DIFGraph) subHierarchy.getGraph();
            for (Iterator subParamIds = subGraph.getParameterNames()
                    .iterator();
                 subParamIds.hasNext(); ) {
                String subParamId = (String) subParamIds.next();
                DIFParameter subParam = subGraph.getParameter(subParamId);
                if (subParam.getValue() != null
                        && subParam.getValue() instanceof DIFParameter) {
                    DIFParameter param = (DIFParameter) subParam.getValue();
                    if (_graph.getParameter(param.getName()) == param) {
                        //this graph parameter is the reference of sub graph parameter.
                        buffer.append(_blank(8) + subParam.getName() + " = "
                                + param.getName() + ";\n");
                    }
                }
            }

            buffer.append(_blank(4) + "}\n");    //refinement{}
        }

        return buffer.toString();
    }

    /**
     * Generate the topology block.
     *
     * @return The DIF specification of the topology block .
     */
    protected String _writeTopology() throws DIFLanguageException {
        //return if no nodes
        Iterator nodes = _graph.nodes().iterator();
        if (nodes.hasNext() == false) {
            return null;
        }

        StringBuffer buffer = new StringBuffer();

        buffer.append(_blank(4) + "topology {\n");
        buffer.append(_blank(8) + "nodes = ");

        while (nodes.hasNext()) {
            Node node = (Node) nodes.next();
            String nodeId = _graph.getName(node);
            if (_usedLabels.contains(nodeId)) {
                System.out.println(_usedLabels.toString());
                throw new DIFLanguageException(
                        "Attempt to duplicate a node id while writing"
                                + " the node " + nodeId + ".");
            }
            _checkLabel(nodeId);

            buffer.append(nodeId);

            if (nodes.hasNext()) {
                buffer.append(",\n" + _blank(16));
            }
            _usedLabels.add(nodeId);
        }

        buffer.append(";\n");    //nodes = ...;

        //return if no edges
        Iterator edges = _graph.edges().iterator();
        if (edges.hasNext() == false) {
            buffer.append(_blank(4) + "}\n");    //topology {}
            return buffer.toString();
        }

        buffer.append(_blank(8) + "edges = ");

        while (edges.hasNext()) {
            Edge edge = (Edge) edges.next();
            String edgeId = _graph.getName(edge);
            if (_usedLabels.contains(edgeId)) {
                throw new DIFLanguageException(
                        "Attempt to duplicate an edge id while writing"
                                + " the edge " + edgeId + ".");
            }
            if (edgeId == null) {
                _graph.setName(edge, "e" + _graph.edgeLabel(edge));
                edgeId = _graph.getName(edge);
            }
            _checkLabel(edgeId);

            buffer.append(edgeId + " (" + _graph.getName(edge.source()) +
                    ", " + _graph.getName(edge.sink()) + ")");

            if (edges.hasNext()) {
                buffer.append(",\n" + _blank(16));
            }
            _usedLabels.add(edgeId);
        }

        buffer.append(";\n");    //edges = ...;
        buffer.append(_blank(4) + "}\n");    //topology {}
        return buffer.toString();
    }

    /**
     * Generate the interface block.
     * All user defined arrtibutes regarding to nodes are writen in
     * the actor block. At this time, user-defined attributes for
     * graph and edges are allowed to be value only.
     *
     * @return The DIF specification of the interface block .
     */
    protected String _writeUserDefAttribute() throws DIFLanguageException {
        StringBuffer buffer = new StringBuffer();

        //graph attributes can contain value but not reference
        Iterator attributes = _graph.getAttributes().iterator();
        while (attributes.hasNext()) {
            DIFAttribute attribute = (DIFAttribute) attributes.next();
            String attributeId = attribute.getName();
            if (attribute.getValue() != null) {
                if (Value.isValue(attribute.getValue())) {
                    buffer.append(_blank(4) + "attribute " + attributeId
                            + " { = "
                            + objectToString(attribute.getValue())
                            + "; }\n");
                } else {
                    String referenceId = _getObjectName(attribute.getValue());
                    if (referenceId == null) {
                        throw new DIFLanguageException(
                                "Cannot find name of "
                                        + attribute.getValue().toString());
                    }
                    buffer.append(_blank(4) + "attribute " + attributeId
                            + " { = " + referenceId + "; }\n");
                }
            }
        }

        LinkedHashMap attributeMap = new LinkedHashMap();

        //edge attributes
        Iterator edges = _graph.edges().iterator();
    /*if (edges.hasNext() == false) {
	    return buffer.toString();
	}*/

        while (edges.hasNext()) {
            Edge edge = (Edge) edges.next();
            attributes = _graph.getAttributes(edge).iterator();
            while (attributes.hasNext()) {
                DIFAttribute attribute = (DIFAttribute) attributes.next();
                String attributeId = attribute.getName();
                if (attribute.getValue() != null &&
                        Value.isValue(attribute.getValue())) {
                    if (attributeMap.containsKey(attributeId) == false) {
                        StringBuffer buf = new StringBuffer();
                        buf.append(_blank(4) + "attribute " + attributeId
                                + "{\n");
                        buf.append(_blank(8) + _graph.getName(edge)
                                + " = "
                                + objectToString(attribute.getValue())
                                + ";\n");
                        attributeMap.put(attributeId, buf);
                    } else {
                        ((StringBuffer) attributeMap.get(attributeId)).append(
                                _blank(8) + _graph.getName(edge) + " = "
                                        + objectToString(attribute.getValue())
                                        + ";\n");
                    }
                } else if (attribute.getValue() != null) {
                    if (attributeMap.containsKey(attributeId) == false) {
                        String referenceId =
                                _getObjectName(attribute.getValue());
                        if (referenceId == null) {
                            throw new DIFLanguageException(
                                    "Cannot find name of "
                                            + attribute.getValue().toString());
                        }

                        StringBuffer buf = new StringBuffer();
                        buf.append(_blank(4) + "attribute " + attributeId
                                + "{\n");
                        buf.append(_blank(8) + _graph.getName(edge)
                                + " = " + referenceId + ";\n");
                        attributeMap.put(attributeId, buf);
                    } else {
                        String referenceId =
                                _getObjectName(attribute.getValue());
                        if (referenceId == null) {
                            throw new DIFLanguageException(
                                    "Cannot find name of "
                                            + attribute.getValue().toString());
                        }
                        ((StringBuffer) attributeMap.get(attributeId)).append(
                                _blank(8) + _graph.getName(edge) + " = "
                                        + referenceId + ";\n");
                    }
                }
            }
        }

        //PORT attributes
        Iterator ports = _hierarchy.getPorts().iterator();
        while (ports.hasNext()) {
            Port port = (Port) ports.next();
            if (_hierarchy.getAttributes(port) != null) {
                attributes = _hierarchy.getAttributes(port).iterator();
                while (attributes.hasNext()) {
                    DIFAttribute attribute = (DIFAttribute) attributes.next();
                    String attributeId = attribute.getName();
                    if (attributeId.equals("production") ||
                            attributeId.equals("consumption")) {
                        continue;
                    }
                    if (attribute.getValue() != null &&
                            Value.isValue(attribute.getValue())) {
                        if (attributeMap.containsKey(attributeId) == false) {
                            StringBuffer buf = new StringBuffer();
                            buf.append(_blank(4) + "attribute " + attributeId
                                    + "{\n");
                            buf.append(_blank(8) + port.getName()
                                    + " = "
                                    + objectToString(attribute.getValue())
                                    + ";\n");
                            attributeMap.put(attributeId, buf);
                        } else {
                            ((StringBuffer) attributeMap.get(attributeId)).append(
                                    _blank(8) + port.getName() + " = "
                                            + objectToString(attribute.getValue())
                                            + ";\n");
                        }
                    } else if (attribute.getValue() != null) {
                        if (attributeMap.containsKey(attributeId) == false) {
                            String referenceId =
                                    _getObjectName(attribute.getValue());
                            if (referenceId == null) {
                                throw new DIFLanguageException(
                                        "Cannot find name of "
                                                + attribute.getValue().toString());
                            }
                            StringBuffer buf = new StringBuffer();
                            buf.append(_blank(4) + "attribute " + attributeId
                                    + "{\n");
                            buf.append(_blank(8) + port.getName()
                                    + " = " + referenceId + ";\n");
                            attributeMap.put(attributeId, buf);
                        } else {
                            String referenceId =
                                    _getObjectName(attribute.getValue());
                            if (referenceId == null) {
                                throw new DIFLanguageException(
                                        "Cannot find name of "
                                                + attribute.getValue().toString());
                            }
                            ((StringBuffer) attributeMap.get(attributeId)).append(
                                    _blank(8) + port.getName() + " = "
                                            + referenceId + ";\n");
                        }
                    }
                }
            }

        }

        Iterator bufs = attributeMap.values().iterator();
        while (bufs.hasNext()) {
            StringBuffer buf = (StringBuffer) bufs.next();
            buf.append(_blank(4) + "}\n");    //attribute id{};
            buffer.append(buf);
        }

        return buffer.toString();
    }
}


