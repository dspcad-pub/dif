/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.difml.parser;

import dif.DIFHierarchy;
import dif.difml.parser.elements.DIFMLDocument;
import org.w3c.dom.Comment;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

//////////////////////////////////////////////////////////////////////////
////  DIFMLWriter

/**
 * DIFMLWriter class.
 * DIFMLNode class for transformation between DIF files to XML files. <p>
 *
 * @author Ruirui Gu, Jonathan Piat
 * @version $Id: DIFMLNode.java 2010-09-06 Ruirui $
 * @see DIFMLNode
 * @see DIFMLNodeFactory
 * @see XMLtoDIFWriter
 * @see DIFMLReader
 */

public class DIFMLWriter {

    private File file;

    /**
     * constructor.
     *
     * @param path - the destination DIFML file.
     */
    public DIFMLWriter(String path) {
        file = new File(path);
    }

    /**
     * The DIFML format corresponding to the given DIFHierarchy.
     *
     * @param hierarchies - A list of DIFHierarchy.
     */
    public void write(List<DIFHierarchy> hierarchies) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.newDocument();
            document.setXmlVersion("1.0");
            Comment comment = document.createComment("Automatically generated DIFML file");
            document.normalize();

            DIFMLDocument difMLDoc = new DIFMLDocument(document, hierarchies);

            Element root = document.getDocumentElement();
            root.appendChild(comment);

            DOMImplementation impl = document.getImplementation();
            DOMImplementationLS implLS = (DOMImplementationLS) impl;
            LSOutput output = implLS.createLSOutput();
            output.setByteStream(new FileOutputStream(file));

            LSSerializer serializer = implLS.createLSSerializer();
            serializer.getDomConfig().setParameter("format-pretty-print", true);

            serializer.write(document, output);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
