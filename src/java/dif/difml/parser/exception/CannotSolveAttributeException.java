package dif.difml.parser.exception;

public class CannotSolveAttributeException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 8415968409021515192L;


    private String message;

    public CannotSolveAttributeException(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
