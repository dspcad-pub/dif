/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.difml.parser;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * DIFMLNode class.
 * DIFMLNode class for bi-directional transformation between DIF files
 * and XML files.<p>
 * <p>
 * The Node interface is the primary datatype for the entire DIFML Document
 * Object Model. It represents a single node in the document tree. While all
 * objects implementing the Node interface expose methods for dealing with
 * children, not all objects implementing the Node interface may have children.<p>
 *
 * @author Ruirui Gu, Jonathan Piat
 * @version $Id: DIFMLNode.java 2010-09-06 Ruirui $
 * @see DIFMLNodeFactory
 * @see DIFMLWriter
 * @see XMLtoDIFWriter
 * @see DIFMLReader
 */

public abstract class DIFMLNode<E> {

    protected Node node;
    protected DIFMLNode<?> parentNode;
    protected E difElement;
    private HashMap<Class<? extends DIFMLNode>, List<DIFMLNode>> childs;

    /**
     * Constructor
     */
    public DIFMLNode(Node n, DIFMLNode parentNode) {
        this.parentNode = parentNode;
        node = n;
        childs = new HashMap<Class<? extends DIFMLNode>, List<DIFMLNode>>();
    }

    /**
     * Constructor
     */
    public DIFMLNode(Document pDoc) {
        node = pDoc.createElement(getDIFMLNodeName());
        childs = new HashMap<Class<? extends DIFMLNode>, List<DIFMLNode>>();
    }

    ///////////////////////////////////////////////////////////////////
    ////                       public methods                      ////   


    /**
     * get all the direct child nodes to the current node.
     * This is for <i>org.w3c.dom.Document </i>, and the <i>node</i>
     * can be found in <i>org.w3c.dom.Node</i>.
     */
    public void parseNode() {
        Node child = node.getFirstChild();
        while (child != null) {
            this.addDIFMLChild(DIFMLNodeFactory.createNode(child, this));
            child = child.getNextSibling();
        }
    }

    /**
     * the node itself
     */
    public Node getNode() {
        return node;
    }

    public abstract String getDIFMLNodeName();

    public Node getDOMNode() {
        return node;
    }

    public List<DIFMLNode> getChilds(Class<? extends DIFMLNode> childClass) {
        return childs.get(childClass);
    }


    /**
     * set the parent DIFML node for this DIFML node.
     * This is for DIFML document.
     *
     * @param node
     */
    public void setDIFMLParentNode(DIFMLNode node) {
        parentNode = node;
    }

    /**
     * the parent of this DIFML node.
     * This is for DIFML document.
     */
    public DIFMLNode getParentNode() {
        return parentNode;
    }

    /**
     * the DIF element associated with the node.
     * This is for DIF file.
     */
    public E getDIFElement() {
        return difElement;
    }

    /**
     * set the give element as DIF element.
     *
     * @param elt
     */
    public void setDIFElement(E elt) {
        difElement = elt;
    }

    /**
     * add child nodes for this DIFML node.
     * This is for DIFML document.
     *
     * @param node
     */
    public void addDIFMLChild(DIFMLNode node) {
        if (childs.get(node.getClass()) == null) {
            List<DIFMLNode> nodes = new ArrayList<DIFMLNode>();
            childs.put(node.getClass(), nodes);
        }
        childs.get(node.getClass()).add(node);

        NodeList childsNodes = ((Element) this.node).getChildNodes();
        for (int i = 0; i < childsNodes.getLength(); i++) {
            if (childsNodes.item(i).equals(node.getDOMNode())) {
                return;
            }
        }
        this.node.appendChild(node.getDOMNode());
    }

    /**
     * add sibling nodes for this DIFML node.
     * This is for DIFML document.
     *
     * @param node
     */
    public void addDIFMLSibling(DIFMLNode node) {
        parentNode.addDIFMLChild(node);
    }
}
