/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.difml.parser;

import dif.DIFParameter;
import dif.difml.parser.elements.*;
import org.w3c.dom.Node;

//////////////////////////////////////////////////////////////////////////
////  DIFMLNodeFactory

/**
 * DIFMLNodeFactory class.
 * <p>
 * DIFMLNodeFactory class for bi-directional transformation between DIF files
 * and XML files.<p>
 * <p>
 * This factory include all the <i>node</i> types which can be used in DIFML
 * documents.<p>
 *
 * @author Ruirui Gu, Jonathan Piat
 * @version $Id: DIFMLNode.java 2010-09-06 Ruirui $
 * @see DIFMLNode
 * @see DIFMLWriter
 * @see XMLtoDIFWriter
 * @see DIFMLReader
 */

public class DIFMLNodeFactory {

    /**
     * create a DIFML node based the name of the node.
     * The given parentNode is a DIFML node.
     *
     * @param n, parentNode
     * @return DIFMLNode
     */
    public static DIFMLNode<?> createNode(Node n, DIFMLNode<?> parentNode) {
        String name = n.getNodeName();
        if (name.equals(DataFlowModelNode.DIFML_NODE_NAME)) {
            return new DataFlowModelNode(n, parentNode);
        } else if (name.equals(DIFMLDocument.DIFML_NODE_NAME)) {
            return new DIFMLDocument(n, parentNode);
        } else if (name.equals(ImplicitAttributesNode.DIFML_NODE_NAME)) {
            return new ImplicitAttributesNode(n, parentNode);
        } else if (name.equals(GraphNameNode.DIFML_NODE_NAME)) {
            return new GraphNameNode(n, parentNode);
        } else if (name.equals(EdgeNode.DIFML_NODE_NAME)) {
            return new EdgeNode(n, parentNode);
        } else if (name.equals(EdgesNode.DIFML_NODE_NAME)) {
            return new EdgesNode(n, parentNode);
        } else if (name.equals(InterfaceNode.DIFML_NODE_NAME)) {
            return new InterfaceNode(n, parentNode);
        } else if (name.equals(NodeNode.DIFML_NODE_NAME)) {
            return new NodeNode(n, parentNode);
        } else if (name.equals(NodesNode.DIFML_NODE_NAME)) {
            return new NodesNode(n, parentNode);
        } else if (name.equals(ParameterNode.DIFML_NODE_NAME)) {
            return new ParameterNode(n, parentNode);
        } else if (name.equals(ParametersNode.DIFML_NODE_NAME)) {
            return new ParametersNode(n, parentNode);
        } else if (name.equals(RangeNode.DIFML_NODE_NAME)) {
            return new RangeNode(n, parentNode);
        } else if (name.equals(ReferenceNode.DIFML_NODE_NAME)) {
            return new ReferenceNode(n, parentNode);
        } else if (name.equals(SubEdgeNode.DIFML_NODE_NAME)) {
            return new SubEdgeNode(n, parentNode);
        } else if (name.equals(SubGraphNode.DIFML_NODE_NAME)) {
            return new SubGraphNode(n, parentNode);
        } else if (name.equals(SubParamNode.DIFML_NODE_NAME)) {
            return new SubParamNode(n, parentNode);
        } else if (name.equals(SubPortNode.DIFML_NODE_NAME)) {
            return new SubPortNode(n, parentNode);
        } else if (name.equals(TopologyNode.DIFML_NODE_NAME)) {
            return new TopologyNode(n, parentNode);
        } else if (name.equals(AttributeNode.DIFML_NODE_NAME)) {
            return new AttributeNode(n, parentNode);
        } else if (name.equals(ValueNode.DIFML_NODE_NAME)) {
            return new ValueNode(n, (DIFMLNode<DIFParameter>) parentNode);
        } else if (name.equals(NodeWeightNode.DIFML_NODE_NAME)) {
            return new NodeWeightNode(n, parentNode);
        } else if (name.equals(EdgeWeightNode.DIFML_NODE_NAME)) {
            return new EdgeWeightNode(n, parentNode);
        } else if (name.equals(ElementNode.DIFML_NODE_NAME)) {
            return new ElementNode(n, parentNode);
        } else if (name.equals(UserDefAttributeNode.DIFML_NODE_NAME)) {
            return new UserDefAttributeNode(n, parentNode);
        } else if (name.equals(PortNode.DIFML_NODE_NAME)) {
            return new PortNode(n, parentNode);
        } else if (name.equals(GraphTypeNode.DIFML_NODE_NAME)) {
            return new GraphTypeNode(n, parentNode);
        } else if (name.equals(NodeNameNode.DIFML_NODE_NAME)) {
            return new NodeNameNode(n, parentNode);
        } else if (name.equals(BuiltInAttributesNode.DIFML_NODE_NAME)) {
            return new BuiltInAttributesNode(n, parentNode);
        } else if (name.equals(UserDefinedAttributesNode.DIFML_NODE_NAME)) {
            return new UserDefinedAttributesNode(n, parentNode);
        } else if (name.equals(EdgeNameNode.DIFML_NODE_NAME)) {
            return new EdgeNameNode(n, parentNode);
        } else if (name.equals(EdgeSourceNode.DIFML_NODE_NAME)) {
            return new EdgeSourceNode(n, parentNode);
        } else if (name.equals(EdgeSinkNode.DIFML_NODE_NAME)) {
            return new EdgeSinkNode(n, parentNode);
        } else if (name.equals(PortDirectionNode.DIFML_NODE_NAME)) {
            return new PortDirectionNode(n, parentNode);
        } else if (name.equals(RefinementNode.DIFML_NODE_NAME)) {
            return new RefinementNode(n, parentNode);
        }

        return new DefaultNode(n, (DIFMLNode<DIFParameter>) parentNode);
    }
}
