/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/

package dif.difml.parser;

import dif.DIFGraph;
import dif.DIFHierarchy;
import dif.difml.parser.elements.DIFMLDocument;
import dif.difml.parser.elements.DataFlowModelNode;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * DIFMLReader class.
 * DIFMLReader class for transformation from a DFML file to the corresponding
 * DIF file.
 *
 * @author Ruirui Gu, Jonathan Piat
 * @version $Id: DIFMLNode.java 2010-09-06 Ruirui $
 * @see DIFMLNode
 * @see DIFMLNodeFactory
 * @see XMLtoDIFWriter
 * @see DIFMLWriter
 */

public class DIFMLReader {

    protected DIFGraph _graph;
    protected DIFHierarchy _hierarchy;
    private File file;
    private DIFMLDocument difmlDoc;
    private StringBuffer _commonBuffer;
    private HashSet _usedLabels;

    private String difType;

    /**
     * constructor.
     *
     * @param path - the input XML file.
     */
    public DIFMLReader(String path) {
        file = new File(path);
        _commonBuffer = new StringBuffer();
        _graph = new DIFGraph();
        //_hierarchy = new DIFHierarchy();
        //_usedLabels = new HashSet();
    }

    public DIFMLReader(File file) {
        this.file = file;
        _commonBuffer = new StringBuffer();
        _graph = new DIFGraph();
    }

    /**
     * Convert the XML file into corresponding DIFML file.
     */
    public void read() {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(file);
            Node n = doc.getDocumentElement();
            Node nFirstChild = n.getFirstChild();
            difmlDoc = new DIFMLDocument(n, null);
        } catch (SAXException | IOException | ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    /**
     * Obtain DIFHierarchy from the DIFML file.
     */
    public DIFHierarchy getHierarchy() {
        List<DIFHierarchy> hierarchies = getHierarchyList();
        return hierarchies.get(0);
    }

    public List<DIFHierarchy> getHierarchyList() {
        List<DataFlowModelNode> difModels = difmlDoc.getChilds(DataFlowModelNode.class);
        Set<String> subGraphSet = difmlDoc.getSubGraphSet();
        Map<String, DIFHierarchy> hierarchyMap = difmlDoc.getHierarchyMap();

        DataFlowModelNode fstElmnt = difModels.get(0);
        difType = fstElmnt.getDIFMLNodeType();


        List<DIFHierarchy> dataflows = new ArrayList<DIFHierarchy>();
        for (DataFlowModelNode dataflow : difModels) {
            String graphID = ((DIFGraph)dataflow.getDIFElement().getGraph()).getName();
            if (!subGraphSet.contains(graphID))
                dataflows.add(dataflow.getDIFElement());
        }
        return dataflows;
    }


    public String toString() {
        return _commonBuffer.toString();
    }

    public String getDIFType() {
        return difType;
    }


    /**
     * Write the DIF specification to a string.
     *
     * @param difFileName - A file name string. A ".dif" extension will
     *                    automatically be appended. <p>
     *                    difString - A string to store the DIF specification.
     * @throws IOException If the file cannot be written.
     */
    public void write(String difFileName, String difString) throws IOException {
        PrintWriter out =
                new PrintWriter(new FileOutputStream(difFileName));

        out.print(difString);
        out.close();
    }

}

