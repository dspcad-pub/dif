/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
/* Ruirui Gu, Jiahao Wu, DSPCAD */
package dif.difml.parser.elements;

import dif.DIFGraph;
import dif.DIFNodeWeight;
import dif.bdf.BDFNodeWeight;
import dif.difml.parser.DIFMLNode;
import dif.mdsdf.MDSDFNodeWeight;
import dif.psdf.PSDFNodeWeight;
import dif.pafg.PAFGNodeWeight;
import dif.wsdf.WSDFNodeWeight;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.List;

public class NodeNode extends DIFMLNode<dif.util.graph.Node> {

    public static final String DIFML_NODE_NAME = "node";

    private dif.util.graph.Node node;

    public NodeNode(Node n, DIFMLNode<?> parentNode) {
        super(n, parentNode);
        DIFMLNode parentGraph = parentNode;
        /*
         * trace back to the root node, whose DIFElement is DIFHierarchy, containing
         * related Graph object, parameter, attribute containers
         */
        while (!(parentGraph instanceof DataFlowModelNode)) {
            parentGraph = parentGraph.getParentNode();
        }
        /*
        create a reference to root node
		 */
        DataFlowModelNode graphNode = (DataFlowModelNode) parentGraph;
        node = new dif.util.graph.Node();
        /*
        DIFElement if current (DOM)node is a DIF node
         */
        String GraphType = graphNode.getDIFMLNodeType();
        DIFNodeWeight initWeight = null;
        switch (GraphType) {
            case "PSDFGraph":
            case "PSDFSystemGraph":
                initWeight = new PSDFNodeWeight();
                break;
            case "WSDFGraph":
                initWeight = new WSDFNodeWeight();
                break;
            case "MDSDFGraph":
                initWeight = new MDSDFNodeWeight();
                break;
            case "BDFGraph":
                initWeight = new BDFNodeWeight();
                break;
            case "PAFGGraph":
                initWeight = new PAFGNodeWeight();
                break;
            default:
                initWeight = new DIFNodeWeight();
                break;
        }
        node.setWeight(initWeight);
        this.setDIFElement(node);
        parseNode();

        /*
          add node to DIF graph if it is not exits
         */
        if (!((DIFGraph) graphNode.getDIFElement().getGraph()).containsNode(node)) {
            ((DIFGraph) graphNode.getDIFElement().getGraph()).addNode(node);
        }

		/*
        add node to up DOM node's arraylist
		 */
        ((NodesNode) parentNode).getDIFElement().add(node);
        /*
        set node name in DIF graph
         */
        ((DIFGraph) graphNode.getDIFElement().getGraph()).setName(node,
                ((Element) ((NodeList) ((Element) n).getElementsByTagName("id")).item(0)).getAttribute("val"));
        /*
        return a list of DIFMLNode instances
         */
        List<DIFMLNode> attributes = this.getChilds(AttributeNode.class);
        if (attributes != null) {
            /*
            set attribute in DIF graph
             */
            for (DIFMLNode attribute : attributes) {
                AttributeNode attr = (AttributeNode) attribute;
                /*
                add attributes to root graph
                 */
                ((DIFGraph) graphNode.getDIFElement().getGraph()).setAttribute(node,
                        attr.getDIFElement());
            }
        }
    }


    public NodeNode(Document doc, dif.util.graph.Node mocNode, DIFGraph graph) {
        super(doc);
        this.addDIFMLChild(new ImplicitAttributesNode(doc, mocNode, graph));
        this.addDIFMLChild(new BuiltInAttributesNode(doc, mocNode, graph));
        this.addDIFMLChild(new UserDefinedAttributesNode(doc, mocNode, graph));
    }

    public dif.util.graph.Node getDIFNode() {
        return node;
    }

    @Override
    public String getDIFMLNodeName() {
        return DIFML_NODE_NAME;
    }
}
