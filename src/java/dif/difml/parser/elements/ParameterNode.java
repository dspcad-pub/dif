/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
/* Ruirui Gu, DSPCAD */
package dif.difml.parser.elements;

import dif.DIFGraph;
import dif.DIFParameter;
import dif.attributes.IntervalCollection;
import dif.difml.parser.DIFMLNode;
import dif.util.Value;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class ParameterNode extends DIFMLNode<DIFParameter> {

    public static final String DIFML_NODE_NAME = "parameter";

    public ParameterNode(Node n, DIFMLNode<?> parentNode) {
        super(n, parentNode);
        String parameterId = ((Element) n).getAttribute("id");
        //System.out.println(parameterId);
        DIFParameter param = new DIFParameter(parameterId);
        this.setDIFElement(param);
        ParametersNode nodesNode = (ParametersNode) parentNode;
        nodesNode.getDIFElement().add(param);
        parentNode = parentNode.getParentNode();
        while (!(parentNode instanceof DataFlowModelNode)) {
            parentNode = parentNode.getParentNode();
        }
        DataFlowModelNode graphNode = (DataFlowModelNode) parentNode;
        ((DIFGraph) graphNode.getDIFElement().getGraph()).setParameter(param);
        parseNode();
    }

    public ParameterNode(Document doc, DIFParameter param) {
        super(doc);
        ((Element) this.getDOMNode()).setAttribute("id", param.getName());
        if (param.getValue() instanceof Value) {
            this.addDIFMLChild(new ValueNode(doc, param));
        } else if (param.getValue() instanceof DIFParameter) {
            this.addDIFMLChild(new ValueNode(doc, param));
        } else if (param.getValue() instanceof IntervalCollection) {
            this.addDIFMLChild(new RangeNode(doc, (IntervalCollection) param.getValue()));
        } else {
            this.addDIFMLChild(new ValueNode(doc, param));
        }
    }

    @Override
    public String getDIFMLNodeName() {
        return DIFML_NODE_NAME;
    }
}
