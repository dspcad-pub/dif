/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
/* Ruirui Gu, Jiahao Wu, DSPCAD */
package dif.difml.parser.elements;

import dif.DIFEdgeWeight;
import dif.bdf.BDFEdgeWeight;
import dif.csdf.CSDFEdgeWeight;
import dif.csdf.sdf.HSDFEdgeWeight;
import dif.csdf.sdf.SDFEdgeWeight;
import dif.difml.parser.DIFMLNode;
import dif.mdsdf.MDSDFEdgeWeight;
import dif.psdf.PSDFEdgeWeight;
import dif.pafg.PAFGEdgeWeight;
import dif.wsdf.WSDFEdgeWeight;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;

public class EdgeWeightNode extends DIFMLNode {

    public static final String DIFML_NODE_NAME = "edgeWeight";

    public EdgeWeightNode(Node n, DIFMLNode parentNode) {
        super(n, parentNode);
        Element elt = (Element) n;
        DIFEdgeWeight weight;
        if (elt.getAttribute("type").equals("SDFEdgeWeight")) {
            weight = new SDFEdgeWeight();
            weight.setConsumptionRate(Integer.parseInt(elt.getAttribute("comsumption")));
            weight.setProductionRate(Integer.parseInt(elt.getAttribute("production")));
            weight.setDelay(Integer.parseInt(elt.getAttribute("delay")));
        } else if (elt.getAttribute("type").equals("BDFEdgeWeight")) {
            weight = new BDFEdgeWeight();
        } else if (elt.getAttribute("type").equals("CSDFEdgeWeight")) {
            weight = new CSDFEdgeWeight();
            weight.setConsumptionRate(Integer.parseInt(elt.getAttribute("comsumption")));
            weight.setProductionRate(Integer.parseInt(elt.getAttribute("production")));
            weight.setDelay(Integer.parseInt(elt.getAttribute("delay")));
        } else if (elt.getAttribute("type").equals("WSDFEdgeWeight")) {
            weight = new WSDFEdgeWeight();
        } else if (elt.getAttribute("type").equals("PSDFEdgeWeight")) {
            weight = new PSDFEdgeWeight();
        } else if (elt.getAttribute("type").equals("PAFGEdgeWeight")) {
            weight = new PAFGEdgeWeight();
        } else if (elt.getAttribute("type").equals("MDSDFEdgeWeight")) {
            weight = new MDSDFEdgeWeight();
        } else if (elt.getAttribute("type").equals("HSDFEdgeWeight")) {
            weight = new HSDFEdgeWeight();
        } else {
            weight = new DIFEdgeWeight();
        }
        while (!(parentNode instanceof EdgeNode)) {
            parentNode = parentNode.getParentNode();
        }
        ((EdgeNode) parentNode).getDIFElement().setWeight(weight);

        //ruirui: in CFDF models, it could be the case that consumption rate, production, and delay
        if (!(elt.getAttribute("consumption")).equals("")) {
            weight.setConsumptionRate(getValueFromString(elt.getAttribute("consumption")));
        }

        if (!(elt.getAttribute("production")).equals("")) {
            weight.setProductionRate(getValueFromString(elt.getAttribute("production")));
        }

        if (!(elt.getAttribute("delay")).equals("")) {
            weight.setDelay(getValueFromString(elt.getAttribute("delay")));
        }
    }

    public EdgeWeightNode(Document doc, DIFEdgeWeight weight) {
        super(doc);
        ((Element) this.getNode()).setAttribute("type", weight.getClass()
                .getSimpleName());
        ((Element) this.getNode()).setAttribute("production",
                getStringRepresentation(weight.getProductionRate()));
        ((Element) this.getNode()).setAttribute("delay",
                getStringRepresentation(weight.getDelay()));
        ((Element) this.getNode()).setAttribute("consumption",
                getStringRepresentation(weight.getConsumptionRate()));
    }

    @Override
    public String getDIFMLNodeName() {
        return DIFML_NODE_NAME;
    }

    private String getStringRepresentation(Object weight) {
        StringBuilder result = new StringBuilder();
        if (weight instanceof double[]) {
            result.append("[");
            double[] tab = (double[]) weight;
            result.append(tab[0]);
            for (int i = 1; i < tab.length; i++) {
                result.append(", ").append(tab[i]);
            }
            result.append("]");
        } else if (weight instanceof float[]) {
            result.append("[");
            float[] tab = (float[]) weight;
            result.append(tab[0]);
            for (int i = 1; i < tab.length; i++) {
                result.append(", ").append(tab[i]);
            }
            result.append("]");
        } else if (weight instanceof int[]) {
            result.append("[");
            int[] tab = (int[]) weight;
            result.append(tab[0]);
            for (int i = 1; i < tab.length; i++) {
                result.append(", ").append(tab[i]);
            }
            result.append("]");
        } else {
            // ruirui: add "else if" when there is new format of weight, such
            // as production, consumption and delay
            // result = weight.toString();
        }
        return result.toString();
    }

    private Object getValueFromString(String s) {
        try {
            if (s.charAt(0) == '[' && s.charAt(s.length() - 1) == ']') {
                s = s.substring(1, s.length() - 1);
                String[] elts = s.split(", ");
                Number[] tab;
                NumberFormat format = new DecimalFormat();
                if (elts.length == 1) {
                    return format.parse(elts[0]).intValue();
                }
                if (format.parse(elts[0]) instanceof Integer) {
                    tab = new Integer[elts.length];
                } else if (format.parse(elts[0]) instanceof Double) {
                    tab = new Double[elts.length];
                } else if (format.parse(elts[0]) instanceof Float) {
                    tab = new Float[elts.length];
                } else {
                    tab = new Integer[elts.length];
                }
                for (int i = 0; i < elts.length; i++) {
                    if (format.parse(elts[i]) instanceof Long) {
                        tab[i] = format.parse(elts[i]).intValue();
                    } else {
                        tab[i] = format.parse(elts[i]);
                    }
                }
                return tab;
            } else {
                NumberFormat format = new DecimalFormat();
                return format.parse(s).intValue();
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
