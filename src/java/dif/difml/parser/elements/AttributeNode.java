/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
/* Ruirui Gu, Jiahao Wu, DSPCAD */
package dif.difml.parser.elements;

import dif.DIFAttribute;
import dif.DIFGraph;
import dif.difml.parser.DIFMLNode;
import dif.difml.parser.exception.CannotSolveAttributeException;
import dif.wsdf.WSDFGraph;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class AttributeNode extends DIFMLNode<DIFAttribute> {

    public static String DIFML_NODE_NAME = "attribute";

    public static List<AttributeNode> unsolvedNodes;

    public AttributeNode(Node n, DIFMLNode parentNode) {
        super(n, parentNode);
        if (unsolvedNodes == null) {
            unsolvedNodes = new ArrayList<AttributeNode>();
        }
        while (!(parentNode instanceof NodeNode) && !(parentNode instanceof EdgeNode)) {
            parentNode = parentNode.getParentNode();
        }
        NodeNode nodeNode;
        dif.util.graph.Node difNode = null;
        EdgeNode edgeNode;
        dif.util.graph.Edge difEdge = null;

        if (parentNode instanceof NodeNode) {
            nodeNode = (NodeNode) parentNode;
            difNode = nodeNode.getDIFElement();
        } else {
            edgeNode = (EdgeNode) parentNode;
            difEdge = edgeNode.getDIFElement();
        }

        while (!(parentNode instanceof DataFlowModelNode)) {
            parentNode = parentNode.getParentNode();
        }
        DataFlowModelNode graphNode = (DataFlowModelNode) parentNode;
        DIFGraph graph = (DIFGraph) graphNode.getDIFElement().getGraph();

        Element elt = (Element) n;
        String attrName = elt.getAttribute("name");
        if (attrName != null) {

            DIFAttribute attribute = new DIFAttribute(attrName);
            String tempValue = elt.getAttribute("val");
            String dataType = elt.getAttribute("type");

            attribute.setType(dataType);

            if (dataType.equals("Integer")) {
                attribute.setValue(Integer.valueOf(tempValue));
            } else if (dataType.equals("List")) {
                String[] strList = tempValue.split(",");
                List<String> tempList = Arrays.asList(strList);
                List<Integer> resultList = new LinkedList<>();
                for (String str : tempList) {
                    resultList.add(Integer.valueOf(str));
                }
                attribute.setValue(resultList);
            } else {
                attribute.setValue(tempValue);
            }
            if (difNode != null) {
                if (!graph.containsNode(difNode)) {
                    graph.addNode(difNode);
                }
                graph.setAttribute(difNode, attribute);
            } else {
                if (!graph.containsEdge(difEdge)) {
                    graph.addEdge(difEdge);
                }
                graph.setAttribute(difEdge, attribute);
            }
            this.setDIFElement(attribute);
        }
    }

    public AttributeNode(Document doc, DIFAttribute attr, DIFGraph graph) {
        super(doc);
        ((Element) this.getDOMNode()).setAttribute("name", attr.getName());
        ((Element) this.getDOMNode()).setAttribute("val", StringFactory.toString(attr.getValue(), graph));
        ((Element) this.getDOMNode()).setAttribute("type", attr.getValue().getClass().getSimpleName());
    }

    public static void solveUnsolvedAttributes() throws CannotSolveAttributeException {
        while (unsolvedNodes != null && unsolvedNodes.size() > 0) {
            AttributeNode unsolvedAttr = unsolvedNodes.get(0);
            DIFMLNode parentNode = unsolvedAttr.getParentNode();
            Element elt = (Element) unsolvedAttr.getNode();
            while (!(parentNode instanceof DataFlowModelNode)) {
                parentNode = parentNode.getParentNode();
            }
            DataFlowModelNode graphNode = (DataFlowModelNode) parentNode;
            DIFGraph graph = (DIFGraph) graphNode.getDIFElement().getGraph();
            if (StringFactory.fromNode(elt.getAttribute("val"), elt.getAttribute("type"), graph) != null) {
                unsolvedAttr.getDIFElement().setValue(StringFactory.fromNode(elt.getAttribute("val"), elt.getAttribute("type"), graph));
                unsolvedNodes.remove(0);
            } else {
                System.out.println("cannot be solved");
                throw (new CannotSolveAttributeException("attribute " + elt.getAttribute("name") + " cannot be solved"));
            }
        }
    }

    @Override
    public String getDIFMLNodeName() {
        return DIFML_NODE_NAME;
    }
}
