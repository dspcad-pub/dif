/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
/* Ruirui Gu, Jiahao Wu, DSPCAD */
package dif.difml.parser.elements;

public enum DataFlowType {

    SDF, PDF, CSDF, HSDF, MDSDF, BCSDF, DIF, ILDIF, CFDF, WSDF, PAFG;

    public static DataFlowType fromString(String n) {
        if (n.equals("sdf")) {
            return SDF;
        } else if (n.equals("pdf")) {
            return PDF;
        } else if (n.equals("csdf")) {
            return CSDF;
        } else if (n.equals("hsdf")) {
            return HSDF;
        } else if (n.equals("mdsdf")) {
            return MDSDF;
        } else if (n.equals("bcsdf")) {
            return BCSDF;
        } else if (n.equals("dif")) {
            return DIF;
        } else if (n.equals("ildif")) {
            return ILDIF;
        } else if (n.equals("cfdf")) {
            return CFDF;
        } else if (n.equals("wsdf")) {
            return WSDF;
        } else if (n.equals("pafg")) {
            return PAFG;
        }
        return null;
    }
}
