/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
/* Ruirui Gu, DSPCAD */
package dif.difml.parser.elements;

import dif.DIFGraph;
import dif.DIFParameter;
import dif.util.graph.Edge;
import dif.util.graph.Node;

public class StringFactory {

    public static String toString(Object obj, DIFGraph graph) {
        if (obj instanceof Node) {
            return graph.getName((Node) obj);
        } else if (obj instanceof Edge) {
            return graph.getName((Edge) obj);
        } else if (obj instanceof DIFParameter) {
            return ((DIFParameter) obj).getName();
        }
        return obj.toString();
    }

    public static Object fromNode(String nodeVal, String type, DIFGraph graph) {
        if (type.equals("Node")) {
            return graph.getNode(nodeVal);
        } else if (type.equals("Edge")) {
            return graph.getEdge(nodeVal);
        } else if (type.equals("Parameter")) {
            return graph.getParameter(nodeVal);
        } else if (type.equals("String")) {
            return nodeVal;
        }
        return nodeVal;
    }


}
