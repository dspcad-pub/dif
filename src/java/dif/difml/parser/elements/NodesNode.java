/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
/* Ruirui Gu, DSPCAD */
package dif.difml.parser.elements;

import dif.DIFGraph;
import dif.difml.parser.DIFMLNode;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class NodesNode extends DIFMLNode<List<dif.util.graph.Node>> {

    public static final String DIFML_NODE_NAME = "nodes";

    public NodesNode(Node n, DIFMLNode<?> parentNode) {
        super(n, parentNode);
        this.setDIFElement(new ArrayList<dif.util.graph.Node>());
        parseNode();
    }

    public NodesNode(Document doc, Collection<dif.util.graph.Node> nodeCollec, DIFGraph graph) {
        super(doc);
        List<dif.util.graph.Node> nodes = new ArrayList<dif.util.graph.Node>(nodeCollec);
        for (dif.util.graph.Node mocNode : nodes) {
            this.addDIFMLChild(new NodeNode(doc, mocNode, graph));
        }
    }


    @Override
    public String getDIFMLNodeName() {
        return DIFML_NODE_NAME;
    }
}
