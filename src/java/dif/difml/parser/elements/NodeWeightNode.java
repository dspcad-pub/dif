/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
/* Ruirui Gu, Jiahao Wu DSPCAD */
package dif.difml.parser.elements;

import dif.DIFGraph;
import dif.DIFNodeWeight;
import dif.attributes.BDFAttributeType;
import dif.bdf.BDFNodeWeight;
import dif.csdf.CSDFNodeWeight;
import dif.csdf.sdf.SDFNodeWeight;
import dif.difml.parser.DIFMLNode;
import dif.mdsdf.MDSDFNodeWeight;
import dif.psdf.PSDFNodeWeight;
import dif.pafg.PAFGNodeWeight;
import dif.wsdf.WSDFNodeWeight;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;


public class NodeWeightNode extends DIFMLNode<DIFNodeWeight> {

    public static final String DIFML_NODE_NAME = "nodeWeight";

    public NodeWeightNode(Node n, DIFMLNode<?> parentNode) {
        super(n, parentNode);
        DIFNodeWeight nodeWeight;
        Element elt = (Element) node;
        if (elt.getAttribute("type").equals("SDFNodeWeight")) {
            SDFNodeWeight sdfNodeWeight = new SDFNodeWeight();
            sdfNodeWeight.setComputation(elt.getAttribute("computation"));
            nodeWeight = sdfNodeWeight;
        } else if (elt.getAttribute("type").equals("BDFNodeWeight")) {
            BDFNodeWeight bdfNodeWeight = new BDFNodeWeight();
            bdfNodeWeight.setComputation(elt.getAttribute("computation"));
            if (elt.getAttribute("nodeType").equals("booleanNode")) {
                bdfNodeWeight.setBDFNodeType(BDFAttributeType.BooleanNode);
            } else if (elt.getAttribute("nodeType").equals("regularNode")) {
                bdfNodeWeight.setBDFNodeType(BDFAttributeType.RegularNode);
            } else if (elt.getAttribute("nodeType").equals("select")) {
                bdfNodeWeight.setBDFNodeType(BDFAttributeType.Select);
            } else if (elt.getAttribute("nodeType").equals("switch")) {
                bdfNodeWeight.setBDFNodeType(BDFAttributeType.Switch);
            } else if (elt.getAttribute("nodeType").equals("trueProbability")) {
                bdfNodeWeight.setBDFNodeType(BDFAttributeType.TrueProbability);
            }
            nodeWeight = bdfNodeWeight;
        } else if (elt.getAttribute("type").equals("PSDFNodeWeight")) {
            PSDFNodeWeight psdfNodeWeight = new PSDFNodeWeight();
            psdfNodeWeight.setComputation(elt.getAttribute("computation"));
            nodeWeight = psdfNodeWeight;
        } else if (elt.getAttribute("type").equals("PAFGNodeWeight")) {
                PAFGNodeWeight pafgNodeWeight = new PAFGNodeWeight();
                pafgNodeWeight.setComputation(elt.getAttribute("computation"));
                nodeWeight = pafgNodeWeight;

        } else if (elt.getAttribute("type").equals("CSDFNodeWeight")) {
            CSDFNodeWeight csdfNodeWeight = new CSDFNodeWeight();
            csdfNodeWeight.setComputation(elt.getAttribute("computation"));
            nodeWeight = csdfNodeWeight;
        } else if (elt.getAttribute("type").equals("MDSDFNodeWeight")) {
            MDSDFNodeWeight mdsdfNodeWeight = new MDSDFNodeWeight();
            mdsdfNodeWeight.setComputation(elt.getAttribute("computation"));
            nodeWeight = mdsdfNodeWeight;
        } else if (elt.getAttribute("type").equals("WSDFNodeWeight")) {
            WSDFNodeWeight wsdfNodeWeight = new WSDFNodeWeight();
            wsdfNodeWeight.setComputation(elt.getAttribute("computation"));
            nodeWeight = wsdfNodeWeight;
        } else {
            nodeWeight = new DIFNodeWeight();
            nodeWeight.setComputation(elt.getAttribute("computation"));
        }
        while (!(parentNode instanceof NodeNode)) {
            parentNode = parentNode.getParentNode();
        }
        NodeNode parentDIFNode = (NodeNode) parentNode;
        parentDIFNode.getDIFElement().setWeight(nodeWeight);
    }

    public NodeWeightNode(Document doc, DIFNodeWeight weight, DIFGraph graph) {
        super(doc);
        if (weight instanceof SDFNodeWeight) {
            SDFNodeWeight sdfNodeWeight = (SDFNodeWeight) weight;
            ((Element) getNode()).setAttribute("type", "SDFNodeWeight");
            ((Element) getNode()).setAttribute("instanceNumber",
                    ((Integer) sdfNodeWeight.getInstanceNumber()).toString());
        } else if (weight instanceof BDFNodeWeight) {
            BDFNodeWeight bdfNodeWeight = (BDFNodeWeight) weight;
            ((Element) getNode()).setAttribute("type", "BDFNodeWeight");
            ((Element) getNode()).setAttribute("nodeType", bdfNodeWeight
                    .getBDFNodeType().toString());
            if (bdfNodeWeight.getTrueProbability() != null) {
                ((Element) getNode()).setAttribute("trueProbability",
                        bdfNodeWeight.getTrueProbability().toString());
            }
        } else if (weight instanceof PSDFNodeWeight) {
            PSDFNodeWeight psdfNodeWeight = (PSDFNodeWeight) weight;
            ((Element) getNode()).setAttribute("type", "PSDFNodeWeight");
        } else if (weight instanceof CSDFNodeWeight) {
            CSDFNodeWeight csdfNodeWeight = (CSDFNodeWeight) weight;
            ((Element) getNode()).setAttribute("type", "CSDFNodeWeight");
            ((Element) getNode()).setAttribute("instanceNumber",
                    ((Integer) csdfNodeWeight.getInstanceNumber()).toString());
        } else if (weight instanceof MDSDFNodeWeight) {
            MDSDFNodeWeight mdsdfNodeWeight = (MDSDFNodeWeight) weight;
            ((Element) getNode()).setAttribute("type", "MDSDFNodeWeight");
        } else {
            ((Element) getNode()).setAttribute("type", "DIFNodeWeight");
        }
        if (weight.getComputation() != null) {
            ((Element) getNode()).setAttribute("computation", weight
                    .getComputation().toString());
        }
    }

    @Override
    public String getDIFMLNodeName() {
        return DIFML_NODE_NAME;
    }

}
