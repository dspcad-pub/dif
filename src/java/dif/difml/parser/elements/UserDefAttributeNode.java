/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
/* Ruirui Gu, DSPCAD */
package dif.difml.parser.elements;

import dif.DIFAttribute;
import dif.DIFGraph;
import dif.difml.parser.DIFMLNode;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class UserDefAttributeNode extends DIFMLNode<DIFAttribute> {

    public static final String DIFML_NODE_NAME = "userDifAttr";

    public UserDefAttributeNode(Node n, DIFMLNode parentNode) {
        super(n, parentNode);
        Element elt = (Element) n;
        DIFAttribute attribute = new DIFAttribute(elt.getAttribute("name"));
        attribute.setValue(elt.getAttribute("value"));
        this.setDIFElement(attribute);
        DIFMLNode parentGraph = parentNode;
        while (!(parentGraph instanceof DataFlowModelNode)) {
            parentGraph = parentGraph.getParentNode();
        }
        DataFlowModelNode graphNode = (DataFlowModelNode) parentGraph;
        ((DIFGraph) graphNode.getDIFElement().getGraph()).setAttribute(attribute);
    }

    public UserDefAttributeNode(Document doc, DIFAttribute attribute, DIFGraph graph) {
        super(doc);
        ((Element) this.getDOMNode()).setAttribute("name", attribute.getName());
        ((Element) this.getDOMNode()).setAttribute("value", attribute.getValue().toString());
    }

    @Override
    public String getDIFMLNodeName() {
        return DIFML_NODE_NAME;
    }
}
