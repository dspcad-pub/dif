/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
/* Ruirui Gu, DSPCAD */
package dif.difml.parser.elements;

import dif.DIFGraph;
import dif.DIFHierarchy;
import dif.difml.parser.DIFMLNode;
import dif.graph.hierarchy.Port;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.List;

public class PortNode extends DIFMLNode<Port> {

    public static final String DIFML_NODE_NAME = "port";

    public PortNode(Node n, DIFMLNode parentNode) {
        super(n, parentNode);
        DIFMLNode hierarchy = parentNode;
        while (!(hierarchy instanceof DataFlowModelNode)) {
            hierarchy = hierarchy.getParentNode();
        }
        DIFHierarchy difHier = ((DataFlowModelNode) hierarchy).getDIFElement();


        int direction = 0;

        Node node = ((Element) n).getElementsByTagName("direction").item(0);
        Element elt = (Element) node;

        if (elt.getAttribute("val").equals("IN")) {
            direction = Port.IN;
        } else if (elt.getAttribute("val").equals("OUT")) {
            direction = Port.OUT;
        } else if (elt.getAttribute("val").equals("INOUT")) {
            direction = Port.INOUT;
        }
        Port port = new Port(elt.getAttribute("id"), difHier, direction);
        port.relate(((DIFGraph) difHier.getGraph()).getNode(elt.getAttribute("nodeId")));
        ((List<Port>) parentNode.getDIFElement()).add(port);
        this.setDIFElement(port);
    }

    public PortNode(Document doc, Port node, DIFGraph graph) {
        super(doc);

        this.addDIFMLChild(new ImplicitAttributesNode(doc, node, graph));
    }

    @Override
    public String getDIFMLNodeName() {
        return DIFML_NODE_NAME;
    }

}
