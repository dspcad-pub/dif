/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
/* Ruirui Gu, Jiahao Wu, DSPCAD */
package dif.difml.parser.elements;

import dif.DIFAttribute;
import dif.DIFGraph;
import dif.DIFHierarchy;
import dif.bdf.BDFGraph;
import dif.cfdf.CFDFGraph;
import dif.csdf.CSDFGraph;
import dif.csdf.sdf.HSDFGraph;
import dif.csdf.sdf.SDFGraph;
import dif.pafg.PAFGGraph;
import dif.difml.parser.DIFMLNode;
import dif.graph.hierarchy.SuperNodeMap;
import dif.psdf.PSDFGraph;
import dif.psdf.PSDFSpecificationGraph;
import dif.wsdf.WSDFGraph;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.List;
import java.util.Map;

public class DataFlowModelNode extends DIFMLNode<DIFHierarchy> {

    public static final String DIFML_NODE_NAME = "graph";
    private String difmlNode_type;

    public DataFlowModelNode(Node n, DIFMLNode<?> parentNode) {
        super(n, parentNode);
        Element fstElmnt = (Element) n;

        NodeList nameElmntLst = fstElmnt.getElementsByTagName("name");
        Element nameElmnt = (Element) nameElmntLst.item(0);

        NodeList typeElmntLst = fstElmnt.getElementsByTagName("type");
        Element typeElmnt = (Element) typeElmntLst.item(0);

        String name = nameElmnt.getAttribute("val");
        String className = typeElmnt.getAttribute("val");
        difmlNode_type = typeElmnt.getAttribute("val");

        if (className.equals("SDFGraph")) {
            this.setDIFElement(new DIFHierarchy(new SDFGraph(), name));
        } else if (className.equals("CSDFGraph")) {
            this.setDIFElement(new DIFHierarchy(new CSDFGraph(), name));
        } else if (className.equals("BDFGraph")) {
            this.setDIFElement(new DIFHierarchy(new BDFGraph(), name));
        } else if (className.equals("CFDFGraph")) {
            this.setDIFElement(new DIFHierarchy(new CFDFGraph(), name));
        } else if (className.equals("DIFGraph")) {
            this.setDIFElement(new DIFHierarchy(new DIFGraph(), name));
        } else if (className.equals("PAFGGraph")) {
            this.setDIFElement(new DIFHierarchy(new PAFGGraph(), name));
        } else if (className.equals("PSDFSystemGraph")) {
            this.setDIFElement(new DIFHierarchy(new PSDFSpecificationGraph(true), name));
        } else if (className.equals("PSDFGraph")) {
            this.setDIFElement(new DIFHierarchy(new PSDFGraph(), name));
        } else if (className.equals("WSDFGraph")) {
            this.setDIFElement(new DIFHierarchy(new WSDFGraph(), name));
        } else if (className.equals("HSDFGraph")) {
            this.setDIFElement(new DIFHierarchy(new HSDFGraph(), name));
        }

        ((DIFGraph) this.getDIFElement().getGraph()).setName(name);
        parseNode();
        try {
            AttributeNode.solveUnsolvedAttributes();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
        DIFMLNode difmlDocument = parentNode;
        while (!(difmlDocument instanceof DIFMLDocument)) {
            difmlDocument = difmlDocument.getParentNode();
        }
        Map<String, DIFHierarchy> hierarchies = ((DIFMLDocument) difmlDocument).getHierarchyMap();
        hierarchies.put(name, (DIFHierarchy) this.getDIFElement());
    }


    public DataFlowModelNode(Document doc, DIFHierarchy hierarchy) {
        super(doc);
        DIFGraph graph = (DIFGraph) hierarchy.getGraph();
        this.addDIFMLChild(new ImplicitAttributesNode(doc, graph));
        this.addDIFMLChild(new TopologyNode(doc, graph));
        this.addDIFMLChild(new InterfaceNode(doc, hierarchy));
        this.addDIFMLChild(new ParametersNode(doc, graph));

        List<DIFAttribute> attributes = graph.getAttributes();
        for (DIFAttribute attribute : attributes) {
            this.addDIFMLChild(new UserDefAttributeNode(doc, attribute, graph));
        }
        SuperNodeMap superNodes = hierarchy.getSuperNodes();
        for (Object obj : superNodes.getNodes()) {
            dif.util.graph.Node n = (dif.util.graph.Node) obj;
            DIFHierarchy subHierarchy = (DIFHierarchy) superNodes.get(n);
            this.addDIFMLChild(new RefinementNode(doc, hierarchy, subHierarchy, n));
        }
    }

    @Override
    public String getDIFMLNodeName() {
        return DIFML_NODE_NAME;
    }

    public String getDIFMLNodeType() {
        return difmlNode_type;
    }
}
