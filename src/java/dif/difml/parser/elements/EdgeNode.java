/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
/* Ruirui Gu, DSPCAD */
package dif.difml.parser.elements;

import dif.DIFGraph;
import dif.difml.parser.DIFMLNode;
import dif.util.graph.Edge;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class EdgeNode extends DIFMLNode<Edge> {

    public static final String DIFML_NODE_NAME = "edge";

    public EdgeNode(Node n, DIFMLNode<?> parentNode) {
        super(n, parentNode);
        DIFMLNode parentGraph = parentNode;
        while (!(parentGraph instanceof DataFlowModelNode)) {
            parentGraph = parentGraph.getParentNode();
        }
        DataFlowModelNode graphNode = (DataFlowModelNode) parentGraph;
        DIFGraph graph = (DIFGraph) graphNode.getDIFElement().getGraph();
        String sourceId = ((Element) ((NodeList) ((Element) n).getElementsByTagName("sourceId")).item(0)).getAttribute("val");

        String targetId = ((Element) ((NodeList) ((Element) n).getElementsByTagName("sinkId")).item(0)).getAttribute("val");

        Edge edge = new Edge(graph.getNode(sourceId), graph.getNode(targetId));
        ((EdgesNode) parentNode).getDIFElement().add(edge);
        this.setDIFElement(edge);
        parseNode();
        if (!graph.containsEdge(edge)) {
            graph.addEdge(edge);
        }
        graph.setName(edge, ((Element) ((NodeList) ((Element) n).getElementsByTagName("id")).item(0)).getAttribute("val"));

    }

    public EdgeNode(Document doc, Edge edge, DIFGraph graph) {
        super(doc);

        this.addDIFMLChild(new ImplicitAttributesNode(doc, edge, graph));
        this.addDIFMLChild(new BuiltInAttributesNode(doc, edge, graph));
    }

    @Override
    public String getDIFMLNodeName() {
        return DIFML_NODE_NAME;
    }
}
