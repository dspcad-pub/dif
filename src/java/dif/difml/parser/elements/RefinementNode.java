/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
/* Ruirui Gu, DSPCAD */
package dif.difml.parser.elements;

import dif.DIFGraph;
import dif.DIFHierarchy;
import dif.DIFParameter;
import dif.difml.parser.DIFMLNode;
import dif.graph.hierarchy.HierarchyException;
import dif.graph.hierarchy.Port;
import dif.graph.hierarchy.PortList;
import dif.util.graph.Edge;
import dif.util.graph.Graph;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.*;


public class RefinementNode extends DIFMLNode {

    public static final String DIFML_NODE_NAME = "refinement";

    public RefinementNode(Node n, DIFMLNode parentNode) {
        super(n, parentNode);
        DIFMLNode hierarchy = parentNode;
        while (!(hierarchy instanceof DataFlowModelNode)) {
            hierarchy = hierarchy.getParentNode();
        }
        DIFHierarchy difHierarchy = ((DataFlowModelNode) hierarchy).getDIFElement();
        DIFGraph graph = (DIFGraph) difHierarchy.getGraph();

        DIFMLNode difmlDocument = hierarchy;
        while (!(difmlDocument instanceof DIFMLDocument)) {
            difmlDocument = difmlDocument.getParentNode();
        }
        Map<String, DIFHierarchy> hierarchies = ((DIFMLDocument) difmlDocument).getHierarchyMap();
        Set<String> subGraphSet =((DIFMLDocument) difmlDocument).getSubGraphSet();

        /*
        Jiahao: loop in all refinement information and process DIFHierarchy
         */
        System.out.println("------ ------ ------");
        Element subGraphNode = (Element) ((Element) n).getElementsByTagName("subGraph").item(0);
        String subGraphName = subGraphNode.getAttribute("name");
        String nodeId = subGraphNode.getAttribute("nodeId");
        System.out.println(subGraphName + "-->" + nodeId);
        DIFHierarchy subHierarchy = hierarchies.get(subGraphName);
        subGraphSet.add(subGraphName);
        if (subHierarchy == null) {
            throw new HierarchyException(subGraphName + " is not found while processing DIFML document.");
        }
        //TODO maybe we can verify the graph type, but this feature is not urgent for now.
        dif.util.graph.Node superNode = getDIFNode(graph, nodeId);
        difHierarchy.addSuperNode(superNode, subHierarchy);

        /*
        Jiahao: support port to port connection only, port to edge connection can be added if required
         */
        NodeList subEdgeList = ((Element) n).getElementsByTagName("subEdge");
        for (int i = 0; i < subEdgeList.getLength(); i++) {
            Element element = (Element) subEdgeList.item(i);
            String subEdgeID = element.getAttribute("subEdgeID");
            String topEdgeID = element.getAttribute("topEdgeID");
            System.out.println("Edge Mapping: " + subEdgeID + "-->" + topEdgeID);
            // TODO fix
            Port subPort = getPort(graph, subHierarchy, subEdgeID);
            Edge edge = ((DIFGraph) difHierarchy.getGraph()).getEdge(topEdgeID);
            subPort.connect(edge);
        }

        NodeList subPortList = ((Element) n).getElementsByTagName("subPort");
        for (int i = 0; i < subPortList.getLength(); i++) {
            Element element = (Element) subPortList.item(i);
            String subPortID = element.getAttribute("subPortID");
            String topPortID = element.getAttribute("topPortID");
            System.out.println("Port Mapping: " + subPortID + "-->" + topPortID);
            Port subPort = getPort(graph, subHierarchy, subPortID);
            Port port = difHierarchy.getPorts().get(topPortID);
            subPort.connect(port);
        }

        NodeList subParamList = ((Element) n).getElementsByTagName("subParam");
        for (int i = 0; i < subParamList.getLength(); i++) {
            Element element = (Element) subParamList.item(i);
            String subParamID = element.getAttribute("subParamID");
            String topParamID = element.getAttribute("topParamID");
            System.out.println("Parameter Mapping: "+ subParamID + "-->" + topParamID);
            DIFParameter subParam = ((DIFGraph) subHierarchy.getGraph()).getParameter(subParamID);
            if (subParam == null) {
                throw new IllegalArgumentException("Cannot find parameter "
                        + subParamID + " in graph " + ((DIFGraph) subHierarchy.getGraph()).getName());
            }
            DIFParameter param = graph.getParameter(topParamID);
            if (param == null) {
                throw new IllegalArgumentException("Cannot find parameter "
                        + topParamID + " in graph " + graph.getName());
            }
            subParam.setValue(param);
        }
    }

    private Edge getEdge(DIFGraph graph, DIFHierarchy subHierarchy, String subEdgeID) {
        DIFGraph g = (DIFGraph) subHierarchy.getGraph();
        Edge edge = g.getEdge(subEdgeID);
        if (edge == null) {
            throw new IllegalArgumentException
                    ("Exception compiling the dif file: "
                            + "Edge: " + subEdgeID + " not found in "
                            + subHierarchy.getName() + " while reading "
                            + graph.getName() + ".");
        }
        return edge;
    }

    private Port getPort(DIFGraph graph, DIFHierarchy subHierarchy, String subPortID) {
        Port port = subHierarchy.getPorts().get(subPortID);
        if (port == null) {
            throw new IllegalArgumentException
                    ("Exception compiling the dif file: "
                            + "Port: " + subPortID + " not found in "
                            + subHierarchy.getName() + " while reading "
                            + graph.getName() + ".");
        }
        return port;
    }

    private dif.util.graph.Node getDIFNode(DIFGraph graph, String nodeId) {
        Object node = graph.getObject(nodeId);
        if (node == null || !(node instanceof dif.util.graph.Node)) {
            throw new IllegalArgumentException
                    ("Exception processing the difml file: Node: " + nodeId + " not found while reading " + graph.getName());
        }
        return (dif.util.graph.Node) node;
    }

    public RefinementNode(Document doc, DIFHierarchy topHierarchy, DIFHierarchy subHierarchy, dif.util.graph.Node n) {
        super(doc);
        this.addDIFMLChild(new SubGraphNode(doc, subHierarchy, n));
        PortList portList = subHierarchy.getPorts();
        List<Port> list = portList.getAll();
        for (Port p : list) {
            if (p.getConnection() != null) {
                this.addDIFMLChild(new SubPortNode(doc, p));
            }
        }
        DIFGraph topGraph = (DIFGraph) topHierarchy.getGraph();
        DIFGraph subGraph = (DIFGraph) subHierarchy.getGraph();
        LinkedList<String> topParamNames = topGraph.getParameterNames();
        LinkedList<String> subParamNames = subGraph.getParameterNames();
        HashSet<String> topParameters = new HashSet<>();
        topParameters.addAll(topParamNames);
        for (String name : subParamNames) {
            DIFParameter subParam = subGraph.getParameter(name);
            String refName = subParam.getRefName();
            if (refName != null && topParameters.contains(refName)) {
                this.addDIFMLChild(new SubParamNode(doc, name, refName));
            }
        }
    }

    @Override
    public String getDIFMLNodeName() {
        return DIFML_NODE_NAME;
    }
}
