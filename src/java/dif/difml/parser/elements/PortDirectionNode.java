/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
/* Ruirui Gu, DSPCAD */
package dif.difml.parser.elements;

import dif.DIFGraph;
import dif.difml.parser.DIFMLNode;
import dif.graph.hierarchy.Port;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.List;

public class PortDirectionNode extends DIFMLNode {

    public static final String DIFML_NODE_NAME = "direction";

    public PortDirectionNode(Node n, DIFMLNode parentNode) {
        super(n, parentNode);
        parseNode();
    }

    public PortDirectionNode(Document doc, Port node, DIFGraph graph) {
        super(doc);
        if (node.getDirection() == Port.IN) {
            ((Element) this.getDOMNode()).setAttribute("val", "IN");
        } else if (node.getDirection() == Port.OUT) {
            ((Element) this.getDOMNode()).setAttribute("val", "OUT");
        } else if (node.getDirection() == Port.INOUT) {
            ((Element) this.getDOMNode()).setAttribute("val", "INOUT");
        }
        ((Element) this.getDOMNode()).setAttribute("id", node.getName());
        ((Element) this.getDOMNode()).setAttribute("nodeId", graph.getName(node.getNode()));

    }


    @Override
    public String getDIFMLNodeName() {
        return DIFML_NODE_NAME;
    }
}
