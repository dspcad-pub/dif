/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
/* Ruirui Gu, DSPCAD */
package dif.difml.parser.elements;

import dif.DIFParameter;
import dif.difml.parser.DIFMLNode;
import dif.util.Value;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.List;

public class ValueNode extends DIFMLNode<List<Number>> {

    public static final String DIFML_NODE_NAME = "value";

    public ValueNode(Node n, DIFMLNode<DIFParameter> parentNode) {
        super(n, parentNode);
        Element elt = (Element) n;
        String type = elt.getAttribute("type");
        parentNode.getDIFElement().setDataType(type);
        if (elt.getAttribute("value") != null
                && !(elt.getAttribute("value").equals(""))) {
            parentNode.getDIFElement().setValue(elt.getAttribute("value"));
        } else {
            List<Number> numbers = new ArrayList<Number>();
            this.setDIFElement(numbers);
            parseNode();
            if (type.equals("float")) {
                float[] elts = new float[numbers.size()];
                for (int i = 0; i < numbers.size(); i++) {
                    elts[i] = numbers.get(i).floatValue();
                }
                parentNode.getDIFElement().setValue(elts);
            } else if (type.equals("int")) {
                int[] elts = new int[numbers.size()];
                for (int i = 0; i < numbers.size(); i++) {
                    elts[i] = numbers.get(i).intValue();
                }
                parentNode.getDIFElement().setValue(elts);
            } else if (type.equals("double")) {
                double[] elts = new double[numbers.size()];
                for (int i = 0; i < numbers.size(); i++) {
                    elts[i] = numbers.get(i).doubleValue();
                }
                parentNode.getDIFElement().setValue(elts);
            }

        }
    }

    public ValueNode(Document doc, DIFParameter param) {
        super(doc);
        ((Element) this.getDOMNode()).setAttribute("type", param.getDataType());
        if (param.getValue() instanceof Value) {
            ((Element) this.getDOMNode()).setAttribute("value", param
                    .getValue().toString());
        } else if (param.getValue() instanceof DIFParameter) {
            DIFParameter difParam = (DIFParameter) param.getValue();
            ((Element) this.getDOMNode()).setAttribute("value", difParam
                    .getName());
        } else if (param.getValue() instanceof String) {
            String str = (String) param.getValue();
            ((Element) this.getDOMNode()).setAttribute("value", str);
        } else if (param.getValue() instanceof double[]) {
            double[] vals = (double[]) param.getValue();
            for (int i = 0; i < vals.length; i++) {
                this.addDIFMLChild(new ElementNode(doc, vals[i]));
            }
        } else if (param.getValue() instanceof float[]) {
            float[] vals = (float[]) param.getValue();
            for (int i = 0; i < vals.length; i++) {
                this.addDIFMLChild(new ElementNode(doc, vals[i]));
            }
        } else if (param.getValue() instanceof int[]) {
            int[] vals = (int[]) param.getValue();
            for (int i = 0; i < vals.length; i++) {
                this.addDIFMLChild(new ElementNode(doc, vals[i]));
            }
        }
    }

    @Override
    public String getDIFMLNodeName() {
        return DIFML_NODE_NAME;
    }
}
