/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
/* Ruirui Gu, DSPCAD */
package dif.difml.parser.elements;

import dif.DIFHierarchy;
import dif.difml.parser.DIFMLNode;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.util.*;

public class DIFMLDocument extends DIFMLNode {

    public static final String DIFML_NODE_NAME = "difml";
    private Set<String> subGraphSet;
    private Map<String, DIFHierarchy> hierarchyMap;

    public DIFMLDocument(Node n, DIFMLNode parentNode) {
        super(n, parentNode);
        /*
        HashMap store graph_name, DIFHierarchy pair for subHierarchy search
         */
        hierarchyMap = new HashMap<>();
        subGraphSet = new HashSet<>();
        parseNode();

    }

    public Set<String> getSubGraphSet() {
        return this.subGraphSet;
    }

    public Map<String, DIFHierarchy> getHierarchyMap() {
        return this.hierarchyMap;
    }


    public DIFMLDocument(Document doc, List<DIFHierarchy> hierarchies) {
        super(doc);
        doc.appendChild(node);
        ((Element) this.getNode()).setAttribute("xmlns", "http://www.ece.umd.edu/DIFML");
        for (DIFHierarchy hierarchy : hierarchies) {
            this.addDIFMLChild(new DataFlowModelNode(doc, hierarchy));
        }
    }

    @Override
    public String getDIFMLNodeName() {
        return DIFML_NODE_NAME;
    }
}
