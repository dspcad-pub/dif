/*******************************************************************************
 @ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
 @ddblock_end copyright
 *******************************************************************************/
/* Ruirui Gu, DSPCAD */
package dif.difml.parser.elements;

import dif.DIFEdgeWeight;
import dif.DIFGraph;
import dif.DIFNodeWeight;
import dif.difml.parser.DIFMLNode;
import dif.util.graph.Edge;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class BuiltInAttributesNode extends DIFMLNode {

    public static final String DIFML_NODE_NAME = "builtInAttributes";

    public BuiltInAttributesNode(Node n, DIFMLNode parentNode) {
        super(n, parentNode);
        parseNode();
    }

    public BuiltInAttributesNode(Document doc, dif.util.graph.Node mocNode, DIFGraph graph) {
        super(doc);
        this.addDIFMLChild(new NodeWeightNode(doc, (DIFNodeWeight) mocNode
                .getWeight(), graph));
    }

    public BuiltInAttributesNode(Document doc, Edge edge, DIFGraph graph) {
        super(doc);
        if (edge.getWeight() instanceof DIFEdgeWeight) {
            this.addDIFMLChild(new EdgeWeightNode(doc, (DIFEdgeWeight) edge
                    .getWeight()));
        }

    }

    @Override
    public String getDIFMLNodeName() {
        return DIFML_NODE_NAME;
    }
}
