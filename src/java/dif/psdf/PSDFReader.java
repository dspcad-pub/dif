/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/
package dif.psdf;

import dif.DIFHierarchy;
import dif.DIFLoader;
import dif.attributes.PSDFAttributeType;
import dif.difml.parser.DIFMLReader;
import dif.graph.hierarchy.SuperNodeMap;
import dif.util.graph.Node;

import java.util.*;

/**
 * PSDFReader read dif file and return PSDFSpecification
 * <p>
 * PSDFReader reads dif file using bison based parser and DIFML tool
 *
 * @author Jiahao Wu
 * @version 0.1
 */
public class PSDFReader {
    private List<DIFHierarchy> hierarchyList;
    private List<PSDFSpecification> specList;
    private String filename;
    private final Set<String> psdfValidNames = new HashSet<>(Arrays.asList("init", "subinit", "body"));

    public PSDFReader(String filename) {
        this.filename = filename;
        this.specList = new ArrayList<>();
    }

    /**
     * automatically choose reader
     *
     * @throws PSDFSpecificationException on failed to obtain specification
     */
    public void read() throws PSDFSpecificationException {
        if (filename.contains(".difml")) {
            readdifml();
        } else {
            readdif();
        }
    }

    /**
     * Reading dif file
     *
     * @throws PSDFSpecificationException on failed to obtain specification
     */
    private void readdif() throws PSDFSpecificationException {
        hierarchyList = DIFLoader.loadDataflowList(this.filename);
        if (!analysis()) {
            throw new PSDFSpecificationException("Failed on assembling subHierarchies");
        }
    }

    /**
     * Reading difml file
     *
     * @throws PSDFSpecificationException on failed to obtain specification
     */
    private void readdifml() throws PSDFSpecificationException {
        DIFMLReader mlreader = new DIFMLReader(filename);
        mlreader.read();
        hierarchyList = mlreader.getHierarchyList();
        if (!analysis()) {
            throw new PSDFSpecificationException("Failed on assembling subHierarchies");
        }
    }

    private boolean analysis() {
        boolean success = true;
        for (DIFHierarchy hier : hierarchyList) {
            /*
            verify graph type
             */
            if (hier.getGraph() instanceof PSDFSpecificationGraph) {
                SuperNodeMap nodeMap = hier.getSuperNodes();
                List<Node> nodeList = nodeMap.getNodes();
                if (nodeMap.getNodes().size() == 3) {
                    if (validNodeNames(nodeList, hier)) {
                        ((PSDFSpecificationGraph) hier.getGraph()).setGraphType(PSDFAttributeType.PSDFSpecification);
                        PSDFSpecification spec = new PSDFSpecification(hier);
                        // TODO Param match
                        specList.add(spec);
                        continue;
                    }
                }
            }
            success = false;
            break;
        }
        return success;
    }

    private boolean validNodeNames(List<Node> nodeList, DIFHierarchy hier) {
        Set<String> nameSet = new HashSet<>();
        SuperNodeMap nodeMap = hier.getSuperNodes();
        PSDFSpecificationGraph graph = (PSDFSpecificationGraph) hier.getGraph();
        for (Node node : nodeList) {
            if (nodeMap.get(node).getGraph() instanceof PSDFGraph) {
                String nodeName = graph.getName(node);
                nameSet.add(nodeName);
                if (!psdfValidNames.contains(nodeName)) {
                    return false;
                }
                if(nodeName.equals("init")) {
                    ((PSDFGraph)nodeMap.get(node).getGraph()).setGraphType(PSDFAttributeType.InitGraph);
                } else if (nodeName.equals("subinit")) {
                    ((PSDFGraph)nodeMap.get(node).getGraph()).setGraphType(PSDFAttributeType.SubinitGraph);
                } else if(nodeName.equals("body")) {
                    ((PSDFGraph)nodeMap.get(node).getGraph()).setGraphType(PSDFAttributeType.BodyGraph);
                }
            }
        }
        return nameSet.size() == 3;
    }

    public List<PSDFSpecification> getPSDFSpecifications() {
        return specList;
    }
}
