/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/


package dif.psdf;

import dif.DIFEdgeWeight;
import dif.data.expr.Variable;
import dif.util.NamedObj;
import dif.util.graph.Edge;
import dif.util.graph.Graph;

//////////////////////////////////////////////////////////////////////////
//// PSDFEdgeWeight

/**
 * Information associated with a PSDF edge.
 * PSDFEdgeWeights are objects associated with {@link Edge}s
 * that represent PSDF edges in {@link Graph}s.
 * This class caches frequently-used data associated with PSDF edges.
 * It is also useful for intermediate PSDF graph representations
 * that do not correspond to Ptolemy II models, and performing graph
 * analyses (e.g., quasi-static scheduling).
 * It is intended for use with analysis/synthesis algorithms that operate
 * on generic graph representations of PSDF models.
 * <p>
 * Production and consumption rate objects, as well as delay objects,
 * in PSDFEdgeWeights are
 * of type {@link dif.data.expr.Variable}. They give the symoblic expressions
 * (or constant expressions) that correspond to the data rates on the associated
 * ports. By default, a PSDF edge weight contains production and consumption
 * rates that are equal to unity, and a zero-valued delay.
 *
 * @author Shuvra S. Bhattacharyya
 * @version $Id: PSDFEdgeWeight.java 606 2012-10-08 16:29:47Z plishker $
 * @see Edge
 * @see dif.csdf.CSDFEdgeWeight
 */

public class PSDFEdgeWeight extends DIFEdgeWeight {

    // An expression that specifies the consumption rate of the edge.
    // This string is used as a scratchpad by schedulers to keep track
    // of consumption rate expressions during scheduling.
    private String _consumptionRateExpression;
    // A dummy container for variables that are defined in this edge weight.
    private NamedObj _container;
    // An expression that specifies the production rate of the edge.
    // This string is used as a scratchpad by schedulers to keep track
    // of production rate expressions during scheduling.
    private String _productionRateExpression;

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /**
     * Construct an edge weight for a homogeneous, zero-delay edge. Production
     * and consumption values are set to 1.
     */
    public PSDFEdgeWeight() {
        _init();
    }

    /**
     * Construct an edge weight for a specified token production rate,
     * token consumption rate, and delay.
     *
     * @param productionRate  The token production rate.
     * @param consumptionRate The token consumption rate.
     * @param delay           The delay.
     */
    public PSDFEdgeWeight(Variable productionRate, Variable consumptionRate,
                          Variable delay) {
        _init();
        setProductionRate(productionRate);
        setConsumptionRate(consumptionRate);
        setDelay(delay);
    }

    /**
     * Construct an edge weight for a specified source port, sink port,
     * token production rate, token consumption rate, and delay.
     * The source port and sink port are ports that correspond to this edge.
     *
     * @param sourcePort      The source port.
     * @param sinkPort        The sink port.
     * @param productionRate  The token production rate.
     * @param consumptionRate The token consumption rate.
     * @param delay           The delay.
     */
    public PSDFEdgeWeight(Object sourcePort, Object sinkPort,
                          Variable productionRate, Variable consumptionRate, Variable delay) {

        setSinkPort(sinkPort);
        setSourcePort(sourcePort);
        setProductionRate(productionRate);
        setConsumptionRate(consumptionRate);
        setDelay(delay);
    }

    /**
     * Get the token consumption rate of the associated PSDF edge.
     *
     * @return The consumption rate.
     */
    public Variable getPSDFConsumptionRate() {
        return (Variable) getConsumptionRate();
    }

    /**
     * Set the token consumption rate of the PSDF edge to the given
     * variable.
     *
     * @param consumptionRate The given variable.
     */
    public void setPSDFConsumptionRate(Variable consumptionRate) {
        setConsumptionRate(consumptionRate);
    }

    /**
     * Get the delay of the associated PSDF edge.
     *
     * @return The delay.
     */
    public Variable getPSDFDelay() {
        return (Variable) getDelay();
    }

    /**
     * Set the delay of the PSDF edge to the given variable.
     *
     * @param delay The given variable.
     */
    public void setPSDFDelay(Variable delay) {
        setDelay(delay);
    }

    /**
     * Get the token production rate of the associated PSDF edge.
     *
     * @return The production rate.
     */
    public Variable getPSDFProductionRate() {
        return (Variable) getProductionRate();
    }

    /**
     * Set the token production rate of the PSDF edge to the given variable.
     *
     * @param productionRate The given variable.
     */
    public void setPSDFProductionRate(Variable productionRate) {
        setProductionRate(productionRate);
    }

    /**
     * Get the expression associated with the token consumption rate of the
     * PSDF edge. This expression is used by schedulers to keep track of
     * changes in consumption rates as graphs are clustered.
     *
     * @return Token consumption rate expression.
     * @see #setConsumptionRateExpression(String).
     */
    public String getConsumptionRateExpression() {
        return _consumptionRateExpression;
    }

    /**
     * Set to a given expression value the expression associated with
     * the token consumption rate of the PSDF edge.
     *
     * @param expression The given expression value.
     * @see #getConsumptionRateExpression().
     */
    public void setConsumptionRateExpression(String expression) {
        _consumptionRateExpression = expression;
    }

    /**
     * Get the expression associated with the token production rate of the
     * PSDF edge. This expression is used by schedulers to keep track of
     * changes in production rates as graphs are clustered.
     *
     * @return The token production rate expression.
     * @see #setProductionRateExpression(String).
     */
    public String getProductionRateExpression() {
        return _productionRateExpression;
    }

    /**
     * Set to a given expression value the expression associated with
     * the token production rate of the PSDF edge.
     *
     * @param expression The given expression value.
     * @see #getProductionRateExpression().
     */
    public void setProductionRateExpression(String expression) {
        _productionRateExpression = expression;
    }

    /**
     * Return a string representation of the edge weight. This string
     * representation is in the following form:
     * <p>
     * <em> productionRate consumptionRate delay </em>
     * <p>
     *
     * @return The string representation of the edge weight.
     */
    public String toString() {
        return new String(getProductionRate() + " " + getConsumptionRate()
                + " " + getDelay());
    }

    private void _init() {
        try {
            setSinkPort(null);
            setSourcePort(null);
            setConsumptionRateExpression(null);
            setProductionRateExpression(null);

            _container = new NamedObj("PSDFEdgeWeight");

            // Initialize the delay to the default value
            Variable delayVariable = new Variable(_container, "PSDFDelay");
            delayVariable.setExpression("0");
            setDelay(delayVariable);

            // Initialize the production rate to the default value
            Variable productionRateVariable =
                    new Variable(_container, "PSDFProductionRate");
            productionRateVariable.setExpression("1");
            setProductionRate(productionRateVariable);

            // Initialize the consumption rate to the default value
            Variable consumptionRateVariable =
                    new Variable(_container, "PSDFConsumptionRate");
            consumptionRateVariable.setExpression("1");
            setConsumptionRate(consumptionRateVariable);
        } catch (Exception exception) {
            System.err.println("Could not initialize PSDF edge weight.");
            exception.printStackTrace();
            throw new RuntimeException("Could not initialize PSDF edge weight."
                    + ".\n" + exception.getMessage() + "\n");
        }
    }


}


