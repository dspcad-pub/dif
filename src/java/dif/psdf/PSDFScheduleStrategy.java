/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/


package dif.psdf;

import dif.DIFScheduleStrategy;

//////////////////////////////////////////////////////////////////////////
//// PSDFScheduleStrategy

/**
 * An abstract class for all the scheduling strategies on PSDF graphs.
 * The associated graph must be an instance of
 * {@link dif.psdf.PSDFGraph}.
 * <p>
 * Hierarchical clustering is also maintained. Clustering operations
 * imposed on the original graph undesirably change the topology. In
 * this class, the original graph is cloned and it is the clone that
 * will be actual clustering target.
 *
 * @author Shuvra S. Bhattacharyya based on a flie by Mingyung Ko
 * @version $Id: PSDFScheduleStrategy.java 606 2012-10-08 16:29:47Z plishker $
 */

public class PSDFScheduleStrategy extends DIFScheduleStrategy {

    /**
     * A constructor with a graph. The associated graph must be an
     * instance of {@link dif.psdf.PSDFGraph}.
     *
     * @param graph The graph.
     */
    public PSDFScheduleStrategy(PSDFGraph graph) {
        super(graph);
        _clusterManager = new PSDFClusterManager(graph);
    }

    ///////////////////////////////////////////////////////////////////
    ////                        public methods                     ////

    /**
     * A description of the scheduler.
     *
     * @return A brief description of the base scheduler.
     */
    public String toString() {
        return "A scheduler for PSDF graphs.\n";
    }
}


