/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/


package dif.psdf;

import dif.DIFGraph;
import dif.attributes.PSDFAttributeType;
import dif.data.expr.Variable;
import dif.util.graph.Edge;

import java.util.Iterator;


/**
 * Information associated with a PSDF graph.
 * This class caches frequently-used data associated with PSDF graphs.
 * It is also useful for intermediate PSDF graph representations
 * (between the application model and implementation), and performing graph
 * transformations (e.g. quasi-static scheduling).
 * It is intended for use with analysis/synthesis algorithms that operate
 * on generic graph representations of PSDF models.
 * <p>
 * PSDFGraph nodes and edges have weights of type {@link PSDFNodeWeight} and
 * {@link PSDFEdgeWeight}, respectively.
 *
 * @author Shuvra S. Bhattacharyya, Chia-Jui Hsu
 * @version $Id: PSDFGraph.java 606 2012-10-08 16:29:47Z plishker $
 * @see PSDFEdgeWeight
 * @see PSDFNodeWeight
 * @see PSDFReader
 */

public class PSDFGraph extends DIFGraph {

    private PSDFAttributeType _type = null;

    /**
     * Construct an empty PSDF graph.
     */
    public PSDFGraph() {
        super();
    }

    /**
     * Construct an empty PSDF graph with enough storage allocated for the
     * specified number of nodes.
     *
     * @param nodeCount The number of nodes.
     */
    public PSDFGraph(int nodeCount) {
        super(nodeCount);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /**
     * Construct an empty PSDF graph with enough storage allocated for the
     * specified number of edges, and number of nodes.
     *
     * @param nodeCount The integer specifying the number of nodes
     * @param edgeCount The integer specifying the number of edges
     */
    public PSDFGraph(int nodeCount, int edgeCount) {
        super(nodeCount, edgeCount);
    }

    /**
     * Get the type of this graph.
     *
     * @return PSDFAttributeType.InitGraph,
     * PSDFAttributeType.SubinitGraph, or PSDFAttributeType.BodyGraph.
     */
    public PSDFAttributeType getGraphType() {
        return _type;
    }

    /**
     * set type to either PSDFAttributeType.InitGraph,
     * PSDFAttributeType.SubinitGraph, PSDFAttributeType.BodyGraph, or
     * PSDFAttributeType.PSDFSpecification.
     *
     * @param type PSDFAttributeType
     */
    public void setGraphType(PSDFAttributeType type) {
        _type = type;
    }

    /**
     * Print the expressions associated with the data rates on each edge.
     * This is a diagnostic method that prints the variable expressions
     * associated
     * with each data rate parameter to standard output.
     */
    public void printEdgeRateExpressions() {
        System.out.println("Printing edge rate expressions.");
        Iterator edges = edges().iterator();
        while (edges.hasNext()) {
            Edge edge = (Edge) edges.next();
            PSDFEdgeWeight weight = (PSDFEdgeWeight) edge.getWeight();
            Variable productionRate = weight.getPSDFProductionRate();
            if (productionRate == null) {
                System.out.print("null");
            } else {
                System.out.print(productionRate.getExpression());
            }
            System.out.print(", ");
            Variable consumptionRate = weight.getPSDFConsumptionRate();
            if (consumptionRate == null) {
                System.out.print("null");
            } else {
                System.out.print(consumptionRate.getExpression());
            }
            System.out.println();
        }
        System.out.println("Finished printing edge rate expressions.");
    }

    /**
     * Verify edge weight for PSDF graph.
     *
     * @param weight The edge weight to verify.
     * @return True if the given edge weight is valid for a PSDF graph.
     */
    @Override
    public boolean validEdgeWeight(Object weight) {
        return weight instanceof PSDFEdgeWeight;
    }


    ///////////////////////////////////////////////////////////////////////
    ////                         protected methods                     ////

    ///////////////////////////////////////////////////////////////////////
    ////                         private variabless                    ////

    /**
     * Verify node weight for PSDF graph.
     *
     * @param weight The node weight to verify.
     * @return True if the given node weight is valid for a PSDF graph.
     */
    @Override
    public boolean validNodeWeight(Object weight) {
        return weight instanceof PSDFNodeWeight;
    }
}


