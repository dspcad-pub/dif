/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/


package dif.psdf;

import dif.DIFGraph;
import dif.attributes.PSDFAttributeType;
import dif.util.graph.Edge;
import dif.util.graph.GraphException;
import dif.util.graph.Node;

/**
 * Information associated with a PSDFSpecification graph.
 * PSDF specification has only three subgraphs: init, subinit, body.
 * The PSDFSpecificationGraph is used as the base graph for
 * PSDFSpecification (which extends DIFGraph). When initiate the
 * PSDFSpecificationGraph, it will automatically add three supernodes:
 * init, subinit, body. Any modification on graph topology such as
 * addNode or addEdge are not allowed in this class.
 *
 * @author Chia-Jui Hsu
 * @version $Id: PSDFSpecificationGraph.java 606 2012-10-08 16:29:47Z plishker $
 * @see PSDFSpecification
 */

public class PSDFSpecificationGraph extends DIFGraph {

    private PSDFAttributeType _type = null;


    /**
     * Construct a PSDFSpecificationGraph with init, subinint, and body
     * nodes.
     */
    public PSDFSpecificationGraph() {
        super();
        Node init = new Node(new PSDFNodeWeight());
        addNode(init);
        setName(init, "init");
        Node subinit = new Node(new PSDFNodeWeight());
        addNode(subinit);
        setName(subinit, "subinit");
        Node body = new Node(new PSDFNodeWeight());
        addNode(body);
        setName(body, "body");
        //After add the three nodes, the graph type can be set to
        //PSDFAttributeType.PSDFSpecification, which prevent addNode and
        //addEdge.
        setGraphType(PSDFAttributeType.PSDFSpecification);
    }

    public PSDFSpecificationGraph(boolean defer) {
        if (!defer) {
            setGraphType(PSDFAttributeType.PSDFSpecification);
        }
    }

    public void freezeGraph() {
        setGraphType(PSDFAttributeType.PSDFSpecification);
    }

    /**
     */
    public PSDFAttributeType getGraphType() {
        return _type;
    }

    /**
     * set type to either PSDFAttributeType.InitGraph,
     * PSDFAttributeType.SubinitGraph, PSDFAttributeType.BodyGraph, or
     * PSDFAttributeType.PSDFSpecification.
     *
     * @param type PSDFAttributeType
     */
    public void setGraphType(PSDFAttributeType type) {
        _type = type;
    }

    /**
     * Verify edge weight for PSDFSpecificationGraph.
     * It always returns false, because PSDF specification has no edges.
     *
     * @param weight The edge weight to verify.
     * @return True if the given edge weight is valid for a PSDF graph.
     */
    @Override
    public boolean validEdgeWeight(Object weight) {
        return false;
    }

    /**
     * Verify node weight for PSDF graph.
     *
     * @param weight The node weight to verify.
     * @return True if the given node weight is valid for a PSDF graph.
     */
    @Override
    public boolean validNodeWeight(Object weight) {
        return weight instanceof PSDFNodeWeight;
    }

    @Override
    protected void _registerEdge(Edge edge) {
        if (_type != null && _type.toString().equals(
                PSDFAttributeType.PSDFSpecification.toString())) {
            throw new GraphException("PSDF Specification cannot have edges.");
        }
        super._registerEdge(edge);
    }

    @Override
    protected void _registerNode(Node node) {
        if (_type != null && _type.toString().equals(
                PSDFAttributeType.PSDFSpecification.toString())) {
            throw new GraphException("PSDF Specification cannot add nodes.");
        }
        super._registerNode(node);
    }
}


