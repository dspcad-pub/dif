/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif.psdf;

import dif.DIFGraph;
import dif.DIFHierarchy;
import dif.DIFParameter;
import dif.attributes.PSDFAttributeType;
import dif.graph.hierarchy.Port;
import dif.util.graph.Node;

import java.util.HashMap;

/**
 * Information associated with a PSDF Specification.
 *
 * @author Chia-Jui Hsu
 * @version $Id: PSDFSpecification.java 606 2012-10-08 16:29:47Z plishker $
 * @see PSDFSpecificationGraph
 */

public class PSDFSpecification extends DIFHierarchy {

    private HashMap _paramsMatch;

    /**
     * Construct a PSDF Specification with an empty name.
     *
     * @param graph A PSDFSpecificationGraph object.
     */
    public PSDFSpecification(PSDFSpecificationGraph graph) {
        super(graph, "");
        _paramsMatch = new HashMap();
    }

    /**
     * Construct a PSDF Specification.
     *
     * @param graph A PSDFSpecificationGraph object.
     * @param name  name
     */
    public PSDFSpecification(PSDFSpecificationGraph graph, String name) {
        //_graph = graph in dif.graph.hierarchy.Hierarchy
        super(graph, name);
        _paramsMatch = new HashMap();
    }

    public PSDFSpecification(DIFHierarchy hierarchy) {
        super((DIFGraph) hierarchy.getGraph(), hierarchy.getName());
        _paramsMatch = new HashMap();
        _superNodes = hierarchy.getSuperNodes();
    }

    public PSDFGraph getBodyGraph() {
        return (PSDFGraph) getSuperNodes()
                .get((Node) ((PSDFSpecificationGraph) _graph).getObject("body"))
                .getGraph();
    }

    public PSDFGraph getInitGraph() {
        return (PSDFGraph) getSuperNodes()
                .get((Node) ((PSDFSpecificationGraph) _graph).getObject("init"))
                .getGraph();
    }

    public PSDFGraph getSubinitGraph() {
        return (PSDFGraph) getSuperNodes()
                .get((Node) ((PSDFSpecificationGraph) _graph).getObject("subinit"))
                .getGraph();
    }

    public DIFHierarchy getBodyHierarchy() {
        return (DIFHierarchy) getSuperNodes()
                .get((Node) ((PSDFSpecificationGraph) _graph).getObject("body"));
    }

    public DIFHierarchy getInitHierarchy() {
        return (DIFHierarchy) getSuperNodes()
                .get((Node) ((PSDFSpecificationGraph) _graph).getObject("init"));
    }

    public DIFHierarchy getSubinitHierarchy() {
        return (DIFHierarchy) getSuperNodes()
                .get((Node) ((PSDFSpecificationGraph) _graph).getObject("subinit"));
    }

    public HashMap getParamsMatch() {
        return _paramsMatch;
    }

    public void paramsMatch(DIFHierarchy srcHierarchy, Object src,
                            DIFHierarchy destHierarchy, Object dest) {
        PSDFGraph destGraph = (PSDFGraph) destHierarchy.getGraph();
        PSDFGraph srcGraph = (PSDFGraph) srcHierarchy.getGraph();

        //destGraph is BodyGraph of this PSDFSpecification
        if (destGraph.getGraphType() == PSDFAttributeType.BodyGraph && getBodyGraph() == destGraph) {
            //srcGraph is InitGraph of this PSDFSpecification
            if (srcGraph.getGraphType() == PSDFAttributeType.InitGraph && getInitGraph() == srcGraph) {
                //src is an output Port of Init Hierarchy and dest is a Parameter of BodyGraph
                if (src instanceof Port && dest instanceof DIFParameter) {
                    if (((Port) src).getDirection() == Port.OUT && ((Port) src).getHierarchy() == srcHierarchy
                            && ((DIFParameter) dest).getContainer() == destGraph)
                        _paramsMatch.put(src, dest);
                }
            }
            //srcGraph is SubinitGraph of this PSDFSpecification
            else if (srcGraph.getGraphType() == PSDFAttributeType.SubinitGraph && getSubinitGraph() == srcGraph) {
                //src is an output Port of Subinit Hierarchy and
                //dest is a Parameter of BodyGraph
                if (src instanceof Port && dest instanceof DIFParameter) {
                    if (((Port) src).getDirection() == Port.OUT && ((Port) src).getHierarchy() == srcHierarchy
                            && ((DIFParameter) dest).getContainer() == destGraph)
                        _paramsMatch.put(src, dest);
                }
            }
        }
        //destGraph is SubinitGraph of this PSDFSpecification
        else if (destGraph.getGraphType() == PSDFAttributeType.SubinitGraph && getSubinitGraph() == destGraph) {
            //srcGraph is InitGraph of this PSDFSpecification
            if (srcGraph.getGraphType() == PSDFAttributeType.InitGraph && getInitGraph() == srcGraph) {
                //src is an output Port of Init Hierarchy and dest is a Parameter of SubinitGraph
                if (src instanceof Port && dest instanceof DIFParameter) {
                    if (((Port) src).getDirection() == Port.OUT && ((Port) src).getHierarchy() == srcHierarchy
                            && ((DIFParameter) dest).getContainer() == destGraph)
                        _paramsMatch.put(src, dest);
                }
            }
            //srcHierarchy is this PSDFSpecification
            else if (srcHierarchy == this) {
                //src is an input port of this PSDFSpecification
                if (src instanceof Port && dest instanceof DIFParameter) {
                    if (((Port) src).getDirection() == Port.IN && ((Port) src).getHierarchy() == srcHierarchy
                            && ((DIFParameter) dest).getContainer() == destGraph)
                        _paramsMatch.put(src, dest);
                }
                //src is a Parameter of this PSDFSpecification
                else if (src instanceof DIFParameter && dest instanceof DIFParameter) {
                    if (((DIFParameter) src).getContainer() == srcGraph
                            && ((DIFParameter) dest).getContainer() == destGraph) {
                        _paramsMatch.put(src, dest);
                    }
                }
            }
        }
        //destGraph is InitGraph of this PSDFSpecification
        else if (destGraph.getGraphType() == PSDFAttributeType.InitGraph && getInitGraph() == destGraph) {
            //srcHierarchy is this PSDFSpecification
            if (srcHierarchy == this) {
                //src is a Parameter of this PSDFSpecification
                if (src instanceof DIFParameter && dest instanceof DIFParameter) {
                    if (((DIFParameter) src).getContainer() == srcGraph &&
                            ((DIFParameter) dest).getContainer() == destGraph) {
                        _paramsMatch.put(src, dest);
                    }
                }
            }
        } else {
            throw new RuntimeException("Only output ports of init and subinit "
                    + "graph can be map to parameters of body graph."
                    + " Only output ports of init graph, input ports "
                    + "of psdf specification, and parameters of psdf "
                    + "specification can be map to parameters of subinit "
                    + "graph."
                    + " Only parameters of psdf specification can be map "
                    + "to parameters of init graph.");
        }
    }

    public void setBody(DIFHierarchy bodyHierarchy) {
        PSDFGraph bodyGraph = (PSDFGraph) bodyHierarchy.getGraph();
        bodyGraph.setGraphType(PSDFAttributeType.BodyGraph);
        addSuperNode((Node) ((PSDFSpecificationGraph) _graph).getObject("body"), bodyHierarchy);
    }

    public void setInit(DIFHierarchy initHierarchy) {
        PSDFGraph initGraph = (PSDFGraph) initHierarchy.getGraph();
        initGraph.setGraphType(PSDFAttributeType.InitGraph);
        addSuperNode((Node) ((PSDFSpecificationGraph) _graph).getObject("init"), initHierarchy);
    }

    public void setSubinit(DIFHierarchy subinitHierarchy) {
        PSDFGraph subinitGraph = (PSDFGraph) subinitHierarchy.getGraph();
        subinitGraph.setGraphType(PSDFAttributeType.SubinitGraph);
        addSuperNode((Node) ((PSDFSpecificationGraph) _graph).getObject("subinit"), subinitHierarchy);
    }

}


