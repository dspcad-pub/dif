/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/


package dif.psdf;

import dif.DIFClusterManager;
import dif.DIFEdgeWeight;
import dif.DIFGraph;
import dif.DIFNodeWeight;
import dif.util.graph.Edge;
import dif.util.graph.Node;

import java.util.Collection;
import java.util.List;

//////////////////////////////////////////////////////////////////////////
//// PSDFClusterManager

/**
 * The hierarchical clustering maintenance class for {@link PSDFGraph}.
 *
 * @author Mingyung Ko
 * @version $Id: PSDFClusterManager.java 606 2012-10-08 16:29:47Z plishker $
 */

public class PSDFClusterManager extends DIFClusterManager {

    // The most recently registered crossing edge.
    private Edge _crossingEdge;

    ///////////////////////////////////////////////////////////////////
    ////                        public methods                     ////

    /**
     * A constructor with the original graph. The original graph must be an
     * instance of {@link DIFGraph}.
     */
    public PSDFClusterManager(PSDFGraph graph) {
        super(graph);
        _crossingEdge = null;
    }

    /**
     * Get the edge that was most recently registered as a crossing edge,
     * or null if no edge was previously registered as a crossing edge.
     *
     * @return The most recently-registered crossing edge.
     * @see setCrossingEdge(Edge).
     */

    public final Edge getCrossingEdge() {
        return _crossingEdge;
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    /**
     * Register an edge to be a crossing edge between nodes in a pairwise
     * cluster. A crossing edge is simply an edge that is incident to
     * both nodes in a pairwise cluster.
     *
     * @see getCrossingEdge().
     */
    public final void setCrossingEdge(Edge edge) {
        _crossingEdge = edge;
    }

    /**
     */
    protected List _clusterNodesComplete(DIFGraph graph,
                                         Collection nodeCollection, Node superNode) {
        return PSDFGraphs.clusterNodesComplete((PSDFGraph) graph, nodeCollection,
                superNode, getCrossingEdge());
    }

    /**
     * Return a PSDF edge weight for a newly created edge in clustering
     * process.
     *
     * @return A PSDF edge weight.
     */
    protected DIFEdgeWeight _newEdgeWeight() {
        return new PSDFEdgeWeight();
    }

    ///////////////////////////////////////////////////////////////////
    ////                        private methods                    ////

    ///////////////////////////////////////////////////////////////////
    ////                       private variables                   ////

    /**
     * Return a PSDF node weight for a newly created node in clustering
     * process.
     *
     * @return A PSDF node weight.
     */
    protected DIFNodeWeight _newNodeWeight() {
        return new PSDFNodeWeight();
    }

}


