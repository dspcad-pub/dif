/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/


package dif.psdf;

import dif.DIFNodeWeight;
import dif.util.graph.Graph;
import dif.util.graph.Node;

//////////////////////////////////////////////////////////////////////////
//// PSDFNodeWeight

/**
 * Information associated with a PSDF node.
 * {@link PSDFNodeWeight}s are objects associated with {@link Node}s
 * that represent PSDF nodes in {@link Graph}s.
 * This class caches frequently-used data associated with PSDF nodes.
 * It is also useful for representing intermediate PSDF graph representations
 * that do not correspond to Ptolemy II models, and performing graph
 * analyses such as quasi-static scheduling.
 * It is intended for use with analysis/synthesis algorithms that operate
 * on generic graph representations of PSDF models.
 *
 * @author Shuvra S. Bhattacharyya
 * @version $Id: PSDFNodeWeight.java 606 2012-10-08 16:29:47Z plishker $
 * @see Node
 * @see DIFNodeWeight
 * @see dif.psdf.PSDFEdgeWeight
 */

public class PSDFNodeWeight extends DIFNodeWeight {

    /**
     * Create a PSDF node weight given a computation that is to be represented
     * by the PSDF node. The computation may be
     * any Object that represents
     * a computation in some way (or null if no computation is to be
     * represented by the node).
     * The represented computation may, for example, be a Ptolemy II
     * AtomicActor.
     *
     * @param computation the computation to be represented by the PSDF node.
     */
    public PSDFNodeWeight(Object computation) {
        setComputation(computation);
    }

    /**
     * Construct a PSDF node weight with no computation to be represented
     * by the associated PSDF node.
     */
    public PSDFNodeWeight() {
        setComputation(null);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /**
     * Return a string representation of the PSDF node that is represented
     * by this node weight.
     *
     * @return the string representation.
     */
    public String toString() {
        String result = getClass() + ", computation: ";
        if (getComputation() == null) {
            result += "null";
        } else {
            if (getComputation() instanceof String) {
                result += getComputation();
            } else {
                result += getComputation().getClass().getName();
            }
        }
        return result;
    }

}


