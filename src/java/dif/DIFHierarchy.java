/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif;

import dif.graph.hierarchy.Hierarchy;
import dif.graph.hierarchy.HierarchyException;
import dif.graph.hierarchy.Port;
import dif.graph.hierarchy.PortList;
import dif.util.graph.Edge;
import dif.util.graph.Graph;
import dif.util.graph.Node;

import java.lang.reflect.Constructor;
import java.util.*;

/**
 * DIFHierarchy class.
 * DIFHierarchy class extends {@link Hierarchy}. It
 * overrides the methods in {@link Hierarchy} to limit the associated graph
 * to be {@link DIFGraph}. It also overrides {@link #mirror} to mirror a
 * DIFHierarchy including all graph topologies, interface ports, element
 * weights,
 * DIFAttributes, and DIFParameters.
 * <p>
 * Ports in DIFHierarchy can have attributes like edges and nodes in DIFGraph.
 * Ports cannot contain weights because they are not elements in graphs.
 * The DIFHierarchy provides the {@link AttributeContainer} to store the
 * attributes of Ports.
 * <p>
 * DIF utilizes edge weights to record the production rate and consumption rate
 * of the source node and sink node of that edge. No edge will be built between
 * a nod and its associated port. In this situation, DIF specifies the
 * production
 * rate and consumption rate as port attributes. Therefore, "production" and
 * "consumption" attributes are reserved for ports in this purpose.
 * Because ports do not have weights to further distinguish between different
 * types of weights (ex: CSDF, BCSDF, SDF, HSDF), users need to be very careful
 * about assigning the "production" and "consumption" attributes. The specific
 * data type for the production and consumption rate of each dataflow model
 * can be found in xxxEdgeWeight. xxxLanguageAnalysis will also take the
 * responsibility to check the production and consumption rate for edges and
 * ports.
 * <p>
 * Override method {@link Hierarchy#flatten(Node)}
 * to support DIFAttribute references problem after flatten and update
 * production and consumption attributes of ports to the flattened edges.
 * <p>
 * {@link #mirror()} method can completely clone/mirror
 * the DIFHierarchy/DIFGraph.
 *
 * @author Chia-Jui Hsu, Fuat Keceli, Shuvra S. Bhattacharyya, Ming-Yung Ko
 * @version $Id: DIFHierarchy.java 606 2008-10-08 16:29:47Z plishker $
 * @see Graph
 * @see DIFGraph
 * @see Hierarchy
 */

public class DIFHierarchy extends Hierarchy {

    ///////////////////////////////////////////////////////////////////
    ////                         constructors                      ////

    //Port -> AttributeContainer
    private HashMap _containers = new LinkedHashMap();

    /**
     * Construct a hierarchy object with an empty name.
     *
     * @param graph The graph object that backs the hierarchy.
     */
    public DIFHierarchy(DIFGraph graph) {
        this(graph, "");
    }

    ///////////////////////////////////////////////////////////////////
    ////                       public methods                      ////

    /**
     * Construct a hierarchy object with a name.
     *
     * @param graph The graph object that backs the hierarchy.
     * @param name  Name of the hierarchy.
     */
    public DIFHierarchy(DIFGraph graph, String name) {
        super(graph, name);
    }

    /*
    WU: "public DIFAttribute getAttribute(Port port, String name, String
    type)" removed on 08/29/16.
        This function exits in commits before 08/29/16.
     */

    /**
     * Returns the DIFAttribute associated with <i>port</i>.
     *
     * @param port A Port in this hierarchy.
     * @param name The name of this DIFAttribute.
     * @return The DIFAttribute with the given name.
     * Null if the attribute doesn't exist.
     * @throws HierarchyException If the port is not contained in
     *                            this hierarchy.
     * @see DIFAttribute
     * @see AttributeContainer
     */
    public DIFAttribute getAttribute(Port port, String name) {
        if (getPorts().get(port.getName()) == null) {
            throw new HierarchyException(
                    "Port: " + port.getName() + " is not in this hierarchy.");
        }
        AttributeContainer container = (AttributeContainer) _containers.get(
                port);
        if (container == null) {
            return null;
        }
        return container.getAttribute(name);
    }

    /**
     * Returns a List of DIFAttribute associated with <i>port</i>.
     *
     * @param port A Port in this hierarchy.
     * @return A List of DIFAttribute associated with <i>port</i>.
     * If there is no AttributeContainer associated with <i>port</i>, it
     * returns null.
     * @throws HierarchyException If the port is not contained in
     *                            this hierarchy.
     * @see AttributeContainer
     */
    public List getAttributes(Port port) {
        if (getPorts().get(port.getName()) == null) {
            throw new HierarchyException(
                    "Port: " + port.getName() + " is not in this hierarchy.");
        }
        AttributeContainer container = (AttributeContainer) _containers.get(
                port);
        if (container == null) {
            return null;
        }
        return container.getAttributes();
    }

    /**
     * Override {@link Hierarchy#flatten(Node)}.
     * Support set the production and consumption attribute of sub port to
     * the edge in the flattened graph. Also support reset the attribute
     * value that refers to the sub port to the new edge or outer port in
     * the flattened graph.<p>
     * Note that the original hierarchy is modified and cannot be
     * restored. If <i>superNode</i> is not actually a super node (it is
     * just a normal node), this hierarchy object won't be modified and
     * this method returns null.
     *
     * @return A DIFHierarchy object of the cloned version of the original
     * sub-hierarchy.
     */
    @Override public Hierarchy flatten(Node superNode) {
        if (getSuperNodes().get(superNode) == null) {
            return null;
        }
        HierarchyException exception = HierarchyException.checkFlatten(
                this, superNode);
        if (exception != null) {
            throw exception;
        }

        DIFGraph graph = (DIFGraph) getGraph();
        DIFHierarchy subHierarchy = (DIFHierarchy) getSuperNodes().get(
                superNode);
        DIFGraph subGraph = (DIFGraph) subHierarchy.getGraph();

        DIFHierarchy clonedSubHierarchy = (DIFHierarchy) subHierarchy.mirror();

        // Merge graphs.
        graph.mergeGraph(subGraph);

        for (Iterator subPorts = subHierarchy.getPorts().iterator();
             subPorts.hasNext(); ) {
            Port subPort = (Port) subPorts.next();
            int direction = subPort.getDirection();

            if (subPort.getConnection() instanceof Edge) {
                Edge outerEdge = (Edge) subPort.getConnection();
                String edgeName = graph.getName(outerEdge);
                Iterator edgeAttrs = graph.getAttributes(outerEdge).iterator();

                //Cannot do graph.removeEdge(outerEdge) at this time,
                //it will _removeReferenceFromAttributes(edge);
                //Which we will first do in _updateAttribute

                //disconnect outer edge
                //this will also unrelate the outer port to subPort
                subPort.disconnect();

                //has inside subNode and that subNode is a subSuperNode
                if (subPort.getNode() != null &&
                        subPort.getRelatedPort() != null) {
                    Node insideSuperNode = subPort.getNode();
                    Port subSubPort = subPort.getRelatedPort();
                    //unrelate inside node and inside port.
                    subPort.unrelate();

                    if (direction == Port.IN && outerEdge.sink() == superNode &&
                            subSubPort.getDirection() == Port.IN) {
                        DIFEdgeWeight edgeWeight =
                                (DIFEdgeWeight) outerEdge.getWeight();
                        Edge newOuterEdge = new Edge(outerEdge.source(),
                                                     insideSuperNode, edgeWeight
                        );
                        //FIXME If edge weight has source/sink port object
                        graph.addEdge(newOuterEdge);
                        graph.setName(newOuterEdge, edgeName);
                        for (; edgeAttrs.hasNext(); ) {
                            DIFAttribute edgeAttr =
                                    (DIFAttribute) edgeAttrs.next();
                            graph.setAttribute(newOuterEdge, edgeAttr);
                        }
                        subSubPort.connect(newOuterEdge);
                        _updateSuperNodeConnection(outerEdge, newOuterEdge,
                                                   "source"
                                                  );
                        _updateAttributes(subPort, newOuterEdge);
                        _updateAttributes(outerEdge, newOuterEdge);
                    } else if (direction == Port.OUT &&
                            outerEdge.source() == superNode &&
                            subSubPort.getDirection() == Port.OUT) {
                        DIFEdgeWeight edgeWeight =
                                (DIFEdgeWeight) outerEdge.getWeight();
                        Edge newOuterEdge = new Edge(insideSuperNode,
                                                     outerEdge.sink(),
                                                     edgeWeight
                        );
                        //FIXME If edge weight has source/sink port object
                        graph.addEdge(newOuterEdge);
                        graph.setName(newOuterEdge, edgeName);
                        for (; edgeAttrs.hasNext(); ) {
                            DIFAttribute edgeAttr =
                                    (DIFAttribute) edgeAttrs.next();
                            graph.setAttribute(newOuterEdge, edgeAttr);
                        }
                        subSubPort.connect(newOuterEdge);
                        _updateSuperNodeConnection(outerEdge, newOuterEdge,
                                                   "sink"
                                                  );
                        _updateAttributes(subPort, newOuterEdge);
                        _updateAttributes(outerEdge, newOuterEdge);
                    } else {
                        throw new RuntimeException("subPort direction and " +
                                                           "outerEdge " +
                                                           "direction do not " +
                                                           "match.");
                    }
                }
                //has inside subNode and that subNode is just a normal node
                else if (subPort.getNode() != null &&
                        subPort.getRelatedPort() == null) {
                    Node insideNode = subPort.getNode();
                    //unrelate inside node and inside port.
                    subPort.unrelate();

                    if (direction == Port.IN && outerEdge.sink() == superNode) {
                        DIFEdgeWeight edgeWeight =
                                (DIFEdgeWeight) outerEdge.getWeight();
                        if (subHierarchy.getAttribute(subPort,
                                                      "consumption") !=
                                null) {
                            if (subHierarchy.getAttribute(
                                    subPort, "consumption").getValue() !=
                                    null) {
                                edgeWeight.setConsumptionRate(subHierarchy
                                                  .getAttribute(
                                                          subPort,
                                                          "consumption"
                                                               )
                                                  .getValue());
                            }
                        }
                        Edge newOuterEdge = new Edge(outerEdge.source(),
                                                     insideNode, edgeWeight
                        );
                        //FIXME If edge weight has source/sink port object
                        graph.addEdge(newOuterEdge);
                        graph.setName(newOuterEdge, edgeName);
                        for (; edgeAttrs.hasNext(); ) {
                            DIFAttribute edgeAttr =
                                    (DIFAttribute) edgeAttrs.next();
                            graph.setAttribute(newOuterEdge, edgeAttr);
                        }
                        _updateSuperNodeConnection(outerEdge, newOuterEdge,
                                                   "source"
                                                  );
                        _updateAttributes(subPort, newOuterEdge);
                        _updateAttributes(outerEdge, newOuterEdge);
                    } else if (direction == Port.OUT &&
                            outerEdge.source() == superNode) {
                        DIFEdgeWeight edgeWeight =
                                (DIFEdgeWeight) outerEdge.getWeight();
                        if (subHierarchy.getAttribute(subPort,
                                                      "production") !=
                                null) {
                            if (subHierarchy.getAttribute(subPort,
                                                          "production")
                                            .getValue() != null) {
                                edgeWeight.setProductionRate(subHierarchy
                                             .getAttribute(
                                                     subPort,
                                                     "production"
                                                          )
                                             .getValue());
                            }
                        }
                        Edge newOuterEdge = new Edge(insideNode,
                                                     outerEdge.sink(),
                                                     edgeWeight
                        );
                        //FIXME If edge weight has source/sink port object
                        graph.addEdge(newOuterEdge);
                        graph.setName(newOuterEdge, edgeName);
                        for (; edgeAttrs.hasNext(); ) {
                            DIFAttribute edgeAttr =
                                    (DIFAttribute) edgeAttrs.next();
                            graph.setAttribute(newOuterEdge, edgeAttr);
                        }
                        _updateSuperNodeConnection(outerEdge, newOuterEdge,
                                                   "sink"
                                                  );
                        _updateAttributes(subPort, newOuterEdge);
                        _updateAttributes(outerEdge, newOuterEdge);
                    } else {
                        throw new RuntimeException("subPort direction and " +
                                                           "outerEdge " +
                                                           "direction do not " +
                                                           "match.");
                    }
                } else {
                    //no inside associated sub-node, outerEdge has no new sink
                }
                //remove original outer edge
                //graph.removeEdge(outerEdge);

            } else if (subPort.getConnection() instanceof Port) {
                Port outerPort = (Port) subPort.getConnection();
                //disconnect outer port
                // equal to subPort.disconnect();
                outerPort.unrelate();

                //has inside subNode and that subNode is a subSuperNode
                if (subPort.getNode() != null &&
                        subPort.getRelatedPort() != null) {
                    Node insideSuperNode = subPort.getNode();
                    Port subSubPort = subPort.getRelatedPort();
                    //unrelate inside node and inside port.
                    subPort.unrelate();

                    if (direction == Port.IN &&
                            outerPort.getDirection() == Port.IN &&
                            subSubPort.getDirection() == Port.IN) {
                        //equal to outerPort.relate(subSubPort)
                        subSubPort.connect(outerPort);
                        if (outerPort.getNode() != insideSuperNode) {
                            throw new RuntimeException(
                                    "outerPort related node" +
                                            " and inside super node do not " +
                                            "match.");
                        }
                        _updateAttributes(subPort, outerPort);
                    } else if (direction == Port.OUT &&
                            outerPort.getDirection() == Port.OUT &&
                            subSubPort.getDirection() == Port.OUT) {
                        //equal to outerPort.relate(subSubPort)
                        subSubPort.connect(outerPort);
                        if (outerPort.getNode() != insideSuperNode) {
                            throw new RuntimeException(
                                    "outerPort related node" +
                                            " and inside super node do not " +
                                            "match.");
                        }
                        _updateAttributes(subPort, outerPort);
                    } else {
                        throw new RuntimeException("subPort direction and " +
                                                           "outerPort " +
                                                           "direction do not " +
                                                           "match.");
                    }
                }
                //has inside subNode and that subNode is just a normal node
                else if (subPort.getNode() != null &&
                        subPort.getRelatedPort() == null) {
                    Node insideNode = subPort.getNode();
                    //unrelate inside node and inside port.
                    subPort.unrelate();

                    if (direction == Port.IN &&
                            outerPort.getDirection() == Port.IN) {
                        outerPort.relate(insideNode);
                        _updateAttributes(subPort, outerPort);
                    } else if (direction == Port.OUT &&
                            outerPort.getDirection() == Port.OUT) {
                        outerPort.relate(insideNode);
                        _updateAttributes(subPort, outerPort);
                    } else {
                        throw new RuntimeException("subPort direction and " +
                                                           "outerEdge " +
                                                           "direction do not " +
                                                           "match.");
                    }
                } else {
                    //no inside associated sub-node, outerPort has not
                    // related node
                }

            } else { // subPort do not have outside connection
                if (subPort.getNode() != null) {
                    subPort.unrelate();
                }
            }
        }

        // put all subSubHierarchy as superNode in the current Hierarchy
        for (Iterator subSuperNodes = subHierarchy.getSuperNodes().iterator();
             subSuperNodes.hasNext(); ) {
            Node subSuperNode = (Node) subSuperNodes.next();
            DIFHierarchy subSubHierarchy =
                    (DIFHierarchy) subHierarchy.getSuperNodes().get(
                            subSuperNode);
            _putSuperNode(subSuperNode, subSubHierarchy);
        }

        // remove superNode
        _removeSuperNode(superNode);
        // Remove superNode and incident edges.
        graph.removeNode(superNode);

        return clonedSubHierarchy;
    }

    /**
     * Override {@link Hierarchy#mirror} to restrict
     * to always clone weights. Note that DIFGraph requires all elements
     * to have proper weights.
     *
     * @return The mirrored DIFHierarchy.
     */
    @Override public Hierarchy mirror(boolean cloneWeights) {
        if (cloneWeights == true) {
            return mirror();
        } else {
            throw new IllegalArgumentException("Input argument: " +
                                                       "cloneWeights can only" +
                                                       " be true. Because " +
                                                       "DIFGraph " +
                                                       "requires all elements" +
                                                       " to have valid " +
                                                       "weights.");
        }
    }

    /**
     * Completely mirror/clone this hierarchy, sub-hierarchies, graphs,
     * attributes, and parameters.
     *
     * @return The mirrored DIFHierarchy.
     */
    public DIFHierarchy mirror() {
        return mirror(new HashMap());
    }

    /**
     * Mirror/clone this hierarchy, sub-hierarchies, graphs,
     * attributes, and parameters based on the given mirrorMap
     *
     * @return The mirrored DIFHierarchy.
     */
    public DIFHierarchy mirror(Map mirrorMap) {
        //In order to allow the derived hierarchy class to call the correct
        //graph class. These methods are provided by java.lang.reflect
        DIFHierarchy mirrorHierarchy = null;
        Class[] classes = {_graphType().getClass(), (new String()).getClass()};
        Object[] parameters = {_graphType(), getName()};
        try {
            Constructor constructor = getClass().getConstructor(classes);
            mirrorHierarchy = (DIFHierarchy) constructor.newInstance(
                    parameters);
        } catch (Exception exception) {
            throw new HierarchyException(exception.getMessage());
        }

        //mirror ports in this hierarchy without connect or relate to
        //anything.
        for (Iterator ports = getPorts().iterator(); ports.hasNext(); ) {
            Port port = (Port) ports.next();
            Port mirrorPort = new Port(port.getName(), mirrorHierarchy,
                                       port.getDirection()
            );
            mirrorMap.put(port, mirrorPort);
        }

        //** add the following mappings to graph **//
        //1. DIFParameters in the upper level.
        //2. Ports in the current level.

        //** Performing graph mirroring here. **//
        ((DIFHierarchy) mirrorHierarchy)._graph = ((DIFGraph) _graph).mirror(
                mirrorMap);

        //mirror subhierarchies and connect mirrored subports to current
        //level mirrored hierarchy
        for (Iterator superNodes = getSuperNodes().iterator();
             superNodes.hasNext(); ) {
            Node superNode = (Node) superNodes.next();
            DIFHierarchy subHierarchy = (DIFHierarchy) getSuperNodes().get(
                    superNode);
            Node mirrorNode = (Node) mirrorMap.get(superNode);
            DIFHierarchy mirrorSubHierarchy = subHierarchy.mirror(mirrorMap);
            mirrorHierarchy.addSuperNode(mirrorNode, mirrorSubHierarchy);
            // Make subport - edge connections now. (subPort - port
            // connections will be dealt with in the last for loop)
            // Connect the edges in this hierarchy to ports in sub
            // hierarchies
            Iterator mirrorPorts = mirrorSubHierarchy.getPorts().iterator();
            for (Iterator ports = subHierarchy.getPorts().iterator();
                 ports.hasNext(); ) {
                Port subPort = (Port) ports.next();
                Port mirrorSubPort = (Port) mirrorPorts.next();
                if (subPort.getConnection() instanceof Edge) {
                    Edge mirrorEdge = (Edge) mirrorMap.get(
                            subPort.getConnection());
                    mirrorSubPort.connect(mirrorEdge);
                }
            }
        }

        // relate mirror ports to inside nodes / inside sub ports
        // mirror port attributes
        for (Iterator ports = getPorts().iterator(); ports.hasNext(); ) {
            Port port = (Port) ports.next();
            Port mirrorPort = (Port) mirrorMap.get(port);
            if (port.getNode() != null) {
                Node mirrorNode = (Node) mirrorMap.get(port.getNode());
                if (port.getRelatedPort() != null) {
                    PortList mirrorSubPorts =
                            mirrorHierarchy.getSuperNodes().get(mirrorNode)
                                           .getPorts();
                    Port mirrorSubPort = mirrorSubPorts.get(
                            port.getRelatedPort().getName());
                    mirrorSubPort.connect(mirrorPort);
                } else {
                    mirrorPort.relate(mirrorNode);
                }
            }
        }

        // mirror port AttributeContainer
        for (Iterator ports = getPorts().iterator(); ports.hasNext(); ) {
            Port port = (Port) ports.next();
            Port mirrorPort = (Port) mirrorMap.get(port);
            AttributeContainer container = (AttributeContainer) _containers.get(
                    port);
            if (container != null) {
                AttributeContainer mirrorContainer =
                        (AttributeContainer) container.clone(mirrorMap);
                mirrorHierarchy._containers.put(mirrorPort, mirrorContainer);
            }
        }

        return mirrorHierarchy;
    }

    /**
     * Sets an attribute of a Port. If there exist another attribute with
     * the same name, the previous value is overridden.<p>
     * DIF utilizes edge weights to record the production rate and
     * consumption rate of the source node and sink node of that edge.
     * No edge will be built between a nod and its associated port.
     * In this situation, DIF specifies the production rate and consumption
     * rate as port attributes. Therefore, "production" and "consumption"
     * attributes are reserved for ports in this purpose.
     * Because ports do not have weights to further distinguish between
     * different types of weights (ex: CSDF, BCSDF, SDF, HSDF), users need
     * to be very careful about assigning the "production" and "consumption"
     * attributes.<p>
     * A port cannot have both "production" and "consumption" attributes at
     * the same time. Because {@link Port} is
     * directional.
     *
     * @param port      A port in this hierarchy.
     * @param attribute A DIFAttribute.
     * @throws HierarchyException       If the port is not contained in
     *                                  this hierarchy.
     * @throws IllegalArgumentException If the name of attribute is null.
     */
    public void setAttribute(Port port, DIFAttribute attribute) {
        if (getPorts().get(port.getName()) == null) {
            throw new HierarchyException(
                    "Port: " + port.getName() + " is not in this hierarchy.");
        }
        if (_containers.get(port) == null) {
            AttributeContainer container = new AttributeContainer(
                    port.getName());
            //set the backward reference to this graph.
            container._setContainer(this);
            _containers.put(port, container);
            container.setAttribute(attribute);
        } else {
            if (attribute.getName() == "production") {
                if (port.getDirection() != Port.OUT) {
                    throw new HierarchyException(
                            "Input Port: " + port.getName() +
                                    " cannot have production attribute.");
                }
            } else if (attribute.getName() == "consumption") {
                if (port.getDirection() != Port.IN) {
                    throw new HierarchyException(
                            "Output Port: " + port.getName() +
                                    " cannot have consumption attribute.");
                }
            }
            AttributeContainer container = (AttributeContainer) _containers.get(
                    port);
            container.setAttribute(attribute);
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////           protected methods                               ////
    @Override protected Edge _copyEdge(Edge edge, Node source, Node sink) {
        Edge newEdge = null;
        if (edge.getWeight() != null) {
            newEdge = new Edge(source, sink, edge.getWeight());
        } else {
            newEdge = new Edge(source, sink);
        }
        getGraph().addEdge(newEdge);
        ((DIFGraph) getGraph())._setAttributeContainer(newEdge,
                               (AttributeContainer) ((DIFGraph) getGraph())
                                       ._getAttributeContainer(
                                               edge)
                                       .clone()
                              );
        return newEdge;
    }

    ///////////////////////////////////////////////////////////////////
    ////                     private methods                       ////

    /**
     * Returns an empty {@link DIFGraph}.
     *
     * @return An empty {@link DIFGraph}.
     */
    @Override protected Graph _graphType() {
        return new DIFGraph();
    }

    /**
     * Update all attributes of nodes, edges, and graph if their value are
     * equal to key objects in map. If yes, their values are updated to
     * the corresponding mapped object.
     *
     * @param oldEle
     * @param newEle
     */
    private void _updateAttributes(Object oldEle, Object newEle) {
        DIFGraph graph = (DIFGraph) getGraph();
        for (Iterator nodes = graph.nodes().iterator(); nodes.hasNext(); ) {
            Node node = (Node) nodes.next();
            for (Iterator attributes = graph.getAttributes(node).iterator();
                 attributes.hasNext(); ) {
                DIFAttribute attribute = (DIFAttribute) attributes.next();
                if (attribute.getValue() == oldEle) {
                    attribute.setValue(newEle);
                    //System.out.println(attribute.getName()
                    //+ graph.getName((Edge)attribute.getValue()));
                }
            }
        }
        for (Iterator edges = graph.edges().iterator(); edges.hasNext(); ) {
            Edge edge = (Edge) edges.next();
            for (Iterator attributes = graph.getAttributes(edge).iterator();
                 attributes.hasNext(); ) {
                DIFAttribute attribute = (DIFAttribute) attributes.next();
                if (attribute.getValue() == oldEle) {
                    attribute.setValue(newEle);
                }
            }
        }
        for (Iterator attributes = graph.getAttributes().iterator();
             attributes.hasNext(); ) {
            DIFAttribute attribute = (DIFAttribute) attributes.next();
            if (attribute.getValue() == oldEle) {
                attribute.setValue(newEle);
            }
        }
    }

    ///////////////////////////////////////////////////////////////////
    ////                     private variables                     ////

    /**
     * If the source or sink of oldEdge is a super node. Then the sub port
     * connection of super node is updated to newEdge.
     *
     * @param oldEdge
     * @param newEdge
     * @param end     "source" or "sink".
     */
    private void _updateSuperNodeConnection(Edge oldEdge, Edge newEdge,
                                            String end) {
        DIFGraph graph = (DIFGraph) getGraph();
        Node node;
        if (end.equals("source")) {
            node = oldEdge.source();
            if (getSuperNodes().contains(node)) {
                DIFHierarchy subHierarchy = (DIFHierarchy) getSuperNodes().get(
                        node);
                for (Iterator ports = subHierarchy.getPorts().iterator();
                     ports.hasNext(); ) {
                    Port port = (Port) ports.next();
                    if (port.getConnection() == oldEdge) {
                        port.connect(newEdge);
                    }
                }
            }
        } else if (end.equals("sink")) {
            node = oldEdge.sink();
            if (getSuperNodes().contains(node)) {
                DIFHierarchy subHierarchy = (DIFHierarchy) getSuperNodes().get(
                        node);
                for (Iterator ports = subHierarchy.getPorts().iterator();
                     ports.hasNext(); ) {
                    Port port = (Port) ports.next();
                    if (port.getConnection() == oldEdge) {
                        port.connect(newEdge);
                    }
                }
            }
        }
    }

}


