/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/
package dif.pafg;

import dif.DIFGraph;
import dif.attributes.PAFGAttributeType;
import dif.data.expr.Variable;
import dif.pafg.PAFGEdgeWeight;
import dif.pafg.PAFGEdgeWeight;
import dif.pafg.PAFGNodeWeight;
import dif.util.graph.Edge;

import java.util.Iterator;

public class PAFGGraph extends DIFGraph {
    private PAFGAttributeType _type = null;

    public void setGraphType(PAFGAttributeType type) {
        _type = type;
    }

    public void printEdgeRateExpressions() {
        System.out.println("Printing edge rate expressions.");
        Iterator edges = edges().iterator();
        while (edges.hasNext()) {
            Edge edge = (Edge) edges.next();
            PAFGEdgeWeight weight = (PAFGEdgeWeight) edge.getWeight();
            Variable productionRate = weight.getPAFGProductionRate();
            if (productionRate == null) {
                System.out.print("null");
            } else {
                System.out.print(productionRate.getExpression());
            }
            System.out.print(", ");
            Variable consumptionRate = weight.getPAFGConsumptionRate();
            if (consumptionRate == null) {
                System.out.print("null");
            } else {
                System.out.print(consumptionRate.getExpression());
            }
            System.out.println();
        }
        System.out.println("Finished printing edge rate expressions.");
    }
    @Override
    public boolean validEdgeWeight(Object weight) {
        return weight instanceof PAFGEdgeWeight;
    }

    @Override
    public boolean validNodeWeight(Object weight) {
        return weight instanceof PAFGNodeWeight;
    }

}
