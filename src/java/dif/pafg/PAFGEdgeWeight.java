/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/
package dif.pafg;

import dif.data.expr.Variable;
import dif.util.NamedObj;
import dif.DIFEdgeWeight;
import dif.util.graph.Edge;
import dif.util.graph.Graph;

public class PAFGEdgeWeight extends DIFEdgeWeight{
    private String _consumptionRateExpression;
    // A dummy container for variables that are defined in this edge weight.
    private NamedObj _container;
    // An expression that specifies the production rate of the edge.
    // This string is used as a scratchpad by schedulers to keep track
    // of production rate expressions during scheduling.
    private String _productionRateExpression;

    public PAFGEdgeWeight() {
        _init();
    }
    public PAFGEdgeWeight(Variable productionRate, Variable consumptionRate,
                          Variable delay) {
        _init();
        setProductionRate(productionRate);
        setConsumptionRate(consumptionRate);
        setDelay(delay);
    }
    public PAFGEdgeWeight(Object sourcePort, Object sinkPort,
                          Variable productionRate, Variable consumptionRate, Variable delay) {

        setSinkPort(sinkPort);
        setSourcePort(sourcePort);
        setProductionRate(productionRate);
        setConsumptionRate(consumptionRate);
        setDelay(delay);
    }
    public Variable getPAFGConsumptionRate() {
        return (Variable) getConsumptionRate();
    }
    public void setPAFGConsumptionRate(Variable consumptionRate) {
        setConsumptionRate(consumptionRate);
    }
    public Variable getPAFGDelay() {
        return (Variable) getDelay();
    }
    public void setPAFGDelay(Variable delay) {
        setDelay(delay);
    }
    public Variable getPAFGProductionRate() {
        return (Variable) getProductionRate();
    }
    public void setPAFGProductionRate(Variable productionRate) {
        setProductionRate(productionRate);
    }
    public String getConsumptionRateExpression() {
        return _consumptionRateExpression;
    }
    public void setConsumptionRateExpression(String expression) {
        _consumptionRateExpression = expression;
    }
    public String getProductionRateExpression() {
        return _productionRateExpression;
    }
    public void setProductionRateExpression(String expression) {
        _productionRateExpression = expression;
    }
    public String toString() {
        return new String(getProductionRate() + " " + getConsumptionRate()
                + " " + getDelay());
    }

    private void _init() {
        try {
            setSinkPort(null);
            setSourcePort(null);
            setConsumptionRateExpression(null);
            setProductionRateExpression(null);

            _container = new NamedObj("PAFGEdgeWeight");

            // Initialize the delay to the default value
            Variable delayVariable = new Variable(_container, "PAFGDelay");
            delayVariable.setExpression("0");
            setDelay(delayVariable);

            // Initialize the production rate to the default value
            Variable productionRateVariable =
                    new Variable(_container, "PAFGProductionRate");
            productionRateVariable.setExpression("1");
            setProductionRate(productionRateVariable);

            // Initialize the consumption rate to the default value
            Variable consumptionRateVariable =
                    new Variable(_container, "PAFGConsumptionRate");
            consumptionRateVariable.setExpression("1");
            setConsumptionRate(consumptionRateVariable);
        } catch (Exception exception) {
            System.err.println("Could not initialize PAFG edge weight.");
            exception.printStackTrace();
            throw new RuntimeException("Could not initialize PAFG edge weight."
                    + ".\n" + exception.getMessage() + "\n");
        }
    }
}
