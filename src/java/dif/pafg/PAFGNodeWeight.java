/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/
package dif.pafg;
import dif.DIFNodeWeight;

public class PAFGNodeWeight extends DIFNodeWeight {
    private static PAFGNodeWeight ourInstance = new PAFGNodeWeight();

    public static PAFGNodeWeight getInstance() {
        return ourInstance;
    }
    public PAFGNodeWeight(Object computation) {
        setComputation(computation);
    }
    public PAFGNodeWeight() {
        setComputation(null);
    }
    public String toString() {
        String result = getClass() + ", computation: ";
        if (getComputation() == null) {
            result += "null";
        } else {
            if (getComputation() instanceof String) {
                result += getComputation();
            } else {
                result += getComputation().getClass().getName();
            }
        }
        return result;
    }

}
