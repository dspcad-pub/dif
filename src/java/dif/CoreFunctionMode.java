/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

//package mocgraph;
package dif;

import java.util.LinkedHashMap;
import java.util.Set;
import java.util.Iterator;

import dif.attributes.CFDFAttributeType;
import dif.util.graph.Node;

////////////////////////////////////////////////////////////////////////// //
//Core Function Mode

/**
 * @author William Plishker, Shuvra S. Bhattacharyya
 * @version $Id: CoreFunctionNode.java 1634 2007-04-06 18:24:07Z ssb $
 * @see Node
 */
public class CoreFunctionMode {
    /**
     * Construct a core function mode with a given name.
     */

    public CoreFunctionMode(String name) {
        _inputs = new LinkedHashMap();
        _outputs = new LinkedHashMap();
        _name = name;
    }

    /**
     * Construct a core function mode with a given name and type.
     */
    public CoreFunctionMode(String name, CFDFAttributeType type) {
        _inputs = new LinkedHashMap();
        _outputs = new LinkedHashMap();
        _name = name;
        setType(type);
    }

    /**
     * Construct a <b>group</b> core function mode from two other modes.
     * <p>
     * This takes in two modes and constructs a group mode from them.
     * Note this is not a <b>super</b> mode because the original
     * modes are still availible in mode list, and a single mode can
     * be included in multiple group modes.
     */
    //we may want to extend this to more than two modes
    //we may want to extend the class to a GroupMode
    public CoreFunctionMode(String name, CoreFunctionMode cfm1,
                            CoreFunctionMode cfm2) {
        _inputs = new LinkedHashMap();
        _outputs = new LinkedHashMap();
        _cfm1 = cfm1;
        _cfm2 = cfm2;
        _name = name;

        //the whole point of the group mode is to combine inputs and
        //outputs when modes have the same port, they need to be added
        //together so first loop through the first mode, trying to add
        //ports (0 is returned by default if it isn't present), and
        //then loop through the second mode to get it out remaining
        //ports

        //Inputs
        Iterator iter1 = cfm1.getInputs().iterator();
        while (iter1.hasNext()) {
            String port = (String) iter1.next();
            setConsumption(
                    port,
                    cfm1.getConsumption(port) + cfm2.getConsumption(port)
                          );
        }
        iter1 = cfm2.getInputs().iterator();
        while (iter1.hasNext()) {
            String port = (String) iter1.next();
            if (cfm1.getConsumption(port) == 0) { //don't double add
                setConsumption(port, cfm2.getConsumption(port));
            }
        }

        //Outputs
        iter1 = cfm1.getOutputs().iterator();
        while (iter1.hasNext()) {
            String port = (String) iter1.next();
            setProduction(
                    port, cfm1.getProduction(port) + cfm2.getProduction(port));
        }
        iter1 = cfm2.getOutputs().iterator();
        while (iter1.hasNext()) {
            String port = (String) iter1.next();
            if (cfm1.getProduction(port) == 0) { //don't double add
                setProduction(port, cfm2.getProduction(port));
            }
        }


        setType(CFDFAttributeType.GroupMode);
    }


    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    /**
     * Set the type of this mode.
     *
     * @param type CFDFAttributeType.*
     */

    public void setType(CFDFAttributeType type) {
        _type = type;

        //	_name = "internal_state_" + _name;
        //create corresponding true and false modes
        if (_type == CFDFAttributeType.BooleanMode) {
            _trueNextMode = new CoreFunctionMode(_name + "_true");
            _falseNextMode = new CoreFunctionMode(_name + "_false");
        }
    }

    ;


    /**
     * Add a production rate to a particular edge of the actor.
     *
     * @param port The string object representing the port.
     * @param rate The integer production rate.
     */

    public void setProduction(String port, int rate) {
        if (_outputs.containsKey(port)) {
            System.out.println(
                    "WARNING: Overwriting " + port + " in mode " + toString() +
                            ".");
        }
        _outputs.put(port, rate);
    }

    ;

    /**
     * Add a consumption rate to a particular edge of the actor.
     *
     * @param port The string object representing the port.
     * @param rate The integer consumption rate.
     */

    public void setConsumption(String port, int rate) {
        if (_inputs.containsKey(port)) {
            System.out.println(
                    "WARNING: Overwriting " + port + " in mode " + toString() +
                            ".");
        }
        _inputs.put(port, rate);
    }

    ;

    /**
     * Get production rate to a particular edge of the actor.
     *
     * @param port The string object representing the port.
     * @return The integer production rate of <code>port</code>.
     */

    public int getProduction(String port) {
        if (!_outputs.containsKey(port)) {
            return 0;
        }
        return (Integer) _outputs.get(port);
    }

    /**
     * Get consumption rate to a particular edge of the actor.
     *
     * @param port The string object representing the port.
     * @return The integer consumption rate of <code>port</code>.
     */

    public int getConsumption(String port) {
        if (!_inputs.containsKey(port)) {
            return 0;
        }
        return (Integer) _inputs.get(port);
    }


    /**
     * Get the type of this node.
     *
     * @return CFDFAttributeType.*
     */
    public CFDFAttributeType getType() {
        return _type;
    }

    public String getName() {
        return _name;
    }

    public Set getInputs() {
        return _inputs.keySet();
    }

    public Set getOutputs() {
        return _outputs.keySet();
    }

    public CoreFunctionMode getTrueNextMode() {
        if (_type == CFDFAttributeType.BooleanMode) {
            return _trueNextMode;
        }
        return null;
    }

    public CoreFunctionMode getFalseNextMode() {
        if (_type == CFDFAttributeType.BooleanMode) {
            return _falseNextMode;
        }
        return null;
    }

    public String toString() {
        return super.toString();
    }

    public boolean visited() {
        return _visited;
    }

    public void setVisited(boolean val) {
        _visited = val;
    }


    ///////////////////////////////////////////////////////////////////
    ////                         protected methods                 ////


    ///////////////////////////////////////////////////////////////////
    ////                         private vaiables                   ////

    CoreFunctionMode _cfm1;
    CoreFunctionMode _cfm2;

    CFDFAttributeType _type;

    CoreFunctionMode _trueNextMode;
    CoreFunctionMode _falseNextMode;

    String _name;

    private LinkedHashMap _outputs;
    private LinkedHashMap _inputs;

    private boolean _visited;
}


