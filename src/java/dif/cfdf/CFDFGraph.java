/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Information associated with a CFDF graph. */

package dif.cfdf;

import dif.attributes.PCFDFAttributeType;
import dif.attributes.CFDFAttributeType;


//import dif.data.expr.Variable;
import dif.CoreFunctionNode;
import dif.CoreFunctionEdge;
import dif.CoreFunctionMode;
//import dif.actors.PrintCount;
import dif.DIFGraph;
import dif.util.graph.Edge;
import dif.util.graph.Node;

import java.util.Iterator;
import java.util.Collection;
import java.util.LinkedHashMap;
//////////////////////////////////////////////////////////////////////////
//// CFDFGraph
/** Information associated with a CFDF graph.
<p>
CFDFGraph nodes and edges have types {@link CoreFunctionNode} and
{@link CoreFunctionEdge}, respectively.

@author Shuvra S. Bhattacharyya and William Plishker
@version $Id: CFDFGraph.java 416 2007-05-31 05:19:54Z plishker $
@see CFDFGraphs
*/

public class CFDFGraph extends DIFGraph {

    /** Construct an empty CFDF graph.
     */
    public CFDFGraph() {
        super();
    }

    /** Construct an empty CFDF graph with enough storage allocated for the
     *  specified number of nodes.
     *  @param nodeCount The number of nodes.
     */
    public CFDFGraph(int nodeCount) {
        super(nodeCount);
    }

    /** Construct an empty CFDF graph with enough storage allocated for the
     *  specified number of edges, and number of nodes.
     *  @param nodeCount The integer specifying the number of nodes
     *  @param edgeCount The integer specifying the number of edges
     */
    public CFDFGraph(int nodeCount, int edgeCount) {
        super(nodeCount, edgeCount);
    }

    ///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////


    /** Add a new weighted CFNode to this graph given the node weight.
     *
     *  @param weight The node weight.
     *  @return The node.
     *  @exception GraphWeightException If the specified weight is null.
     */
    public Node addNodeWeight(CoreFunctionNode weight) {
        Node node = new Node(weight);
        _registerNode(node);
        return node;
    }

    /** Overloaded to empty.  Needs to be rewritten for CFNode, if it
     *  makes sense.
     *  @return The connected components.
     */
    //    public Collection connectedComponents() {return null;} 


    /** Get the type of this graph.
     *  @return PCFDFAttributeType.InitGraph,
     *  PCFDFAttributeType.SubinitGraph, or PCFDFAttributeType.BodyGraph.
     */
    public PCFDFAttributeType getGraphType() {
        return _type;
    }

    /** set type to either PCFDFAttributeType.InitGraph,
     *  PCFDFAttributeType.SubinitGraph, PCFDFAttributeType.BodyGraph, or
     *  PCFDFAttributeType.PCFDFSpecification.
     *  @param type PCFDFAttributeType
     */

    public void setGraphType(PCFDFAttributeType type) {
        _type = type;
    }

    /** Initialize internal structure to prep for running.
     *
     *  Note this is specifically <b>not</b> for actor initilization
     *  or any parameter settings as described by the user.  All of
     *  that functionality should be reperestented in the application
     *  graph.  This function is strictly for setting up the internals
     *  of DIF related variables before running a graph.  Ideally this
     *  would be a private function called before invoking actors in
     *  this graph, but for the time being it is a public function
     *  that must be called seperately before running actors in the
     *  graph.
     *
     */

    public void simInit() {
	Collection nodeCol = nodes();
	Iterator nodeItr = nodeCol.iterator();
	while(nodeItr.hasNext()) {
	    Node node = (Node)nodeItr.next();
	    CoreFunctionNode cfn = (CoreFunctionNode)nodeWeight(nodeLabel(node));
	    cfn.setGraph(this);
	    cfn.setNode(node);

	    if(cfn.getNodeType()==CFDFAttributeType.BooleanNode) {
		cfn.createModes();
	    }
	}
    }


    public void run() {
	Collection nodeCol = nodes();
	Iterator nodeItr = nodeCol.iterator();
	while(nodeItr.hasNext()) {
	    Node node = (Node)nodeItr.next();
	    CoreFunctionNode cfn = (CoreFunctionNode)nodeWeight(nodeLabel(node));
	    //System.out.println(nw.getComputation());
	    //	    Object nextMode;
	    if(!modes.containsKey(cfn)) {
	        modes.put(cfn,cfn.invoke("init"));
	    }

	    if(cfn.enable((CoreFunctionMode)modes.get(cfn))) {
		modes.put(cfn,cfn.invoke((CoreFunctionMode)modes.get(cfn)));
	    } else {
		//non-verbosely:
		System.out.print(".");
		//verbosely:
		//		System.out.println("(" + cfn + " did not fire because not all inputs were ready.)");
	    }
	}
    }

    public void printState() {
	Collection edgeCol = edges();
	Iterator edgeItr = edgeCol.iterator();
	int size = 0;
	int max = 0;
	while(edgeItr.hasNext()) {
	    Edge edge = (Edge)edgeItr.next();
	    CoreFunctionEdge cfe = (CoreFunctionEdge)edgeWeight(edgeLabel(edge));
	    if(cfe.max()>0) {
		System.out.println(getName(edge) + ": " + cfe.printStats());
	    }
	    size += cfe.peek();
	    max += cfe.max();
	}
	if(size>_max) {
	    _max = size;
	}
	System.out.println("Max: " + _max + ", " + max);
    }

    public void resetVisited() {
	Collection nodeCol = nodes();
	Iterator nodeItr = nodeCol.iterator();
	while(nodeItr.hasNext()) {
	    Node node = (Node)nodeItr.next();
	    CoreFunctionNode cfn = (CoreFunctionNode)nodeWeight(nodeLabel(node));
	    cfn.setVisited(false);
	}
    }

    public void resetVisitedMode() {
	Collection nodeCol = nodes();
	Iterator nodeItr = nodeCol.iterator();
	while(nodeItr.hasNext()) {
	    Node node = (Node)nodeItr.next();
	    CoreFunctionNode cfn = (CoreFunctionNode)nodeWeight(nodeLabel(node));
	    Collection modes = cfn.getModes();
	    Iterator modesIter = modes.iterator();
	    while(modesIter.hasNext()){
		CoreFunctionMode cfm = (CoreFunctionMode)modesIter.next();
		cfm.setVisited(false);
	    }
	    cfn.setVisited(false);
	}
    }




    ///////////////////////////////////////////////////////////////////////
    ////                         protected methods                     ////


    ///////////////////////////////////////////////////////////////////////
    ////                         private variabless                    ////
    private PCFDFAttributeType _type = null;
    private int _max = 0;
    private LinkedHashMap modes = new LinkedHashMap();

}


