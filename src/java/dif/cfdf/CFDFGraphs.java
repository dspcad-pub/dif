/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Utilities for working with parameterized synchronous dataflow graphs. */

package dif.cfdf;

import dif.util.graph.Edge;
import dif.util.graph.Graph;
import dif.util.graph.Node;
import dif.data.ExtendedMath;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


//////////////////////////////////////////////////////////////////////////
//// CFDFGraphs
/**
Utilities for working with parametereized synchronous dataflow graphs.

@author Ming-Yung Ko, Shuvra S. Bhattacharyya
@version $Id: CFDFGraphs.java 416 2007-05-31 05:19:54Z plishker $
@see Graph
@see mapss.dif.cfdf.CFDFGraph
*/
public class CFDFGraphs {

    // Private constructor to prevent instantiation of the class
    private CFDFGraphs() {
    }

    /** Given a collection of nodes in a CFDF graph, replace the subgraph
     *  induced by the nodes with a single node N. Besides topological
     *  clustering, this method also adjusts the token exchanging rates
     *  on affected edges and computes repetitions of the newly
     *  created super node. NOTE: presently it is assumed that the node
     *  collection contains exactly two nodes. The crossingEdge argument
     *  is an edge that is directed between these two nodes. It is used
     *  to determine repetition counts.
     *
     *  @param graph The CFDF graph.
     *  @param nodeCollection The collection of nodes.
     *  @param superNode The CFDF node that replaces the subgraph.
     *  @param crossingEdge An edge directed between the nodes to be clustered.
     *  @return The subgraph that is replaced.
     */
    public CFDFGraph clusterNodes(CFDFGraph graph,
            Collection nodeCollection, Node superNode, Edge crossingEdge) {

        List result = clusterNodesComplete(graph, nodeCollection, superNode,
                crossingEdge);
        return (CFDFGraph)result.get(0);
    }

    /** Given a collection of nodes in a CFDF graph, replace the subgraph
     *  induced by the nodes with a single node N. This method returns
     *  both the subgraph and a map from newly created edges to old edges
     *  replaced. The map is important for cluster status tracking.
     *
     *  @param graph The CFDF graph.
     *  @param nodeCollection The collection of nodes.
     *  @param superNode The CFDF node that replaces the subgraph.
     *  @return A list with the first element the subgraph and the second
     *          element a map of edges.
     * SLIN: due to missing CoreFunctionEdge.getProductRateExpression,
     * this method is commented out. 10/15/2015
     */
    public static List clusterNodesComplete(
            CFDFGraph graph, Collection nodeCollection, Node superNode,
            Edge crossingEdge) {
/* SLIN: comment begins. 
        // Collect the crossing edges between the subgraph and the
        // remaining part of the graph.

        if (_debugFlag) {
            System.out.println("In CFDFGraphs.clusterNodesComplete");
        }

        // Assemble the list of edges that are external to the supernode.
        CFDFGraph subGraph = (CFDFGraph)graph.subgraph(nodeCollection);
        ArrayList oldEdgeList = new ArrayList();
        Iterator nodes = nodeCollection.iterator();
        while (nodes.hasNext()) {
            Node node = (Node)nodes.next();
            if (!node.hasWeight()) {
                throw new RuntimeException("CFDF Node expected; unweighted "
                        + "node received");
            }
            
            Iterator edges = graph.incidentEdges(node).iterator();
            while (edges.hasNext()) {
                Edge edge = (Edge)edges.next();
                if (!subGraph.containsEdge(edge))
                    oldEdgeList.add(edge);
            }
        }

        // Add the super node to the clustered graph.
        graph.addNode(superNode);

        // Add the new edges in the clustered graph.
        if (_debugFlag) {
            System.out.println("Adding new edges in clustered graph"); 
        }
        ArrayList newEdgeList = new ArrayList();
        Iterator oldEdges = oldEdgeList.iterator();

        CoreFunctionEdge crossingWeight = 
                (CoreFunctionEdge)(crossingEdge.getWeight());
        String gcdString = gcdExpression(
                crossingWeight.getProductionRateExpression(),
                crossingWeight.getConsumptionRateExpression());

        while (oldEdges.hasNext()) {
            Edge oldEdge = (Edge)oldEdges.next();
            Node source = oldEdge.source();
            Node sink = oldEdge.sink();
 
            CoreFunctionEdge oldWeight = (CoreFunctionEdge)oldEdge.getWeight();
            CoreFunctionEdge newWeight = new CoreFunctionEdge();

            // Port mappings refer back to the original model so that
            // we can trace relations back.
            newWeight.setSourcePort(oldWeight.getSourcePort());
            newWeight.setSinkPort(oldWeight.getSinkPort());

            String repetitionsNumerator;

            // The super node is the source of the new edge.
            if (nodeCollection.contains(source)) {
                if (crossingEdge.source() == source) {
                    repetitionsNumerator = 
                            crossingWeight.getConsumptionRateExpression();
                } else if (crossingEdge.sink() == source) {
                    repetitionsNumerator = 
                            crossingWeight.getProductionRateExpression();
                } else {
                    throw new RuntimeException("Internal error: invalid "
                            + "edge incident to super node. A dump of the "
                            + "edge follows.\n" + oldEdge.toString());
                }
                String productionRateExpression = 
                        "((" 
                        + repetitionsNumerator
                        + ") * ("
                        + oldWeight.getProductionRateExpression()
                        + ")) / "
                        + gcdString;
                newWeight.setProductionRateExpression(productionRateExpression);
                newWeight.setConsumptionRateExpression(
                        oldWeight.getConsumptionRateExpression());
                newEdgeList.add(new Edge(superNode, sink, newWeight));

            // The super node is the sink of the new edge
            } else if (nodeCollection.contains(sink)) {
                if (crossingEdge.source() == sink) {
                    repetitionsNumerator = 
                            crossingWeight.getConsumptionRateExpression();
                } else if (crossingEdge.sink() == sink) {
                    repetitionsNumerator = 
                            crossingWeight.getProductionRateExpression();
                } else {
                    throw new RuntimeException("Internal error: invalid "
                            + "edge incident to super node. A dump of the "
                            + "edge follows.\n" + oldEdge.toString());
                }
                String consumptionRateExpression =
                        "(("
                        + repetitionsNumerator
                        + ") * ("
                        + oldWeight.getConsumptionRateExpression()
                        + ")) / "
                        + gcdString;
                newWeight.setConsumptionRateExpression(
                        consumptionRateExpression); 
                newWeight.setProductionRateExpression(
                        oldWeight.getProductionRateExpression());
                newEdgeList.add(new Edge(source, superNode, newWeight));
            } else {
                // This should not happen
                throw new RuntimeException("Internal error: invalid clustered "
                        + "edge");
            }
        } 
     
        if (_debugFlag) { 
            System.out.println("Removing old edges in clustered graph"); 
        }
        // Add new edges, remove old edges, and put the correspondence
        // of old and new edges in the edge map.
        Map edgeMap = new HashMap();
        for (int i = 0; i < oldEdgeList.size(); i++) {
            Edge oldEdge = (Edge)oldEdgeList.get(i);
            Edge newEdge = (Edge)newEdgeList.get(i);
            graph.addEdge(newEdge);
            graph.removeEdge(oldEdge);
            edgeMap.put(newEdge, oldEdge);
        }

        if (_debugFlag) {
            System.out.println("Removing old nodes in clustered graph"); 
        }
        // remove old nodes
        nodes = nodeCollection.iterator();
        while (nodes.hasNext()) {
            graph.removeNode((Node)nodes.next());
        }
*/
        // Assemble the result list.
        List result = new ArrayList();
        // SLIN : result.add(subGraph);
        // SLIN : result.add(edgeMap);
        if (_debugFlag) {
            System.out.println("Exiting CFDFGraphs.clusterNodesComplete");
        }
        return result;
    }

    /* Given two expressions, return an expression that is equivalent
     * to the gcd of the two expressions. This method attempts to
     * simplify the gcd expression. It is assumed that if one of the
     * argument expressions starts with an integer, then it contains
     * only that integer. This method uses a few very simple heuristics to
     * simplify gcd expressions.
     * @param expression1 The first expression.
     * @param expression2 The second expression.
     * @return The gcd.
     */
    public static String gcdExpression(String expression1, String expression2) {
        // If both expressions are numeric, then compute and return
        // their gcd.
        if ((expression1.length() == 0) || (expression2.length() == 0)) {
            // FIXME: Need better exception handling.
            throw new RuntimeException("Empty argument in gcd expression.");
        }

        int integer1=0, integer2=0;
        boolean firstIsInteger = false, secondIsInteger = false;
        if (Character.isDigit(expression1.charAt(0))) {
            try {
                integer1 = Integer.parseInt(expression1);
            } catch (NumberFormatException exception) {
                System.out.println("Error converting numeric string: invalid "
                        + "trailing characters.\n The string is " + expression1
                        + "\n");
            }
            firstIsInteger = true;
        }
        if (Character.isDigit(expression2.charAt(0))) {
            try {
                integer2 = Integer.parseInt(expression2);
            } catch (NumberFormatException exception) {
                System.out.println("Error converting numeric string: invalid "
                        + "trailing characters.\n The string is " + expression2
                        + "\n");
            }
            secondIsInteger = true;
        }
        if (firstIsInteger && secondIsInteger) {
            int gcd = ExtendedMath.gcd(integer1, integer2);
            return Integer.toString(gcd);
        }
        if (firstIsInteger && (integer1 == 1)) {
            return "1";
        }
        if (secondIsInteger && (integer2 == 1)) {
            return "1";
        }
        return ("gcd(" + expression1 + ", " + expression2 + ")");
    }

    ///////////////////////////////////////////////////////////////////
    ////                      private variables                    ////

    // Flag for turning on debugging output.
    private static boolean _debugFlag = false;

}


