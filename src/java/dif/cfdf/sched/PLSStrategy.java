/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* Acyclic Pair-wise Group of Adjacent Nodes (APGAN) scheduler. */

package dif.cfdf.sched;

import java.util.ArrayList;

import dif.util.graph.Node;
import dif.util.sched.ScheduleTree;

import dif.DIFScheduleStrategy;

import dif.cfdf.CFDFGraph;

//////////////////////////////////////////////////////////////////////////
//// PLSStrategy
/* PLSStrategy extends the more abstract DIFScheduleStrategy class.
 * It provides functionality to produce a generalized schedule tree
 * from a dynamic application described in Core Functional Dataflow
 * (CFDF) for certain class of applications.
 *
 * @author Nimish Sane
 */

public class PLSStrategy extends DIFScheduleStrategy {

    /** Constructor. The graph input is the CFDF Graph to be
     * scheduled.
     *
     *  @param graph The CFDF graph to be scheduled.
     */
    public PLSStrategy(CFDFGraph graph) {
        super(graph);
	_verbose_level = 0;
    }

    ///////////////////////////////////////////////////////////////////
    ////                        public methods                     ////


    /** Run generalized scheduling strategy and then expand it to a schedule.
     *
     *  This top level method first builds the mode graphs using the
     *  iterative method buildModeGraphsIterative().  The resulting
     *  set of mode graphs are then scheduled using one of our
     *  existing SDF Scheduling algorithms.  The resulting set of
     *  trees are merged so that they may be used by our existing
     *  runtime system which accepts a single graph and a single
     *  schedule tree to run.  Variables set within this method may be
     *  used to manually tune the order and balance of the tree
     *  merging process.
     *
     *  @return A single ScheduleTree in which all of the mode graph
     *  SDF schedule trees are brought under one root node.
     *  SLIN07: due to lack of ScheduleTree.getRoot(), this class
     *  is not compilable.
     */

    public ScheduleTree schedule( ) {
	/*	CFDFGraph g = (CFDFGraph)graph();
	Node map = g.getNode("mapper");
	Node demap = g.getNode("demapper");
	Node snk = g.getNode("sink");
	Node bits = g.getNode("bits");
	Node channel = g.getNode("channel");
	ScheduleTree st = new ScheduleTree();

	Node root = new Node();
	Node mapper1 = new Node();
	Node m = new Node();
	Node src = new Node();
	Node mapper2 = new Node();
	Node chnl = new Node();
	Node loop = new Node();
	Node demapper = new Node();
	Node n = new Node();
	Node sink = new Node();

	st.addNode(root);
	st.addNode(mapper1);
	st.addNode(m);
	st.addNode(src);
	st.addNode(mapper2);
	st.addNode(chnl);
	st.addNode(loop);
	st.addNode(demapper);
	st.addNode(n);
	st.addNode(sink);

	st.addEdge(root, mapper1);
	st.addEdge(root, m);
	st.addEdge(m, src);
	st.addEdge(root, mapper2);
	st.addEdge(root, chnl);
	st.addEdge(root, loop);
	st.addEdge(loop, demapper);
	st.addEdge(root, n);
	st.addEdge(n, sink);

	st.setNodeInfoMap();

	st.setActor(mapper1, map);
	st.setActor(mapper2, map);
	st.setActor(src, bits);
	st.setActor(demapper, demap);
	st.setActor(chnl, channel);
	st.setActor(sink, snk);

	st.setLoopCount(root, 1);
	st.setLoopCount(m, 1);
	st.setLoopCount(loop, 2);
	st.setLoopCount(n, 1);

	st.buildRootedTree(st, root);
	st._initializeTreeWalker();


	// // int count = 0;
	// // Collection nodeCol = _graph.nodes();
	// // Iterator nodeItr = nodeCol.iterator();
	// st = null;

	// st = new ScheduleTree(map);
	// st.addScheduleElement(bits, 1);
	// st.addScheduleElement(map);
	// st.addScheduleElement(channel);
	// st.addScheduleElement(demap, 2);
	// st.addScheduleElement(snk, 1);

	return st;
	} */

    	CFDFGraph graph = (CFDFGraph)graph();
	Node map = graph.getNode("mapper");
	Node demap = graph.getNode("demapper");
	Node snk = graph.getNode("sink");
	Node bits = graph.getNode("bits");
	Node channel = graph.getNode("channel");

    	print(" %%%%%%%%%%  Original Graph %%%%%%%%%%%%% ", 1);
    	print(graph.toString(), 1);

    	// ScheduleTree st = new ScheduleTree();

    	// ScheduleTree stMapper = new ScheduleTree(graph.getNode("mapper"), 1);
    	// st.addScheduleElement(stMapper);

    	// ScheduleTree stBits = new ScheduleTree(graph.getNode("bits"), 1);
    	// st.addScheduleElement(stBits);

    	// ScheduleTree stMapper1 = new ScheduleTree(graph.getNode("mapper"), 1);
    	// st.addScheduleElement(stMapper1);

    	// ScheduleTree stChannel = new ScheduleTree(graph.getNode("channel"), 1);
    	// st.addScheduleElement(stChannel);

    	// ScheduleTree stDemapper = new ScheduleTree(graph.getNode("demapper"), 2);
    	// st.addScheduleElement(stDemapper);

    	// ScheduleTree stSink = new ScheduleTree(graph.getNode("sink"), 1);
    	// st.addScheduleElement(stSink);
    	ScheduleTree st = new ScheduleTree();
	st = null;

/* SLIN: This section is commented out due to missing ScheduleTree.getRoot() method.
	//    	st.addScheduleElement(map);
	st = new ScheduleTree(map);

    	ScheduleTree stBits = new ScheduleTree(bits, 1);
	_plcNodes.add((ScheduleTreeNode)stBits.getRoot());
    	st.addScheduleElement(stBits);

	//    	ScheduleTree stMapper1 = new ScheduleTree(map);
    	st.addScheduleElement(map);

	//    	ScheduleTree stChannel = new ScheduleTree(channel, 1);
    	st.addScheduleElement(channel);

    	ScheduleTree stDemapper = new ScheduleTree(demap, 2);
    	st.addScheduleElement(stDemapper);

    	ScheduleTree stSink = new ScheduleTree(snk, 1);
	_plcNodes.add((ScheduleTreeNode)stSink.getRoot());
    	st.addScheduleElement(stSink);

	//    	System.out.println(st.printTree());
*/
    	return st;
    }

    public ArrayList plcNodes( ) {
	return _plcNodes;
    }

    ///////////////////////////////////////////////////////////////////
    ////                       protected methods                   ////

    ///////////////////////////////////////////////////////////////////
    ////                      private methods                      ////


    /**  Prints debug output to the screen based on the verbosity level set.
     *
     *  At the moment, this is for debug purposes to produces traces
     *  of how the graph is traversed and mode graphs are found.  The
     *  verbosity level is set in the constructor.
     *  @param str The string to be printed
     *  @param level The verbosity level of the current statement to be printed
     */
    public static void print(String str, int level) {
    	if(level<=_verbose_level) {
    	    System.out.println(str);
    	}
    }

    ///////////////////////////////////////////////////////////////////
    ////                     private variables                     ////

    // The verbosity level
    static private int _verbose_level;

    // The parameterized loop counts
    private ArrayList _plcNodes = new ArrayList();
}



