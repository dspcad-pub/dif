/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif.cfdf.sched;

import java.lang.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import dif.util.graph.Node;
import dif.util.sched.ScheduleTree;

import dif.CoreFunctionNode;
import dif.DIFScheduleStrategy;
import dif.cfdf.CFDFGraph;

/*********************************************************************/
/** ForwardPartitioner extends the more abstract DIFScheduleStrategy
 *  class.  It provides functionality to produce a generalized schedule
 *  tree from a dynamic application described in Core Functional
 *  Dataflow (CFDF).
 *
 *  @author Kishan Sudusinghe, William Plishker
 */

public class ForwardPartitioner extends DIFScheduleStrategy {

    /** Constructor. The graph input is the CFDF Graph to be
     * scheduled.
     *
     *  @param graph The CFDF graph to be scheduled.
     */
    public ForwardPartitioner(CFDFGraph graph) {
        super(graph);
	_verbose_level = 2;
    }

    /***********************************************************************/
    /***                        public methods                           ***/
    /***********************************************************************/

    /** Run generalized scheduling strategy and then expand it to a schedule.
     *
     *
     *  @return A single ScheduleTree in which all of the mode graph
     *  SDF schedule trees are brought under one root node.
     */
    public ScheduleTree schedule() {
	CFDFGraph graph = (CFDFGraph)graph();
	print(" %%%%%%%%%%  Original Graph %%%%%%%%%%%%% ", 1);
	print(graph.toString(), 1);
	//DIFdoc gDoc = new DIFdoc(graph);
       	//gDoc.toFile("index_app");
	BuildPartitions(graph);
	/// Print out the combined schedule tree
	//graph.setName(graph.getName()+"_sched");
	//SchedDoc sDoc = new SchedDoc(ST, graph);
	//sDoc.toFile("index_sched");

      	//return ST;

	return null;
    }

    /*************************************************************************/
    /***                       protected methods                           ***/
    /*************************************************************************/
    /*************************************************************************/
    /***                       private methods                             ***/
    /*************************************************************************/

    /**  Prints debug output to the screen based on the verbosity level
     *  At the moment, this is for debug purposes to produces traces
     *  of how the graph is traversed and mode graphs are found.  The
     *  verbosity level is set in the constructor.
     *  @param str The string to be printed
     *  @param level The verbosity level of the current statement to be printed
     */
    public static void print(String str, int level) {
	if(level<=_verbose_level) {
	    System.out.println(str);
	}
    }

    /**
     * Builds partitions by moving forward through the graph
     */
    public static LinkedList BuildPartitions(CFDFGraph g) {
       	Collection nodes = g.nodes();
	HashMap included = new HashMap();
	Collection H = new ArrayList();
 	Iterator nodesIter = nodes.iterator();

	//first iterate over all nodes in the graph
	while(nodesIter.hasNext()){
	    //First find the source modes.  Actors may have a both
	    //source modes and non-source modes, so actors may be both
	    //source and non-source in different mode graphs.
	    Node node = (Node)nodesIter.next();
	    CoreFunctionNode cfn = (CoreFunctionNode)node.getWeight();
	    print("Node = " + cfn.toString(), 2);
	    //Collections modes = (Collections)cfn.getModes();
	    //System.out.println("Node = " + node.toString());
	    int edgeCount = g.inputEdgeCount(node);

	    if (edgeCount == 0) {
		H.add(node);
	    }
	    // v is the current node and need to set its flag to false through an iteration
	    included.put(node, false);
	}
	System.out.println("Included = " + included.toString());

	Collection RP = new ArrayList();
	Collection pred_v = new ArrayList();
	//(ArrayList)node.predecessors()
	// Have an iterator going through nodes and pull out inputEdgeCount and append it to H

	System.out.println("H = " + H.toString());

	while (H.size() > 0){
	    ArrayList R = new ArrayList();
	    Boolean R_change = true;

	    Node h = (Node)H.toArray()[0];
	    R.add(h);
	    H.remove(h);
	    included.put(h, true);
	    int length_R = R.size();
	    length_R = 1;
	    System.out.println("H_1 = " + H.toString());
	    System.out.println("Included_H = " + included.toString());

	    /* Looping untill we add a node to current Region. Break out when no node
	       is added during an iteration or region size has been satisfied*/
	    while (R_change) {
		R_change = false;
		nodesIter = nodes.iterator();

		while (nodesIter.hasNext()) {
		    Node node = (Node)nodesIter.next();
		    pred_v = g.predecessors(node);
		    Collection pred_v_clone = new ArrayList (pred_v);
		    Iterator nodesIter1 = pred_v_clone.iterator();
		    
		    /* Removing any nodes in predecessor list and also in current region */
		    while (nodesIter1.hasNext()) {
			Node pred_v_node = (Node)nodesIter1.next();

			if ((Boolean)included.get(pred_v_node) == true) {
			    pred_v.remove(pred_v_node);
			    //System.out.println("ClonedPredV = " + pred_v_node.toString());
			}
		    }
		    //System.out.println("Node = " + node.toString());
		    //System.out.println("PredV = " + pred_v.toString());
		    Boolean isSubset = R.containsAll(pred_v);

		    if (pred_v.size() == 0) {
			isSubset = true;
		    }
		    //System.out.println("ISSubset = " + isSubset.toString());

		    if (((Boolean)included.get(node) == false) &&  (isSubset == true) &&
		               ((length_R + 1) <= 3)) {
			R.add(node);
			R_change = true;
			included.put(node, new Boolean("true"));
			length_R = length_R + 1;
			//System.out.println("R_included = " + included.toString());
		    }
		}
	    }
	    System.out.println("R = " + R.toString());
	    RP.add(R);
	    //System.out.println("RP = " + RP.toString());
	    Iterator nodesIter1 = nodes.iterator();

	    while (nodesIter1.hasNext()) {
		Node node = (Node)nodesIter1.next();
		/* Generate predecessors here again and any node in pred.v is in R condition satisfy. 
		   NOT all nodes have to be in there. */
		pred_v = g.predecessors(node);
		Iterator nodesIter2 = pred_v.iterator();
		Boolean intersection = false;

		while (nodesIter2.hasNext()) {
		    intersection = R.contains(nodesIter2.next());
		    if (intersection) {
			break;
		    }
		}

		    if ((intersection == true) && ((Boolean)included.get(node) == false)) {
		    H.add(node);
		}
	    }
	    System.out.println("H_lastadd = " + H.toString());

	    Boolean contains_in_RP = false;
	    Collection H_clone = new ArrayList(H);
	    Iterator nodesIter3 = H_clone.iterator();

	    while (nodesIter3.hasNext()) {
		Node node_H = (Node) nodesIter3.next();
		System.out.println("HeadNode = " + node_H.toString());
		pred_v = g.predecessors(node_H);
		System.out.println("Predecessors_of_Head = " + pred_v.toString());
		Iterator nodesIter2 = pred_v.iterator();

		while (nodesIter2.hasNext()) {
		    Node H_clone_node = (Node) nodesIter2.next();
		    System.out.println("PREDECESSOR = " + H_clone_node.toString());

		    Boolean everContains = false;
		    Iterator nodesIter4 = RP.iterator();

		    while (nodesIter4.hasNext()) {
			ArrayList setOfCollection = (ArrayList) nodesIter4.next();
			contains_in_RP = setOfCollection.contains(H_clone_node);

			if (contains_in_RP) {
			    everContains = true;
			}
		    }

		    if (everContains == false) {
			H.remove(node_H);
			System.out.println("STATUS = " + H.toString());
		    }
		}

	    }
	    Iterator rIter = R.iterator();
	    Boolean containsInR = false;

	    while (rIter.hasNext()) {
		Node currentNode = (Node)rIter.next();
		containsInR = H.contains(currentNode);

		if (containsInR) {
		    H.remove(currentNode);
		}
	    }
	    System.out.println("H_endofloop = " + H.toString());

	}

	// Print the RP here for testing purpose
	System.out.println("RP = " + RP.toString());
	return null;
	}

    /**********************************************************************/
    /***                     private variables                          ***/
    /**********************************************************************/

    // The verbosity level
    static private int _verbose_level;
}


