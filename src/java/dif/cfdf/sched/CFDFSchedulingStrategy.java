/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

/* CFDF Scheduling strategies based on plis209x1 and plis2009x2. */

package dif.cfdf.sched;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

import dif.CoreFunctionNode;
import dif.DIFGraph;
import dif.util.graph.Edge;
import dif.util.graph.Node;
import dif.util.sched.ScheduleTree;

import dif.CoreFunctionMode;

import dif.attributes.CFDFAttributeType;
import dif.DIFScheduleStrategy;
import dif.csdf.sdf.SDFGraph;
import dif.csdf.sdf.SDFEdgeWeight;

import dif.csdf.sdf.sched.FlatStrategy;

import dif.cfdf.CFDFGraph;

//////////////////////////////////////////////////////////////////////////
//// CFDFSchedulingStrategy

/** <p>CFDFSchedulingStrategy extends the more abstract
 * {@link DIFScheduleStrategy} class.  It provides
 * functionality to produce a generalized schedule tree from a dynamic
 * application described in Core Functional Dataflow (CFDF).
 *
 * Based on {@link GeneralizedStrategy}
 *
 * This is different from {@link GeneralizedStrategy}
 *
 * in that it searches the graph exhaustively to generate all possible
 * SDF graphs. The edges with either production or consumption rate
 * equal to zero in a given mode are removed from the SDF
 * graphs. Hence, some of the SDF graphs may be disconnected. For such
 * graphs, their each of the connected components is scheduled
 * separately.
 *
 * One change from {@link GeneralizedStrategy} is
 * the use of {@link DIFGraph.getEdge(String)} method
 * instead of {@link CoreFunctionNode.getEdge(String)}
 *
 * @author Nimish Sane, William Plishker
 */

public class CFDFSchedulingStrategy extends DIFScheduleStrategy {

    /** Constructor. The graph input is the CFDF Graph to be
     * scheduled.
     *
     *  @param graph The CFDF graph to be scheduled.
     *  @param modeGrouping Set this to true if mode grouping is to be
     *  considered; false, otherwise.
     */
    public CFDFSchedulingStrategy(CFDFGraph graph, boolean modeGrouping) {
        super(graph);
	_verbose_level = 2;
	_modeGrouping = modeGrouping;
    }

    ///////////////////////////////////////////////////////////////////
    ////                        public methods                     ////


    /** Run generalized scheduling strategy and then expand it to a schedule.
     *
     *  This top level method first builds the mode graphs using the
     *  iterative method buildModeGraphsIterative().  The resulting
     *  set of mode graphs are then scheduled using one of our
     *  existing SDF Scheduling algorithms.  The resulting set of
     *  trees are merged so that they may be used by our existing
     *  runtime system which accepts a single graph and a single
     *  schedule tree to run.  Variables set within this method may be
     *  used to manually tune the order and balance of the tree
     *  merging process.
     *
     *  @return A single ScheduleTree in which all of the mode graph
     *  SDF schedule trees are brought under one root node.
     */
    public ScheduleTree schedule() {
	CFDFGraph graph = (CFDFGraph)graph();

	print(" %%%%%%%%%%  Original Graph %%%%%%%%%%%%% ", 1);
	print(graph.toString(), 1);

	/* Build the mode graphs */
	LinkedList modeGraphs = new LinkedList();
	modeGraphs = buildModeGraphIterative(graph, _modeGrouping);

	/* Now step through each mode graph */
	ScheduleTree schedTree = null;
	Boolean once = true;//false; //turn this on for graph specific functionality

	//Schedule Tree Indices - for schedule tree balancing of the forest
	int[] STI = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 };
	//for schedule tree balancing of the forest
	int[] STICounts = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

	print(" %%%%%%%%%%  Mode graphs (" + modeGraphs.size() + ") %%%%%%%%%%%%% ", 1);
	int count=0;
	for(Iterator iter = modeGraphs.iterator(); iter.hasNext();) {

	    SDFGraph sg = (SDFGraph)iter.next();

	    sg.setName(sg.getName() + "_" + count);
	    print(sg.getName() + ": " + sg.toString(), 2);

	    DIFGraph dg = (DIFGraph)graph.subgraph(sg.nodes()); //WLP - really should account for edges too...
	    dg.setName(dg.getName() + "_" + count);

	    HashMap graphHM = sg.computeRepetitions();
	    Set hmSet = graphHM.keySet();
	    Iterator hmIter = hmSet.iterator();

	    print("     %%%%%% Mode Graph #" + count + " %%%%%%",1);
	    print(sg.toString(),1);

	    print("Repition Vector for this graph", 1);
	    while (hmIter.hasNext()) {
		Node node = (Node)hmIter.next();
		print(sg.getRepetitions(node) + "\t" +
		      sg.getName(node),1);
	    }

	    // Schedule the mode graph
	    //APGANStrategy sched = new APGANStrategy((SDFGraph)sg);
	    FlatStrategy sched = new FlatStrategy((SDFGraph)sg);
	    print("Creating SDF Schedule",1);
	    ScheduleTree subSchedTree = sched.schedule();
	    print("Done with schedule",1);

	    // Add the resulting schedule tree to the larger schedule tree
	    // Use the schedTree iteration counts to combine the trees
	    if (schedTree == null) {
		if (STICounts[count] > 0) {
		    ScheduleTree stemp = new ScheduleTree(subSchedTree, STICounts[count]);
		    schedTree = new ScheduleTree(stemp, 1);
		}
	    } else {
		if (STICounts[count] > 0) {
		    schedTree.addScheduleElement(subSchedTree, STICounts[count]);
		}
	    }
	    count++;
	}

	graph.setName(graph.getName() + "_sched");

	return schedTree;
    }

    ///////////////////////////////////////////////////////////////////
    ////                      public methods                      ////


    /**  Prints debug output to the screen based on the verbosity level set.
     *
     *  At the moment, this is for debug purposes to produces traces
     *  of how the graph is traversed and mode graphs are found.  The
     *  verbosity level is set in the constructor.
     *  @param str The string to be printed
     *  @param level The verbosity level of the current statement to be printed
     */
    public static void print(String str, int level) {
	if (level <= _verbose_level) {
	    System.out.println(str);
	}
    }

    /**  Constructs a set of mode graphs from a CFDFGraph.
     *
     *  This simply calls another version buildModeGraphIterative so
     *  as to not consider mode groupings (the original behavior of
     *  the scheduler).
     *
     *  @param g The CFDF graph from which the mode graphs are constructed
     *  @returns A Linked List SDFGraphs which are the resulting mode graphs
     */

    public static LinkedList buildModeGraphIterative (CFDFGraph g) {
	return buildModeGraphIterative(g, false);
    }

    /**  Constructs a set of mode graphs from a CFDFGraph.
     *
     *  This method does the heavy lifting described by this work.
     *  Refer to plis2009x1 and plis2009x2 on the www.umd.edu/DSPCAD
     *  website for full details.
     *
     *  @param g The CFDF graph from which the mode graphs are constructed
     *  @param considerGroupModes When true, the algorithms considers mode grouping.
     *  @returns A Linked List SDFGraphs which are the resulting mode graphs
     */
    public static LinkedList buildModeGraphIterative (CFDFGraph g, boolean considerGroupModes) {

	LinkedList modeGraphs = new LinkedList();
	Collection nodes = g.nodes();

	//first iterate over all nodes in the graph
	for(Iterator nodesIter = nodes.iterator(); nodesIter.hasNext();){
	    //First find the source modes.  Actors may have a both
	    //source modes and non-source modes, so actors may be both
	    //source and non-source in different mode graphs.
	    Node node = (Node)nodesIter.next();
	    CoreFunctionNode cfn = (CoreFunctionNode)node.getWeight();
	    print("Node = " + cfn.toString(), 2);
	    Collection modes = cfn.getModes();

	    for (Iterator modesIter = modes.iterator();modesIter.hasNext();){
		boolean hasNoInput = true;
		CoreFunctionMode cfm = (CoreFunctionMode)modesIter.next();
		if((cfm.getType() != CFDFAttributeType.InitMode) &&
		   (cfm.getType() != CFDFAttributeType.NullMode)) {
		    //look for a source _modes_ (i.e. not nodes)

		    print("src mode?= " + cfm.getName(),2);
		    Set inputs = cfm.getInputs();

		    // if (inputs == null | inputs.isEmpty()) {
		    // 	print("***** No inputs specified. *****", 1);
		    // }

		    for(Iterator inputsiter = inputs.iterator(); inputsiter.hasNext();) {
			String inputName = (String)inputsiter.next();
			// System.out.println("    input  = " + inputName + ": " + cfm.getConsumption(inputName) + " <- " + g.getName(cfn.getEdge(inputName).source()));
			System.out.println("    input  = " + inputName + ": " +
					   cfm.getConsumption(inputName) + " <- " +
					   g.getEdge(inputName).source());
			if(cfm.getConsumption(inputName)>0) {
			    hasNoInput = false;
			}
		    }

		    //if we detect that the mode for this actor has no input, it becomes a source
		    if(hasNoInput) {
			//Use a working queue to keep track of nodes to be processed
			LinkedList nodeWorkStack = new LinkedList();
			LinkedList edgeWorkStack = new LinkedList();

			LinkedList committedNodes = new LinkedList();
			LinkedList committedEdges = new LinkedList();

			//this is a brand new search through the graph, so reset everything
			g.resetVisitedMode();
			g.resetVisited();

			committedNodes.addLast(node);
			committedEdges.addLast(null);
			//be sure the source node is locked into a particular mode
			{
			    CoreFunctionNode cfnn = (CoreFunctionNode)node.getWeight();
			    cfnn.setAllVisitedExcept(cfm);
			}

			while(committedNodes.peek()!=null) {

			    print("committed = " + committedNodes.toString(),2);
			    print("visited = ", 2);
			    for(Iterator nnIter = g.nodes().iterator(); nnIter.hasNext();){
				Node nn = (Node)nnIter.next();
				CoreFunctionNode cfnn = (CoreFunctionNode)nn.getWeight();
				if(cfnn.visited()){
				    print("   " + cfnn.getName(),2);
				}
			    }

			    nodeWorkStack.addLast(committedNodes.removeFirst());
			    edgeWorkStack.addLast(committedEdges.removeFirst());

			    LinkedList modeGraphWork = new LinkedList(committedNodes);

			    //There must be nodes to work through, so
			    //it is up to them to make the graph
			    //"valid" (i.e. the current modes for the nodes
			    //added have all inputs and outputs satisfied)
			    boolean validGraph = false;


			    //this prompts the creation of a new list of mode
			    //graphs to be produced, which are fundamentally
			    //just collections to start

			    //now go through the working
			    while(nodeWorkStack.peek()!=null) {
				Node nodeWork = (Node)nodeWorkStack.removeFirst();
				Edge edgeWork = (Edge)edgeWorkStack.removeFirst();
				CoreFunctionNode cfnWork = (CoreFunctionNode)nodeWork.getWeight();
				print("====== Node = " + cfnWork.toString() + " =======",2);

				//First see if this node still has any
				//business being in the working stack
				//by ensuring that the ingressing edge
				//still has some connection to the
				//committed nodes. This works because
				//a valid node is immediately queued
				//on the committed queue

				boolean inCommitted = false;
				String name = new String("NO VALID EDGE");
				if(edgeWork != null /*&& !modeGraphWork.isEmpty()*/) {
				    name = cfnWork.getActorPortName(edgeWork);
				    CoreFunctionMode cfmWork = cfnWork.getVisitedMode();

				    print("edgeWork:"+edgeWork.toString(),2);
				    print("cfnWork:"+cfnWork.toString(),2);
				    print("modeGraph:"+modeGraphWork.toString(),2);


				    for(Iterator edgeCheckIter = modeGraphWork.iterator(); edgeCheckIter.hasNext();){
					Node ecnn = (Node)edgeCheckIter.next();
					CoreFunctionNode eccfn = (CoreFunctionNode)ecnn.getWeight();
					CoreFunctionMode eccfm = eccfn.getVisitedMode();
					String ecname = eccfn.getActorPortName(edgeWork);
					print("EdgeCheck: Node " + eccfn.toString() +
					      "(" + eccfm.getName() +
					      ") is being checked for edge "
					      + ecname + ".", 2);

					if((eccfm.getConsumption(ecname)>0) || (eccfm.getProduction(ecname)>0)) {
					    print(ecname + " is still in committed graph at " + eccfn.getName(),2);
					    inCommitted = true;
					    break;
					}
				    }
				} else {
				    //source nodes get a free pass to
				    //be visited (i.e. they don't need
				    //a valid ingressing or egressing
				    //edge to or from the committed
				    //list)
				    inCommitted = true;
				}

				//check if we've already been here
				if(cfnWork.visited()) {
				    print("... previously visited", 2);
				    //The current mode must have a way of satisfying the
				    //upstream (or downstream) mode that directed us here.
				    if(edgeWork != null) {
					CoreFunctionMode cfmWork = cfnWork.getVisitedMode();

					if(!inCommitted) {
					    print("Node " + cfnWork.toString() +
						  "(" + cfnWork.getVisitedMode().getName() +
						  ") is getting a pass because edge "
						  + name + " is no longer in the working set", 2);
					}

					if(inCommitted && (cfmWork.getConsumption(name)<=0) && (cfmWork.getProduction(name)<=0)) {
					    print("ERROR: it appears Node " + cfnWork.toString() +
						  "(" + cfnWork.getVisitedMode().getName() +
						  ") cannot satisfy the mode configuration. (Offending edge = "
						  + name + ")", 2);
					    validGraph = false;
					    break;
					}
				    }

				} else if(inCommitted){ // if we haven't been here before (and the node is worth visiting)
				    print("... is now being visited", 2);
				    //otherwise we've got a new node to process
				    cfnWork.setVisited(true);

				    validGraph = false;

				    //first look for ONE mode that matches the input
				    boolean foundOne=false;
				    Collection modesWork = cfnWork.getModes();
				    print("checkmodes = " + modesWork,2);
				    for (Iterator modesWorkIter = modesWork.iterator();modesWorkIter.hasNext();){
					CoreFunctionMode cfmWork = (CoreFunctionMode)modesWorkIter.next();
					if((cfmWork.getType() == CFDFAttributeType.GroupMode) && !considerGroupModes) {
					    cfmWork.setVisited(true);
					    continue;
					}

					if(edgeWork != null) {
					    print("checkmode = " + cfmWork.getName() + " edge: " +
						  cfnWork.getActorPortName(edgeWork) + " (" +
						  cfmWork.getProduction(cfnWork.getActorPortName(edgeWork)) + "," +
						  cfmWork.getConsumption(cfnWork.getActorPortName(edgeWork)) + ")",2);
					}

					//if we find a matching mode...
					if(!cfmWork.visited() &&  //do not recheck visited modes
					   !foundOne  &&          //and we have not already found a good mode
					   (cfmWork.getType() != CFDFAttributeType.InitMode) &&
					   (cfmWork.getType() != CFDFAttributeType.NullMode) &&
					   ((edgeWork == null) ||  //the source mode won't have an edge
					    (cfmWork.getConsumption(cfnWork.getActorPortName(edgeWork))>0) ||
					    (cfmWork.getProduction(cfnWork.getActorPortName(edgeWork))>0))) {

					    //this is a new node for us
					    cfmWork.setVisited(true);
					    print(cfnWork.toString() + "Visited Mode: " + cfmWork.getName(), 1);
					    cfnWork.setVisitedMode(cfmWork);

					    //based on the previous node/mode, this node
					    //locally makes the graph valid
					    validGraph = true;

					    //then look through the inputs and outputs for more nodes/modes to discover
					    Set inputsWork = cfmWork.getInputs();
					    Set outputsWork = cfmWork.getOutputs();
					    print("Visiting: " + cfnWork.getName() + " mode = " + cfmWork.getName() + ", " + inputsWork.toString() + ", " + outputsWork.toString(),2);
					    for(Iterator inputsiterWork = inputsWork.iterator(); inputsiterWork.hasNext();) {
						String inputName = (String)inputsiterWork.next();
						//					print("        input  = " + inputName + ": " + cfmWork.getConsumption(inputName) + " <- " + g.getName(g.getEdge(inputName).source()),2);
						if(cfmWork.getConsumption(inputName)>0) {
						    // nodeWorkStack.addLast(cfnWork.getEdge(inputName).source());
						    // edgeWorkStack.addLast(cfnWork.getEdge(inputName));
						    nodeWorkStack.addLast(g.getEdge(inputName).source());
						    edgeWorkStack.addLast(g.getEdge(inputName));
						}
					    }

					    for(Iterator outputsiter = outputsWork.iterator(); outputsiter.hasNext();) {
						String outputName = (String)outputsiter.next();
						//						print("    output  = " + outputName + ": " +  cfmWork.getProduction(outputName) +
						//						      " <- " + g.getName(g.getEdge(outputName).sink()),2);
						if(cfmWork.getProduction(outputName)>0) {
						    nodeWorkStack.addLast(g.getEdge(outputName).sink());
						    edgeWorkStack.addLast(g.getEdge(outputName));
						}
					    }

					    //done processing all inputs and outputs if
					    foundOne = true;

					    //we can now officially commit the node
					    committedNodes.addLast(nodeWork);
					    committedEdges.addLast(edgeWork);

					    //now we are ready to add this node to the current graphs under construction
					    modeGraphWork.add(nodeWork);

					    print("Working node stack: " + nodeWorkStack.toString(),2);
					    print("Graph so far: ",2);
					    for(Iterator nnIter = modeGraphWork.iterator(); nnIter.hasNext();){
						Node nn = (Node)nnIter.next();
						CoreFunctionNode cfntemp = (CoreFunctionNode)nn.getWeight();
						print("   " + cfntemp.getName() + ":" + cfntemp.getVisitedMode().getName(),2);
					    }
					    //no need to keep looking at modes for this actor
					    break;

					} else {
					    //it wasn't a mode worth considering
					    //but we still mark it as visited
					    cfmWork.setVisited(true);
					}
				    }
				}
				//we have finished checking all the modes of this node
				// if we haven't found a valid mode here, we need to role back
				if(!validGraph) {
				    print(".... no valid modes",2);

				    //to keep the unwind logic clean,
				    //we push this mode less node back
				    //onto the commit stack. since we
				    //break the loop here, it will
				    //immediately be unwound
				    //(i.e. removeFirstped)

				    committedNodes.addLast(nodeWork);
				    committedEdges.addLast(edgeWork);

				    break;
				}
			    }
			    //now the working stack for the nodes is
			    //done, so the graph is processed.  Now
			    //convert the resulting collection into a
			    //graph

			    print("*************   Done with a graph: **********\n" + modeGraphWork.toString(),2);

			    if(validGraph && modeGraphWork.size()>0) { //if there is something in it
				SDFGraph newGraph = new SDFGraph();

				//first iterate over all nodes in the graph
				for(Iterator nnIter = modeGraphWork.iterator(); nnIter.hasNext();){
				    Node nn = (Node)nnIter.next();
				    CoreFunctionNode cfntemp = (CoreFunctionNode)nn.getWeight();
				    print("   " + cfntemp.getName() + ":" + cfntemp.getVisitedMode().getName(),2);
				    newGraph.addNode(nn);
				}

				//now add the accompanying edges by iterating through nodes again
				for(Iterator nnIter = modeGraphWork.iterator(); nnIter.hasNext();){
				    Node nn = (Node)nnIter.next();
				    print("Weight " + nn.getWeight(),2);
				    CoreFunctionNode cfnn = (CoreFunctionNode)nn.getWeight();
				    CoreFunctionMode cfmm = cfnn.getVisitedMode();

				    print(cfmm + "        mode = " + cfmm.getName() + ", " + cfmm.getInputs() + ", " + cfmm.getOutputs(),2);
				    Set outputs = cfmm.getOutputs();
				    for(Iterator outputsiter = outputs.iterator(); outputsiter.hasNext();) {
					String outputName = (String)outputsiter.next();
					//					print("    output  = " + outputName + ": " +  cfmm.getProduction(outputName) + " <- " + g.getName(g.getEdge(outputName).sink()),2);
					if(cfmm.getProduction(outputName)>0) {
					    Edge origEdge = g.getEdge(outputName);
					    CoreFunctionNode cfnSink = (CoreFunctionNode)origEdge.sink().getWeight();
					    CoreFunctionMode modeTemp = cfnSink.getVisitedMode();
					    String portName = cfnSink.getActorPortName(origEdge);
					    int prod = cfmm.getProduction(outputName);

					    print("    output  = " + portName, 2);
					    print("visited " + modeTemp, 2);
					    // print(": " +  cfnSink.getVisitedMode().getConsumption(cfnSink.getActorPortName(origEdge)),2);

					    if (modeTemp != null) {
						print("Mode = " + modeTemp.toString() + " port: " + portName, 2);
					    } else {
						print("Null mode", 2);
					    }

					    if (modeTemp != null) {
						int cons = modeTemp.getConsumption(portName);
						if(cfmm.getType()!=CFDFAttributeType.GroupMode && considerGroupModes){
						    prod = prod*2;
						}

						if(cfnSink.getVisitedMode().getType()!=CFDFAttributeType.GroupMode && considerGroupModes){
						    cons = cons*2;
						}

						SDFEdgeWeight sdfew = new SDFEdgeWeight(prod, cons, 0);

						Edge tempE = new Edge(origEdge.source(), origEdge.sink(), sdfew);
						newGraph.addEdge(tempE);
					    }
					}
				    }

				}
				//the graph has been built, but we need to make sure it is not already in the set we have
				boolean unique = true;
				for(Iterator iter = modeGraphs.iterator(); iter.hasNext(); ) {
				    SDFGraph sg = (SDFGraph)iter.next();
				    print("Compare:" + sg.toString() + " vs. " + newGraph.toString(),2);
				    // equals doesn't seem to work, doing just a node equivalance instead
				    //if(sg.equals(newGraph)){
				    boolean totalmatch = false;
				    if(sg.nodes().size()==newGraph.nodes().size()) {
					totalmatch = true;
					for(Iterator n1Iter = sg.nodes().iterator(); n1Iter.hasNext();){
					    CoreFunctionNode n1 = (CoreFunctionNode)((Node)n1Iter.next()).getWeight();

					    boolean foundmatch = false;
					    for(Iterator n2Iter = newGraph.nodes().iterator(); n2Iter.hasNext();){
						CoreFunctionNode n2 = (CoreFunctionNode)((Node)n2Iter.next()).getWeight();
						if(n1.getName().equals(n2.getName())) {
						    foundmatch = true;
						}
					    }

					    if(!foundmatch) {
						print("not a total match becase of " + n1.getName(),2);
						totalmatch = false;
					    }
					}
				    }

				    if(totalmatch) {
					print("Found total match, not adding to graph set",2);
					unique = false;
				    }

				}
				if(unique) {
				    print("Adding new graph",1);
				    modeGraphs.add(newGraph);
				}
				//now we reset enough of the graph to explore the other modes possible
			    } else {
				print("The graph was invalid or empty",2);
			    }

			    print("   committed = " + committedNodes.toString(),2);
			    print("Rolling back to :",2);
			    while(committedNodes.peek()!=null) {
				Node cnn = (Node)committedNodes.peek();
				CoreFunctionNode ccfn = (CoreFunctionNode)cnn.getWeight();
				print("   checking " + ccfn.getName(), 2);
				if(ccfn.allModesVisited()) { //if all modes have been touched
				    print("   ...unwinding it", 2);
				    //then unwind the graph
				    committedNodes.removeFirst();
				    committedEdges.removeFirst();
				    ccfn.resetAllModes();
				    ccfn.setVisited(false);

				} else { // otherwise, we have a mode to check, so let the outer loop proceed as normally
				    ccfn.setVisited(false);
				    break;
				}
			    }
			    print("   committed = " + committedNodes.toString(),2);

			    //WLP - Not sure if this is okay, but we
			    //seem to have a problem adding redundant
			    //edges/nodes to the working stack, so
			    //instead just dump everything and populate
			    nodeWorkStack.clear();
			    edgeWorkStack.clear();

			    print("Working node stack - before: " + nodeWorkStack.toString() + "\n",2);
			    print("Working edge stack - before: " + edgeWorkStack.toString() + "\n\n",2);

			    //referesh the node/edge working stack to
			    //be those edges that extend from this committed list
			    //then look through the inputs and outputs for more nodes/modes to discover
			    for(Iterator cnnIter = committedNodes.iterator(); cnnIter.hasNext();){
				Node cnn = (Node)cnnIter.next();
				CoreFunctionNode ccfn = (CoreFunctionNode)cnn.getWeight();
				CoreFunctionMode ccfm = ccfn.getVisitedMode();
				print("ccfn:"+ccfn.toString(),2);
				print("ccfm:"+ccfm.getName().toString(),2);
				Set inputsWork = ccfm.getInputs();
				Set outputsWork = ccfm.getOutputs();
				print("Visiting: " + ccfn.getName() + " mode = " + ccfm.getName() + ", " + inputsWork.toString() + ", " + outputsWork.toString(),2);
				for(Iterator inputsiterWork = inputsWork.iterator(); inputsiterWork.hasNext();) {
				    String inputName = (String)inputsiterWork.next();
				    //				    print("        input  = " + inputName + ": " + ccfm.getConsumption(inputName) + " <- " + g.getName(g.getEdge(inputName).source()),2);
				    if(ccfm.getConsumption(inputName)>0) {
					if(!committedEdges.contains(g.getEdge(inputName))){
					    nodeWorkStack.addLast(g.getEdge(inputName).source());
					    edgeWorkStack.addLast(g.getEdge(inputName));
					}
				    }
				}

				for(Iterator outputsiter = outputsWork.iterator(); outputsiter.hasNext();) {
				    String outputName = (String)outputsiter.next();
				    //				    print("    output  = " + outputName + ": " +  ccfm.getProduction(outputName) + " <- " + g.getName(g.getEdge(outputName).sink()),2);
				    if(ccfm.getProduction(outputName)>0) {
					if(!committedEdges.contains(g.getEdge(outputName))){
					    nodeWorkStack.addLast(g.getEdge(outputName).sink());
					    edgeWorkStack.addLast(g.getEdge(outputName));
					}
				    }
				}
			    }


			    print("Working node stack: " + nodeWorkStack.toString() + "\n",2);
			    print("Working edge stack: " + edgeWorkStack.toString() + "\n\n",2);
			    print("visited = ", 2);
			    for(Iterator nnIter = g.nodes().iterator(); nnIter.hasNext();){
				Node nn = (Node)nnIter.next();
				CoreFunctionNode cfnn = (CoreFunctionNode)nn.getWeight();
				if(cfnn.visited()){
				    print("   " + cfnn.getName(),2);
				}
			    }
			    print("*************   Done with building one **********\n",2);
			}
		    }
		}
	    }
	}
	return modeGraphs;
    }

    ///////////////////////////////////////////////////////////////////
    ////                     private variables                     ////

    /** The verbosity level */
    static private int _verbose_level;

    /** Flag to indicate whether mode grouping should be considered or
     * not. */
    static private boolean _modeGrouping;
}



