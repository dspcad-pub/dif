/*******************************************************************************
@ddblock_begin copyright
Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
@ddblock_end copyright
*******************************************************************************/

package dif;

import dif.graph.hierarchy.Port;
import dif.util.Conventions;
import dif.util.Value;
import dif.util.graph.Edge;
import dif.util.graph.Node;

import java.util.LinkedList;
import java.util.Map;

/**
 * Attribute class for DIF graphs.
 * DIFAttribute is contained by {@link AttributeContainer}.
 * Each element ({@link dif.util.graph.Node} and ({@link dif.util.graph.Edge}
 * ) in a {@link DIFGraph}
 * associates with an {@link AttributeContainer} containing all attributes
 * ({@link DIFAttribute}) of that element.
 * <p>
 * A value of a DIFAttribute can be either an instance of graph element
 * (reference type) or value object. The reference type can be
 * an instance of {@link Port}, {@link Edge}, {@link Node}, or
 * {@link DIFParameter}, or a LinkedList of those graph objects.
 * The value object of an DIFAttribute can be one of the following classes:
 * Integer, Double, Complex, String, Boolean, int[], double[],
 * Complex[], int[][], double[][], Complex[][], ArrayList.
 * {@link #setDataType} and {@link #getDataType} can be used to specify and
 * get the data type information of the value object.
 * <p>
 * A DIFAttribute can have a type. A type of a DIFAttribute is the class
 * identifier of the object represented by that DIFAttribute.
 * In some cases, attributes of the same actor can have the same name but
 * different types, the type of DIFAttribute is used to distinguish between
 * them.
 * For example, a Ptolemy actor may have a parameter called "input" and
 * have a IOPort called "input".
 *
 * @author Chia-Jui Hsu
 * @version $Id: DIFAttribute.java 606 2008-10-08 16:29:47Z plishker $
 * @see DIFGraph
 */


public class DIFAttribute {

    ///////////////////////////////////////////////////////////////////
    ////                       private fields                      ////
    private String _name;
    private AttributeContainer _container;
    private Object _value;
    private String _type;
    private String _dataType;

    /**
     * Empty default constructor.
     */
    protected DIFAttribute() {
    }

    /**
     * Constructor.
     *
     * @param name The attribute name.
     * @throws IllegalArgumentException If
     * {@link Conventions#labelConvention(String)} returns an error
     *                                  for the parameter name.
     */
    public DIFAttribute(String name) {
        String error = Conventions.labelConvention(name);
        if (error != null) {
            throw new IllegalArgumentException(
                    "Attribute name error: " + error);
        }
        _name = name;
    }

    /**
     * Return the cloned version of DIFAttribute with the same name, same
     * type, and same value.
     * <p>
     * For reference value referring to other element, we cannot simply copy
     * the reference. Thus,
     * if <i>_value</i> contains a reference, its cloned version is null.
     * This clone() method does
     * not copy the backward reference to {@link AttributeContainer}, ie,
     * <i>_container</i>, instead,
     * it is null in the cloned version. {@link AttributeContainer#clone()}
     * will handle this backward
     * reference.
     *
     * @return Cloned DIFAttribute instance.
     */
    @SuppressWarnings("CloneDoesntCallSuperClone") public Object clone() {
        DIFAttribute attribute = new DIFAttribute(_name);
        if (_value != null) {
            attribute.setValue(_cloneValue(null));
        }
        if (_type != null) {
            attribute.setType(_type);
        }
        if (_dataType != null) {
            attribute.setDataType(_dataType);
        }
        return attribute;
    }

    /**
     * Return the cloned version of DIFAttribute with the same name, the same
     * type, the same value,
     * and the reference of Node, Edge, or DIFParameter object, or the deep
     * clone of LinkedList.
     * <p>
     * For reference value, the mirrored object is obtained by <i>map</i>
     * that contains the mapping
     * from the original reference value to the cloned reference value. This
     * clone() method does not
     * copy the backward reference to {@link AttributeContainer}, ie,
     * <i>_container</i>, instead,
     * it is null in the cloned version.
     * {@link AttributeContainer#clone(Object)} will handle this
     * backward reference.
     *
     * @param map The Map that contains the mapping
     *            from the original reference value to the mirrored reference
     *            value.
     * @return Cloned DIFAttribute instance.
     * @see #_cloneValue(Object)
     */
    public Object clone(Object map) {
        DIFAttribute attribute = new DIFAttribute(_name);
        if (_value != null) {
            if (map instanceof Map) {
                attribute.setValue(_cloneValue(map));
            } else {
                attribute.setValue(_cloneValue(null));
            }
        }
        if (_type != null) {
            attribute.setType(_type);
        }
        if (_dataType != null) {
            attribute.setDataType(_dataType);
        }
        return attribute;
    }

    /**
     * Compare the DIFAttribute object with the input object. This method
     * compares their name,
     * type, and token value. To compare <i>_value</i> fields in both
     * objects, we use their
     * toString() methods to make the comparison.
     *
     * @param object The DIFAttribute instance.
     * @return True if equal, otherwise, false.
     */
    public boolean equals(Object object) {
        if (object instanceof DIFAttribute) {
            boolean equalName = false;
            boolean equalType = false;
            boolean equalDataType = false;
            boolean equalValue = false;
            if (((DIFAttribute) object).getName().equals(_name)) {
                equalName = true;
            }

            if (_type != null && _type.equals(
                    ((DIFAttribute) object).getType())) {
                equalType = true;
            } else if (_type == null &&
                    ((DIFAttribute) object).getType() == null) {
                equalType = true;
            }

            if (_dataType != null && _dataType.equals(
                    ((DIFAttribute) object).getDataType())) {
                equalDataType = true;
            } else if (_dataType == null &&
                    ((DIFAttribute) object).getDataType() == null) {
                equalDataType = true;
            }

            if (_value != null && ((DIFAttribute) object).getValue() != null &&
                    _value.toString().equals(
                            ((DIFAttribute) object).getValue().toString())) {
                equalValue = true;
            } else if (_value == null &&
                    ((DIFAttribute) object).getValue() == null) {
                equalValue = true;
            }

            if (equalName && equalType && equalDataType && equalValue) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get container.
     * Note that AttributeContainer.setAttribute() will set the
     * back reference.
     *
     * @return An AttributeContainer
     */
    public AttributeContainer getContainer() {
        return _container;
    }

    /**
     * Get data type of the value of attribute.
     *
     * @return data type
     */
    public String getDataType() {
        return _dataType;
    }

    /**
     * Set the data type of the value of attribute. E.g., int, float,
     * int*, float*.
     */
    public void setDataType(String datatype) {
        _dataType = datatype;
    }

    /**
     * Get attribute name.
     *
     * @return name
     */
    public String getName() {
        return _name;
    }

    /**
     * Get attribute type. DIF provides three
     * built-in actor attribute types: INPUT, OUTPUT, and PARAMETER.
     * Users can also specify their own types.
     * Another way to use attribute type is to indicate its actual
     * class. For example, a TypedIOPort in Ptolemy actor corrosponds to
     * an attribute with the same name and type
     * "ptolemy.actor.TypedIOPort".
     *
     * @return attribute type
     */
    public String getType() {
        return _type;
    }

    /**
     * Set the type of attribute.
     * <p>
     * DIF provides three built-in actor attribute types: INPUT, OUTPUT, and
     * PARAMETER.
     * Users can also specify their own types. Another way to use attribute
     * type is to
     * indicate its actual class.
     */
    public void setType(String type) {
        String error = Conventions.typeConvention(type);
        if (error != null) {
            throw new IllegalArgumentException(
                    "Attribute type error: " + error);
        }
        _type = type;
    }

    /**
     * Get attribute value.
     *
     * @return The value of this attribute or the reference
     * (port, edge, or DIFParameter) of this attribute.
     * @see #setValue(Object)
     */
    public Object getValue() {
        return _value;
    }

    public int hashCode() {
        return _name.hashCode();
    }

    /**
     * Set attribute value.
     *
     * @param value A value object. The current DIF Language supports
     *              are Integer, Double, Complex, String, Boolean, int[],
     *              double[],
     *              Complex[], int[][], double[][], Complex[][], ArrayList,
     *              or an object of
     *              Port/Edge/Node/DIFParameter, or a LinkedList of objects.
     * @return The previous value object.
     */
    public Object setValue(Object value) {
        Object returnValue = _value;
        _value = value;
        return returnValue;
    }

    /**
     * @return String description of attribute's name, type, and
     * value.
     */
    public String toString() {
        StringBuilder buffer = new StringBuilder("Attribute: " + _name);
        if (getType() != null) {
            buffer.append(", type = ").append(getType());
        }
        if (getDataType() != null) {
            buffer.append(", datatype = ").append(getDataType());
        }
        if (getValue() != null) {
            buffer.append(", value = ").append(getValue().toString());
        }
        return buffer.toString();
    }

    /**
     * Clone token value. If <i>_value</i> contains a reference to an
     * element object, it finds the cloned object by using <i>map</i>.
     * Note that it will return null
     * if <i>map</i> does not contain the mapping. Users must be
     * aware of this.
     *
     * @param map The HashMap object.
     * @see #clone(Object)
     */
    private Object _cloneValue(Object map) {
        if (Value.isValue(_value)) {
            return Value.cloneValue(_value);
        } else if ((_value instanceof Port || _value instanceof Edge ||
                _value instanceof Node || _value instanceof DIFParameter) &&
                map instanceof Map) {
            return ((Map) map).get(_value);
        } else if (_value instanceof LinkedList && map instanceof Map) {
            LinkedList<Object> objList = new LinkedList<>();
            for (Object o : ((LinkedList) _value)) {
                objList.add(((Map) map).get(o));
            }
            return objList;
        } else {
            return null;
        }
    }

    /**
     * {@link AttributeContainer#setAttribute} will set the back
     * reference.
     *
     * @param container The {@link AttributeContainer} contains this
     *                  DIFAttribute.
     */
    void _setContainer(AttributeContainer container) {
        _container = container;
    }
}


