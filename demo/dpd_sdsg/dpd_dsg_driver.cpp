/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <iostream>

#include "welt_cpp_graph.h"
#include "dpd_graph_difdsg.h"
#include "dpd_sdsg.h"

int main(int argc, char **argv) {

	char *data_filename;
	char *pa_in_filename;
	char *pa_out_filename;
	char *param_filename;
	char *output_filename;

	int i = 0;
	int arg_count = 6;

	/* Check program usage. */
	if (argc != arg_count) {
		fprintf(stderr,
				".exe error: arg count\n");
		exit(1);
	}

	/* Open the input and output file(s). */
	i = 1;
	/* Open input and output files. */
	data_filename = argv[i++];
	pa_in_filename = argv[i++];
	pa_out_filename = argv[i++];
	param_filename = argv[i++];
	output_filename = argv[i++];
	int param_length = 13;
	int coeffs_length = 65536;

	int* param = (int*)malloc(sizeof(int)* param_length);
	float* coeffs =(float*) malloc(sizeof(float)* coeffs_length);
	auto* test_dpd_graph = new dpd_graph_difdsg( data_filename, pa_in_filename,
			pa_out_filename, param_filename, output_filename, param,
			param_length, coeffs, coeffs_length);


	/* Execute the graph. */
	auto* dsg = new dpd_sdsg(test_dpd_graph, param);
	dsg->dsg_scheduler(0,1);

	/* Terminate graph */
	delete test_dpd_graph;
	delete dsg;

	return 0;

}



