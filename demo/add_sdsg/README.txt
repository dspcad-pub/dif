This is demo code for Sequential Dataflow Schedule Graphs(SDSG) for ADD
application.

Usage: To compile the demo run `makeme`
to run the demo run `runme`

This demo takes two inputs (x.txt  and y.txt). The sum of the two inputs
should be stored in out.txt.