SET(CMAKE_C_STANDARD 11)
SET(CMAKE_CXX_STANDARD 14)
set(THREADS_PREFER_PTHREAD_FLAG ON)

INCLUDE_DIRECTORIES(
        $ENV{UXWELTER}/lang/c/src/gems/actors/basic
        $ENV{UXWELTER}/lang/c/src/gems/actors/common
        $ENV{UXWELTER}/lang/c/src/gems/edges
        $ENV{UXWELTER}/lang/c/src/tools/runtime
        $ENV{UXWELTER}/lang/c/src/tools/graph

        $ENV{UXWELTER}/exp/lang/cpp/src/gems/actors/dpd

        $ENV{UXWELTER}/lang/cpp/src/apps/basic
        $ENV{UXWELTER}/lang/cpp/src/gems/actors/common
        $ENV{UXWELTER}/lang/cpp/src/gems/actors/basic
        $ENV{UXWELTER}/lang/cpp/src/tools/graph
        $ENV{UXWELTER}/lang/cpp/src/tools/runtime
        $ENV{UXWELTER}/lang/cpp/src/utils

        $ENV{UXWELTER}/lang/cpp/src/gems/actors/dsg_actors/cdsg_actors
        $ENV{UXWELTER}/lang/cpp/src/gems/actors/dsg_actors/ref_actors
        $ENV{UXWELTER}/lang/cpp/src/gems/actors/dsg_actors/sca_actors
        $ENV{UXWELTER}/lang/cpp/src/gems/actors/dsg_actors/common
        $ENV{UXWELTER}/lang/cpp/src/utils/dsg
        $ENV{UXWELTER}/exp/lang/cpp/src/apps/dsg
        $ENV{UXWELTER}/lang/cpp/src/tools/graph
        $ENV{UXWELTER}/lang/cpp/src/tools/scheduler/dsg
        $ENV{UXDIF}/lang/cpp/src/apps/add

        $ENV{UXDICELANG}/c/src/util
)

LINK_DIRECTORIES(
        $ENV{WELTERGEN}
        $ENV{DIFGEN}
)

ADD_EXECUTABLE(add_cdsg_driver.exe
        add_cdsg_driver.cpp
        $ENV{UXDIF}/lang/cpp/src/apps/add/add_graph.cpp
        $ENV{UXDIF}/lang/cpp/src/apps/add/add_shched2.cpp
        )

TARGET_LINK_LIBRARIES(
        add_cdsg_driver.exe
        -lpthread
        $ENV{WELTERGEN}/libwelt_c_actors_basic.a
        $ENV{WELTERGEN}/libwelt_c_actors_common.a
        $ENV{WELTERGEN}/libwelt_c_edges.a
        $ENV{WELTERGEN}/libwelt_c_graph_common.a
        $ENV{WELTERGEN}/libwelt_c_runtime.a

        $ENV{WELTERGEN}/libwelt_cpp_actors_basic.a
        $ENV{WELTERGEN}/libwelt_cpp_actor.a
        $ENV{WELTERGEN}/libwelt_cpp_graph_basic.a
        $ENV{WELTERGEN}/libwelt_cpp_runtime.a
        $ENV{WELTERGEN}/libwelt_cpp_utils.a
        $ENV{WELTERGEN}/libwelt_cpp_graph.a
        $ENV{WELTERGEN}/libwelt_c_runtime.a

        $ENV{WELTERGEN}/libwelt_cpp_dsg_actor.a
        $ENV{WELTERGEN}/libwelt_cpp_cdsg_actors.a
        $ENV{WELTERGEN}/libwelt_cpp_dsg_ref.a
        $ENV{WELTERGEN}/libwelt_cpp_dsg_sca.a
        $ENV{WELTERGEN}/libwelt_cpp_dsg_scheduler.a
        $ENV{WELTERGEN}/libwelt_cpp_dsg_graph.a
        $ENV{WELTERGEN}/libwelt_cpp_cdsg_util.a

        $ENV{DLXGEN}/c/libutil.a
)

INSTALL(TARGETS add_cdsg_driver.exe DESTINATION .)





