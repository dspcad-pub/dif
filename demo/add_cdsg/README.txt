This is demo code for Concurrent Dataflow Schedule Graphs(CDSG) for ADD
application.
Usage: To compile the demo run `makeme`
to run the demo run `runme`

This demo takes two inputs (x.txt  and y.txt). The sum of the two inputs
should be stored in out.txt.