cmake_minimum_required(VERSION 2.9)
SET(CMAKE_C_STANDARD 11)
SET(CMAKE_CXX_STANDARD 14)
set(CMAKE_C_COMPILER "gcc")
set(CMAKE_CXX_COMPILER "g++")

INCLUDE_DIRECTORIES(
        $ENV{UXDIF}/import/INCLUDE
        $ENV{UXWELTER}/lang/c/src/gems/actors/basic
        $ENV{UXWELTER}/lang/c/src/gems/actors/common
        $ENV{UXWELTER}/lang/c/src/gems/edges
        $ENV{UXWELTER}/lang/c/src/tools/runtime
        $ENV{UXWELTER}/lang/c/src/tools/graph

        $ENV{UXDIF}/lang/cpp/src/gems/actors/dpd
        $ENV{UXDIF}/lang/cpp/src/apps/dpd
        $ENV{UXWELTER}/lang/cpp/src/tools/graph
        $ENV{UXWELTER}/lang/cpp/src/gems/actors/dsg_actors/cdsg_actors
        $ENV{UXWELTER}/lang/cpp/src/gems/actors/dsg_actors/ref_actors
        $ENV{UXWELTER}/lang/cpp/src/gems/actors/dsg_actors/sca_actors
        $ENV{UXWELTER}/lang/cpp/src/gems/actors/dsg_actors/common
        $ENV{UXWELTER}/lang/cpp/src/utils/dsg


        $ENV{UXWELTER}/lang/cpp/src/apps/basic
        $ENV{UXWELTER}/lang/cpp/src/gems/actors/common
        $ENV{UXWELTER}/lang/cpp/src/gems/actors/basic
        $ENV{UXWELTER}/lang/cpp/src/tools/graph
        $ENV{UXWELTER}/lang/cpp/src/tools/runtime
        $ENV{UXWELTER}/lang/cpp/src/utils

        $ENV{UXDICELANG}/c/src/util
)

LINK_DIRECTORIES(
        $ENV{WELTERGEN}
)

ADD_EXECUTABLE(dpd_cdsg_difdsg_driver.exe
        dpd_cdsg_driver.cpp
        )

TARGET_LINK_LIBRARIES(
        dpd_cdsg_difdsg_driver.exe
        -lm
#        -lcblas
#        -llapack
        -lpthread

        $ENV{DIFGEN}/libdpd_app.a
        $ENV{DIFGEN}/libdpd_actors.a

        $ENV{WELTERGEN}/libwelt_c_runtime.a
        $ENV{WELTERGEN}/libwelt_cpp_dsg_actor.a
        $ENV{WELTERGEN}/libwelt_cpp_cdsg_actors.a
        $ENV{WELTERGEN}/libwelt_cpp_dsg_ref.a
        $ENV{WELTERGEN}/libwelt_cpp_dsg_sca.a
        $ENV{WELTERGEN}/libwelt_cpp_dsg_scheduler.a
        $ENV{WELTERGEN}/libwelt_cpp_graph_dsg.a
        $ENV{WELTERGEN}/libwelt_cpp_dsg_graph.a
        $ENV{WELTERGEN}/libwelt_cpp_cdsg_util.a

        $ENV{WELTERGEN}/libwelt_cpp_actors_basic.a
        $ENV{WELTERGEN}/libwelt_cpp_actor.a
        $ENV{WELTERGEN}/libwelt_cpp_graph_basic.a
        $ENV{WELTERGEN}/libwelt_cpp_runtime.a
        $ENV{WELTERGEN}/libwelt_cpp_utils.a
        $ENV{WELTERGEN}/libwelt_cpp_graph.a
        $ENV{WELTERGEN}/libwelt_cpp_graph_dsg.a
        $ENV{WELTERGEN}/libwelt_cpp_dsg_actor.a

        $ENV{WELTERGEN}/libwelt_c_actors_basic.a
        $ENV{WELTERGEN}/libwelt_c_actors_common.a
        $ENV{WELTERGEN}/libwelt_c_edges.a
        $ENV{WELTERGEN}/libwelt_c_graph_common.a
        $ENV{WELTERGEN}/libwelt_c_runtime.a

		$ENV{DIFGEN}/CLAPACK/libblas.a
		$ENV{DIFGEN}/CLAPACK/libf2c.a
		$ENV{DIFGEN}/CLAPACK/liblapack.a

        $ENV{DLXGEN}/c/libutil.a
)
add_library(lapack STATIC IMPORTED)


INSTALL(TARGETS dpd_cdsg_difdsg_driver.exe DESTINATION $ENV{DIFGEN})





