This is demo code for CDSG (Concurrent Dataflow Schedule Graphs) for DPD(
(Digital Predistortion))
application.

Usage: To compile the demo run `makeme`
to run the demo run `runme`


The output of the adaptive digital predistortion system[1] will be created in
out.txt

[1]L. Li et al., "A Framework for Design and Implementation of Adaptive Digital
Predistortion Systems," 2019 IEEE International Conference on Artificial
Intelligence Circuits and Systems (AICAS), 2019, pp. 112-116,
doi: 10.1109/AICAS.2019.8771476.