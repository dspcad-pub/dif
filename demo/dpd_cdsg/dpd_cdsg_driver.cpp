/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <iostream>

#include "welt_cpp_graph.h"
#include "dpd_graph_difdsg.h"
#include "dpd_cdsg.h"
#include <pthread.h>

pthread_mutex_t *mutex1;
pthread_cond_t *cond;

typedef struct {
	int id;
	dpd_graph_difdsg **graph;
	dpd_cdsg **dsg;
}parm;

void *pthread_scheduler(void *arg);

int main(int argc, char **argv) {

	mutex1 = (pthread_mutex_t*)malloc(sizeof(pthread_mutex_t));
	cond = (pthread_cond_t*)malloc(sizeof(pthread_cond_t));

	if (pthread_mutex_init(mutex1, NULL) != 0)
	{
		printf("\n mutex init failed\n");
		return 1;
	}

	if (pthread_cond_init(cond, NULL) != 0)
	{
		printf("\n mutex_cond init failed\n");
		return 1;
	}

	char *data_filename;
	char *pa_in_filename;
	char *pa_out_filename;
	char *param_filename;
	char *output_filename;

	int i = 0;
	int arg_count = 6;

	/* Check program usage. */
	if (argc != arg_count) {
		fprintf(stderr,
				".exe error: arg count\n");
		exit(1);
	}

	/* Open the input and output file(s). */
	i = 1;
	/* Open input and output files. */
	data_filename = argv[i++];
	pa_in_filename = argv[i++];
	pa_out_filename = argv[i++];
	param_filename = argv[i++];
	output_filename = argv[i++];
	int param_length = 13;
	int coeffs_length = 65536;

	int* param = (int*)malloc(sizeof(int)* param_length);
	float* coeffs =(float*) malloc(sizeof(float)* coeffs_length);
	auto* dpd_graph = new dpd_graph_difdsg( data_filename, pa_in_filename,
			pa_out_filename, param_filename, output_filename, param,
			param_length, coeffs, coeffs_length);

	/* Posix thread related variable(s). */
	pthread_t *threads;
	pthread_attr_t pthread_custom_attr;
	parm *p;

	/* Posix thread configuration(s). */

	threads=(pthread_t *)malloc(NUM_THREAD * sizeof(*threads));
	pthread_attr_init(&pthread_custom_attr);

	p=(parm *)malloc(sizeof(parm)*NUM_THREAD);

	/* Execute the graph. */
	auto* dsg = new dpd_cdsg(dpd_graph, param, param,
							mutex1, cond);

	for (i=0; i<NUM_THREAD; i++)
	{
		p[i].id=i;
		p[i].graph = &dpd_graph;
		p[i].dsg = &dsg;
	}

	/* Execute graph*/
	for (i=0; i<NUM_THREAD; i++)
	{
		pthread_create(&threads[i], &pthread_custom_attr, pthread_scheduler,
					   (void *)(p+i));
	}

	for (i=0; i<NUM_THREAD; i++)
	{
		pthread_join(threads[i],NULL);
	}

	/* Terminate graph */
	delete dpd_graph;
	delete dsg;

	return 0;

}

void *pthread_scheduler(void *arg)
{
	parm *p=(parm *)arg;

	if (p->id == 0){
		/* Execute graph*/
		printf("id 0\n");
		(*(p->dsg))->dsg_scheduler(0,NUM_THREAD);
	}
	else if (p->id == 1){
		/* Execute graph*/
		printf("id 1\n");

		(*(p->dsg))->dsg_scheduler(1,NUM_THREAD);
	}
	else if (p->id == 2){
		/* Execute graph*/
		printf("id 2\n");
		(*(p->dsg))->dsg_scheduler(2,NUM_THREAD);
	}

	/* Execute graph */
	return (NULL);
}


