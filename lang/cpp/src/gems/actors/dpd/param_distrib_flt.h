/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#ifndef _param_distrib_flt_h
#define _param_distrib_flt_h

/* Overview: This actor distributes the parameters(tap) related to FIR filters.
It assigns the amount of memory required to implement the filter.*/


#include "welt_cpp_actor.h"

extern "C" {
#include "welt_c_fifo.h"
#include "welt_c_util.h"
};
#include "common_param.h"

/* Actor modes */
#define PARAM_DISTRIB_FLT_MODE_READ (1)
#define PARAM_DISTRIB_FLT_MODE_WRITE    (2)

class param_distrib_flt:public welt_cpp_actor{
public:

    param_distrib_flt(int* param, welt_c_fifo_pointer out_poly,
            welt_c_fifo_pointer out_acc, welt_c_fifo_pointer out_flt_coeff,
            vector<welt_c_fifo_pointer>& out_flt, int fifo_begin);

    bool enable() override;

    void invoke() override;

    void reset() override;

    void connect(welt_cpp_graph *graph) override;


private:
    /* length params */
    int data_length;
    int num_filter_norm;
    int num_filter_conj;
    int* param;
    int iter;
    /* Actor interface ports. */
    welt_c_fifo_pointer out_poly;
    vector<welt_c_fifo_pointer> out_flt;
    welt_c_fifo_pointer out_acc;
    welt_c_fifo_pointer out_flt_coeff;
};

#endif
