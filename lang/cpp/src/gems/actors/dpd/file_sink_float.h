/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#ifndef _file_sink_float_h
#define _file_sink_float_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

/*******************************************************************************
The primary mode of this actor is the PROCESS mode. In that mode,
the actor consumes one integer-valued token on its input edge on each firing,
and appends the integer value to a text file that is associated with
that actor.

Input(s): The token type is integer.

Output(a): This is a sink_float actor; it has no output edges.

Actor modes and transitions: The PROCESS mode consumes one token from its
input edge and appends the token value to the file that is associated
with the actor.

Initial mode: The actor starts out in the PROCESS mode.

Mode     in_fifo
 ----------------------------
    PROCESS         0
    COMPLETE    -1
    ERROR           -1

*******************************************************************************/
/*Overview: This actor reads a float value one at a time, and writes the values
to a text file via the standard ofstream. Reading stops as soon as there is no
available data exisiting in the file or when end of file is reached, whichever
happens first. The reset function ensures that the output file is re-opened for
writing, discarding any previously written contents in the file.*/

#include <fstream>
#include <string>
extern "C" {
#include "welt_c_util.h"
#include "welt_c_fifo.h"
}
#include "welt_cpp_actor.h"
#define FILE_SINK_INT_MAX_FIFO_COUNT (1)
/* Actor modes */
#define FILE_SINK_INT_MODE_PROCESS (1)
#define FILE_SINK_INT_MODE_COMPLETE (2)
#define FILE_SINK_INT_MODE_ERROR (3)

class file_sink_float : public welt_cpp_actor{
public:
    file_sink_float(welt_c_fifo_pointer in, char* file_name);

    /*Destructor*/
    ~file_sink_float() override;

    bool enable() override;

    void invoke() override;

    /* Reset the actor so that the output file is re-opened for writing,
    discarding any previously written contents in the file.*/
    void reset() override;

    void connect(welt_cpp_graph *graph) override;


private:
    char* file_name;
    ofstream outStream;
    float data;
    welt_c_fifo_pointer in;
};


#endif
