/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "dpd_filter.h"
#define MAX_FIFO_COUNT (4)

inline cplx complex_multiply(cplx a, cplx b)
{
    cplx result;
    result.real = a.real * b.real - a.imag * b.imag;
    result.imag = a.real * b.imag + a.imag * b.real;
    return(result);
}

inline cplx complex_add(cplx a, cplx b)
{
    cplx result;
    result.real = a.real + b.real;
    result.imag = a.imag + b.imag;
    return(result);
}

dpd_filter::dpd_filter(welt_c_fifo_pointer param,
        welt_c_fifo_pointer est, welt_c_fifo_pointer data,
        welt_c_fifo_pointer out) {

    this->mode = DPD_FILTER_MODE_STORE_PARAM;

    this->tap_flt = 0;
    this->param = param;
    this->in = data;
    this->est = est;
    this->out = out;

    /* Set maximum port count */
    this->actor_set_max_port_count(MAX_FIFO_COUNT);

    this->actor_add_portrefs(&this->param);
    this->actor_add_portrefs(&this->in);
    this->actor_add_portrefs(&this->est);
    this->actor_add_portrefs(&this->out);
}

bool dpd_filter::enable( ) {
    bool result = false;
    switch (this->mode) {
        case DPD_FILTER_MODE_STORE_PARAM:
            result = (welt_c_fifo_population(this->param) >= 1);
            break;
        case DPD_FILTER_MODE_READ_EST:
            result = (welt_c_fifo_population(this->est) >= SIZE_CPLX *
                    this->tap_flt);
            break;
        case DPD_FILTER_MODE_PROCESS:
            result = (welt_c_fifo_population(this->in) >= SIZE_CPLX) &&
                    (welt_c_fifo_population(this->out) <
                    welt_c_fifo_capacity(this->out));
            break;
        default:
            result = false;
            break;
    }
    return result;
}

void dpd_filter::invoke() {
    int i;
    cplx sum;

    switch (this->mode) {
        case DPD_FILTER_MODE_STORE_PARAM:
            /* Error checking not done on parameters */
            welt_c_fifo_read(this->param, &(this->tap_flt));
            this->mode = DPD_FILTER_MODE_READ_EST;
            break;

        case DPD_FILTER_MODE_READ_EST:
            for(i=0; i < this->tap_flt; i++){
                welt_c_fifo_read(this->est, &(this->flt_tap[i].real));
                welt_c_fifo_read(this->est, &(this->flt_tap[i].imag));
                this->data[i].real = 0;
                this->data[i].imag = 0;
            }
            this->mode = DPD_FILTER_MODE_PROCESS;
            break;

        case DPD_FILTER_MODE_PROCESS:
            welt_c_fifo_read(this->in, &(this->data[0].real));
            welt_c_fifo_read(this->in, &(this->data[0].imag));

            /* FIR filtering */
            sum.real = 0;
            sum.imag = 0;
            for(i = 0; i < this->tap_flt; i++){
                sum = complex_add(sum, complex_multiply(this->data[i],
                        this->flt_tap[i]));
            }
            welt_c_fifo_write(this->out, &(sum.real));
            welt_c_fifo_write(this->out, &(sum.imag));
        
            /* Delay line shifting */
            for(i = this->tap_flt-1; i > 0; i--){
                this->data[i] = this->data[i-1];
            }
            break;

        default:
            this->mode = DPD_FILTER_MODE_STORE_PARAM;
            break;
    }
}


void dpd_filter::reset() {
    return;
}

void dpd_filter::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;

    /* Register the port in enclosing graph. */
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection(this, port_index, direction);

    /* Register the port in enclosing graph. */
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 1;
    graph->add_connection(this, port_index, direction);

    /* Register the port in enclosing graph. */
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 2;
    graph->add_connection(this, port_index, direction);

    /* Register the port in enclosing graph. */
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 3;
    graph->add_connection(this, port_index, direction);

}
