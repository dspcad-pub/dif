/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "accumulator.h"

#define MAX_FIFO_COUNT (MAX_FLT+3)

accumulator::accumulator(
        welt_c_fifo_pointer params, vector<welt_c_fifo_pointer>& in_flt_in, int
        fifo_begin, welt_c_fifo_pointer in_est, welt_c_fifo_pointer out){

    int i;
    this->mode = ACCUMULATOR_MODE_STORE_PARAM;
    this->params = params;
    this->in_flt.reserve(MAX_FLT);
    for (i = fifo_begin; i < fifo_begin +MAX_FLT; i++){
        this->in_flt.push_back(in_flt_in[i]);
    }
    this->in_est = in_est;
    this->out = out;
    
    /* Set maximum port count */
    this->actor_set_max_port_count(MAX_FIFO_COUNT);
    /* Set portrefs*/
    this->actor_add_portrefs(&this->params);
    for (i = 0; i < MAX_FLT; i++) {
        this->actor_add_portrefs(&this->in_flt[i]);
    }
    this->actor_add_portrefs(&this->in_est);
    this->actor_add_portrefs(&this->out);

}


bool accumulator::enable() {
    bool result = false;
    int i;
    switch (this->mode) {
        case ACCUMULATOR_MODE_STORE_PARAM:
            result = (welt_c_fifo_population(this->params) >= 1);
            result = result && (welt_c_fifo_population(this->in_est) >=
                    SIZE_CPLX);
            break;
        case ACCUMULATOR_MODE_PROCESS:
            result = (welt_c_fifo_population(this->out) < \
                    welt_c_fifo_capacity(this->out));
            for (i = 0; i < this->num_filter; i++){
                result = result && (welt_c_fifo_population(this->in_flt[i])>=
                        SIZE_CPLX);
            }

            break;
        default:
            result = false;
            break;
    }
    return result;
}

void accumulator::invoke() {
    int i;
    float data_i, data_q, sum_i, sum_q;

    switch (this->mode) {
        case ACCUMULATOR_MODE_STORE_PARAM:
            welt_c_fifo_read(this->params, &(this->num_filter));
            //printf("ACCUMULATOR_MODE_STORE_PARAM:%d\n",this->num_filter);
            welt_c_fifo_read(this->in_est, &(this->est_i));
            welt_c_fifo_read(this->in_est, &(this->est_q));
            this->mode = ACCUMULATOR_MODE_PROCESS;
            break;

        case ACCUMULATOR_MODE_PROCESS:
            //printf("ACCUMULATOR_MODE_PROCESS\n");
            sum_i = 0;
            sum_q = 0;
            for(i=0; i < this->num_filter; i++){
                welt_c_fifo_read(this->in_flt[i], &data_i);
                welt_c_fifo_read(this->in_flt[i], &data_q);
                sum_i +=  data_i;
                sum_q +=  data_q;
            }
            
            #ifdef LO_ESTIMATE
            sum_i +=  this->est_i;
            sum_q +=  this->est_q;
            #endif
            welt_c_fifo_write(this->out, &sum_i);
            welt_c_fifo_write(this->out, &sum_q);
            break;
        default:
            this->mode = ACCUMULATOR_MODE_STORE_PARAM;
            break;
    }
    return;
}
void accumulator::reset() {
    return;
}
void accumulator::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;

    /* Register the port in enclosing graph. */
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection(this, port_index, direction);

    int i;
    for (i = 1; i < MAX_FLT+1; i++){
        /* Register the port in enclosing graph. */
        direction = GRAPH_IN_CONN_DIRECTION;
        port_index = i;
        graph->add_connection(this, port_index, direction);
    }
    /* Register the port in enclosing graph. */
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = MAX_FLT+1;
    graph->add_connection(this, port_index, direction);

    /* Register the port in enclosing graph. */
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = MAX_FLT+2;
    graph->add_connection(this, port_index, direction);
}
