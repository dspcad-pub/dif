/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "config_sink_int.h"

#define MAX_FIFO_COUNT (1)

config_sink_int::config_sink_int(welt_c_fifo_pointer input,
        int* target, int target_param_index, int target_param_length) {

    this->input = input;
    this->target = target;
    /*target_param_index is used to cache the target param length
    information that is available from the actor's parameter information. */
    this->target_param_index = target_param_index;
    this->target_param_length = target_param_length;
    this->mode=CONFIG_SINK_INT_MODE_CONFIG;

    /* Set maximum port count */
    this->actor_set_max_port_count(MAX_FIFO_COUNT);
    this->actor_add_portrefs(&this->input);
}

bool config_sink_int::enable() {
    bool result = false;
    switch (this->mode) {
        case CONFIG_SINK_INT_MODE_CONFIG:
            result = (welt_c_fifo_population(this->input) >=
                    this->target_param_length);
            break;
        case CONFIG_SINK_INT_MODE_INACTIVE:
            result = false;
            break;
        default:
            result = false;
            break;
    }
    return result;
}

void config_sink_int::invoke() {
    int i;
    switch (this->mode) {
        case CONFIG_SINK_INT_MODE_CONFIG:
            for(i = 0; i < this->target_param_length; i++) {
                welt_c_fifo_read(this->input, (
                        &this->target[this->target_param_index] + i));
            }
            this->mode = CONFIG_SINK_INT_MODE_CONFIG;
            break;
        case CONFIG_SINK_INT_MODE_INACTIVE:
            this->mode = CONFIG_SINK_INT_MODE_INACTIVE;
            break;
        default:
            this->mode = CONFIG_SINK_INT_MODE_INACTIVE;
            break;
    }
}


void config_sink_int::reset() {
    return;
}

void config_sink_int::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;

    /* Register the port in enclosing graph. */
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection(this, port_index, direction);

}
