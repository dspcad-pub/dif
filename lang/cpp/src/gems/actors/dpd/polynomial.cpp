/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "polynomial.h"

#define MAX_FIFO_COUNT (1 + MAX_FLT)

/* local utility functions */
static inline float abs_sqr(float i, float q)
{
    return(i*i+q*q);
}

static inline float power(float x, int p)
{
    int i;
    float prod = 1.0;
    for(i = 0; i < p; i++)
        prod = prod * x;
    return(prod);
}

polynomial::polynomial(welt_c_fifo_pointer data, welt_c_fifo_pointer params,
        vector<welt_c_fifo_pointer>& out_in, int fifo_begin) {

    int i;
    this->mode = POLYNOMIAL_MODE_STORE_PARAM;
    this->num_filter_norm = 0;
    this->num_filter_conj = 0;
    this->data = data;
    this->params = params;
    this->out.reserve(MAX_FLT);
    for (i = fifo_begin; i < fifo_begin + MAX_FLT; i++){
        this->out.push_back(out_in[i]);
    }
    /* Set maximum port count */
    this->actor_set_max_port_count(MAX_FIFO_COUNT);
    this->actor_add_portrefs(&this->data);
    this->actor_add_portrefs(&this->params);

    for (i = 0; i < MAX_FLT; i++) {
        this->actor_add_portrefs(&this->out[i]);
    }
}

bool polynomial::enable() {
    bool result = false;
    int i;
    switch (this->mode) {
        case POLYNOMIAL_MODE_STORE_PARAM:
            result = (welt_c_fifo_population(this->params) >= 2);
            /* Check if all the previous output has been consumed before
                    updating the params*/
            for (i = 0; i < MAX_FLT; i++){
                result = result && (welt_c_fifo_population(this->out[i]) == 0);
            }
            break;
        case POLYNOMIAL_MODE_PROCESS:
            result = (welt_c_fifo_population(this->data) >= SIZE_CPLX);
            for (i = 0; i < this->num_filter_norm + this->num_filter_conj; i++){
                result = result && (welt_c_fifo_population(this->out[i]) <
                        welt_c_fifo_capacity(this->out[i]));
            }
            break;

        default:
            result = false;
            break;
    }

    return result;
}

void polynomial::invoke() {
    int i;
    float data_i, data_q, x_i, x_q;
    switch (this->mode) {
        case POLYNOMIAL_MODE_STORE_PARAM:
            welt_c_fifo_read(this->params, &(this->num_filter_norm));
            welt_c_fifo_read(this->params, &(this->num_filter_conj));
            this->mode = POLYNOMIAL_MODE_PROCESS;
            break;

        case POLYNOMIAL_MODE_PROCESS:
            /* polynomial computation */
            welt_c_fifo_read(this->data, &data_i);
            welt_c_fifo_read(this->data, &data_q);
            #ifdef POLY_TYPE_ORTH
            #else
            for(i=0; i < this->num_filter_norm; i++){
                x_i =  data_i*power(abs_sqr(data_i, data_q), i);
                x_q =  data_q*power(abs_sqr(data_i, data_q), i);
                welt_c_fifo_write(this->out[i], &x_i);
                welt_c_fifo_write(this->out[i], &x_q);
            }
            for(i=0; i < this->num_filter_conj; i++){
                x_i =  data_i*power(abs_sqr(data_i, data_q), i);
                x_q =  -data_q*power(abs_sqr(data_i, data_q), i);
                welt_c_fifo_write(this->out[this->num_filter_norm+i], &x_i);
                welt_c_fifo_write(this->out[this->num_filter_norm+i], &x_q);
            }

            #endif
            /* Next Mode Decision: if new param available,
                    switch to STORE_PARAM */
            if(welt_c_fifo_population(this->params) >= 2)
                this->mode = POLYNOMIAL_MODE_STORE_PARAM;
            else
                this->mode = POLYNOMIAL_MODE_PROCESS;
            break;
        default:
            this->mode = POLYNOMIAL_MODE_STORE_PARAM;
            break;
    }
    return;
}



void polynomial::reset() {
    this->mode = POLYNOMIAL_MODE_STORE_PARAM;
}


void polynomial::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;

    /* Register the port in enclosing graph. */
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection(this, port_index, direction);

    /* Register the port in enclosing graph. */
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 1;
    graph->add_connection(this, port_index, direction);

    int i;
    for (i = 2; i < MAX_FLT+2; i++){
        /* Register the port in enclosing graph. */
        direction = GRAPH_OUT_CONN_DIRECTION;
        port_index = i;
        graph->add_connection(this, port_index, direction);
    }
}
