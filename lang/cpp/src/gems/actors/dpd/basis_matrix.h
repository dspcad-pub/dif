/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#ifndef _basis_matrix_h
#define _basis_matrix_h

/* Overview: This actor reads data to form two matrices of float value, and
output the address of two matrices' element to its output edge intersectually.
The output edge holds matrix element address row by row. For example:
The first row of matrix q is [q_1, q_2, q_3] and the first row of matrix p is
[p_1, p_2, p_3], then the output edge holds
[&q_1, &p_1, &q_2, &p_2, &q_3, &p_3 ...]. Read matrix row and column length
first and read matrix element value after. Reading stops when all input value
from FIFO is read up.  */

#include "welt_cpp_actor.h"
#include "welt_cpp_graph.h"
extern "C"{
#include "welt_c_fifo.h"
#include "welt_c_util.h"
};
#include "common_param.h"

/* Actor modes */
#define BASIS_MATRIX_MODE_STORE_PARAM   (1)
#define BASIS_MATRIX_MODE_PROCESS   (2)

class basis_matrix: public welt_cpp_actor{
public:
    basis_matrix(welt_c_fifo_pointer data, welt_c_fifo_pointer params,
            welt_c_fifo_pointer out);
    bool enable() override;

    void invoke() override;

    void reset() override;

    void connect(welt_cpp_graph *graph) override;

private:
    /* length variable. */
    int num_filter;
    int data_length;

    /* Input ports. */
    welt_c_fifo_pointer data;
    welt_c_fifo_pointer params;

    /* Output port. */
    welt_c_fifo_pointer out;
};


#endif
