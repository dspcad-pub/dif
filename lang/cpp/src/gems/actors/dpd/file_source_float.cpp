/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "file_source_float.h"

#define MAX_FIFO_COUNT (1)

file_source_float::file_source_float(char *file, welt_c_fifo_pointer out) {

    this->file = welt_c_util_fopen((const char *)file, "r");
    this->mode = FILE_SOURCE_FLOAT_MODE_WRITE;
    this->out = out;

    /* Set maximum port count */
    this->actor_set_max_port_count(MAX_FIFO_COUNT);
    this->actor_add_portrefs(&this->out);

}

bool file_source_float::enable() {
    bool result = false;

    switch (this->mode) {
        case FILE_SOURCE_FLOAT_MODE_WRITE:
            result = (welt_c_fifo_population(this->out) <
                    welt_c_fifo_capacity(this->out));
            break;
        case FILE_SOURCE_FLOAT_MODE_INACTIVE:
            result = false;
            break;
        default:
            result = false;
            break;
    }
    return result;
}

void file_source_float::invoke() {
    float value = 0;
    switch (this->mode) {
        case FILE_SOURCE_FLOAT_MODE_WRITE:
            if (fscanf(this->file, "%f", &value) != 1) {
                /* End of input */
                this->mode = FILE_SOURCE_FLOAT_MODE_INACTIVE;
                return;
            }
            welt_c_fifo_write(this->out, &value);
            this->mode = FILE_SOURCE_FLOAT_MODE_WRITE;
            break;
        case FILE_SOURCE_FLOAT_MODE_INACTIVE:
            this->mode = FILE_SOURCE_FLOAT_MODE_INACTIVE;
            break;
        default:
            this->mode = FILE_SOURCE_FLOAT_MODE_INACTIVE;
            break;
    }
}


void file_source_float::reset() {
    return;
}

void file_source_float::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;


    /* Register the port in enclosing graph. */
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection(this, port_index, direction);

}
