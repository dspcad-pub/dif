/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#ifndef _file_source_float_h
#define _file_source_float_h

/* Overview: This actor reads float values from a text file, and outputs the
* values one at a time to its output edge. Reading stops as soon as there is no
* available data exisiting in the file or when end of file is reached, whichever
* happens first. */

#include "welt_cpp_actor.h"

extern "C"{
#include "welt_c_fifo.h"
#include "welt_c_util.h"
};
#include "common_param.h"

/* Actor modes */
#define FILE_SOURCE_FLOAT_MODE_WRITE        (1)
#define FILE_SOURCE_FLOAT_MODE_INACTIVE     (2)

class file_source_float: public welt_cpp_actor{
public:
    file_source_float(char *file, welt_c_fifo_pointer out);

    bool enable() override;

    void invoke() override;

    void reset() override;

    void connect(welt_cpp_graph *graph) override;


private:
    FILE *file;
    welt_c_fifo_pointer out;
};

#endif
