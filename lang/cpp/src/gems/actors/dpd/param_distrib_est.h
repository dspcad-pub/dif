/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#ifndef _param_distrib_est_h
#define _param_distrib_est_h

/*Overview: This actor distributes all the length parameters, including the
data length, the number of filters, and the filter length. */

#include <stdio.h>
#include <stdlib.h>

#include "welt_cpp_actor.h"

extern "C"{
#include "welt_c_fifo.h"
#include "welt_c_util.h"
};
#include "common_param.h"

#define PARAM_DISTRIB_EST_DATA_LENGTH_INDEX (0)
#define PARAM_DISTRIB_EST_FLT_NORM_INDEX    (1)
#define PARAM_DISTRIB_EST_FLT_CONJ_INDEX    (2)

/* Actor modes */
#define PARAM_DISTRIB_EST_MODE_READ (1)
#define PARAM_DISTRIB_EST_MODE_WRITE    (2)

class param_distrib_est: public welt_cpp_actor{

public:
    param_distrib_est(int* param,
            welt_c_fifo_pointer out_bm_norm,
            welt_c_fifo_pointer out_bm_conj,
            welt_c_fifo_pointer out_all);

    bool enable() override;

    void invoke() override;

    void reset() override;

    void connect(welt_cpp_graph *graph) override;

private:

    /* length params */
    int data_length;
    int num_filter_norm;
    int num_filter_conj;
    int* param;
    int iter;
    /* Actor interface ports. */
    welt_c_fifo_pointer out_bm_norm; /* for basis_matrix */
    welt_c_fifo_pointer out_bm_conj; /* for basis_matrix conjugate branch */
    welt_c_fifo_pointer out_all; /* All the parameters */

};

#endif
