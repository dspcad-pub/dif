/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#ifndef _accumulator_h
#define _accumulator_h

/* Overview: This actor has two modes: ACCUMULATOR_MODE_STORE_PARAM and
* ACCUMULATOR_MODE_PROCESS.
ACCUMULATOR_MODE_STORE_PARAM is the default mode, which reads and stores the
paramaters. ACCUMULATOR_MODE_PROCESS adds all available parameters and output
the sum.*/

#include "welt_cpp_actor.h"
#include "welt_cpp_graph.h"
extern "C"{
#include "welt_c_fifo.h"
#include "welt_c_util.h"
};
#include "common_param.h"

/* Actor modes */
#define ACCUMULATOR_MODE_STORE_PARAM    (1)
#define ACCUMULATOR_MODE_PROCESS    (2)

class accumulator: public welt_cpp_actor{
public:
    accumulator(welt_c_fifo_pointer params,
            vector<welt_c_fifo_pointer>&in_flt, int fifo_begin,
            welt_c_fifo_pointer in_est, welt_c_fifo_pointer out);
    bool enable() override;

    void invoke() override;

    void reset() override;

    void connect(welt_cpp_graph *graph) override;

private:
    int num_filter;
    float est_i;
    float est_q;

    /* Input ports. */
    welt_c_fifo_pointer params;
    vector<welt_c_fifo_pointer> in_flt;
    welt_c_fifo_pointer in_est;

    /* Output ports */
    welt_c_fifo_pointer out;

};

#endif
