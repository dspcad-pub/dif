/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <iostream>
#include "param_distrib_est.h"
#define MAX_FIFO_COUNT (3)

param_distrib_est::param_distrib_est(int* param,
        welt_c_fifo_pointer out_bm_norm, welt_c_fifo_pointer out_bm_conj,
        welt_c_fifo_pointer out_all){

    this->mode = PARAM_DISTRIB_EST_MODE_WRITE;
    this->iter=0;
    this->data_length = 0;
    this->num_filter_norm = 0;
    this->num_filter_conj = 0;
    this->param = param;
    this->out_bm_norm = out_bm_norm;
    this->out_bm_conj = out_bm_conj;
    this->out_all = out_all;

    /* Set maximum port count */
    this->actor_set_max_port_count(MAX_FIFO_COUNT);

    this->actor_add_portrefs(&this->out_bm_norm);
    this->actor_add_portrefs(&this->out_bm_conj);
    this->actor_add_portrefs(&this->out_all);
}

bool param_distrib_est::enable() {
    bool result = false;
    switch (this->mode) {
        case PARAM_DISTRIB_EST_MODE_WRITE:
            result = true;
            break;
        default:
            result = false;
            break;
    }
    return result;
}

void param_distrib_est::invoke() {
    int i;
    int max_len_norm = 0, max_len_conj = 0, sum_len_norm = 0, sum_len_conj = 0;
    switch (this->mode) {
        case PARAM_DISTRIB_EST_MODE_WRITE:
            /* write data length */
            this->data_length = this->param[PARAM_DISTRIB_EST_DATA_LENGTH_INDEX];
            welt_c_fifo_write(this->out_bm_norm, &this->data_length);
            welt_c_fifo_write(this->out_bm_conj, &this->data_length);
            welt_c_fifo_write(this->out_all, &this->data_length);
            
            /* write number of filters */
            this->num_filter_norm =
                    this->param[PARAM_DISTRIB_EST_FLT_NORM_INDEX];
            this->num_filter_conj =
                    this->param[PARAM_DISTRIB_EST_FLT_CONJ_INDEX];
            welt_c_fifo_write(this->out_bm_norm, &this->num_filter_norm);
            welt_c_fifo_write(this->out_bm_conj, &this->num_filter_conj);
            welt_c_fifo_write(this->out_all, &this->num_filter_norm);
            welt_c_fifo_write(this->out_all, &this->num_filter_conj);

            /* write filter lengths */
            for (i = PARAM_DISTRIB_EST_FLT_CONJ_INDEX + 1; i <
                    PARAM_DISTRIB_EST_FLT_CONJ_INDEX + 1 +
                    this->num_filter_norm; i++){
                welt_c_fifo_write(this->out_bm_norm, &this->param[i]);
                sum_len_norm += this->param[i];
                if (this->param[i] > max_len_norm)
                    max_len_norm = this->param[i];
                
            }
            for (i = PARAM_DISTRIB_EST_FLT_CONJ_INDEX +1+this->num_filter_norm;
                    i < PARAM_DISTRIB_EST_FLT_CONJ_INDEX + 1 +
                    this->num_filter_norm + this->num_filter_conj; i++){
                welt_c_fifo_write(this->out_bm_conj, &this->param[i]);
                sum_len_conj += this->param[i];
                if (this->param[i] > max_len_conj)
                    max_len_conj = this->param[i];
            }
            welt_c_fifo_write(this->out_all, &max_len_norm);
            welt_c_fifo_write(this->out_all, &max_len_conj);
            welt_c_fifo_write(this->out_all, &sum_len_norm);
            welt_c_fifo_write(this->out_all, &sum_len_conj);
            this->mode = PARAM_DISTRIB_EST_MODE_READ;
            this->iter++;
            break;
        default:
            this->mode = PARAM_DISTRIB_EST_MODE_READ;
            break;
    }
}

void param_distrib_est::reset() {
    this->mode = PARAM_DISTRIB_EST_MODE_WRITE;
}

void param_distrib_est::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;

    /* Register the port in enclosing graph. */
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection(this, port_index, direction);
    
    /* Register the port in enclosing graph. */
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 1;
    graph->add_connection(this, port_index, direction);

    /* Register the port in enclosing graph. */
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 2;
    graph->add_connection(this, port_index, direction);


}
