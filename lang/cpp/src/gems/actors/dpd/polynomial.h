/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#ifndef _polynomial_h
#define _polynomial_h

/* Overview: This actor first takes the number of filter taps, then take
pa_in data and compute them */

#include "welt_cpp_actor.h"
extern "C"{
#include "welt_c_fifo.h"
#include "welt_c_util.h"
};
#include "common_param.h"

/* Actor modes */
#define POLYNOMIAL_MODE_STORE_PARAM (1)
#define POLYNOMIAL_MODE_PROCESS (2)

class polynomial:public welt_cpp_actor{

public:
    polynomial(welt_c_fifo_pointer data, welt_c_fifo_pointer params,
            vector<welt_c_fifo_pointer>& out, int fifo_begin);

    bool enable() override;

    void invoke() override;

    void reset() override;

    void connect(welt_cpp_graph *graph) override;

private:
    /* length variable. */
    int num_filter_norm;
    int num_filter_conj;

    /* Input ports. */
    welt_c_fifo_pointer data;
    welt_c_fifo_pointer params;

    /* Output ports */
    vector<welt_c_fifo_pointer> out;
};

#endif
