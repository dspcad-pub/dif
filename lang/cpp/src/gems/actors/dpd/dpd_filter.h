/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#ifndef _dpd_filter_h
#define _dpd_filter_h

/* Overview: This actor does the application of an FIR filter to the input,
which comes from the output of the polynomial. It also properly counteracts
the delay introduced by the filtering by shifting the signal in time. */


#include "welt_cpp_actor.h"
extern "C"{
#include "welt_c_fifo.h"
#include "welt_c_util.h"
};
#include "common_param.h"

/* Actor modes */
#define DPD_FILTER_MODE_STORE_PARAM (1)
#define DPD_FILTER_MODE_READ_EST    (2)
#define DPD_FILTER_MODE_PROCESS (3)

class dpd_filter: public welt_cpp_actor{
public:
    dpd_filter(welt_c_fifo_pointer param, welt_c_fifo_pointer est,
            welt_c_fifo_pointer data, welt_c_fifo_pointer out);
    bool enable() override;

    void invoke() override;

    void reset() override;

    void connect(welt_cpp_graph *graph) override;

private:
    /* params */
    int tap_flt;
    cplx data[MAX_TAP_FLT];
    cplx flt_tap[MAX_TAP_FLT];

    /* Actor interface ports. */
    welt_c_fifo_pointer param;
    welt_c_fifo_pointer in;
    welt_c_fifo_pointer est;
    welt_c_fifo_pointer out;
};

#endif
