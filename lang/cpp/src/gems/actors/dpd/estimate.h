/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#ifndef _estimate_h
#define _estimate_h

/*Overview: This actor estimates the DPD coefficients according to the reference
data and basic matrices. Its output will be used as the basis for the filtering
stage.*/

#include "welt_cpp_actor.h"

extern "C"{
#include "welt_c_fifo.h"
#include "welt_c_util.h"
};
#include "common_param.h"

/* Actor modes */
#define ESTIMATE_MODE_STORE_PARAM   (1)
#define ESTIMATE_MODE_PROCESS   (2)

class estimate: public welt_cpp_actor{

public:
    estimate(welt_c_fifo_pointer param, welt_c_fifo_pointer norm,
            welt_c_fifo_pointer conj, welt_c_fifo_pointer ref,
            welt_c_fifo_pointer out);
    bool enable() override;

    void invoke() override;

    void reset() override;

    void connect(welt_cpp_graph *graph) override;

private:
    /* params */
    int ref_length;
    int num_filter_norm;
    int num_filter_conj;
    int max_len_norm;
    int max_len_conj;
    int sum_len_norm;
    int sum_len_conj;
    /* Actor interface ports. */
    welt_c_fifo_pointer param;
    welt_c_fifo_pointer norm;
    welt_c_fifo_pointer conj;
    welt_c_fifo_pointer ref;
    welt_c_fifo_pointer out;
};

#endif
