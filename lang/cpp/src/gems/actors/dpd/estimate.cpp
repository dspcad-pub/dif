/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
extern "C"{
#include "stdio.h"
#include "stdlib.h"
#include "f2c.h"
#include "blaswrap.h"
#include "clapack.h"
#include "string.h"
};
#include "estimate.h"

#define MAX_FIFO_COUNT (5)

estimate::estimate(welt_c_fifo_pointer param,
        welt_c_fifo_pointer norm, welt_c_fifo_pointer conj,
        welt_c_fifo_pointer ref, welt_c_fifo_pointer out) {

    this->mode = ESTIMATE_MODE_STORE_PARAM;
    this->ref_length = 0;
    this->num_filter_norm = 0;
    this->num_filter_conj = 0;
    this->max_len_norm = 0;
    this->max_len_conj = 0;
    this->sum_len_norm = 0;
    this->sum_len_conj = 0;
    this->param = param;
    this->norm = norm;
    this->conj = conj;
    this->ref = ref;
    this->out = out;

    /* Set maximum port count */
    this->actor_set_max_port_count(MAX_FIFO_COUNT);

    this->actor_add_portrefs(&this->param);
    this->actor_add_portrefs(&this->norm);
    this->actor_add_portrefs(&this->conj);
    this->actor_add_portrefs(&this->ref);
    this->actor_add_portrefs(&this->out);

}

bool estimate::enable() {
    bool result = false;

    switch (this->mode) {
        case ESTIMATE_MODE_STORE_PARAM:
            result = (welt_c_fifo_population(this->param) >= 7);
            break;

        case ESTIMATE_MODE_PROCESS:
            result = (welt_c_fifo_population(this->norm) >=
                    (this->ref_length + this->max_len_norm - 1)*
                    this->sum_len_norm * 2) &&
                    (welt_c_fifo_population(this->conj) >=
                    (this->ref_length + this->max_len_conj - 1)*
                    this->sum_len_conj * 2) &&
                    (welt_c_fifo_population(this->ref) >= this->ref_length) &&
                    (welt_c_fifo_population(this->out) < welt_c_fifo_capacity
                    (this->out));
        break;
    default:
        result = false;
        break;
    }
    return result;
}

/*******************************************************************************
The invoke method assumes that a corresponding call to enable has
returned TRUE so that the required data is available.

*******************************************************************************/
void estimate::invoke() {
    int row, col, mmax, size_norm;
    integer lda, ldb, lwork, info=0, nrhs, num_row, num_col; //LAPACK Variables
    char trans = 'N';
    complex *ref, *mat, *mat_norm, *mat_conj, *work;
    float val_r, val_i;  //real, imag follwing CLAPACK convention
    switch (this->mode) {
        case ESTIMATE_MODE_STORE_PARAM:
            /* Error checking not done on parameters */
            welt_c_fifo_read(this->param, &(this->ref_length));
            welt_c_fifo_read(this->param, &(this->num_filter_norm));
            welt_c_fifo_read(this->param, &(this->num_filter_conj));
            welt_c_fifo_read(this->param, &(this->max_len_norm));
            welt_c_fifo_read(this->param, &(this->max_len_conj));
            welt_c_fifo_read(this->param, &(this->sum_len_norm));
            welt_c_fifo_read(this->param, &(this->sum_len_conj));
            this->mode = ESTIMATE_MODE_PROCESS;
            break;
        case ESTIMATE_MODE_PROCESS:
            
        #ifdef LO_ESTIMATE
            num_col = this->sum_len_norm + this->sum_len_conj + 1;
        #else
            num_col = this->sum_len_norm + this->sum_len_conj;
        #endif
            mmax = this->max_len_norm > this->max_len_conj?
                this->max_len_norm:this->max_len_conj;
            mmax = mmax - 1;
            /* Read reference data */
            /*Discard first Mmax samples, (Covariance windowing) */
            ref = (complex *)welt_c_util_malloc(sizeof(complex)*
                    (this->ref_length - mmax));
            for(row = 0; row < mmax; row++){
                welt_c_fifo_read(this->ref, &(val_r));
                welt_c_fifo_read(this->ref, &(val_i));
            }
            for(; row < this->ref_length; row++){
                welt_c_fifo_read(this->ref, &(val_r));
                welt_c_fifo_read(this->ref, &(val_i));
                ref[row - mmax].r = val_r;
                ref[row - mmax].i = val_i;
            }
            /* Read Basis Matrices (Column major as required by CLAPACK) */
            mat = (complex *)welt_c_util_malloc(sizeof(complex) *
                    (this->ref_length - mmax) * num_col);
            memset(mat, 0, (this->ref_length - mmax) * num_col);
            /* same contigous memory is being used for storing both norm and
            conj basis matrices.*/
            mat_norm = mat;
            size_norm = this->sum_len_norm * (this->ref_length - mmax);
            mat_conj = &(mat[size_norm]);
            /* Norm Matrix: Discard mmax rows */
            for(row = 0; row < mmax; row++) {
                for(col = 0; col < this->sum_len_norm; col++){
                    welt_c_fifo_read(this->norm, &val_r);
                    welt_c_fifo_read(this->norm, &val_i);
                }
            }
            for(; row < this->ref_length; row++){
                for(col = 0; col < this->sum_len_norm; col++){
                    welt_c_fifo_read(this->norm, &val_r);
                    welt_c_fifo_read(this->norm, &val_i);
                    mat_norm[row - mmax + col * (this->ref_length - mmax)].r =
                            val_r;
                    mat_norm[row - mmax + col * (this->ref_length - mmax)].i =
                            val_i;
                }
            }
            /* Conj Matrix: Discard mmax rows */
            for(row = 0; row < mmax; row++){
                for(col = 0; col < this->sum_len_conj; col++){
                    welt_c_fifo_read(this->conj, &val_r);
                    welt_c_fifo_read(this->conj, &val_i);
                }
            }
            for(; row < this->ref_length; row++){
                for(col = 0; col < this->sum_len_conj; col++){
                    welt_c_fifo_read(this->conj, &val_r);
                    welt_c_fifo_read(this->conj, &val_i);
                    mat_conj[row - mmax + col * (this->ref_length - mmax)].r =
                            val_r;
                    mat_conj[row - mmax + col * (this->ref_length - mmax)].i =
                            val_i;
                }
            }
            #ifdef LO_ESTIMATE
            /* last colmn of mat to all 1s */
            col = num_col-1;
            for(row = 0; row < this->ref_length - mmax; row++){
                mat[row + col * (this->ref_length - mmax)].r = 1;
                mat[row + col * (this->ref_length - mmax)].i = 0;
            }
            #endif
            
            num_row = this->ref_length - mmax;
            nrhs = 1;
            lda = num_row;
            ldb = num_row;
            lwork = 2*(num_row < num_col?num_row:num_col);
            work = (complex *)welt_c_util_malloc(sizeof(complex) *  lwork);
    
            cgels_(&trans, &num_row, &num_col, &nrhs, mat, &lda, ref, &ldb,
                    work, &lwork, &info);
            for(col = 0; col < num_col; col++){
                val_r = ref[col].r;
                val_i = ref[col].i;
                welt_c_fifo_write(this->out, &val_r);
                welt_c_fifo_write(this->out, &val_i);
                
            }
            fflush(stdout);
            this->mode = ESTIMATE_MODE_STORE_PARAM;
            break;
        default:
            this->mode = ESTIMATE_MODE_STORE_PARAM;
            break;
    }
}

void estimate::reset() {
    return;
}

void estimate::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;

    /* Register the port in enclosing graph. */
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection(this, port_index, direction);

    /* Register the port in enclosing graph. */
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 1;
    graph->add_connection(this, port_index, direction);

    /* Register the port in enclosing graph. */
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 2;
    graph->add_connection(this, port_index, direction);

    /* Register the port in enclosing graph. */
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 3;
    graph->add_connection(this, port_index, direction);

    /* Register the port in enclosing graph. */
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 4;
    graph->add_connection(this, port_index, direction);

}
