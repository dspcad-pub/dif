/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include "split_fifo.h"
#define MAX_FIFO_COUNT 3

split_fifo::split_fifo(welt_c_fifo_pointer in,
        welt_c_fifo_pointer out1, welt_c_fifo_pointer out2) {

    this->mode = SPLIT_FIFO_MODE_PROCESS;
    this->in = in;
    this->out1 = out1;
    this->out2 = out2;
    /* Set maximum port count */
    this->actor_set_max_port_count(MAX_FIFO_COUNT);

    this->actor_add_portrefs(&this->in);
    this->actor_add_portrefs(&this->out1);
    this->actor_add_portrefs(&this->out2);

}

bool split_fifo::enable() {
    bool result = false;

    switch (this->mode) {
        case SPLIT_FIFO_MODE_PROCESS:
            result = (welt_c_fifo_population(this->in) >= 1) &&
                    (welt_c_fifo_population(this->out1) <
                    welt_c_fifo_capacity(this->out1)) &&
                    (welt_c_fifo_population(this->out2) <
                    welt_c_fifo_capacity(this->out2));
            break;
        default:
            result = false;
            break;
    }
    return result;
}

void split_fifo::invoke() {
    int value = 0;
    
    switch (this->mode) {
        case SPLIT_FIFO_MODE_PROCESS:
            welt_c_fifo_read(this->in, &value);
            welt_c_fifo_write(this->out1, &value);
            welt_c_fifo_write(this->out2, &value);
            this->mode = SPLIT_FIFO_MODE_PROCESS;
            break;
        default:
            this->mode = SPLIT_FIFO_MODE_PROCESS;
            break;
    }
}

void split_fifo::reset() {
    this->mode = SPLIT_FIFO_MODE_PROCESS;
}

void split_fifo::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;

    /* Register the port in enclosing graph. */
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection(this, port_index, direction);

    /* Register the port in enclosing graph. */
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 1;
    graph->add_connection(this, port_index, direction);

    /* Register the port in enclosing graph. */
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 2;
    graph->add_connection(this, port_index, direction);

}
