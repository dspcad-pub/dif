/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#ifndef _flt_coeff_distrib_h
#define _flt_coeff_distrib_h

/*Overview: This actor reads and writes the filter coefficients. It stores the
coefficients separately based on their real and imaginary parts. This actor
lays foundation for the further accumulation of samples from all branches. */

#include "welt_cpp_actor.h"

extern "C"{
#include "welt_c_fifo.h"
#include "welt_c_util.h"
};
#include "common_param.h"

/* Actor modes */
#define FLT_COEFF_DISTRIB_MODE_READ_NUM_FILTER  (1)
#define FLT_COEFF_DISTRIB_MODE_READ_FLT_TAPS    (2)
#define FLT_COEFF_DISTRIB_MODE_DISTRIBUTE   (3)

class flt_coeff_distrib: public welt_cpp_actor{

public:
    flt_coeff_distrib(float* coeffs, welt_c_fifo_pointer param,
            vector<welt_c_fifo_pointer>& out_flt, int fifo_begin,
            welt_c_fifo_pointer out_acc);

    bool enable() override;

    void invoke() override;

    void reset() override;

    void connect(welt_cpp_graph *graph) override;


private:
    /* length params */
    int num_filter;
    int tot_coeff;
    int flt_tap[MAX_FLT];

    /* Actor interface ports. */
    float* coeffs;
    welt_c_fifo_pointer param;
    vector<welt_c_fifo_pointer> out_flt;
    welt_c_fifo_pointer out_acc;
};


#endif
