/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include "flt_coeff_distrib.h"
#define MAX_FIFO_COUNT (2 + MAX_FLT)

flt_coeff_distrib::flt_coeff_distrib(float* coeffs,
        welt_c_fifo_pointer param, vector<welt_c_fifo_pointer>& out_flt, int
        fifo_begin, welt_c_fifo_pointer out_acc) {
    int i;

    this->mode = FLT_COEFF_DISTRIB_MODE_READ_NUM_FILTER;
    this->num_filter = 0;
    this->tot_coeff = 0;
    this->coeffs = coeffs;
    this->param = param;
    this->out_flt.reserve(MAX_FLT);
    for (i = fifo_begin; i <fifo_begin+ MAX_FLT; i++){
        this->out_flt.push_back(out_flt[i]);
    }
    this->out_acc = out_acc;

    /* Set maximum port count */
    this->actor_set_max_port_count(MAX_FIFO_COUNT);
    this->actor_add_portrefs(&this->param);
    for (i = 0; i < MAX_FLT; i++) {
        this->actor_add_portrefs(&this->out_flt[i]);
    }
    this->actor_add_portrefs(&this->out_acc);
}

bool flt_coeff_distrib::enable() {
    bool result = false;
    int i;
    switch (this->mode) {
        case FLT_COEFF_DISTRIB_MODE_READ_NUM_FILTER:
            result = (welt_c_fifo_population(this->param) >= 1);
            break;
        case FLT_COEFF_DISTRIB_MODE_READ_FLT_TAPS:
            result = (welt_c_fifo_population(this->param) >= this->num_filter);
            break;
        case FLT_COEFF_DISTRIB_MODE_DISTRIBUTE:
            result = true;
            for (i = 0; i < this->num_filter; i++)
            result = result && (welt_c_fifo_population
                    (this->out_flt[i])<welt_c_fifo_capacity(this->out_flt[i]));
            result = result && (welt_c_fifo_population(this->out_acc) <
                    welt_c_fifo_capacity(this->out_acc));
            break;
        default:
            result = false;
            break;
}
    return result;
}

void flt_coeff_distrib::invoke() {
    int i, j, pt;
    float value_real, value_imag;
    switch (this->mode) {
        case FLT_COEFF_DISTRIB_MODE_READ_NUM_FILTER:
            welt_c_fifo_read(this->param, &(this->num_filter));
            this->mode = FLT_COEFF_DISTRIB_MODE_READ_FLT_TAPS;
            break;
        case FLT_COEFF_DISTRIB_MODE_READ_FLT_TAPS:
            this->tot_coeff = 0;
            for (i = 0; i < this->num_filter; i++){
                welt_c_fifo_read(this->param, &(this->flt_tap[i]));
                this->tot_coeff = this->tot_coeff + this->flt_tap[i];
            }
            this->tot_coeff += 1; /* For LO leakage compensation */
            this->mode = FLT_COEFF_DISTRIB_MODE_DISTRIBUTE;
            break;
        case FLT_COEFF_DISTRIB_MODE_DISTRIBUTE:
            /* read and write filter co-efficients */
            pt = 0;
        for (i = 0; i < this->num_filter; i++)
            for (j = 0; j < this->flt_tap[i]; j++)
            {
                value_real = this->coeffs[pt];
                pt++;
                value_imag = this->coeffs[pt];
                pt++;
                welt_c_fifo_write(this->out_flt[i], &value_real);
                welt_c_fifo_write(this->out_flt[i], &value_imag);
            }
        #ifdef LO_ESTIMATE
            value_real = this->coeffs[pt];
            pt++;
            value_imag = this->coeffs[pt];
            welt_c_fifo_write(this->out_acc, &value_real);
            welt_c_fifo_write(this->out_acc, &value_imag);
        #endif
            this->mode = FLT_COEFF_DISTRIB_MODE_READ_NUM_FILTER;
            break;
        default:
            this->mode = FLT_COEFF_DISTRIB_MODE_READ_NUM_FILTER;
            break;
    }
}

void flt_coeff_distrib::reset() {
    this->mode = FLT_COEFF_DISTRIB_MODE_READ_NUM_FILTER;
}
void flt_coeff_distrib::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;

    /* Register the port in enclosing graph. */
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection(this, port_index, direction);

    int i;
    for (i = 1; i < MAX_FLT+1; i++){
        /* Register the port in enclosing graph. */
        direction = GRAPH_OUT_CONN_DIRECTION;
        port_index = i;
        graph->add_connection(this, port_index, direction);
    }

    /* Register the port in enclosing graph. */
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = MAX_FLT+1;
    graph->add_connection(this, port_index, direction);

}
