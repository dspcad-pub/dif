/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include "basis_matrix.h"

#define MAX_FIFO_COUNT (3)

inline float abs_sqr(float i, float q)
{
    return(i*i+q*q);
}
inline float power(float x, int p)
{
    int i;
    float prod = 1.0;
    for(i = 0; i < p; i++)
        prod = prod * x;
    return(prod);
}

/*Implementations of interface functions*/
basis_matrix::basis_matrix(
welt_c_fifo_pointer data, welt_c_fifo_pointer params,
welt_c_fifo_pointer out) {
    this->mode = BASIS_MATRIX_MODE_STORE_PARAM;
    this->num_filter = 0;
    this->data_length=0;
    this->data = data;
    this->params = params;
    this->out = out;

    /* Set maximum port count */
    this->actor_set_max_port_count(MAX_FIFO_COUNT);
    /* Set portrefs*/
    this->actor_add_portrefs(&this->data);
    this->actor_add_portrefs(&this->params);
    this->actor_add_portrefs(&this->out);
}

bool basis_matrix::enable() {
    bool result = false;

    switch (this->mode) {
        case BASIS_MATRIX_MODE_STORE_PARAM:
            result = (welt_c_fifo_population(this->params) >= 2);
            break;
        case BASIS_MATRIX_MODE_PROCESS:
            result = (welt_c_fifo_population(this->data) >= 2 *
                    this->data_length) && (welt_c_fifo_population(this->params)
                    >= this->num_filter) && (welt_c_fifo_population(this->out) <
                    welt_c_fifo_capacity(this->out));
            break;
        default:
            result = false;
            break;
    }
    return result;
}

void basis_matrix::invoke() {
    int i,j,k,colmn;
    int *filter_lengths;
    int m_max=0, param_tot=0;
    float *data_i, *data_q;
    float **x_i, **x_q; /*matrix for storing X */
    float **PHI_i, **PHI_q;/* Basis Matrix */

    switch (this->mode) {
        case BASIS_MATRIX_MODE_STORE_PARAM:

            welt_c_fifo_read(this->params, &(this->data_length));
            if (this->data_length <= 0) {
                this->mode = BASIS_MATRIX_MODE_STORE_PARAM;
                return;
            }
            welt_c_fifo_read(this->params, &(this->num_filter));
            if (this->num_filter <= 0) {
                this->mode = BASIS_MATRIX_MODE_STORE_PARAM;
                return;
            }
            this->mode = BASIS_MATRIX_MODE_PROCESS;
            break;

        case BASIS_MATRIX_MODE_PROCESS:
            /* basis matrix computation */
            /*Read filter lengths from fifo */
            filter_lengths = (int *) welt_c_util_malloc(sizeof(int)
                    * this->num_filter);
            for (i = 0; i < this->num_filter; i++) {
                welt_c_fifo_read(this->params, &(filter_lengths[i]));
                param_tot += filter_lengths[i];
                if (m_max < filter_lengths[i])
                    m_max = filter_lengths[i];
            }
            #ifdef POLY_TYPE_ORTH
            #else
            data_i = (float *) welt_c_util_malloc(
                    sizeof(float) * this->data_length);
            data_q = (float *) welt_c_util_malloc(
                    sizeof(float) * this->data_length);

            x_i = (float **) welt_c_util_malloc(sizeof(float *)
                    * this->num_filter);
            x_q = (float **) welt_c_util_malloc(sizeof(float *)
                    * this->num_filter);
            for (i = 0; i < this->num_filter; i++) {
                x_i[i] = (float *) welt_c_util_malloc(sizeof(float)
                        * this->data_length);
                x_q[i] = (float *) welt_c_util_malloc(sizeof(float)
                        * this->data_length);
            }

            PHI_i = (float **) welt_c_util_malloc(sizeof(float *) * param_tot);
            PHI_q = (float **) welt_c_util_malloc(sizeof(float *) * param_tot);
            for (i = 0; i < param_tot; i++) {
                PHI_i[i] = (float *) welt_c_util_malloc(sizeof(float) *
                        (this->data_length + m_max - 1));
                PHI_q[i] = (float *) welt_c_util_malloc(sizeof(float) *
                        (this->data_length + m_max - 1));
            }
            //read data and form matrix X
            for (i = 0; i < this->data_length; i++) {
                welt_c_fifo_read(this->data, &(data_i[i]));
                welt_c_fifo_read(this->data, &(data_q[i]));
                for (j = 0; j < this->num_filter; j++) {
                    x_i[j][i] = data_i[i] * power(abs_sqr(data_i[i],
                            data_q[i]), j);
                    x_q[j][i] = data_q[i] * power(abs_sqr(data_i[i],
                            data_q[i]), j);
                }
            }

            /* form basis matrix */
            colmn = 0;
            for (j = 0; j < this->num_filter; j++) {
                for (k = 0; k < filter_lengths[j]; k++) {
                    // fill k zeros in beggining of coulmn
                    for (i = 0; i < k; i++) {
                        PHI_i[colmn][i] = 0;
                        PHI_q[colmn][i] = 0;
                    }
                    for (i = k; i < this->data_length + k; i++) {
                        PHI_i[colmn][i] = x_i[j][i - k];
                        PHI_q[colmn][i] = x_q[j][i - k];
                    }
                    for (i = this->data_length + k; i < this->data_length
                            + m_max - 1; i++) {
                        PHI_i[colmn][i] = 0;
                        PHI_q[colmn][i] = 0;
                    }
                    colmn++;
                }
            }
            
            #endif
            /* write to fifo row first */
            for (i = 0; i < this->data_length + m_max - 1; i++) {
                for (j = 0; j < param_tot; j++) {
                    welt_c_fifo_write(this->out, &(PHI_i[j][i]));
                    welt_c_fifo_write(this->out, &(PHI_q[j][i]));
                }
            }
            /* free memories */
            free(data_i);
            free(data_q);
            
            for (i = 0; i < this->num_filter; i++) {
                free(x_i[i]);
                free(x_q[i]);
            }
            free(x_i);
            free(x_q);
            
            for (i = 0; i < param_tot; i++) {
                free(PHI_i[i]);
                free(PHI_q[i]);
            }
            free(PHI_i);
            free(PHI_q);

            this->mode = BASIS_MATRIX_MODE_STORE_PARAM;
            break;

        default:
            this->mode = BASIS_MATRIX_MODE_STORE_PARAM;
            break;
    }
    return;
}

void basis_matrix::reset() {
    this->mode = BASIS_MATRIX_MODE_STORE_PARAM;
}

void basis_matrix::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;

    /* Register the port in enclosing graph. */
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection(this, port_index, direction);

    /* Register the port in enclosing graph. */
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 1;
    graph->add_connection(this, port_index, direction);

    /* Register the port in enclosing graph. */
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 2;
    graph->add_connection(this, port_index, direction);
}


