/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include "param_distrib_flt.h"
#define MAX_FIFO_COUNT 3+MAX_FLT

param_distrib_flt::param_distrib_flt(int* param, welt_c_fifo_pointer out_poly,
        welt_c_fifo_pointer out_acc, welt_c_fifo_pointer out_flt_coeff,
        vector<welt_c_fifo_pointer>& out_flt, int fifo_begin) {
    int i;
    this->iter=0;
    this->mode = PARAM_DISTRIB_FLT_MODE_WRITE;
    this->data_length = 0;
    this->num_filter_norm = 0;
    this->num_filter_conj = 0;
    this->param = param;
    this->out_poly = out_poly;
    this->out_flt.reserve(MAX_FLT);
    for(i =  fifo_begin; i < MAX_FLT+ fifo_begin; i++){
        this->out_flt.push_back(out_flt[i]);
    }
    this->out_acc = out_acc;
    this->out_flt_coeff = out_flt_coeff;

    /* Set maximum port count */
    this->actor_set_max_port_count(MAX_FIFO_COUNT);

    this->actor_add_portrefs(&this->out_poly);
    this->actor_add_portrefs(&this->out_acc);
    this->actor_add_portrefs(&this->out_flt_coeff);
    for (i = 0; i < MAX_FLT; i++) {
        this->actor_add_portrefs(&this->out_flt[i]);
    }
}

bool param_distrib_flt::enable() {
    bool result = false;
    switch (this->mode) {
        case PARAM_DISTRIB_FLT_MODE_WRITE:
            result = true;
            break;
        default:
            result = false;
            break;
    }
    return result;
}

void param_distrib_flt::invoke() {
    int value, i;
    int num_flt = 0;
    switch (this->mode) {
        case PARAM_DISTRIB_FLT_MODE_WRITE:
            this->data_length = this->param[0];
            this->num_filter_norm = this->param[1];
            this->num_filter_conj = this->param[2];

            /* write filter taps */
            num_flt = this->num_filter_norm + this->num_filter_conj;
            welt_c_fifo_write(this->out_flt_coeff, &num_flt);
            for (i = 0; i < num_flt; i++){
                value = this->param[2 + i];
                welt_c_fifo_write(this->out_flt[i], &value);
                welt_c_fifo_write(this->out_flt_coeff, &value);
            }
            welt_c_fifo_write(this->out_poly, &(this->num_filter_norm));
            welt_c_fifo_write(this->out_poly, &(this->num_filter_conj));
            welt_c_fifo_write(this->out_acc, &num_flt);
            this->mode = PARAM_DISTRIB_FLT_MODE_READ;
            this->iter++;
            break;
        default:
            this->mode = PARAM_DISTRIB_FLT_MODE_READ;
            break;
    }
}

void param_distrib_flt::reset() {
    this->mode = PARAM_DISTRIB_FLT_MODE_WRITE;
}

void param_distrib_flt::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;

    /* Register the port in enclosing graph. */
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection(this, port_index, direction);

    /* Register the port in enclosing graph. */
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 1;
    graph->add_connection(this, port_index, direction);

    /* Register the port in enclosing graph. */
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 2;
    graph->add_connection(this, port_index, direction);

    int i;
    for (i = 3; i < MAX_FLT+3; i++){
        /* Register the port in enclosing graph. */
        direction = GRAPH_OUT_CONN_DIRECTION;
        port_index = i;
        graph->add_connection(this, port_index, direction);
    }
}
