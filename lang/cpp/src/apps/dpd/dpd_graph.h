/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#ifndef DPD_GRAPH_H
#define DPD_GRAPH_H

#include "welt_cpp_graph.h"
#include "welt_cpp_util.h"

#include "file_source_int.h"
#include "accumulator.h"
#include "basis_matrix.h"
#include "config_sink_float.h"
#include "config_sink_int.h"
#include "conjugate.h"
#include "dpd_filter.h"
#include "estimate.h"
#include "file_sink_float.h"
#include "file_source_float.h"
#include "flt_coeff_distrib.h"
#include "param_distrib_flt.h"
#include "param_distrib_est.h"
#include "polynomial.h"
#include "split_fifo.h"

#define BUFFER_CAPACITY (65536*4)

/* An enumeration of the actors in this application. */
#define ACTOR_SOURCE                    (0)
#define ACTOR_CONFIG_SINK_INT_SUBINIT   (1)

/* An enumeration of the fifos in this application. */
#define FIFO_SRC_OUT                    (0)

/* The total number of actors in the application. */
#define ACTOR_COUNT_INIT                (2)
#define FIFO_COUNT_INIT                 (1)

/* An enumeration of the actors in this application. */
#define PARAM_DISTRIB_EST       (0 + ACTOR_COUNT_INIT)
#define PA_IN_SRC               (1 + ACTOR_COUNT_INIT)
#define PA_OUT_SRC              (2 + ACTOR_COUNT_INIT)
#define SPLIT_FIFO              (3 + ACTOR_COUNT_INIT)
#define BASIS_MATRIX_NORM       (4 + ACTOR_COUNT_INIT)
#define CONJUGATE               (5 + ACTOR_COUNT_INIT)
#define BASIS_MATRIX_CONJ       (6 + ACTOR_COUNT_INIT)
#define ESTIMATE                (7 + ACTOR_COUNT_INIT)
#define ACTOR_CONFIG_SINK_FLOAT (8 + ACTOR_COUNT_INIT)

/* An enumeration of the fifos in this application. */
#define FIFO_PARAM_0        (0 + FIFO_COUNT_INIT)
#define FIFO_PARAM_1        (1 + FIFO_COUNT_INIT)
#define FIFO_PARAM_2        (2 + FIFO_COUNT_INIT)
#define FIFO_DATA_0         (3 + FIFO_COUNT_INIT)
#define FIFO_DATA_1         (4 + FIFO_COUNT_INIT)
#define FIFO_DATA_2         (5 + FIFO_COUNT_INIT)
#define FIFO_DATA_3         (6 + FIFO_COUNT_INIT)
#define FIFO_DATA_4         (7 + FIFO_COUNT_INIT)
#define FIFO_DATA_5         (8 + FIFO_COUNT_INIT)
#define FIFO_DATA_6         (9 + FIFO_COUNT_INIT)
#define FIFO_FLT_COEFF_0    (10 + FIFO_COUNT_INIT)

/* The total number of actors in the application. */
#define ACTOR_COUNT_SUBINIT         (9)
#define FIFO_COUNT_SUBINIT          (10)

#define FIFO_PARAM_COUNT_SUBINIT    (3)
#define FIFO_DATA_COUNT_SUBINIT     (7)

/* An enumeration of the actors in this application. */
#define ACTOR_PARAM_DISTRIB_FLT     (0 + ACTOR_COUNT_SUBINIT + ACTOR_COUNT_INIT)
#define ACTOR_FLT_COEFF_DISTRIB     (1 + ACTOR_COUNT_SUBINIT + ACTOR_COUNT_INIT)
#define ACTOR_FILE_SOURCE_DATA      (2 + ACTOR_COUNT_SUBINIT + ACTOR_COUNT_INIT)
#define ACTOR_POLY                  (3 + ACTOR_COUNT_SUBINIT + ACTOR_COUNT_INIT)
#define ACTOR_ACC                   (4 + ACTOR_COUNT_SUBINIT + ACTOR_COUNT_INIT)
#define FILTER_0                    (5 + ACTOR_COUNT_SUBINIT + ACTOR_COUNT_INIT)
#define ACTOR_FILE_SINK (5 + MAX_FLT + ACTOR_COUNT_SUBINIT + ACTOR_COUNT_INIT)

/* The total number of actors in the application. */
#define ACTOR_COUNT_BODY            (6 + MAX_FLT)

#define FIFO_COUNT_BODY             (6 + 4 * MAX_FLT)
#define FIFO_PARAM_COUNT_BODY       (3 + MAX_FLT)
#define FIFO_DATA_COUNT_BODY        (2 + MAX_FLT)
#define FIFO_FLT_COEFF_COUNT_BODY   (1 + MAX_FLT)
#define FIFO_FLT_OUT_COUNT_BODY     (MAX_FLT)

#define TOTAL_ACTOR_COUNT   (6 + MAX_FLT+ACTOR_COUNT_SUBINIT+ACTOR_COUNT_INIT)
#define TOTAL_FIFO_COUNT (FIFO_COUNT_INIT+FIFO_COUNT_SUBINIT + FIFO_COUNT_BODY)


/*******************************************************************************
Overview: This is an example of  Graph ADT
for dpd application written by hand.
*******************************************************************************/

class dpd_graph: public welt_cpp_graph{
public:
    dpd_graph(char* data_filename, char*
            pa_in_filename, char* pa_out_filename, char*
            param_filename, char* output_filename);
    void scheduler() override;
    int* get_param();

private:

    int* param;
    int param_length;
    float* coeffs;
    int coeffs_length;

};
#endif
