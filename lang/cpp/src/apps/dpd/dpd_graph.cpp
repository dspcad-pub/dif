/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include "dpd_graph.h"

dpd_graph::dpd_graph(char* data_file, char*pa_in_file, char* pa_out_file, char*
param_file, char* output_file){

    this->param_length = 13;
    this->coeffs_length = 65536;
    this->param = (int*)malloc(sizeof(int)* this->param_length);
    this->coeffs =(float*) malloc(sizeof(float)* this->coeffs_length);
    this->actor_count = TOTAL_ACTOR_COUNT;
    this->actors.reserve(this->actor_count);
    this->descriptors.reserve(this->actor_count);
    this->fifo_count = TOTAL_FIFO_COUNT;
    this->fifos.reserve(this->fifo_count);
    int i;
    int token_size;

    /*Init fifo*/
    token_size = sizeof(int);

    this->fifos[0]= (welt_c_fifo_pointer)welt_c_fifo_new (BUFFER_CAPACITY,
            token_size, 0);

    /*subinit fifo*/
    token_size = sizeof(int);
    for(i = FIFO_COUNT_INIT; i < FIFO_PARAM_COUNT_SUBINIT+FIFO_COUNT_INIT; i++){
        this->fifos[i] = (welt_c_fifo_pointer)welt_c_fifo_new(BUFFER_CAPACITY,
                token_size, i);
    }

    token_size = sizeof(float);
    for(i = FIFO_PARAM_COUNT_SUBINIT+ FIFO_COUNT_INIT; i <
            FIFO_PARAM_COUNT_SUBINIT +
            FIFO_DATA_COUNT_SUBINIT+ FIFO_COUNT_INIT; i++){
        this->fifos[i] = (welt_c_fifo_pointer)welt_c_fifo_new(BUFFER_CAPACITY,
                token_size, i);
    }

    /*Body fifo*/
    token_size = sizeof(int);
    for(i = FIFO_COUNT_INIT+ FIFO_COUNT_SUBINIT; i < FIFO_PARAM_COUNT_BODY+
            FIFO_COUNT_INIT+ FIFO_COUNT_SUBINIT; i++){
        this->fifos[i] = (welt_c_fifo_pointer)welt_c_fifo_new(BUFFER_CAPACITY,
            token_size, i);
    }
    token_size = sizeof(float);
    for(i = FIFO_PARAM_COUNT_BODY + FIFO_COUNT_INIT+ FIFO_COUNT_SUBINIT; i <
            TOTAL_FIFO_COUNT; i++){
        this->fifos[i] = (welt_c_fifo_pointer)welt_c_fifo_new(BUFFER_CAPACITY,
                token_size, i);
    }

    /* Initialize source array and sink array */
    this->source_array.reserve(this->fifo_count);
    this->sink_array.reserve(this->fifo_count);

    /*Init actors*/
    i=0;
    this->actors[ACTOR_SOURCE] =new file_source_int
            ((welt_c_fifo_pointer)this->fifos[FIFO_SRC_OUT], param_file);
    this->descriptors[ACTOR_SOURCE] = (char*)"ACTOR_SOURCE";
    this->actors[ACTOR_SOURCE]->actor_set_index(ACTOR_SOURCE);
    this->actors[ACTOR_SOURCE]->connect((welt_cpp_graph*)this);

    this->actors[ACTOR_CONFIG_SINK_INT_SUBINIT]=
            (new config_sink_int((welt_c_fifo_pointer)this->
            fifos[FIFO_SRC_OUT], (this->param), 0, this->param_length));
    this->descriptors[ACTOR_CONFIG_SINK_INT_SUBINIT] =
            (char*)"ACTOR_CONFIG_SINK_INT_SUBINIT";
    this->actors[ACTOR_CONFIG_SINK_INT_SUBINIT]->actor_set_index
            (ACTOR_CONFIG_SINK_INT_SUBINIT);


    this->actors[ACTOR_CONFIG_SINK_INT_SUBINIT]->connect((welt_cpp_graph*)this);

    /*Subinit actors*/
    this->actors[PARAM_DISTRIB_EST] =
            new param_distrib_est(this->param,
                    (welt_c_fifo_pointer)this->fifos[FIFO_PARAM_0],
                    (welt_c_fifo_pointer)this->fifos[FIFO_PARAM_1],
                    (welt_c_fifo_pointer)this->fifos[FIFO_PARAM_2]);
    this->descriptors[PARAM_DISTRIB_EST] = (char*)"PARAM_DISTRIB_EST";
    this->actors[PARAM_DISTRIB_EST]->actor_set_index(PARAM_DISTRIB_EST);
    this->actors[PARAM_DISTRIB_EST]->connect((welt_cpp_graph*)this);

    this->actors[PA_IN_SRC] =
            new file_source_float(pa_in_file,
            (welt_c_fifo_pointer)this->fifos[FIFO_DATA_6]);
    this->descriptors[PA_IN_SRC] = (char*)"PA_IN_SRC";
    this->actors[PA_IN_SRC]->actor_set_index(PA_IN_SRC);
    this->actors[PA_IN_SRC]->connect((welt_cpp_graph*)this);

    this->actors[PA_OUT_SRC] = (new file_source_float(pa_out_file,
            (welt_c_fifo_pointer)this->fifos[FIFO_DATA_0]));
    this->descriptors[PA_OUT_SRC] = (char*)"PA_OUT_SRC";
    this->actors[PA_OUT_SRC]->connect((welt_cpp_graph*)this);
    this->actors[PA_OUT_SRC]->actor_set_index(PA_OUT_SRC);

    this->actors[SPLIT_FIFO] =
            (new split_fifo((welt_c_fifo_pointer)this->fifos[FIFO_DATA_0],
            (welt_c_fifo_pointer)this->fifos[FIFO_DATA_1],
            (welt_c_fifo_pointer)this->fifos[FIFO_DATA_2]));
    this->descriptors[SPLIT_FIFO] = (char*)"SPLIT_FIFO";
    this->actors[SPLIT_FIFO]->actor_set_index(SPLIT_FIFO);
    this->actors[SPLIT_FIFO]->connect((welt_cpp_graph*)this);

    this->actors[BASIS_MATRIX_NORM] = (new basis_matrix((welt_c_fifo_pointer)
            this->fifos[FIFO_DATA_1], (welt_c_fifo_pointer)
            this->fifos[FIFO_PARAM_0],
            (welt_c_fifo_pointer)this->fifos[FIFO_DATA_4]));
    this->descriptors[BASIS_MATRIX_NORM] = (char*)"BASIS_MATRIX_NORM";
    this->actors[BASIS_MATRIX_NORM]->actor_set_index(BASIS_MATRIX_NORM);
    this->actors[BASIS_MATRIX_NORM]->connect((welt_cpp_graph*)this);

    this->actors[CONJUGATE] = (new conjugate((welt_c_fifo_pointer)
            this->fifos[FIFO_DATA_2], (welt_c_fifo_pointer)this->
            fifos[FIFO_DATA_3]));
    this->descriptors[CONJUGATE] = (char*)"CONJUGATE";
    this->actors[CONJUGATE]->actor_set_index(CONJUGATE);

    this->actors[CONJUGATE]->connect((welt_cpp_graph*)this);

    this->actors[BASIS_MATRIX_CONJ] = (new basis_matrix((welt_c_fifo_pointer)
            this->fifos[FIFO_DATA_3], (welt_c_fifo_pointer)
            this->fifos[FIFO_PARAM_1], (welt_c_fifo_pointer)
            this->fifos[FIFO_DATA_5]));
    this->descriptors[BASIS_MATRIX_CONJ] =(char*) "BASIS_MATRIX_CONJ";
    this->actors[BASIS_MATRIX_CONJ]->actor_set_index(BASIS_MATRIX_CONJ);

    this->actors[BASIS_MATRIX_CONJ]->connect((welt_cpp_graph*)this);

    this->actors[ESTIMATE] = (new estimate( (welt_c_fifo_pointer)
            this->fifos[FIFO_PARAM_2], (welt_c_fifo_pointer)this->
            fifos[FIFO_DATA_4], (welt_c_fifo_pointer)this-> fifos[FIFO_DATA_5],
            (welt_c_fifo_pointer)this->fifos[FIFO_DATA_6], (welt_c_fifo_pointer)
            this->fifos[FIFO_FLT_COEFF_0]));
    this->descriptors[ESTIMATE] = (char*)"ESTIMATE";
    this->actors[ESTIMATE]->actor_set_index(ESTIMATE);

    this->actors[ESTIMATE]->connect((welt_cpp_graph*)this);

    this->actors[ACTOR_CONFIG_SINK_FLOAT] = (new config_sink_float
            ((welt_c_fifo_pointer)this->fifos[FIFO_FLT_COEFF_0],
            (this->coeffs),
            1, 1));
    this->descriptors[ACTOR_CONFIG_SINK_FLOAT]=
            (char*) "ACTOR_CONFIG_SINK_FLOAT";
    this->actors[ACTOR_CONFIG_SINK_FLOAT]->actor_set_index(
            ACTOR_CONFIG_SINK_FLOAT);

    this->actors[ACTOR_CONFIG_SINK_FLOAT]->connect((welt_cpp_graph*)this);


    /*Body actors*/
    this->actors[ACTOR_PARAM_DISTRIB_FLT] = new param_distrib_flt
            (this->param, this->fifos[FIFO_COUNT_SUBINIT +
            FIFO_COUNT_INIT],(welt_c_fifo_pointer)
            this->fifos[FIFO_COUNT_SUBINIT + FIFO_COUNT_INIT+ 2],
            (welt_c_fifo_pointer)this->fifos[FIFO_COUNT_SUBINIT +
            FIFO_COUNT_INIT+1], this->fifos, FIFO_COUNT_SUBINIT +
            FIFO_COUNT_INIT+ 3);
    this->descriptors[ACTOR_PARAM_DISTRIB_FLT] = (char*)
            "ACTOR_PARAM_DISTRIB_FLT";
    this->actors[ACTOR_PARAM_DISTRIB_FLT]->actor_set_index
            (ACTOR_PARAM_DISTRIB_FLT);

    this->actors[ACTOR_PARAM_DISTRIB_FLT]->connect((welt_cpp_graph*)this);

    this->actors[ACTOR_FLT_COEFF_DISTRIB] = (new flt_coeff_distrib(this->coeffs,
            (welt_c_fifo_pointer)this->fifos[FIFO_COUNT_SUBINIT +
            FIFO_COUNT_INIT+1], this->fifos,
            FIFO_PARAM_COUNT_BODY + FIFO_DATA_COUNT_BODY + 2 +
            FIFO_COUNT_SUBINIT + FIFO_COUNT_INIT, (welt_c_fifo_pointer)
            this->fifos[FIFO_COUNT_SUBINIT + FIFO_COUNT_INIT+
            FIFO_PARAM_COUNT_BODY + FIFO_DATA_COUNT_BODY ]));
    this->descriptors[ACTOR_FLT_COEFF_DISTRIB] = (char*)
            "ACTOR_FLT_COEFF_DISTRIB";
    this->actors[ACTOR_FLT_COEFF_DISTRIB]->actor_set_index(
            ACTOR_FLT_COEFF_DISTRIB);

    this->actors[ACTOR_FLT_COEFF_DISTRIB]->connect((welt_cpp_graph*)this);

    this->actors[ACTOR_FILE_SOURCE_DATA] = (new file_source_float
            (data_file, (welt_c_fifo_pointer)
            this->fifos[FIFO_COUNT_SUBINIT +
            FIFO_COUNT_INIT +3 + MAX_FLT]));
    this->descriptors[ACTOR_FILE_SOURCE_DATA] = (char*)"ACTOR_FILE_SOURCE_DATA";
    this->actors[ACTOR_FILE_SOURCE_DATA]->actor_set_index
            (ACTOR_FILE_SOURCE_DATA);

    this->actors[ACTOR_FILE_SOURCE_DATA]->connect((welt_cpp_graph*)this);

    this->actors[ACTOR_POLY] = (new polynomial( (welt_c_fifo_pointer)
            this->fifos[FIFO_COUNT_SUBINIT + FIFO_COUNT_INIT+ 3 + MAX_FLT],
            (welt_c_fifo_pointer)this->fifos[FIFO_COUNT_SUBINIT +
            FIFO_COUNT_INIT], this->fifos, FIFO_COUNT_SUBINIT +
            FIFO_COUNT_INIT+ 5 + MAX_FLT));
    
    this->descriptors[ACTOR_POLY] = (char*)"ACTOR_POLY";
    this->actors[ACTOR_POLY]->actor_set_index(ACTOR_POLY);
    this->actors[ACTOR_POLY]->connect((welt_cpp_graph*)this);

    this->actors[ACTOR_ACC] = (new accumulator(
            (welt_c_fifo_pointer)this->fifos[FIFO_COUNT_SUBINIT +
            FIFO_COUNT_INIT+2],
            this->fifos, FIFO_COUNT_SUBINIT +
            FIFO_COUNT_INIT+ 6 + 3 * MAX_FLT , this->
            fifos[FIFO_COUNT_SUBINIT + FIFO_COUNT_INIT+FIFO_PARAM_COUNT_BODY +
            FIFO_DATA_COUNT_BODY], (welt_c_fifo_pointer)
            this->fifos[FIFO_COUNT_SUBINIT +
            FIFO_COUNT_INIT+FIFO_PARAM_COUNT_BODY + 1]));
    this->descriptors[ACTOR_ACC] = (char*)"ACTOR_ACC";
    this->actors[ACTOR_ACC]->actor_set_index(ACTOR_ACC);
    this->actors[ACTOR_ACC]->connect((welt_cpp_graph*)this);

    for(i = 0; i < MAX_FLT; i++) {
        this->actors[5 + i + ACTOR_COUNT_INIT+ ACTOR_COUNT_SUBINIT] =
                new dpd_filter((welt_c_fifo_pointer)this->fifos[3 + i+
                FIFO_COUNT_SUBINIT + FIFO_COUNT_INIT],
                (welt_c_fifo_pointer)this->fifos[FIFO_PARAM_COUNT_BODY +
                FIFO_DATA_COUNT_BODY + FIFO_COUNT_SUBINIT + FIFO_COUNT_INIT+2+
                i], (welt_c_fifo_pointer)this->fifos[2+
                FIFO_PARAM_COUNT_BODY
                + FIFO_COUNT_SUBINIT + FIFO_COUNT_INIT+ i],
                (welt_c_fifo_pointer)this->fifos[6 + 3 * MAX_FLT +
                FIFO_COUNT_SUBINIT + FIFO_COUNT_INIT + i]);
        this->descriptors[5 + i + ACTOR_COUNT_INIT+ ACTOR_COUNT_SUBINIT] =
                (char*)"ACTOR_FLT";
        this->actors[5 + i + ACTOR_COUNT_INIT+ ACTOR_COUNT_SUBINIT]->
                actor_set_index(5 + i + ACTOR_COUNT_INIT+ ACTOR_COUNT_SUBINIT);
        this->actors[5 + i + ACTOR_COUNT_INIT+ ACTOR_COUNT_SUBINIT]->
                connect((welt_cpp_graph*)this);
    }

    this->actors[ACTOR_FILE_SINK] = new file_sink_float(
    (welt_c_fifo_pointer)this->fifos[FIFO_COUNT_SUBINIT +
            FIFO_COUNT_INIT+1 + FIFO_PARAM_COUNT_BODY], output_file);
    this->descriptors[ACTOR_FILE_SINK] =(char*) "ACTOR_FILE_SINK";
    this->actors[ACTOR_FILE_SINK]->actor_set_index(ACTOR_FILE_SINK);
    this->actors[ACTOR_FILE_SINK]->connect((welt_cpp_graph*)this);
}

void dpd_graph::scheduler(){

    /*Init*/
    int i=0, j =0;
    for (j = 0; j < 100; j++) {
        if (this->actors[0]->enable()) {
            this->actors[0]->invoke();
        }
    }
    i++;
    if (this->actors[ACTOR_CONFIG_SINK_INT_SUBINIT]->enable()) {
        this->actors[ACTOR_CONFIG_SINK_INT_SUBINIT]->invoke();
    }

    welt_cpp_util_guarded_execution(this->actors[PARAM_DISTRIB_EST],
            this->descriptors[PARAM_DISTRIB_EST]);
    welt_cpp_util_simple_scheduler(this->actors, PARAM_DISTRIB_EST+1
            ,ACTOR_COUNT_SUBINIT- 1, this->descriptors);

    welt_cpp_util_guarded_execution(this->actors[ACTOR_PARAM_DISTRIB_FLT],
            this->descriptors[ACTOR_PARAM_DISTRIB_FLT]);

    welt_cpp_util_simple_scheduler(this->actors,
            1+ACTOR_COUNT_SUBINIT+ACTOR_COUNT_INIT,
            ACTOR_COUNT_BODY-1, this->descriptors);

}

int* dpd_graph::get_param() {
    return this->param;
}

