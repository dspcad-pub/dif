/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "welt_cpp_dsg_scheduler.h"
#include "welt_cpp_actor.h"
#include "dpd_cdsg.h"
#include "welt_cpp_cdsg_util.h"


dpd_cdsg::dpd_cdsg (welt_cpp_graph* app_graph, int* target, int* param,
    pthread_mutex_t* mutex, pthread_cond_t* cond){
    this->dsg_count = DSG_ACTOR_COUNT;
    this->thread_count = NUM_THREAD;

    this -> first_actors_index.reserve(this->thread_count);

    this->set_fifo_count(DSG_FIFO_COUNT);

    this->set_actor_count(DSG_ACTOR_COUNT);

    this->actors.reserve(DSG_ACTOR_COUNT);

    this->fifos.reserve(DSG_FIFO_COUNT);
    this->descriptors.reserve(this->actor_count);
    (this->source_array).reserve(this->fifo_count);
    (this->sink_array).reserve(this->fifo_count);
    for (int i =0; i<DSG_FIFO_COUNT; i++){
        (this->source_array).push_back(nullptr);
        (this->sink_array).push_back(nullptr);
    }

    int token_size;

    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E46] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E46);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E21] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E21);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E28] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E28);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E35] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E35);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E2] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E2);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E17] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E17);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E15] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E15);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E51] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E51);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E34] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E34);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E42] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E42);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E27] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E27);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E37] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E37);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E44] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E44);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E29] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E29);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E33] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E33);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E47] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E47);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E16] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E16);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E19] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E19);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E22] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E22);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E39] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E39);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E0] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E0);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E8] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E8);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E14] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E14);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E30] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E30);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E31] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E31);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E18] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E18);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E6] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E6);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E55] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E55);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E4] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E4);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E11] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E11);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E5] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E5);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E1] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E1);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E20] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E20);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E3] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E3);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E50] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E50);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E52] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E52);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E45] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E45);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E53] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E53);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E23] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E23);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E48] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E48);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E32] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E32);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E24] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E24);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E54] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E54);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E36] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E36);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E38] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E38);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E10] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E10);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E26] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E26);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E13] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E13);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E25] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E25);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E41] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E41);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E43] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E43);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E40] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E40);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E7] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E7);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E9] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E9);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E12] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E12);
    token_size = sizeof(int);
    this->fifos[DSG_FIFO_E49] = (welt_c_fifo_pointer)welt_c_fifo_unit_size_new(
        token_size, DSG_FIFO_E49);

    this->actors[DSG_ACTOR_LAE3] = (welt_cpp_actor *) ( new
        welt_cpp_sca_sch_loop(this->fifos[DSG_FIFO_E54],
        this->fifos[DSG_FIFO_E31], 1, DSG_ACTOR_LAE3));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_LAE3], DSG_ACTOR_LAE3);


    welt_c_fifo_write_advance(( welt_c_fifo_pointer)this->fifos[DSG_FIFO_E54]);

        this->actors[DSG_ACTOR_RCONJ] = (welt_cpp_actor *) ( new
        welt_cpp_ref_actor(this->fifos[DSG_FIFO_E16],
        this->fifos[DSG_FIFO_E17], DSG_ACTOR_RCONJ,
        app_graph->actors[ACTOR_CONJUGATE], 0, 0));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_RCONJ], DSG_ACTOR_RCONJ);


    this->actors[DSG_ACTOR_RCONFIG] = (welt_cpp_actor *) ( new
        welt_cpp_ref_actor(this->fifos[DSG_FIFO_E4], this->fifos[DSG_FIFO_E5],
        DSG_ACTOR_RCONFIG, app_graph->actors[ACTOR_CONFIG_SINK_INIT_SUBINIT],
        0, 0));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_RCONFIG], 
        DSG_ACTOR_RCONFIG);


    this->actors[DSG_ACTOR_REC1] = (welt_cpp_actor *) ( new
        welt_cpp_param_rec_actor(this->fifos[DSG_FIFO_E55],
        this->fifos[DSG_FIFO_E0], this->fifos[DSG_FIFO_E1], mutex, cond, param,
        1, DSG_ACTOR_REC1));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_REC1], DSG_ACTOR_REC1);


    this->actors[DSG_ACTOR_LAE2] = (welt_cpp_actor *) ( new
        welt_cpp_sca_sch_loop(this->fifos[DSG_FIFO_E29],
        this->fifos[DSG_FIFO_E8], 1, DSG_ACTOR_LAE2));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_LAE2], DSG_ACTOR_LAE2);


    welt_c_fifo_write_advance(( welt_c_fifo_pointer)this->fifos[DSG_FIFO_E29]);

        this->actors[DSG_ACTOR_SCONJ] = (welt_cpp_actor *) ( new
        welt_cpp_sca_static_loop(this->fifos[DSG_FIFO_E15],
        this->fifos[DSG_FIFO_E17], this->fifos[DSG_FIFO_E16],
        this->fifos[DSG_FIFO_E18], 100, DSG_ACTOR_SCONJ));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_SCONJ], DSG_ACTOR_SCONJ);


    this->actors[DSG_ACTOR_RBNORM] = (welt_cpp_actor *) ( new
        welt_cpp_ref_actor(this->fifos[DSG_FIFO_E19],
        this->fifos[DSG_FIFO_E20], DSG_ACTOR_RBNORM,
        app_graph->actors[ACTOR_BASIS_MATRIX_NORM], 0, 0));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_RBNORM], 
        DSG_ACTOR_RBNORM);


    this->actors[DSG_ACTOR_RFILT3] = (welt_cpp_actor *) ( new
        welt_cpp_ref_actor(this->fifos[DSG_FIFO_E43],
        this->fifos[DSG_FIFO_E44], DSG_ACTOR_RFILT3,
        app_graph->actors[ACTOR_FILTER_2], 0, 0));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_RFILT3], 
        DSG_ACTOR_RFILT3);


    this->actors[DSG_ACTOR_SCONFIGS] = (welt_cpp_actor *) ( new
        welt_cpp_sca_static_loop(this->fifos[DSG_FIFO_E25],
        this->fifos[DSG_FIFO_E27], this->fifos[DSG_FIFO_E26],
        this->fifos[DSG_FIFO_E28], 72, DSG_ACTOR_SCONFIGS));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_SCONFIGS], 
        DSG_ACTOR_SCONFIGS);


    this->actors[DSG_ACTOR_RPDF] = (welt_cpp_actor *) ( new welt_cpp_ref_actor(
        this->fifos[DSG_FIFO_E33], this->fifos[DSG_FIFO_E34], DSG_ACTOR_RPDF,
        app_graph->actors[ACTOR_PARAM_DISTRIB_FLT], 0, 0));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_RPDF], DSG_ACTOR_RPDF);


    this->actors[DSG_ACTOR_RPAIN] = (welt_cpp_actor *) ( new welt_cpp_ref_actor(
        this->fifos[DSG_FIFO_E37], this->fifos[DSG_FIFO_E38], DSG_ACTOR_RPAIN,
        app_graph->actors[ACTOR_FILE_SOURCE_DATA], 0, 0));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_RPAIN], DSG_ACTOR_RPAIN);


    this->actors[DSG_ACTOR_RSINKF] = (welt_cpp_actor *) ( new
        welt_cpp_ref_actor(this->fifos[DSG_FIFO_E51],
        this->fifos[DSG_FIFO_E52], DSG_ACTOR_RSINKF,
        app_graph->actors[ACTOR_FILE_SINK], 0, 0));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_RSINKF], 
        DSG_ACTOR_RSINKF);


    this->actors[DSG_ACTOR_SSRC] = (welt_cpp_actor *) ( new
        welt_cpp_sca_static_loop(this->fifos[DSG_FIFO_E1],
        this->fifos[DSG_FIFO_E3], this->fifos[DSG_FIFO_E2],
        this->fifos[DSG_FIFO_E4], 100, DSG_ACTOR_SSRC));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_SSRC], DSG_ACTOR_SSRC);


    this->actors[DSG_ACTOR_SDISTRIB] = (welt_cpp_actor *) ( new
        welt_cpp_sca_static_loop(this->fifos[DSG_FIFO_E32],
        this->fifos[DSG_FIFO_E35], this->fifos[DSG_FIFO_E33],
        this->fifos[DSG_FIFO_E36], 3, DSG_ACTOR_SDISTRIB));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_SDISTRIB], 
        DSG_ACTOR_SDISTRIB);


    this->actors[DSG_ACTOR_RSRC] = (welt_cpp_actor *) ( new welt_cpp_ref_actor(
        this->fifos[DSG_FIFO_E2], this->fifos[DSG_FIFO_E3], DSG_ACTOR_RSRC,
        app_graph->actors[ACTOR_SOURCE], 0, 0));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_RSRC], DSG_ACTOR_RSRC);


    this->actors[DSG_ACTOR_SBASIS] = (welt_cpp_actor *) ( new
        welt_cpp_sca_static_loop(this->fifos[DSG_FIFO_E18],
        this->fifos[DSG_FIFO_E21], this->fifos[DSG_FIFO_E19],
        this->fifos[DSG_FIFO_E22], 2, DSG_ACTOR_SBASIS));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_SBASIS], 
        DSG_ACTOR_SBASIS);


    this->actors[DSG_ACTOR_RBCONJ] = (welt_cpp_actor *) ( new
        welt_cpp_ref_actor(this->fifos[DSG_FIFO_E20],
        this->fifos[DSG_FIFO_E21], DSG_ACTOR_RBCONJ,
        app_graph->actors[ACTOR_BASIS_MATRIX_CONJ], 0, 0));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_RBCONJ], 
        DSG_ACTOR_RBCONJ);


    this->actors[DSG_ACTOR_SES] = (welt_cpp_actor *) ( new
        welt_cpp_sca_static_loop(this->fifos[DSG_FIFO_E22],
        this->fifos[DSG_FIFO_E24], this->fifos[DSG_FIFO_E23],
        this->fifos[DSG_FIFO_E25], 4, DSG_ACTOR_SES));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_SES], DSG_ACTOR_SES);


    this->actors[DSG_ACTOR_SPFA] = (welt_cpp_actor *) ( new
        welt_cpp_sca_static_loop(this->fifos[DSG_FIFO_E39],
        this->fifos[DSG_FIFO_E49], this->fifos[DSG_FIFO_E40],
        this->fifos[DSG_FIFO_E50], 105402, DSG_ACTOR_SPFA));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_SPFA], DSG_ACTOR_SPFA);


    this->actors[DSG_ACTOR_RFILT4] = (welt_cpp_actor *) ( new
        welt_cpp_ref_actor(this->fifos[DSG_FIFO_E44],
        this->fifos[DSG_FIFO_E45], DSG_ACTOR_RFILT4,
        app_graph->actors[ACTOR_FILTER_3], 0, 0));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_RFILT4], 
        DSG_ACTOR_RFILT4);


    this->actors[DSG_ACTOR_RFILT1] = (welt_cpp_actor *) ( new
        welt_cpp_ref_actor(this->fifos[DSG_FIFO_E41],
        this->fifos[DSG_FIFO_E42], DSG_ACTOR_RFILT1,
        app_graph->actors[ACTOR_FILTER_0], 0, 0));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_RFILT1], 
        DSG_ACTOR_RFILT1);


    this->actors[DSG_ACTOR_RCONFIGS] = (welt_cpp_actor *) ( new
        welt_cpp_ref_actor(this->fifos[DSG_FIFO_E26],
        this->fifos[DSG_FIFO_E27], DSG_ACTOR_RCONFIGS,
        app_graph->actors[ACTOR_CONFIG_SINK_FLOAT], 0, 0));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_RCONFIGS], 
        DSG_ACTOR_RCONFIGS);


    this->actors[DSG_ACTOR_RSF] = (welt_cpp_actor *) ( new welt_cpp_ref_actor(
        this->fifos[DSG_FIFO_E13], this->fifos[DSG_FIFO_E14], DSG_ACTOR_RSF,
        app_graph->actors[ACTOR_SPLIT_FIFO], 0, 0));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_RSF], DSG_ACTOR_RSF);


    this->actors[DSG_ACTOR_RFILT5] = (welt_cpp_actor *) ( new
        welt_cpp_ref_actor(this->fifos[DSG_FIFO_E45],
        this->fifos[DSG_FIFO_E46], DSG_ACTOR_RFILT5,
        app_graph->actors[ACTOR_FILTER_4], 0, 0));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_RFILT5], 
        DSG_ACTOR_RFILT5);


    this->actors[DSG_ACTOR_RES] = (welt_cpp_actor *) ( new welt_cpp_ref_actor(
        this->fifos[DSG_FIFO_E23], this->fifos[DSG_FIFO_E24], DSG_ACTOR_RES,
        app_graph->actors[ACTOR_ESTIMATE], 0, 0));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_RES], DSG_ACTOR_RES);


    this->actors[DSG_ACTOR_SND3] = (welt_cpp_actor *) ( new
        welt_cpp_param_snd_actor(this->fifos[DSG_FIFO_E53],
        this->fifos[DSG_FIFO_E54], this->fifos[DSG_FIFO_E55], mutex, cond,
        param, 1, DSG_ACTOR_SND3));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_SND3], DSG_ACTOR_SND3);


    this->actors[DSG_ACTOR_RPI] = (welt_cpp_actor *) ( new welt_cpp_ref_actor(
        this->fifos[DSG_FIFO_E11], this->fifos[DSG_FIFO_E12], DSG_ACTOR_RPI,
        app_graph->actors[ACTOR_PA_IN_SRC], 0, 0));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_RPI], DSG_ACTOR_RPI);


    this->actors[DSG_ACTOR_SPA] = (welt_cpp_actor *) ( new
        welt_cpp_sca_static_loop(this->fifos[DSG_FIFO_E10],
        this->fifos[DSG_FIFO_E14], this->fifos[DSG_FIFO_E11],
        this->fifos[DSG_FIFO_E15], 201, DSG_ACTOR_SPA));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_SPA], DSG_ACTOR_SPA);


    this->actors[DSG_ACTOR_REC3] = (welt_cpp_actor *) ( new
        welt_cpp_param_rec_actor(this->fifos[DSG_FIFO_E30],
        this->fifos[DSG_FIFO_E31], this->fifos[DSG_FIFO_E32], mutex, cond,
        param, 1, DSG_ACTOR_REC3));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_REC3], DSG_ACTOR_REC3);


    this->actors[DSG_ACTOR_LAE1] = (welt_cpp_actor *) ( new
        welt_cpp_sca_sch_loop(this->fifos[DSG_FIFO_E6],
        this->fifos[DSG_FIFO_E0], 1, DSG_ACTOR_LAE1));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_LAE1], DSG_ACTOR_LAE1);


    welt_c_fifo_write_advance(( welt_c_fifo_pointer)this->fifos[DSG_FIFO_E6]);

        this->actors[DSG_ACTOR_SPAIN] = (welt_cpp_actor *) ( new
        welt_cpp_sca_static_loop(this->fifos[DSG_FIFO_E36],
        this->fifos[DSG_FIFO_E38], this->fifos[DSG_FIFO_E37],
        this->fifos[DSG_FIFO_E39], 210802, DSG_ACTOR_SPAIN));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_SPAIN], DSG_ACTOR_SPAIN);


    this->actors[DSG_ACTOR_RFILT6] = (welt_cpp_actor *) ( new
        welt_cpp_ref_actor(this->fifos[DSG_FIFO_E46],
        this->fifos[DSG_FIFO_E47], DSG_ACTOR_RFILT6,
        app_graph->actors[ACTOR_FILTER_5], 0, 0));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_RFILT6], 
        DSG_ACTOR_RFILT6);


    this->actors[DSG_ACTOR_REC2] = (welt_cpp_actor *) ( new
        welt_cpp_param_rec_actor(this->fifos[DSG_FIFO_E7],
        this->fifos[DSG_FIFO_E8], this->fifos[DSG_FIFO_E9], mutex, cond, param,
        1, DSG_ACTOR_REC2));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_REC2], DSG_ACTOR_REC2);


    this->actors[DSG_ACTOR_RFILT2] = (welt_cpp_actor *) ( new
        welt_cpp_ref_actor(this->fifos[DSG_FIFO_E42],
        this->fifos[DSG_FIFO_E43], DSG_ACTOR_RFILT2,
        app_graph->actors[ACTOR_FILTER_1], 0, 0));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_RFILT2], 
        DSG_ACTOR_RFILT2);


    this->actors[DSG_ACTOR_RPDE] = (welt_cpp_actor *) ( new welt_cpp_ref_actor(
        this->fifos[DSG_FIFO_E9], this->fifos[DSG_FIFO_E10], DSG_ACTOR_RPDE,
        app_graph->actors[ACTOR_PARAM_DISTRIB_EST], 0, 0));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_RPDE], DSG_ACTOR_RPDE);


    this->actors[DSG_ACTOR_RFCD] = (welt_cpp_actor *) ( new welt_cpp_ref_actor(
        this->fifos[DSG_FIFO_E34], this->fifos[DSG_FIFO_E35], DSG_ACTOR_RFCD,
        app_graph->actors[ACTOR_FLT_COEFF_DISTRIB], 0, 0));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_RFCD], DSG_ACTOR_RFCD);


    this->actors[DSG_ACTOR_RPO] = (welt_cpp_actor *) ( new welt_cpp_ref_actor(
        this->fifos[DSG_FIFO_E12], this->fifos[DSG_FIFO_E13], DSG_ACTOR_RPO,
        app_graph->actors[ACTOR_PA_OUT_SRC], 0, 0));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_RPO], DSG_ACTOR_RPO);


    this->actors[DSG_ACTOR_SSINKF] = (welt_cpp_actor *) ( new
        welt_cpp_sca_static_loop(this->fifos[DSG_FIFO_E50],
        this->fifos[DSG_FIFO_E52], this->fifos[DSG_FIFO_E51],
        this->fifos[DSG_FIFO_E53], 210800, DSG_ACTOR_SSINKF));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_SSINKF], 
        DSG_ACTOR_SSINKF);


    this->actors[DSG_ACTOR_SND1] = (welt_cpp_actor *) ( new
        welt_cpp_param_snd_actor(this->fifos[DSG_FIFO_E5],
        this->fifos[DSG_FIFO_E6], this->fifos[DSG_FIFO_E7], mutex, cond, param,
        1, DSG_ACTOR_SND1));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_SND1], DSG_ACTOR_SND1);


    this->actors[DSG_ACTOR_RPOLY] = (welt_cpp_actor *) ( new welt_cpp_ref_actor(
        this->fifos[DSG_FIFO_E40], this->fifos[DSG_FIFO_E41], DSG_ACTOR_RPOLY,
        app_graph->actors[ACTOR_POLY], 0, 0));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_RPOLY], DSG_ACTOR_RPOLY);


    this->actors[DSG_ACTOR_RFILT7] = (welt_cpp_actor *) ( new
        welt_cpp_ref_actor(this->fifos[DSG_FIFO_E47],
        this->fifos[DSG_FIFO_E48], DSG_ACTOR_RFILT7,
        app_graph->actors[ACTOR_FILTER_6], 0, 0));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_RFILT7], 
        DSG_ACTOR_RFILT7);


    this->actors[DSG_ACTOR_RACC] = (welt_cpp_actor *) ( new welt_cpp_ref_actor(
        this->fifos[DSG_FIFO_E48], this->fifos[DSG_FIFO_E49], DSG_ACTOR_RACC,
        app_graph->actors[ACTOR_ACC], 0, 0));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_RACC], DSG_ACTOR_RACC);


    this->actors[DSG_ACTOR_SND2] = (welt_cpp_actor *) ( new
        welt_cpp_param_snd_actor(this->fifos[DSG_FIFO_E28],
        this->fifos[DSG_FIFO_E29], this->fifos[DSG_FIFO_E30], mutex, cond,
        param, 1, DSG_ACTOR_SND2));
    this->dsg_add_actor(this,this->actors[ DSG_ACTOR_SND2], DSG_ACTOR_SND2);


    this->set_first_actor( this->actors[DSG_ACTOR_LAE1],DSG_ACTOR_LAE1);

        this->set_first_actor( this->actors[DSG_ACTOR_LAE2],DSG_ACTOR_LAE2);

        this->set_first_actor( this->actors[DSG_ACTOR_LAE3],DSG_ACTOR_LAE3);

	welt_c_fifo_write_advance(( welt_c_fifo_pointer)this->fifos[DSG_FIFO_E55]);
	welt_c_fifo_write_advance(( welt_c_fifo_pointer)this->fifos[DSG_FIFO_E54]);
	welt_c_fifo_write_advance(( welt_c_fifo_pointer)this->fifos[DSG_FIFO_E29]);
	welt_c_fifo_write_advance(( welt_c_fifo_pointer)this->fifos[DSG_FIFO_E6]);
    }

    void dpd_cdsg::dsg_scheduler(int thread_ind, int thread_num){
    int start_ind;
    int end_ind;
    start_ind = this->first_actors_index[thread_ind];
     if (thread_ind != thread_num -1){
         end_ind =this->first_actors_index[thread_ind+1] - 1;
      }else{
      end_ind = this->dsg_count -1;}

     welt_cpp_util_cdsg_scheduler( this->actors, start_ind, end_ind,
							   this->descriptors );
    return;
}
void dpd_cdsg::scheduler(){
    return;
}
