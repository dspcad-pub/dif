/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#ifndef DPD_SHCHED1_H
#define DPD_SHCHED1_H
/* This is a dataflow scheduling graph for dpd application using single
thread.*/
extern "C"{
#include "welt_c_util.h"
#include "welt_c_fifo.h"
}
#include "common_param.h"
#include "welt_cpp_graph.h"
#include "welt_cpp_dsg.h"
#include "welt_cpp_ref_actor.h"
#include "welt_cpp_sca_sch_loop.h"
#include "welt_cpp_sca_fi.h"
#include "welt_cpp_sca_static_loop.h"

#define DSG_ACTOR_LAE               (0)
#define DSG_ACTOR_FSI_SCA           (1)
#define DSG_ACTOR_FSI_REF           (2)
#define DSG_ACTOR_FCSI_REF          (3)
#define DSG_ACTOR_PDE_REF           (4)
#define DSG_ACTOR_PIPO_SCA          (5)
#define DSG_ACTOR_PI_REF            (6)
#define DSG_ACTOR_PO_REF            (7)
#define DSG_ACTOR_SPLIT_REF         (8)
#define DSG_ACTOR_CONJ_SCA          (9)
#define DSG_ACTOR_CONJ_REF          (10)
#define DSG_ACTOR_BM_SCA            (11)
#define DSG_ACTOR_BMN_REF           (12)
#define DSG_ACTOR_BMC_REF           (13)
#define DSG_ACTOR_EST_SCA           (14)
#define DSG_ACTOR_EST_REF           (15)
#define DSG_ACTOR_CSF_SCA           (16)
#define DSG_ACTOR_CSF_REF           (17)
#define DSG_ACTOR_PC_DISTRIB_SCA    (18)
#define DSG_ACTOR_PDF_REF           (19)
#define DSG_ACTOR_FCD_REF           (20)
#define DSG_ACTOR_PI_BODY_SCA       (21)
#define DSG_ACTOR_PI_BODY_REF       (22)
#define DSG_ACTOR_PFA_SCA           (23)
#define DSG_ACTOR_POLY_REF          (24)
#define DSG_ACTOR_FILT_REF          (25)
#define DSG_ACTOR_ACCU_REF      (25 + MAX_FLT)
#define DSG_ACTOR_SF_SCA        (26 + MAX_FLT)
#define DSG_ACTOR_SF_REF        (27 + MAX_FLT)
#define DSG_ACTOR_COUNT         (28 + MAX_FLT)

#define DSG_FIFO_LAE_FSI_SCA                (0)
#define DSG_FIFO_FSI_SCA_FSI_REF            (1)
#define DSG_FIFO_FSI_REF_FSI_SCA            (2)
#define DSG_FIFO_FSI_SCA_FCSI_REF           (3)
#define DSG_FIFO_FCSI_REF_PDE_REF           (4)
#define DSG_FIFO_PDE_REF_PIPO_SCA           (5)
#define DSG_FIFO_PIPO_SCA_PI_REF            (6)
#define DSG_FIFO_PI_REF_PO_REF              (7)
#define DSG_FIFO_PO_REF_SF_REF              (8)
#define DSG_FIFO_SF_REF_PIPO_SCA            (9)
#define DSG_FIFO_PIPO_SCA_CONJ_SCA          (10)
#define DSG_FIFO_CONJ_SCA_CONJ_REF          (11)
#define DSG_FIFO_CONJ_REF_CONJ_SCA          (12)
#define DSG_FIFO_CONJ_SCA_BM_SCA            (13)
#define DSG_FIFO_BM_SCA_BMN_REF             (14)
#define DSG_FIFO_BMN_REF_BMC_REF            (15)
#define DSG_FIFO_BMC_REF_BM_SCA             (16)
#define DSG_FIFO_BM_SCA_EST_SCA             (17)
#define DSG_FIFO_EST_SCA_EST_REF            (18)
#define DSG_FIFO_EST_REF_EST_SCA            (19)
#define DSG_FIFO_EST_SCA_CSF_SCA            (20)
#define DSG_FIFO_CSF_SCA_CSF_REF            (21)
#define DSG_FIFO_CSF_REF_CSF_SCA            (22)
#define DSG_FIFO_CSF_SCA_PC_DISTRIB_SCA     (23)
#define DSG_FIFO_PC_DISTRIB_SCA_PDF_REF     (24)
#define DSG_FIFO_PDF_REF_FCD_REF            (25)
#define DSG_FIFO_FCD_REF_PC_DISTRIB_SCA     (26)
#define DSG_FIFO_PC_DISTRIB_SCA_PI_BODY_SCA (27)
#define DSG_FIFO_PI_BODY_SCA_PI_BODY_REF    (28)
#define DSG_FIFO_PI_BODY_REF_PI_BODY_SCA    (29)
#define DSG_FIFO_PI_BODY_SCA_PFA_SCA        (30)
#define DSG_FIFO_PFA_SCA_POLY_REF           (31)
#define DSG_FIFO_POLY_REF_FILT_REF          (32)
#define DSG_FIFO_FILT_REF_ACCU_REF      (33 + MAX_FLT)
#define DSG_FIFO_ACCU_REF_PFA_SCA       (34 + MAX_FLT)
#define DSG_FIFO_PFA_SCA_SF_SCA         (35 + MAX_FLT)
#define DSG_FIFO_SF_SCA_SF_REF          (36 + MAX_FLT)
#define DSG_FIFO_SF_REF_SF_SCA          (37 + MAX_FLT)
#define DSG_FIFO_SF_SCA_LAE             (38 + MAX_FLT)
#define DSG_FIFO_COUNT                  (39 + MAX_FLT)

#define NAME_LENGTH     (20)
#define NUM_THREAD      (1)
/*******************************************************************************
Overview: This is an example of
Sequential Dataflow Schedule (SDSG)
for dpd application written by hand.
*******************************************************************************/

class dpd_shched1: public welt_cpp_dsg{
public:
    dpd_shched1(
            welt_cpp_graph * app_graph, int* target);
    void dsg_scheduler(int thread_ind, int thread_num);
    int thread_ind = 0;
    void scheduler();
private:
    int filt_num;
};

#endif
