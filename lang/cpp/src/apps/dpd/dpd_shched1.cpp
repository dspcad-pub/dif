/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <iostream>
#include "dpd_shched1.h"
#include "welt_cpp_dsg_scheduler.h"
#include "welt_cpp_actor.h"
#include "dpd_graph.h"

dpd_shched1::dpd_shched1(welt_cpp_graph *app_graph, int* target){

    this -> dsg_count = DSG_ACTOR_COUNT;
    this -> thread_count = NUM_THREAD;

    /* arrays of indices of first actors of each sdsg */
    this -> first_actors_index.reserve(this->thread_count);
    this -> ind = 0;

    this->set_fifo_count(DSG_FIFO_COUNT);
    this->set_actor_count(DSG_ACTOR_COUNT);

    this->actors.reserve(this->actor_count);
    this->fifos.reserve(this->fifo_count);
    this->descriptors.reserve(this->actor_count);

    (this->source_array).reserve(this->fifo_count);
    (this->sink_array).reserve(this->fifo_count);

    int i;
    for (i =0; i<DSG_FIFO_COUNT; i++){
        (this->source_array).push_back(nullptr);
        (this->sink_array).push_back(nullptr);
    }

    int token_size = sizeof(int);
    for (int i =0; i<DSG_FIFO_COUNT; i++){
        this->fifos[i]=(welt_c_fifo_pointer)
            welt_c_fifo_unit_size_new(token_size, i);
    }

    /* DSG_ACTOR_LAE */
    this->actors[DSG_ACTOR_LAE]=(welt_cpp_actor *) (new
            welt_cpp_sca_sch_loop(
            this->fifos[DSG_FIFO_SF_SCA_LAE],
            this->fifos[DSG_FIFO_LAE_FSI_SCA], 1, DSG_ACTOR_LAE));
    this->dsg_add_actor(this, this->actors[DSG_ACTOR_LAE], DSG_ACTOR_LAE);
    this->set_first_actor(this->actors[DSG_ACTOR_LAE], DSG_ACTOR_LAE);

    /* DSG_ACTOR_FSI_SCA */
    this->actors[DSG_ACTOR_FSI_SCA]=(welt_cpp_actor *) (new
            welt_cpp_sca_static_loop(
            this->fifos[DSG_FIFO_LAE_FSI_SCA],
            this->fifos[DSG_FIFO_FSI_REF_FSI_SCA],
            this->fifos[DSG_FIFO_FSI_SCA_FSI_REF],
            this->fifos[DSG_FIFO_FSI_SCA_FCSI_REF],
            100, DSG_ACTOR_FSI_SCA));
    this->dsg_add_actor(this, this->actors[DSG_ACTOR_FSI_SCA],
            DSG_ACTOR_FSI_SCA);

    /* DSG_ACTOR_FSI_REF */
    this->actors[DSG_ACTOR_FSI_REF]=(welt_cpp_actor *)(new welt_cpp_ref_actor(
            this->fifos[DSG_FIFO_FSI_SCA_FSI_REF] ,
            this->fifos[DSG_FIFO_FSI_REF_FSI_SCA], DSG_ACTOR_FSI_REF,
            app_graph->actors[ACTOR_SOURCE], 0, 0 ));
    this->dsg_add_actor(this, this->actors[DSG_ACTOR_FSI_REF],
            DSG_ACTOR_FSI_REF);

    /* DSG_ACTOR_FCSI_REF */
    this->actors[DSG_ACTOR_FCSI_REF]=(welt_cpp_actor*)(new welt_cpp_ref_actor(
            this->fifos[DSG_FIFO_FSI_SCA_FCSI_REF] ,
            this->fifos[DSG_FIFO_FCSI_REF_PDE_REF], DSG_ACTOR_FCSI_REF,
            app_graph->actors[ACTOR_CONFIG_SINK_INT_SUBINIT], 0, 0 ));
    this->dsg_add_actor(this,  this->actors[DSG_ACTOR_FCSI_REF],
            DSG_ACTOR_FCSI_REF);


    /* DSG_ACTOR_PDE_REF */
    this->actors[DSG_ACTOR_PDE_REF]=(welt_cpp_actor *)(new welt_cpp_ref_actor(
            this->fifos[DSG_FIFO_FCSI_REF_PDE_REF] ,
            this->fifos[DSG_FIFO_PDE_REF_PIPO_SCA], DSG_ACTOR_PDE_REF,
            app_graph->actors[PARAM_DISTRIB_EST], 0, 0 ));
    this->dsg_add_actor(this,  this->actors[DSG_ACTOR_PDE_REF],
            DSG_ACTOR_PDE_REF);

    /* DSG_ACTOR_PIPO_SCA */
    this->actors[DSG_ACTOR_PIPO_SCA]=(welt_cpp_actor *) (new
            welt_cpp_sca_static_loop(
            this->fifos[DSG_FIFO_PDE_REF_PIPO_SCA],
            this->fifos[DSG_FIFO_SF_REF_PIPO_SCA],
            this->fifos[DSG_FIFO_PIPO_SCA_PI_REF],
            this->fifos[DSG_FIFO_PIPO_SCA_CONJ_SCA],
            201, DSG_ACTOR_PIPO_SCA));
    this->dsg_add_actor(this, this->actors[DSG_ACTOR_PIPO_SCA],
            DSG_ACTOR_PIPO_SCA);

    /* DSG_ACTOR_PI_REF */
    this->actors[DSG_ACTOR_PI_REF]=(welt_cpp_actor *)(new welt_cpp_ref_actor(
            this->fifos[DSG_FIFO_PIPO_SCA_PI_REF],
            this->fifos[DSG_FIFO_PI_REF_PO_REF], DSG_ACTOR_PI_REF,
            app_graph->actors[PA_IN_SRC], 0, 0 ));
    this->dsg_add_actor(this, this->actors[DSG_ACTOR_PI_REF], DSG_ACTOR_PI_REF);

    /* DSG_ACTOR_PO_REF */
    this->actors[DSG_ACTOR_PO_REF]=(welt_cpp_actor *)(new welt_cpp_ref_actor(
            this->fifos[DSG_FIFO_PI_REF_PO_REF] ,
            this->fifos[DSG_FIFO_PO_REF_SF_REF], DSG_ACTOR_PO_REF,
            app_graph->actors[PA_OUT_SRC], 0, 0 ));
    this->dsg_add_actor(this, this->actors[DSG_ACTOR_PO_REF], DSG_ACTOR_PO_REF);

    /* DSG_ACTOR_SF_REF */
    this->actors[DSG_ACTOR_SPLIT_REF]=(welt_cpp_actor *)(new welt_cpp_ref_actor(
            this->fifos[DSG_FIFO_PO_REF_SF_REF] ,
            this->fifos[DSG_FIFO_SF_REF_PIPO_SCA], DSG_ACTOR_SPLIT_REF,
            app_graph->actors[SPLIT_FIFO], 0, 0 ));
    this->dsg_add_actor(this,  this->actors[DSG_ACTOR_SPLIT_REF],
            DSG_ACTOR_SPLIT_REF);

    /*DSG_ACTOR_CONJ_SCA */
    this->actors[DSG_ACTOR_CONJ_SCA] =(welt_cpp_actor *) (new
            welt_cpp_sca_static_loop(
            this->fifos[DSG_FIFO_PIPO_SCA_CONJ_SCA],
            this->fifos[DSG_FIFO_CONJ_REF_CONJ_SCA],
            this->fifos[DSG_FIFO_CONJ_SCA_CONJ_REF],
            this->fifos[DSG_FIFO_CONJ_SCA_BM_SCA],
            100, DSG_ACTOR_CONJ_SCA));
    this->dsg_add_actor(this, this->actors[DSG_ACTOR_CONJ_SCA],
            DSG_ACTOR_CONJ_SCA);

    /* DSG_ACTOR_CONJ_REF */
    this->actors[DSG_ACTOR_CONJ_REF]=(welt_cpp_actor *)(new welt_cpp_ref_actor(
            this->fifos[DSG_FIFO_CONJ_SCA_CONJ_REF] ,
            this->fifos[DSG_FIFO_CONJ_REF_CONJ_SCA], DSG_ACTOR_CONJ_REF,
            app_graph->actors[CONJUGATE], 0, 0 ));
    this->dsg_add_actor(this,  this->actors[DSG_ACTOR_CONJ_REF],
            DSG_ACTOR_CONJ_REF);

    /* DSG_ACTOR_BM_SCA */
    this->actors[DSG_ACTOR_BM_SCA] =(welt_cpp_actor *) (new
            welt_cpp_sca_static_loop(
            this->fifos[DSG_FIFO_CONJ_SCA_BM_SCA],
            this->fifos[DSG_FIFO_BMC_REF_BM_SCA],
            this->fifos[DSG_FIFO_BM_SCA_BMN_REF],
            this->fifos[DSG_FIFO_BM_SCA_EST_SCA],
            2, DSG_ACTOR_BM_SCA));
    this->dsg_add_actor(this, this->actors[DSG_ACTOR_BM_SCA],
            DSG_ACTOR_BM_SCA);

    /* DSG_ACTOR_BMN_REF */
    this->actors[DSG_ACTOR_BMN_REF]=(welt_cpp_actor *)(new welt_cpp_ref_actor(
            this->fifos[DSG_FIFO_BM_SCA_BMN_REF],
            this->fifos[DSG_FIFO_BMN_REF_BMC_REF], DSG_ACTOR_BMN_REF,
            app_graph->actors[BASIS_MATRIX_NORM], 0, 0 ));
    this->dsg_add_actor(this,  this->actors[DSG_ACTOR_BMN_REF],
            DSG_ACTOR_BMN_REF);

    /* DSG_ACTOR_BMC_REF */
    this->actors[DSG_ACTOR_BMC_REF] = (welt_cpp_actor *)(new welt_cpp_ref_actor(
            this->fifos[DSG_FIFO_BMN_REF_BMC_REF],
            this->fifos[DSG_FIFO_BMC_REF_BM_SCA], DSG_ACTOR_BMC_REF,
            app_graph->actors[BASIS_MATRIX_CONJ], 0, 0 ));
    this->dsg_add_actor(this,  this->actors[DSG_ACTOR_BMC_REF],
            DSG_ACTOR_BMC_REF);


    /* DSG_ACTOR_EST_SCA */
    this->actors[DSG_ACTOR_EST_SCA] =(welt_cpp_actor *) (new
            welt_cpp_sca_static_loop(
            this->fifos[DSG_FIFO_BM_SCA_EST_SCA],
            this->fifos[DSG_FIFO_EST_REF_EST_SCA],
            this->fifos[DSG_FIFO_EST_SCA_EST_REF],
            this->fifos[DSG_FIFO_EST_SCA_CSF_SCA],
            4, DSG_ACTOR_EST_SCA));
    this->dsg_add_actor(this, this->actors[DSG_ACTOR_EST_SCA],
            DSG_ACTOR_EST_SCA);

    /* DSG_ACTOR_EST_REF */
    this->actors[DSG_ACTOR_EST_REF] = (welt_cpp_actor *)(new welt_cpp_ref_actor(
            this->fifos[DSG_FIFO_EST_SCA_EST_REF],
            this->fifos[DSG_FIFO_EST_REF_EST_SCA], DSG_ACTOR_EST_REF,
            app_graph->actors[ESTIMATE], 0, 0 ));
    this->dsg_add_actor(this, this->actors[DSG_ACTOR_EST_REF],
            DSG_ACTOR_EST_REF);


    /* DSG_ACTOR_CSF_SCA */
    this->actors[DSG_ACTOR_CSF_SCA] =(welt_cpp_actor *) (new
            welt_cpp_sca_static_loop(
            this->fifos[DSG_FIFO_EST_SCA_CSF_SCA],
            this->fifos[DSG_FIFO_CSF_REF_CSF_SCA],
            this->fifos[DSG_FIFO_CSF_SCA_CSF_REF],
            this->fifos[DSG_FIFO_CSF_SCA_PC_DISTRIB_SCA],
            72, DSG_ACTOR_CSF_SCA));
    this->dsg_add_actor(this, this->actors[DSG_ACTOR_CSF_SCA],
            DSG_ACTOR_CSF_SCA);

    /* DSG_ACTOR_CSF_REF */
    this->actors[DSG_ACTOR_CSF_REF] = (welt_cpp_actor *)(new welt_cpp_ref_actor(
            this->fifos[DSG_FIFO_CSF_SCA_CSF_REF] ,
            this->fifos[DSG_FIFO_CSF_REF_CSF_SCA], DSG_ACTOR_CSF_REF,
            app_graph->actors[ACTOR_CONFIG_SINK_FLOAT], 0, 0 ));
    this->dsg_add_actor(this, this->actors[DSG_ACTOR_CSF_REF],
            DSG_ACTOR_CSF_REF);


    /* DSG_ACTOR_PC_DISTRIB_SCA */
    this->actors[DSG_ACTOR_PC_DISTRIB_SCA] =(welt_cpp_actor *) (new
            welt_cpp_sca_static_loop(
            this->fifos[DSG_FIFO_CSF_SCA_PC_DISTRIB_SCA],
            this->fifos[DSG_FIFO_FCD_REF_PC_DISTRIB_SCA],
            this->fifos[DSG_FIFO_PC_DISTRIB_SCA_PDF_REF],
            this->fifos[DSG_FIFO_PC_DISTRIB_SCA_PI_BODY_SCA],
            3, DSG_ACTOR_PC_DISTRIB_SCA));
    this->dsg_add_actor(this, this->actors[DSG_ACTOR_PC_DISTRIB_SCA],
            DSG_ACTOR_PC_DISTRIB_SCA);

    /* DSG_ACTOR_PDF_REF */
    this->actors[DSG_ACTOR_PDF_REF] = (welt_cpp_actor *)(new welt_cpp_ref_actor(
            this->fifos[DSG_FIFO_PC_DISTRIB_SCA_PDF_REF] ,
            this->fifos[DSG_FIFO_PDF_REF_FCD_REF], DSG_ACTOR_PDF_REF,
            app_graph->actors[ACTOR_PARAM_DISTRIB_FLT], 0, 0 ));
    this->dsg_add_actor(this, this->actors[DSG_ACTOR_PDF_REF],
            DSG_ACTOR_PDF_REF);

    /* DSG_ACTOR_FCD_REF */
    this->actors[DSG_ACTOR_FCD_REF] = (welt_cpp_actor *)(new welt_cpp_ref_actor(
            this->fifos[DSG_FIFO_PDF_REF_FCD_REF] ,
            this->fifos[DSG_FIFO_FCD_REF_PC_DISTRIB_SCA], DSG_ACTOR_FCD_REF,
            app_graph->actors[ACTOR_FLT_COEFF_DISTRIB], 0, 0 ));
    this->dsg_add_actor(this, this->actors[DSG_ACTOR_FCD_REF],
            DSG_ACTOR_FCD_REF);


    /* DSG_ACTOR_PI_BODY_SCA */
    this->actors[DSG_ACTOR_PI_BODY_SCA] =(welt_cpp_actor *) (new
            welt_cpp_sca_static_loop(
            this->fifos[DSG_FIFO_PC_DISTRIB_SCA_PI_BODY_SCA],
            this->fifos[DSG_FIFO_PI_BODY_REF_PI_BODY_SCA],
            this->fifos[DSG_FIFO_PI_BODY_SCA_PI_BODY_REF],
            this->fifos[DSG_FIFO_PI_BODY_SCA_PFA_SCA],
            210802, DSG_ACTOR_PI_BODY_SCA));
    this->dsg_add_actor(this, this->actors[DSG_ACTOR_PI_BODY_SCA],
            DSG_ACTOR_PI_BODY_SCA);


    /* DSG_ACTOR_PI_BODY_REF */
    this->actors[DSG_ACTOR_PI_BODY_REF]=(welt_cpp_actor *)(new
            welt_cpp_ref_actor(
            this->fifos[DSG_FIFO_PI_BODY_SCA_PI_BODY_REF] ,
            this->fifos[DSG_FIFO_PI_BODY_REF_PI_BODY_SCA],
            DSG_ACTOR_PI_BODY_REF,
            app_graph->actors[ACTOR_FILE_SOURCE_DATA], 0, 0 ));
    this->dsg_add_actor(this, this->actors[DSG_ACTOR_PI_BODY_REF],
            DSG_ACTOR_PI_BODY_REF);


    /* DSG_ACTOR_PFA_SCA */
    this->actors[DSG_ACTOR_PFA_SCA] =(welt_cpp_actor *) (new
            welt_cpp_sca_static_loop(
            this->fifos[DSG_FIFO_PI_BODY_SCA_PFA_SCA],
            this->fifos[DSG_FIFO_ACCU_REF_PFA_SCA],
            this->fifos[DSG_FIFO_PFA_SCA_POLY_REF],
            this->fifos[DSG_FIFO_PFA_SCA_SF_SCA],
            105402, DSG_ACTOR_PFA_SCA));
    this->dsg_add_actor(this, this->actors[DSG_ACTOR_PFA_SCA],
            DSG_ACTOR_PFA_SCA);


    /* DSG_ACTOR_POLY_REF */
    this->actors[DSG_ACTOR_POLY_REF]=(welt_cpp_actor *)(new welt_cpp_ref_actor(
            this->fifos[DSG_FIFO_PFA_SCA_POLY_REF],
            this->fifos[DSG_FIFO_POLY_REF_FILT_REF], DSG_ACTOR_POLY_REF,
            app_graph->actors[ACTOR_POLY], 0, 0 ));
    this->dsg_add_actor(this, this->actors[DSG_ACTOR_POLY_REF],
            DSG_ACTOR_POLY_REF);

    /* DSG_ACTOR_FILT_REF */
    for (int i=0;i< MAX_FLT;i++) {
        this->actors[DSG_ACTOR_FILT_REF] = (welt_cpp_actor * )(
                new welt_cpp_ref_actor(
                this->fifos[DSG_FIFO_POLY_REF_FILT_REF+i],
                this->fifos[DSG_FIFO_POLY_REF_FILT_REF+i+1], DSG_ACTOR_FILT_REF,
                app_graph->actors[FILTER_0+i], 0, 0));
        this->dsg_add_actor(this, this->actors[DSG_ACTOR_FILT_REF],
                DSG_ACTOR_FILT_REF);
    }

    /* DSG_ACTOR_ACCU_REF */
    this->actors[DSG_ACTOR_ACCU_REF]=(welt_cpp_actor *)(new welt_cpp_ref_actor(
            this->fifos[DSG_FIFO_POLY_REF_FILT_REF+MAX_FLT] ,
            this->fifos[DSG_FIFO_ACCU_REF_PFA_SCA], DSG_ACTOR_ACCU_REF,
            app_graph->actors[ACTOR_ACC], 0, 0 ));
    this->dsg_add_actor(this, this->actors[DSG_ACTOR_ACCU_REF],
            DSG_ACTOR_ACCU_REF);


    /* DSG_ACTOR_SF_SCA */
    this->actors[DSG_ACTOR_SF_SCA] =(welt_cpp_actor *) (new
            welt_cpp_sca_static_loop(
            this->fifos[DSG_FIFO_PFA_SCA_SF_SCA],
            this->fifos[DSG_FIFO_SF_REF_SF_SCA],
            this->fifos[DSG_FIFO_SF_SCA_SF_REF],
            this->fifos[DSG_FIFO_SF_SCA_LAE],
            210800, DSG_ACTOR_SF_SCA));
    this->dsg_add_actor(this, this->actors[DSG_ACTOR_SF_SCA],
            DSG_ACTOR_SF_SCA);

    /* DSG_ACTOR_SF_BODY_REF */
    this->actors[DSG_ACTOR_SF_REF] = (welt_cpp_actor *)(new welt_cpp_ref_actor(
            this->fifos[DSG_FIFO_SF_SCA_SF_REF],
            this->fifos[DSG_FIFO_SF_REF_SF_SCA], DSG_ACTOR_SF_REF,
            app_graph->actors[ACTOR_FILE_SINK], 0, 0 ));
    this->dsg_add_actor(this, this->actors[DSG_ACTOR_SF_REF], DSG_ACTOR_SF_REF);

    /* Delay */
    welt_c_fifo_write_advance(
            (welt_c_fifo_pointer)this->fifos[DSG_FIFO_LAE_FSI_SCA]);

}

void dpd_shched1::dsg_scheduler(int thread_ind, int thread_num) {
    int start_ind;
    int end_ind;
    start_ind = this->first_actors_index[thread_ind];
    if (thread_ind != thread_num -1){
            end_ind =this->first_actors_index[thread_ind+1] - 1;
    }else{
        end_ind = this->dsg_count -1;
    }

    /*simple scheduler*/
    //welt_c_util_cdsg_scheduler( this->actors, start_ind, end_ind,
    //this->descriptors );
    /*dtt scheduler*/
    welt_cpp_util_dsg_optimized_scheduler( this, start_ind, end_ind-start_ind
            + 1, (this->sink_array));

}

void dpd_shched1::scheduler() {
    return;
}
