/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdlib.h>
#include <string.h>
#include "dpd_graph_difdsg.h"


dpd_graph_difdsg::dpd_graph_difdsg(char* data_filename, char* pa_in_filename,
        char* pa_out_filename, char* param_filename, char* output_filename,
        int* param, int param_length, float* coeffs, int coeffs_length){
    this->data_filename = data_filename;
    this->pa_in_filename = pa_in_filename;
    this->pa_out_filename = pa_out_filename;
    this->param_filename = param_filename;
    this->output_filename = output_filename;
    this->param = param;
    this->param_length = param_length;
    this->coeffs = coeffs;
    this->coeffs_length = coeffs_length;
    this->actor_count = ACTOR_COUNT;
    this->fifo_count = FIFO_COUNT;

    this->actors.reserve(this->actor_count);
    this->descriptors.reserve(this->actor_count);
    this->fifos.reserve(this->fifo_count);
    this->source_array.reserve(this->fifo_count);
    this->sink_array.reserve(this->fifo_count);
    int token_size;

    token_size = sizeof(float);
    this->fifos[FIFO_BODY_PARAM7] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_PARAM7);
    token_size = sizeof(int);
    this->fifos[FIFO_DATA0] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_DATA0);
    token_size = sizeof(float);
    this->fifos[FIFO_BODY_PARAM3] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_PARAM3);
    token_size = sizeof(float);
    this->fifos[FIFO_BODY_COEFF0] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_COEFF0);
    token_size = sizeof(int);
    this->fifos[FIFO_BODY_DATA11] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_DATA11);
    token_size = sizeof(int);
    this->fifos[FIFO_BODY_DATA7] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_DATA7);
    token_size = sizeof(int);
    this->fifos[FIFO_DATA5] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_DATA5);
    token_size = sizeof(float);
    this->fifos[FIFO_BODY_PARAM1] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_PARAM1);
    token_size = sizeof(float);
    this->fifos[FIFO_BODY_DATA0] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_DATA0);
    token_size = sizeof(float);
    this->fifos[FIFO_BODY_OUT1] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_OUT1);
    token_size = sizeof(float);
    this->fifos[FIFO_BODY_OUT3] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_OUT3);
    token_size = sizeof(float);
    this->fifos[FIFO_PARAM1] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_PARAM1);
    token_size = sizeof(float);
    this->fifos[FIFO_BODY_PARAM8] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_PARAM8);
    token_size = sizeof(int);
    this->fifos[FIFO_BODY_DATA1] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_DATA1);
    token_size = sizeof(float);
    this->fifos[FIFO_BODY_COEFF1] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_COEFF1);
    token_size = sizeof(int);
    this->fifos[FIFO_DATA2] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_DATA2);
    token_size = sizeof(float);
    this->fifos[FIFO_BODY_COEFF3] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_COEFF3);
    token_size = sizeof(float);
    this->fifos[FIFO_BODY_COEFF5] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_COEFF5);
    token_size = sizeof(float);
    this->fifos[FIFO_BODY_COEFF4] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_COEFF4);
    token_size = sizeof(int);
    this->fifos[FIFO_SRC_OUT] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_SRC_OUT);
    token_size = sizeof(float);
    this->fifos[FIFO_BODY_PARAM2] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_PARAM2);
    token_size = sizeof(int);
    this->fifos[FIFO_BODY_DATA5] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_DATA5);
    token_size = sizeof(float);
    this->fifos[FIFO_BODY_OUT4] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_OUT4);
    token_size = sizeof(int);
    this->fifos[FIFO_BODY_DATA4] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_DATA4);
    token_size = sizeof(int);
    this->fifos[FIFO_DATA1] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_DATA1);
    token_size = sizeof(float);
    this->fifos[FIFO_BODY_PARAM9] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_PARAM9);
    token_size = sizeof(float);
    this->fifos[FIFO_BODY_COEFF2] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_COEFF2);
    token_size = sizeof(int);
    this->fifos[FIFO_DATA4] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_DATA4);
    token_size = sizeof(float);
    this->fifos[FIFO_BODY_OUT5] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_OUT5);
    token_size = sizeof(float);
    this->fifos[FIFO_BODY_OUT0] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_OUT0);
    token_size = sizeof(int);
    this->fifos[FIFO_DATA6] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_DATA6);
    token_size = sizeof(float);
    this->fifos[FIFO_BODY_COEFF10] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_COEFF10);
    token_size = sizeof(float);
    this->fifos[FIFO_BODY_PARAM4] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_PARAM4);
    token_size = sizeof(float);
    this->fifos[FIFO_BODY_COEFF6] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_COEFF6);
    token_size = sizeof(float);
    this->fifos[FIFO_BODY_OUT2] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_OUT2);
    token_size = sizeof(int);
    this->fifos[FIFO_BODY_DATA2] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_DATA2);
    token_size = sizeof(int);
    this->fifos[FIFO_BODY_DATA6] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_DATA6);
    token_size = sizeof(float);
    this->fifos[FIFO_BODY_PARAM6] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_PARAM6);
    token_size = sizeof(float);
    this->fifos[FIFO_PARAM2] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_PARAM2);
    token_size = sizeof(float);
    this->fifos[FIFO_PARAM0] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_PARAM0);
    token_size = sizeof(float);
    this->fifos[FIFO_BODY_PARAM0] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_PARAM0);
    token_size = sizeof(int);
    this->fifos[FIFO_DATA3] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_DATA3);
    token_size = sizeof(float);
    this->fifos[FIFO_FLT_COEFF_0] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_FLT_COEFF_0);
    token_size = sizeof(int);
    this->fifos[FIFO_BODY_DATA3] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_DATA3);
    token_size = sizeof(float);
    this->fifos[FIFO_BODY_PARAM5] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_PARAM5);
    token_size = sizeof(float);
    this->fifos[FIFO_BODY_OUT6] = ( welt_c_fifo_pointer)welt_c_fifo_new(
        BUFFER_CAPACITY, token_size,FIFO_BODY_OUT6);

    this->actors[ACTOR_CONFIG_SINK_FLOAT] = ( new config_sink_float(
        this->fifos[ FIFO_FLT_COEFF_0], this->coeffs, 1, 1));
    this->actors[ACTOR_CONFIG_SINK_FLOAT]
        ->actor_set_index(ACTOR_CONFIG_SINK_FLOAT);
    this->descriptors[ACTOR_CONFIG_SINK_FLOAT]=(char*)"ACTOR_CONFIG_SINK_FLOAT";
    this->actors[ACTOR_CONFIG_SINK_FLOAT]->connect((welt_cpp_graph *)this);

    this->actors[ACTOR_ACC] = ( new accumulator(this->fifos[ FIFO_BODY_PARAM2],
        this->fifos, FIFO_BODY_OUT0, this->fifos[ FIFO_BODY_COEFF10],
        this->fifos[ FIFO_BODY_DATA0]));
    this->actors[ACTOR_ACC]->actor_set_index(ACTOR_ACC);
    this->descriptors[ACTOR_ACC]=(char*)"ACTOR_ACC";
    this->actors[ACTOR_ACC]->connect((welt_cpp_graph *)this);

    this->actors[ACTOR_FILTER_4] = ( new dpd_filter(this->fifos[
        FIFO_BODY_PARAM7], this->fifos[ FIFO_BODY_COEFF4], this->fifos[
        FIFO_BODY_DATA5], this->fifos[ FIFO_BODY_OUT4]));
    this->actors[ACTOR_FILTER_4]->actor_set_index(ACTOR_FILTER_4);
    this->descriptors[ACTOR_FILTER_4]=(char*)"ACTOR_FILTER_4";
    this->actors[ACTOR_FILTER_4]->connect((welt_cpp_graph *)this);

    this->actors[ACTOR_FILTER_3] = ( new dpd_filter(this->fifos[
        FIFO_BODY_PARAM6], this->fifos[ FIFO_BODY_COEFF3], this->fifos[
        FIFO_BODY_DATA4], this->fifos[ FIFO_BODY_OUT3]));
    this->actors[ACTOR_FILTER_3]->actor_set_index(ACTOR_FILTER_3);
    this->descriptors[ACTOR_FILTER_3]=(char*)"ACTOR_FILTER_3";
    this->actors[ACTOR_FILTER_3]->connect((welt_cpp_graph *)this);

    this->actors[ACTOR_FILE_SINK] = ( new file_sink_float(this->fifos[
        FIFO_BODY_DATA0], output_filename));
    this->actors[ACTOR_FILE_SINK]->actor_set_index(ACTOR_FILE_SINK);
    this->descriptors[ACTOR_FILE_SINK]=(char*)"ACTOR_FILE_SINK";
    this->actors[ACTOR_FILE_SINK]->connect((welt_cpp_graph *)this);

    this->actors[ACTOR_ESTIMATE] = ( new estimate(this->fifos[ FIFO_PARAM2],
        this->fifos[ FIFO_DATA4], this->fifos[ FIFO_DATA5], this->fifos[
        FIFO_DATA6], this->fifos[ FIFO_FLT_COEFF_0]));
    this->actors[ACTOR_ESTIMATE]->actor_set_index(ACTOR_ESTIMATE);
    this->descriptors[ACTOR_ESTIMATE]=(char*)"ACTOR_ESTIMATE";
    this->actors[ACTOR_ESTIMATE]->connect((welt_cpp_graph *)this);

    this->actors[ACTOR_FILE_SOURCE_DATA] = ( new file_source_float(
        data_filename, this->fifos[ FIFO_BODY_DATA11]));
    this->actors[ACTOR_FILE_SOURCE_DATA]
        ->actor_set_index(ACTOR_FILE_SOURCE_DATA);
    this->descriptors[ACTOR_FILE_SOURCE_DATA]=(char*)"ACTOR_FILE_SOURCE_DATA";
    this->actors[ACTOR_FILE_SOURCE_DATA]->connect((welt_cpp_graph *)this);

    this->actors[ACTOR_SOURCE] = ( new file_source_int(this->fifos[
        FIFO_SRC_OUT], param_filename));
    this->actors[ACTOR_SOURCE]->actor_set_index(ACTOR_SOURCE);
    this->descriptors[ACTOR_SOURCE]=(char*)"ACTOR_SOURCE";
    this->actors[ACTOR_SOURCE]->connect((welt_cpp_graph *)this);

    this->actors[ACTOR_PA_OUT_SRC] = ( new file_source_float(pa_out_filename,
        this->fifos[ FIFO_DATA0]));
    this->actors[ACTOR_PA_OUT_SRC]->actor_set_index(ACTOR_PA_OUT_SRC);
    this->descriptors[ACTOR_PA_OUT_SRC]=(char*)"ACTOR_PA_OUT_SRC";
    this->actors[ACTOR_PA_OUT_SRC]->connect((welt_cpp_graph *)this);

    this->actors[ACTOR_FILTER_1] = ( new dpd_filter(this->fifos[
        FIFO_BODY_PARAM4], this->fifos[ FIFO_BODY_COEFF1], this->fifos[
        FIFO_BODY_DATA2], this->fifos[ FIFO_BODY_OUT1]));
    this->actors[ACTOR_FILTER_1]->actor_set_index(ACTOR_FILTER_1);
    this->descriptors[ACTOR_FILTER_1]=(char*)"ACTOR_FILTER_1";
    this->actors[ACTOR_FILTER_1]->connect((welt_cpp_graph *)this);

    this->actors[ACTOR_POLY] = ( new polynomial(this->fifos[ FIFO_BODY_DATA11],
        this->fifos[ FIFO_BODY_PARAM0], this->fifos, FIFO_BODY_DATA1));
    this->actors[ACTOR_POLY]->actor_set_index(ACTOR_POLY);
    this->descriptors[ACTOR_POLY]=(char*)"ACTOR_POLY";
    this->actors[ACTOR_POLY]->connect((welt_cpp_graph *)this);

    this->actors[ACTOR_PARAM_DISTRIB_EST] = ( new param_distrib_est(
        this->param, this->fifos[ FIFO_PARAM0], this->fifos[ FIFO_PARAM1],
        this->fifos[ FIFO_PARAM2]));
    this->actors[ACTOR_PARAM_DISTRIB_EST]
        ->actor_set_index(ACTOR_PARAM_DISTRIB_EST);
    this->descriptors[ACTOR_PARAM_DISTRIB_EST]=(char*)"ACTOR_PARAM_DISTRIB_EST";
    this->actors[ACTOR_PARAM_DISTRIB_EST]->connect((welt_cpp_graph *)this);

    this->actors[ACTOR_FILTER_2] = ( new dpd_filter(this->fifos[
        FIFO_BODY_PARAM5], this->fifos[ FIFO_BODY_COEFF2], this->fifos[
        FIFO_BODY_DATA3], this->fifos[ FIFO_BODY_OUT2]));
    this->actors[ACTOR_FILTER_2]->actor_set_index(ACTOR_FILTER_2);
    this->descriptors[ACTOR_FILTER_2]=(char*)"ACTOR_FILTER_2";
    this->actors[ACTOR_FILTER_2]->connect((welt_cpp_graph *)this);

    this->actors[ACTOR_PARAM_DISTRIB_FLT] = ( new param_distrib_flt(this->
        param, this->fifos[ FIFO_BODY_PARAM0], this->fifos[ FIFO_BODY_PARAM2],
        this->fifos[ FIFO_BODY_PARAM1], this->fifos, FIFO_BODY_PARAM3));
    this->actors[ACTOR_PARAM_DISTRIB_FLT]
        ->actor_set_index(ACTOR_PARAM_DISTRIB_FLT);
    this->descriptors[ACTOR_PARAM_DISTRIB_FLT]=(char*)"ACTOR_PARAM_DISTRIB_FLT";
    this->actors[ACTOR_PARAM_DISTRIB_FLT]->connect((welt_cpp_graph *)this);

    this->actors[ACTOR_FLT_COEFF_DISTRIB] = ( new flt_coeff_distrib(
        this->coeffs, this->fifos[ FIFO_BODY_PARAM1], this->fifos,
        FIFO_BODY_COEFF0, this->fifos[ FIFO_BODY_COEFF10]));
    this->actors[ACTOR_FLT_COEFF_DISTRIB]
        ->actor_set_index(ACTOR_FLT_COEFF_DISTRIB);
    this->descriptors[ACTOR_FLT_COEFF_DISTRIB]=(char*)"ACTOR_FLT_COEFF_DISTRIB";
    this->actors[ACTOR_FLT_COEFF_DISTRIB]->connect((welt_cpp_graph *)this);

    this->actors[ACTOR_FILTER_0] = ( new dpd_filter(this->fifos[
        FIFO_BODY_PARAM3], this->fifos[ FIFO_BODY_COEFF0], this->fifos[
        FIFO_BODY_DATA1], this->fifos[ FIFO_BODY_OUT0]));
    this->actors[ACTOR_FILTER_0]->actor_set_index(ACTOR_FILTER_0);
    this->descriptors[ACTOR_FILTER_0]=(char*)"ACTOR_FILTER_0";
    this->actors[ACTOR_FILTER_0]->connect((welt_cpp_graph *)this);

    this->actors[ACTOR_BASIS_MATRIX_NORM] = ( new basis_matrix(this->fifos[
        FIFO_DATA1], this->fifos[ FIFO_PARAM0], this->fifos[ FIFO_DATA4]));
    this->actors[ACTOR_BASIS_MATRIX_NORM]
        ->actor_set_index(ACTOR_BASIS_MATRIX_NORM);
    this->descriptors[ACTOR_BASIS_MATRIX_NORM]=(char*)"ACTOR_BASIS_MATRIX_NORM";
    this->actors[ACTOR_BASIS_MATRIX_NORM]->connect((welt_cpp_graph *)this);

    this->actors[ACTOR_FILTER_6] = ( new dpd_filter(this->fifos[
        FIFO_BODY_PARAM9], this->fifos[ FIFO_BODY_COEFF6], this->fifos[
        FIFO_BODY_DATA7], this->fifos[ FIFO_BODY_OUT6]));
    this->actors[ACTOR_FILTER_6]->actor_set_index(ACTOR_FILTER_6);
    this->descriptors[ACTOR_FILTER_6]=(char*)"ACTOR_FILTER_6";
    this->actors[ACTOR_FILTER_6]->connect((welt_cpp_graph *)this);

    this->actors[ACTOR_BASIS_MATRIX_CONJ] = ( new basis_matrix(this->fifos[
        FIFO_DATA3], this->fifos[ FIFO_PARAM1], this->fifos[ FIFO_DATA5]));
    this->actors[ACTOR_BASIS_MATRIX_CONJ]
        ->actor_set_index(ACTOR_BASIS_MATRIX_CONJ);
    this->descriptors[ACTOR_BASIS_MATRIX_CONJ]=(char*)"ACTOR_BASIS_MATRIX_CONJ";
    this->actors[ACTOR_BASIS_MATRIX_CONJ]->connect((welt_cpp_graph *)this);

    this->actors[ACTOR_CONFIG_SINK_INIT_SUBINIT] = ( new config_sink_int(
        this->fifos[ FIFO_SRC_OUT], (this->param), 0, this->param_length));
    this->actors[ACTOR_CONFIG_SINK_INIT_SUBINIT]
        ->actor_set_index(ACTOR_CONFIG_SINK_INIT_SUBINIT);
    this->descriptors[ACTOR_CONFIG_SINK_INIT_SUBINIT]=
        (char*)"ACTOR_CONFIG_SINK_INIT_SUBINIT";
    this->actors[ACTOR_CONFIG_SINK_INIT_SUBINIT]->connect((welt_cpp_graph
        *)this);

    this->actors[ACTOR_SPLIT_FIFO] = ( new split_fifo(this->fifos[ FIFO_DATA0],
        this->fifos[ FIFO_DATA1], this->fifos[ FIFO_DATA2]));
    this->actors[ACTOR_SPLIT_FIFO]->actor_set_index(ACTOR_SPLIT_FIFO);
    this->descriptors[ACTOR_SPLIT_FIFO]=(char*)"ACTOR_SPLIT_FIFO";
    this->actors[ACTOR_SPLIT_FIFO]->connect((welt_cpp_graph *)this);

    this->actors[ACTOR_PA_IN_SRC] = ( new file_source_float(pa_in_filename,
        this->fifos[ FIFO_DATA6]));
    this->actors[ACTOR_PA_IN_SRC]->actor_set_index(ACTOR_PA_IN_SRC);
    this->descriptors[ACTOR_PA_IN_SRC]=(char*)"ACTOR_PA_IN_SRC";
    this->actors[ACTOR_PA_IN_SRC]->connect((welt_cpp_graph *)this);

    this->actors[ACTOR_CONJUGATE] = ( new conjugate(this->fifos[ FIFO_DATA2],
        this->fifos[ FIFO_DATA3]));
    this->actors[ACTOR_CONJUGATE]->actor_set_index(ACTOR_CONJUGATE);
    this->descriptors[ACTOR_CONJUGATE]=(char*)"ACTOR_CONJUGATE";
    this->actors[ACTOR_CONJUGATE]->connect((welt_cpp_graph *)this);

    this->actors[ACTOR_FILTER_5] = ( new dpd_filter(this->fifos[
        FIFO_BODY_PARAM8], this->fifos[ FIFO_BODY_COEFF5], this->fifos[
        FIFO_BODY_DATA6], this->fifos[ FIFO_BODY_OUT5]));
    this->actors[ACTOR_FILTER_5]->actor_set_index(ACTOR_FILTER_5);
    this->descriptors[ACTOR_FILTER_5]=(char*)"ACTOR_FILTER_5";
    this->actors[ACTOR_FILTER_5]->connect((welt_cpp_graph *)this);


}
void dpd_graph_difdsg::scheduler(){
    /* scheduler*/

    return;
}
