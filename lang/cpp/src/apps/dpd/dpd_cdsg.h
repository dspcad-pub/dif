/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#ifndef _dpd_cdsg_h
#define _dpd_cdsg_h

#include <stdio.h>
#include <stdlib.h>
extern "C"{
#include "welt_c_util.h"
#include "welt_c_fifo.h"
#include "welt_c_fifo_unit_size.h"
}

#include "welt_cpp_graph.h"
#include "welt_cpp_dsg.h"

#include "welt_cpp_sca_sch_loop.h"
#include "welt_cpp_ref_actor.h"
#include "welt_cpp_param_rec_actor.h"
#include "welt_cpp_sca_static_loop.h"
#include "welt_cpp_param_snd_actor.h"

#include "dpd_graph_difdsg.h"

#define DSG_BUFFER_CAPACITY 1024

#define DSG_ACTOR_LAE3	23
#define DSG_ACTOR_RCONJ	14
#define DSG_ACTOR_RCONFIG	4
#define DSG_ACTOR_REC1	1
#define DSG_ACTOR_LAE2	6
#define DSG_ACTOR_SCONJ	13
#define DSG_ACTOR_RBNORM	16
#define DSG_ACTOR_RFILT3	34
#define DSG_ACTOR_SCONFIGS	20
#define DSG_ACTOR_RPDF	26
#define DSG_ACTOR_RPAIN	29
#define DSG_ACTOR_RSINKF	41
#define DSG_ACTOR_SSRC	2
#define DSG_ACTOR_SDISTRIB	25
#define DSG_ACTOR_RSRC	3
#define DSG_ACTOR_SBASIS	15
#define DSG_ACTOR_RBCONJ	17
#define DSG_ACTOR_SES	18
#define DSG_ACTOR_SPFA	30
#define DSG_ACTOR_RFILT4	35
#define DSG_ACTOR_RFILT1	32
#define DSG_ACTOR_RCONFIGS	21
#define DSG_ACTOR_RSF	12
#define DSG_ACTOR_RFILT5	36
#define DSG_ACTOR_RES	19
#define DSG_ACTOR_SND3	42
#define DSG_ACTOR_RPI	10
#define DSG_ACTOR_SPA	9
#define DSG_ACTOR_REC3	24
#define DSG_ACTOR_LAE1	0
#define DSG_ACTOR_SPAIN	28
#define DSG_ACTOR_RFILT6	37
#define DSG_ACTOR_REC2	7
#define DSG_ACTOR_RFILT2	33
#define DSG_ACTOR_RPDE	8
#define DSG_ACTOR_RFCD	27
#define DSG_ACTOR_RPO	11
#define DSG_ACTOR_SSINKF	40
#define DSG_ACTOR_SND1	5
#define DSG_ACTOR_RPOLY	31
#define DSG_ACTOR_RFILT7	38
#define DSG_ACTOR_RACC	39
#define DSG_ACTOR_SND2	22

#define DSG_FIFO_E46	46
#define DSG_FIFO_E21	21
#define DSG_FIFO_E28	28
#define DSG_FIFO_E35	35
#define DSG_FIFO_E2	2
#define DSG_FIFO_E17	17
#define DSG_FIFO_E15	15
#define DSG_FIFO_E51	51
#define DSG_FIFO_E34	34
#define DSG_FIFO_E42	42
#define DSG_FIFO_E27	27
#define DSG_FIFO_E37	37
#define DSG_FIFO_E44	44
#define DSG_FIFO_E29	29
#define DSG_FIFO_E33	33
#define DSG_FIFO_E47	47
#define DSG_FIFO_E16	16
#define DSG_FIFO_E19	19
#define DSG_FIFO_E22	22
#define DSG_FIFO_E39	39
#define DSG_FIFO_E0	0
#define DSG_FIFO_E8	8
#define DSG_FIFO_E14	14
#define DSG_FIFO_E30	30
#define DSG_FIFO_E31	31
#define DSG_FIFO_E18	18
#define DSG_FIFO_E6	6
#define DSG_FIFO_E55	55
#define DSG_FIFO_E4	4
#define DSG_FIFO_E11	11
#define DSG_FIFO_E5	5
#define DSG_FIFO_E1	1
#define DSG_FIFO_E20	20
#define DSG_FIFO_E3	3
#define DSG_FIFO_E50	50
#define DSG_FIFO_E52	52
#define DSG_FIFO_E45	45
#define DSG_FIFO_E53	53
#define DSG_FIFO_E23	23
#define DSG_FIFO_E48	48
#define DSG_FIFO_E32	32
#define DSG_FIFO_E24	24
#define DSG_FIFO_E54	54
#define DSG_FIFO_E36	36
#define DSG_FIFO_E38	38
#define DSG_FIFO_E10	10
#define DSG_FIFO_E26	26
#define DSG_FIFO_E13	13
#define DSG_FIFO_E25	25
#define DSG_FIFO_E41	41
#define DSG_FIFO_E43	43
#define DSG_FIFO_E40	40
#define DSG_FIFO_E7	7
#define DSG_FIFO_E9	9
#define DSG_FIFO_E12	12
#define DSG_FIFO_E49	49

#define DSG_ACTOR_COUNT	43
#define DSG_FIFO_COUNT	56
#define NUM_THREAD	3

/*******************************************************************************
Overview: This is an example of  CDSG (Concurrent Dataflow Schedule Graph)
for dpd application which uses 3 threads that is generated from DIF-DSG.
*******************************************************************************/

class dpd_cdsg: public welt_cpp_dsg{
public:
dpd_cdsg(welt_cpp_graph* app_graph, int* target, int* param, pthread_mutex_t*
    mutex, pthread_cond_t* cond);

void dsg_scheduler(int thread_ind, int thread_num);
int thread_ind = 0;
void
    scheduler(); 



};
#endif
