/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>

#include <sys/types.h>

#include "add_sdsg.h"
#include "welt_cpp_dsg_scheduler.h"

#include "welt_cpp_actor.h"
#include "add_graph.h"
#include "welt_cpp_cdsg_util.h"

add_sdsg::add_sdsg(welt_cpp_graph *contextApp){

    this -> dsg_count = DSG_ACTOR_COUNT;
    this -> thread_count = NUM_THREAD;

    /* arrays of indicies of first actors of each sdsg */
    this -> first_actors_index.reserve(this->thread_count);
    this -> ind = 0;

    this->set_fifo_count(DSG_FIFO_COUNT);
    this->set_actor_count(DSG_ACTOR_COUNT);

    this->actors.reserve(this->actor_count);
    this->fifos.reserve(this->fifo_count);
    this->descriptors.reserve(this->actor_count);

    (this->source_array).reserve(this->fifo_count);
    (this->sink_array).reserve(this->fifo_count);

    int i;
    for (i =0; i<DSG_FIFO_COUNT; i++){
        (this->source_array).push_back(nullptr);
        (this->sink_array).push_back(nullptr);
    }

    int token_size = sizeof(int);
    for (int i =0; i<DSG_FIFO_COUNT; i++){
        this->fifos[i]=(welt_c_fifo_pointer)
                welt_c_fifo_unit_size_new(token_size, i);
    }
    /* DSG actor LAE1 */
    this->actors[DSG_ACTOR_LAE] = (welt_cpp_actor *)(new welt_cpp_sca_sch_loop(
            this->fifos[DSG_FIFO_SNKREF_LAE],
            this->fifos[DSG_FIFO_LAE_XREF], 1, DSG_ACTOR_LAE));

    this->dsg_add_actor(this,  this->actors[DSG_ACTOR_LAE], DSG_ACTOR_LAE);
    this->set_first_actor(this->actors[DSG_ACTOR_LAE], DSG_ACTOR_LAE);

    /* DSG actor Xsource ref */
    this->actors[DSG_ACTOR_XREF] = (welt_cpp_actor *)(new welt_cpp_ref_actor(
            this->fifos[DSG_FIFO_LAE_XREF] ,
            this->fifos[DSG_FIFO_XREF_YREF], DSG_ACTOR_XREF,
            contextApp->actors[ACTOR_XSOURCE], 0, 0 ));
    this->dsg_add_actor(this,  this->actors[DSG_ACTOR_XREF], DSG_ACTOR_XREF);

    /* DSG actor Ysource ref */
    this->actors[DSG_ACTOR_YREF] = (welt_cpp_actor *)(new welt_cpp_ref_actor(
            this->fifos[DSG_FIFO_XREF_YREF],
            this->fifos[DSG_FIFO_YREF_ADDSCA], DSG_ACTOR_YREF,
            contextApp->actors[ACTOR_YSOURCE], 0, 0 ));
    this->dsg_add_actor(this, this->actors[DSG_ACTOR_YREF], DSG_ACTOR_YREF);

    /* DSG actor addSCA */
    this->actors[DSG_ACTOR_ADDSCA] =(welt_cpp_actor *) (new
            welt_cpp_sca_static_loop(
            this->fifos[DSG_FIFO_YREF_ADDSCA],
            this->fifos[DSG_FIFO_ADDREF_ADDSCA],
            this->fifos[DSG_FIFO_ADDSCA_ADDREF],
            this->fifos[DSG_FIFO_ADDSCA_SNKREF],
            1, DSG_ACTOR_ADDSCA));
    this->dsg_add_actor(this,this->actors[DSG_ACTOR_ADDSCA], DSG_ACTOR_ADDSCA);

    /* DSG actor addRef */
    this->actors[DSG_ACTOR_ADDREF] = (welt_cpp_actor *)(new welt_cpp_ref_actor(
            this->fifos[DSG_FIFO_ADDSCA_ADDREF],
            this->fifos[DSG_FIFO_ADDREF_ADDSCA], DSG_ACTOR_ADDREF,
            contextApp->actors[ACTOR_ADD], 0, 0 ));
    this->dsg_add_actor(this, this->actors[DSG_ACTOR_ADDREF], DSG_ACTOR_ADDREF);

    /* DSG actor sinkREF */
    this->actors[DSG_ACTOR_SNKREF] = (welt_cpp_actor *)(new welt_cpp_ref_actor(
            this->fifos[DSG_FIFO_ADDSCA_SNKREF],
            this->fifos[DSG_FIFO_SNKREF_LAE], DSG_ACTOR_SNKREF,
            contextApp->actors[ACTOR_SNK], 0, 0));
    this->dsg_add_actor(this,this->actors[DSG_ACTOR_SNKREF], DSG_ACTOR_SNKREF);

    /* Delay */
    welt_c_fifo_write_advance((welt_c_fifo_pointer)this->fifos[DSG_FIFO_SNKREF_LAE]);

}

void add_sdsg::dsg_scheduler(int thread_ind, int thread_num) {
    int start_ind;
    int end_ind;
    start_ind = this->first_actors_index[thread_ind];
    if (thread_ind != thread_num -1){
        end_ind =this->first_actors_index[thread_ind+1] - 1;
    }else{
        end_ind = this->dsg_count-1;
    }
    /*simple scheduler*/
    //welt_c_util_cdsg_scheduler( this->actors, start_ind, end_ind, this->descriptors );
    /*dtt scheduler*/
    welt_cpp_util_dsg_optimized_scheduler( this, start_ind, end_ind-start_ind
           + 1, (this->sink_array));

}

void add_sdsg::scheduler() {
    return;
}

