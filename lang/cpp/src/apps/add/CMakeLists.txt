SET(CMAKE_C_STANDARD 11)
SET(CMAKE_CXX_STANDARD 14)
SET(source_list
        add_shched2.cpp
		add_graph.cpp
		add_sdsg.cpp
       )

INCLUDE_DIRECTORIES(
        $ENV{UXWELTER}/lang/c/src/gems/actors/basic
        $ENV{UXWELTER}/lang/c/src/gems/actors/common
        $ENV{UXWELTER}/lang/c/src/gems/edges
        $ENV{UXWELTER}/lang/c/src/tools/runtime
        $ENV{UXWELTER}/lang/c/src/tools/graph
        $ENV{UXWELTER}/src/tools/scheduler/dsg
        $ENV{UXWELTER}/lang/cpp/src/gems/actors/common
        $ENV{UXWELTER}/lang/cpp/src/gems/actors/basic
        $ENV{UXWELTER}/lang/cpp/src/tools/graph
        $ENV{UXWELTER}/exp/lang/cpp/src/gems/actors/dpd
        $ENV{UXWELTER}/lang/cpp/src/gems/actors/dsg_actors/common
        $ENV{UXWELTER}/lang/cpp/src/gems/actors/dsg_actors/ref_actors
        $ENV{UXWELTER}/lang/cpp/src/gems/actors/dsg_actors/sca_actors
        $ENV{UXWELTER}/lang/cpp/src/gems/actors/dsg_actors/cdsg_actors
        $ENV{UXWELTER}/exp/lang/cpp/src/apps/dpd_app
        $ENV{UXWELTER}/lang/cpp/src/utils/dsg
        $ENV{UXWELTER}/lang/cpp/src/tools/scheduler/dsg
        $ENV{UXWELTER}/lang/cpp/src/tools/graph
        $ENV{UXWELTER}/lang/cpp/src/tools/graph
        $ENV{UXWELTER}/lang/cpp/src/tools/runtime
        $ENV{UXWELTER}/lang/cpp/src/apps/basic
        $ENV{UXWELTER}/lang/cpp/src/utils
        $ENV{UXWELTER}/lang/cpp/src/utils/dsg
		$ENV{UXWELTER}/lang/cpp/src/tools/graph
		$ENV{UXWELTER}/lang/cpp/src/tools/scheduler/dsg
        $ENV{UXDICELANG}/c/src/util
)

ADD_LIBRARY(add_app ${source_list})

TARGET_LINK_LIBRARIES( add_app)

INSTALL(TARGETS add_app DESTINATION $ENV{DIFGEN})
