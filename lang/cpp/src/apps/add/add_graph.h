
/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#ifndef _add_graph_h
#define _add_graph_h

extern "C" {
#include "welt_c_basic.h"
#include "welt_c_fifo.h"
#include "welt_c_util.h"
}

#include "file_source_int.h"
#include "welt_cpp_add.h"
#include "file_sink_int.h"
#include "welt_cpp_graph.h"
#include "welt_cpp_util.h"

#define BUFFER_CAPACITY (1024)

/* An enumeration of the actors in this application. */
#define ACTOR_XSOURCE    (0)
#define ACTOR_YSOURCE    (1)
#define ACTOR_ADD        (2)
#define ACTOR_SNK        (3)

#define FIFO_XSRC_ADD      (0)
#define FIFO_YSRC_ADD      (1)
#define FIFO_ADD_SNK       (2)

/* The total number of actors in the application. */
#define ACTOR_COUNT     (4)
#define FIFO_COUNT      (3)

/*******************************************************************************
Overview: This is a graph ADT for add application.
*******************************************************************************/

class add_graph : public welt_cpp_graph {
public:
    add_graph(char *x_file, char *y_file,
                       char *out_file);

    ~add_graph();

    void scheduler() override;

private:
    char *x_file;
    char *y_file;
    char *out_file;
};

#endif
