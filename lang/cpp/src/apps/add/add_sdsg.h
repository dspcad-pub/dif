/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#ifndef WELT_CPP_ADD_SHCHED2_H
#define WELT_CPP_ADD_SHCHED2_H

extern "C"{
#include "welt_c_util.h"
#include "welt_c_fifo.h"
}

#include "welt_cpp_graph.h"
#include "welt_cpp_dsg.h"
#include "welt_cpp_ref_actor.h"
#include "welt_cpp_sca_sch_loop.h"
#include "welt_cpp_sca_static_loop.h"
#include "welt_cpp_rec_actor.h"
#include "welt_cpp_snd_actor.h"

#define DSG_ACTOR_LAE      (0)
#define DSG_ACTOR_XREF      (1)
#define DSG_ACTOR_YREF      (2)
#define DSG_ACTOR_ADDSCA    (3)
#define DSG_ACTOR_ADDREF    (4)
#define DSG_ACTOR_SNKREF    (5)
#define DSG_ACTOR_COUNT     (6)

#define DSG_FIFO_SNKREF_LAE      (0)
#define DSG_FIFO_LAE_XREF      (1)
#define DSG_FIFO_XREF_YREF        (2)
#define DSG_FIFO_YREF_ADDSCA       (3)
#define DSG_FIFO_ADDSCA_ADDREF       (4)
#define DSG_FIFO_ADDREF_ADDSCA       (5)
#define DSG_FIFO_ADDSCA_SNKREF       (6)
#define DSG_FIFO_COUNT          (7)

#define NAME_LENGTH     (20)
#define NUM_THREAD      (1)

/*******************************************************************************
Overview: This is an example of  SDSG (Sequential thread Dataflow Schedule
Graph) for add  application.
*******************************************************************************/

class add_sdsg: public welt_cpp_dsg{
public:
    add_sdsg(welt_cpp_graph * contextApp);
    void dsg_scheduler(int thread_ind, int thread_num);
    int thread_ind = 0;
    void scheduler();

};

#endif
