
The Dataflow Interchange Format.

The Dataflow Interchange Format (DIF) is a design language for specifying
mixed-grain dataflow models for digital signal processing (DSP) systems. DSP
refers to the digital analysis and manipulation of data streams, such as those
arising from audio signals, images, video streams and digital communication
waveforms. Major objectives of the DIF project are to design this standard
language; to provide an extensible repository for representing, experimenting
with, and developing dataflow models and techniques; and to facilitate
technology transfer of applications across DSP design tools. Our ongoing work
on the DIF project is focusing on improving the DIF language and the DIF
package to represent more sophisticated dataflow semantics and exploring the
capability of DIF in transferring DSP applications and technology. This
exploration has resulted so far in an approach to automate exporting and
importing processes and a novel solution to porting DSP applications through
DIF.

More information about DIF can be found in the DIF Publications
Page:

http://dspcad.umd.edu/dif/publications.htm

Release created on Wed 03 Nov 2021 10:34:26 PM UTC
